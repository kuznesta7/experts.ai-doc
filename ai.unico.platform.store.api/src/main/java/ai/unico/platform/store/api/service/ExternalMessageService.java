package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ExternalMessageDto;
import ai.unico.platform.util.exception.InvalidParameterException;
import ai.unico.platform.util.exception.NonexistentEntityException;

import java.io.IOException;

public interface ExternalMessageService {

    void sendMessage(ExternalMessageDto externalMessageDto) throws NonexistentEntityException, InvalidParameterException, IOException;

    void sendContactUsMessage(ExternalMessageDto externalMessageDto) throws NonexistentEntityException, InvalidParameterException, IOException;

    void sendRequestInformationMessage(ExternalMessageDto externalMessageDto) throws NonexistentEntityException, InvalidParameterException, IOException;

    void sendApplyRequest(ExternalMessageDto externalMessageDto) throws NonexistentEntityException, InvalidParameterException, IOException;
}
