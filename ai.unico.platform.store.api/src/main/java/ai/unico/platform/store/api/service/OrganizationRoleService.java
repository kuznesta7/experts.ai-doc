package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.OrganizationLicenceUsersDto;
import ai.unico.platform.util.dto.OrganizationRolesDto;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface OrganizationRoleService {

    void addUserOrganizationRole(Long userId, Long organizationId, OrganizationRole role, UserContext userContext);

    void removeUserOrganizationRole(Long userId, Long organizationId, OrganizationRole role, UserContext userContext);

    List<OrganizationRolesDto> findOrganizationsForUser(Long userId);

    OrganizationLicenceUsersDto findOrganizationUsers(Long organizationId, OrganizationRole role, Integer page, Integer limit);

    List<OrganizationLicenceUsersDto> findOrganizationUsers(Long organizationId, Integer limit);
}
