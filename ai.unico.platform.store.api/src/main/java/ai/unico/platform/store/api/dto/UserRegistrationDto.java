package ai.unico.platform.store.api.dto;

import ai.unico.platform.util.enums.Role;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserRegistrationDto {

    private Long id;

    private String fullName;

    private String email;

    private Boolean social = false;

    private boolean termsOfUseAccepted = false;

    private List<OrganizationBaseDto> organizationDtos = new ArrayList<>();

    private List<Role> userRolePresets = new ArrayList<>();

    private Set<String> countryOfInterestCodes = new HashSet<>();

    private boolean invitation;

    private String password;

    public Set<String> getCountryOfInterestCodes() {
        return countryOfInterestCodes;
    }

    public void setCountryOfInterestCodes(Set<String> countryOfInterestCodes) {
        this.countryOfInterestCodes = countryOfInterestCodes;
    }

    public List<Role> getUserRolePresets() {
        return userRolePresets;
    }

    public void setUserRolePresets(List<Role> userRolePresets) {
        this.userRolePresets = userRolePresets;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<OrganizationBaseDto> getOrganizationDtos() {
        return organizationDtos;
    }

    public void setOrganizationDtos(List<OrganizationBaseDto> organizationDtos) {
        this.organizationDtos = organizationDtos;
    }

    public Boolean getSocial() {
        return social;
    }

    public void setSocial(Boolean social) {
        this.social = social;
    }

    public boolean isTermsOfUseAccepted() {
        return termsOfUseAccepted;
    }

    public void setTermsOfUseAccepted(boolean termsOfUseAccepted) {
        this.termsOfUseAccepted = termsOfUseAccepted;
    }

    public boolean isInvitation() {
        return invitation;
    }

    public void setInvitation(boolean invitation) {
        this.invitation = invitation;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
