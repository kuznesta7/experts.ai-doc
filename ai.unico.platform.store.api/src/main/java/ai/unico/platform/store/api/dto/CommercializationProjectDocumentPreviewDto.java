package ai.unico.platform.store.api.dto;

import java.util.Date;

public class CommercializationProjectDocumentPreviewDto {

    private Long commercializationId;

    private Long documentId;

    private String documentName;

    private String documentType;

    private Date dateCreated;

    private Long authorUserId;

    private Integer size;

    public Long getCommercializationId() {
        return commercializationId;
    }

    public void setCommercializationId(Long commercializationId) {
        this.commercializationId = commercializationId;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getAuthorUserId() {
        return authorUserId;
    }

    public void setAuthorUserId(Long authorUserId) {
        this.authorUserId = authorUserId;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
