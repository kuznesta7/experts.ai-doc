package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ProjectTransactionDto;
import ai.unico.platform.store.api.dto.ProjectTransactionPreviewDto;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface ProjectTransactionService {

    Long addTransaction(Long projectId, ProjectTransactionDto projectTransactionDto, UserContext userContext);

    void removeTransaction(Long projectId, Long transactionId, UserContext userContext);

    List<ProjectTransactionPreviewDto> findProjectTransactions(Long projectId);

}
