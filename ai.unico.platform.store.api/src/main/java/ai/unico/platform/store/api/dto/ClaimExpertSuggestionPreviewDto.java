package ai.unico.platform.store.api.dto;

import java.util.HashSet;
import java.util.Set;

public class ClaimExpertSuggestionPreviewDto {

    private Long expertId;

    private String expertFullName;

    private Set<String> organizationNames = new HashSet<>();

    private Set<String> itemNames = new HashSet<>();

    private Boolean alreadyClaimed;

    private Boolean alreadyClaimedByAnotherUser;

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public String getExpertFullName() {
        return expertFullName;
    }

    public void setExpertFullName(String expertFullName) {
        this.expertFullName = expertFullName;
    }

    public Set<String> getOrganizationNames() {
        return organizationNames;
    }

    public void setOrganizationNames(Set<String> organizationNames) {
        this.organizationNames = organizationNames;
    }

    public Set<String> getItemNames() {
        return itemNames;
    }

    public void setItemNames(Set<String> itemNames) {
        this.itemNames = itemNames;
    }

    public Boolean getAlreadyClaimed() {
        return alreadyClaimed;
    }

    public void setAlreadyClaimed(Boolean alreadyClaimed) {
        this.alreadyClaimed = alreadyClaimed;
    }

    public Boolean getAlreadyClaimedByAnotherUser() {
        return alreadyClaimedByAnotherUser;
    }

    public void setAlreadyClaimedByAnotherUser(Boolean alreadyClaimedByAnotherUser) {
        this.alreadyClaimedByAnotherUser = alreadyClaimedByAnotherUser;
    }
}
