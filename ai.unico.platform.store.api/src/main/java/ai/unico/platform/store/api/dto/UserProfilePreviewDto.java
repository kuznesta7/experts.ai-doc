package ai.unico.platform.store.api.dto;

import ai.unico.platform.util.dto.OrganizationDto;

import java.util.ArrayList;
import java.util.List;

public class UserProfilePreviewDto {

    private Long userId;

    private String name;

    private String username;

    private String email;

    private String description;

    private String positionDescription;

    private List<OrganizationDto> organizationDtos = new ArrayList<>();

    private String country;

    private String city;

    private List<TagDto> tagDtos;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPositionDescription() {
        return positionDescription;
    }

    public void setPositionDescription(String positionDescription) {
        this.positionDescription = positionDescription;
    }

    public List<OrganizationDto> getOrganizationDtos() {
        return organizationDtos;
    }

    public void setOrganizationDtos(List<OrganizationDto> organizationDtos) {
        this.organizationDtos = organizationDtos;
    }

    public List<TagDto> getTagDtos() {
        return tagDtos;
    }

    public void setTagDtos(List<TagDto> tagDtos) {
        this.tagDtos = tagDtos;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
