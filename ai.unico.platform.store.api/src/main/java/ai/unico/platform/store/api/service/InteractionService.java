package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.InteractionIndividualDto;
import ai.unico.platform.store.api.dto.InteractionWidgetDto;

public interface InteractionService {

    void saveInteraction(InteractionIndividualDto interactionIndividualDto);

    void saveInteraction(InteractionWidgetDto interactionWidgetDto);
}
