package ai.unico.platform.store.api.dto;

import ai.unico.platform.store.api.enums.IndexHistoryStatus;
import ai.unico.platform.store.api.enums.IndexTarget;

import java.util.Date;

public class IndexHistoryDto {

    private Long historyId;

    private IndexTarget target;

    private IndexHistoryStatus status;

    private String errorLog;

    private boolean clean;

    private Long userId;

    private Date date;

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public IndexTarget getTarget() {
        return target;
    }

    public void setTarget(IndexTarget target) {
        this.target = target;
    }

    public IndexHistoryStatus getStatus() {
        return status;
    }

    public void setStatus(IndexHistoryStatus status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isClean() {
        return clean;
    }

    public void setClean(boolean clean) {
        this.clean = clean;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getErrorLog() {
        return errorLog;
    }

    public void setErrorLog(String errorLog) {
        this.errorLog = errorLog;
    }
}
