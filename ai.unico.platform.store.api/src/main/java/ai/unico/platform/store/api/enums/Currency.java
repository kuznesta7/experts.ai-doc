package ai.unico.platform.store.api.enums;

public enum Currency {
    CZK,
    EUR,
    USD
}
