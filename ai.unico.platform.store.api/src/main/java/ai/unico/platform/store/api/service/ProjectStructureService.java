package ai.unico.platform.store.api.service;

import ai.unico.platform.util.model.UserContext;

public interface ProjectStructureService {

    void addParentProject(Long projectId, Long parentProjectId, Long organizationId, UserContext userContext);

    void removeParentProject(Long projectId, Long organizationId, UserContext userContext);

}
