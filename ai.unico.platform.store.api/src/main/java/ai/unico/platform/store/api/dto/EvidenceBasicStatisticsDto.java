package ai.unico.platform.store.api.dto;

public class EvidenceBasicStatisticsDto {

    private Long numOfAllItems;

    public Long getNumOfAllItems() {
        return numOfAllItems;
    }

    public void setNumOfAllItems(Long numOfAllItems) {
        this.numOfAllItems = numOfAllItems;
    }
}
