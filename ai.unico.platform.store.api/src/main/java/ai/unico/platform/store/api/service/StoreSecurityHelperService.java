package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ItemDetailDto;
import ai.unico.platform.store.api.dto.ProjectDto;
import ai.unico.platform.store.api.enums.WidgetType;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;

public interface StoreSecurityHelperService {

    Boolean isUserOwnerOfSearchHistoryItem(Long searchHistId, UserContext userContext);

    Boolean canUserSeeItem(Long itemId, UserContext userContext);

    Boolean canUserEditItem(ItemDetailDto itemDetailDto, UserContext userContext);

    Boolean canUserManageItemOrganization(Long itemId, Long organizationId, UserContext userContext);

    Boolean canUserManageItemExperts(Long itemId, Long userId, UserContext userContext);

    Boolean canUserSeeProject(Long projectId, UserContext userContext);

    Boolean canUserEditProject(ProjectDto projectDto, UserContext userContext);

    Boolean canUserEditProject(Long projectId, UserContext userContext);

    Boolean canUserSeeCommercializationProjectDetail(Long commercializationId, UserContext userContext);

    Boolean canUserEditCommercializationProject(Long commercializationId, UserContext userContext);

    Boolean doesUserOwnOriginalItem(Long originalItemId, UserContext userContext) throws IOException;

    Boolean hasOrganizationPublicProfile(Long organizationId);

    Boolean hasWidget(Long organizationId, WidgetType widgetType);
}
