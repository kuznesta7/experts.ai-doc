package ai.unico.platform.store.api.exception;

import ai.unico.platform.util.exception.RuntimeExceptionWithMessage;

public class InvalidCommercializationStatusException extends RuntimeExceptionWithMessage {
    public InvalidCommercializationStatusException(Long statusTypeId) {
        super("SELECTED_STATE_COULD_NOT_BE_SET", "commercialization project status type with id " + statusTypeId + " is invalid");
    }
}
