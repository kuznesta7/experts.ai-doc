package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ExternalMessageDto;
import ai.unico.platform.store.api.dto.InternalMessageDto;
import ai.unico.platform.store.api.dto.MessageDto;
import ai.unico.platform.store.api.enums.ClaimRequestType;

public interface UnicoNotifierService {

    void sendUserContactRequest(MessageDto messageDto, String contactEmail);

    void sendRegisterNotificationToUnico(String userMail);

    void sentRequestMail(InternalMessageDto internalMessageDto, String senderName, String organizationName, String contactEmail);

    void sendInternalOrganizationContactRequest(MessageDto messageDto, String emails, String contactEmail, String userFromName, String OrganizationName);

    void sendExternalContactRequest(MessageDto messageDto, String emails, String contactEmail);

    void sendExternalLectureRequest(MessageDto messageDto, String emails, String contactEmail);

    void sendRequestInformation(MessageDto messageDto, String emails, String contactEmail);

    void sendApplyRequest(MessageDto messageDto, String emails, String contactEmail);

    void sendExternalEquipmentRequest(MessageDto messageDto, String emails, String contactEmail);

    void sendExternalOutcomeProjectRequest(ExternalMessageDto externalMessageDto, MessageDto messageDto, String emails, String contactEmail);

    void sendExternalOrganizationContactRequest(MessageDto messageDto, String emails, String contactEmail, String organizationName);

    void sendItemUnclaimNotification(Long userId, String userName, Long itemId, String itemName, String message);

    void sendClaimRequestNotification(Long userId, String userName, String note, ClaimRequestType type);

}
