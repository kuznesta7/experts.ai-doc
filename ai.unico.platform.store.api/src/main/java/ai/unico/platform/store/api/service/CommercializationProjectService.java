package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.CommercializationProjectDto;
import ai.unico.platform.store.api.dto.CommercializationProjectIndexDto;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface CommercializationProjectService {

    Long createCommercializationProject(CommercializationProjectDto commercializationProjectDto, UserContext userContext);

    void updateCommercializationProject(Long commercializationProjectId, CommercializationProjectDto commercializationProjectDto, UserContext userContext);

    List<CommercializationProjectIndexDto> findCommercializationProjects(Integer limit, Integer offset);

    void loadAndIndexCommercializationProject(Long commercializationProjectId);
}
