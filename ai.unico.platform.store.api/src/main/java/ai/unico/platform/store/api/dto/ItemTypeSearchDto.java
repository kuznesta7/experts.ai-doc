package ai.unico.platform.store.api.dto;

public class ItemTypeSearchDto {

    private Long typeId;

    private Long typeGroupId;

    private Float weight;

    private String typeTitle;

    private String typeCode;

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getTypeTitle() {
        return typeTitle;
    }

    public void setTypeTitle(String typeTitle) {
        this.typeTitle = typeTitle;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public Long getTypeGroupId() {
        return typeGroupId;
    }

    public void setTypeGroupId(Long typeGroupId) {
        this.typeGroupId = typeGroupId;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }
}
