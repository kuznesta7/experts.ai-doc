package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.GlobalSearchStatisticsDto;

public interface GlobalStatisticsService {

    GlobalSearchStatisticsDto findSearchStatistics(Long userId);

}
