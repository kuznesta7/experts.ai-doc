package ai.unico.platform.store.api.dto;

public class PublicProfileVisibilityDto {
    private Long organizationId;
    private boolean visible;
    private boolean keywordsVisible;
    private boolean graphVisible;
    private boolean expertsVisible;
    private boolean outcomesVisible;
    private boolean projectsVisible;

    public PublicProfileVisibilityDto() {
    }

    public PublicProfileVisibilityDto(Long organizationId) {
        this.organizationId = organizationId;
        visible = false;
        keywordsVisible = false;
        graphVisible = false;
        expertsVisible = false;
        outcomesVisible = false;
        projectsVisible = false;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isKeywordsVisible() {
        return keywordsVisible;
    }

    public void setKeywordsVisible(boolean keywordsVisible) {
        this.keywordsVisible = keywordsVisible;
    }

    public boolean isGraphVisible() {
        return graphVisible;
    }

    public void setGraphVisible(boolean graphVisible) {
        this.graphVisible = graphVisible;
    }

    public boolean isExpertsVisible() {
        return expertsVisible;
    }

    public void setExpertsVisible(boolean expertsVisible) {
        this.expertsVisible = expertsVisible;
    }

    public boolean isOutcomesVisible() {
        return outcomesVisible;
    }

    public void setOutcomesVisible(boolean outcomesVisible) {
        this.outcomesVisible = outcomesVisible;
    }

    public boolean isProjectsVisible() {
        return projectsVisible;
    }

    public void setProjectsVisible(boolean projectsVisible) {
        this.projectsVisible = projectsVisible;
    }
}
