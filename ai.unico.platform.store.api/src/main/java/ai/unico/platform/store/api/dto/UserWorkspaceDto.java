package ai.unico.platform.store.api.dto;

public class UserWorkspaceDto {

    private Long userId;

    private String workspaceName;

    private String workspaceNote;

    public String getWorkspaceName() {
        return workspaceName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setWorkspaceName(String workspaceName) {
        this.workspaceName = workspaceName;
    }

    public String getWorkspaceNote() {
        return workspaceNote;
    }

    public void setWorkspaceNote(String workspaceNote) {
        this.workspaceNote = workspaceNote;
    }
}
