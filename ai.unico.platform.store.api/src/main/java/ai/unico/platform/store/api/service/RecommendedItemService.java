package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.enums.WidgetType;

public interface RecommendedItemService {

    void saveRecommendation(String recommId, String recommendation, WidgetType widgetType, String user, int count);

    void extendRecommendation(String recommId, String[] recomm);

    String[] getRecommendations(String recommId);

    int getNumberOfRecomms(String recommId);
}
