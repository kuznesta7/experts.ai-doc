package ai.unico.platform.store.api.service;

import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.model.UserContext;

public interface UserProfileImgService {

    void saveImg(UserContext userContext, Long userId, ImgType type, FileDto fileDto);

    FileDto getImg(Long userId, ImgType type, String size);

    enum ImgType {
        PORTRAIT, COVER
    }
}
