package ai.unico.platform.store.api.dto;

import java.util.ArrayList;
import java.util.List;

public class MultilingualDto<T> {

    private Long id;
    private String code;
    private T preview;
    private List<T> translations = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public T getPreview() {
        return preview;
    }

    public void setPreview(T preview) {
        this.preview = preview;
    }

    public List<T> getTranslations() {
        return translations;
    }

    public void setTranslations(List<T> translations) {
        this.translations = translations;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
