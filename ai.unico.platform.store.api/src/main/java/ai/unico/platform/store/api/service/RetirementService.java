package ai.unico.platform.store.api.service;

import ai.unico.platform.util.model.UserContext;

public interface RetirementService {

    void setExpertRetired(Long expertId, boolean retired, UserContext userContext);

    void setUserRetired(Long userId, boolean retired, UserContext userContext);
}
