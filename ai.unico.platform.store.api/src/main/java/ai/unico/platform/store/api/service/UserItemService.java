package ai.unico.platform.store.api.service;

import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface UserItemService {

    void addUserItem(Long userId, Long itemId, UserContext userContext) throws NonexistentEntityException;

    void addExpertItem(Long expertId, Long itemId, UserContext userContext) throws NonexistentEntityException;

    void removeUserItem(Long userId, Long itemId, String message, UserContext userContext) throws NonexistentEntityException;

    void removeExpertItem(Long expertId, Long itemId, String message, UserContext userContext) throws NonexistentEntityException;

    void setHideUserItem(Long userId, Long itemId, UserContext userContext, boolean hide) throws NonexistentEntityException;

    void setFavoriteUserItem(Long userId, Long itemId, UserContext userContext, boolean favorite) throws NonexistentEntityException;

    Set<Long> switchExpertItems(Map<Long, Boolean> newExperts, Long itemId, UserContext userContext) throws NonexistentEntityException;

    Set<Long> switchUserItems(Map<Long, Boolean> newUsers, Long itemId, UserContext userContext) throws NonexistentEntityException;

    List<Long> getUserItems(Long userId, UserContext userContext) throws NonexistentEntityException;

    List<Long> getExpertItems(Long expertId, UserContext userContext) throws NonexistentEntityException;
}
