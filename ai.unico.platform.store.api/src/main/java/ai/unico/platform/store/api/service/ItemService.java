package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ItemDetailDto;
import ai.unico.platform.store.api.dto.ItemIndexDto;
import ai.unico.platform.store.api.dto.ItemNotRegisteredExpertDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ItemService {

    Long createItem(ItemDetailDto itemDetailDto, UserContext userContext) throws IOException;

    void editItem(ItemDetailDto itemDetailDto, UserContext userContext) throws NonexistentEntityException, IOException;

    List<ItemNotRegisteredExpertDto> findNotRegisteredExperts(Set<Long> expertIds);

    List<ItemIndexDto> updateAndFindItems(Integer limit, Integer offset);

    void loadAndIndexItem(Long itemId);

    void loadAndIndexItems(Set<Long> itemId);

    Map<Long, Long> findVerifiedItemIdsMap(Set<Long> originalItemIds);
}
