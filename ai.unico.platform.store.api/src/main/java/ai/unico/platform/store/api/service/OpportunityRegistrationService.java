package ai.unico.platform.store.api.service;

import ai.unico.platform.util.model.UserContext;

import java.io.IOException;

public interface OpportunityRegistrationService {
    Long registerOpportunity(Long originalId, boolean clean, UserContext userContext) throws IOException;

    Long registerOpportunity(String originalCode, boolean clean, UserContext userContext) throws IOException;
}
