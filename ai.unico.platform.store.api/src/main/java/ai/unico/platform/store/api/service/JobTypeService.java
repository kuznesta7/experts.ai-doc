package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.CategoryDto;

import java.util.List;
import java.util.Map;

public interface JobTypeService {

    void updateJobTypesFromDwh();

    List<CategoryDto> getJobTypes();

    Map<Long, Long> translateJobTypes();

    CategoryDto getJobType(Long id);

}
