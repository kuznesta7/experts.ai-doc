package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ProfilePersonalNoteDto;
import ai.unico.platform.util.model.UserContext;

public interface ProfileNoteService {

    ProfilePersonalNoteDto findUserProfileNote(Long userFromId, Long userToId);

    ProfilePersonalNoteDto findExpertProfileNote(Long userFromId, Long expertId);

    void saveProfileNote(Long userId, ProfilePersonalNoteDto profilePersonalNoteDto, UserContext userContext);

}
