package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.EvidenceImportDto;
import ai.unico.platform.store.api.enums.EvidenceCategory;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;

public interface EvidenceImportService {

    void uploadImportFile(Long organizationId, EvidenceCategory evidenceCategory, FileDto fileDto, String note, UserContext userContext);

    EvidenceImportDto getImportDetail(Long evidenceImportId, Long organizationId, UserContext userContext) throws UserUnauthorizedException;

    FileDto getImportFile(Long evidenceImportId, Long organizationId, UserContext userContext) throws UserUnauthorizedException;

    void deleteImport(Long evidenceImportId, Long organizationId, UserContext userContext) throws UserUnauthorizedException;
}
