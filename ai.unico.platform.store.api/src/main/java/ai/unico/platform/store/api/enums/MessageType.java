package ai.unico.platform.store.api.enums;

public enum MessageType {
    ORGANIZATION,
    EXPERT,
    LECTURE,
    OUTCOME,
    PROJECT,
    REQUEST_INFORMATION,
    APPLY,
    CONTACT,
    EQUIPMENT
}
