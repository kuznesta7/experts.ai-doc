package ai.unico.platform.store.api.service;

import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.model.UserContext;

public interface FileService {

    FileDto getFile(Long fileId, UserContext userContext);

}
