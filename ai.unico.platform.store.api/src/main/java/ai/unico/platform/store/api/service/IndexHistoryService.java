package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.IndexHistoryDto;
import ai.unico.platform.store.api.enums.IndexHistoryStatus;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface IndexHistoryService {

    List<IndexHistoryDto> findLatest();

    Long saveIndexHistory(IndexTarget target, boolean clean, UserContext userContext);

    void updateIndexHistory(Long historyId, IndexHistoryStatus status, UserContext userContext);

    void updateIndexHistory(Long historyId, IndexHistoryStatus status, String errorLog, UserContext userContext);

}
