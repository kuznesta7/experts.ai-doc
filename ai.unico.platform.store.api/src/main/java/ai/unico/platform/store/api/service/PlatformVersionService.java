package ai.unico.platform.store.api.service;

public interface PlatformVersionService {

    String getDBVersion();

    String lastBuildDate();

}
