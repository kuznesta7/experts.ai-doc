package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.OrganizationLicenceDto;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.model.UserContext;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface OrganizationLicenceService {

    Long addOrganizationLicence(Long organizationId, OrganizationRole licenceRole, Date validUntil, Integer numberOfUserLicences, UserContext userContext);

    void invalidateLicence(Long organizationLicenceId, UserContext userContext);

    Boolean organizationHasLicense(Long organizationId);

    Boolean organizationsHasLicenses(Set<Long> organizationIds);

    List<OrganizationLicenceDto> getOrganizationLicences(Long organizationId, boolean validOnly);

    List<OrganizationLicenceDto> getOrganizationLicences(Set<Long> organizationIds, boolean validOnly);

}
