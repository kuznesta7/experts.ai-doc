package ai.unico.platform.store.api.service;


import ai.unico.platform.store.api.dto.HttpRequestDto;

public interface HttpRequestPersistService {

    void save(HttpRequestDto httpRequestDto);

}
