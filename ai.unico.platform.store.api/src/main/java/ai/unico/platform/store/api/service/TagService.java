package ai.unico.platform.store.api.service;

import java.util.List;

public interface TagService {

    List<String> findRelevantTags(String query);

}
