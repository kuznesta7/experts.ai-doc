package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ExpertOrganizationDto;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface ExpertOrganizationService {

    void addExpertOrganization(Long expertId, Long organizationId, boolean verified, UserContext userContext);

    void verifyExpertOrganization(Long expertId, Long organizationId, boolean verify, UserContext userContext) throws IOException;

    void retireExpertOrganization(Long expertId, Long organizationId, boolean retired, UserContext userContext) throws IOException;

    void changeExpertVisibilityInOrganization(Long expertId, Long organizationId, boolean visible, UserContext userContext) throws IOException;

    void changeExpertActivityInOrganization(Long expertId, Long organizationId, boolean active, UserContext userContext) throws IOException;

    void removeOrganizationExperts(Set<Long> expertIds, Long organizationId, UserContext userContext) throws IOException;

    List<ExpertOrganizationDto> findExpertOrganizations();

    List<Long> addExpertItemsToOrganization(Long expertId, Long organizationId, boolean verify, UserContext userContext) throws IOException;

}
