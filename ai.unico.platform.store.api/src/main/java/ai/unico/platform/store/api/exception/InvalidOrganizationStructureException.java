package ai.unico.platform.store.api.exception;

import ai.unico.platform.util.exception.RuntimeExceptionWithMessage;

public class InvalidOrganizationStructureException extends RuntimeExceptionWithMessage {
    public InvalidOrganizationStructureException(String secretMessage) {
        super("INVALID_ORGANIZATION_STRUCTURE", secretMessage);
    }
}
