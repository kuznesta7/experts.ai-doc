package ai.unico.platform.store.api.service;

import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

import java.util.Map;
import java.util.Set;

public interface ItemOrganizationService {

    void addItemOrganization(Long organizationId, Set<Long> itemId, boolean verify, UserContext userContext) throws NonexistentEntityException;

    void removeItemOrganization(Long organizationId, Long itemId, UserContext userContext) throws NonexistentEntityException;

    void removeOrganizationItems(Long organizationId, Set<Long> itemIds, UserContext userContext) throws NonexistentEntityException;

    void verifyOrganizationItems(Long organizationId, Set<Long> itemIds, boolean verify, UserContext userContext);

    void acrivateOrganizationItems(Long organizationId, Set<Long> itemIds, boolean active, UserContext userContext);

    Set<Long> switchOrganizationItems(Set<Long> dtoIds, Set<Long> entityIds, Long entityId, UserContext userContext) throws NonexistentEntityException;

    void addExternalItemOrganization(Long organizationId, Set<Long> externalItemIds, boolean verify, UserContext userContext) throws NonexistentEntityException ;

    Map<Long, Set<Long>> getExternalItemOrganizations(Set<Long> itemIds);

    Set<Long> getExternalItemOrganization(Long itemId);
}
