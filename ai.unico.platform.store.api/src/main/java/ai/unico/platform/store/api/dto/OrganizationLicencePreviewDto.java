package ai.unico.platform.store.api.dto;

import ai.unico.platform.util.enums.OrganizationRole;

import java.util.Date;

public class OrganizationLicencePreviewDto {

    private Long organizationLicenceId;

    private Date validUntil;

    private OrganizationRole licenceRole;

    public Long getOrganizationLicenceId() {
        return organizationLicenceId;
    }

    public void setOrganizationLicenceId(Long organizationLicenceId) {
        this.organizationLicenceId = organizationLicenceId;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    public OrganizationRole getLicenceRole() {
        return licenceRole;
    }

    public void setLicenceRole(OrganizationRole licenceRole) {
        this.licenceRole = licenceRole;
    }

}
