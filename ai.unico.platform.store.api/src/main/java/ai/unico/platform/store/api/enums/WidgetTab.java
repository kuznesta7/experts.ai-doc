package ai.unico.platform.store.api.enums;

public enum WidgetTab {
    EXPERT {
        @Override
        public WidgetType asWidgetType() {
            return WidgetType.ORGANIZATION_EXPERTS;
        }
    },
    RESEARCH_OUTCOME {
        @Override
        public WidgetType asWidgetType() {
            return WidgetType.ORGANIZATION_ITEMS;
        }
    },
    RESEARCH_PROJECT{
        @Override
        public WidgetType asWidgetType() {
            return WidgetType.ORGANIZATION_PROJECTS;
        }
    },
    COMMERCIALIZATION_PROJECT {
        @Override
        public WidgetType asWidgetType() {
            return WidgetType.ORGANIZATION_COMMERCIALIZATION;
        }
    },
    LECTURE {
        @Override
        public WidgetType asWidgetType() {
            return WidgetType.ORGANIZATION_LECTURES;
        }
    },
    EQUIPMENT {
        @Override
        public WidgetType asWidgetType() {
            return WidgetType.ORGANIZATION_EQUIPMENT;
        }
    },
    OPPORTUNITY {
        @Override
        public WidgetType asWidgetType() {
            return WidgetType.ORGANIZATION_OPPORTUNITY;
        }
    },
    ORGANIZATION_CHILDREN;

    public WidgetType asWidgetType() {
        return WidgetType.ORGANIZATION_OVERVIEW;
    }
}
