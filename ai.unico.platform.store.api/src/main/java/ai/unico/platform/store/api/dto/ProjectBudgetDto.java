package ai.unico.platform.store.api.dto;

public class ProjectBudgetDto {

    private Double totalAmount;

    private Double nationalSupportAmount;

    private Double privateAmount;

    private Double otherAmount;

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getNationalSupportAmount() {
        return nationalSupportAmount;
    }

    public void setNationalSupportAmount(Double nationalSupportAmount) {
        this.nationalSupportAmount = nationalSupportAmount;
    }

    public Double getPrivateAmount() {
        return privateAmount;
    }

    public void setPrivateAmount(Double privateAmount) {
        this.privateAmount = privateAmount;
    }

    public Double getOtherAmount() {
        return otherAmount;
    }

    public void setOtherAmount(Double otherAmount) {
        this.otherAmount = otherAmount;
    }
}
