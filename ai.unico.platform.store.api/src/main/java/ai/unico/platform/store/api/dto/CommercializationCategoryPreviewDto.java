package ai.unico.platform.store.api.dto;

public class CommercializationCategoryPreviewDto {

    private Long commercializationCategoryId;

    private String commercializationCategoryName;

    public void setCommercializationCategoryId(Long commercializationCategoryId) {
        this.commercializationCategoryId = commercializationCategoryId;
    }

    public Long getCommercializationCategoryId() {
        return commercializationCategoryId;
    }

    public void setCommercializationCategoryName(String commercializationCategoryName) {
        this.commercializationCategoryName = commercializationCategoryName;
    }

    public String getCommercializationCategoryName() {
        return commercializationCategoryName;
    }
}
