package ai.unico.platform.store.api.dto;

import ai.unico.platform.store.api.enums.OrganizationRelationType;

import java.util.ArrayList;
import java.util.List;

public class OrganizationStructureDto {

    private Long organizationId;

    private String organizationAbbrev;

    private String organizationName;

    private String countryCode;
    
    private Boolean memberOrganization;

    private OrganizationRelationType relationType;

    private List<OrganizationStructureDto> organizationUnits = new ArrayList<>();

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationAbbrev() {
        return organizationAbbrev;
    }

    public void setOrganizationAbbrev(String organizationAbbrev) {
        this.organizationAbbrev = organizationAbbrev;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public List<OrganizationStructureDto> getOrganizationUnits() {
        return organizationUnits;
    }

    public void setOrganizationUnits(List<OrganizationStructureDto> organizationUnits) {
        this.organizationUnits = organizationUnits;
    }

    public OrganizationRelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(OrganizationRelationType relationType) {
        this.relationType = relationType;
    }

    public Boolean getMemberOrganization() {
        return memberOrganization;
    }

    public void setMemberOrganization(Boolean memberOrganization) {
        this.memberOrganization = memberOrganization;
    }
}
