package ai.unico.platform.store.api.filter;

import java.util.Set;

public class FollowedProfilesFilter {

    private Integer limit = 10;

    private Integer page = 1;

    private Set<String> workspaces;

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Set<String> getWorkspaces() {
        return workspaces;
    }

    public void setWorkspaces(Set<String> workspaces) {
        this.workspaces = workspaces;
    }
}
