package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.CountryDto;

import java.util.List;

public interface CountryService {

    List<CountryDto> findAvailableCountries();

}
