package ai.unico.platform.store.api.enums;

public enum EvidenceCategory {

    ITEMS, EXPERTS, PROJECTS, COMMERCIALIZATION_PROJECTS

}
