package ai.unico.platform.store.api.service;

import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.model.UserContext;

public interface OrganizationLogoService {

    void saveImg(UserContext userContext, Long organizationId, ImgType type, FileDto fileDto);

    FileDto getImg(Long userId, ImgType imgType, String size);

    enum ImgType {
        LOGO, COVER
    }
}
