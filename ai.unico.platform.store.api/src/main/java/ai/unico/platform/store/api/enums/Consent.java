package ai.unico.platform.store.api.enums;

public class Consent {
    public static final Long PERSONALIZATION_BASED_ON_LOG_DATA = 1L;
    public static final Long TRANSFER_FOR_RECRUITMENT_PURPOSES = 2L;
    public static final Long TRANSFER_FOR_MAPPING_AND_CONNECTING = 3L;
    public static final Long TRANSFER_FOR_ANALYSIS = 4L;
}
