package ai.unico.platform.store.api.wrapper;

import ai.unico.platform.store.api.dto.SearchStatisticsDto;

import java.util.ArrayList;
import java.util.List;

public class SearchStatisticsWrapper {

    private Long numberOfResults;

    private Integer limit;

    private Integer page;

    private List<SearchStatisticsDto> searchStatisticsDtos = new ArrayList<>();

    public Long getNumberOfResults() {
        return numberOfResults;
    }

    public void setNumberOfResults(Long numberOfResults) {
        this.numberOfResults = numberOfResults;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<SearchStatisticsDto> getSearchStatisticsDtos() {
        return searchStatisticsDtos;
    }

    public void setSearchStatisticsDtos(List<SearchStatisticsDto> searchStatisticsDtos) {
        this.searchStatisticsDtos = searchStatisticsDtos;
    }
}
