package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ExpertsStatisticsDto;

import java.io.IOException;
import java.util.List;

public interface ExpertsStatisticsAdministrationService {

    List<ExpertsStatisticsDto> getCountryExpertsStatistics() throws IOException;

    ExpertsStatisticsDto getAllExpertsStatistics() throws IOException;
}
