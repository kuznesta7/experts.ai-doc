package ai.unico.platform.store.api.service;

import ai.unico.platform.util.enums.PlatformDataSource;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

import java.util.Set;

public interface ProjectItemService {

    void addItemToProject(Long projectId, Long itemId, PlatformDataSource itemDataSource, UserContext userContext) throws NonexistentEntityException;

    void removeItemFromProject(Long projectId, Long itemId, PlatformDataSource itemDataSource, UserContext userContext) throws NonexistentEntityException;

    void updateProjectItems(Set<Long> originalItemIds);

}
