package ai.unico.platform.store.api.dto;

import java.util.ArrayList;
import java.util.List;

public class EvidenceSearchedExpertStatisticsDto {

    private Long numberOfProfilesShown;

    private Integer page;

    private Integer limit;

    private List<ShownProfileStatisticsDto> mostShownProfiles = new ArrayList<>();

    public Long getNumberOfProfilesShown() {
        return numberOfProfilesShown;
    }

    public void setNumberOfProfilesShown(Long numberOfProfilesShown) {
        this.numberOfProfilesShown = numberOfProfilesShown;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<ShownProfileStatisticsDto> getMostShownProfiles() {
        return mostShownProfiles;
    }

    public void setMostShownProfiles(List<ShownProfileStatisticsDto> mostShownProfiles) {
        this.mostShownProfiles = mostShownProfiles;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public static class ShownProfileStatisticsDto {

        private Long userId;

        private Long expertId;

        private Long shownCount;

        private Double avgPosition;

        private Integer bestPosition;

        private String fullName;

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getExpertId() {
            return expertId;
        }

        public void setExpertId(Long expertId) {
            this.expertId = expertId;
        }

        public Long getShownCount() {
            return shownCount;
        }

        public void setShownCount(Long shownCount) {
            this.shownCount = shownCount;
        }

        public Double getAvgPosition() {
            return avgPosition;
        }

        public void setAvgPosition(Double avgPosition) {
            this.avgPosition = avgPosition;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public Integer getBestPosition() {
            return bestPosition;
        }

        public void setBestPosition(Integer bestPosition) {
            this.bestPosition = bestPosition;
        }
    }
}
