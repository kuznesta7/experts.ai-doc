package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.CommercializationProjectDocumentDto;
import ai.unico.platform.store.api.dto.CommercializationProjectDocumentPreviewDto;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface CommercializationDocumentService {

    List<CommercializationProjectDocumentPreviewDto> findCommercializationProjectDocuments(Long commercializationProjectId);

    FileDto getCommercializationProjectDocument(Long commercializationProjectId, Long fileId, UserContext userContext);

    Long addCommercializationProjectDocument(Long commercializationProjectId, CommercializationProjectDocumentDto documentDto, UserContext userContext);

    void deleteCommercializationProjectDocument(Long commercializationProjectId, Long fileId, UserContext userContext);

}
