package ai.unico.platform.store.api.enums;

public enum IndexTarget {

    ITEMS, EXPERTS, ORGANIZATIONS, ONTOLOGY, PROJECTS, COMMERCIALIZATION_PROJECTS, EXPERT_EVALUATION, ITEM_ASSOCIATION, THESIS, LECTURES, EQUIPMENT, OPPORTUNITY

}
