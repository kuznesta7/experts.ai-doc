package ai.unico.platform.store.api.dto;

import ai.unico.platform.store.api.enums.WidgetType;

public class InteractionWidgetDto extends InteractionDto {

    private Long interactionWidgetId;

    private WidgetType interactionType;

    private String interactionDetails;

    public Long getInteractionWidgetId() {
        return interactionWidgetId;
    }

    public void setInteractionWidgetId(Long interactionWidgetId) {
        this.interactionWidgetId = interactionWidgetId;
    }

    public WidgetType getInteractionType() {
        return interactionType;
    }

    public void setInteractionType(WidgetType interactionType) {
        this.interactionType = interactionType;
    }

    public String getInteractionDetails() {
        return interactionDetails;
    }

    public void setInteractionDetails(String interactionDetails) {
        if ("".equals(interactionDetails))
            interactionDetails = null;
        this.interactionDetails = interactionDetails;
    }
}
