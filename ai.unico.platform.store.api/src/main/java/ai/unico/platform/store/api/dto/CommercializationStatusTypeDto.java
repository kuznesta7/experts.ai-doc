package ai.unico.platform.store.api.dto;

import java.util.HashSet;
import java.util.Set;

public class CommercializationStatusTypeDto {

    private Long statusId;

    private Integer index;

    private String statusName;

    private String statusCode;

    private boolean useInSearch;

    private boolean useInInvest;

    private boolean canSetOrganization;

    private boolean deleted;

    private boolean mustBeFilled;

    private long statusOrder;

    private Set<Long> categoryIds = new HashSet<>();

    public long getStatusOrder() {
        return statusOrder;
    }

    public void setStatusOrder(long statusOrder) {
        this.statusOrder = statusOrder;
    }

    public void setMustBeFilled(boolean mustBeFilled) {
        this.mustBeFilled = mustBeFilled;
    }

    public boolean isMustBeFilled() {
        return mustBeFilled;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public boolean isUseInSearch() {
        return useInSearch;
    }

    public void setUseInSearch(boolean useInSearch) {
        this.useInSearch = useInSearch;
    }

    public boolean isUseInInvest() {
        return useInInvest;
    }

    public void setUseInInvest(boolean useInInvest) {
        this.useInInvest = useInInvest;
    }

    public Set<Long> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(Set<Long> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isCanSetOrganization() {
        return canSetOrganization;
    }

    public void setCanSetOrganization(boolean canSetOrganization) {
        this.canSetOrganization = canSetOrganization;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
