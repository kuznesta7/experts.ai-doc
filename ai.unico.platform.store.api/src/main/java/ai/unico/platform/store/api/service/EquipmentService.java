package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.EquipmentRegisterDto;
import ai.unico.platform.store.api.dto.MultilingualDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.List;

public interface EquipmentService {

    List<EquipmentRegisterDto> findAllEquipment(Integer limit, Integer offset, boolean includeDeleted);

    MultilingualDto<EquipmentRegisterDto> find(Long id);

    List<MultilingualDto<EquipmentRegisterDto>> findAllEquipmentMultilingual(Integer limit, Integer offset, boolean includeDeleted);

    List<EquipmentRegisterDto> findByOriginalIds(List<String> originalIds);

    List<Long> getAllOriginalIds();

    Long saveEquipment(MultilingualDto<EquipmentRegisterDto> equipmentRegisterDto, UserContext userContext) throws IOException;

    void updateEquipment(MultilingualDto<EquipmentRegisterDto> equipmentRegisterDto, UserContext userContext) throws IOException;

    void deleteEquipment(String equipmentCode, UserContext userContext) throws NonexistentEntityException, IOException;

    void toggleHidden(String equipmentCode, boolean hidden, UserContext userContext) throws IOException;

    Long registerEquipment(String originalCode, UserContext userContext) throws IOException;

}
