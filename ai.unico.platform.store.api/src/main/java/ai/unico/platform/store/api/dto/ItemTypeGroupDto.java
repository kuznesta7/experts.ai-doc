package ai.unico.platform.store.api.dto;

import java.util.HashSet;
import java.util.Set;

public class ItemTypeGroupDto {

    private Long itemTypeGroupId;

    private String itemTypeGroupTitle;

    private Set<Long> itemTypeIds = new HashSet<>();

    public Long getItemTypeGroupId() {
        return itemTypeGroupId;
    }

    public void setItemTypeGroupId(Long itemTypeGroupId) {
        this.itemTypeGroupId = itemTypeGroupId;
    }

    public String getItemTypeGroupTitle() {
        return itemTypeGroupTitle;
    }

    public void setItemTypeGroupTitle(String itemTypeGroupTitle) {
        this.itemTypeGroupTitle = itemTypeGroupTitle;
    }

    public Set<Long> getItemTypeIds() {
        return itemTypeIds;
    }

    public void setItemTypeIds(Set<Long> itemTypeIds) {
        this.itemTypeIds = itemTypeIds;
    }
}
