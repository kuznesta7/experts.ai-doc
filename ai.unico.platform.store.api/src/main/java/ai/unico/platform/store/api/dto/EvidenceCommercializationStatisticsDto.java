package ai.unico.platform.store.api.dto;

public class EvidenceCommercializationStatisticsDto {

    private Long numberOfAllProjects;

    public Long getNumberOfAllProjects() {
        return numberOfAllProjects;
    }

    public void setNumberOfAllProjects(Long numberOfAllProjects) {
        this.numberOfAllProjects = numberOfAllProjects;
    }

}
