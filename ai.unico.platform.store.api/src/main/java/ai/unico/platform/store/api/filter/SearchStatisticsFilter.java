package ai.unico.platform.store.api.filter;

public class SearchStatisticsFilter {


    private Integer limit = 30;

    private Integer page = 1;

    private String value;
    private boolean hidden;
    private boolean hotTopic;

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public boolean isHotTopic() {
        return hotTopic;
    }

    public void setHotTopic(boolean hotTopic) {
        this.hotTopic = hotTopic;
    }
}
