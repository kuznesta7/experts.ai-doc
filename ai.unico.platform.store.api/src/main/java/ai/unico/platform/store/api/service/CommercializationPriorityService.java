package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.CommercializationPriorityTypeDto;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface CommercializationPriorityService {

    List<CommercializationPriorityTypeDto> findAvailableCommercializationPriorityTypes(boolean includeInactive);

    Long createPriority(CommercializationPriorityTypeDto commercializationPriorityTypeDto, UserContext userContext);

    void updatePriority(Long priorityId, CommercializationPriorityTypeDto commercializationPriorityTypeDto, UserContext userContext);

}
