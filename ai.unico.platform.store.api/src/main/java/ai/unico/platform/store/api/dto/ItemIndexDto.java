package ai.unico.platform.store.api.dto;

import java.util.HashSet;
import java.util.Set;

public class ItemIndexDto extends ItemPreviewDto {

    private Set<Long> activeUserIds = new HashSet<>();

    private Set<Long> allUserIds = new HashSet<>();

    private Set<Long> favoriteUserIds = new HashSet<>();

    private Set<Long> activeExpertIds = new HashSet<>();

    private Set<Long> allExpertIds = new HashSet<>();

    private Set<Long> notRegisteredExpertIds = new HashSet<>();

    private Set<Long> organizationIds = new HashSet<>();

    private Set<Long> verifiedOrganizationIds = new HashSet<>();

    private Set<Long> activeOrganizationIds = new HashSet<>();

    public Set<Long> getActiveUserIds() {
        return activeUserIds;
    }

    public void setActiveUserIds(Set<Long> activeUserIds) {
        this.activeUserIds = activeUserIds;
    }

    public Set<Long> getActiveExpertIds() {
        return activeExpertIds;
    }

    public void setActiveExpertIds(Set<Long> activeExpertIds) {
        this.activeExpertIds = activeExpertIds;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<Long> getAllUserIds() {
        return allUserIds;
    }

    public void setAllUserIds(Set<Long> allUserIds) {
        this.allUserIds = allUserIds;
    }

    public Set<Long> getAllExpertIds() {
        return allExpertIds;
    }

    public void setAllExpertIds(Set<Long> allExpertIds) {
        this.allExpertIds = allExpertIds;
    }

    public Set<Long> getNotRegisteredExpertIds() {
        return notRegisteredExpertIds;
    }

    public void setNotRegisteredExpertIds(Set<Long> notRegisteredExpertIds) {
        this.notRegisteredExpertIds = notRegisteredExpertIds;
    }

    public Set<Long> getFavoriteUserIds() {
        return favoriteUserIds;
    }

    public void setFavoriteUserIds(Set<Long> favoriteUserIds) {
        this.favoriteUserIds = favoriteUserIds;
    }

    public Set<Long> getVerifiedOrganizationIds() {
        return verifiedOrganizationIds;
    }

    public void setVerifiedOrganizationIds(Set<Long> verifiedOrganizationIds) {
        this.verifiedOrganizationIds = verifiedOrganizationIds;
    }

    public Set<Long> getActiveOrganizationIds() {
        return activeOrganizationIds;
    }

    public void setActiveOrganizationIds(Set<Long> activeOrganizationIds) {
        this.activeOrganizationIds = activeOrganizationIds;
    }
}
