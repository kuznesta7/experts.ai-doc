package ai.unico.platform.store.api.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class OrganizationIndexDto {

    private Long organizationId;

    private Long originalOrganizationId;

    private String organizationName;

    private String organizationAbbrev;

    private Set<Long> childrenOrganizationIds;

    private Set<Long> affiliatedOrganizationIds;

    private Set<Long> memberOrganizationIds;

    private Long substituteOrganizationId;

    private Long rank;

    private String countryCode;

    private Boolean allowedSearch;

    private Boolean isSearchable;

    private Boolean allowedMembers;

    private Long organizationTypeId;

    private List<OrganizationLicencePreviewDto> organizationLicencePreviewDtos = new ArrayList<>();

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationAbbrev() {
        return organizationAbbrev;
    }

    public void setOrganizationAbbrev(String organizationAbbrev) {
        this.organizationAbbrev = organizationAbbrev;
    }

    public Long getSubstituteOrganizationId() {
        return substituteOrganizationId;
    }

    public void setSubstituteOrganizationId(Long substituteOrganizationId) {
        this.substituteOrganizationId = substituteOrganizationId;
    }

    public Long getOriginalOrganizationId() {
        return originalOrganizationId;
    }

    public void setOriginalOrganizationId(Long originalOrganizationId) {
        this.originalOrganizationId = originalOrganizationId;
    }

    public Long getRank() {
        return rank;
    }

    public void setRank(Long rank) {
        this.rank = rank;
    }

    public Boolean getAllowedMembers() {
        return allowedMembers;
    }

    public void setAllowedMembers(Boolean allowedMembers) {
        this.allowedMembers = allowedMembers;
    }

    public Set<Long> getMemberOrganizationIds() {
        return memberOrganizationIds;
    }

    public void setMemberOrganizationIds(Set<Long> memberOrganizationIds) {
        this.memberOrganizationIds = memberOrganizationIds;
    }

    public List<OrganizationLicencePreviewDto> getOrganizationLicencePreviewDtos() {
        return organizationLicencePreviewDtos;
    }

    public void setOrganizationLicencePreviewDtos(List<OrganizationLicencePreviewDto> organizationLicencePreviewDtos) {
        this.organizationLicencePreviewDtos = organizationLicencePreviewDtos;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Set<Long> getChildrenOrganizationIds() {
        return childrenOrganizationIds;
    }

    public void setChildrenOrganizationIds(Set<Long> childrenOrganizationIds) {
        this.childrenOrganizationIds = childrenOrganizationIds;
    }

    public Set<Long> getAffiliatedOrganizationIds() {
        return affiliatedOrganizationIds;
    }

    public void setAffiliatedOrganizationIds(Set<Long> affiliatedOrganizationIds) {
        this.affiliatedOrganizationIds = affiliatedOrganizationIds;
    }

    public Boolean getAllowedSearch() {
        return allowedSearch;
    }

    public void setAllowedSearch(Boolean allowedSearch) {
        this.allowedSearch = allowedSearch;
    }

    public Long getOrganizationTypeId() {
        return organizationTypeId;
    }

    public void setOrganizationTypeId(Long organizationTypeId) {
        this.organizationTypeId = organizationTypeId;
    }

    public Boolean getSearchable() {
        return isSearchable;
    }

    public void setSearchable(Boolean searchable) {
        isSearchable = searchable;
    }
}
