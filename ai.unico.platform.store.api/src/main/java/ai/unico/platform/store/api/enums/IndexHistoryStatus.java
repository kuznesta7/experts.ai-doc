package ai.unico.platform.store.api.enums;

public enum IndexHistoryStatus {

    STARTED, FINISHED, FAILED

}
