package ai.unico.platform.store.api.service;

import ai.unico.platform.util.model.UserContext;

public interface CommercializationTeamMemberService {

    void addTeamMemberUser(Long commercializationProjectId, Long userId, boolean teamLeader, UserContext userContext);

    void addTeamMemberExpert(Long commercializationProjectId, Long expertId, boolean teamLeader, UserContext userContext);

    void removeTeamMemberUser(Long commercializationProjectId, Long userId, UserContext userContext);

    void removeTeamMemberExpert(Long commercializationProjectId, Long expertId, UserContext userContext);

}
