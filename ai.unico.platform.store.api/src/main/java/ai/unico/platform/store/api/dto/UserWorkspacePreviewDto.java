package ai.unico.platform.store.api.dto;

public class UserWorkspacePreviewDto {

    private Long workspaceId;

    private Long userId;

    private String workspaceName;

    private String workspaceNote;

    public Long getWorkspaceId() {
        return workspaceId;
    }

    public void setWorkspaceId(Long workspaceId) {
        this.workspaceId = workspaceId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getWorkspaceName() {
        return workspaceName;
    }

    public void setWorkspaceName(String workspaceName) {
        this.workspaceName = workspaceName;
    }

    public String getWorkspaceNote() {
        return workspaceNote;
    }

    public void setWorkspaceNote(String workspaceNote) {
        this.workspaceNote = workspaceNote;
    }
}
