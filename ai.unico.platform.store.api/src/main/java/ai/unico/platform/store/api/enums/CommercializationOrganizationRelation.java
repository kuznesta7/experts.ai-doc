package ai.unico.platform.store.api.enums;

public enum CommercializationOrganizationRelation {

    INTELLECTUAL_PROPERTY_OWNER,
    INVESTOR,
    COMMERCIALIZATION_PARTNER

}

