package ai.unico.platform.store.api.dto;

public class SearchSuggestionDto {

    private String searchExpression;

    private Long appearance;

    private Long totalAppearance;

    public String getSearchExpression() {
        return searchExpression;
    }

    public void setSearchExpression(String searchExpression) {
        this.searchExpression = searchExpression;
    }

    public Long getAppearance() {
        return appearance;
    }

    public void setAppearance(Long appearance) {
        this.appearance = appearance;
    }

    public Long getTotalAppearance() {
        return totalAppearance;
    }

    public void setTotalAppearance(Long totalAppearance) {
        this.totalAppearance = totalAppearance;
    }
}
