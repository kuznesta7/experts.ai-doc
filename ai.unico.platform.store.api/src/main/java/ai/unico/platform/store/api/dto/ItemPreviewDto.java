package ai.unico.platform.store.api.dto;

import ai.unico.platform.util.enums.Confidentiality;

import java.util.ArrayList;
import java.util.List;

public class ItemPreviewDto {

    private Long itemId;

    private Long originalItemId;

    private String itemName;

    private String itemDescription;

    private Integer year;

    private Long itemTypeId;

    private List<String> keywords = new ArrayList<>();

    private Boolean hidden;

    private Boolean deleted;

    private Long ownerUserId;

    private Long ownerOrganizationId;

    private Confidentiality confidentiality;

    private Boolean translationUnavailable;

    private String doi;

    private String itemLink;

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Long getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(Long itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public Long getOriginalItemId() {
        return originalItemId;
    }

    public void setOriginalItemId(Long originalItemId) {
        this.originalItemId = originalItemId;
    }

    public Long getOwnerUserId() {
        return ownerUserId;
    }

    public void setOwnerUserId(Long ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    public Confidentiality getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Confidentiality confidentiality) {
        this.confidentiality = confidentiality;
    }

    public Long getOwnerOrganizationId() {
        return ownerOrganizationId;
    }

    public void setOwnerOrganizationId(Long ownerOrganizationId) {
        this.ownerOrganizationId = ownerOrganizationId;
    }

    public Boolean getTranslationUnavailable() {
        return translationUnavailable;
    }

    public void setTranslationUnavailable(Boolean translationUnavailable) {
        this.translationUnavailable = translationUnavailable;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getItemLink() {
        return itemLink;
    }

    public void setItemLink(String itemLink) {
        this.itemLink = itemLink;
    }
}
