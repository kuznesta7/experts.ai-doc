package ai.unico.platform.store.api.exception;

import ai.unico.platform.util.exception.RuntimeExceptionWithMessage;

public class MissingParameterException extends RuntimeExceptionWithMessage {
    public MissingParameterException(String s) {
        super("Missing field " + s);
    }
}
