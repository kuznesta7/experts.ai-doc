package ai.unico.platform.store.api.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserProfileDto {

    private Long userId;

    private String name;

    private String username;

    private String email;

    private String description;

    private String positionDescription;

    private List<OrganizationBaseDto> organizationDtos = new ArrayList<>();

    private String city;

    private List<String> keywords = new ArrayList<>();

    private Set<Long> expertIds = new HashSet<>();

    private List<ItemPreviewDto> itemPreviewDtos = new ArrayList<>();

    private boolean claimable = false;

    private boolean invitation = false;

    private List<ItemTypeGroupWithCountsDto> itemTypeGroupsWithCount = new ArrayList<>();

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPositionDescription() {
        return positionDescription;
    }

    public void setPositionDescription(String positionDescription) {
        this.positionDescription = positionDescription;
    }

    public List<OrganizationBaseDto> getOrganizationDtos() {
        return organizationDtos;
    }

    public void setOrganizationDtos(List<OrganizationBaseDto> organizationDtos) {
        this.organizationDtos = organizationDtos;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Set<Long> getExpertIds() {
        return expertIds;
    }

    public void setExpertIds(Set<Long> expertIds) {
        this.expertIds = expertIds;
    }

    public List<ItemPreviewDto> getItemPreviewDtos() {
        return itemPreviewDtos;
    }

    public void setItemPreviewDtos(List<ItemPreviewDto> itemPreviewDtos) {
        this.itemPreviewDtos = itemPreviewDtos;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<ItemTypeGroupWithCountsDto> getItemTypeGroupsWithCount() {
        return itemTypeGroupsWithCount;
    }

    public void setItemTypeGroupsWithCount(List<ItemTypeGroupWithCountsDto> itemTypeGroupsWithCount) {
        this.itemTypeGroupsWithCount = itemTypeGroupsWithCount;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public boolean isClaimable() {
        return claimable;
    }

    public void setClaimable(boolean claimable) {
        this.claimable = claimable;
    }

    public boolean isInvitation() {
        return invitation;
    }

    public void setInvitation(boolean invitation) {
        this.invitation = invitation;
    }

    public static class ItemTypeGroupWithCountsDto {

        private Long itemTypeGroupId;

        private Set<Long> itemTypeIds = new HashSet<>();

        private String itemTypeGroupTitle;

        private Integer count = 0;

        public Long getItemTypeGroupId() {
            return itemTypeGroupId;
        }

        public void setItemTypeGroupId(Long itemTypeGroupId) {
            this.itemTypeGroupId = itemTypeGroupId;
        }

        public Set<Long> getItemTypeIds() {
            return itemTypeIds;
        }

        public void setItemTypeIds(Set<Long> itemTypeIds) {
            this.itemTypeIds = itemTypeIds;
        }

        public String getItemTypeGroupTitle() {
            return itemTypeGroupTitle;
        }

        public void setItemTypeGroupTitle(String itemTypeGroupTitle) {
            this.itemTypeGroupTitle = itemTypeGroupTitle;
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }
    }
}
