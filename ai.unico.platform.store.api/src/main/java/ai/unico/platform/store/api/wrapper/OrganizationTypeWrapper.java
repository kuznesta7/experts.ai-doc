package ai.unico.platform.store.api.wrapper;

import ai.unico.platform.store.api.dto.OrganizationTypeDto;

import java.util.ArrayList;
import java.util.List;

public class OrganizationTypeWrapper {

    private List<OrganizationTypeDto> organizationTypeDtos = new ArrayList<>();

    private OrganizationTypeDto selected;

    public List<OrganizationTypeDto> getOrganizationTypeDtos() {
        return organizationTypeDtos;
    }

    public void setOrganizationTypeDtos(List<OrganizationTypeDto> organizationTypeDtos) {
        this.organizationTypeDtos = organizationTypeDtos;
    }

    public OrganizationTypeDto getSelected() {
        return selected;
    }

    public void setSelected(OrganizationTypeDto selected) {
        this.selected = selected;
    }
}
