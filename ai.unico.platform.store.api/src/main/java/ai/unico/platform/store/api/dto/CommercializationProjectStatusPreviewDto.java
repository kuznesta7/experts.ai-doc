package ai.unico.platform.store.api.dto;

import java.util.Date;

public class CommercializationProjectStatusPreviewDto {

    private Long statusTypeId;

    private String statusName;

    private Long userUpdateId;

    private Date dateUpdate;

    private boolean active;

    private boolean mustBeFilled;

    private long statusOrder;

    public long getStatusOrder() {
        return statusOrder;
    }

    public void setStatusOrder(long statusOrder) {
        this.statusOrder = statusOrder;
    }


    public boolean isMustBeFilled() {
        return mustBeFilled;
    }

    public void setMustBeFilled(boolean mustBeFilled) {
        this.mustBeFilled = mustBeFilled;
    }

    public Long getStatusTypeId() {
        return statusTypeId;
    }

    public void setStatusTypeId(Long statusTypeId) {
        this.statusTypeId = statusTypeId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Long getUserUpdateId() {
        return userUpdateId;
    }

    public void setUserUpdateId(Long userUpdateId) {
        this.userUpdateId = userUpdateId;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
