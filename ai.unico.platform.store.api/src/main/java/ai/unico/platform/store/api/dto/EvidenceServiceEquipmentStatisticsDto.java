package ai.unico.platform.store.api.dto;

public class EvidenceServiceEquipmentStatisticsDto {

    private Long numberOfAllServices;

    public Long getNumberOfAllServices() {
        return numberOfAllServices;
    }

    public void setNumberOfAllServices(Long numberOfAllServices) {
        this.numberOfAllServices = numberOfAllServices;
    }

}
