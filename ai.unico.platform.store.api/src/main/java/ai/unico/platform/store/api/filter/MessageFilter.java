package ai.unico.platform.store.api.filter;

public class MessageFilter {

    private String query;

    private String userFrom;

    private String userTo;

    private Integer limit;

    private Integer page;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(String userFrom) {
        this.userFrom = userFrom;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getUserTo() {
        return userTo;
    }

    public void setUserTo(String userTo) {
        this.userTo = userTo;
    }

    public MessageFilter(String query, String userFrom, String userTo, Integer limit, Integer page) {
        this.query = query;
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.limit = limit;
        this.page = page;
    }
}
