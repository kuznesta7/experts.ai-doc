package ai.unico.platform.store.api.dto;

public class OrganizationSumFinancesDto extends OrganizationBaseDto {
    Double amount;

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
