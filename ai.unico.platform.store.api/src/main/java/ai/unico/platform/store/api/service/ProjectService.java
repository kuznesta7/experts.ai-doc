package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.PrivateProjectDto;
import ai.unico.platform.store.api.dto.ProjectDto;
import ai.unico.platform.store.api.dto.ProjectIndexDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface ProjectService {

    ProjectDto findProject(Long projectId) throws NonexistentEntityException;

    PrivateProjectDto findPrivateProject(Long projectId, Long organizationId) throws NonexistentEntityException, IOException;

    Long createProject(ProjectDto projectDto, UserContext userContext);

    void updateProject(ProjectDto projectDto, UserContext userContext) throws NonexistentEntityException;

    void deleteProject(Long projectId, UserContext userContext) throws NonexistentEntityException, IOException;

    List<ProjectIndexDto> findAllProjects(Integer limit, Integer offset);

    void loadAndIndexProject(Long projectId) throws NonexistentEntityException;

    void loadAndIndexProjects(Set<Long> projectToReindexIds);
}
