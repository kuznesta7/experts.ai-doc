package ai.unico.platform.store.api.service;

import java.util.Map;

public interface PlatformParamService {

    String DEMO_SEARCH_QUERY = "DEMO_SEARCH_QUERY";
    String DEMO_SEARCH_RESULT_USER_IDS = "DEMO_SEARCH_RESULT_USER_IDS";
    String DEMO_ORGANIZATION_ID = "DEMO_ORGANIZATION_ID";
    String DEMO_USER_ID = "DEMO_USER_ID";

    Map<String, String> findPlatformParams();

    String findPlatformParamValue(String paramKey);

}
