package ai.unico.platform.store.api.service;

import ai.unico.platform.util.model.UserContext;

public interface ProfileVisitationService {

    void saveUserProfileVisitation(UserContext userContext, Long userId, Long searchId);

    void saveExpertProfileVisitation(UserContext userContext, Long expertId, Long searchId);
}
