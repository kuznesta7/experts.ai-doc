package ai.unico.platform.store.api.dto;

public class EvidenceOpportunityStatisticsDto {

    private Long numberOfAllOpportunities;

    public Long getNumberOfAllOpportunities() {
        return numberOfAllOpportunities;
    }

    public void setNumberOfAllOpportunities(Long numberOfAllOpportunities) {
        this.numberOfAllOpportunities = numberOfAllOpportunities;
    }
}
