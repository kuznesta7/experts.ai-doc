package ai.unico.platform.store.api.dto;

import java.util.HashSet;
import java.util.Set;

public class EquipmentRegisterDto {
    private Long id;
    private String languageCode;
    private String equipmentCode;
    private String name;
    private String nameEn;
    private String marking;
    private String description;
    private String descriptionEn;
    private Set<String> specialization = new HashSet<>();
    private Integer year;
    private Integer serviceLife;
    private Boolean portableDevice;
    private Set<String> expertCodes = new HashSet<>();
    private Set<Long> organizationIds = new HashSet<>();
    private Long type;
    private Boolean hidden = false;
    private String goodForDescription;
    private String targetGroup;
    private String preparationTime;
    private String equipmentCost;
    private String infrastructureDescription;
    private String durationTime;
    private String trl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getMarking() {
        return marking;
    }

    public void setMarking(String marking) {
        this.marking = marking;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public Set<String> getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Set<String> specialization) {
        this.specialization = specialization;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getServiceLife() {
        return serviceLife;
    }

    public void setServiceLife(Integer serviceLife) {
        this.serviceLife = serviceLife;
    }

    public Boolean getPortableDevice() {
        return portableDevice;
    }

    public void setPortableDevice(Boolean portableDevice) {
        this.portableDevice = portableDevice;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<String> getExpertCodes() {
        return expertCodes;
    }

    public void setExpertCodes(Set<String> expertCodes) {
        this.expertCodes = expertCodes;
    }

    public Boolean isHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getEquipmentCode() {
        return equipmentCode;
    }

    public void setEquipmentCode(String equipmentCode) {
        this.equipmentCode = equipmentCode;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public String getGoodForDescription() {
        return goodForDescription;
    }

    public void setGoodForDescription(String goodForDescription) {
        this.goodForDescription = goodForDescription;
    }

    public String getTargetGroup() {
        return targetGroup;
    }

    public void setTargetGroup(String targetGroup) {
        this.targetGroup = targetGroup;
    }

    public String getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(String preparationTime) {
        this.preparationTime = preparationTime;
    }

    public String getEquipmentCost() {
        return equipmentCost;
    }

    public void setEquipmentCost(String equipmentCost) {
        this.equipmentCost = equipmentCost;
    }

    public String getInfrastructureDescription() {
        return infrastructureDescription;
    }

    public void setInfrastructureDescription(String infrastructureDescription) {
        this.infrastructureDescription = infrastructureDescription;
    }

    public String getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(String durationTime) {
        this.durationTime = durationTime;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getTrl() {
        return trl;
    }

    public void setTrl(String trl) {
        this.trl = trl;
    }
}
