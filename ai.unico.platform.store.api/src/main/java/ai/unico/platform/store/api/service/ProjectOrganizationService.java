package ai.unico.platform.store.api.service;

import ai.unico.platform.util.enums.ProjectOrganizationRole;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

import java.util.Set;

public interface ProjectOrganizationService {

    void addProjectOrganization(Long projectId, Long organizationId, boolean verify, ProjectOrganizationRole role, UserContext userContext) throws NonexistentEntityException;

    void removeProjectOrganization(Long projectId, Long organizationId, UserContext userContext) throws NonexistentEntityException;

    void verifyOrganizationProjects(Set<Long> projectId, Long organizationIds, boolean verify, UserContext userContext);

    void removeOrganizationProjects(Set<Long> projectIds, Long organizationId, UserContext userContext);

    void switchOrganizations(Long projectId, Set<Long> organizationIds, UserContext userContext);
}
