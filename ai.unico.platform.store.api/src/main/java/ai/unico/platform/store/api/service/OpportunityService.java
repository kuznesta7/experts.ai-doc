package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.OpportunityRegisterDto;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.List;

public interface OpportunityService {

    List<OpportunityRegisterDto> findOpportunities(Integer limit, Integer offset, boolean ignoreDeleted);

    OpportunityRegisterDto getOpportunity(Long opportunityId);

    Long createOpportunity(OpportunityRegisterDto registerDto, UserContext userContext) throws IOException;

    void updateOpportunity(OpportunityRegisterDto registerDto, String opportunityCode, UserContext userContext) throws IOException;

    void deleteOpportunity(String opportunityCode, UserContext userContext) throws IOException;

    void toggleHiddenOpportunity(String opportunityCode, Boolean hide, UserContext userContext) throws IOException;
}
