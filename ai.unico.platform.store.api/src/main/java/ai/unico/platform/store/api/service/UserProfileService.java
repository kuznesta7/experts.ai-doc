package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.UserProfileDto;
import ai.unico.platform.store.api.dto.UserProfileIndexDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

import java.util.Collection;
import java.util.List;

public interface UserProfileService {

    UserProfileDto loadProfile(Long userId) throws NonexistentEntityException;

    Long createProfile(UserProfileDto userProfileDto, UserContext userContext);

    void updateProfile(UserProfileDto userProfile, UserContext userContext) throws NonexistentEntityException;

    void deleteProfile(Long userId, UserContext userContext) throws NonexistentEntityException;

    void loadAndIndexUserProfile(Long userId);

    void loadAndIndexUserProfiles(Collection<Long> userIds);

    List<UserProfileIndexDto> findUserProfiles(Integer limit, Integer offset);

}
