package ai.unico.platform.store.api.dto;

import java.util.ArrayList;
import java.util.List;

public class EvidenceSearchedKeywordStatisticsDto {

    private List<SearchedKeywordDto> mostSearchedKeywords = new ArrayList<>();

    private Long numberOfSearchedKeywords;

    private Integer page;

    private Integer limit;

    public List<SearchedKeywordDto> getMostSearchedKeywords() {
        return mostSearchedKeywords;
    }

    public void setMostSearchedKeywords(List<SearchedKeywordDto> mostSearchedKeywords) {
        this.mostSearchedKeywords = mostSearchedKeywords;
    }

    public Long getNumberOfSearchedKeywords() {
        return numberOfSearchedKeywords;
    }

    public void setNumberOfSearchedKeywords(Long numberOfSearchedKeywords) {
        this.numberOfSearchedKeywords = numberOfSearchedKeywords;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public static class SearchedKeywordDto {

        private String query;

        private Long searchCount;

        private Long numberOfExpertsShown;

        private Double avgPosition;

        private Integer bestPosition;

        public String getQuery() {
            return query;
        }

        public void setQuery(String query) {
            this.query = query;
        }

        public Long getSearchCount() {
            return searchCount;
        }

        public void setSearchCount(Long searchCount) {
            this.searchCount = searchCount;
        }

        public Long getNumberOfExpertsShown() {
            return numberOfExpertsShown;
        }

        public void setNumberOfExpertsShown(Long numberOfExpertsShown) {
            this.numberOfExpertsShown = numberOfExpertsShown;
        }

        public Double getAvgPosition() {
            return avgPosition;
        }

        public void setAvgPosition(Double avgPosition) {
            this.avgPosition = avgPosition;
        }


        public Integer getBestPosition() {
            return bestPosition;
        }

        public void setBestPosition(Integer bestPosition) {
            this.bestPosition = bestPosition;
        }
    }

}
