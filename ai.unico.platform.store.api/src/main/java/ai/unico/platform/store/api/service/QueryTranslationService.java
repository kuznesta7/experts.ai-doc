package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.QueryTranslationDto;

import java.util.List;

public interface QueryTranslationService {

    QueryTranslationDto translate(String query);

    Long createTranslation(QueryTranslationDto translationDto);

    void updateTranslation(QueryTranslationDto translationDto);

    List<QueryTranslationDto> findAllTranslations();

    void deleteTranslation(Long translationId);
}
