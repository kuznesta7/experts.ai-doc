package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.SearchStatisticsDto;
import ai.unico.platform.store.api.filter.SearchStatisticsFilter;
import ai.unico.platform.store.api.wrapper.SearchStatisticsWrapper;

import java.util.List;

public interface SearchStatisticsService {

    SearchStatisticsWrapper findSearchedData(SearchStatisticsFilter filter);

    List<SearchStatisticsDto> findHotTopics();

    void updateSearchedData(SearchStatisticsDto statisticsDto);

    Long addSearchedData(SearchStatisticsDto statisticsDto);
}
