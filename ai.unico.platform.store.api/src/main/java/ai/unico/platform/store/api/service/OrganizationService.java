package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.*;
import ai.unico.platform.store.api.wrapper.OrganizationTypeWrapper;
import ai.unico.platform.util.dto.OrganizationDto;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface OrganizationService {

    List<OrganizationIndexDto> findAllOrganizations();

    List<OrganizationDto> findOrganizationsByIds(Set<Long> organizationIds);

    OrganizationDetailDto findOrganizationDetail(Long organizationId);

    void updateOrganizationsFromDWH();

    void updateOrganization(AdminOrganizationUpdateDto organizationDto, UserContext userContext) throws IOException;

    void restoreOrganization(Long organizationId, UserContext userContext);

    Long createOrganization(AdminOrganizationUpdateDto organizationDto, UserContext userContext);

    Map<Long, Long> findOrganizationIdsMap(Set<Long> originalOrganizationIds);

    void replaceOrganization(Long organizationId, Long substituteOrganizationId, UserContext userContext) throws IOException;

    void loadAndIndexOrganization(Long organizationId);

    void toggleSearchable(Long organizationId, boolean allow);

    void toggleMembers(Long organizationId, boolean allow);

    void changeOrganizationType(Long organizationId, Long organizationType);

    Boolean hasPublicProfile(Long organizationId);

    Boolean hasGraphVisible(Long organizationId);

    Boolean hasExpertsVisible(Long organizationId);

    Boolean hasOutcomesVisible(Long organizationId);

    Boolean hasProjectsVisible(Long organizationId);

    Boolean hasKeywordsVisible(Long organizationId);

    Boolean hasGdpr(Long organizationId);

    PublicProfileVisibilityDto getProfileVisibility(Long organizationId);

    void setProfileVisibility(PublicProfileVisibilityDto profileVisibilityDto);

    void saveProfile(Long organizationId, OrganizationDetailDto profileDto, UserContext userContext);

    OrganizationTypeWrapper getOrganizationTypes(Long organizationId);

    RecombeeConnectionDto getRecombeeConnection(Long organizationId);

    List<RecombeeConnectionDto> getRecombeeConnections(Set<Long> organizationIds);

    List<OrganizationDetailDto> getAllRecombeeConnections();

    List<OrganizationTypeDto> getOrganizationsTypes();
}
