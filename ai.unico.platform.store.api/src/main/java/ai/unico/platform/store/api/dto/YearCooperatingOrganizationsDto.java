package ai.unico.platform.store.api.dto;

import java.util.ArrayList;
import java.util.List;

public class YearCooperatingOrganizationsDto {
    private Integer year;
    private Double others;

    private List<OrganizationSumFinancesDto> organizations = new ArrayList<>();

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Double getOthers() {
        return others;
    }

    public void setOthers(Double others) {
        this.others = others;
    }

    public List<OrganizationSumFinancesDto> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(List<OrganizationSumFinancesDto> organizations) {
        this.organizations = organizations;
    }
}
