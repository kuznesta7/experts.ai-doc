package ai.unico.platform.store.api.dto;

public class CountryDto {

    private String countryCode;

    private String countryName;

    private String countryAbbrev;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryAbbrev() {
        return countryAbbrev;
    }

    public void setCountryAbbrev(String countryAbbrev) {
        this.countryAbbrev = countryAbbrev;
    }
}
