package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.CommercializationProjectStatusPreviewDto;
import ai.unico.platform.store.api.dto.CommercializationStatusTypeDto;
import ai.unico.platform.util.model.UserContext;

import java.util.List;
import java.util.Set;

public interface CommercializationStatusService {

    Long createStatus(CommercializationStatusTypeDto commercializationStatusTypeDto, UserContext userContext);

    void updateStatus(Long statusId, CommercializationStatusTypeDto commercializationStatusTypeDto, UserContext userContext);

    void setStatus(Long commercializationProjectId, Long statusTypeId, UserContext userContext);

    List<CommercializationStatusTypeDto> findAvailableStatuses(boolean includeInactive);

    List<CommercializationProjectStatusPreviewDto> findProjectStatusHistory(Long commercializationProjectId);

    Set<Long> findStatusIdsForInvest();

    Set<Long> findStatusIdsForSearch();
}
