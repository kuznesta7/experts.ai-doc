package ai.unico.platform.store.api.service.email;

import java.util.Map;

public interface EmailService {

    void sendEmail(String to, String subject, String body, String replyTo);

    void sendEmailWithTemplate(String to, String templateId, Map<String, String> variables);

}