package ai.unico.platform.store.api.service;


import ai.unico.platform.store.api.dto.ConsentDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface UserConsentService {

    List<ConsentDto> findAllConsents();

    List<ConsentDto> findUserConsents(Long userId);

    Boolean hasUserConsent(Long userId, Long consentId);

    void addUserConsent(Long userId, Long consentId, UserContext userContext) throws NonexistentEntityException;

    void updateUserConsents(Long userId, List<ConsentDto> consentDtos, UserContext userContext);
}
