package ai.unico.platform.store.api.dto;

import java.util.Date;

public class ExpertIndexDto {

    private Long expertId;

    private Date retirementDate;

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public Date getRetirementDate() {
        return retirementDate;
    }

    public void setRetirementDate(Date retirementDate) {
        this.retirementDate = retirementDate;
    }
}
