package ai.unico.platform.store.api.dto;

import java.util.ArrayList;
import java.util.List;

public class PrivateProjectDto {

    private Long projectId;

    private Long organizationId;

    private ProjectPreviewDto parentProjectPreview;

    private List<ProjectPreviewDto> subProjectPreviews = new ArrayList<>();

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public ProjectPreviewDto getParentProjectPreview() {
        return parentProjectPreview;
    }

    public void setParentProjectPreview(ProjectPreviewDto parentProjectPreview) {
        this.parentProjectPreview = parentProjectPreview;
    }

    public List<ProjectPreviewDto> getSubProjectPreviews() {
        return subProjectPreviews;
    }

    public void setSubProjectPreviews(List<ProjectPreviewDto> subProjectPreviews) {
        this.subProjectPreviews = subProjectPreviews;
    }

}
