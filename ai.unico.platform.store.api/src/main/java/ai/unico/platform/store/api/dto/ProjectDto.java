package ai.unico.platform.store.api.dto;

import ai.unico.platform.util.enums.Confidentiality;

import java.util.*;

public class ProjectDto {

    private Long projectId;
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;
    private Long projectProgramId;
    private List<String> keywords;
    private Confidentiality confidentiality;
    private Long projectOwnerOrganizationId;
    private String projectNumber;
    private Map<Long, Map<Integer, Double>> organizationFinanceMap = new HashMap<>();
    private Set<Long> userIds;
    private Set<Long> expertIds;
    private Set<Long> organizationIds;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Long getProjectProgramId() {
        return projectProgramId;
    }

    public void setProjectProgramId(Long projectProgramId) {
        this.projectProgramId = projectProgramId;
    }

    public Confidentiality getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Confidentiality confidentiality) {
        this.confidentiality = confidentiality;
    }

    public Long getProjectOwnerOrganizationId() {
        return projectOwnerOrganizationId;
    }

    public void setProjectOwnerOrganizationId(Long projectOwnerOrganizationId) {
        this.projectOwnerOrganizationId = projectOwnerOrganizationId;
    }

    public Map<Long, Map<Integer, Double>> getOrganizationFinanceMap() {
        return organizationFinanceMap;
    }

    public void setOrganizationFinanceMap(Map<Long, Map<Integer, Double>> organizationFinanceMap) {
        this.organizationFinanceMap = organizationFinanceMap;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public Set<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }

    public Set<Long> getExpertIds() {
        return expertIds;
    }

    public void setExpertIds(Set<Long> expertIds) {
        this.expertIds = expertIds;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }
}
