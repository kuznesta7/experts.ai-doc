package ai.unico.platform.store.api.dto;

import java.util.*;

public class CommercializationProjectDto {

    private Long commercializationProjectId;

    private String name;

    private String executiveSummary;

    private String useCase;

    private String painDescription;

    private String competitiveAdvantage;

    private String technicalPrinciples;

    private Integer technologyReadinessLevel;

    private Date startDate;

    private Date endDate;

    private Date statusDeadline;

    private boolean deleted;

    private Integer commercializationInvestmentFrom;

    private Integer commercializationInvestmentTo;

    private List<CommercializationCategoryPreviewDto> commercializationCategoryPreview = new ArrayList<>();

    private Set<Long> commercializationCategoryIds = new HashSet<>();

    private Long commercializationPriorityId;

    private Long commercializationDomainId;

    private Integer ideaScore;

    private List<String> keywords = new ArrayList<>();

    private Long ownerOrganizationId;

    private String link;

    private Long commercializationStatusId;

    public Long getCommercializationProjectId() {
        return commercializationProjectId;
    }

    public void setCommercializationProjectId(Long commercializationProjectId) {
        this.commercializationProjectId = commercializationProjectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExecutiveSummary() {
        return executiveSummary;
    }

    public void setExecutiveSummary(String executiveSummary) {
        this.executiveSummary = executiveSummary;
    }

    public String getUseCase() {
        return useCase;
    }

    public void setUseCase(String useCase) {
        this.useCase = useCase;
    }

    public String getPainDescription() {
        return painDescription;
    }

    public void setPainDescription(String painDescription) {
        this.painDescription = painDescription;
    }

    public String getCompetitiveAdvantage() {
        return competitiveAdvantage;
    }

    public void setCompetitiveAdvantage(String competitiveAdvantage) {
        this.competitiveAdvantage = competitiveAdvantage;
    }

    public String getTechnicalPrinciples() {
        return technicalPrinciples;
    }

    public void setTechnicalPrinciples(String technicalPrinciples) {
        this.technicalPrinciples = technicalPrinciples;
    }

    public Integer getTechnologyReadinessLevel() {
        return technologyReadinessLevel;
    }

    public void setTechnologyReadinessLevel(Integer technologyReadinessLevel) {
        this.technologyReadinessLevel = technologyReadinessLevel;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStatusDeadline() {
        return statusDeadline;
    }

    public void setStatusDeadline(Date statusDeadline) {
        this.statusDeadline = statusDeadline;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getCommercializationInvestmentFrom() {
        return commercializationInvestmentFrom;
    }

    public void setCommercializationInvestmentFrom(Integer commercializationInvestmentFrom) {
        this.commercializationInvestmentFrom = commercializationInvestmentFrom;
    }

    public Integer getCommercializationInvestmentTo() {
        return commercializationInvestmentTo;
    }

    public void setCommercializationInvestmentTo(Integer commercializationInvestmentTo) {
        this.commercializationInvestmentTo = commercializationInvestmentTo;
    }

    public void setCommercializationCategoryPreview(List<CommercializationCategoryPreviewDto> commercializationCategoryPreview) {
        this.commercializationCategoryPreview = commercializationCategoryPreview;
    }

    public List<CommercializationCategoryPreviewDto> getCommercializationCategoryPreview() {
        return commercializationCategoryPreview;
    }

    public Long getCommercializationPriorityId() {
        return commercializationPriorityId;
    }

    public void setCommercializationPriorityId(Long commercializationPriorityId) {
        this.commercializationPriorityId = commercializationPriorityId;
    }

    public Long getCommercializationDomainId() {
        return commercializationDomainId;
    }

    public void setCommercializationDomainId(Long commercializationDomainId) {
        this.commercializationDomainId = commercializationDomainId;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Integer getIdeaScore() {
        return ideaScore;
    }

    public void setIdeaScore(Integer ideaScore) {
        this.ideaScore = ideaScore;
    }

    public Long getOwnerOrganizationId() {
        return ownerOrganizationId;
    }

    public void setOwnerOrganizationId(Long ownerOrganizationId) {
        this.ownerOrganizationId = ownerOrganizationId;
    }

    public Set<Long> getCommercializationCategoryIds() {
        return commercializationCategoryIds;
    }

    public void setCommercializationCategoryIds(Set<Long> commercializationCategoryIds) {
        this.commercializationCategoryIds = commercializationCategoryIds;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Long getCommercializationStatusId() {
        return commercializationStatusId;
    }

    public void setCommercializationStatusId(Long commercializationStatusId) {
        this.commercializationStatusId = commercializationStatusId;
    }
}
