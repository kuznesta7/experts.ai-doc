package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.CategoryDto;

import java.util.List;
import java.util.Map;

public interface OpportunityCategoryService {

    void updateOpportunityCategoriesFromDWH();

    List<CategoryDto> getOpportunityCategories();

    CategoryDto getOpportunityCategory(Long id);

    Map<Long, Long> translateOpportunities();
}
