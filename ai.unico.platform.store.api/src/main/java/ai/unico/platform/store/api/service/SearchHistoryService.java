package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ExpertSearchHistoryDto;
import ai.unico.platform.store.api.dto.ExpertSearchHistoryPreviewDto;
import ai.unico.platform.store.api.filter.ExpertSearchHistoryFilter;
import ai.unico.platform.store.api.wrapper.ExpertSearchHistoryWrapper;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;

import java.util.Set;

public interface SearchHistoryService {

    ExpertSearchHistoryPreviewDto findSavedSearch(ExpertSearchHistoryDto expertSearchHistoryDto, UserContext userContext);

    Long saveSearchHistory(ExpertSearchHistoryDto expertSearchHistoryDto, UserContext userContext);

    ExpertSearchHistoryPreviewDto findSearch(Long searchHistoryId, String shareCode, UserContext userContext) throws UserUnauthorizedException;

    ExpertSearchHistoryWrapper findSearchHistoryForUser(Long userId, ExpertSearchHistoryFilter expertSearchHistoryFilter);

    void saveFavoriteSearch(Long searchHistId, boolean favorite, Set<String> workspaces, UserContext userContext);

    void deleteSearchHistory(Long searchHistId, UserContext userContext);

}
