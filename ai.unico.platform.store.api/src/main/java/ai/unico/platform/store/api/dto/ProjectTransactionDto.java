package ai.unico.platform.store.api.dto;


import ai.unico.platform.store.api.enums.Currency;

import java.util.Date;

public class ProjectTransactionDto {

    private String transactionNote;

    private Double amount;

    private Currency currency;

    private Long organizationFromId;

    private Long organizationToId;

    private Date transactionDate;

    public String getTransactionNote() {
        return transactionNote;
    }

    public void setTransactionNote(String transactionNote) {
        this.transactionNote = transactionNote;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Long getOrganizationFromId() {
        return organizationFromId;
    }

    public void setOrganizationFromId(Long organizationFromId) {
        this.organizationFromId = organizationFromId;
    }

    public Long getOrganizationToId() {
        return organizationToId;
    }

    public void setOrganizationToId(Long organizationToId) {
        this.organizationToId = organizationToId;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }
}
