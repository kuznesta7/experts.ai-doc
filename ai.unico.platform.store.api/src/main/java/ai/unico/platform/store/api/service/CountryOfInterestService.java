package ai.unico.platform.store.api.service;

import ai.unico.platform.util.model.UserContext;

import java.util.Set;

public interface CountryOfInterestService {

    Set<String> findCountryOfInterestCodesForUser(Long userId);

    void updateCountriesOfInterestForUser(Long userId, Set<String> countryCodes, UserContext userContext);
}
