package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ProjectExpertDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

public interface ProjectExpertService {

    void saveExpertToProject(ProjectExpertDto projectExpertDto, UserContext userContext) throws NonexistentEntityException;

    void removeExpertFromProject(ProjectExpertDto projectExpertDto, UserContext userContext) throws NonexistentEntityException;

}
