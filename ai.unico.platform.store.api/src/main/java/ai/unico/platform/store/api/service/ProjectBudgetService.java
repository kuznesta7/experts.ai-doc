package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ProjectBudgetDto;
import ai.unico.platform.util.model.UserContext;

public interface ProjectBudgetService {

    void updateProjectBudget(Long projectId, Long organizationId, Integer year, ProjectBudgetDto projectBudgetDto, UserContext userContext);

    void deleteProjectBudget(Long projectId, Long organizationId, Integer year, UserContext userContext);

}
