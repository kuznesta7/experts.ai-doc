package ai.unico.platform.store.api.service;

import java.util.List;

public interface StudentService {

    List<String> getStudentRecommendations(String usernameHash);

    String getStudentTestGroup(String usernameHash);

}
