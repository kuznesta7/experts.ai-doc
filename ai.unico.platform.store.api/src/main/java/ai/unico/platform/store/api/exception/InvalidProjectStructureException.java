package ai.unico.platform.store.api.exception;

import ai.unico.platform.util.exception.RuntimeExceptionWithMessage;

public class InvalidProjectStructureException extends RuntimeExceptionWithMessage {

    public InvalidProjectStructureException(String secretMessage) {
        super("INVALID_PROJECT_STRUCTURE", secretMessage);
    }
}
