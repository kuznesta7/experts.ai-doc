package ai.unico.platform.store.api.dto;

public class PreClaimDto {

    private Long userId;

    private String userEmail;

    private Long userToClaimId;

    private Long expertToClaimId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Long getUserToClaimId() {
        return userToClaimId;
    }

    public void setUserToClaimId(Long userToClaimId) {
        this.userToClaimId = userToClaimId;
    }

    public Long getExpertToClaimId() {
        return expertToClaimId;
    }

    public void setExpertToClaimId(Long expertToClaimId) {
        this.expertToClaimId = expertToClaimId;
    }
}
