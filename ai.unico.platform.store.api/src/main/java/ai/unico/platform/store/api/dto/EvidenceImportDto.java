package ai.unico.platform.store.api.dto;

import ai.unico.platform.store.api.enums.EvidenceCategory;

import java.util.Date;

public class EvidenceImportDto {

    private Long evidenceImportId;

    private Long organizationId;

    private String organizationName;

    private EvidenceCategory evidenceCategory;

    private String note;

    private String fileName;

    private Integer fileSize;

    private Long userImportedId;

    private String userImportedName;

    private String email;

    private Date fileUploadedDate;

    private boolean deleted;

    public Long getEvidenceImportId() {
        return evidenceImportId;
    }

    public void setEvidenceImportId(Long evidenceImportId) {
        this.evidenceImportId = evidenceImportId;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public EvidenceCategory getEvidenceCategory() {
        return evidenceCategory;
    }

    public void setEvidenceCategory(EvidenceCategory evidenceCategory) {
        this.evidenceCategory = evidenceCategory;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Long getUserImportedId() {
        return userImportedId;
    }

    public void setUserImportedId(Long userImportedId) {
        this.userImportedId = userImportedId;
    }

    public String getUserImportedName() {
        return userImportedName;
    }

    public void setUserImportedName(String userImportedName) {
        this.userImportedName = userImportedName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getFileUploadedDate() {
        return fileUploadedDate;
    }

    public void setFileUploadedDate(Date fileUploadedDate) {
        this.fileUploadedDate = fileUploadedDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
