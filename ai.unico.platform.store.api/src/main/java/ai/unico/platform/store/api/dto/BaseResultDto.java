package ai.unico.platform.store.api.dto;


import ai.unico.platform.util.enums.ResultCode;

public class BaseResultDto<T> {

    private ResultCode resultCode;
    private String resultMessage;
    private T data;

    public BaseResultDto(ResultCode resultCode, String resultMessage) {
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
    }

    public BaseResultDto(ResultCode resultCode) {
        this.resultCode = resultCode;
    }

    public BaseResultDto(T data) {
        this.resultCode = ResultCode.OK;
        this.data = data;
    }

    public ResultCode getResultCode() {
        return resultCode;
    }

    public void setResultCode(ResultCode resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
