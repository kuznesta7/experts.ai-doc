package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.OrganizationStructureDto;
import ai.unico.platform.store.api.enums.OrganizationRelationType;
import ai.unico.platform.util.dto.OrganizationDto;
import ai.unico.platform.util.model.UserContext;

import java.util.List;
import java.util.Set;

public interface OrganizationStructureService {

    List<OrganizationStructureDto> findOrganizationStructure(Set<Long> organizationIds, boolean members);

    OrganizationStructureDto findLowerOrganizationStructure(Long organizationId, boolean members); 

    Set<Long> findChildOrganizationIds(Long organizationId, boolean members); 

    Set<Long> findChildOrganizationIds(Set<Long> organizationIds, boolean members);

    Long findRootOrganizationId(Long organizationId);

    List<OrganizationDto> findRootOrganizationDtos(Set<Long> organizationIds);

    List<Long> findMemberOrganizations(Long organizationId);

    Set<Long> findMyMemberships(Long organizationId);

    Set<Long> findParents(Long organizationId);

    void deleteOrganizationStructure(Long organizationId, Long childOrganizationId, UserContext userContext);

    void addOrganizationStructure(Long organizationId, Long childOrganizationId, OrganizationRelationType relationType, UserContext userContext);

    void deleteCache();
}
