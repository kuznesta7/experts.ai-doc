package ai.unico.platform.store.api.dto;

public class EvidenceExpertStatisticsDto {

    private Long numberOfExperts;

    private Long numberOfRegisteredExperts;

    public Long getNumberOfExperts() {
        return numberOfExperts;
    }

    public void setNumberOfExperts(Long numberOfExperts) {
        this.numberOfExperts = numberOfExperts;
    }

    public Long getNumberOfRegisteredExperts() {
        return numberOfRegisteredExperts;
    }

    public void setNumberOfRegisteredExperts(Long numberOfRegisteredExperts) {
        this.numberOfRegisteredExperts = numberOfRegisteredExperts;
    }
}
