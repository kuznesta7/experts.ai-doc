package ai.unico.platform.store.api.dto;

public class UserOrganizationDto extends OrganizationBaseDto {

    private boolean visible = true;

    private boolean active = true;

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
