package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.SubscribeDto;
import ai.unico.platform.store.api.dto.SubscriptionTypeDto;

import java.util.List;

public interface SubscriptionService {

    String subscribeForNewsletter(SubscribeDto subscribeDto);

    void unsubscribe(SubscribeDto subscribeDto);

    List<SubscriptionTypeDto> listSubscriptionTypes();
}
