package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ClaimRequestDto;
import ai.unico.platform.store.api.dto.PreClaimDto;
import ai.unico.platform.store.api.dto.UserProfileDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.OutdatedEntityException;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ClaimProfileService {

    void claimExpertProfile(Long expertId, Long userId, UserContext userContext) throws NonexistentEntityException, OutdatedEntityException, IOException;

    void claimUserProfile(Long userToClaimId, Long claimingUserId, UserContext userContext) throws IOException;

    void unclaimExpertProfile(Long expertId, UserContext userContext) throws NonexistentEntityException, IOException;

    void unclaimUserProfile(Long userId, UserContext userContext) throws NonexistentEntityException, IOException;

    void preClaimProfiles(Long userId, Set<Long> userToClaimIds, Set<Long> expertToClaimIds, UserContext userContext) throws IOException;

    void claimAllPreClaimedProfiles(Long userId, UserContext userContext) throws IOException;

    Map<Long, Long> findClaimedExpertIdUserIdMap();

    Map<Long, Long> findClaimedExpertIdUserIdMap(Set<Long> expertIds);

    Map<Long, Long> findClaimedUserIdUserIdMap();

    Map<Long, Long> findClaimedUserIdUserIdMap(Set<Long> userIds);

    Set<Long> findVerifiedProfilesToClaimIds();

    void requestClaiming(ClaimRequestDto claimRequestDto, UserContext userContext);

    Set<Claim> findClaimsForUser(Long userId);

    List<Claim> findPreClaimsForUser(Long userId);

    List<PreClaimDto> findPreClaimsForUsersExperts(Set<Long> userIds, Set<Long> expertIds);

    Long DWHExpertToSTUser(Long expertId, String name, UserContext userContext) throws IOException;
    Long DWHExpertToSTUser(Long expertId, UserProfileDto userProfileDto, UserContext userContext) throws IOException;

    class Claim {
        private Long userId;
        private Long expertId;

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getExpertId() {
            return expertId;
        }

        public void setExpertId(Long expertId) {
            this.expertId = expertId;
        }
    }
}
