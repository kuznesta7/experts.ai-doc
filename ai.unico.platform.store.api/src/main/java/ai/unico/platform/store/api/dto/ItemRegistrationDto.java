package ai.unico.platform.store.api.dto;

import ai.unico.platform.util.enums.Confidentiality;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ItemRegistrationDto {

    private Long itemId;

    private Long originalItemId;

    private String itemName;

    private String itemDescription;

    private Integer year;

    private Long itemTypeId;

    private Set<Long> notRegisteredExpertsIds = new HashSet<>();

    private Set<Long> organizationIds;

    private Set<Long> expertIds = new HashSet<>();

    private Set<Long> userIds = new HashSet<>();

    private List<String> keywords;

    private Confidentiality confidentiality;

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getOriginalItemId() {
        return originalItemId;
    }

    public void setOriginalItemId(Long originalItemId) {
        this.originalItemId = originalItemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Long getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(Long itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public Set<Long>getNotRegisteredExpertsIds() {
        return notRegisteredExpertsIds;
    }

    public void setNotRegisteredExpertsIds(Set<Long> notRegisteredExperts) {
        this.notRegisteredExpertsIds = notRegisteredExperts;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<Long> getExpertIds() {
        return expertIds;
    }

    public void setExpertIds(Set<Long> expertIds) {
        this.expertIds = expertIds;
    }

    public Set<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Confidentiality getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Confidentiality confidentiality) {
        this.confidentiality = confidentiality;
    }
}
