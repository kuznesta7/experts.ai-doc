package ai.unico.platform.store.api.dto;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class EvidenceItemStatisticsDto {

    private Long numberOfAllItems;

    private Map<Integer, Long> allItemsYearHistogram;

    private Set<AnalyticsItemTypeGroupDto> itemTypeStatistics = new HashSet<>();

    public Long getNumberOfAllItems() {
        return numberOfAllItems;
    }

    public void setNumberOfAllItems(Long numberOfAllItems) {
        this.numberOfAllItems = numberOfAllItems;
    }

    public Map<Integer, Long> getAllItemsYearHistogram() {
        return allItemsYearHistogram;
    }

    public void setAllItemsYearHistogram(Map<Integer, Long> allItemsYearHistogram) {
        this.allItemsYearHistogram = allItemsYearHistogram;
    }

    public Set<AnalyticsItemTypeGroupDto> getItemTypeStatistics() {
        return itemTypeStatistics;
    }

    public void setItemTypeStatistics(Set<AnalyticsItemTypeGroupDto> itemTypeStatistics) {
        this.itemTypeStatistics = itemTypeStatistics;
    }

    public static class AnalyticsItemTypeGroupDto {

        private Long itemTypeGroupId;

        private String groupTitle;

        private Long numberOfItems;

        private Long numberOfImpacted;

        private Long numberOfUtilityModels;

        private Map<Integer, Long> itemsYearHistogram = new HashMap<>();

        public Long getItemTypeGroupId() {
            return itemTypeGroupId;
        }

        public void setItemTypeGroupId(Long itemTypeGroupId) {
            this.itemTypeGroupId = itemTypeGroupId;
        }

        public String getGroupTitle() {
            return groupTitle;
        }

        public void setGroupTitle(String groupTitle) {
            this.groupTitle = groupTitle;
        }

        public Long getNumberOfItems() {
            return numberOfItems;
        }

        public void setNumberOfItems(Long numberOfItems) {
            this.numberOfItems = numberOfItems;
        }

        public Long getNumberOfImpacted() {
            return numberOfImpacted;
        }

        public void setNumberOfImpacted(Long numberOfImpacted) {
            this.numberOfImpacted = numberOfImpacted;
        }

        public Long getNumberOfUtilityModels() {
            return numberOfUtilityModels;
        }

        public void setNumberOfUtilityModels(Long numberOfUtilityModels) {
            this.numberOfUtilityModels = numberOfUtilityModels;
        }

        public Map<Integer, Long> getItemsYearHistogram() {
            return itemsYearHistogram;
        }

        public void setItemsYearHistogram(Map<Integer, Long> itemsYearHistogram) {
            this.itemsYearHistogram = itemsYearHistogram;
        }
    }
}
