package ai.unico.platform.store.api.service;

import java.io.IOException;

public interface RecaptchaValidationService {

    void validateToken(String recaptchaToken) throws IOException;

}
