package ai.unico.platform.store.api.dto;

import ai.unico.platform.util.enums.OrganizationRole;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrganizationLicenceUsersDto {

    private Long organizationLicenceId;

    private Long organizationId;

    private OrganizationRole licenceRole;

    private Date validUntil;

    private boolean isValid;

    private Integer numberOfAvailableLicences;

    private Integer numberOfUsedLicences;

    private List<OrganizationUserDto> organizationUserDtos = new ArrayList<>();

    public Long getOrganizationLicenceId() {
        return organizationLicenceId;
    }

    public void setOrganizationLicenceId(Long organizationLicenceId) {
        this.organizationLicenceId = organizationLicenceId;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public OrganizationRole getLicenceRole() {
        return licenceRole;
    }

    public void setLicenceRole(OrganizationRole licenceRole) {
        this.licenceRole = licenceRole;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    public Integer getNumberOfAvailableLicences() {
        return numberOfAvailableLicences;
    }

    public void setNumberOfAvailableLicences(Integer numberOfAvailableLicences) {
        this.numberOfAvailableLicences = numberOfAvailableLicences;
    }

    public Integer getNumberOfUsedLicences() {
        return numberOfUsedLicences;
    }

    public void setNumberOfUsedLicences(Integer numberOfUsedLicences) {
        this.numberOfUsedLicences = numberOfUsedLicences;
    }

    public List<OrganizationUserDto> getOrganizationUserDtos() {
        return organizationUserDtos;
    }

    public void setOrganizationUserDtos(List<OrganizationUserDto> organizationUserDtos) {
        this.organizationUserDtos = organizationUserDtos;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }
}
