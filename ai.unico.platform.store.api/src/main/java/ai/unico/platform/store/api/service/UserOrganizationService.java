package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface UserOrganizationService {

    void updateOrganizationsForUser(Long userId, List<OrganizationBaseDto> organizationDtos, UserContext userContext) throws NonexistentEntityException;

    void addOrganization(Long userId, Long organizationId, boolean verify, UserContext userContext);

    void removeOrganization(Set<Long> userId, Long organizationId, UserContext userContext);

    void setFavoriteUserUserOrganization(Long userId, Long organizationId, boolean favorite, UserContext userContext) throws NonexistentEntityException;

    void verifyUsers(Set<Long> userId, Long organizationId, boolean verify, UserContext userContext);

    List<Long> addUserItemsToOrganization(Long userId, Long organizationId, boolean verify, UserContext userContext) throws IOException;

    void retireUserOrganization(Long userId, Long organizationId, boolean retired, UserContext userContext);

    void changeExpertOrganizationVisibility(Long userId, Long organizationId, boolean visible, UserContext userContext);

    void changeExpertOrganizationActivity(Long userId, Long organizationId, boolean active, UserContext userContext);
}
