package ai.unico.platform.store.api.exception;

import ai.unico.platform.util.exception.RuntimeExceptionWithMessage;

public class RecaptchaValidationException extends RuntimeExceptionWithMessage {
    public RecaptchaValidationException() {
        super("An error occurred during the request validation. Please try to repeat the action once again.", "Recaptcha validation failed");
    }
}
