package ai.unico.platform.store.api.dto;

import ai.unico.platform.util.enums.ExpertSearchType;

import java.util.*;

public class ExpertSearchHistoryPreviewDto {

    private Long searchHistId;

    private Date date;

    private String query;

    private Set<Long> organizationIds = new HashSet<>();

    private Set<Long> resultTypeGroupIds = new HashSet<>();

    private Integer yearFrom;

    private Integer yearTo;

    private List<String> secondaryQueries = new ArrayList<>();

    private List<Float> secondaryWeights = new ArrayList<>();

    private Set<String> workspaces;

    private boolean ontologyDisabled = false;

    private boolean favorite;

    private Long resultsFound;

    private Set<String> countryCodes = new HashSet<>();

    private ExpertSearchType expertSearchType;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<Long> getResultTypeGroupIds() {
        return resultTypeGroupIds;
    }

    public void setResultTypeGroupIds(Set<Long> resultTypeGroupIds) {
        this.resultTypeGroupIds = resultTypeGroupIds;
    }

    public Integer getYearFrom() {
        return yearFrom;
    }

    public void setYearFrom(Integer yearFrom) {
        this.yearFrom = yearFrom;
    }

    public Integer getYearTo() {
        return yearTo;
    }

    public void setYearTo(Integer yearTo) {
        this.yearTo = yearTo;
    }

    public List<String> getSecondaryQueries() {
        return secondaryQueries;
    }

    public void setSecondaryQueries(List<String> secondaryQueries) {
        this.secondaryQueries = secondaryQueries;
    }

    public boolean isOntologyDisabled() {
        return ontologyDisabled;
    }

    public void setOntologyDisabled(boolean ontologyDisabled) {
        this.ontologyDisabled = ontologyDisabled;
    }

    public Long getResultsFound() {
        return resultsFound;
    }

    public void setResultsFound(Long resultsFound) {
        this.resultsFound = resultsFound;
    }

    public List<Float> getSecondaryWeights() {
        return secondaryWeights;
    }

    public void setSecondaryWeights(List<Float> secondaryWeights) {
        this.secondaryWeights = secondaryWeights;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public Long getSearchHistId() {
        return searchHistId;
    }

    public void setSearchHistId(Long searchHistId) {
        this.searchHistId = searchHistId;
    }

    public Set<String> getWorkspaces() {
        return workspaces;
    }

    public void setWorkspaces(Set<String> workspaces) {
        this.workspaces = workspaces;
    }

    public Set<String> getCountryCodes() {
        return countryCodes;
    }

    public void setCountryCodes(Set<String> countryCodes) {
        this.countryCodes = countryCodes;
    }

    public ExpertSearchType getExpertSearchType() {
        return expertSearchType;
    }

    public void setExpertSearchType(ExpertSearchType expertSearchType) {
        this.expertSearchType = expertSearchType;
    }
}
