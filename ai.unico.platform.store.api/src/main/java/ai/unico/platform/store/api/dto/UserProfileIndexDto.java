package ai.unico.platform.store.api.dto;

import ai.unico.platform.util.enums.Role;

import java.util.*;

public class UserProfileIndexDto {

    private Long userId;

    private String name;

    private String username;

    private String email;

    private String description;

    private String positionDescription;

    private Set<Long> organizationIds = new HashSet<>();

    private Set<Long> verifiedOrganizationIds = new HashSet<>();

    private Set<Long> favoriteOrganizationIds = new HashSet<>();

    private Set<Long> retiredOrganizationIds = new HashSet<>();

    private Set<Long> visibleOrganizationIds = new HashSet<>();

    private Set<Long> activeOrganizationIds;

    private Set<Long> expertIds = new HashSet<>();

    private boolean claimable;

    private boolean invitation;

    private Long claimedById;

    private Set<Long> claimedExpertIds = new HashSet<>();

    private Set<Long> claimedUserIds = new HashSet<>();

    private Date retirementDate;

    private List<String> keywords = new ArrayList<>();

    private Set<Role> roles = new HashSet<>();

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPositionDescription() {
        return positionDescription;
    }

    public void setPositionDescription(String positionDescription) {
        this.positionDescription = positionDescription;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<Long> getExpertIds() {
        return expertIds;
    }

    public void setExpertIds(Set<Long> expertIds) {
        this.expertIds = expertIds;
    }

    public boolean isClaimable() {
        return claimable;
    }

    public void setClaimable(boolean claimable) {
        this.claimable = claimable;
    }

    public Long getClaimedById() {
        return claimedById;
    }

    public void setClaimedById(Long claimedById) {
        this.claimedById = claimedById;
    }

    public Set<Long> getVerifiedOrganizationIds() {
        return verifiedOrganizationIds;
    }

    public void setVerifiedOrganizationIds(Set<Long> verifiedOrganizationIds) {
        this.verifiedOrganizationIds = verifiedOrganizationIds;
    }

    public Set<Long> getFavoriteOrganizationIds() {
        return favoriteOrganizationIds;
    }

    public void setFavoriteOrganizationIds(Set<Long> favoriteOrganizationIds) {
        this.favoriteOrganizationIds = favoriteOrganizationIds;
    }

    public Set<Long> getRetiredOrganizationIds() {
        return retiredOrganizationIds;
    }

    public void setRetiredOrganizationIds(Set<Long> retiredOrganizationIds) {
        this.retiredOrganizationIds = retiredOrganizationIds;
    }

    public Date getRetirementDate() {
        return retirementDate;
    }

    public void setRetirementDate(Date retirementDate) {
        this.retirementDate = retirementDate;
    }

    public Set<Long> getClaimedExpertIds() {
        return claimedExpertIds;
    }

    public void setClaimedExpertIds(Set<Long> claimedExpertIds) {
        this.claimedExpertIds = claimedExpertIds;
    }

    public Set<Long> getClaimedUserIds() {
        return claimedUserIds;
    }

    public void setClaimedUserIds(Set<Long> claimedUserIds) {
        this.claimedUserIds = claimedUserIds;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public boolean isInvitation() {
        return invitation;
    }

    public void setInvitation(boolean invitation) {
        this.invitation = invitation;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Long> getVisibleOrganizationIds() {
        return visibleOrganizationIds;
    }

    public void setVisibleOrganizationIds(Set<Long> visibleOrganizationIds) {
        this.visibleOrganizationIds = visibleOrganizationIds;
    }

    public Set<Long> getActiveOrganizationIds() {
        return activeOrganizationIds;
    }

    public void setActiveOrganizationIds(Set<Long> activeOrganizationId) {
        this.activeOrganizationIds = activeOrganizationId;
    }
}
