package ai.unico.platform.store.api.enums;

public enum ClaimRequestType {
    ANOTHER_NAME, ABUSE, MISSING
}
