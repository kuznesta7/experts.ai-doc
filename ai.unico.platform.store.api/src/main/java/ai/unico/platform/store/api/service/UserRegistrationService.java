package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.UserRegistrationDto;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

public interface UserRegistrationService {

    Long registerUser(UserRegistrationDto userRegistrationDto, UserContext userContext) throws EntityAlreadyExistsException, NonexistentEntityException;

    UserRegistrationDto findUser(Long userId) throws NonexistentEntityException;

    void updateUser(UserRegistrationDto userRegistrationDto, UserContext userContext) throws NonexistentEntityException;

    void reinviteUser(UserRegistrationDto userRegistrationDto, UserContext userContext);
}
