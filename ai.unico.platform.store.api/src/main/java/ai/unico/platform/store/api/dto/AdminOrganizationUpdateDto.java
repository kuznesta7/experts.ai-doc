package ai.unico.platform.store.api.dto;

public class AdminOrganizationUpdateDto {

    private Long organizationId;

    private Long parentOrganizationId;

    private String organizationName;

    private String countryCode;

    private String organizationAbbrev;

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationAbbrev() {
        return organizationAbbrev;
    }

    public void setOrganizationAbbrev(String organizationAbbrev) {
        this.organizationAbbrev = organizationAbbrev;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Long getParentOrganizationId() {
        return parentOrganizationId;
    }

    public void setParentOrganizationId(Long parentOrganizationId) {
        this.parentOrganizationId = parentOrganizationId;
    }
}
