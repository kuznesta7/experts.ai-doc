package ai.unico.platform.store.api.exception;

import ai.unico.platform.util.exception.RuntimeExceptionWithMessage;

public class ChecksumMismatchException extends RuntimeExceptionWithMessage {
    public ChecksumMismatchException() {
        super("Invalid checksum");
    }
}
