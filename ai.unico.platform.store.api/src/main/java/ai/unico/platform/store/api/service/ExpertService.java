package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ExpertIndexDto;

import java.io.IOException;
import java.util.List;

public interface ExpertService {

    List<ExpertIndexDto> findUpdatedExperts();

    void loadAndUpdateExpert(Long expertId) throws IOException;

}
