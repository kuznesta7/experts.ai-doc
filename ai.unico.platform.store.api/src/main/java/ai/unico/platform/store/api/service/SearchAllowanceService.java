package ai.unico.platform.store.api.service;

import java.util.Set;

public interface SearchAllowanceService {

    void updateSearchAllowance(Long organizationId, boolean allowed);

    boolean isAllowed(Set<Long> organizationIds);

}
