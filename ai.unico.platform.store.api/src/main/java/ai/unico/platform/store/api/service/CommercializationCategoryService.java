package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.CommercializationCategoryDto;
import ai.unico.platform.store.api.dto.CommercializationCategoryPreviewDto;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface CommercializationCategoryService {

    List<CommercializationCategoryDto> findAvailableCommercializationCategories(boolean includeInactive);

    List<CommercializationCategoryPreviewDto> findAvailableCommercializationCategoriesPreview(boolean includeInactive);

    Long createCommercializationCategory(CommercializationCategoryDto commercializationCategoryDto, UserContext userContext);

    void updateCommercializationCategory(Long categoryId, CommercializationCategoryDto commercializationCategoryDto, UserContext userContext);

}
