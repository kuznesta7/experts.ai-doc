package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.enums.WidgetType;
import ai.unico.platform.util.model.UserContext;

import java.util.Map;

public interface WidgetService {

    void updateWidgets(Long organizationId, Map<WidgetType, Boolean> widgetChanges, UserContext userContext);

    boolean isAllowed(Long organizationId, WidgetType widgetType);

    Map<WidgetType, Boolean> allowed(Long organizationId);

}
