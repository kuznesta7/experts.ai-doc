package ai.unico.platform.store.api.dto;

import java.util.Date;

public class InteractionIndividualDto extends InteractionDto {

    private Long interactionIndividualId;
    private String visitedExpertCode;
    private String visitedOutcomeCode;
    private String visitedProjectCode;
    private Long visitedCommercializationId;
    private String visitedLectureCode;


    public Long getInteractionIndividualId() {
        return interactionIndividualId;
    }

    public void setInteractionIndividualId(Long interactionIndividualId) {
        this.interactionIndividualId = interactionIndividualId;
    }

    public Long getOrganization() {
        return organization;
    }

    public void setOrganization(Long organization) {
        this.organization = organization;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Long getVisitedCommercializationId() {
        return visitedCommercializationId;
    }

    public void setVisitedCommercializationId(Long visitedCommercializationId) {
        this.visitedCommercializationId = visitedCommercializationId;
    }

    public String getVisitedExpertCode() {
        return visitedExpertCode;
    }

    public void setVisitedExpertCode(String visitedExpertCode) {
        this.visitedExpertCode = visitedExpertCode;
    }

    public String getVisitedOutcomeCode() {
        return visitedOutcomeCode;
    }

    public void setVisitedOutcomeCode(String visitedOutcomeCode) {
        this.visitedOutcomeCode = visitedOutcomeCode;
    }

    public String getVisitedProjectCode() {
        return visitedProjectCode;
    }

    public void setVisitedProjectCode(String visitedProjectCode) {
        this.visitedProjectCode = visitedProjectCode;
    }

    public String getVisitedLectureCode() {
        return visitedLectureCode;
    }

    public void setVisitedLectureCode(String visitedLectureCode) {
        this.visitedLectureCode = visitedLectureCode;
    }
}
