package ai.unico.platform.store.api.filter;

import ai.unico.platform.util.enums.OrderDirection;

import java.util.Set;


public class UserProfilePreviewFilter {

    private Order order;

    private OrderDirection direction;

    private String query;

    private Set<String> tags;

    private Set<String> locations;

    private Integer limit;

    private Integer page;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public OrderDirection getDirection() {
        return direction;
    }

    public void setDirection(OrderDirection direction) {
        this.direction = direction;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Set<String> getLocations() {
        return locations;
    }

    public void setLocations(Set<String> locations) {
        this.locations = locations;
    }

    public static enum Order {
        NAME
    }

    public UserProfilePreviewFilter() {
    }

    public UserProfilePreviewFilter(Order order, OrderDirection direction, String query, Set<String> tags, Set<String> locations, Integer limit, Integer page) {
        this.order = order;
        this.direction = direction;
        this.query = query;
        this.tags = tags;
        this.locations = locations;
        this.limit = limit;
        this.page = page;
    }
}
