package ai.unico.platform.store.api.dto;

public class EvidenceMemberStatisticsDto {

    private Long memberCount;
    private Long membershipCount;
    private boolean allowedMembers;

    public Long getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(Long memberCount) {
        this.memberCount = memberCount;
    }

    public Long getMembershipCount() {
        return membershipCount;
    }

    public void setMembershipCount(Long membershipCount) {
        this.membershipCount = membershipCount;
    }

    public boolean isAllowedMembers() {
        return allowedMembers;
    }

    public void setAllowedMembers(boolean allowedMembers) {
        this.allowedMembers = allowedMembers;
    }

}
