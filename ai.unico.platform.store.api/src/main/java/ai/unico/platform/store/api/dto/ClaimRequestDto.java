package ai.unico.platform.store.api.dto;

import ai.unico.platform.store.api.enums.ClaimRequestType;

public class ClaimRequestDto {

    private ClaimRequestType claimRequestType;

    private String message;

    private Long concernedUserId;

    private Long concernedExpertId;

    public ClaimRequestType getClaimRequestType() {
        return claimRequestType;
    }

    public void setClaimRequestType(ClaimRequestType claimRequestType) {
        this.claimRequestType = claimRequestType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getConcernedUserId() {
        return concernedUserId;
    }

    public void setConcernedUserId(Long concernedUserId) {
        this.concernedUserId = concernedUserId;
    }

    public Long getConcernedExpertId() {
        return concernedExpertId;
    }

    public void setConcernedExpertId(Long concernedExpertId) {
        this.concernedExpertId = concernedExpertId;
    }
}
