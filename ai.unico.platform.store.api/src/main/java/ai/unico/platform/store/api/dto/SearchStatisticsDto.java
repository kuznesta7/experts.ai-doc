package ai.unico.platform.store.api.dto;

public class SearchStatisticsDto {

    private long statisticId;
    private String value;
    private long searches;
    private long uniqueSearches;
    private boolean hidden;
    private boolean hotTopic;

    public long getStatisticId() {
        return statisticId;
    }

    public void setStatisticId(long statisticId) {
        this.statisticId = statisticId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public long getSearches() {
        return searches;
    }

    public void setSearches(long searches) {
        this.searches = searches;
    }

    public long getUniqueSearches() {
        return uniqueSearches;
    }

    public void setUniqueSearches(long uniqueSearches) {
        this.uniqueSearches = uniqueSearches;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public boolean isHotTopic() {
        return hotTopic;
    }

    public void setHotTopic(boolean hotTopic) {
        this.hotTopic = hotTopic;
    }
}
