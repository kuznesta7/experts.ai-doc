package ai.unico.platform.store.api.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class OpportunityRegisterDto {

    private String opportunityCode;
    private Long originalId;
    private String opportunityName;
    private String opportunityDescription;
    private Set<String> opportunityKw;
    private Date opportunitySignupDate;
    private String opportunityLocation;
    private String opportunityWage;
    private String opportunityTechReq;
    private String opportunityFormReq;
    private String opportunityOtherReq;
    private String opportunityBenefit;
    private Date opportunityJobStartDate;
    private String opportunityExtLink;
    private String opportunityHomeOffice;
    private Long opportunityType;

    private Boolean hidden;
    private Set<Long> jobTypes = new HashSet<>();
    private Set<Long> organizationIds = new HashSet<>();
    private Set<String> expertIds = new HashSet<>();
    private Set<Long> memberOrganizationIds = new HashSet<>();


    public String getOpportunityCode() {
        return opportunityCode;
    }

    public void setOpportunityCode(String opportunityCode) {
        this.opportunityCode = opportunityCode;
    }

    public Long getOriginalId() {
        return originalId;
    }

    public void setOriginalId(Long originalId) {
        this.originalId = originalId;
    }

    public String getOpportunityName() {
        return opportunityName;
    }

    public void setOpportunityName(String opportunityName) {
        this.opportunityName = opportunityName;
    }

    public String getOpportunityDescription() {
        return opportunityDescription;
    }

    public void setOpportunityDescription(String opportunityDescription) {
        this.opportunityDescription = opportunityDescription;
    }

    public Set<String> getOpportunityKw() {
        return opportunityKw;
    }

    public void setOpportunityKw(Set<String> opportunityKw) {
        this.opportunityKw = opportunityKw;
    }

    public Date getOpportunitySignupDate() {
        return opportunitySignupDate;
    }

    public void setOpportunitySignupDate(Date opportunitySignupDate) {
        this.opportunitySignupDate = opportunitySignupDate;
    }

    public String getOpportunityLocation() {
        return opportunityLocation;
    }

    public void setOpportunityLocation(String opportunityLocation) {
        this.opportunityLocation = opportunityLocation;
    }

    public String getOpportunityWage() {
        return opportunityWage;
    }

    public void setOpportunityWage(String opportunityWage) {
        this.opportunityWage = opportunityWage;
    }

    public String getOpportunityTechReq() {
        return opportunityTechReq;
    }

    public void setOpportunityTechReq(String opportunityTechReq) {
        this.opportunityTechReq = opportunityTechReq;
    }

    public String getOpportunityFormReq() {
        return opportunityFormReq;
    }

    public void setOpportunityFormReq(String opportunityFormReq) {
        this.opportunityFormReq = opportunityFormReq;
    }

    public String getOpportunityOtherReq() {
        return opportunityOtherReq;
    }

    public void setOpportunityOtherReq(String opportunityOtherReq) {
        this.opportunityOtherReq = opportunityOtherReq;
    }

    public String getOpportunityBenefit() {
        return opportunityBenefit;
    }

    public void setOpportunityBenefit(String opportunityBenefit) {
        this.opportunityBenefit = opportunityBenefit;
    }

    public Date getOpportunityJobStartDate() {
        return opportunityJobStartDate;
    }

    public void setOpportunityJobStartDate(Date opportunityJobStartDate) {
        this.opportunityJobStartDate = opportunityJobStartDate;
    }

    public String getOpportunityExtLink() {
        return opportunityExtLink;
    }

    public void setOpportunityExtLink(String opportunityExtLink) {
        this.opportunityExtLink = opportunityExtLink;
    }

    public String getOpportunityHomeOffice() {
        return opportunityHomeOffice;
    }

    public void setOpportunityHomeOffice(String opportunityHomeOffice) {
        this.opportunityHomeOffice = opportunityHomeOffice;
    }

    public Long getOpportunityType() {
        return opportunityType;
    }

    public void setOpportunityType(Long opportunityType) {
        this.opportunityType = opportunityType;
    }

    public Set<Long> getJobTypes() {
        return jobTypes;
    }

    public void setJobTypes(Set<Long> jobTypes) {
        this.jobTypes = jobTypes;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<String> getExpertIds() {
        return expertIds;
    }

    public void setExpertIds(Set<String> expertIds) {
        this.expertIds = expertIds;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public Set<Long> getMemberOrganizationIds() {
        return memberOrganizationIds;
    }

    public void setMemberOrganizationIds(Set<Long> memberOrganizationIds) {
        this.memberOrganizationIds = memberOrganizationIds;
    }
}
