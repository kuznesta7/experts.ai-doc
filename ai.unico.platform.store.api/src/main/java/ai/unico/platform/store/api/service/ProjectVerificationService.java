package ai.unico.platform.store.api.service;

import ai.unico.platform.util.model.UserContext;

public interface ProjectVerificationService {

    Long verifyDWHProject(Long originalProjectId, UserContext userContext);

}
