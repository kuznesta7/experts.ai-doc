package ai.unico.platform.store.api.dto;

public class ProjectOrganizationDto {

    private Long organizationId;

    private String organizationName;

    private boolean hidden;

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public ProjectOrganizationDto() {
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public ProjectOrganizationDto(Long organizationId, String organizationName) {
        this.organizationId = organizationId;
        this.organizationName = organizationName;
    }
}
