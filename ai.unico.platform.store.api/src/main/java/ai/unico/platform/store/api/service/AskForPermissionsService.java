package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.AskForPermissionsDto;
import ai.unico.platform.util.model.UserContext;

public interface AskForPermissionsService {

    void askForPermissions(AskForPermissionsDto askForPermissionsDto, UserContext userContext);

}
