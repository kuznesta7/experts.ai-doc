package ai.unico.platform.store.api.wrapper;

import ai.unico.platform.store.api.dto.ExpertSearchHistoryPreviewDto;

import java.util.ArrayList;
import java.util.List;

public class ExpertSearchHistoryWrapper {

    private Integer limit;

    private Integer page;

    private List<ExpertSearchHistoryPreviewDto> historyDtos = new ArrayList<>();

    private Long numberOfResults;

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<ExpertSearchHistoryPreviewDto> getHistoryDtos() {
        return historyDtos;
    }

    public void setHistoryDtos(List<ExpertSearchHistoryPreviewDto> historyDtos) {
        this.historyDtos = historyDtos;
    }

    public Long getNumberOfResults() {
        return numberOfResults;
    }

    public void setNumberOfResults(Long numberOfResults) {
        this.numberOfResults = numberOfResults;
    }
}
