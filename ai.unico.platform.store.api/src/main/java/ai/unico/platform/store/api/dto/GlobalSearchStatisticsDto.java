package ai.unico.platform.store.api.dto;

public class GlobalSearchStatisticsDto {

    private Long numberOfSearches;

    private Long numberOfProfilesVisited;

    public Long getNumberOfSearches() {
        return numberOfSearches;
    }

    public void setNumberOfSearches(Long numberOfSearches) {
        this.numberOfSearches = numberOfSearches;
    }

    public Long getNumberOfProfilesVisited() {
        return numberOfProfilesVisited;
    }

    public void setNumberOfProfilesVisited(Long numberOfProfilesVisited) {
        this.numberOfProfilesVisited = numberOfProfilesVisited;
    }
}
