package ai.unico.platform.store.api.dto;

import java.util.Set;

public class QueryTranslationDto {

    private Long translationId;

    private String translationPhrase;

    private String query;

    private String fullNameQuery;

    private boolean excludeInvestProjects;

    private boolean excludeOutcomes;

    private Integer yearFrom;

    private Integer yearTo;

    private Set<Long> organizationIds;

    private Set<Long> typeGroupIds;

    private Set<Long> typeIds;

    private Set<String> countryCodes;

    public Long getTranslationId() {
        return translationId;
    }

    public void setTranslationId(Long translationId) {
        this.translationId = translationId;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getFullNameQuery() {
        return fullNameQuery;
    }

    public void setFullNameQuery(String fullNameQuery) {
        this.fullNameQuery = fullNameQuery;
    }

    public String getTranslationPhrase() {
        return translationPhrase;
    }

    public void setTranslationPhrase(String translationPhrase) {
        this.translationPhrase = translationPhrase;
    }

    public boolean isExcludeInvestProjects() {
        return excludeInvestProjects;
    }

    public void setExcludeInvestProjects(boolean excludeInvestProjects) {
        this.excludeInvestProjects = excludeInvestProjects;
    }

    public boolean isExcludeOutcomes() {
        return excludeOutcomes;
    }

    public void setExcludeOutcomes(boolean excludeOutcomes) {
        this.excludeOutcomes = excludeOutcomes;
    }

    public Integer getYearFrom() {
        return yearFrom;
    }

    public void setYearFrom(Integer yearFrom) {
        this.yearFrom = yearFrom;
    }

    public Integer getYearTo() {
        return yearTo;
    }

    public void setYearTo(Integer yearTo) {
        this.yearTo = yearTo;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<Long> getTypeGroupIds() {
        return typeGroupIds;
    }

    public void setTypeGroupIds(Set<Long> typeGroupIds) {
        this.typeGroupIds = typeGroupIds;
    }

    public Set<Long> getTypeIds() {
        return typeIds;
    }

    public void setTypeIds(Set<Long> typeIds) {
        this.typeIds = typeIds;
    }

    public Set<String> getCountryCodes() {
        return countryCodes;
    }

    public void setCountryCodes(Set<String> countryCodes) {
        this.countryCodes = countryCodes;
    }
}
