package ai.unico.platform.store.api.enums;

public enum OrganizationRelationType {

    PARENT, AFFILIATED, MEMBER

}
