package ai.unico.platform.store.api.wrapper;

import ai.unico.platform.store.api.dto.FollowedProfilePreviewDto;

import java.util.List;

public class FollowedProfilesWrapper {

    private Long numberOfResults;

    private Integer limit;

    private Integer page;

    private List<FollowedProfilePreviewDto> followedProfilePreviewDtos;

    public Long getNumberOfResults() {
        return numberOfResults;
    }

    public void setNumberOfResults(Long numberOfResults) {
        this.numberOfResults = numberOfResults;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<FollowedProfilePreviewDto> getFollowedProfilePreviewDtos() {
        return followedProfilePreviewDtos;
    }

    public void setFollowedProfilePreviewDtos(List<FollowedProfilePreviewDto> followedProfilePreviewDtos) {
        this.followedProfilePreviewDtos = followedProfilePreviewDtos;
    }
}
