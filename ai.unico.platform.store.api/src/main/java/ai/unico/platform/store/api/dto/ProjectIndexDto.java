package ai.unico.platform.store.api.dto;

import ai.unico.platform.util.enums.Confidentiality;

import java.util.*;

public class ProjectIndexDto {

    private Long projectId;
    private Long originalProjectId;
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;
    private Long projectProgramId;
    private Long ownerOrganizationId;
    private Confidentiality confidentiality;
    private String projectNumber;
    private List<String> keywords = new ArrayList<>();
    private Set<Long> userIds = new HashSet<>();
    private Set<Long> expertIds = new HashSet<>();
    private Map<Long, Long> organizationParentProjectIdMap = new HashMap<>();
    private Set<Long> organizationIds = new HashSet<>();
    private Set<Long> verifiedOrganizationIds = new HashSet<>();
    private Set<Long> providingOrganizationIds = new HashSet<>();
    private Set<Long> itemIds = new HashSet<>();
    private Set<Long> originalItemIds = new HashSet<>();
    private List<ProjectOrganizationBudget> projectOrganizationBudgetList = new ArrayList<>();

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getOriginalProjectId() {
        return originalProjectId;
    }

    public void setOriginalProjectId(Long originalProjectId) {
        this.originalProjectId = originalProjectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<Long> getItemIds() {
        return itemIds;
    }

    public void setItemIds(Set<Long> itemIds) {
        this.itemIds = itemIds;
    }

    public Map<Long, Long> getOrganizationParentProjectIdMap() {
        return organizationParentProjectIdMap;
    }

    public void setOrganizationParentProjectIdMap(Map<Long, Long> organizationParentProjectIdMap) {
        this.organizationParentProjectIdMap = organizationParentProjectIdMap;
    }

    public Set<Long> getVerifiedOrganizationIds() {
        return verifiedOrganizationIds;
    }

    public void setVerifiedOrganizationIds(Set<Long> verifiedOrganizationIds) {
        this.verifiedOrganizationIds = verifiedOrganizationIds;
    }

    public Set<Long> getOriginalItemIds() {
        return originalItemIds;
    }

    public void setOriginalItemIds(Set<Long> originalItemIds) {
        this.originalItemIds = originalItemIds;
    }

    public Set<Long> getProvidingOrganizationIds() {
        return providingOrganizationIds;
    }

    public void setProvidingOrganizationIds(Set<Long> providingOrganizationIds) {
        this.providingOrganizationIds = providingOrganizationIds;
    }

    public List<ProjectOrganizationBudget> getProjectOrganizationBudgetList() {
        return projectOrganizationBudgetList;
    }

    public void setProjectOrganizationBudgetList(List<ProjectOrganizationBudget> projectOrganizationBudgetList) {
        this.projectOrganizationBudgetList = projectOrganizationBudgetList;
    }

    public Long getProjectProgramId() {
        return projectProgramId;
    }

    public void setProjectProgramId(Long projectProgramId) {
        this.projectProgramId = projectProgramId;
    }

    public Confidentiality getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Confidentiality confidentiality) {
        this.confidentiality = confidentiality;
    }

    public Long getOwnerOrganizationId() {
        return ownerOrganizationId;
    }

    public void setOwnerOrganizationId(Long ownerOrganizationId) {
        this.ownerOrganizationId = ownerOrganizationId;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public Set<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }

    public Set<Long> getExpertIds() {
        return expertIds;
    }

    public void setExpertIds(Set<Long> expertIds) {
        this.expertIds = expertIds;
    }

    public static class ProjectOrganizationBudget {
        private Long organizationId;

        private Integer year;

        private Double total;

        private Double nationalSupport;

        private Double privateFinances;

        private Double otherFinances;

        public Long getOrganizationId() {
            return organizationId;
        }

        public void setOrganizationId(Long organizationId) {
            this.organizationId = organizationId;
        }

        public Integer getYear() {
            return year;
        }

        public void setYear(Integer year) {
            this.year = year;
        }

        public Double getTotal() {
            return total;
        }

        public void setTotal(Double total) {
            this.total = total;
        }

        public Double getNationalSupport() {
            return nationalSupport;
        }

        public void setNationalSupport(Double nationalSupport) {
            this.nationalSupport = nationalSupport;
        }

        public Double getPrivateFinances() {
            return privateFinances;
        }

        public void setPrivateFinances(Double privateFinances) {
            this.privateFinances = privateFinances;
        }

        public Double getOtherFinances() {
            return otherFinances;
        }

        public void setOtherFinances(Double otherFinances) {
            this.otherFinances = otherFinances;
        }
    }

}
