package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.PublicCommercializationRequestDto;

import java.io.IOException;

public interface PublicCommercializationRequestService {

    void createPublicRequest(Long commercializationId, PublicCommercializationRequestDto requestDto, String recaptchaToken) throws IOException;

}
