package ai.unico.platform.store.api.wrapper;

import ai.unico.platform.store.api.dto.MessageDto;

import java.util.ArrayList;
import java.util.List;

public class MessageWrapper {

    private Integer limit;

    private Integer page;

    private List<MessageDto> messages = new ArrayList<>();

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<MessageDto> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageDto> messages) {
        this.messages = messages;
    }
}
