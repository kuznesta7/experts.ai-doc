package ai.unico.platform.store.api.dto;

import java.util.Map;

public class EvidenceProjectStatisticsDto {

    private Long numberOfAllProjects;

    private Double allProjectsFinancesSum;

    private Map<Integer, Double> yearProjectFinancesSumHistogram;

    public Long getNumberOfAllProjects() {
        return numberOfAllProjects;
    }

    public void setNumberOfAllProjects(Long numberOfAllProjects) {
        this.numberOfAllProjects = numberOfAllProjects;
    }

    public Double getAllProjectsFinancesSum() {
        return allProjectsFinancesSum;
    }

    public void setAllProjectsFinancesSum(Double allProjectsFinancesSum) {
        this.allProjectsFinancesSum = allProjectsFinancesSum;
    }

    public Map<Integer, Double> getYearProjectFinancesSumHistogram() {
        return yearProjectFinancesSumHistogram;
    }

    public void setYearProjectFinancesSumHistogram(Map<Integer, Double> yearProjectFinancesSumHistogram) {
        this.yearProjectFinancesSumHistogram = yearProjectFinancesSumHistogram;
    }
}
