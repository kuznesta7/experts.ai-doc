package ai.unico.platform.store.api.dto;

public class ShareCodeDto {

    private String code;

    public ShareCodeDto(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
