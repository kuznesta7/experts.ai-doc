package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.FollowProfileDto;
import ai.unico.platform.store.api.dto.FollowedProfilePreviewDto;
import ai.unico.platform.store.api.filter.FollowedProfilesFilter;
import ai.unico.platform.store.api.wrapper.FollowedProfilesWrapper;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;

public interface FollowProfileService {

    void followProfile(FollowProfileDto followProfileDto, UserContext userContext);

    void unfollowUserProfile(Long followingUserId, Long followedUserId, UserContext userContext);

    void unfollowExpertProfile(Long followingUserId, Long followedExpertId, UserContext userContext);

    FollowedProfilesWrapper findFollowedProfilesForUser(Long userId, FollowedProfilesFilter followedProfilesFilter) throws IOException;

    FollowedProfilePreviewDto findFollowedUserProfile(Long userId, Long followedUserId);

    FollowedProfilePreviewDto findFollowedExpertProfile(Long userId, Long followedExpertId) throws IOException;
}
