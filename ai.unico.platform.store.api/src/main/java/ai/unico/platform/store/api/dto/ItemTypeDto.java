package ai.unico.platform.store.api.dto;

public class ItemTypeDto {

    private Long typeId;

    private String typeTitle;

    private String typeCode;

    private boolean addable;

    private boolean impacted;

    private boolean utilityModel;


    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getTypeTitle() {
        return typeTitle;
    }

    public void setTypeTitle(String typeTitle) {
        this.typeTitle = typeTitle;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public boolean isAddable() {
        return addable;
    }

    public void setAddable(boolean addable) {
        this.addable = addable;
    }

    public boolean isImpacted() {
        return impacted;
    }

    public void setImpacted(boolean impacted) {
        this.impacted = impacted;
    }

    public boolean isUtilityModel() {
        return utilityModel;
    }

    public void setUtilityModel(boolean utilityModel) {
        this.utilityModel = utilityModel;
    }
}
