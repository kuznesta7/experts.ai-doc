package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.enums.CommercializationOrganizationRelation;
import ai.unico.platform.util.model.UserContext;

public interface CommercializationOrganizationService {

    void addOrganization(Long commercializationId, Long organizationId, CommercializationOrganizationRelation relation, UserContext userContext);

    void removeOrganization(Long commercializationId, Long organizationId, CommercializationOrganizationRelation relation, UserContext userContext);

}
