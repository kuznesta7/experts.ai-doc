package ai.unico.platform.store.api.dto;

import java.util.Map;

public class ExpertsStatisticsDto {
    private CountryDto country;
    private Long experts;
    private Long verified;
    private Long active;
    private Map<String, Long> roleCount;
    private Long retired;
    private Long claimed;

    public CountryDto getCountry() {
        return country;
    }

    public void setCountry(CountryDto country) {
        this.country = country;
    }

    public Long getExperts() {
        return experts;
    }

    public void setExperts(Long experts) {
        this.experts = experts;
    }

    public Long getVerified() {
        return verified;
    }

    public void setVerified(Long verified) {
        this.verified = verified;
    }

    public Long getActive() {
        return active;
    }

    public void setActive(Long active) {
        this.active = active;
    }

    public Map<String, Long> getRoleCount() {
        return roleCount;
    }

    public void setRoleCount(Map<String, Long> roleCount) {
        this.roleCount = roleCount;
    }

    public Long getRetired() {
        return retired;
    }

    public void setRetired(Long retired) {
        this.retired = retired;
    }

    public Long getClaimed() {
        return claimed;
    }

    public void setClaimed(Long claimed) {
        this.claimed = claimed;
    }
}
