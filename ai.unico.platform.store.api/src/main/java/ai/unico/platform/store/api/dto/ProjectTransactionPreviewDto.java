package ai.unico.platform.store.api.dto;


import ai.unico.platform.store.api.enums.Currency;

import java.util.Date;

public class ProjectTransactionPreviewDto {

    private Long transactionId;

    private Long projectId;

    private String transactionNote;

    private Double amount;

    private Currency currency;

    private OrganizationBaseDto organizationFrom;

    private OrganizationBaseDto organizationTo;

    private Date transactionDate;

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getTransactionNote() {
        return transactionNote;
    }

    public void setTransactionNote(String transactionNote) {
        this.transactionNote = transactionNote;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public OrganizationBaseDto getOrganizationFrom() {
        return organizationFrom;
    }

    public void setOrganizationFrom(OrganizationBaseDto organizationFrom) {
        this.organizationFrom = organizationFrom;
    }

    public OrganizationBaseDto getOrganizationTo() {
        return organizationTo;
    }

    public void setOrganizationTo(OrganizationBaseDto organizationTo) {
        this.organizationTo = organizationTo;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }
}
