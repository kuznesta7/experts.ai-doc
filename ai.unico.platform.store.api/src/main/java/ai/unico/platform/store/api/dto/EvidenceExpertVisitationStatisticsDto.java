package ai.unico.platform.store.api.dto;

import java.util.ArrayList;
import java.util.List;

public class EvidenceExpertVisitationStatisticsDto {

    private Long numberOfProfilesVisited;

    private Integer page;

    private Integer limit;

    private List<ProfileVisitationStatisticsDto> mostVisitedProfiles = new ArrayList<>();

    public Long getNumberOfProfilesVisited() {
        return numberOfProfilesVisited;
    }

    public void setNumberOfProfilesVisited(Long numberOfProfilesVisited) {
        this.numberOfProfilesVisited = numberOfProfilesVisited;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<ProfileVisitationStatisticsDto> getMostVisitedProfiles() {
        return mostVisitedProfiles;
    }

    public void setMostVisitedProfiles(List<ProfileVisitationStatisticsDto> mostVisitedProfiles) {
        this.mostVisitedProfiles = mostVisitedProfiles;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public static class ProfileVisitationStatisticsDto {

        private Long userId;

        private Long expertId;

        private Long numberOfVisitations;

        private String fullName;

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getExpertId() {
            return expertId;
        }

        public void setExpertId(Long expertId) {
            this.expertId = expertId;
        }

        public Long getNumberOfVisitations() {
            return numberOfVisitations;
        }

        public void setNumberOfVisitations(Long numberOfVisitations) {
            this.numberOfVisitations = numberOfVisitations;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }
    }
}
