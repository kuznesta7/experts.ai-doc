package ai.unico.platform.store.api.dto;

import ai.unico.platform.util.enums.ExpertSearchType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ExpertSearchHistoryDto {

    private String query;

    private Integer page;

    private Integer limit;

    private Set<Long> organizationIds = new HashSet<>();

    private Set<Long> resultTypeGroupIds = new HashSet<>();

    private Integer yearFrom;

    private Integer yearTo;

    private Set<String> countryCodes = new HashSet<>();

    private List<String> secondaryQueries = new ArrayList<>();

    private List<Float> secondaryWeights = new ArrayList<>();

    private List<String> disabledSecondaryQueries = new ArrayList<>();

    private List<ResultDto> resultDtos = new ArrayList<>();

    private boolean ontologyDisabled = false;

    private Long resultsFound;

    private String shareCode;

    private ExpertSearchType searchType;

    private Long userId;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<Long> getResultTypeGroupIds() {
        return resultTypeGroupIds;
    }

    public void setResultTypeGroupIds(Set<Long> resultTypeGroupIds) {
        this.resultTypeGroupIds = resultTypeGroupIds;
    }

    public Integer getYearFrom() {
        return yearFrom;
    }

    public void setYearFrom(Integer yearFrom) {
        this.yearFrom = yearFrom;
    }

    public Integer getYearTo() {
        return yearTo;
    }

    public void setYearTo(Integer yearTo) {
        this.yearTo = yearTo;
    }

    public List<String> getSecondaryQueries() {
        return secondaryQueries;
    }

    public void setSecondaryQueries(List<String> secondaryQueries) {
        this.secondaryQueries = secondaryQueries;
    }

    public boolean isOntologyDisabled() {
        return ontologyDisabled;
    }

    public void setOntologyDisabled(boolean ontologyDisabled) {
        this.ontologyDisabled = ontologyDisabled;
    }

    public List<String> getDisabledSecondaryQueries() {
        return disabledSecondaryQueries;
    }

    public void setDisabledSecondaryQueries(List<String> disabledSecondaryQueries) {
        this.disabledSecondaryQueries = disabledSecondaryQueries;
    }

    public Long getResultsFound() {
        return resultsFound;
    }

    public void setResultsFound(Long resultsFound) {
        this.resultsFound = resultsFound;
    }

    public List<Float> getSecondaryWeights() {
        return secondaryWeights;
    }

    public void setSecondaryWeights(List<Float> secondaryWeights) {
        this.secondaryWeights = secondaryWeights;
    }

    public List<ResultDto> getResultDtos() {
        return resultDtos;
    }

    public void setResultDtos(List<ResultDto> resultDtos) {
        this.resultDtos = resultDtos;
    }

    public Set<String> getCountryCodes() {
        return countryCodes;
    }

    public void setCountryCodes(Set<String> countryCodes) {
        this.countryCodes = countryCodes;
    }

    public String getShareCode() {
        return shareCode;
    }

    public void setShareCode(String shareCode) {
        this.shareCode = shareCode;
    }

    public ExpertSearchType getSearchType() {
        return searchType;
    }

    public void setSearchType(ExpertSearchType searchType) {
        this.searchType = searchType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public static class ResultDto {
        private Integer position;
        private Long userId;
        private Long expertId;

        public ResultDto(Integer position, Long userId, Long expertId) {
            this.position = position;
            this.userId = userId;
            this.expertId = expertId;
        }

        public Integer getPosition() {
            return position;
        }

        public void setPosition(Integer position) {
            this.position = position;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getExpertId() {
            return expertId;
        }

        public void setExpertId(Long expertId) {
            this.expertId = expertId;
        }
    }
}
