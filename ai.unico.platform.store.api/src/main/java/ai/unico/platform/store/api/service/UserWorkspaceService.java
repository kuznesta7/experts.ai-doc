package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.UserWorkspaceDto;
import ai.unico.platform.store.api.dto.UserWorkspacePreviewDto;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface UserWorkspaceService {

    Long createUserWorkspace(UserWorkspaceDto userWorkspaceDto, UserContext userContext);

    void updateUserWorkspace(Long userWorkspaceId, UserWorkspaceDto userWorkspaceDto, UserContext userContext);

    void deleteUserWorkspace(Long userWorkspaceId, UserContext userContext);

    List<UserWorkspacePreviewDto> findUserWorkspaces(Long userId);

}
