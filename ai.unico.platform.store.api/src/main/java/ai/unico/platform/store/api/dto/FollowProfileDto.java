package ai.unico.platform.store.api.dto;

import java.util.HashSet;
import java.util.Set;

public class FollowProfileDto {

    private Long followedUserId;

    private Long followedExpertId;

    private Long followingUserId;

    private Set<String> workspaces = new HashSet<>();

    public Long getFollowedUserId() {
        return followedUserId;
    }

    public void setFollowedUserId(Long followedUserId) {
        this.followedUserId = followedUserId;
    }

    public Long getFollowedExpertId() {
        return followedExpertId;
    }

    public void setFollowedExpertId(Long followedExpertId) {
        this.followedExpertId = followedExpertId;
    }

    public Long getFollowingUserId() {
        return followingUserId;
    }

    public void setFollowingUserId(Long followingUserId) {
        this.followingUserId = followingUserId;
    }

    public Set<String> getWorkspaces() {
        return workspaces;
    }

    public void setWorkspaces(Set<String> workspaces) {
        this.workspaces = workspaces;
    }

}
