package ai.unico.platform.store.api.dto;

import ai.unico.platform.store.api.enums.CommercializationOrganizationRelation;

import java.util.*;

public class CommercializationProjectIndexDto {

    private Long commercializationProjectId;

    private String imgChecksum;

    private String name;

    private String executiveSummary;

    private String useCase;

    private String painDescription;

    private String competitiveAdvantage;

    private String technicalPrinciples;

    private Integer technologyReadinessLevel;

    private Date startDate;

    private Date endDate;

    private Date statusDeadline;

    private boolean deleted;

    private Integer commercializationInvestmentFrom;

    private Integer commercializationInvestmentTo;

    private Long commercializationPriorityId;

    private Long intermediaryOrganizationId;

    private Long commercializationDomainId;

    private Set<Long> commercializationCategoryIds = new HashSet<>();

    private Set<Long> teamMemberUserIds = new HashSet<>();

    private Set<Long> teamLeaderUserIds = new HashSet<>();

    private Set<Long> teamMemberExpertIds = new HashSet<>();

    private Set<Long> teamLeaderExpertIds = new HashSet<>();

    private Long statusId;

    private Integer ideaScore;

    private List<String> keywords;

    private Map<CommercializationOrganizationRelation, Set<Long>> commercializationOrganizationRelationMap = new HashMap<>();

    private Long ownerOrganizationId;

    private String link;

    public Long getCommercializationProjectId() {
        return commercializationProjectId;
    }

    public void setCommercializationProjectId(Long commercializationProjectId) {
        this.commercializationProjectId = commercializationProjectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExecutiveSummary() {
        return executiveSummary;
    }

    public void setExecutiveSummary(String executiveSummary) {
        this.executiveSummary = executiveSummary;
    }

    public String getUseCase() {
        return useCase;
    }

    public void setUseCase(String useCase) {
        this.useCase = useCase;
    }

    public String getPainDescription() {
        return painDescription;
    }

    public void setPainDescription(String painDescription) {
        this.painDescription = painDescription;
    }

    public String getCompetitiveAdvantage() {
        return competitiveAdvantage;
    }

    public void setCompetitiveAdvantage(String competitiveAdvantage) {
        this.competitiveAdvantage = competitiveAdvantage;
    }

    public String getTechnicalPrinciples() {
        return technicalPrinciples;
    }

    public void setTechnicalPrinciples(String technicalPrinciples) {
        this.technicalPrinciples = technicalPrinciples;
    }

    public Integer getTechnologyReadinessLevel() {
        return technologyReadinessLevel;
    }

    public void setTechnologyReadinessLevel(Integer technologyReadinessLevel) {
        this.technologyReadinessLevel = technologyReadinessLevel;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStatusDeadline() {
        return statusDeadline;
    }

    public void setStatusDeadline(Date statusDeadline) {
        this.statusDeadline = statusDeadline;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getCommercializationInvestmentFrom() {
        return commercializationInvestmentFrom;
    }

    public void setCommercializationInvestmentFrom(Integer commercializationInvestmentFrom) {
        this.commercializationInvestmentFrom = commercializationInvestmentFrom;
    }

    public Integer getCommercializationInvestmentTo() {
        return commercializationInvestmentTo;
    }

    public void setCommercializationInvestmentTo(Integer commercializationInvestmentTo) {
        this.commercializationInvestmentTo = commercializationInvestmentTo;
    }

    public Set<Long> getCommercializationCategoryIds() {
        return commercializationCategoryIds;
    }

    public void setCommercializationCategoryIds(Set<Long> commercializationCategoryIds) {
        this.commercializationCategoryIds = commercializationCategoryIds;
    }

    public Long getCommercializationPriorityId() {
        return commercializationPriorityId;
    }

    public void setCommercializationPriorityId(Long commercializationPriorityId) {
        this.commercializationPriorityId = commercializationPriorityId;
    }

    public Long getCommercializationDomainId() {
        return commercializationDomainId;
    }

    public void setCommercializationDomainId(Long commercializationDomainId) {
        this.commercializationDomainId = commercializationDomainId;
    }

    public Set<Long> getTeamMemberUserIds() {
        return teamMemberUserIds;
    }

    public void setTeamMemberUserIds(Set<Long> teamMemberUserIds) {
        this.teamMemberUserIds = teamMemberUserIds;
    }

    public Set<Long> getTeamLeaderUserIds() {
        return teamLeaderUserIds;
    }

    public void setTeamLeaderUserIds(Set<Long> teamLeaderUserIds) {
        this.teamLeaderUserIds = teamLeaderUserIds;
    }

    public Set<Long> getTeamMemberExpertIds() {
        return teamMemberExpertIds;
    }

    public void setTeamMemberExpertIds(Set<Long> teamMemberExpertIds) {
        this.teamMemberExpertIds = teamMemberExpertIds;
    }

    public Set<Long> getTeamLeaderExpertIds() {
        return teamLeaderExpertIds;
    }

    public void setTeamLeaderExpertIds(Set<Long> teamLeaderExpertIds) {
        this.teamLeaderExpertIds = teamLeaderExpertIds;
    }

    public Integer getIdeaScore() {
        return ideaScore;
    }

    public void setIdeaScore(Integer ideaScore) {
        this.ideaScore = ideaScore;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getIntermediaryOrganizationId() {
        return intermediaryOrganizationId;
    }

    public void setIntermediaryOrganizationId(Long intermediaryOrganizationId) {
        this.intermediaryOrganizationId = intermediaryOrganizationId;
    }

    public String getImgChecksum() {
        return imgChecksum;
    }

    public void setImgChecksum(String imgChecksum) {
        this.imgChecksum = imgChecksum;
    }

    public Map<CommercializationOrganizationRelation, Set<Long>> getCommercializationOrganizationRelationMap() {
        return commercializationOrganizationRelationMap;
    }

    public void setCommercializationOrganizationRelationMap(Map<CommercializationOrganizationRelation, Set<Long>> commercializationOrganizationRelationMap) {
        this.commercializationOrganizationRelationMap = commercializationOrganizationRelationMap;
    }

    public Long getOwnerOrganizationId() {
        return ownerOrganizationId;
    }

    public void setOwnerOrganizationId(Long ownerOrganizationId) {
        this.ownerOrganizationId = ownerOrganizationId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
