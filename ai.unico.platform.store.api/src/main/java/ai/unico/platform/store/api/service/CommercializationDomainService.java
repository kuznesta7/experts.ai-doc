package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.CommercializationDomainDto;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface CommercializationDomainService {

    Long createCommercializationDomain(CommercializationDomainDto commercializationDomainDto, UserContext userContext);

    void updateCommercializationDomain(Long domainId, CommercializationDomainDto commercializationDomainDto, UserContext userContext);

    List<CommercializationDomainDto> findAvailableCommercializationDomains(boolean includeInactive);

}
