package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ProjectDocumentDto;
import ai.unico.platform.store.api.dto.ProjectDocumentPreviewDto;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface ProjectDocumentService {

    List<ProjectDocumentPreviewDto> findProjectDocuments(Long projectId);

    FileDto getProjectDocument(Long commercializationprojectId, Long fileId, UserContext userContext);

    Long addProjectDocument(Long projectId, ProjectDocumentDto documentDto, UserContext userContext);

    void deleteProjectDocument(Long projectId, Long fileId, UserContext userContext);

}
