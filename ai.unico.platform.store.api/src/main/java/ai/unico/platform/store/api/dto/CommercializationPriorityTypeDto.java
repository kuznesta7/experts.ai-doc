package ai.unico.platform.store.api.dto;

public class CommercializationPriorityTypeDto {

    private Long priorityTypeId;

    private String priorityTypeName;

    private String priorityTypeCode;

    private String priorityTypeDescription;

    private boolean deleted;

    public Long getPriorityTypeId() {
        return priorityTypeId;
    }

    public void setPriorityTypeId(Long priorityTypeId) {
        this.priorityTypeId = priorityTypeId;
    }

    public String getPriorityTypeName() {
        return priorityTypeName;
    }

    public void setPriorityTypeName(String priorityTypeName) {
        this.priorityTypeName = priorityTypeName;
    }

    public String getPriorityTypeCode() {
        return priorityTypeCode;
    }

    public void setPriorityTypeCode(String priorityTypeCode) {
        this.priorityTypeCode = priorityTypeCode;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getPriorityTypeDescription() {
        return priorityTypeDescription;
    }

    public void setPriorityTypeDescription(String priorityTypeDescription) {
        this.priorityTypeDescription = priorityTypeDescription;
    }
}
