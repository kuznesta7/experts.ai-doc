package ai.unico.platform.store.api.dto;

import java.util.ArrayList;
import java.util.List;

public class EvidenceItemExpertStatisticsDto {
    Long organizationId;
    private Long sum;
    List<Expert> experts = new ArrayList<>();

    public static class Expert {
        private Long expertId;
        private String name;
        private Long resultCount;

        public Long getExpertId() {
            return expertId;
        }

        public void setExpertId(Long expertId) {
            this.expertId = expertId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getResultCount() {
            return resultCount;
        }

        public void setResultCount(Long resultCount) {
            this.resultCount = resultCount;
        }
    }


    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public List<Expert> getExperts() {
        return experts;
    }

    public void setExperts(List<Expert> experts) {
        this.experts = experts;
    }

    public Long getSum() {
        return sum;
    }

    public void setSum(Long sum) {
        this.sum = sum;
    }
}
