package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.*;
import ai.unico.platform.store.api.enums.WidgetTab;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface EvidenceStatisticsService {

    EvidenceStatisticsHeaderDto findStatisticsHeader(Long organizationId);

    EvidenceExpertStatisticsDto findExpertStatistics(Long organizationId) throws IOException;

    EvidenceExpertVisitationStatisticsDto findExpertVisitationStatistics(Long organizationId, Integer page, Integer limit) throws IOException;

    EvidenceItemStatisticsDto findItemStatistics(Long organizationId, boolean verified) throws IOException;

    EvidenceSearchedExpertStatisticsDto findSearchedExpertStatistics(Long organizationId, Integer page, Integer limit) throws IOException;

    EvidenceSearchedKeywordStatisticsDto findSearchedKeywordStatistics(Long organizationId, Integer page, Integer limit);

    EvidenceProjectStatisticsDto findProjectStatistics(Long organizationId) throws IOException;

    EvidenceProjectStatisticsDto findProjectCumulativeStatistics(Long organizationId) throws IOException;

    EvidenceCommercializationStatisticsDto findCommercializationStatistics(Long organizationId) throws IOException;

    EvidenceOpportunityStatisticsDto findOpportunityStatistics(Long organizationId) throws IOException;

    EvidenceMemberStatisticsDto findMemberStatistics(Long organizationId);

    EvidenceBasicStatisticsDto findServiceEquipmentStatistics(Long organizationId) throws IOException;

    List<OrganizationSumFinancesDto> findUnitsProjectStatistics(Long organizationId, Integer year) throws IOException;

    List<YearCooperatingOrganizationsDto> findCooperatingOrganizations(Long organizationId, Integer yearFrom, Integer yearTo) throws IOException;

    EvidenceItemExpertStatisticsDto findItemExpertStatistics(Long organizationId, boolean verified) throws IOException;

    Map<String, Long> findWidgetInteractionHistory(Long organizationId, WidgetTab widgetTab);
}
