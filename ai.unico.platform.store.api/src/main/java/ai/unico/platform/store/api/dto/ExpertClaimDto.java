package ai.unico.platform.store.api.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ExpertClaimDto {

    private Long expertId;

    private String fullName;

    private Set<Long> organizationIds;

    private Set<Long> verifiedOrganizationIds;

    private List<ItemRegistrationDto> expertItemDtos = new ArrayList<>();

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public List<ItemRegistrationDto> getExpertItemDtos() {
        return expertItemDtos;
    }

    public void setExpertItemDtos(List<ItemRegistrationDto> expertItemDtos) {
        this.expertItemDtos = expertItemDtos;
    }

    public Set<Long> getVerifiedOrganizationIds() {
        return verifiedOrganizationIds;
    }

    public void setVerifiedOrganizationIds(Set<Long> verifiedOrganizationIds) {
        this.verifiedOrganizationIds = verifiedOrganizationIds;
    }
}
