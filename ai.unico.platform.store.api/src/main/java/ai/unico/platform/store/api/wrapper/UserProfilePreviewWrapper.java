package ai.unico.platform.store.api.wrapper;

import ai.unico.platform.store.api.dto.UserProfilePreviewDto;

import java.util.ArrayList;
import java.util.List;

public class UserProfilePreviewWrapper {

    private Long numberOfResults;

    private Integer limit;

    private Integer page;

    private Long searchId;

    private List<UserProfilePreviewDto> userProfilePreviews = new ArrayList<>();

    public Long getNumberOfResults() {
        return numberOfResults;
    }

    public void setNumberOfResults(Long numberOfResults) {
        this.numberOfResults = numberOfResults;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<UserProfilePreviewDto> getUserProfilePreviews() {
        return userProfilePreviews;
    }

    public void setUserProfilePreviews(List<UserProfilePreviewDto> userProfilePreviews) {
        this.userProfilePreviews = userProfilePreviews;
    }

    public Long getSearchId() {
        return searchId;
    }

    public void setSearchId(Long searchId) {
        this.searchId = searchId;
    }
}
