package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.InternalMessageDto;
import ai.unico.platform.store.api.dto.MessageDto;
import ai.unico.platform.store.api.filter.MessageFilter;
import ai.unico.platform.store.api.wrapper.MessageWrapper;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;

public interface MessageService {

    void sendInternalMessage(InternalMessageDto messageDto, UserContext userContext);

    void sendMessage(MessageDto messageDto, UserContext userContext, Long searchId) throws NonexistentEntityException;

    void sendMessageToOrganization(InternalMessageDto messageDto, UserContext userContext) throws NonexistentEntityException;

    void markMessageAsRead(Long messageId, UserContext userContext) throws NonexistentEntityException, UserUnauthorizedException;

    MessageWrapper getMessages(MessageFilter messageFilter, UserContext userContext);

    MessageDto findMessage(Long messageId, UserContext userContext) throws UserUnauthorizedException;
}
