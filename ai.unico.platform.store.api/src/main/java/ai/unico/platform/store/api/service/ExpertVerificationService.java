package ai.unico.platform.store.api.service;

import ai.unico.platform.util.model.UserContext;

import java.io.IOException;

public interface ExpertVerificationService {

    Long createUserFromExpertProfile(Long expertId, UserContext userContext) throws IOException;

}
