package ai.unico.platform.store.api.dto;

public class OrganizationProfileDetailsDto {

    private String organizationDescription;

    public String getOrganizationDescription() {
        return organizationDescription;
    }

    public void setOrganizationDescription(String organizationDescription) {
        this.organizationDescription = organizationDescription;
    }
}
