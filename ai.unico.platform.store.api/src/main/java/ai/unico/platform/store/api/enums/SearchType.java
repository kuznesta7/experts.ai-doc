package ai.unico.platform.store.api.enums;

public enum SearchType {
    EXPERT_FIELD, EXPERT_NAME, PROJECT_FIELD
}
