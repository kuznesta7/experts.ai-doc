package ai.unico.platform.store.api.dto;

public class CommercializationCategoryDto {

    private Long commercializationCategoryId;

    private String commercializationCategoryName;

    private String commercializationCategoryDescription;

    private String commercializationCategoryCode;

    private boolean deleted;

    public Long getCommercializationCategoryId() {
        return commercializationCategoryId;
    }

    public void setCommercializationCategoryId(Long commercializationCategoryId) {
        this.commercializationCategoryId = commercializationCategoryId;
    }

    public String getCommercializationCategoryName() {
        return commercializationCategoryName;
    }

    public void setCommercializationCategoryName(String commercializationCategoryName) {
        this.commercializationCategoryName = commercializationCategoryName;
    }

    public String getCommercializationCategoryCode() {
        return commercializationCategoryCode;
    }

    public void setCommercializationCategoryCode(String commercializationCategoryCode) {
        this.commercializationCategoryCode = commercializationCategoryCode;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getCommercializationCategoryDescription() {
        return commercializationCategoryDescription;
    }

    public void setCommercializationCategoryDescription(String commercializationCategoryDescription) {
        this.commercializationCategoryDescription = commercializationCategoryDescription;
    }
}
