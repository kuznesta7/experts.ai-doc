package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ItemRegistrationDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;

public interface ItemRegistrationService {

    Long registerItem(Long originalItemId, UserContext userContext) throws IOException, NonexistentEntityException;

    Long registerItem(ItemRegistrationDto itemRegistrationDto, boolean clean, UserContext userContext);

}
