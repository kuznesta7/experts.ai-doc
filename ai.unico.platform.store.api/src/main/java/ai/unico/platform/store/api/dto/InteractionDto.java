package ai.unico.platform.store.api.dto;

import java.sql.Timestamp;
import java.util.Date;

public class InteractionDto {

    protected Long user;
    protected Date date = new Timestamp(new Date().getTime());
    protected String sessionId;
    protected Long organization;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public Long getOrganization() {
        return organization;
    }

    public void setOrganization(Long organization) {
        this.organization = organization;
    }
}
