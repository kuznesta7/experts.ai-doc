package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ExpertSearchHistoryDto;
import ai.unico.platform.store.api.enums.ShareCodeType;
import ai.unico.platform.util.model.UserContext;

public interface ShareCodeService {

    String generateShareCode(ShareCodeType shareCodeType, String sharedEntityId, UserContext userContext);

    boolean verifySearchCode(String shareCode, ExpertSearchHistoryDto expertSearchHistoryDto);

    boolean verifyExpertProfileCode(String shareCode, String expertCode);

}

