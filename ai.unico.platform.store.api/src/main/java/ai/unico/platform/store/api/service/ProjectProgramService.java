package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ProjectProgramPreviewDto;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ProjectProgramService {

    Map<Long, Long> updateProgramsFromDWH();

    List<ProjectProgramPreviewDto> findPreviews(Set<Long> projectProgramIds);

    List<ProjectProgramPreviewDto> findPreviews();

    ProjectProgramPreviewDto findPreview(Long projectProgramId);

    Long createProjectProgram(String projectProgramName, String projectProgramDescription);

    void updateProjectProgram(Long projectProgram, String projectProgramName, String projectProgramDescription);

    List<ProjectProgramPreviewDto> autocompleteProjectProgram(String query);

}
