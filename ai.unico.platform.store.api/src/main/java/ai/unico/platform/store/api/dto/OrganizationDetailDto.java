package ai.unico.platform.store.api.dto;

import java.util.HashSet;
import java.util.Set;

public class OrganizationDetailDto {

    private Long organizationId;

    private String organizationName;

    private String organizationAbbrev;

    private Set<Long> organizationUnitIds = new HashSet<>();

    private Double organizationRank = 0D;

    private String organizationDescription;

    private String phone;

    private String email;

    private String street;

    private String city;

    private String web;

    private String countryName;

    private String countryCode;

    private String recombeePrivateToken;

    private String recombeeDbIdentifier;

    private String recombeeScenario;

    private String recombeePublicToken;

    private Boolean gdpr;

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationAbbrev() {
        return organizationAbbrev;
    }

    public void setOrganizationAbbrev(String organizationAbbrev) {
        this.organizationAbbrev = organizationAbbrev;
    }

    public Set<Long> getOrganizationUnitIds() {
        return organizationUnitIds;
    }

    public void setOrganizationUnitIds(Set<Long> organizationUnitIds) {
        this.organizationUnitIds = organizationUnitIds;
    }

    public Double getOrganizationRank() {
        return organizationRank;
    }

    public void setOrganizationRank(Double organizationRank) {
        this.organizationRank = organizationRank;
    }

    public String getOrganizationDescription() {
        return organizationDescription;
    }

    public void setOrganizationDescription(String organizationDescription) {
        this.organizationDescription = organizationDescription;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getRecombeePrivateToken() {
        return recombeePrivateToken;
    }

    public void setRecombeePrivateToken(String recombeePrivateToken) {
        this.recombeePrivateToken = recombeePrivateToken;
    }

    public String getRecombeeDbIdentifier() {
        return recombeeDbIdentifier;
    }

    public void setRecombeeDbIdentifier(String recombeeDbIdentifier) {
        this.recombeeDbIdentifier = recombeeDbIdentifier;
    }

    public String getRecombeeScenario() {
        return recombeeScenario;
    }

    public void setRecombeeScenario(String recombeeScenario) {
        this.recombeeScenario = recombeeScenario;
    }

    public String getRecombeePublicToken() {
        return recombeePublicToken;
    }

    public void setRecombeePublicToken(String recombeePublicToken) {
        this.recombeePublicToken = recombeePublicToken;
    }

    public Boolean getGdpr() {
        return gdpr;
    }

    public void setGdpr(Boolean gdpr) {
        this.gdpr = gdpr;
    }
}
