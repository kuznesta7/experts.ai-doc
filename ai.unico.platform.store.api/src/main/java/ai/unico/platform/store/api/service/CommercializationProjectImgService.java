package ai.unico.platform.store.api.service;

import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.model.UserContext;

public interface CommercializationProjectImgService {

    String saveImg(UserContext userContext, Long commercializationProjectId, FileDto fileDto);

    FileDto getImg(Long commercializationProjectId, String checkSum, String size);

}
