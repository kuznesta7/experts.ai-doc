package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ItemTypeDto;
import ai.unico.platform.store.api.dto.ItemTypeGroupDto;
import ai.unico.platform.store.api.dto.ItemTypeSearchDto;

import java.util.List;
import java.util.Set;

public interface ItemTypeService {

    List<ItemTypeDto> findItemTypes();

    List<ItemTypeDto> findItemTypes(boolean all);

    List<ItemTypeGroupDto> findItemTypeGroups();

    List<ItemTypeSearchDto> findItemTypesSearch();

    Set<Long> findItemTypeIds(Set<Long> groupIds);

}
