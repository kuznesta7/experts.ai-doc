package ai.unico.platform.store.api.dto;

import ai.unico.platform.util.enums.OrganizationRole;

import java.util.Date;

public class OrganizationLicenceDto {

    private Long organizationLicenceId;

    private Long organizationId;

    private Date validUntil;

    private OrganizationRole licenceRole;

    private Integer numberOfUserLicences;

    private boolean valid;

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    public OrganizationRole getLicenceRole() {
        return licenceRole;
    }

    public void setLicenceRole(OrganizationRole licenceRole) {
        this.licenceRole = licenceRole;
    }

    public Long getOrganizationLicenceId() {
        return organizationLicenceId;
    }

    public void setOrganizationLicenceId(Long organizationLicenceId) {
        this.organizationLicenceId = organizationLicenceId;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public Integer getNumberOfUserLicences() {
        return numberOfUserLicences;
    }

    public void setNumberOfUserLicences(Integer numberOfUserLicences) {
        this.numberOfUserLicences = numberOfUserLicences;
    }
}
