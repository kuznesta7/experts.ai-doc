package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.SearchSuggestionDto;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface SearchAutocompleteService {

    List<SearchSuggestionDto> findRelevantSuggestions(String query, Integer limit, boolean randomize, UserContext userContext);

}
