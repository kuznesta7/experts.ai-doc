package ai.unico.platform.store.api.service;

import ai.unico.platform.store.api.dto.ProjectDto;
import ai.unico.platform.store.api.dto.ProjectUserDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

public interface ProjectUserService {

    void saveUserToProject(ProjectUserDto projectUserDto, UserContext userContext) throws NonexistentEntityException;

    void removeUserFromProject(ProjectUserDto projectUserDto, UserContext userContext) throws NonexistentEntityException;

    void switchUsers(ProjectDto projectDto, UserContext userContext) throws NonexistentEntityException;

}
