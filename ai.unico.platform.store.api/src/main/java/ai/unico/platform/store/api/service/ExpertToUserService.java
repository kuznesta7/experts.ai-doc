package ai.unico.platform.store.api.service;

import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.Set;

public interface ExpertToUserService {

    Long expertToUser(Long expertId, UserContext userContext) throws IOException;

    Long expertToUser(String expertCode, UserContext userContext) throws IOException;

    Set<Long> expertsToUser(Set<String> expertCodes, UserContext userContext) throws IOException;

}
