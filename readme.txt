h1. Setup

# clone project: https://gitlab.com/fiserale/platform (git@gitlab.com:fiserale/platform.git)
# download and install stuff:
** download Apache Tomcat: https://tomcat.apache.org/download-80.cgi (recommended version 8.5.x; move tomcat home to /c/unico/apache-tomcat)
** download and install Postgresql: https://www.postgresql.org/download/ (recommended version 9.6.x)
** download and install pgAdmin https://www.pgadmin.org/
** download Elasticsearch: https://www.elastic.co/downloads/elasticsearch (recommended version 6.6.x)
** download Kibana: https://www.elastic.co/downloads/kibana
** others that you probably already have but don't forget:
** install Sevistate extension to Google Chrome browser (or use another rest request editor): https://chrome.google.com/webstore/detail/servistate-http-editor-re/mmdjghedkfbdhbjhmefbbgjaihmmhkeg
*** JDK v8: https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html; set JAVA_HOME environment variable
*** NodeJs: https://nodejs.org/en/; set M2 and M2_HOME environment variables
*** Apache Maven: https://maven.apache.org/
# add it to the path environment variable:
** C:\Users\{{user}}\AppData\Roaming\npm
** C:\Program Files\PostgreSQL\9.6\bin
** C:\Program Files\nodejs\
** %M2%
# download setup folder from the server (/opt/setup/)
** If you don't have access, ask some developer what to do. Server connection config is: <pre>Host unico
	HostName dev.unico.ai 
	User {{username}} 
	IdentityFile {{path to private ssh key}}</pre>
# setup database
** create databases _unico_store_, _unico_semantic_ and _unico_ume_
*** you can use psql commands <pre>psql -c "create database unico_store" -U postgres</pre>
** add databases to IDEA's data sources
** init databases by running init scripts and migrations
*** in the setup folder find _dump/dwh*.backup_ and _dump/ontology*.backup_ files and use them to restore unico_semantic database. Yes, both into single database. Both backup files will be restored into separated schemas so they shouldn't overwrite each other. You can use pg admin or pg_restore command. This action may take some time.
<pre>pg_restore -v -U postgres -d unico_semantic {{filename.backup}}</pre>
*** from unico-platform/ai.unico.platform.store/src/main/resources/sql run V1__init.sql and then all migrations in specified order under database unico_store
*** from unico-platform/ai.unico.platform.ume/src/main/resources/sql run V1__init_ume.sql and then all migrations in specified order under database unico_ume
*** after restore of both backup files run script V1__db_preparation.sql from unico-platform/ai.unico.platform.extdata/src/main/sql under database unico_semantic
# copy config files from the setup folder
** from tomcat folder copy everything into the tomcat home folder (/c/unico/apache-tomcat)
** from elasticsearch folder copy _elasticsearch.yml_ into elasticsearch-home/config folder
# setup server and IDE deployment
## in Run/Debug Configurations menu add a new configuration by clicking plus icon in the top left corner
## select Tomcat Server > Local
## click configure > Add > find path to the tomcat home > confirm
## switch tab to Deployment > Add > Artifact > select *:war; repeat it for all *:war (not *:war exploded) artifacts
## confirm
## open command line and go to unico-platform/ai.unico.platform.ui
## run command <pre>npm install</pre>; it may take some time
# run server
## build project with Maven; in IDEA select with ctrl clean+install and then press Run Maven Build
## when Maven Build is finished you can run Tomcat server by the top "play" or "bug" button
## run Elasticsearch (elasticsearch.bat in elasticsearch-home/bin folder)
## run command gulp libs
## run command gulp
# do initial setup
## run http://localhost:8000/ in your browser and register yourself into the platform
## insert a new row into unico_ume.user_role table; user_id=(your id - most probably 1) role='PORTAL_ADMIN'
## logout and login yourself again to get admin permissions
## go to Servistate Chrome plugin and run indexing of everything by creating PUT request on http://localhost:8000/ai.unico.platform.rest/api/internal/system-admin/search?clean=true
## it will take about 20 minutes to complete indexing
## enjoy EXPERTS.AI platform


