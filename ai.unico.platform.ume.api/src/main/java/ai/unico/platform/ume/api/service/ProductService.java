package ai.unico.platform.ume.api.service;

import ai.unico.platform.ume.api.dto.FeatureDto;
import ai.unico.platform.ume.api.dto.PaymentResponseDto;
import ai.unico.platform.ume.api.dto.ProductDto;
import ai.unico.platform.ume.api.exception.OrderAlreadyDeliveredException;
import ai.unico.platform.ume.api.exception.TrialExpiredException;
import ai.unico.platform.ume.api.exception.TrialUnavailableException;
import ai.unico.platform.ume.api.exception.WebPayUnavailableException;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface ProductService {

    List<ProductDto> findProducts(UserContext userContext);

    List<FeatureDto> findAllFeatures();

    PaymentResponseDto purchaseProduct(Long productVariantId, Long userId, UserContext userContext) throws WebPayUnavailableException, OrderAlreadyDeliveredException;

    void deliverProduct(Long orderId) throws OrderAlreadyDeliveredException;

    void tryProduct(Long productId, Long userId, UserContext userContext) throws TrialExpiredException, TrialUnavailableException;

}
