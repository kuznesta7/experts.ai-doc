package ai.unico.platform.ume.api.service;

public interface GoogleOauthService {

    String GOOGLE_OAUTH = "OAUTH_2_GOOGLE";

    String getApplicationUrl();

    String getLogin(String code) throws Exception;

}
