package ai.unico.platform.ume.api.service;

import ai.unico.platform.ume.api.dto.UserDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.OutdatedEntityException;
import ai.unico.platform.util.model.UserContext;

public interface UserPasswordService {

    UserDto doSetPassword(String email, String token, String newPassword, UserContext userContext) throws OutdatedEntityException, NonexistentEntityException;

    UserDto doSetPassword(String email, String newPassword, UserContext userContext) throws OutdatedEntityException, NonexistentEntityException;

    void doChangePassword(Long userId, String currentPassword, String newPassword, UserContext userContext) throws NonexistentEntityException;

    String createPasswordSetToken(Long userId);

    UserDto validatePasswordToken(String email, String token);

    void validateNewPassword(String newPassword);

}
