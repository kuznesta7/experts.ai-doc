package ai.unico.platform.ume.api.service;

import ai.unico.platform.ume.api.filter.AdminUserFilter;
import ai.unico.platform.ume.api.wrapper.AdminUserWrapper;

public interface UserAdministrationService {

    AdminUserWrapper findUsersForAdmin(AdminUserFilter adminUserFilter);

}
