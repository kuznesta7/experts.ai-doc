package ai.unico.platform.ume.api.service;

import ai.unico.platform.ume.api.dto.UserCredentialsDto;
import ai.unico.platform.ume.api.dto.UserDto;
import ai.unico.platform.ume.api.model.UserFilter;
import ai.unico.platform.util.enums.Role;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface UserService {

    Long createUmeUser(UserDto userDto, Boolean sendEmail, UserContext userContext) throws EntityAlreadyExistsException, NonexistentEntityException;

    void editUmeUser(UserDto userDto, UserContext userContext) throws EntityAlreadyExistsException, NonexistentEntityException;

    UserDto find(Long id) throws NonexistentEntityException;

    UserDto findByUsername(String username) throws NonexistentEntityException;

    UserDto findByEmail(String email);

    List<UserDto> find(UserFilter userFilter);

    List<UserDto> findUsersWithRole(Role role);

    UserDto authenticate(UserCredentialsDto userCredentialsDto) throws NonexistentEntityException;

}