package ai.unico.platform.ume.api.dto;

import java.util.Date;

public class PromoCodeDto {

    private Long productPromoId;

    private Long productId;

    private String promoCode;

    private Integer duration;

    private boolean oneTime;

    private boolean used;

    private Date validUntil;

    public Long getProductPromoId() {
        return productPromoId;
    }

    public void setProductPromoId(Long productPromoId) {
        this.productPromoId = productPromoId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public boolean isOneTime() {
        return oneTime;
    }

    public void setOneTime(boolean oneTime) {
        this.oneTime = oneTime;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }
}
