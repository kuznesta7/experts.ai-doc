package ai.unico.platform.ume.api.service;

import ai.unico.platform.ume.api.dto.PromoCodeDto;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface PromoService {

    Long createPromoCode(PromoCodeDto promoCodeDto, UserContext userContext);

    List<PromoCodeDto> findPromoCodes();

    PromoCodeDto findPromoCode(String promoCode);

    void deletePromoCode(Long promoCodeId, UserContext userContext);

    void usePromoCode(String promoCode, Long userId, UserContext userContext);

}
