package ai.unico.platform.ume.api.dto;

import ai.unico.platform.util.enums.Role;

import java.util.Date;

public class UserRoleDto {

    private Long userRoleId;

    private Role role;

    private Date validTo;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public Long getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(Long userRoleId) {
        this.userRoleId = userRoleId;
    }
}
