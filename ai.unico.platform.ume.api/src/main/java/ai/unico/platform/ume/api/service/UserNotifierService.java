package ai.unico.platform.ume.api.service;

public interface UserNotifierService {

    void sendResetPasswordLink(String email, String token);

    void sendRegistrationSetPasswordLink(String email, String token);

    void sendInvitationSetPasswordLink(String email, String passwordSetToken, String invitingUser);

}
