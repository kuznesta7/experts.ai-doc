package ai.unico.platform.ume.api.service;

import ai.unico.platform.util.exception.NonexistentEntityException;

public interface UserPasswordResetService {

    void applyForResetPassword(String email) throws NonexistentEntityException;

}
