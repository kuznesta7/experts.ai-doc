package ai.unico.platform.ume.api.service;

import ai.unico.platform.util.enums.Role;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface UserRoleService {

    Map<Long, List<Role>> findRolesForUsers(Collection<Long> userIds);

    List<Role> findRolesForUser(Long userId);

    List<Long> findUsersForRole(String id);

    void addRole(Long userId, Role role, UserContext userContext) throws NonexistentEntityException;

    void addRole(Long userId, Role role, Date validTo, boolean trial, UserContext userContext) throws NonexistentEntityException;

    void addRole(Long userId, Role role, Date validTo, Long promoCodeId, UserContext userContext) throws NonexistentEntityException;

    void removeRole(Long userId, Role role, UserContext userContext) throws NonexistentEntityException;

}
