package ai.unico.platform.ume.api.service;

public interface LinkedInOauthService {

    public static String LINKEDIN_OAUTH = "OAUTH_2_LINKEDIN";

    String getApplicationUrl();

    String getLogin(String code) throws Exception;

}
