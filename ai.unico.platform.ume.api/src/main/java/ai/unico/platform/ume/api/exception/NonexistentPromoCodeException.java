package ai.unico.platform.ume.api.exception;

import ai.unico.platform.util.exception.RuntimeExceptionWithMessage;

public class NonexistentPromoCodeException extends RuntimeExceptionWithMessage {
    public NonexistentPromoCodeException() {
        super("Entered promo code is not valid", "Invalid promo code");
    }
}
