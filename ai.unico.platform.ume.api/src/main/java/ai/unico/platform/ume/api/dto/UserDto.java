package ai.unico.platform.ume.api.dto;

import ai.unico.platform.util.enums.Role;

import java.util.ArrayList;
import java.util.List;

public class UserDto {

    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private String username;

    private boolean deleted;

    private boolean termsOfUseAccepted;

    private List<Role> userRolesPreset = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<Role> getUserRolesPreset() {
        return userRolesPreset;
    }

    public void setUserRolesPreset(List<Role> userRolesPreset) {
        this.userRolesPreset = userRolesPreset;
    }

    public boolean isTermsOfUseAccepted() {
        return termsOfUseAccepted;
    }

    public void setTermsOfUseAccepted(boolean termsOfUseAccepted) {
        this.termsOfUseAccepted = termsOfUseAccepted;
    }
}
