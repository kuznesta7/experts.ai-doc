package ai.unico.platform.ume.api.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ProductDto {

    private Long productId;

    private String productName;

    private String productDescription;

    private Integer roleTrialDuration;

    private Date trialUntil;

    private Date paidUntil;

    private boolean trialAvailable;

    private Map<Long, ProductFeatureDto> featureMap;

    private List<ProductVariantDto> productVariantDtos = new ArrayList<>();

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Integer getRoleTrialDuration() {
        return roleTrialDuration;
    }

    public void setRoleTrialDuration(Integer roleTrialDuration) {
        this.roleTrialDuration = roleTrialDuration;
    }

    public Date getTrialUntil() {
        return trialUntil;
    }

    public void setTrialUntil(Date trialUntil) {
        this.trialUntil = trialUntil;
    }

    public Date getPaidUntil() {
        return paidUntil;
    }

    public void setPaidUntil(Date paidUntil) {
        this.paidUntil = paidUntil;
    }

    public boolean isTrialAvailable() {
        return trialAvailable;
    }

    public void setTrialAvailable(boolean trialAvailable) {
        this.trialAvailable = trialAvailable;
    }

    public Map<Long, ProductFeatureDto> getFeatureMap() {
        return featureMap;
    }

    public void setFeatureMap(Map<Long, ProductFeatureDto> featureMap) {
        this.featureMap = featureMap;
    }

    public List<ProductVariantDto> getProductVariantDtos() {
        return productVariantDtos;
    }

    public void setProductVariantDtos(List<ProductVariantDto> productVariantDtos) {
        this.productVariantDtos = productVariantDtos;
    }

    public static class ProductVariantDto {

        private Long productVariantId;

        private Double price;

        private Integer rolePaidDuration;

        private String note;

        public Long getProductVariantId() {
            return productVariantId;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public void setProductVariantId(Long productVariantId) {
            this.productVariantId = productVariantId;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public Integer getRolePaidDuration() {
            return rolePaidDuration;
        }

        public void setRolePaidDuration(Integer rolePaidDuration) {
            this.rolePaidDuration = rolePaidDuration;
        }
    }

    public static class ProductFeatureDto {

        private String note;

        public ProductFeatureDto(String note) {
            this.note = note;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }
    }
}
