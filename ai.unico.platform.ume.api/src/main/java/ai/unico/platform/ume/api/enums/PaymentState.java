package ai.unico.platform.ume.api.enums;

public enum PaymentState {
    CREATED, PAYMENT_METHOD_CHOSEN, PAID, AUTHORIZED, CANCELED, TIMEOUTED, REFUNDED, PARTIALLY_REFUNDED
}
