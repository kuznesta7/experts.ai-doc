package ai.unico.platform.ume.api.exception;

import ai.unico.platform.util.exception.RuntimeExceptionWithMessage;

public class PromoExpiredException extends RuntimeExceptionWithMessage {
    public PromoExpiredException() {
        super("Promo code expired", "promo code expired");
    }
}
