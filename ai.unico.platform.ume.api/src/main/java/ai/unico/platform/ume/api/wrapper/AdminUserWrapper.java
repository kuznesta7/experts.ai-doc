package ai.unico.platform.ume.api.wrapper;

import ai.unico.platform.ume.api.dto.AdminUserDto;

import java.util.ArrayList;
import java.util.List;

public class AdminUserWrapper {

    private Long numberOfUsers;

    private Integer page;

    private Integer limit;

    private List<AdminUserDto> adminUserDtos = new ArrayList();

    public Long getNumberOfUsers() {
        return numberOfUsers;
    }

    public void setNumberOfUsers(Long numberOfUsers) {
        this.numberOfUsers = numberOfUsers;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public List<AdminUserDto> getAdminUserDtos() {
        return adminUserDtos;
    }

    public void setAdminUserDtos(List<AdminUserDto> adminUserDtos) {
        this.adminUserDtos = adminUserDtos;
    }
}
