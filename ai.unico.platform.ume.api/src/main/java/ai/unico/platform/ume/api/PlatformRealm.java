package ai.unico.platform.ume.api;

import ai.unico.platform.ume.api.dto.UserCredentialsDto;
import ai.unico.platform.ume.api.dto.UserDto;
import ai.unico.platform.ume.api.service.*;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Role;
import ai.unico.platform.util.exception.NonexistentEntityException;
import org.apache.catalina.realm.GenericPrincipal;
import org.apache.catalina.realm.RealmBase;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

public class PlatformRealm extends RealmBase {

    @Override
    public Principal authenticate(String login, String secret) {

        final UserService userService = ServiceRegistryUtil.getService(UserService.class);
        final UserRoleService userRoleService = ServiceRegistryUtil.getService(UserRoleService.class);
        final UserActivityService userActivityService = ServiceRegistryUtil.getService(UserActivityService.class);

        final UserDto userDto;

        if (GoogleOauthService.GOOGLE_OAUTH.equals(login)) {
            final GoogleOauthService service = ServiceRegistryUtil.getService(GoogleOauthService.class);
            try {
                final String email = service.getLogin(secret);
                userDto = userService.findByEmail(email);
                if (userDto == null)
                    return null;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        } else if (LinkedInOauthService.LINKEDIN_OAUTH.equals(login)) {
            final LinkedInOauthService service = ServiceRegistryUtil.getService(LinkedInOauthService.class);
            try {
                final String email = service.getLogin(secret);
                userDto = userService.findByEmail(email);
                if (userDto == null)
                    return null;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            UserDto foundUserDto = null;
            foundUserDto = userService.findByEmail(login);
            if (foundUserDto == null) {
                foundUserDto = userService.findByUsername(login);
            }
            if (foundUserDto == null) {
                return null;
            }

            try {
                userDto = userService.authenticate(UserCredentialsDto.createForPassword(foundUserDto.getId(), secret));
            } catch (NonexistentEntityException e) {
                return null;
            }

            if (userDto == null) {
                userActivityService.saveActivity(foundUserDto.getId(), UserActivityService.AUTHENTICATION_FAILED, "");
                return null;
            }
        }
        final List<Role> rolesForUser = userRoleService.findRolesForUser(userDto.getId());
        final List<String> rolesForUserString = new ArrayList<>();
        for (Role role : rolesForUser) {
            rolesForUserString.add(role.name());
        }

        userActivityService.saveActivity(userDto.getId(), UserActivityService.AUTHENTICATION_SUCCEEDED, "");
        return new GenericPrincipal(userDto.getId().toString(), null, rolesForUserString);
    }

    @Override
    protected String getName() {
        return getClass().getSimpleName();
    }

    @Override
    protected String getPassword(String s) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected Principal getPrincipal(String s) {
        throw new UnsupportedOperationException();
    }
}
