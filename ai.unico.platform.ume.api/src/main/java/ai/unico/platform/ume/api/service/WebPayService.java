package ai.unico.platform.ume.api.service;

import ai.unico.platform.ume.api.dto.PaymentRequestDto;
import ai.unico.platform.ume.api.dto.PaymentResponseDto;
import ai.unico.platform.ume.api.exception.OrderAlreadyDeliveredException;
import ai.unico.platform.ume.api.exception.WebPayUnavailableException;

public interface WebPayService {

    PaymentResponseDto createPayment(PaymentRequestDto paymentRequestDto) throws WebPayUnavailableException, OrderAlreadyDeliveredException;

    PaymentResponseDto checkStateForPayment(Long paymentId) throws WebPayUnavailableException, OrderAlreadyDeliveredException;

}
