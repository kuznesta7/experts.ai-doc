package ai.unico.platform.ume.api.exception;

import ai.unico.platform.util.exception.RuntimeExceptionWithMessage;

public class PromoAlreadyUsedException extends RuntimeExceptionWithMessage {
    public PromoAlreadyUsedException() {
        super("Promo code has been already used", "promo code used");
    }
}
