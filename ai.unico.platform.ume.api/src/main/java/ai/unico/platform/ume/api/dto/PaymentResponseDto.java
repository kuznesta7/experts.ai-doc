package ai.unico.platform.ume.api.dto;

import ai.unico.platform.ume.api.enums.PaymentState;

public class PaymentResponseDto {

    private Long paymentId;

    private String gwUrl;

    private PaymentState paymentState;

    private Long userOrderId;

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public String getGwUrl() {
        return gwUrl;
    }

    public void setGwUrl(String gwUrl) {
        this.gwUrl = gwUrl;
    }

    public PaymentState getPaymentState() {
        return paymentState;
    }

    public void setPaymentState(PaymentState paymentState) {
        this.paymentState = paymentState;
    }

    public Long getUserOrderId() {
        return userOrderId;
    }

    public void setUserOrderId(Long userOrderId) {
        this.userOrderId = userOrderId;
    }
}
