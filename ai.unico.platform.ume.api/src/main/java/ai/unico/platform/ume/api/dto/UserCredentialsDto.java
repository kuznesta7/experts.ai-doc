package ai.unico.platform.ume.api.dto;

public class UserCredentialsDto {

    public static final String TYPE_PASSWORD = "PASSWORD";
    public static final String TYPE_GOOGLE = "GOOGLE";
    public static final String TYPE_LINKEDIN = "LINKEDIN";

    public static UserCredentialsDto createForPassword(Long userId, String password) {
        final UserCredentialsDto userCredentialsDto = new UserCredentialsDto();
        userCredentialsDto.setUserId(userId);
        userCredentialsDto.setIdentification(userId.toString());
        userCredentialsDto.setSecret(password);
        userCredentialsDto.setType(TYPE_PASSWORD);
        return userCredentialsDto;
    }

    public static UserCredentialsDto createForGoogle(String userEmail, String password) {
        final UserCredentialsDto userCredentialsDto = new UserCredentialsDto();
        userCredentialsDto.setIdentification(userEmail);
        userCredentialsDto.setType(TYPE_GOOGLE);
        return userCredentialsDto;
    }

    private Long id;

    private Long userId;

    private String type;

    private String identification;

    private String secret = "";

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}