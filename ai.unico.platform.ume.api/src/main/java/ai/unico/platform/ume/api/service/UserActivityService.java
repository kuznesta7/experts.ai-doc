package ai.unico.platform.ume.api.service;

import java.util.Date;

public interface UserActivityService {

    String AUTHENTICATION_FAILED = "AUTHENTICATION_FAILED";
    String AUTHENTICATION_SUCCEEDED = "AUTHENTICATION_SUCCEEDED";
    String USER_LOGOUT = "USER_LOGOUT";
    String IMPERSONATION_STARTED = "IMPERSONATION_STARTED";
    String IMPERSONATION_ENDED = "IMPERSONATION_ENDED";

    void saveActivity(Long userId, String type, String note);

    Date getLastLogin(Long userId);
}
