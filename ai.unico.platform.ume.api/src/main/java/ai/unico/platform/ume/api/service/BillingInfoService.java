package ai.unico.platform.ume.api.service;

import ai.unico.platform.ume.api.dto.BillingInfoDto;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface BillingInfoService {

    Long createBillingInfo(BillingInfoDto billingInfoDto, UserContext userContext);

    Long updateBillingInfo(BillingInfoDto billingInfoDto, UserContext userContext);

    void deleteBillingInfo(Long billingInfoId, UserContext userContext);

    void setDefault(Long userId, Long billingInfoId, boolean favorite, UserContext userContext);

    List<BillingInfoDto> findBillingInfoForUser(Long userId);
}
