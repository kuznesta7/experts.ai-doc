package ai.unico.platform.recommendation.api.service;

import ai.unico.platform.recommendation.api.dto.RecombeeFilterDto;
import ai.unico.platform.recommendation.api.dto.RecommendationWrapperDto;
import ai.unico.platform.recommendation.api.enums.RecombeeScenario;
import ai.unico.platform.store.api.dto.OrganizationDetailDto;

public interface RecombeeRecommendationService {

    int RECOMMENDATION_BATCH_SIZE = 200;
    int SEARCH_BATCH_SIZE = 50;

    String[] textSearch(OrganizationDetailDto organizationDetailDto, String recombeeFilter, String scenario, RecommendationWrapperDto recommendationWrapperDto, String user, String query, Integer limit, Integer page) throws RuntimeException;

    void fillRecommendationWrapperDto(RecommendationWrapperDto recommendationWrapperDto, OrganizationDetailDto organizationDetailDto);

    String[] recommend(OrganizationDetailDto organizationDetailDto, RecombeeFilterDto filter, RecombeeScenario scenario, RecommendationWrapperDto recommendationWrapperDto) throws RuntimeException;

    String[] recommendNextItems(String recommId, Integer limit, OrganizationDetailDto organizationDetailDto, RecommendationWrapperDto recommendationWrapperDto) throws RuntimeException;

    int numberOfRecomms(String recommId);

}
