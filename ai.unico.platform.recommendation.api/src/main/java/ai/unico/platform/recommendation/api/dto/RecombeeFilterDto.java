package ai.unico.platform.recommendation.api.dto;

import ai.unico.platform.util.enums.Confidentiality;

import java.util.HashSet;
import java.util.Set;

public class RecombeeFilterDto {
    private String query;
    private Integer limit = 5;
    private Integer page = 1;
    private String recommId;
    private String user;


    private Long organizationId;
    private Set<Long> organizationIds;
    private Set<Long> suborganizationIds;
    private Set<Long> affiliatedOrganizationIds;
    private Set<String> expertCodes;
    private Integer yearTo;
    private boolean verifiedOnly;
    private boolean notVerifiedOnly;
    private boolean retired;
    private Set<Confidentiality> confidentiality = new HashSet<>();
    private Set<String> countryCodes = new HashSet<>();
    private boolean retirementIgnored = true;
    private Set<Long> resultTypeGroupIds = new HashSet<>();
    private Integer yearFrom;
    private Set<Long> participatingOrganizationIds = new HashSet<>();
    private boolean activeOnly;


    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getRecommId() {
        return recommId;
    }

    public void setRecommId(String recommId) {
        this.recommId = recommId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<Long> getSuborganizationIds() {
        return suborganizationIds;
    }

    public void setSuborganizationIds(Set<Long> suborganizationIds) {
        this.suborganizationIds = suborganizationIds;
    }

    public Set<Long> getAffiliatedOrganizationIds() {
        return affiliatedOrganizationIds;
    }

    public void setAffiliatedOrganizationIds(Set<Long> affiliatedOrganizationIds) {
        this.affiliatedOrganizationIds = affiliatedOrganizationIds;
    }

    public Set<String> getExpertCodes() {
        return expertCodes;
    }

    public void setExpertCodes(Set<String> expertCodes) {
        this.expertCodes = expertCodes;
    }

    public Integer getYearTo() {
        return yearTo;
    }

    public void setYearTo(Integer yearTo) {
        this.yearTo = yearTo;
    }

    public boolean isVerifiedOnly() {
        return verifiedOnly;
    }

    public void setVerifiedOnly(boolean verifiedOnly) {
        this.verifiedOnly = verifiedOnly;
    }

    public boolean isNotVerifiedOnly() {
        return notVerifiedOnly;
    }

    public void setNotVerifiedOnly(boolean notVerifiedOnly) {
        this.notVerifiedOnly = notVerifiedOnly;
    }

    public boolean isRetired() {
        return retired;
    }

    public void setRetired(boolean retired) {
        this.retired = retired;
    }

    public Set<Confidentiality> getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Set<Confidentiality> confidentiality) {
        this.confidentiality = confidentiality;
    }

    public Set<String> getCountryCodes() {
        return countryCodes;
    }

    public void setCountryCodes(Set<String> countryCodes) {
        this.countryCodes = countryCodes;
    }

    public boolean isRetirementIgnored() {
        return retirementIgnored;
    }

    public void setRetirementIgnored(boolean retirementIgnored) {
        this.retirementIgnored = retirementIgnored;
    }
    private Set<Long> projectProgramIds = new HashSet<>();
    private boolean restrictOrganization = false;
    private Long opportunityType;

    private Long jobType;


    public Set<Long> getResultTypeGroupIds() {
        return resultTypeGroupIds;
    }

    public void setResultTypeGroupIds(Set<Long> resultTypeGroupIds) {
        this.resultTypeGroupIds = resultTypeGroupIds;
    }

    public Integer getYearFrom() {
        return yearFrom;
    }

    public void setYearFrom(Integer yearFrom) {
        this.yearFrom = yearFrom;
    }

    public Set<Long> getParticipatingOrganizationIds() {
        return participatingOrganizationIds;
    }

    public void setParticipatingOrganizationIds(Set<Long> participatingOrganizationIds) {
        this.participatingOrganizationIds = participatingOrganizationIds;
    }

    public Set<Long> getProjectProgramIds() {
        return projectProgramIds;
    }

    public void setProjectProgramIds(Set<Long> projectProgramIds) {
        this.projectProgramIds = projectProgramIds;
    }

    public boolean isRestrictOrganization() {
        return restrictOrganization;
    }

    public void setRestrictOrganization(boolean restrictOrganization) {
        this.restrictOrganization = restrictOrganization;
    }

    public Long getOpportunityType() {
        return opportunityType;
    }

    public void setOpportunityType(Long opportunityType) {
        this.opportunityType = opportunityType;
    }

    public Long getJobType() {
        return jobType;
    }

    public void setJobType(Long jobType) {
        this.jobType = jobType;
    }

    public boolean isActiveOnly() {
        return activeOnly;
    }

    public void setActiveOnly(boolean activeOnly) {
        this.activeOnly = activeOnly;
    }
}
