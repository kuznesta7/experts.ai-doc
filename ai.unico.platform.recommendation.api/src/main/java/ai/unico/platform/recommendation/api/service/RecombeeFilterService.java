package ai.unico.platform.recommendation.api.service;

import ai.unico.platform.recommendation.api.dto.RecombeeFilterDto;
import ai.unico.platform.recommendation.api.enums.RecombeeScenario;

import java.util.List;

public interface RecombeeFilterService {

    String setupFilters(RecombeeFilterDto filter, RecombeeScenario scenario);

    List<String> setupExpertFilters(RecombeeFilterDto filter);

    List<String> setupItemFilters(RecombeeFilterDto filter);

    List<String> setupProjectFilters(RecombeeFilterDto filter);

    List<String> setupOpportunityFilters(RecombeeFilterDto filter);

}
