package ai.unico.platform.recommendation.api.enums;

import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.api.enums.WidgetType;

public enum RecombeeScenario {
    EXPERT("WidgetOverview", "ExpertSearch", WidgetType.ORGANIZATION_EXPERTS, IndexTarget.EXPERTS, "EX"),
    ITEM("WidgetOverviewItems", "ItemSearch", WidgetType.ORGANIZATION_ITEMS, IndexTarget.ITEMS, "IT"),
    OPPORTUNITY("WidgetOverviewOpportunities", "OpportunitySearch", WidgetType.ORGANIZATION_OPPORTUNITY, IndexTarget.OPPORTUNITY, "OP"),
    PROJECT("WidgetOverviewProjects", "ProjectSearch", WidgetType.ORGANIZATION_PROJECTS, IndexTarget.PROJECTS, "PR");

    public final String baseScenario;
    public final String searchScenario;
    public final WidgetType widgetType;
    public final IndexTarget target;
    public final String prefix;

    RecombeeScenario(String baseScenario, String searchScenario, WidgetType widgetType, IndexTarget target, String prefix) {
        this.baseScenario = baseScenario;
        this.searchScenario = searchScenario;
        this.widgetType = widgetType;
        this.target = target;
        this.prefix = prefix;
    }

    public String getBaseScenario() {
        return baseScenario;
    }

    public String getSearchScenario() {
        return searchScenario;
    }

    public WidgetType getWidgetType() {
        return widgetType;
    }

    public String getPrefix() {
        return prefix;
    }

    public IndexTarget getTarget() {
        return target;
    }

    public static RecombeeScenario findByTarget(IndexTarget target) {
        for (RecombeeScenario scenario : RecombeeScenario.values()) {
            if (scenario.target == target) {
                return scenario;
            }
        }
        return null;
    }
}
