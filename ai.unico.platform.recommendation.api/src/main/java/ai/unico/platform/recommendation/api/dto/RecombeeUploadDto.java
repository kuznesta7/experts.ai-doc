package ai.unico.platform.recommendation.api.dto;

import java.util.Map;

public class RecombeeUploadDto {

    String id;
    Map<String, Object> serializedObject;

    public RecombeeUploadDto(String id, Map<String, Object> serializedObject) {
        this.id = id;
        this.serializedObject = serializedObject;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Object> getSerializedObject() {
        return serializedObject;
    }
}
