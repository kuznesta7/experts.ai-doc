package ai.unico.platform.recommendation.api.interfaces;

import java.util.Map;
import java.util.Set;

public interface RecombeeSearchEntity {

    Map<String, Object> serialize();

    Set<Long> getOrganizationIds();

    String getId();

}
