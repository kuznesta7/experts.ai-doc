package ai.unico.platform.recommendation.api.service;

import ai.unico.platform.recommendation.api.dto.RecombeeReindexOptionDto;
import ai.unico.platform.recommendation.api.enums.RecombeeScenario;
import ai.unico.platform.recommendation.api.interfaces.RecombeeSearchEntity;
import ai.unico.platform.store.api.enums.IndexTarget;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public interface RecombeeManipulationService {

    int MAX_BATCH_SIZE = 1000;

    List<IndexTarget> TARGETS = new ArrayList<>(Arrays.asList(
            IndexTarget.EXPERTS,
            IndexTarget.ITEMS,
            IndexTarget.PROJECTS,
            IndexTarget.OPPORTUNITY));

    <T extends RecombeeSearchEntity> void uploadEntry(T item, String prefix);

    <T extends RecombeeSearchEntity> void uploadEntries(Collection<T> items, Long organizationId, String prefix);

    void deleteEntry(String id, Long organizationId, String prefix);

    boolean reindexTargetInOrganization(Long organizationId, RecombeeScenario scenario) throws IOException;

    List<RecombeeReindexOptionDto> findRecombeeReindexOptions();

    void resetColumns(Long organizationId);

    void resetDatabase(Long organizationId);

}
