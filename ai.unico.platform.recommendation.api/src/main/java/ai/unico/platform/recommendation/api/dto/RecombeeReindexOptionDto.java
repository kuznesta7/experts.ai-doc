package ai.unico.platform.recommendation.api.dto;

import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.enums.IndexTarget;

import java.util.List;

public class RecombeeReindexOptionDto {

    OrganizationBaseDto organization;
    List<IndexTarget> targets;
    List<IndexTarget> availableTargets;
    String recombeeIdentifier;


    public OrganizationBaseDto getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationBaseDto organization) {
        this.organization = organization;
    }

    public List<IndexTarget> getTargets() {
        return targets;
    }

    public void setTargets(List<IndexTarget> targets) {
        this.targets = targets;
    }

    public List<IndexTarget> getAvailableTargets() {
        return availableTargets;
    }

    public void setAvailableTargets(List<IndexTarget> availableTargets) {
        this.availableTargets = availableTargets;
    }

    public String getRecombeeIdentifier() {
        return recombeeIdentifier;
    }

    public void setRecombeeIdentifier(String recombeeIdentifier) {
        this.recombeeIdentifier = recombeeIdentifier;
    }
}
