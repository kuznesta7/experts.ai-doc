package ai.unico.platform.recommendation.api.enums;

public enum RecombeeColumn {
    CLAIMED_BY_ID("claimedById", "string"),
    CONFIDENTIALITY("confidentiality", "string"),
    COUNTRY_CODES("countryCodes", "set"),
    COUNTRY_CODE("countryCode", "string"),
    DESCRIPTION("description", "string"),
    ITEMS_DESCRIPTION("itemsDescription", "string"),
    FULL_NAME("fullName", "string"),
    IS_EXPERT("isExpert", "boolean"),
    IS_ITEM("isItem", "boolean"),
    IS_OPPORTUNITY("isOpportunity", "boolean"),
    IS_PROJECT("isProject", "boolean"),
    HIDDEN("hidden", "boolean"),
    DELETED("deleted", "boolean"),
    AFFILIATED_ORGANIZATION_IDS("affiliatedOrganizationIds", "set"),
    ITEM_CODE("itemCode", "string"),
    ID("entryId", "int"),
    EXPERT_CODES("expertCodes", "set"),
    KEYWORDS("keywords", "set"),
    ORGANIZATION_IDS("organizationIds", "set"),
    ORGANIZATION_ID("organizationId", "int"),
    OWNER_ORGANIZATION_ID("ownerOrganizationId", "int"),
    OWNER_USER_ID("ownerUserId", "int"),
    TYPE_ID("typeId", "int"),
    VERIFIED_ORGANIZATION_IDS("verifiedOrganizationIds", "set"),
    YEAR("year", "int"),
    JOB_TYPES("jobTypes", "set"),
    BENEFIT("benefit", "string"),
    HOME_OFFICE("homeOffice", "string"),
    JOB_START("jobStart", "timestamp"),
    LOCATION("location", "string"),
    WAGE("wage", "string"),
    ORGANIZATION_NAME("organizationName", "string"),
    ORGANIZATION_NAMES("organizationNames", "set"),
    PARTICIPATING_EXPERT_NAMES("participatingExpertNames", "set"),
    ORIGINAL_ID("originalId", "int"),
    NAME("name", "string"),
    LINK("link", "string"),
    RETIREMENT_DATE("retirementDate", "timestamp"),
    RETIRED_ORGANIZATION_IDS("retiredOrganizationIds", "set"),
    START_DATE("startDate", "timestamp"),
    END_DATE("endDate", "timestamp"),
    PROJECT_PROGRAM_ID("projectProgramId", "int"),
    MERGED_NAMES("mergedNames", "set"),
    MERGED_KEYWORDS("mergedKeywords", "set"),
    EXPERT_NAMES("expertNames", "set");


    private final String columnName;
    private final String type;

    RecombeeColumn(String columnName, String type) {
        this.columnName = columnName;
        this.type = type;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getType() {
        return type;
    }

    public static RecombeeColumn fromColumnName(String columnName) {
        for (RecombeeColumn column : RecombeeColumn.values()) {
            if (column.getColumnName().equals(columnName)) {
                return column;
            }
        }
        return null;
    }
}
