package ai.unico.platform.recommendation.api.dto;

public class RecommendationWrapperDto {
    private String recommId;
    private String publicKey;
    private String databaseName;
    private String testGroup;
    private Long recommendationCount;

    public void setRecommId(String recommId) {
        this.recommId = recommId;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getRecommId() {
        return recommId;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public String getTestGroup() {
        return testGroup;
    }

    public void setTestGroup(String testGroup) {
        this.testGroup = testGroup;
    }

    public Long getRecommendationCount() {
        return recommendationCount;
    }

    public void setRecommendationCount(Long recommendationCount) {
        this.recommendationCount = recommendationCount;
    }

}
