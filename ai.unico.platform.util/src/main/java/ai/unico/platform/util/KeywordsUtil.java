package ai.unico.platform.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class KeywordsUtil {

    public static List<String> convert(String keywordsText) {
        final List<String> bannedKeywords = Arrays.asList("not available", "-", "", "n/a");
        if (keywordsText != null && !keywordsText.isEmpty()) {
            final List<String> keywords = Arrays.asList(keywordsText.split(",[ ]*"));
            return keywords.stream().filter(k -> !bannedKeywords.contains(k.toLowerCase())).sorted(Comparator.comparingInt(String::length)).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public static List<String> sortKeywordList(List<String> keywords) {
        keywords.sort(Comparator.comparingInt(String::length));
        return keywords;
    }
}
