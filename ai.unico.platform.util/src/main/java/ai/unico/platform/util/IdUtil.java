package ai.unico.platform.util;

public class IdUtil {

    public static final String APPLICATION_STORAGE_PREFIX = "ST";
    public static final String DATA_WAREHOUSE_PREFIX = "DWH";

    public static String convertToExtDataCode(Long expertId) {
        if (expertId == null) return null;
        return DATA_WAREHOUSE_PREFIX + expertId;
    }

    public static String convertToIntDataCode(Long userId) {
        if (userId == null) return null;
        return APPLICATION_STORAGE_PREFIX + userId;
    }

    public static Long getIntDataId(String expertCode) {
        final String prefix = APPLICATION_STORAGE_PREFIX;
        if (expertCode != null && expertCode.startsWith(prefix)) {
            return Long.parseLong(expertCode.substring(prefix.length()));
        }
        return null;
    }

    public static Long getExtDataId(String expertCode) {
        final String prefix = DATA_WAREHOUSE_PREFIX;
        if (expertCode != null && expertCode.startsWith(prefix)) {
            return Long.parseLong(expertCode.substring(prefix.length()));
        }
        return null;
    }

    public static Boolean isIntId(String expertCode) {
        return expertCode.length() > APPLICATION_STORAGE_PREFIX.length() && APPLICATION_STORAGE_PREFIX.equals(expertCode.substring(0, APPLICATION_STORAGE_PREFIX.length()));
    }

    public static Boolean isExtId(String expertCode) {
        return expertCode.length() > DATA_WAREHOUSE_PREFIX.length() && DATA_WAREHOUSE_PREFIX.equals(expertCode.substring(0, DATA_WAREHOUSE_PREFIX.length()));
    }
}
