package ai.unico.platform.util.enums;

public enum Confidentiality {

    PUBLIC, CONFIDENTIAL, SECRET

}
