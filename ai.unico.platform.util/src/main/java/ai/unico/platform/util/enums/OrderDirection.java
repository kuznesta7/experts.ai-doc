package ai.unico.platform.util.enums;

public enum OrderDirection {

    ASC, DESC

}
