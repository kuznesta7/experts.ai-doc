package ai.unico.platform.util.exception;

public class EntityAlreadyExistsException extends RuntimeExceptionWithMessage {
    public EntityAlreadyExistsException(String secretMessage) {
        super(secretMessage);
    }

    public EntityAlreadyExistsException(Class entityClass) {
        super("ALREADY_EXIST_EXCEPTION", "Entity " + entityClass.getName() + " already exists");
    }

    public EntityAlreadyExistsException(String message, String secretMessage) {
        super(message, secretMessage);
    }
}
