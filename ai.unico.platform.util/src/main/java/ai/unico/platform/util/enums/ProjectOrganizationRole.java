package ai.unico.platform.util.enums;

public enum ProjectOrganizationRole {
    PROVIDER, PARTICIPANT
}
