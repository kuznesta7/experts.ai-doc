package ai.unico.platform.util.exception;

public class NonexistentEntityException extends RuntimeExceptionWithMessage {

    public NonexistentEntityException() {
        super("Entity does not exist");
    }

    public NonexistentEntityException(Class entityClass) {
        super("Entity " + entityClass.getName() + " does not exist");
    }

    public NonexistentEntityException(Class entityClass, String message) {
        super(message, "Entity " + entityClass.getName() + " does not exist");
    }

}
