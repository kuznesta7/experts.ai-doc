package ai.unico.platform.util.model;

import java.util.Collections;
import java.util.List;

public class PageInfo {

    private Integer pageSize;

    private Integer pageNumber;

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public List use(List list) {
        if (pageSize == null || pageNumber == null) return list;

        int fromIndex = (pageNumber - 1) * pageSize;

        final int size = list.size();

        if (fromIndex >= size) return Collections.emptyList();

        int toIndex = (pageSize) * pageNumber;
        if (toIndex >= size) toIndex = size;

        return list.subList(fromIndex, toIndex);
    }
}
