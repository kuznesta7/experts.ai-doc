package ai.unico.platform.util.exception;

public class InvalidParameterException extends RuntimeExceptionWithMessage {
    public InvalidParameterException(String secretMessage) {
        super(secretMessage);
    }

    public InvalidParameterException(String message, String secretMessage) {
        super(message, secretMessage);
    }
}
