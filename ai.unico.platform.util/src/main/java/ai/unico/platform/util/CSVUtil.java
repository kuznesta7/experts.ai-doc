package ai.unico.platform.util;

import ai.unico.platform.util.dto.FileDto;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class CSVUtil {
    public static String create(List<List<String>> rows, boolean quoteAll) {
        try {
            final StringBuilder stringBuilder = new StringBuilder();
            CSVFormat csvFormat = CSVFormat.EXCEL.withDelimiter(';');
            if (quoteAll) csvFormat = csvFormat.withQuoteMode(QuoteMode.ALL);
            final CSVPrinter csvPrinter = new CSVPrinter(stringBuilder, csvFormat);

            for (List<String> row : rows) {
                csvPrinter.printRecord(row);
            }

            return stringBuilder.toString();
        } catch (IOException e) {
            //TODO LOGGER
            e.printStackTrace(System.err);
            throw new RuntimeException();
        }
    }

    public static FileDto createFile(List<List<String>> input, String fileName, boolean quoteAll) {
        try {
            final String content = create(input, quoteAll);
            return new FileDto(content.getBytes("Windows-1250"), fileName + ".csv");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
}
