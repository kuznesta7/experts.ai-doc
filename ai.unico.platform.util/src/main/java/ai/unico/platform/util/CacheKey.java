package ai.unico.platform.util;


import java.util.Objects;

public class CacheKey {
    private Long entityId;
    private String size;
    private String type;
    private String checkSum;

    public CacheKey(Long entityId, String size) {
        this.entityId = entityId;
        this.size = size;
    }

    public CacheKey(Long entityId, String checkSum, String size) {
        this.entityId = entityId;
        this.checkSum = checkSum;
        this.size = size;
    }

    public CacheKey(Long entityId, String checkSum, String type, String size) {
        this.entityId = entityId;
        this.checkSum = checkSum;
        this.type = type;
        this.size = size;
    }

    public Long getEntityId() {
        return entityId;
    }

    public String getSize() {
        return size;
    }

    public String getCheckSum() {
        return checkSum;
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CacheKey cacheKey = (CacheKey) o;
        return Objects.equals(entityId, cacheKey.entityId) &&
                Objects.equals(size, cacheKey.size) &&
                Objects.equals(type, cacheKey.type) &&
                Objects.equals(checkSum, cacheKey.checkSum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entityId, size, checkSum, type);
    }
}
