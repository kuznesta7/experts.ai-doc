package ai.unico.platform.util.dto;

import ai.unico.platform.util.enums.OrganizationRole;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrganizationRolesDto {

    private List<OrganizationRoleDto> roles = new ArrayList<>();

    private Long organizationId;

    private String organizationName;

    private String organizationAbbrev;

    public List<OrganizationRoleDto> getRoles() {
        return roles;
    }

    public void setRoles(List<OrganizationRoleDto> roles) {
        this.roles = roles;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationAbbrev() {
        return organizationAbbrev;
    }

    public void setOrganizationAbbrev(String organizationAbbrev) {
        this.organizationAbbrev = organizationAbbrev;
    }

    public static class OrganizationRoleDto {
        private boolean valid;

        private Date validUntil;

        private OrganizationRole role;

        public boolean isValid() {
            return valid;
        }

        public void setValid(boolean valid) {
            this.valid = valid;
        }

        public Date getValidUntil() {
            return validUntil;
        }

        public void setValidUntil(Date validUntil) {
            this.validUntil = validUntil;
        }

        public OrganizationRole getRole() {
            return role;
        }

        public void setRole(OrganizationRole role) {
            this.role = role;
        }
    }
}
