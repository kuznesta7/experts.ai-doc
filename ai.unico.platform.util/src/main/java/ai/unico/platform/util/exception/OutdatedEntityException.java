package ai.unico.platform.util.exception;

public class OutdatedEntityException extends RuntimeExceptionWithMessage {
    public OutdatedEntityException(String secretMessage) {
        super(secretMessage);
    }

    public OutdatedEntityException(String message, String secretMessage) {
        super(message, secretMessage);
    }
}
