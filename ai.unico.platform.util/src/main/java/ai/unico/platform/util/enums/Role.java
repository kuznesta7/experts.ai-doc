package ai.unico.platform.util.enums;

public enum Role {
    PORTAL_ADMIN,
    COMMERCIALIZATION_ADMIN,
    PORTAL_MAINTAINER,
    USER,
    BASIC_USER,
    PREMIUM_USER,
    INFINITY_USER,
    DATA_CLEANER,
    DATA_IMPORTER,
    COMMERCIALIZATION_INVESTOR,
    COMPANY_EXPERT(true),
    ACADEMIC_EXPERT(true),
    INVESTOR(true);

    private boolean open = false;

    Role() {
    }

    Role(boolean open) {
        this.open = open;
    }

    public boolean isOpen() {
        return open;
    }
}
