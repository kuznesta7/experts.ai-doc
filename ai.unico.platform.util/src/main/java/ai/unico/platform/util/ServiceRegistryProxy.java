package ai.unico.platform.util;

import java.lang.reflect.*;

public class ServiceRegistryProxy {

    public static <T> T createProxy(final Class<T> class1) {
        final InvocationHandler h = new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                final T implementation = ServiceRegistryUtil.getService(class1);
                if (implementation == null) {
                    if (method.getName().equals("hashCode")) {
                        return 1;
                    }
                    throw new NoServiceInServiceRegistryException(class1, method);
                }

                try {
                    return method.invoke(implementation, args);
                } catch (InvocationTargetException e) {
                    if (e.getCause() == null) {
                        throw e;
                    }
                    throw e.getCause();
                } catch (UndeclaredThrowableException e) {
                    if (e.getCause() == null) {
                        throw e;
                    }
                    throw e.getCause();
                }
            }
        };
        final Object p = Proxy.newProxyInstance(class1.getClassLoader(), new Class[]{class1}, h);
        return (T) p;
    }
}
