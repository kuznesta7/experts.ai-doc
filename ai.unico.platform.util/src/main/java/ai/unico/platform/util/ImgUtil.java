package ai.unico.platform.util;

import ai.unico.platform.util.exception.RuntimeExceptionWithMessage;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.filters.Canvas;
import net.coobird.thumbnailator.geometry.Positions;
import net.coobird.thumbnailator.resizers.configurations.Antialiasing;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class ImgUtil {

    public static final int SIZE_XS = 40;
    public static final int SIZE_S = 100;
    public static final int SIZE_M = 250;
    public static final int SIZE_L = 500;
    public static final int SIZE_XL = 1920;

    public static int codeToSize(String code) {
        switch (code) {
            case "s":
                return SIZE_XS;
            case "S":
                return SIZE_S;
            case "M":
                return SIZE_M;
            case "L":
                return SIZE_L;
            case "XL":
                return SIZE_XL;
            default:
                throw new RuntimeException("unsupported size code " + code);
        }
    }

    public static void validateImageIsResizable(byte[] content) {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(content);
        try {
            final BufferedImage bufferedImage = ImageIO.read(byteArrayInputStream);
            if (bufferedImage == null) throw new RuntimeException("");
        } catch (Exception e) {
            final String message = "Forbidden image format";
            throw new RuntimeExceptionWithMessage(message, message);
        }
    }

    public static byte[] resize(byte[] image, Integer width, Integer height) {
        try {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            final Thumbnails.Builder<? extends InputStream> thumbnailBuilder = Thumbnails.of(new ByteArrayInputStream(image));
            if (height != null) thumbnailBuilder.height(height);
            if (width != null) thumbnailBuilder.width(width);
            if (width != null && height != null) {
                thumbnailBuilder.addFilter(new Canvas(width, height, Positions.CENTER));
            }
            thumbnailBuilder
                    .keepAspectRatio(true)
                    .outputFormat("png")
                    .imageType(BufferedImage.TYPE_INT_ARGB)
                    .antialiasing(Antialiasing.OFF)
                    .outputQuality(0.4F)
                    .toOutputStream(outputStream);
            return outputStream.toByteArray();
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return image;
        }
    }

    public static byte[] resize(byte[] image, int size) {
        return resize(image, size, size);
    }

}
