package ai.unico.platform.util.enums;

public enum ExpertSearchType {
    EXPERTISE, NAME
}
