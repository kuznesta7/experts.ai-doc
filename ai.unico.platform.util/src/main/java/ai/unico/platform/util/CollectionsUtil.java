package ai.unico.platform.util;

import java.text.Collator;
import java.util.*;
import java.util.function.Function;

public class CollectionsUtil {

    public static final int HIBERNATE_LIMIT = 30000;

    public static <T> List<List<T>> toSubCollection(Collection<T> ids, int limit) {
        final List<List<T>> result = new ArrayList<>();
        List<T> copy = new ArrayList<>(ids);
        while (true) {
            if (copy.size() > limit) {
                result.add(copy.subList(0, limit));
                copy = copy.subList(limit, copy.size());
            } else {
                result.add(copy);
                break;
            }
        }

        return result;
    }

    public static <T> List<List<T>> toSubCollectionForHibernate(Collection<T> ids) {
        return toSubCollection(ids, HIBERNATE_LIMIT);
    }

    public static <T> void sortWithCollator(final List<T> list, Function<? super T, String> function) {
        final Collator collator = Collator.getInstance(new Locale("cs"));
        Collections.sort(list, new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return collator.compare(function.apply(o1), function.apply(o2));
            }
        });
    }

    @SafeVarargs
    public static <T> Set<T> mergeCollections(Collection<T>... collections) {
        final Set<T> result = new HashSet<>();
        for (Collection<T> collection : collections) {
            result.addAll(collection);
        }
        return result;
    }
}
