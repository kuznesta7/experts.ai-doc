package ai.unico.platform.util.exception;

public class NoLoggedInUserException extends Throwable {

    public NoLoggedInUserException() {
        super("Authentication required");
    }
}
