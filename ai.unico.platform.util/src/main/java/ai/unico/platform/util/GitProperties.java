package ai.unico.platform.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

public class GitProperties {

    private static final Logger LOG = LoggerFactory.getLogger(GitProperties.class);

    private final static Properties properties = new Properties();

    static {
        try {
            properties.load(GitProperties.class.getClassLoader().getResourceAsStream("git.properties"));
        } catch (IOException e) {
            LOG.error("err", e);
        }
    }

    public static String getValue(String key) {
        return properties.getProperty(key);
    }

    public static String getLastCommitId() {
        return getValue("git.commit.id.abbrev");
    }

}