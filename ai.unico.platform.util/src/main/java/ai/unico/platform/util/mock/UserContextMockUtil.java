package ai.unico.platform.util.mock;

import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.dto.OrganizationRolesDto;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.enums.Role;
import ai.unico.platform.util.model.UserContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public class UserContextMockUtil {

    public static UserContext createBaseUserContext() {
        final List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        final Long userId = IdGeneratorUtil.generateId();
        final UserContext userContext = new UserContext(
                userId,
                IdUtil.convertToIntDataCode(userId),
                "Test",
                "Test",
                roles,
                new Date(),
                new ArrayList<>(),
                null,
                new HashSet<>(),
                true);
        userContext.getCountryOfInterestCodes().add("CZ");
        return userContext;
    }

    public static UserContext createOrganizationUserContextWithOrganizationRoles(Long organizationId, OrganizationRole... organizationRoles) {
        final UserContext userContext = createBaseUserContext();
        final OrganizationRolesDto organizationRolesDto = new OrganizationRolesDto();
        organizationRolesDto.setOrganizationId(organizationId);
        organizationRolesDto.setOrganizationName("Test organization " + organizationId);
        for (OrganizationRole organizationRole : organizationRoles) {
            final OrganizationRolesDto.OrganizationRoleDto organizationRoleDto = new OrganizationRolesDto.OrganizationRoleDto();
            organizationRoleDto.setValid(true);
            organizationRoleDto.setRole(organizationRole);
            organizationRolesDto.getRoles().add(organizationRoleDto);
            userContext.getRoles().addAll(organizationRole.getConnectedRoles());
        }
        userContext.getOrganizationRoles().add(organizationRolesDto);
        return userContext;
    }

}
