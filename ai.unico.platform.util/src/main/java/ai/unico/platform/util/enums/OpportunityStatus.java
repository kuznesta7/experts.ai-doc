package ai.unico.platform.util.enums;

public enum OpportunityStatus {
    NEGOTIATION, ACTIVE
}
