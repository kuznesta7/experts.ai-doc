package ai.unico.platform.util;

import java.lang.reflect.Method;

public class NoServiceInServiceRegistryException extends RuntimeException {

    public NoServiceInServiceRegistryException(Class clazz, Method method) {
        super("No Service implementation found in ServiceRegistry for class " + clazz.getName() + " method called " + method.getName());
    }
}
