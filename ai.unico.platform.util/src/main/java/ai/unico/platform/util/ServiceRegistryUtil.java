package ai.unico.platform.util;

import java.lang.ref.WeakReference;
import java.util.*;

public class ServiceRegistryUtil {

    private static Map<Class<?>, WeakReference<Object>> SERVICES = new HashMap<Class<?>, WeakReference<Object>>();

    private Map<Class<?>, Object> localServices = new WeakHashMap<Class<?>, Object>();

    public static <T> T getService(Class<T> aClass) {
        final WeakReference<Object> objectWeakReference = SERVICES.get(aClass);
        if (objectWeakReference == null) {
            return null;
        }
        return (T) objectWeakReference.get();
    }

    public final void setService(Object o) {
        for (Class<?> aClass : o.getClass().getInterfaces()) {
            final WeakReference<Object> weakReference = new WeakReference<>(o);
            SERVICES.put(aClass, weakReference);
            localServices.put(aClass, o);
        }
    }

    public final void setServices(List<Object> services) {
        for (Object service : services) {
            setService(service);
        }
    }

    public void destroy() {
        final Set<Map.Entry<Class<?>, Object>> entries = localServices.entrySet();
        for (Map.Entry<Class<?>, Object> entry : entries) {
            final Class<?> key = entry.getKey();
            final Object globalService = getService(key);
            final Object localService = entry.getValue();
            if (globalService == localService) {
                SERVICES.remove(key);
            }
        }
    }
}
