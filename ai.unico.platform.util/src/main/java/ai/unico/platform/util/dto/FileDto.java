package ai.unico.platform.util.dto;

public class FileDto {
    private byte[] content;
    private String name;

    public FileDto() {
    }

    public FileDto(byte[] content, String name) {
        this.content = content;
        this.name = name;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public byte[] getContent() {
        return content;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
