package ai.unico.platform.util.mock;

import java.util.HashSet;
import java.util.Set;

public class IdGeneratorUtil {

    private static final Set<Long> generatedIds = new HashSet<>();

    public static Long generateId() {
        final long id = (long) (Math.random() * 1000000);
        if (generatedIds.contains(id)) return generateId();
        generatedIds.add(id);
        return id;
    }
}
