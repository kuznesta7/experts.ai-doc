package ai.unico.platform.util.enums;

/**
 * @author Beka.Saldadze
 * Date: 21.03.2022
 */
public enum ResultCode {

    OK,
    ALREADY_EXISTS,
    NOT_FOUND

}
