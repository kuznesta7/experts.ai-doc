package ai.unico.platform.util.security;

import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.enums.Role;
import ai.unico.platform.util.model.UserContext;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class SecurityUtil {

    public static List<OrganizationRole> getRolesToSeeConfidentialityLevel(Confidentiality confidentiality) {
        switch (confidentiality) {
            case PUBLIC:
                return Collections.emptyList();
            case CONFIDENTIAL:
                return Arrays.asList(OrganizationRole.ORGANIZATION_VIEWER, OrganizationRole.ORGANIZATION_EDITOR);
            case SECRET:
                return Collections.singletonList(OrganizationRole.ORGANIZATION_EDITOR);
        }
        return null;
    }

    public static boolean hasOrganizationRoles(Long organizationId, UserContext userContext, OrganizationRole... requiredRoles) {
        final List<OrganizationRole> organizationRoles = Arrays.asList(requiredRoles);
        return hasOrganizationRoles(organizationId, userContext, organizationRoles);
    }

    public static boolean isPortalMaintainer(UserContext userContext) {
        return hasUserOneOfRoles(userContext, Role.PORTAL_ADMIN, Role.PORTAL_MAINTAINER);
    }

    public static boolean hasOrganizationRoles(Long organizationId, UserContext userContext, Collection<OrganizationRole> requiredRoles) {
        if (isPortalMaintainer(userContext)) return true;
        if (userContext == null) return false;
        if (requiredRoles == null || requiredRoles.isEmpty()) return true;
        return userContext.getOrganizationRoles().stream()
                .anyMatch(organization -> organizationId.equals(organization.getOrganizationId()) && organization.getRoles().stream()
                        .anyMatch(userRole -> userRole.isValid() && requiredRoles.stream().anyMatch(availableRole -> availableRole.equals(userRole.getRole()))));
    }

    public static boolean canDetailBeLoaded(UserContext userContext, Collection<Long> evidenceOrganizationIds, Long ownerOrganizationId, Confidentiality confidentiality) {
        if (Confidentiality.PUBLIC.equals(confidentiality)) {
            return true;
        }
        final List<OrganizationRole> requiredRoles = SecurityUtil.getRolesToSeeConfidentialityLevel(confidentiality);
        if (Confidentiality.SECRET.equals(confidentiality)) {
            if (!SecurityUtil.hasOrganizationRoles(ownerOrganizationId, userContext, requiredRoles)) {
                return false;
            }
        }
        if (Confidentiality.CONFIDENTIAL.equals(confidentiality)) {
            if (evidenceOrganizationIds.stream().noneMatch(oId -> SecurityUtil.hasOrganizationRoles(oId, userContext, requiredRoles))) {
                return false;
            }
        }
        return true;
    }

    private static boolean hasUserOneOfRoles(UserContext userContext, Role... roles) {
        if (userContext == null) return false;
        for (Role role : roles) {
            if (userContext.getRoles().contains(role)) return true;
        }
        return false;
    }

}
