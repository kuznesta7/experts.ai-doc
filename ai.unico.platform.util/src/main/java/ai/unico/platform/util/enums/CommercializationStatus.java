package ai.unico.platform.util.enums;

public enum CommercializationStatus {
    //only system statuses must be here
    DRAFT("draft"),
    INPUT("input");

    private String statusCode;

    CommercializationStatus(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }
}
