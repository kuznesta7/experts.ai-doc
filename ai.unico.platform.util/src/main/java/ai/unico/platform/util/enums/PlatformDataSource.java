package ai.unico.platform.util.enums;

public enum PlatformDataSource {

    STORE, DATA_WAREHOUSE

}
