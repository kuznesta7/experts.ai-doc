package ai.unico.platform.util.model;

import ai.unico.platform.util.dto.OrganizationRolesDto;
import ai.unico.platform.util.enums.Role;

import java.util.Date;
import java.util.List;
import java.util.Set;

public class UserContext {

    private Long userId;

    private String expertCode;

    private String username;

    private String fullName;

    private List<Role> roles;

    private Date lastLoginDate;

    private List<OrganizationRolesDto> organizationRoles;

    private Long impersonatorUserId;

    private boolean profileNeedsToBeCompleted = false;

    private Set<String> countryOfInterestCodes;

    private boolean canSearch;

    public UserContext(Long userId, String expertCode, String username, String fullName, List<Role> roles, Date lastLoginDate, List<OrganizationRolesDto> organizationRoles, Long impersonatorUserId, Set<String> countryOfInterestCodes,boolean canSearch) {
        this.userId = userId;
        this.expertCode = expertCode;
        this.username = username;
        this.fullName = fullName;
        this.roles = roles;
        this.lastLoginDate = lastLoginDate;
        this.impersonatorUserId = impersonatorUserId;
        this.organizationRoles = organizationRoles;
        this.countryOfInterestCodes = countryOfInterestCodes;
        this.canSearch = canSearch;
    }

    public Long getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public List<OrganizationRolesDto> getOrganizationRoles() {
        return organizationRoles;
    }

    public Long getImpersonatorUserId() {
        return impersonatorUserId;
    }

    public boolean isProfileNeedsToBeCompleted() {
        return profileNeedsToBeCompleted;
    }

    public void setProfileNeedsToBeCompleted(boolean profileNeedsToBeCompleted) {
        this.profileNeedsToBeCompleted = profileNeedsToBeCompleted;
    }

    public Set<String> getCountryOfInterestCodes() {
        return countryOfInterestCodes;
    }

    public String getExpertCode() {
        return expertCode;
    }

    public boolean isCanSearch() {
        return canSearch;
    }
}
