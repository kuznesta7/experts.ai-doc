package ai.unico.platform.util.enums;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public enum OrganizationRole {
    ORGANIZATION_EDITOR(false, Role.PREMIUM_USER),
    ORGANIZATION_VIEWER(false, Role.PREMIUM_USER),
    ORGANIZATION_USER(false, Role.BASIC_USER),
    ORGANIZATION_ADMIN(true);

    private Set<Role> connectedRoles = new HashSet<>();
    private boolean alwaysPresent;

    OrganizationRole(boolean alwaysPresent, Role... connectedRoles) {
        this.connectedRoles.addAll(Arrays.asList(connectedRoles));
        this.alwaysPresent = alwaysPresent;
    }

    public Set<Role> getConnectedRoles() {
        return connectedRoles;
    }

    public boolean isAlwaysPresent() {
        return alwaysPresent;
    }
}
