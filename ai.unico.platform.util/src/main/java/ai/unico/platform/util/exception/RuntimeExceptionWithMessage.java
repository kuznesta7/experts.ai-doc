package ai.unico.platform.util.exception;

public class RuntimeExceptionWithMessage extends RuntimeException {
    private final String secretMessage;

    public RuntimeExceptionWithMessage(String secretMessage) {
        super(secretMessage);
        this.secretMessage = secretMessage;
    }

    public RuntimeExceptionWithMessage(String message, String secretMessage) {
        super(message);
        this.secretMessage = secretMessage;
    }

    public String getSecretMessage() {
        return secretMessage;
    }
}
