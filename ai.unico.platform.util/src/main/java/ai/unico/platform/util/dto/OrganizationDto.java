package ai.unico.platform.util.dto;

import java.util.ArrayList;
import java.util.List;

public class OrganizationDto {
    private Long organizationId;
    private String organizationName;
    private String organizationAbbrev;
    private Long rank;
    private Long substituteOrganizationId;
    private List<OrganizationDto> organizationUnits = new ArrayList<>();
    private String countryCode;

    private Boolean gdpr;

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public List<OrganizationDto> getOrganizationUnits() {
        return organizationUnits;
    }

    public void setOrganizationUnits(List<OrganizationDto> organizationUnits) {
        this.organizationUnits = organizationUnits;
    }

    public Long getRank() {
        return rank;
    }

    public void setRank(Long rank) {
        this.rank = rank;
    }

    public Long getSubstituteOrganizationId() {
        return substituteOrganizationId;
    }

    public void setSubstituteOrganizationId(Long substituteOrganizationId) {
        this.substituteOrganizationId = substituteOrganizationId;
    }

    public String getOrganizationAbbrev() {
        return organizationAbbrev;
    }

    public void setOrganizationAbbrev(String organizationAbbrev) {
        this.organizationAbbrev = organizationAbbrev;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Boolean getGdpr() {
        return gdpr;
    }

    public void setGdpr(Boolean gdpr) {
        this.gdpr = gdpr;
    }
}
