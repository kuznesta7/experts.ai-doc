package ai.unico.platform.rest.controller.common;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.filter.ProjectFilter;
import ai.unico.platform.search.api.service.*;
import ai.unico.platform.search.api.wrapper.EvidenceExpertWrapper;
import ai.unico.platform.search.api.wrapper.EvidenceItemWrapper;
import ai.unico.platform.search.api.wrapper.ProjectWrapper;
import ai.unico.platform.store.api.dto.EvidenceItemStatisticsDto;
import ai.unico.platform.store.api.dto.EvidenceProjectStatisticsDto;
import ai.unico.platform.store.api.dto.EvidenceSearchedKeywordStatisticsDto;
import ai.unico.platform.store.api.dto.PublicProfileVisibilityDto;
import ai.unico.platform.store.api.enums.WidgetTab;
import ai.unico.platform.store.api.service.EvidenceStatisticsService;
import ai.unico.platform.store.api.service.OrganizationService;
import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static ai.unico.platform.rest.util.OrganizationUtil.findOrganizationSubset;

@RestController
@RequestMapping("common/organizations/{organizationId}")
public class PublicOrganizationProfileController {

    private final PublicOrganizationStatisticsService publicOrganizationStatisticsService = ServiceRegistryProxy.createProxy(PublicOrganizationStatisticsService.class);
    private final EvidenceStatisticsService evidenceStatisticsService = ServiceRegistryProxy.createProxy(EvidenceStatisticsService.class);
    private final EvidenceProjectService evidenceProjectService = ServiceRegistryProxy.createProxy(EvidenceProjectService.class);
    private final EvidenceItemService evidenceItemService = ServiceRegistryProxy.createProxy(EvidenceItemService.class);
    private final EvidenceExpertService evidenceExpertService = ServiceRegistryProxy.createProxy(EvidenceExpertService.class);
    private final OrganizationKeywordService organizationKeywordService = ServiceRegistryProxy.createProxy(OrganizationKeywordService.class);
    private final OrganizationStructureService organizationStructureService = ServiceRegistryProxy.createProxy(OrganizationStructureService.class);
    private final OrganizationService organizationService = ServiceRegistryProxy.createProxy(OrganizationService.class);


    @RequestMapping(method = RequestMethod.GET, path = "statistics")
    public PublicOrganizationStatisticsService.PublicOrganizationStatisticsDto findStatistics(@PathVariable Long organizationId,
                                                                                              @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!organizationService.hasPublicProfile(organizationId))
            return null;

        final Set<Long> childOrganizationIds = organizationStructureService.findChildOrganizationIds(organizationId, true);
        return publicOrganizationStatisticsService.getPublicOrganizationStatistics(childOrganizationIds);
    }

    @RequestMapping(method = RequestMethod.GET, path = "statistics/items")
    public EvidenceItemStatisticsDto findItemStatistics(@PathVariable Long organizationId,
                                                        @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!organizationService.hasGraphVisible(organizationId) && !userSecurityHelper.canEditOrganizationPublicProfile(organizationId))
            return null;

        return evidenceStatisticsService.findItemStatistics(organizationId, true);
    }

    @RequestMapping(method = RequestMethod.GET, path = "statistics/members")
    public Integer findMemberCount(@PathVariable Long organizationId){
        return organizationStructureService.findMemberOrganizations(organizationId).size();
    }

    @RequestMapping(method = RequestMethod.GET, path = "statistics/projects")
    public EvidenceProjectStatisticsDto findProjectStatistics(@PathVariable Long organizationId,
                                                              @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!organizationService.hasGraphVisible(organizationId) && !userSecurityHelper.canEditOrganizationPublicProfile(organizationId))
            return null;

        return evidenceStatisticsService.findProjectStatistics(organizationId);
    }


    @RequestMapping(method = RequestMethod.GET, path = "projects")
    public ProjectWrapper findProjects(@PathVariable Long organizationId,
                                       @RequestParam Integer page,
                                       @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!organizationService.hasProjectsVisible(organizationId) && !userSecurityHelper.canEditOrganizationPublicProfile(organizationId))
            return null;

        final ProjectFilter projectFilter = new ProjectFilter();
        projectFilter.setOrganizationId(organizationId);
        projectFilter.setOrganizationIds(findOrganizationSubset(organizationId, new HashSet<>(), false));
        projectFilter.setPage(page);
        if (!organizationService.hasProjectsVisible(organizationId) && !userSecurityHelper.canEditOrganizationPublicProfile(organizationId))
            projectFilter.setLimit(0);
        else
            projectFilter.setLimit(5);
        projectFilter.setConfidentiality(Collections.singleton(Confidentiality.PUBLIC));
//        projectFilter.setVerifiedOnly(true);
        return evidenceProjectService.findProjects(projectFilter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "items")
    public EvidenceItemWrapper findItems(@PathVariable Long organizationId,
                                         @RequestParam Integer page,
                                         @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!organizationService.hasOutcomesVisible(organizationId) && !userSecurityHelper.canEditOrganizationPublicProfile(organizationId))
            return null;

        final EvidenceFilter filter = new EvidenceFilter();
        filter.setOrganizationId(organizationId);
        filter.setOrganizationIds(findOrganizationSubset(organizationId, new HashSet<>(), false));
        filter.setPage(page);
        if (!organizationService.hasOutcomesVisible(organizationId) && !userSecurityHelper.canEditOrganizationPublicProfile(organizationId))
            filter.setLimit(0);
        else
            filter.setLimit(5);
        filter.setConfidentiality(Collections.singleton(Confidentiality.PUBLIC));
//        filter.setVerifiedOnly(true);
        return evidenceItemService.findAnalyticsItems(filter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "experts")
    public EvidenceExpertWrapper findExperts(@PathVariable Long organizationId,
                                             @RequestParam Integer page,
                                             @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!organizationService.hasExpertsVisible(organizationId) && !userSecurityHelper.canEditOrganizationPublicProfile(organizationId))
            return null;

        final EvidenceFilter filter = new EvidenceFilter();
        filter.setOrganizationId(organizationId);
        filter.setOrganizationIds(findOrganizationSubset(organizationId, new HashSet<>(), false));
        filter.setPage(page);
        if (!organizationService.hasExpertsVisible(organizationId) && !userSecurityHelper.canEditOrganizationPublicProfile(organizationId))
            filter.setLimit(0);
        else
            filter.setLimit(5);
//        filter.setVerifiedOnly(true);
        filter.setRetirementIgnored(false);
        return evidenceExpertService.findAnalyticsExperts(filter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "keywords")
    public Map<String, Long> findOrganizationKeywords(@PathVariable Long organizationId,
                                                      @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException {
        if (!organizationService.hasKeywordsVisible(organizationId) && !userSecurityHelper.canEditOrganizationPublicProfile(organizationId))
            return null;
        final Set<Long> childOrganizationIds = organizationStructureService.findChildOrganizationIds(organizationId, true);
        return organizationKeywordService.findOrganizationKeywords(childOrganizationIds);
    }

    @RequestMapping(method = RequestMethod.GET, path = "keywords/suggested")
    public Map<String, Long> findOrganizationSuggestedKeywords(@PathVariable Long organizationId,
                                                      @RequestParam WidgetTab widgetTab,
                                                      @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException {
        if (!organizationService.hasKeywordsVisible(organizationId) && !userSecurityHelper.canEditOrganizationPublicProfile(organizationId))
            return null;
        final Set<Long> childOrganizationIds = organizationStructureService.findChildOrganizationIds(organizationId, true);
        EvidenceSearchedKeywordStatisticsDto evidenceSearchedKeywordStatisticsDto = evidenceStatisticsService.findSearchedKeywordStatistics(organizationId, 1, 2000);
        Map<String, Long> searchHistory = evidenceSearchedKeywordStatisticsDto.getMostSearchedKeywords().stream()
                .collect(Collectors.toMap(EvidenceSearchedKeywordStatisticsDto.SearchedKeywordDto::getQuery, EvidenceSearchedKeywordStatisticsDto.SearchedKeywordDto::getSearchCount));
        Map<String, Long> widgetHistory = evidenceStatisticsService.findWidgetInteractionHistory(organizationId, widgetTab);
        Map<String, Long> result = organizationKeywordService.suggestKeywords(childOrganizationIds, searchHistory, widgetHistory, widgetTab);
        OrganizationKeywordService.removeStopWords(result);
        return result.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    @RequestMapping(method = RequestMethod.GET, path = "visibility")
    public PublicProfileVisibilityDto findVisibilities(@PathVariable Long organizationId,
                                                       @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canSeeOrganizationPublicProfile(organizationId) && !userSecurityHelper.canEditOrganizationPublicProfile(organizationId))
            throw new UserUnauthorizedException();
        return organizationService.getProfileVisibility(organizationId);
    }

    @RequestMapping(method = RequestMethod.POST, path = "visibility")
    public void updateVisibilities(@PathVariable Long organizationId,
                                   @RequestBody PublicProfileVisibilityDto profileVisibilityDto,
                                   @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canSeeOrganizationPublicProfile(organizationId) && !userSecurityHelper.canEditOrganizationPublicProfile(organizationId))
            throw new UserUnauthorizedException();
        profileVisibilityDto.setOrganizationId(organizationId);
        organizationService.setProfileVisibility(profileVisibilityDto);
    }
}
