package ai.unico.platform.rest.controller.common;


import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.UserProfileDto;
import ai.unico.platform.store.api.service.ClaimProfileService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NoLoggedInUserException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("common/profiles/experts")
public class ExpertToUserController {

    private final ClaimProfileService claimProfileService = ServiceRegistryProxy.createProxy(ClaimProfileService.class);


    @RequestMapping(value = "/{expertId}", method = RequestMethod.PUT)
    public Long expertToUserProfile(@PathVariable("expertId") Long expertId,
                                    @RequestBody UserProfileDto userProfileDto,
                                    UserContext userContext,
                                    @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, NoLoggedInUserException, IOException {
        if (userContext == null) throw new NoLoggedInUserException();
        userProfileDto.setClaimable(true);
        return claimProfileService.DWHExpertToSTUser(expertId, userProfileDto, userContext);
    }
}
