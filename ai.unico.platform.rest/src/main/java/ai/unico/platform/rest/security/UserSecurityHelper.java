package ai.unico.platform.rest.security;

import ai.unico.platform.store.api.dto.ItemDetailDto;
import ai.unico.platform.store.api.dto.ProjectDto;
import ai.unico.platform.store.api.enums.WidgetType;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.exception.NonexistentEntityException;

import java.io.IOException;
import java.util.Set;

public interface UserSecurityHelper {

    boolean isLoggedIn();

    boolean canEditSearchSettings();

    boolean canEditUserProfile(Long userId);

    boolean canEditProfile(String expertCode);

    boolean canClaimExpertProfile(Long expertProfileId) throws NonexistentEntityException, IOException;

    boolean canUnclaimExpertProfile(Long expertProfileId) throws NonexistentEntityException, IOException;

    boolean canEditProject(ProjectDto projectDto);

    boolean canCreateItem();

    boolean canConfirmItem(Long originalItemId) throws IOException;

    boolean canCreateOrganizationItem(Long organizationId);

    boolean canCreateUserItem(Long userId);

    boolean canSeeItem(Long itemId);

    boolean canSeeItemsWithConfidentiality(Long organizationId, Set<Confidentiality> confidentiality);

    boolean canEditItem(ItemDetailDto itemDetailDto);

    boolean canManageItemOrganization(Long itemId, Long organizationId);

    boolean canManageItemUser(Long itemId, Long userId);

    boolean canManageItemExpert(Long itemId, Long expertId);

    boolean canSeeAllItems();

    boolean canSeeAllExperts();

    boolean canSeeOrganizationEvidence(Long organizationId);

    boolean canCreateProject();

    boolean canCreateOrganizationProject(Long organizationId);

    boolean canEditProject(Long projectId);

    boolean canSeeProject(Long projectId);

    boolean canSeePrivateOrganizationProject(Long organizationId);

    boolean canEditPrivateOrganizationProject(Long projectId, Long organizationId);

    boolean canSeeOrganizationCollaborations(Long organizationId);

    boolean canSeeAllProjects();

    boolean canSeeUserSearchHistory(Long userId);

    boolean canUserEditSearchHistory(Long searchHistId);

    boolean canSeeUserFollowedProfiles(Long userId);

    boolean canManageUserFollowedProfiles(Long userId);

    boolean canSeeUserWorkspaces(Long userId);

    boolean canClaimUserProfile(Long userId) throws IOException;

    boolean canUnclaimUserProfile(Long userId) throws IOException;

    boolean canManageBillingInfo(Long userId);

    boolean canVerifyExpert();

    boolean canSearchWithFilters();

    boolean canExportSearch();

    boolean canPaginate();

    boolean canContactIndirectly();

    boolean canContactDirectly();

    boolean canManagePromoCodes();

    boolean canManageUserOrganization(Long userId, Long organizationId);

    boolean canVerifyUserOrganization(Long organizationId);

    boolean canVerifyItemOrganization(Long organizationId);

    boolean canAdminOrganizations();

    boolean canManageOrganizationLicences();

    boolean canManageOrganizationRoles(Long organizationId);

    boolean canImpersonate();

    boolean canAdminUsers();

    boolean canMangeOrganizationExperts(Long organizationId);

    boolean canCreateUser();

    boolean checkUserCanAddCountriesOfInterestForUser(Long userId, Set<String> countryOfInterestCodes);

    boolean canVerifyProjectOrganization(Long organizationId);

    boolean canConfirmProject();

    boolean canAccessCountries(Set<String> countryCodes);

    boolean canSetExpertRetired();

    boolean canManagePersonalNotes();

    boolean canSeeAllInvestProjects();

    boolean canSeeCommercializationProjectPreview(Long commercializationProjectId);

    boolean canSeeCommercializationProjectDetail(Long commercializationProjectId);

    boolean canSetCommercializationProjectStatus(Long commercializationProjectId, Long stateId);

    boolean canSeeAllCommercializationProjects();

    boolean canAdminCommercialization();

    boolean canEditCommercializationProject(Long commercializationProjectId);

    boolean canCreateOrganizationCommercializationProject(Long organizationId);

    boolean canSeeActiveCommercializationProjects();

    boolean canCreateCommercializationProject();

    boolean canClaimExpertForAnotherUser(Long expertId, Long userId);

    boolean canEditOrganizationStructure(Long organizationId);

    boolean canManageExpertClaims();

    boolean canSeeSearchStatistics();

    boolean canEditSearchStatistics();

    boolean canImportEvidence(Long organizationId);

    boolean canSeeSearchTranslations();

    boolean canEditSearchTranslations();

    boolean canDeleteImportEvidence(Long organizationId);

    boolean canSeeExpertsStatistics();

    boolean canSeeOrganizationPublicProfile(Long organizationId);

    boolean canEditOrganizationPublicProfile(Long organizationId);

    boolean canSeeWidget(Long organizationId, WidgetType widgetType);

    boolean canEditWidgetSettings(Long organizationId);

    boolean canSetSearchAllowance();
}
