package ai.unico.platform.rest.controller.common;

import ai.unico.platform.rest.factory.UserContextFactory;
import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;

@RestController
public class SessionController {

    @Autowired
    private UserContextFactory userContextFactory;

    @RequestMapping(path = {"internal/session", "common/session"}, method = RequestMethod.GET)
    public UserContext getSession(UserContext userContext, @ModelAttribute UserSecurityHelper userSecurityHelper) {
        return userContext;
    }

    @RequestMapping(path = "common/session", method = RequestMethod.DELETE)
    public void logout() throws ServletException {
        userContextFactory.logout();
    }
}
