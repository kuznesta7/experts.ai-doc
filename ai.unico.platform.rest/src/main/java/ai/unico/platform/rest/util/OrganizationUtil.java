package ai.unico.platform.rest.util;

import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.util.ServiceRegistryUtil;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class OrganizationUtil {
    public static Set<Long> findOrganizationSubset(Long mainOrganizationId, Set<Long> organizationIds, boolean restrictOrganization) {
        final OrganizationStructureService service = ServiceRegistryUtil.getService(OrganizationStructureService.class);
        final Set<Long> result = new HashSet<>();
        if (restrictOrganization) {
            result.add(mainOrganizationId);
            return result;
        }
        final Set<Long> organizationWithUnitIds = service.findChildOrganizationIds(mainOrganizationId, true);
        organizationWithUnitIds.add(mainOrganizationId);
        if (organizationIds.isEmpty()) {
            result.addAll(organizationWithUnitIds);
            return result;
        }
        result.addAll(organizationIds.stream().filter(organizationWithUnitIds::contains).collect(Collectors.toSet()));
        return result;
    }
}
