package ai.unico.platform.rest.controller.common;

import ai.unico.platform.search.api.dto.LecturePreviewDto;
import ai.unico.platform.search.api.service.LectureSearchService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("common/lecture")
public class LectureController {

    private LectureSearchService lectureSearchService = ServiceRegistryProxy.createProxy(LectureSearchService.class);

    @RequestMapping(method = RequestMethod.GET, path = "{lectureCode}")
    public LecturePreviewDto getThesisDetail(@PathVariable String lectureCode,
                                             UserContext userContext) throws NonexistentEntityException, IOException, UserUnauthorizedException {
        return lectureSearchService.getLecture(lectureCode);
    }
}
