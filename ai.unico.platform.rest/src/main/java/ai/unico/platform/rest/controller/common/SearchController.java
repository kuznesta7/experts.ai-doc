package ai.unico.platform.rest.controller.common;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.search.api.filter.*;
import ai.unico.platform.search.api.service.*;
import ai.unico.platform.search.api.wrapper.*;
import ai.unico.platform.store.api.dto.ExpertSearchHistoryDto;
import ai.unico.platform.store.api.dto.ExpertSearchHistoryPreviewDto;
import ai.unico.platform.store.api.service.CommercializationStatusService;
import ai.unico.platform.store.api.service.SearchHistoryService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Collections;
import java.util.Set;

@RestController
@RequestMapping("common/search")
public class SearchController {

    private final SearchHistoryService searchHistoryService = ServiceRegistryProxy.createProxy(SearchHistoryService.class);
    private final OrganizationSearchService organizationSearchService = ServiceRegistryProxy.createProxy(OrganizationSearchService.class);
    private final ThesisSearchService thesisSearchService = ServiceRegistryProxy.createProxy(ThesisSearchService.class);
    private final LectureSearchService lectureSearchService = ServiceRegistryProxy.createProxy(LectureSearchService.class);
    private final EvidenceExpertService evidenceExpertService = ServiceRegistryProxy.createProxy(EvidenceExpertService.class);
    private final EvidenceCommercializationService evidenceCommercializationService = ServiceRegistryProxy.createProxy(EvidenceCommercializationService.class);
    private final EvidenceItemService evidenceItemService = ServiceRegistryProxy.createProxy(EvidenceItemService.class);
    private final CommercializationStatusService commercializationStatusService = ServiceRegistryProxy.createProxy(CommercializationStatusService.class);
    private final EquipmentSearchService equipmentSearchService = ServiceRegistryProxy.createProxy(EquipmentSearchService.class);

    @RequestMapping(method = RequestMethod.GET, value = "experts")
    public EvidenceExpertWrapper doSearchExperts(@ModelAttribute EvidenceFilter evidenceFilter,
                                                 @RequestParam(required = false) Long searchId,
                                                 @RequestParam(required = false) String shareCode,
                                                 @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                 UserContext userContext) throws IOException, InstantiationException, IllegalAccessException, UserUnauthorizedException {
        evidenceFilter.setConfidentiality(Collections.singleton(Confidentiality.PUBLIC));
        checkUserCanSearch(userContext);
        if (searchId != null) {
            final ExpertSearchHistoryPreviewDto search = searchHistoryService.findSearch(searchId, shareCode, userContext);
            updateFilter(search, evidenceFilter);
        }
        final EvidenceExpertWrapper analyticsExperts = this.evidenceExpertService.findAnalyticsExperts(evidenceFilter);

        if (searchId == null && evidenceFilter.getQuery() != null && !evidenceFilter.getQuery().isEmpty()) {
            final ExpertSearchHistoryDto expertSearchHistoryDto = new ExpertSearchHistoryDto();
            expertSearchHistoryDto.setQuery(evidenceFilter.getQuery());
            expertSearchHistoryDto.setYearFrom(evidenceFilter.getYearFrom());
            expertSearchHistoryDto.setYearTo(evidenceFilter.getYearTo());
            expertSearchHistoryDto.setOrganizationIds(evidenceFilter.getOrganizationIds());
            expertSearchHistoryDto.setCountryCodes(evidenceFilter.getCountryCodes());
            expertSearchHistoryDto.setSearchType(evidenceFilter.getExpertSearchType());
            expertSearchHistoryDto.setResultTypeGroupIds(evidenceFilter.getResultTypeGroupIds());
            expertSearchHistoryDto.setPage(evidenceFilter.getPage());
            expertSearchHistoryDto.setLimit(evidenceFilter.getLimit());

            expertSearchHistoryDto.setResultDtos(analyticsExperts.transformAndGetResultDtos());
            expertSearchHistoryDto.setResultsFound(analyticsExperts.getNumberOfAllItems());

            searchHistoryService.saveSearchHistory(expertSearchHistoryDto, userContext);
        }
        return analyticsExperts;
    }

    @RequestMapping(method = RequestMethod.GET, value = "outcomes")
    public EvidenceItemWrapper doSearchOutcomes(@ModelAttribute EvidenceFilter evidenceFilter,
                                                @ModelAttribute UserSecurityHelper userSecurityHelper,
//                                          @RequestParam(required = false) String shareCode,
                                                UserContext userContext) throws IOException {
        evidenceFilter.setConfidentiality(Collections.singleton(Confidentiality.PUBLIC));
        return this.evidenceItemService.findAnalyticsItems(evidenceFilter);
    }

    @RequestMapping(method = RequestMethod.GET, value = "commercialization")
    public EvidenceCommercializationWrapper doSearchOutcomes(@ModelAttribute CommercializationFilter commercializationFilter,
                                                             @ModelAttribute UserSecurityHelper userSecurityHelper,
//                                          @RequestParam(required = false) String shareCode,
                                                             UserContext userContext) throws IOException {
        final Set<Long> statusIdsForSearch = commercializationStatusService.findStatusIdsForSearch();
        commercializationFilter.setCommercializationStatusIds(statusIdsForSearch);
        return this.evidenceCommercializationService.findEvidenceCommercialization(commercializationFilter);
    }

    @RequestMapping(method = RequestMethod.GET, value = "organizations")
    public OrganizationWrapper doSearchOrganizations(@ModelAttribute OrganizationSearchFilter organizationSearchFilter,
                                                     @ModelAttribute UserSecurityHelper userSecurityHelper,
//                                          @RequestParam(required = false) String shareCode,
                                                     UserContext userContext) throws IOException, InstantiationException, IllegalAccessException {
        return this.organizationSearchService.findOrganizations(organizationSearchFilter);
    }

    @RequestMapping(method = RequestMethod.GET, value = "thesis")
    public ThesisWrapper doSearchThesis(@ModelAttribute EvidenceFilter evidenceFilter,
                                        @ModelAttribute UserSecurityHelper userSecurityHelper) throws IOException {
        return this.thesisSearchService.findThesis(evidenceFilter, ExpertSearchResultFilter.Order.QUALITY);
    }

    @RequestMapping(method = RequestMethod.GET, value = "lectures")
    public LectureWrapper doSearchLectures(@ModelAttribute EvidenceFilter evidenceFilter,
                                           @ModelAttribute UserSecurityHelper userSecurityHelper) throws IOException {
        return this.lectureSearchService.findLecture(evidenceFilter, ExpertSearchResultFilter.Order.QUALITY);
    }
    @RequestMapping(method = RequestMethod.GET,value = "equipment")
    public EquipmentWrapper doSearchEquipment(@ModelAttribute EquipmentFilter equipmentFilter) throws IOException {
        return equipmentSearchService.findEquipment(equipmentFilter, ExpertSearchResultFilter.Order.QUALITY);
    }

    private EvidenceFilter updateFilter(ExpertSearchHistoryPreviewDto dto, EvidenceFilter evidenceFilter) {
        evidenceFilter.setQuery(dto.getQuery());
        evidenceFilter.setYearFrom(dto.getYearFrom());
        evidenceFilter.setYearTo(dto.getYearTo());
        evidenceFilter.setOrganizationIds(dto.getOrganizationIds());
        evidenceFilter.setCountryCodes(dto.getCountryCodes());
        evidenceFilter.setExpertSearchType(dto.getExpertSearchType());
        evidenceFilter.setResultTypeGroupIds(dto.getResultTypeGroupIds());
        return evidenceFilter;
    }

    private void checkUserCanSearch(UserContext userContext) throws UserUnauthorizedException {
        if (userContext == null || !userContext.isCanSearch()) {
            throw new UserUnauthorizedException();
        }
    }

}
