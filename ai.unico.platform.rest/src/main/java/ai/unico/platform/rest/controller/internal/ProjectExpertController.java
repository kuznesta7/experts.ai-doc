package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.ProjectExpertDto;
import ai.unico.platform.store.api.service.ProjectExpertService;
import ai.unico.platform.store.api.service.ProjectService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("internal")
public class ProjectExpertController {

    private ProjectExpertService projectExpertService = ServiceRegistryProxy.createProxy(ProjectExpertService.class);
    private ProjectService projectService = ServiceRegistryProxy.createProxy(ProjectService.class);

    @RequestMapping(method = RequestMethod.PUT, path = "/projects/{projectId}/experts/{expertId}")
    public void saveProjectExpert(@PathVariable Long projectId,
                                  @PathVariable Long expertId,
                                  @RequestParam(required = false) String label,
                                  UserContext userContext,
                                  @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NonexistentEntityException {
        if (!userSecurityHelper.canEditProject(projectId)) throw new UserUnauthorizedException();
        final ProjectExpertDto projectExpertDto = new ProjectExpertDto();
        projectExpertDto.setProjectId(projectId);
        projectExpertDto.setExpertId(expertId);
        projectExpertDto.setLabel(label);
        projectExpertService.saveExpertToProject(projectExpertDto, userContext);
        projectService.loadAndIndexProject(projectId);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/projects/{projectId}/experts/{expertId}")
    public void deleteProjectExpert(@PathVariable Long projectId, @PathVariable Long expertId,
                                    UserContext userContext,
                                    @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NonexistentEntityException {
        if (!userSecurityHelper.canEditProject(projectId)) throw new UserUnauthorizedException();
        final ProjectExpertDto projectExpertDto = new ProjectExpertDto();
        projectExpertDto.setProjectId(projectId);
        projectExpertDto.setExpertId(expertId);
        projectExpertService.removeExpertFromProject(projectExpertDto, userContext);
        projectService.loadAndIndexProject(projectId);
    }
}
