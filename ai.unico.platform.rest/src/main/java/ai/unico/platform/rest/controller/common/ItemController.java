package ai.unico.platform.rest.controller.common;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.search.api.dto.EvidenceItemPreviewWithExpertsDto;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.service.EvidenceItemService;
import ai.unico.platform.search.api.service.ItemLookupService;
import ai.unico.platform.store.api.dto.ItemDetailDto;
import ai.unico.platform.store.api.service.ItemRegistrationService;
import ai.unico.platform.store.api.service.ItemService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("common/items")
public class ItemController {

    private final ItemService itemService = ServiceRegistryProxy.createProxy(ItemService.class);

    @RequestMapping(method = RequestMethod.GET)
    public List<EvidenceItemPreviewWithExpertsDto> lookupItems(@ModelAttribute EvidenceFilter filter,
                                                               UserContext userContext,
                                                               @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, IOException {
        EvidenceItemService service = ServiceRegistryUtil.getService(EvidenceItemService.class);
        filter.setConfidentiality(Collections.singleton(Confidentiality.PUBLIC));
        filter.setLimit(10);
        return service.findAnalyticsItems(filter).getItemPreviewDtos();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Long createItem(@RequestBody ItemDetailDto itemDetailDto,
                           UserContext userContext,
                           @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, NonexistentEntityException {
        if (!userSecurityHelper.canCreateItem()) throw new UserUnauthorizedException();
        final ItemService itemService = ServiceRegistryUtil.getService(ItemService.class);
        Long itemId = itemService.createItem(itemDetailDto, userContext);
        itemService.loadAndIndexItem(itemId);
        return itemId;
    }

    @RequestMapping(method = RequestMethod.POST, path = "register/dwh/{originalItemId}")
    public Long registerItem(@PathVariable Long originalItemId,
                             UserContext userContext,
                             @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, NonexistentEntityException {
        if (!userSecurityHelper.canConfirmItem(originalItemId)) throw new UserUnauthorizedException();
        final ItemRegistrationService itemRegistrationService = ServiceRegistryUtil.getService(ItemRegistrationService.class);
        final Long itemId = itemRegistrationService.registerItem(originalItemId, userContext);
        itemService.loadAndIndexItem(itemId);
        return itemId;
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/{itemId}")
    public Long updateItem(@PathVariable("itemId") Long itemId,
                           @RequestBody ItemDetailDto itemDetailDto,
                           UserContext userContext,
                           @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException, IOException {
        if (itemDetailDto.getItemId() == null) {
            final ItemRegistrationService itemRegistrationService = ServiceRegistryUtil.getService(ItemRegistrationService.class);
            itemId = itemRegistrationService.registerItem(itemDetailDto.getOriginalItemId(), userContext);
            itemService.loadAndIndexItem(itemId);
        }
        itemDetailDto.setItemId(itemId);
        if (!userSecurityHelper.canEditItem(itemDetailDto)) throw new UserUnauthorizedException();
        ItemService service = ServiceRegistryUtil.getService(ItemService.class);
        service.editItem(itemDetailDto, userContext);
        itemService.loadAndIndexItem(itemId);
        return itemId;
    }

    @RequestMapping(method = RequestMethod.GET, path = "{itemCode}")
    public ItemDetailDto getItemDetail(@PathVariable String itemCode,
                                       UserContext userContext) throws NonexistentEntityException, IOException, UserUnauthorizedException {
        ItemLookupService service = ServiceRegistryUtil.getService(ItemLookupService.class);
        return service.findItemDetail(userContext, itemCode);
    }
}
