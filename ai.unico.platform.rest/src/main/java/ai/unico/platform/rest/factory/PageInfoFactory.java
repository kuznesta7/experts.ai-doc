package ai.unico.platform.rest.factory;

import ai.unico.platform.util.model.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class PageInfoFactory {

    @Autowired
    private HttpServletRequest httpServletRequest;

    @ModelAttribute
    public PageInfo getPageInfo() {
        try {
            final PageInfo pageInfo = new PageInfo();
            pageInfo.setPageNumber(Integer.parseInt(httpServletRequest.getParameter("pageNumber")));
            pageInfo.setPageSize(Integer.parseInt(httpServletRequest.getParameter("pageSize")));
            return pageInfo;
        } catch (Exception e) {
            return null;
        }
    }

}
