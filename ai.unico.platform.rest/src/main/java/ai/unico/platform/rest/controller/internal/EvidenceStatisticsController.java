package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.*;
import ai.unico.platform.store.api.service.EvidenceStatisticsService;
import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("internal/evidence/organizations/{organizationId}/statistics")
public class EvidenceStatisticsController {

    private final EvidenceStatisticsService evidenceStatisticsService = ServiceRegistryProxy.createProxy(EvidenceStatisticsService.class);
    private final OrganizationStructureService organizationStructureService = ServiceRegistryProxy.createProxy(OrganizationStructureService.class);

    @RequestMapping(method = RequestMethod.GET)
    public EvidenceStatisticsHeaderDto findAnalyticsStatisticsHeader(@PathVariable Long organizationId,
                                                                     UserContext userContext,
                                                                     @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        return evidenceStatisticsService.findStatisticsHeader(organizationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/experts/searched")
    public EvidenceSearchedExpertStatisticsDto findSearchedExpertStatistics(@PathVariable Long organizationId,
                                                                            @RequestParam(required = false, defaultValue = "1") Integer page,
                                                                            @RequestParam(required = false, defaultValue = "10") Integer limit,
                                                                            UserContext userContext,
                                                                            @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        return evidenceStatisticsService.findSearchedExpertStatistics(organizationId, page, limit);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/experts/visited")
    public EvidenceExpertVisitationStatisticsDto findExpertVisitationStatistics(@PathVariable Long organizationId,
                                                                                @RequestParam(required = false, defaultValue = "1") Integer page,
                                                                                @RequestParam(required = false, defaultValue = "10") Integer limit,
                                                                                UserContext userContext,
                                                                                @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        return evidenceStatisticsService.findExpertVisitationStatistics(organizationId, page, limit);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/keywords")
    public EvidenceSearchedKeywordStatisticsDto findKeywordStatistics(@PathVariable Long organizationId,
                                                                      @RequestParam(required = false, defaultValue = "1") Integer page,
                                                                      @RequestParam(required = false, defaultValue = "10") Integer limit,
                                                                      UserContext userContext,
                                                                      @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        return evidenceStatisticsService.findSearchedKeywordStatistics(organizationId, page, limit);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/experts")
    public EvidenceExpertStatisticsDto findExpertStatistics(@PathVariable Long organizationId,
                                                            UserContext userContext,
                                                            @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        return evidenceStatisticsService.findExpertStatistics(organizationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/items")
    public EvidenceItemStatisticsDto findItemStatistics(@PathVariable Long organizationId,
                                                        UserContext userContext,
                                                        @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        return evidenceStatisticsService.findItemStatistics(organizationId, false);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/projects")
    public EvidenceProjectStatisticsDto findProjectStatistics(@PathVariable Long organizationId,
                                                              UserContext userContext,
                                                              @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        return evidenceStatisticsService.findProjectStatistics(organizationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/commercialization")
    public EvidenceCommercializationStatisticsDto findCommercializationStatistics(@PathVariable Long organizationId,
                                                              UserContext userContext,
                                                              @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        return evidenceStatisticsService.findCommercializationStatistics(organizationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/opportunity")
    public EvidenceOpportunityStatisticsDto findOpportunityStatistics(@PathVariable Long organizationId,
                                                                                  UserContext userContext,
                                                                                  @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        return evidenceStatisticsService.findOpportunityStatistics(organizationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/service")
    public EvidenceBasicStatisticsDto findServiceEquipmentStatistics(@PathVariable Long organizationId,
                                                                      UserContext userContext,
                                                                      @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        return evidenceStatisticsService.findServiceEquipmentStatistics(organizationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/projects/cumulative")
    public EvidenceProjectStatisticsDto findProjectCumulativeStatistics(@PathVariable Long organizationId,
                                                              UserContext userContext,
                                                              @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        return evidenceStatisticsService.findProjectCumulativeStatistics(organizationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/members")
    public EvidenceMemberStatisticsDto findMemberCount(@PathVariable Long organizationId){
        return evidenceStatisticsService.findMemberStatistics(organizationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/innovationIndex")
    public EvidenceItemExpertStatisticsDto findItemExpertStatistics(@PathVariable Long organizationId,
                                                                    @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        return evidenceStatisticsService.findItemExpertStatistics(organizationId, true);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/unitsFinances")
    public List<OrganizationSumFinancesDto> findUnitsFinances(@PathVariable Long organizationId,
                                                              @RequestParam(required = false) Integer year,
                                                              @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        if (year == null)
            year = LocalDate.now().getYear();
        return evidenceStatisticsService.findUnitsProjectStatistics(organizationId, year);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/cooperatingOrganizations")
    public List<YearCooperatingOrganizationsDto> findCooperatingOrganizations(@PathVariable Long organizationId,
                                                                              @RequestParam Integer yearFrom,
                                                                              @RequestParam Integer yearTo,
                                                                              @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        return evidenceStatisticsService.findCooperatingOrganizations(organizationId, yearFrom, yearTo);
    }
}
