package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.recommendation.api.dto.RecombeeReindexOptionDto;
import ai.unico.platform.recommendation.api.enums.RecombeeScenario;
import ai.unico.platform.recommendation.api.service.RecombeeManipulationService;
import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.search.api.service.*;
import ai.unico.platform.store.api.dto.IndexHistoryDto;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.api.service.IndexHistoryService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("internal/index")
public class FillDataController {

    private final OntologyIndexService ontologyService = ServiceRegistryProxy.createProxy(OntologyIndexService.class);
    private final ExpertIndexService expertIndexService = ServiceRegistryProxy.createProxy(ExpertIndexService.class);
    private final ItemIndexService itemIndexService = ServiceRegistryProxy.createProxy(ItemIndexService.class);
    private final OrganizationIndexService organizationIndexService = ServiceRegistryProxy.createProxy(OrganizationIndexService.class);
    private final ProjectIndexService projectIndexService = ServiceRegistryProxy.createProxy(ProjectIndexService.class);
    private final ExpertEvaluationService expertEvaluationService = ServiceRegistryProxy.createProxy(ExpertEvaluationService.class);
    private final CommercializationProjectIndexService commercializationProjectIndexService = ServiceRegistryProxy.createProxy(CommercializationProjectIndexService.class);
    private final ThesisIndexService thesisIndexService = ServiceRegistryProxy.createProxy(ThesisIndexService.class);
    private final LectureIndexService lectureIndexService = ServiceRegistryProxy.createProxy(LectureIndexService.class);
    private final EquipmentIndexService equipmentIndexService = ServiceRegistryProxy.createProxy(EquipmentIndexService.class);
    private final IndexHistoryService indexHistoryService = ServiceRegistryProxy.createProxy(IndexHistoryService.class);
    private final ItemAffiliationService itemAffiliationService = ServiceRegistryProxy.createProxy(ItemAffiliationService.class);
    private final OpportunityIndexService opportunityIndexService = ServiceRegistryProxy.createProxy(OpportunityIndexService.class);
    private final RecombeeManipulationService recombeeManipulationService = ServiceRegistryProxy.createProxy(RecombeeManipulationService.class);

    private final Set<IndexTarget> targetsInProgress = new HashSet<>();

    @RequestMapping(method = RequestMethod.GET)
    List<IndexHistoryDto> find(@ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditSearchSettings()) throw new UserUnauthorizedException();
        return indexHistoryService.findLatest();
    }

    @RequestMapping(path = "recombee", method = RequestMethod.GET)
    List<RecombeeReindexOptionDto> findRecombee(@ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditSearchSettings()) throw new UserUnauthorizedException();
        return recombeeManipulationService.findRecombeeReindexOptions();
    }

    @RequestMapping(path = "recombee", method = RequestMethod.PUT)
    void reindexRecombee(@RequestParam(required = false) Integer limit,
                         @RequestParam(required = false) boolean clean,
                         @RequestParam Long organizationId,
                         @RequestParam List<IndexTarget> targets,
                         @ModelAttribute UserSecurityHelper userSecurityHelper,
                         UserContext userContext) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditSearchSettings()) throw new UserUnauthorizedException();
        if (limit == null) limit = 1000;
        if (clean)
            recombeeManipulationService.resetColumns(organizationId);
        for (IndexTarget indexTarget: targets) {
            try {
                if (! recombeeManipulationService.reindexTargetInOrganization(organizationId, RecombeeScenario.findByTarget(indexTarget)))
                    System.out.println("Failed to reindex " + indexTarget + " in organization " + organizationId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @RequestMapping(path = "recombee", method = RequestMethod.DELETE)
    void deleteRecombee(@RequestParam Long organizationId,
                        @ModelAttribute UserSecurityHelper userSecurityHelper,
                        UserContext userContext) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditSearchSettings()) throw new UserUnauthorizedException();
        recombeeManipulationService.resetDatabase(organizationId);
    }

    @RequestMapping(method = RequestMethod.PUT)
    void reindexAll(@RequestParam(required = false) Integer limit,
                    @RequestParam(required = false) boolean clean,
                    @RequestParam List<IndexTarget> targets,
                    @ModelAttribute UserSecurityHelper userSecurityHelper,
                    UserContext userContext) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditSearchSettings()) throw new UserUnauthorizedException();
        if (limit == null) limit = 20000;
        for (IndexTarget target : targets) {
            if (targetsInProgress.contains(target)) continue;
            targetsInProgress.add(target);
            try {
                switch (target) {
                    case ITEMS:
                        itemIndexService.reindexItems(10000, clean, "ALL", userContext);
                        break;
                    case EXPERTS:
                        expertIndexService.reindexExperts(limit, clean, userContext);
                        break;
                    case ORGANIZATIONS:
                        organizationIndexService.reindexOrganizations(limit, clean, userContext);
                        break;
                    case ONTOLOGY:
                        ontologyService.reindexOntology(userContext);
                        break;
                    case PROJECTS:
                        projectIndexService.reindexProjects(10000, clean, "ALL", userContext);
                        break;
                    case COMMERCIALIZATION_PROJECTS:
                        commercializationProjectIndexService.reindexInvestmentProjects(1000, userContext);
                        break;
                    case EXPERT_EVALUATION:
                        expertEvaluationService.evaluateExperts(1000, userContext);
                        break;
                    case ITEM_ASSOCIATION:
                        itemAffiliationService.recalculateItems(1000, userContext);
                        break;
                    case THESIS:
                        thesisIndexService.reindexThesis(limit, clean, "ALL", userContext);
                        break;
                    case LECTURES:
                        lectureIndexService.reindexLectures(limit, clean, "ALL", userContext);
                        break;
                    case EQUIPMENT:
                        equipmentIndexService.reindexEquipment(limit, clean, "ALL", userContext);
                        break;
                    case OPPORTUNITY:
                        opportunityIndexService.reindexOpportunity(limit, clean, "ALL", userContext);
                        break;

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        targets.forEach(targetsInProgress::remove);
    }

}
