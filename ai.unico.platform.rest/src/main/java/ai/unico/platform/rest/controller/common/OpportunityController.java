package ai.unico.platform.rest.controller.common;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.search.api.dto.OpportunityPreviewDto;
import ai.unico.platform.search.api.filter.OpportunityFilter;
import ai.unico.platform.search.api.service.OpportunitySearchService;
import ai.unico.platform.search.api.wrapper.OpportunityWrapper;
import ai.unico.platform.store.api.dto.CategoryDto;
import ai.unico.platform.store.api.dto.OpportunityRegisterDto;
import ai.unico.platform.store.api.service.*;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("common/opportunity")
public class OpportunityController {

    final private OpportunitySearchService opportunitySearchService = ServiceRegistryProxy.createProxy(OpportunitySearchService.class);
    final private OpportunityCategoryService opportunityCategoryService = ServiceRegistryProxy.createProxy(OpportunityCategoryService.class);
    final private JobTypeService jobTypeService = ServiceRegistryProxy.createProxy(JobTypeService.class);
    final private OpportunityRegistrationService registrationService = ServiceRegistryProxy.createProxy(OpportunityRegistrationService.class);
    final private OpportunityService opportunityService = ServiceRegistryProxy.createProxy(OpportunityService.class);
    final private OrganizationStructureService organizationStructureService = ServiceRegistryProxy.createProxy(OrganizationStructureService.class);

    @RequestMapping(method = RequestMethod.GET)
    public OpportunityWrapper findOpportunities(@ModelAttribute OpportunityFilter opportunityFilter) throws IOException {
        if (opportunityFilter.getOrganizationIds().isEmpty())
            opportunityFilter.setOrganizationIds(organizationStructureService.findChildOrganizationIds(opportunityFilter.getOrganizationId(), true));
        return opportunitySearchService.findOpportunities(opportunityFilter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{opportunityId}")
    private OpportunityPreviewDto getOpportunity(@PathVariable String opportunityId) throws IOException {
        return opportunitySearchService.findOpportunity(opportunityId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/type")
    public List<CategoryDto> getTypes() {
        return opportunityCategoryService.getOpportunityCategories();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/jobType")
    public List<CategoryDto> getJobTypes() {
        return jobTypeService.getJobTypes();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Long createOpportunity(@RequestBody OpportunityRegisterDto opportunityDto,
                                  @ModelAttribute UserContext userContext,
                                  @ModelAttribute UserSecurityHelper securityHelper) throws UserUnauthorizedException, IOException {
        if (!opportunityDto.getOrganizationIds().stream().allMatch(securityHelper::canCreateOrganizationItem))
            throw new UserUnauthorizedException();
        return opportunityService.createOpportunity(opportunityDto, userContext);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/{opportunityCode}")
    public void updateOpportunity(@PathVariable String opportunityCode,
                                  @RequestBody OpportunityRegisterDto opportunityDto,
                                  @ModelAttribute UserContext userContext,
                                  @ModelAttribute UserSecurityHelper securityHelper) throws UserUnauthorizedException, IOException {
        //todo think of security
        opportunityService.updateOpportunity(opportunityDto, opportunityCode, userContext);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{opportunityCode}")
    public void deleteOpportunity(@PathVariable String opportunityCode,
                                  @ModelAttribute UserContext userContext,
                                  @ModelAttribute UserSecurityHelper securityHelper) throws UserUnauthorizedException, IOException {
        opportunityService.deleteOpportunity(opportunityCode, userContext);
    }

    @RequestMapping(method = RequestMethod.POST, path = "{oportunityCode}/register")
    public void registerOpportunity(@PathVariable String oportunityCode,
                                    @ModelAttribute UserContext userContext,
                                    @ModelAttribute UserSecurityHelper securityHelper) throws IOException {
        registrationService.registerOpportunity(oportunityCode, true, userContext);
    }

    @RequestMapping(method = RequestMethod.POST, path = "{oportunityCode}/hide")
    public void hideOpportunity(@PathVariable String oportunityCode,
                                @ModelAttribute UserContext userContext,
                                @ModelAttribute UserSecurityHelper securityHelper) throws IOException {
        opportunityService.toggleHiddenOpportunity(oportunityCode, true, userContext);
    }

    @RequestMapping(method = RequestMethod.POST, path = "{oportunityCode}/show")
    public void showOpportunity(@PathVariable String oportunityCode,
                                @ModelAttribute UserContext userContext,
                                @ModelAttribute UserSecurityHelper securityHelper) throws IOException {
        opportunityService.toggleHiddenOpportunity(oportunityCode, false, userContext);
    }
}
