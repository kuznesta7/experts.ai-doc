package ai.unico.platform.rest.factory;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.rest.security.UserSecurityHelperImpl;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class UserSecurityHelperFactory {

    @ModelAttribute
    public UserSecurityHelper getUserSecurityHelper(UserContext userContext) {
        return new UserSecurityHelperImpl(userContext);
    }

}