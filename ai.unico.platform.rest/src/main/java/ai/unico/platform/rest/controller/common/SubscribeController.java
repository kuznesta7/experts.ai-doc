package ai.unico.platform.rest.controller.common;

import ai.unico.platform.store.api.dto.BaseResultDto;
import ai.unico.platform.store.api.dto.SubscribeDto;
import ai.unico.platform.store.api.dto.SubscriptionTypeDto;
import ai.unico.platform.store.api.service.SubscriptionService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.enums.ResultCode;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("common/subscribe")
public class SubscribeController {

    private final SubscriptionService subscriptionService = ServiceRegistryProxy.createProxy(SubscriptionService.class);

    @RequestMapping(method = RequestMethod.GET, path = "/type")
    public List<SubscriptionTypeDto> getSubscribeTypes() {
        return subscriptionService.listSubscriptionTypes();
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{organizationId}/{email}/{userToken}")
    public BaseResultDto unsubscribe(@PathVariable Long organizationId,
                                     @PathVariable String email,
                                     @PathVariable String userToken) {
        final SubscribeDto subscribeDto = new SubscribeDto();
        subscribeDto.setOrganizationId(organizationId);
        subscribeDto.setEmail(email);
        subscribeDto.setUserToken(userToken);
        try {
            subscriptionService.unsubscribe(subscribeDto);
        } catch (Exception e) {
            return new BaseResultDto<>(ResultCode.NOT_FOUND, "Subscription for user " + subscribeDto.getEmail() + " not found");
        }
        return new BaseResultDto<>(ResultCode.OK, "Subscription for user " + subscribeDto.getEmail() + " deleted");
    }
}
