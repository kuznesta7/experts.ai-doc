package ai.unico.platform.rest;

import ai.unico.platform.rest.exception.ServiceTemporarilyUnavailableException;
import ai.unico.platform.util.exception.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class PlatformExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(PlatformExceptionHandler.class);

    @ExceptionHandler(NonexistentEntityException.class)
    public ResponseEntity<String> handleNonexistentEntityException(NonexistentEntityException e) {
        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.TEXT_PLAIN);
        return new ResponseEntity<String>(e.getMessage(), responseHeaders, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NoLoggedInUserException.class)
    public ResponseEntity<String> handleNoLoggedInUserException(NoLoggedInUserException e) {
        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.TEXT_PLAIN);
        return new ResponseEntity<String>(e.getMessage(), responseHeaders, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(UserUnauthorizedException.class)
    public ResponseEntity<String> handleUserUnauthorizedException(UserUnauthorizedException e) {
        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.TEXT_PLAIN);
        return new ResponseEntity<String>(e.getMessage(), responseHeaders, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(ServiceTemporarilyUnavailableException.class)
    public ResponseEntity<String> handleServiceTemporarilyUnavailableException(ServiceTemporarilyUnavailableException e) {
        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.TEXT_PLAIN);
        return new ResponseEntity<String>(e.getMessage(), responseHeaders, HttpStatus.SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler(EntityAlreadyExistsException.class)
    public ResponseEntity<Map<String, String>> handleEntityAlreadyUsedException(EntityAlreadyExistsException e) {
        final HttpHeaders responseHeaders = new HttpHeaders();
        LOG.error(e.getSecretMessage());
        responseHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        final Map<String, String> map = new HashMap<>();
        map.put("msg", e.getMessage());
        return new ResponseEntity<Map<String, String>>(map, responseHeaders, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(RuntimeExceptionWithMessage.class)
    public ResponseEntity<Map<String, String>> handleRuntimeExceptionWithMessage(RuntimeExceptionWithMessage e) {
        final HttpHeaders responseHeaders = new HttpHeaders();
        LOG.error(e.getSecretMessage());
        responseHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        final Map<String, String> map = new HashMap<>();
        map.put("msg", e.getMessage());
        return new ResponseEntity<Map<String, String>>(map, responseHeaders, HttpStatus.BAD_REQUEST);
    }
}
