package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.rest.util.ControllerUtil;
import ai.unico.platform.store.api.dto.EvidenceImportDto;
import ai.unico.platform.store.api.enums.EvidenceCategory;
import ai.unico.platform.store.api.service.EvidenceImportService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("internal/evidence/organizations/{organizationId}/imports")
public class EvidenceImportController {

    private EvidenceImportService evidenceImportService = ServiceRegistryProxy.createProxy(EvidenceImportService.class);

    @RequestMapping(method = RequestMethod.POST)
    public void uploadImportFile(UserContext userContext,
                                 @PathVariable Long organizationId,
                                 @RequestParam String fileName,
                                 @RequestParam String note,
                                 @RequestParam EvidenceCategory category,
                                 @ModelAttribute UserSecurityHelper userSecurityHelper,
                                 @RequestParam(value = "content", required = false) MultipartFile multipartFile) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canImportEvidence(organizationId))
            throw new UserUnauthorizedException();
        final FileDto fileDto = new FileDto(multipartFile.getBytes(), fileName);
        evidenceImportService.uploadImportFile(organizationId, category, fileDto, note, userContext);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{importId}")
    public EvidenceImportDto getEvidenceImportDetail(UserContext userContext,
                                                     @PathVariable Long organizationId,
                                                     @PathVariable Long importId,
                                                     @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canImportEvidence(organizationId))
            throw new UserUnauthorizedException();
        return evidenceImportService.getImportDetail(importId, organizationId, userContext);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{importId}/file")
    public HttpEntity<byte[]> getEvidenceImportFile(UserContext userContext,
                                                    @PathVariable Long organizationId,
                                                    @PathVariable Long importId,
                                                    @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canImportEvidence(organizationId))
            throw new UserUnauthorizedException();
        final FileDto fileDto = evidenceImportService.getImportFile(importId, organizationId, userContext);
        return ControllerUtil.toFileResponse(fileDto);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{importId}")
    public void deleteEvidenceImportFile(UserContext userContext,
                                         @PathVariable Long organizationId,
                                         @PathVariable Long importId,
                                         @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canDeleteImportEvidence(organizationId))
            throw new UserUnauthorizedException();
        evidenceImportService.deleteImport(importId, organizationId, userContext);
    }
}
