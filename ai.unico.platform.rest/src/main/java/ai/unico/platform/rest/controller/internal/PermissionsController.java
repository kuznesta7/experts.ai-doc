package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.store.api.dto.AskForPermissionsDto;
import ai.unico.platform.store.api.service.AskForPermissionsService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("internal/")
public class PermissionsController {

    private final AskForPermissionsService askForPermissionsService = ServiceRegistryProxy.createProxy(AskForPermissionsService.class);

    @RequestMapping(method = RequestMethod.POST, path = "permissions/requests")
    public void askForPermissions(UserContext userContext, @RequestBody AskForPermissionsDto askForPermissionsDto) {
        askForPermissionsService.askForPermissions(askForPermissionsDto, userContext);
    }
}
