package ai.unico.platform.rest.controller.common;

import ai.unico.platform.store.api.dto.ExternalMessageDto;
import ai.unico.platform.store.api.enums.MessageType;
import ai.unico.platform.store.api.service.ExternalMessageService;
import ai.unico.platform.util.ServiceRegistryProxy;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/common/messages")
public class ExternalMessageController {

    private final ExternalMessageService externalMessageService = ServiceRegistryProxy.createProxy(ExternalMessageService.class);

    @RequestMapping(method = RequestMethod.POST)
    public void contactFromWidget(@RequestBody ExternalMessageDto externalMessageDto) throws IOException {
        if (externalMessageDto.getMessageType().equals(MessageType.REQUEST_INFORMATION)) {
            externalMessageService.sendRequestInformationMessage(externalMessageDto);
        } else {
            externalMessageService.sendMessage(externalMessageDto);
        }
    }

    @RequestMapping(method = RequestMethod.POST, path = "contactus")
    public void contactUs(@RequestBody ExternalMessageDto externalMessageDto) throws IOException {
        externalMessageService.sendContactUsMessage(externalMessageDto);
    }
}
