package ai.unico.platform.rest.security;

import ai.unico.platform.search.api.filter.ExpertSearchFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.util.exception.NoLoggedInUserException;
import ai.unico.platform.util.exception.UserUnauthorizedException;

public class SecurityHelperImpl implements SecurityHelper {

    @Override
    public void checkAccessibility(ExpertSearchFilter expertSearchFilter, UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NoLoggedInUserException {
        if (((Integer) 0).equals(expertSearchFilter.getLimit())) return;

        if (!userSecurityHelper.isLoggedIn()) throw new NoLoggedInUserException();

        if (!userSecurityHelper.canSearchWithFilters()
                && (expertSearchFilter.getOrganizationIds().size() > 0
                || expertSearchFilter.getResultTypeGroupIds().size() > 0
                || expertSearchFilter.getResultTypeIds().size() > 0
                || expertSearchFilter.getYearFrom() != null
                || expertSearchFilter.getYearTo() != null)) {
            throw new UserUnauthorizedException();
        }
        if (!userSecurityHelper.canPaginate() && (expertSearchFilter.getPage() > 1 || expertSearchFilter.getLimit() > ExpertSearchFilter.LIMIT_BASE)) {
            throw new UserUnauthorizedException();
        }
        if (!(expertSearchFilter instanceof ExpertSearchResultFilter)
                && !userSecurityHelper.canSearchWithFilters()
                && (expertSearchFilter.getCountryCodes().size() != 1 || !userSecurityHelper.canAccessCountries(expertSearchFilter.getCountryCodes()))) {
            throw new UserUnauthorizedException();
        }
        if (expertSearchFilter instanceof ExpertSearchResultFilter) {
            final ExpertSearchResultFilter expertSearchResultFilter = (ExpertSearchResultFilter) expertSearchFilter;
        }
    }
}
