package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.rest.util.ControllerUtil;
import ai.unico.platform.search.api.dto.CommercializationProjectDetailDto;
import ai.unico.platform.search.api.filter.CommercializationFilter;
import ai.unico.platform.search.api.service.CommercializationProjectSearchService;
import ai.unico.platform.search.api.service.EvidenceCommercializationService;
import ai.unico.platform.search.api.wrapper.EvidenceCommercializationWrapper;
import ai.unico.platform.store.api.dto.CommercializationProjectDocumentDto;
import ai.unico.platform.store.api.dto.CommercializationProjectDocumentPreviewDto;
import ai.unico.platform.store.api.dto.CommercializationProjectDto;
import ai.unico.platform.store.api.dto.CommercializationProjectStatusPreviewDto;
import ai.unico.platform.store.api.service.CommercializationDocumentService;
import ai.unico.platform.store.api.service.CommercializationProjectImgService;
import ai.unico.platform.store.api.service.CommercializationProjectService;
import ai.unico.platform.store.api.service.CommercializationStatusService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("internal/commercialization/projects")
public class CommercializationProjectController {

    private final CommercializationProjectSearchService commercializationProjectSearchService = ServiceRegistryProxy.createProxy(CommercializationProjectSearchService.class);
    private final CommercializationProjectService commercializationProjectService = ServiceRegistryProxy.createProxy(CommercializationProjectService.class);
    private final CommercializationProjectImgService commercializationProjectImgService = ServiceRegistryProxy.createProxy(CommercializationProjectImgService.class);
    private final CommercializationStatusService commercializationStatusService = ServiceRegistryProxy.createProxy(CommercializationStatusService.class);
    private final CommercializationDocumentService commercializationDocumentService = ServiceRegistryProxy.createProxy(CommercializationDocumentService.class);
    private final EvidenceCommercializationService evidenceCommercializationService = ServiceRegistryProxy.createProxy(EvidenceCommercializationService.class);

    @RequestMapping(method = RequestMethod.POST)
    public Long createOrganizationCommercializationProject(@RequestBody CommercializationProjectDto commercializationProjectDto,
                                                           @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                           UserContext userContext) throws UserUnauthorizedException {
        if (!userSecurityHelper.canCreateCommercializationProject())
            throw new UserUnauthorizedException();
        return commercializationProjectService.createCommercializationProject(commercializationProjectDto, userContext);
    }

    @RequestMapping(path = "{projectId}", method = RequestMethod.PUT)
    public void updateCommercializationProject(@RequestBody CommercializationProjectDto commercializationProjectDto,
                                               @PathVariable Long projectId,
                                               @ModelAttribute UserSecurityHelper userSecurityHelper,
                                               UserContext userContext) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditCommercializationProject(projectId)) throw new UserUnauthorizedException();
        commercializationProjectService.updateCommercializationProject(projectId, commercializationProjectDto, userContext);
    }

    @RequestMapping(path = "{projectId}/image", method = RequestMethod.PUT, consumes = {"multipart/form-data"}, produces = "text/plain")
    public String updateCommercializationProjectImage(@RequestParam String fileName,
                                                      @RequestParam(value = "content", required = false) MultipartFile multipartFile,
                                                      @PathVariable Long projectId,
                                                      @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                      UserContext userContext) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canEditCommercializationProject(projectId)) throw new UserUnauthorizedException();
        final byte[] bytes = multipartFile.getBytes();
        return commercializationProjectImgService.saveImg(userContext, projectId, new FileDto(bytes, fileName));
    }

    @RequestMapping(path = "{commercializationProjectId}/documents", method = RequestMethod.GET)
    public List<CommercializationProjectDocumentPreviewDto> getCommercializationProjectDocuments(@PathVariable Long commercializationProjectId,
                                                                                                 @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canSeeCommercializationProjectDetail(commercializationProjectId))
            throw new UserUnauthorizedException();
        return commercializationDocumentService.findCommercializationProjectDocuments(commercializationProjectId);
    }

    @RequestMapping(path = "{commercializationProjectId}/documents/{fileId}", method = RequestMethod.GET)
    public HttpEntity<byte[]> getCommercializationDocument(@PathVariable Long commercializationProjectId,
                                                           @PathVariable Long fileId,
                                                           @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                           UserContext userContext) throws UserUnauthorizedException {
        if (!userSecurityHelper.canSeeCommercializationProjectDetail(commercializationProjectId))
            throw new UserUnauthorizedException();
        final FileDto fileDto = commercializationDocumentService.getCommercializationProjectDocument(commercializationProjectId, fileId, userContext);
        return ControllerUtil.toFileResponse(fileDto);
    }

    @RequestMapping(path = "{commercializationProjectId}/documents", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public Long addCommercializationDocument(@PathVariable Long commercializationProjectId,
                                             @ModelAttribute UserSecurityHelper userSecurityHelper,
                                             @RequestParam String fileName,
                                             @RequestParam(value = "content", required = false) MultipartFile multipartFile,
                                             UserContext userContext) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canEditCommercializationProject(commercializationProjectId))
            throw new UserUnauthorizedException();
        final CommercializationProjectDocumentDto documentDto = new CommercializationProjectDocumentDto();
        documentDto.setContent(multipartFile.getBytes());
        documentDto.setName(fileName);
        return commercializationDocumentService.addCommercializationProjectDocument(commercializationProjectId, documentDto, userContext);
    }

    @RequestMapping(path = "{commercializationProjectId}/documents/{fileId}", method = RequestMethod.DELETE)
    public void deleteCommercializationDocument(@PathVariable Long commercializationProjectId,
                                                @PathVariable Long fileId,
                                                @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                UserContext userContext) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditCommercializationProject(commercializationProjectId))
            throw new UserUnauthorizedException();
        commercializationDocumentService.deleteCommercializationProjectDocument(commercializationProjectId, fileId, userContext);
    }

    @RequestMapping(path = "{commercializationProjectId}/states", method = RequestMethod.GET)
    public List<CommercializationProjectStatusPreviewDto> getCommercializationProjectStates(@PathVariable Long commercializationProjectId,
                                                                                            @ModelAttribute UserSecurityHelper userSecurityHelper) throws IOException, UserUnauthorizedException {
        if (!userSecurityHelper.canSeeCommercializationProjectDetail(commercializationProjectId))
            throw new UserUnauthorizedException();
        return commercializationStatusService.findProjectStatusHistory(commercializationProjectId);
    }

    @RequestMapping(path = "{commercializationProjectId}/states/{stateId}", method = RequestMethod.PUT)
    public void addCommercializationProjectState(@PathVariable Long commercializationProjectId,
                                                 @PathVariable Long stateId,
                                                 @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                 UserContext userContext) throws IOException, UserUnauthorizedException {
        if (!userSecurityHelper.canSetCommercializationProjectStatus(commercializationProjectId, stateId))
            throw new UserUnauthorizedException();
        commercializationStatusService.setStatus(commercializationProjectId, stateId, userContext);
    }

    @RequestMapping(path = "{commercializationProjectId}", method = RequestMethod.GET)
    public CommercializationProjectDetailDto findCommercializationProjectDetail(@PathVariable Long commercializationProjectId,
                                                                                @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeCommercializationProjectDetail(commercializationProjectId))
            throw new UserUnauthorizedException();
        return commercializationProjectSearchService.findCommercializationProjectDetail(commercializationProjectId);
    }

    @RequestMapping(method = RequestMethod.GET)
    public EvidenceCommercializationWrapper findInvestmentProjects(@ModelAttribute CommercializationFilter filter,
                                                                   @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                                   UserContext userContext) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeActiveCommercializationProjects()) throw new UserUnauthorizedException();
        final Set<Long> statusIdsForInvest = commercializationStatusService.findStatusIdsForInvest();
        filter.setCommercializationStatusIds(statusIdsForInvest);
        return evidenceCommercializationService.findEvidenceCommercialization(filter);
    }
}
