package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.rest.util.ControllerUtil;
import ai.unico.platform.search.api.filter.CommercializationFilter;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.filter.ProjectFilter;
import ai.unico.platform.search.api.service.*;
import ai.unico.platform.search.api.wrapper.EvidenceCommercializationWrapper;
import ai.unico.platform.search.api.wrapper.EvidenceExpertWrapper;
import ai.unico.platform.search.api.wrapper.EvidenceItemWrapper;
import ai.unico.platform.search.api.wrapper.ProjectWrapper;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.dto.OrganizationRolesDto;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static ai.unico.platform.rest.util.OrganizationUtil.findOrganizationSubset;

@RestController
@RequestMapping("internal/evidence/")
public class EvidenceController {

    private EvidenceExpertService evidenceExpertService = ServiceRegistryProxy.createProxy(EvidenceExpertService.class);
    private EvidenceItemService evidenceItemService = ServiceRegistryProxy.createProxy(EvidenceItemService.class);
    private EvidenceSearchService evidenceSearchService = ServiceRegistryProxy.createProxy(EvidenceSearchService.class);

    @RequestMapping(method = RequestMethod.GET, path = "search")
    public Map<String, Object> searchGlobal(UserContext userContext,
                                            @RequestParam String query,
                                            @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        final Set<Long> organizationIds = userContext.getOrganizationRoles().stream().filter(or -> or.getRoles().stream()
                .anyMatch(r -> {
                    if (!r.isValid()) return false;
                    return Arrays.asList(OrganizationRole.ORGANIZATION_EDITOR, OrganizationRole.ORGANIZATION_VIEWER).contains(r.getRole());
                })).map(OrganizationRolesDto::getOrganizationId)
                .collect(Collectors.toSet());
        if (organizationIds.isEmpty()) return Collections.emptyMap();
        return evidenceSearchService.search(organizationIds, query, userContext);
    }

    @RequestMapping(method = RequestMethod.GET, path = "organizations/{organizationId}/experts")
    public EvidenceExpertWrapper findOrganizationAnalyticsExperts(UserContext userContext,
                                                                  @PathVariable Long organizationId,
                                                                  @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                                  @ModelAttribute EvidenceFilter evidenceFilter) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        evidenceFilter.setOrganizationId(organizationId);
        evidenceFilter.setOrganizationIds(findOrganizationSubset(organizationId, evidenceFilter.getOrganizationIds(), evidenceFilter.isRestrictOrganization()));
        return evidenceExpertService.findAnalyticsExperts(evidenceFilter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "organizations/{organizationId}/affiliated-experts")
    public EvidenceExpertWrapper findOrganizationAnalyticsAffiliatedExperts(UserContext userContext,
                                                                            @PathVariable Long organizationId,
                                                                            @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                                            @ModelAttribute EvidenceFilter evidenceFilter) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        evidenceFilter.setOrganizationId(organizationId);
        evidenceFilter.setAffiliatedOrganizationIds(findOrganizationSubset(organizationId, evidenceFilter.getAffiliatedOrganizationIds(), evidenceFilter.isRestrictOrganization()));
        return evidenceExpertService.findAnalyticsExperts(evidenceFilter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "organizations/{organizationId}/experts/csv")
    public HttpEntity<byte[]> exportOrganizationAnalyticsExperts(UserContext userContext,
                                                                 @PathVariable Long organizationId,
                                                                 @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                                 @ModelAttribute EvidenceFilter evidenceFilter) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        evidenceFilter.setOrganizationId(organizationId);
        evidenceFilter.setOrganizationIds(findOrganizationSubset(organizationId, evidenceFilter.getOrganizationIds(), evidenceFilter.isRestrictOrganization()));
        return ControllerUtil.toFileResponse(evidenceExpertService.exportAnalyticsExperts(evidenceFilter));
    }

    @RequestMapping(method = RequestMethod.GET, path = "/experts/csv")
    public HttpEntity<byte[]> exportAnalyticsExperts(UserContext userContext,
                                                     @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                     @ModelAttribute EvidenceFilter evidenceFilter) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeAllExperts())
            throw new UserUnauthorizedException();
        return ControllerUtil.toFileResponse(evidenceExpertService.exportAnalyticsExperts(evidenceFilter));
    }

    @RequestMapping(method = RequestMethod.GET, path = "experts")
    public EvidenceExpertWrapper findAnalyticsExperts(UserContext userContext,
                                                      @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                      @ModelAttribute EvidenceFilter evidenceFilter) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeAllExperts())
            throw new UserUnauthorizedException();
        return evidenceExpertService.findAnalyticsExperts(evidenceFilter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "organizations/{organizationId}/items")
    public EvidenceItemWrapper findOrganizationAnalyticsItems(UserContext userContext,
                                                              @PathVariable Long organizationId,
                                                              @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                              @ModelAttribute EvidenceFilter evidenceFilter) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeItemsWithConfidentiality(organizationId, evidenceFilter.getConfidentiality())) {
            throw new UserUnauthorizedException();
        }
        evidenceFilter.setOrganizationId(organizationId);
        evidenceFilter.setOrganizationIds(findOrganizationSubset(organizationId, evidenceFilter.getOrganizationIds(), evidenceFilter.isRestrictOrganization()));
        return evidenceItemService.findAnalyticsItems(evidenceFilter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "organizations/{organizationId}/items/exists/{name}")
    public Boolean organizationHasItem(UserContext userContext,
                                       @PathVariable Long organizationId,
                                       @ModelAttribute UserSecurityHelper userSecurityHelper,
                                       @ModelAttribute EvidenceFilter evidenceFilter,
                                       @PathVariable String name) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeItemsWithConfidentiality(organizationId, evidenceFilter.getConfidentiality())) {
            throw new UserUnauthorizedException();
        }
        evidenceFilter.setOrganizationId(organizationId);
        evidenceFilter.setOrganizationIds(findOrganizationSubset(organizationId, evidenceFilter.getOrganizationIds(), evidenceFilter.isRestrictOrganization()));
        return evidenceItemService.entityHasItem(evidenceFilter, name);
    }

    @RequestMapping(method = RequestMethod.GET, path = "users/{userId}/items/exists/{name}")
    public Boolean userHasItem(UserContext userContext,
                               @PathVariable Long userId,
                               @ModelAttribute UserSecurityHelper userSecurityHelper,
                               @ModelAttribute EvidenceFilter evidenceFilter,
                               @PathVariable String name) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeItemsWithConfidentiality(userId, evidenceFilter.getConfidentiality())) {
            throw new UserUnauthorizedException();
        }
        evidenceFilter.setExpertCodes(Collections.singleton("ST" + userId));
        return evidenceItemService.entityHasItem(evidenceFilter, name);
    }

    @RequestMapping(method = RequestMethod.GET, path = "organizations/{organizationId}/affiliated-items")
    public EvidenceItemWrapper findOrganizationAnalyticsAffiliatedItems(UserContext userContext,
                                                                        @PathVariable Long organizationId,
                                                                        @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                                        @ModelAttribute EvidenceFilter evidenceFilter) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeItemsWithConfidentiality(organizationId, evidenceFilter.getConfidentiality())) {
            throw new UserUnauthorizedException();
        }
        evidenceFilter.setOrganizationId(organizationId);
        evidenceFilter.setAffiliatedOrganizationIds(findOrganizationSubset(organizationId, evidenceFilter.getAffiliatedOrganizationIds(), evidenceFilter.isRestrictOrganization()));
        return evidenceItemService.findAnalyticsItems(evidenceFilter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "organizations/{organizationId}/items/csv")
    public HttpEntity<byte[]> exportOrganizationAnalyticsItems(UserContext userContext,
                                                               @PathVariable Long organizationId,
                                                               @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                               @ModelAttribute EvidenceFilter evidenceFilter) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeItemsWithConfidentiality(organizationId, evidenceFilter.getConfidentiality()))
            throw new UserUnauthorizedException();
        evidenceFilter.setOrganizationId(organizationId);
        evidenceFilter.setOrganizationIds(findOrganizationSubset(organizationId, evidenceFilter.getOrganizationIds(), evidenceFilter.isRestrictOrganization()));
        return ControllerUtil.toFileResponse(evidenceItemService.exportAnalyticsItems(evidenceFilter));
    }

    @RequestMapping(method = RequestMethod.GET, path = "/items/csv")
    public HttpEntity<byte[]> exportAnalyticsItems(UserContext userContext,
                                                   @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                   @ModelAttribute EvidenceFilter evidenceFilter) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeAllItems())
            throw new UserUnauthorizedException();
        return ControllerUtil.toFileResponse(evidenceItemService.exportAnalyticsItems(evidenceFilter));
    }

    @RequestMapping(method = RequestMethod.GET, path = "items")
    public EvidenceItemWrapper findAnalyticsItems(UserContext userContext,
                                                  @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                  @ModelAttribute EvidenceFilter evidenceFilter) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeAllItems())
            throw new UserUnauthorizedException();
        return evidenceItemService.findAnalyticsItems(evidenceFilter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "projects")
    public ProjectWrapper getProjects(@ModelAttribute ProjectFilter projectFilter,
                                      UserContext userContext,
                                      @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, IOException, UserUnauthorizedException {
        if (!userSecurityHelper.canSeeAllProjects())
            throw new UserUnauthorizedException();
        final EvidenceProjectService evidenceProjectService = ServiceRegistryUtil.getService(EvidenceProjectService.class);
        return evidenceProjectService.findProjects(projectFilter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "organizations/{organizationId}/projects")
    public ProjectWrapper getProjects(@ModelAttribute ProjectFilter projectFilter,
                                      @PathVariable Long organizationId,
                                      UserContext userContext,
                                      @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, IOException, UserUnauthorizedException {
        if (!userSecurityHelper.canSeeItemsWithConfidentiality(organizationId, projectFilter.getConfidentiality()))
            throw new UserUnauthorizedException();
        final EvidenceProjectService evidenceProjectService = ServiceRegistryUtil.getService(EvidenceProjectService.class);
        projectFilter.setOrganizationId(organizationId);
        projectFilter.setOrganizationIds(findOrganizationSubset(organizationId, projectFilter.getOrganizationIds(), projectFilter.isRestrictOrganization()));
        if (projectFilter.getConfidentiality().isEmpty())
            projectFilter.getConfidentiality().addAll(Arrays.asList(Confidentiality.PUBLIC, Confidentiality.CONFIDENTIAL, Confidentiality.SECRET));
        return evidenceProjectService.findProjects(projectFilter);
    }

    private final EvidenceCommercializationService evidenceCommercializationService = ServiceRegistryProxy.createProxy(EvidenceCommercializationService.class);

    @RequestMapping(method = RequestMethod.GET, path = "commercialization")
    public EvidenceCommercializationWrapper findCommercialization(@ModelAttribute CommercializationFilter commercializationFilter,
                                                                  @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeAllInvestProjects()) throw new UserUnauthorizedException();
        return evidenceCommercializationService.findEvidenceCommercialization(commercializationFilter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "organizations/{organizationId}/commercialization")
    public EvidenceCommercializationWrapper findEvidenceCommercialization(@PathVariable Long organizationId,
                                                                          @ModelAttribute CommercializationFilter commercializationFilter,
                                                                          @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeAllInvestProjects() && !userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        commercializationFilter.setOrganizationId(organizationId);
        commercializationFilter.setOrganizationIds(findOrganizationSubset(organizationId, commercializationFilter.getOrganizationIds(), false));
        return evidenceCommercializationService.findEvidenceCommercialization(commercializationFilter);
    }

}
