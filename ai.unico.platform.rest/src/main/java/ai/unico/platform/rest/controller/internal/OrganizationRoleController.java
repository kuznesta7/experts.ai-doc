package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.OrganizationLicenceUsersDto;
import ai.unico.platform.store.api.service.OrganizationRoleService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.dto.OrganizationRolesDto;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("internal/")
public class OrganizationRoleController {

    private final OrganizationRoleService organizationRoleService = ServiceRegistryProxy.createProxy(OrganizationRoleService.class);

    @RequestMapping(method = RequestMethod.GET, path = "users/{userId}/roles")
    public List<OrganizationRolesDto> findAvailableOrganizationsRoles(UserContext userContext) {
        return organizationRoleService.findOrganizationsForUser(userContext.getUserId());
    }

    @RequestMapping(method = RequestMethod.GET, path = "/organizations/{organizationId}/roles")
    public List<OrganizationLicenceUsersDto> findAvailableOrganizationsRoles(
            @PathVariable Long organizationId,
            @PathVariable(required = false) Integer limit,
            @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId)) throw new UserUnauthorizedException();
        return organizationRoleService.findOrganizationUsers(organizationId, limit);
    }


    @RequestMapping(method = RequestMethod.POST, path = "/users/{addedUserId}/organizations/{organizationId}/roles")
    public void addOrganizationRoles(
            @PathVariable Long organizationId,
            @PathVariable Long addedUserId,
            @RequestParam OrganizationRole role,
            UserContext userContext,
            @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManageOrganizationRoles(organizationId)) throw new UserUnauthorizedException();
        organizationRoleService.addUserOrganizationRole(addedUserId, organizationId, role, userContext);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/users/{addedUserId}/organizations/{organizationId}/roles")
    public void removeOrganizationRoles(
            @PathVariable Long organizationId,
            @PathVariable Long addedUserId,
            @RequestParam OrganizationRole role,
            UserContext userContext,
            @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManageOrganizationRoles(organizationId)) throw new UserUnauthorizedException();
        organizationRoleService.removeUserOrganizationRole(addedUserId, organizationId, role, userContext);
    }
}
