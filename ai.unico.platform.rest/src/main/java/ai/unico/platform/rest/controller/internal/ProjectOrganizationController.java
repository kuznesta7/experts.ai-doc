package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.service.ProjectOrganizationService;
import ai.unico.platform.store.api.service.ProjectService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.enums.ProjectOrganizationRole;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("internal/projects/{projectId}/organizations")
public class ProjectOrganizationController {

    private ProjectOrganizationService projectOrganizationService = ServiceRegistryProxy.createProxy(ProjectOrganizationService.class);
    private final ProjectService projectService = ServiceRegistryProxy.createProxy(ProjectService.class);

    @RequestMapping(method = RequestMethod.PUT, path = "/{organizationId}")
    public void saveProjectOrganization(@PathVariable Long projectId, @PathVariable Long organizationId,
                                        @RequestParam(required = false) boolean verify,
                                        @RequestParam(required = false) ProjectOrganizationRole projectOrganizationRole,
                                        UserContext userContext,
                                        @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NonexistentEntityException {
        if (!userSecurityHelper.canEditProject(projectId)) throw new UserUnauthorizedException();
        if (projectOrganizationRole == null) {
            projectOrganizationRole = ProjectOrganizationRole.PARTICIPANT;
        }
        if (verify) {
            if (!userSecurityHelper.canVerifyProjectOrganization(organizationId)) throw new UserUnauthorizedException();
        }
        projectOrganizationService.addProjectOrganization(projectId, organizationId, verify, projectOrganizationRole, userContext);
        projectService.loadAndIndexProject(projectId);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{organizationId}")
    public void deleteProjectOrganization(@PathVariable Long projectId, @PathVariable Long organizationId,
                                          UserContext userContext,
                                          @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NonexistentEntityException {
        if (!userSecurityHelper.canEditProject(projectId)) throw new UserUnauthorizedException();
        projectOrganizationService.removeProjectOrganization(projectId, organizationId, userContext);
        projectService.loadAndIndexProject(projectId);
    }
}
