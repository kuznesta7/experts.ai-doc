package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.FollowProfileDto;
import ai.unico.platform.store.api.dto.FollowedProfilePreviewDto;
import ai.unico.platform.store.api.filter.FollowedProfilesFilter;
import ai.unico.platform.store.api.service.FollowProfileService;
import ai.unico.platform.store.api.wrapper.FollowedProfilesWrapper;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("internal")
public class FollowProfileController {

    private final FollowProfileService followProfileService = ServiceRegistryProxy.createProxy(FollowProfileService.class);

    @RequestMapping(method = RequestMethod.GET, path = "/users/{userId}/followed-profiles")
    public FollowedProfilesWrapper getFollowedProfiles(@PathVariable Long userId,
                                                       @ModelAttribute FollowedProfilesFilter filter,
                                                       UserContext userContext,
                                                       @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeUserFollowedProfiles(userId)) throw new UserUnauthorizedException();
        return followProfileService.findFollowedProfilesForUser(userId, filter);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/users/{userId}/followed-profiles")
    public void followProfile(@PathVariable Long userId,
                              @RequestBody FollowProfileDto followProfileDto,
                              UserContext userContext,
                              @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManageUserFollowedProfiles(userId)) throw new UserUnauthorizedException();
        followProfileDto.setFollowingUserId(userId);
        followProfileService.followProfile(followProfileDto, userContext);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/users/{userId}/followed-profiles/users/{followedUserId}")
    public FollowedProfilePreviewDto getFollow(@PathVariable Long userId,
                                               @PathVariable Long followedUserId,
                                               UserContext userContext,
                                               @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canSeeUserFollowedProfiles(userId)) throw new UserUnauthorizedException();
        return followProfileService.findFollowedUserProfile(userId, followedUserId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/users/{userId}/followed-profiles/experts/{followedExpertId}")
    public FollowedProfilePreviewDto getExpertFollow(@PathVariable Long userId,
                                                     @PathVariable Long followedExpertId,
                                                     UserContext userContext,
                                                     @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeUserFollowedProfiles(userId)) throw new UserUnauthorizedException();
        return followProfileService.findFollowedExpertProfile(userId, followedExpertId);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/users/{userId}/followed-profiles/users/{followedUserId}")
    public void unfollowUserProfile(@PathVariable Long userId,
                                    @PathVariable Long followedUserId,
                                    UserContext userContext,
                                    @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManageUserFollowedProfiles(userId)) throw new UserUnauthorizedException();
        followProfileService.unfollowUserProfile(userId, followedUserId, userContext);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/users/{userId}/followed-profiles/experts/{followedExpertId}")
    public void unfollowExpertProfile(@PathVariable Long userId,
                                      @PathVariable Long followedExpertId,
                                      UserContext userContext,
                                      @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManageUserFollowedProfiles(userId)) throw new UserUnauthorizedException();
        followProfileService.unfollowExpertProfile(userId, followedExpertId, userContext);
    }
}
