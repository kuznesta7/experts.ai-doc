package ai.unico.platform.rest.controller.internal;


import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.service.ProjectService;
import ai.unico.platform.store.api.service.ProjectStructureService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("internal/organizations/{organizationId}/projects/{projectId}")
public class ProjectStructureController {

    private final ProjectStructureService projectStructureService = ServiceRegistryProxy.createProxy(ProjectStructureService.class);
    private final ProjectService projectService = ServiceRegistryProxy.createProxy(ProjectService.class);

    @RequestMapping(method = RequestMethod.PUT, path = "/parent/{parentProjectId}")
    public void addParentProject(@PathVariable Long projectId,
                                 @PathVariable Long organizationId,
                                 @PathVariable Long parentProjectId,
                                 UserContext userContext,
                                 @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NonexistentEntityException {
        if (!userSecurityHelper.canEditPrivateOrganizationProject(projectId, organizationId))
            throw new UserUnauthorizedException();
        projectStructureService.addParentProject(projectId, parentProjectId, organizationId, userContext);
        projectService.loadAndIndexProject(projectId);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/parent")
    public void removeParentProject(@PathVariable Long projectId,
                                    @PathVariable Long organizationId,
                                    UserContext userContext,
                                    @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NonexistentEntityException {
        if (!userSecurityHelper.canEditPrivateOrganizationProject(projectId, organizationId))
            throw new UserUnauthorizedException();
        projectStructureService.removeParentProject(projectId, organizationId, userContext);
        projectService.loadAndIndexProject(projectId);
    }
}
