package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.service.ItemOrganizationService;
import ai.unico.platform.store.api.service.ItemService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;

@RestController
@RequestMapping("internal/items/{itemId}/organizations/{organizationId}")
public class ItemOrganizationController {

    private ItemService itemService = ServiceRegistryProxy.createProxy(ItemService.class);

    @RequestMapping(method = RequestMethod.PUT)
    public void addItemOrganization(@PathVariable Long organizationId,
                                    @PathVariable Long itemId,
                                    @RequestParam(required = false) boolean verify,
                                    UserContext userContext,
                                    @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException {
        if (!userSecurityHelper.canManageItemOrganization(itemId, organizationId))
            throw new UserUnauthorizedException();
        if (verify && !userSecurityHelper.canVerifyItemOrganization(organizationId))
            throw new UserUnauthorizedException();
        final ItemOrganizationService service = ServiceRegistryUtil.getService(ItemOrganizationService.class);
        service.addItemOrganization(organizationId, Collections.singleton(itemId), verify, userContext);
        itemService.loadAndIndexItem(itemId);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void removeItemOrganization(@PathVariable Long organizationId,
                                       @PathVariable Long itemId,
                                       UserContext userContext,
                                       @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException {
        if (!userSecurityHelper.canManageItemOrganization(itemId, organizationId))
            throw new UserUnauthorizedException();
        final ItemOrganizationService service = ServiceRegistryUtil.getService(ItemOrganizationService.class);
        service.removeItemOrganization(organizationId, itemId, userContext);
        itemService.loadAndIndexItem(itemId);
    }
}
