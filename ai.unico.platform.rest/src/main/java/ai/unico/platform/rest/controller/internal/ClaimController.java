package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.search.api.dto.ExpertClaimPreviewDto;
import ai.unico.platform.search.api.service.ElasticsearchService;
import ai.unico.platform.search.api.service.ExpertClaimSearchService;
import ai.unico.platform.search.api.service.ExpertIndexService;
import ai.unico.platform.store.api.dto.ClaimRequestDto;
import ai.unico.platform.store.api.dto.PreClaimDto;
import ai.unico.platform.store.api.service.ClaimProfileService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.OutdatedEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("internal/claims")
public class ClaimController {

    private final ClaimProfileService claimProfileService = ServiceRegistryProxy.createProxy(ClaimProfileService.class);
    private final ElasticsearchService elasticsearchService = ServiceRegistryProxy.createProxy(ElasticsearchService.class);

    @RequestMapping(method = RequestMethod.POST, path = "")
    public void requestClaiming(UserContext userContext, @RequestBody ClaimRequestDto claimRequestDto) {
        claimProfileService.requestClaiming(claimRequestDto, userContext);
    }

    @RequestMapping(path = "/experts/{expertId}", method = RequestMethod.PUT)
    public void claimExpertProfile(
            @PathVariable Long expertId,
            @ModelAttribute UserSecurityHelper userSecurityHelper,
            @RequestParam(required = false) Long claimingUserId,
            UserContext userContext) throws UserUnauthorizedException, NonexistentEntityException, OutdatedEntityException, IOException {
        if (claimingUserId != null) {
            if (!userSecurityHelper.canManageExpertClaims()) {
                throw new UserUnauthorizedException();
            }
        } else if (!userSecurityHelper.canClaimExpertProfile(expertId)) {
            throw new UserUnauthorizedException();
        }
        final Long userId = claimingUserId != null ? claimingUserId : userContext.getUserId();
        claimProfileService.claimExpertProfile(expertId, userId, userContext);
        elasticsearchService.flushSyncedAll();
    }

    @RequestMapping(path = "/experts/{expertId}", method = RequestMethod.DELETE)
    public void unclaimExpertProfile(
            @PathVariable Long expertId,
            @ModelAttribute UserSecurityHelper userSecurityHelper,
            UserContext userContext) throws UserUnauthorizedException, NonexistentEntityException, IOException {
        if (!userSecurityHelper.canUnclaimExpertProfile(expertId)) {
            throw new UserUnauthorizedException();
        }
        claimProfileService.unclaimExpertProfile(expertId, userContext);
        elasticsearchService.flushSyncedAll();
    }

    @RequestMapping(path = "/users/{userToClaimId}", method = RequestMethod.PUT)
    public void claimUserProfile(
            @PathVariable Long userToClaimId,
            @ModelAttribute UserSecurityHelper userSecurityHelper,
            @RequestParam(required = false) Long claimingUserId,
            UserContext userContext) throws UserUnauthorizedException, NonexistentEntityException, OutdatedEntityException, IOException {
        if (claimingUserId != null) {
            if (!userSecurityHelper.canManageExpertClaims()) {
                throw new UserUnauthorizedException();
            }
        } else if (!userSecurityHelper.canClaimUserProfile(userToClaimId)) {
            throw new UserUnauthorizedException();
        }
        final Long claimerUserId = claimingUserId != null ? claimingUserId : userContext.getUserId();
        claimProfileService.claimUserProfile(userToClaimId, claimerUserId, userContext);
        elasticsearchService.flushSyncedAll();
    }

    @RequestMapping(path = "/users/{userId}", method = RequestMethod.DELETE)
    public void unclaimUserProfile(
            @PathVariable Long userId,
            @ModelAttribute UserSecurityHelper userSecurityHelper,
            UserContext userContext) throws UserUnauthorizedException, NonexistentEntityException, IOException {
        if (!userSecurityHelper.canUnclaimUserProfile(userId)) {
            throw new UserUnauthorizedException();
        }
        claimProfileService.unclaimUserProfile(userId, userContext);
        elasticsearchService.flushSyncedAll();
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<ExpertClaimPreviewDto> findExpertsToClaim(
            @ModelAttribute UserSecurityHelper userSecurityHelper,
            UserContext userContext) throws UserUnauthorizedException, NonexistentEntityException, IOException {
        final ExpertClaimSearchService service = ServiceRegistryUtil.getService(ExpertClaimSearchService.class);
        return service.findExpertsToClaim(userContext.getUserId());
    }

    @RequestMapping(method = RequestMethod.GET, path = "users/{userId}/pre-claims")
    public List<ClaimProfileService.Claim> findPreClaims(
            @PathVariable Long userId,
            @ModelAttribute UserSecurityHelper userSecurityHelper,
            UserContext userContext) throws UserUnauthorizedException, NonexistentEntityException, IOException {
        if (!userSecurityHelper.canEditUserProfile(userId)) throw new UserUnauthorizedException();
        return claimProfileService.findPreClaimsForUser(userId);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "pre-claims")
    public void claimPreClaimedProfiles(
            @ModelAttribute UserSecurityHelper userSecurityHelper,
            UserContext userContext) throws UserUnauthorizedException, NonexistentEntityException, IOException {
        claimProfileService.claimAllPreClaimedProfiles(userContext.getUserId(), userContext);
        try {
            final ExpertIndexService expertIndexService = ServiceRegistryUtil.getService(ExpertIndexService.class);
            expertIndexService.flush();
        } catch (Exception ignore) {
        }
    }

    @RequestMapping(method = RequestMethod.GET, path = "pre-claims")
    public List<PreClaimDto> findPreClaims(
            @RequestParam(required = false) Set<Long> userIds,
            @RequestParam(required = false) Set<Long> expertIds,
            @ModelAttribute UserSecurityHelper userSecurityHelper,
            UserContext userContext) throws UserUnauthorizedException, NonexistentEntityException, IOException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(null)) throw new UserUnauthorizedException();
        return claimProfileService.findPreClaimsForUsersExperts(userIds, expertIds);
    }
}
