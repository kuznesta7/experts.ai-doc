package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.service.UserItemService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("internal/items/{itemId}/users/{userId}")
public class ItemUserController {

    private final UserItemService userItemService = ServiceRegistryProxy.createProxy(UserItemService.class);

    @RequestMapping(method = RequestMethod.PUT)
    public void addUserItem(@PathVariable Long itemId,
                            @PathVariable Long userId,
                            UserContext userContext,
                            @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canManageItemUser(itemId, userId)) throw new UserUnauthorizedException();
        userItemService.addUserItem(userId, itemId, userContext);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void removeUserItem(@PathVariable Long userId,
                               @PathVariable Long itemId,
                               @RequestParam(required = false) String message,
                               UserContext userContext,
                               @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException {
        if (!userSecurityHelper.canManageItemUser(itemId, userId)) throw new UserUnauthorizedException();
        userItemService.removeUserItem(userId, itemId, message, userContext);
    }
}
