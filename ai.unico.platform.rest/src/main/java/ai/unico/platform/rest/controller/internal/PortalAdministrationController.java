package ai.unico.platform.rest.controller.internal;


import ai.unico.platform.extdata.service.DWHVariableService;
import ai.unico.platform.store.api.service.PlatformVersionService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("internal/administration")
public class PortalAdministrationController {

    @RequestMapping(method = RequestMethod.GET, path = "version")
    public Map<String, String> getVersionInfo(UserContext userContext) {
        final Map<String, String> versionInfo = new HashMap<>();
        final DWHVariableService dwhVariableService = ServiceRegistryUtil.getService(DWHVariableService.class);
        final PlatformVersionService platformVersionService = ServiceRegistryUtil.getService(PlatformVersionService.class);
        versionInfo.put("DWH_VERSION", dwhVariableService.getDWHVersion());
        versionInfo.put("DB_VERSION", platformVersionService.getDBVersion());
        versionInfo.put("LAST_BUILD_DATE", platformVersionService.lastBuildDate());
        return versionInfo;
    }
}
