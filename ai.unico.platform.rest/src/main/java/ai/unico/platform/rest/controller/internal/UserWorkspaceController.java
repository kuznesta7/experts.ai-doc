package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.UserWorkspacePreviewDto;
import ai.unico.platform.store.api.service.UserWorkspaceService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("internal/users/{userId}/workspaces")
public class UserWorkspaceController {

    private final UserWorkspaceService userWorkspaceService = ServiceRegistryProxy.createProxy(UserWorkspaceService.class);

    @RequestMapping(method = RequestMethod.GET)
    public List<UserWorkspacePreviewDto> getWorkspaces(@PathVariable Long userId,
                                                       UserContext userContext,
                                                       @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canSeeUserWorkspaces(userId)) throw new UserUnauthorizedException();
        return userWorkspaceService.findUserWorkspaces(userId);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{workspaceId}")
    public void deleteWorkspace(@PathVariable Long userId,
                                @PathVariable Long workspaceId,
                                UserContext userContext,
                                @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canSeeUserWorkspaces(userId)) throw new UserUnauthorizedException();
        final List<UserWorkspacePreviewDto> userWorkspaces = userWorkspaceService.findUserWorkspaces(userId);
        if (userWorkspaces.stream().noneMatch(x -> x.getWorkspaceId().equals(workspaceId)))
            throw new UserUnauthorizedException();
        userWorkspaceService.deleteUserWorkspace(workspaceId, userContext);
    }
}
