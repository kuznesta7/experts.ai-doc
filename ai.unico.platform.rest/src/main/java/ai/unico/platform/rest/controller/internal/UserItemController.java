package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.search.api.service.ItemIndexService;
import ai.unico.platform.store.api.dto.ItemDetailDto;
import ai.unico.platform.store.api.service.ItemService;
import ai.unico.platform.store.api.service.UserItemService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("internal/users/{userId}/items")
public class UserItemController {

    private UserItemService userItemService = ServiceRegistryProxy.createProxy(UserItemService.class);
    private ItemService itemService = ServiceRegistryProxy.createProxy(ItemService.class);
    private ItemIndexService itemIndexService = ServiceRegistryProxy.createProxy(ItemIndexService.class);

    @RequestMapping(method = RequestMethod.POST)
    public Long createUserItem(@PathVariable("userId") Long userId,
                               @RequestBody ItemDetailDto itemDetailDto,
                               UserContext userContext,
                               @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canCreateUserItem(userId)) throw new UserUnauthorizedException();
        final Long itemId = itemService.createItem(itemDetailDto, userContext);
        userItemService.addUserItem(userId, itemId, userContext);
        return itemId;
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/{itemId}")
    public void updateUserItem(@PathVariable("userId") Long userId,
                               @PathVariable("itemId") Long itemId,
                               @RequestParam(required = false) Boolean hide,
                               @RequestParam(required = false) Boolean favorite,
                               UserContext userContext,
                               @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException {
        if (!userSecurityHelper.canEditUserProfile(userId)) throw new UserUnauthorizedException();
        if (hide != null)
            userItemService.setHideUserItem(userId, itemId, userContext, hide);
        if (favorite != null)
            userItemService.setFavoriteUserItem(userId, itemId, userContext, favorite);
        itemService.loadAndIndexItem(itemId);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{itemId}")
    public void updateUserItem(@PathVariable("userId") Long userId,
                               @PathVariable("itemId") Long itemId,
                               @RequestParam(required = false) String message,
                               UserContext userContext,
                               @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException {
        if (!userSecurityHelper.canEditUserProfile(userId)) throw new UserUnauthorizedException();
        userItemService.removeUserItem(userId, itemId, message, userContext);
        itemService.loadAndIndexItem(itemId);
    }
}
