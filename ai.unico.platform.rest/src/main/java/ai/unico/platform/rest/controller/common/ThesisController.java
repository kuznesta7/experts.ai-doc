package ai.unico.platform.rest.controller.common;

import ai.unico.platform.search.api.dto.ThesisPreviewDto;
import ai.unico.platform.search.api.service.ThesisSearchService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("common/thesis")
public class ThesisController {

    private ThesisSearchService thesisSearchService = ServiceRegistryProxy.createProxy(ThesisSearchService.class);

    @RequestMapping(method = RequestMethod.GET, path = "{thesisCode}")
    public ThesisPreviewDto getThesisDetail(@PathVariable String thesisCode,
                                            UserContext userContext) throws NonexistentEntityException, IOException, UserUnauthorizedException {
        return thesisSearchService.getThesis(thesisCode);
    }
}
