package ai.unico.platform.rest.exception;

import ai.unico.platform.util.exception.RuntimeExceptionWithMessage;

public class ServiceTemporarilyUnavailableException extends RuntimeExceptionWithMessage {

    public ServiceTemporarilyUnavailableException(String message) {
        super(message);
    }
}
