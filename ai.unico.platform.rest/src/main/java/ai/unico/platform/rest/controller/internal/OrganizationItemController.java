package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.ItemDetailDto;
import ai.unico.platform.store.api.service.ItemOrganizationService;
import ai.unico.platform.store.api.service.ItemService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Collections;
import java.util.Set;

@RestController
@RequestMapping("internal/organizations/{organizationId}/items")
public class OrganizationItemController {

    private final ItemService itemService = ServiceRegistryProxy.createProxy(ItemService.class);
    private final ItemOrganizationService itemOrganizationService = ServiceRegistryProxy.createProxy(ItemOrganizationService.class);

    @RequestMapping(method = RequestMethod.PUT)
    public void verifyOrganizationItems(@PathVariable Long organizationId,
                                        @RequestParam Set<Long> itemIds,
                                        @RequestParam(required = false, defaultValue = "true") boolean verify,
                                        UserContext userContext,
                                        @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canVerifyItemOrganization(organizationId)) throw new UserUnauthorizedException();
        itemOrganizationService.verifyOrganizationItems(organizationId, itemIds, verify, userContext);
        itemService.loadAndIndexItems(itemIds);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/activate")
    public void activateOrganizationItems(@PathVariable Long organizationId,
                                        @RequestParam Set<Long> itemIds,
                                        @RequestParam(required = false, defaultValue = "true") boolean active,
                                        UserContext userContext,
                                        @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canVerifyItemOrganization(organizationId)) throw new UserUnauthorizedException();
        itemOrganizationService.acrivateOrganizationItems(organizationId, itemIds, active, userContext);
        itemService.loadAndIndexItems(itemIds);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/add")
    public void addOrganizationItems(@PathVariable Long organizationId,
                                     @RequestParam Set<Long> itemIds,
                                     UserContext userContext,
                                     @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canVerifyItemOrganization(organizationId)) throw new UserUnauthorizedException();
        itemOrganizationService.addItemOrganization(organizationId, itemIds, true, userContext);
        itemService.loadAndIndexItems(itemIds);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Long createOrganizationItem(@PathVariable Long organizationId,
                                       @RequestBody ItemDetailDto itemDetailDto,
                                       UserContext userContext,
                                       @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canVerifyItemOrganization(organizationId)) throw new UserUnauthorizedException();
        itemDetailDto.setOwnerOrganizationId(organizationId);
        final Long itemId = itemService.createItem(itemDetailDto, userContext);
        itemOrganizationService.addItemOrganization(organizationId, Collections.singleton(itemId), true, userContext);
        itemService.loadAndIndexItem(itemId);
        return itemId;
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void removeOrganizationItems(@PathVariable Long organizationId,
                                        @RequestParam Set<Long> itemIds,
                                        UserContext userContext,
                                        @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canVerifyItemOrganization(organizationId)) throw new UserUnauthorizedException();
        itemOrganizationService.removeOrganizationItems(organizationId, itemIds, userContext);
        itemService.loadAndIndexItems(itemIds);
    }
}
