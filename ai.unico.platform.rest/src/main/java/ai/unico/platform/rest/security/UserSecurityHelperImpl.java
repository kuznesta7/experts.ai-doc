package ai.unico.platform.rest.security;

import ai.unico.platform.search.api.dto.ExpertClaimPreviewDto;
import ai.unico.platform.search.api.service.ExpertClaimSearchService;
import ai.unico.platform.store.api.dto.ItemDetailDto;
import ai.unico.platform.store.api.dto.ProjectDto;
import ai.unico.platform.store.api.enums.WidgetType;
import ai.unico.platform.store.api.service.StoreSecurityHelperService;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.enums.Role;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public class UserSecurityHelperImpl implements UserSecurityHelper {

    private final UserContext userContext;

    private ExpertClaimSearchService expertClaimSearchService = ServiceRegistryProxy.createProxy(ExpertClaimSearchService.class);

    private StoreSecurityHelperService storeSecurityHelperService = ServiceRegistryProxy.createProxy(StoreSecurityHelperService.class);

    @Autowired
    public UserSecurityHelperImpl(UserContext userContext) {
        this.userContext = userContext;
    }

    @Override
    public boolean isLoggedIn() {
        return userContext != null;
    }

    @Override
    public boolean canEditSearchSettings() {
        return isPortalAdmin();
    }

    @Override
    public boolean canEditUserProfile(Long userId) {
        return canEditProfile(IdUtil.convertToIntDataCode(userId));
    }

    @Override
    public boolean canEditProfile(String expertCode) {
        final Long userId = IdUtil.getIntDataId(expertCode);
        if (userId == null || userContext == null) return false;
        return isPortalMaintainer() || userId.equals(userContext.getUserId());
    }

    @Override
    public boolean canClaimExpertProfile(Long expertProfileId) throws NonexistentEntityException, IOException {
        if (userContext == null) return false;
        if (isPortalMaintainer()) return true;
        final List<ExpertClaimPreviewDto> expertsToClaim = expertClaimSearchService.findExpertsToClaim(userContext.getUserId());
        return expertsToClaim.stream()
                .anyMatch(expertClaimPreviewDto -> !expertClaimPreviewDto.isClaimed() && expertProfileId.equals(expertClaimPreviewDto.getExpertId()));
    }

    @Override
    public boolean canUnclaimExpertProfile(Long expertProfileId) throws NonexistentEntityException, IOException {
        if (userContext == null) return false;
        if (isPortalMaintainer()) return true;
        final List<ExpertClaimPreviewDto> expertsToClaim = expertClaimSearchService.findExpertsToClaim(userContext.getUserId());
        return expertsToClaim.stream()
                .anyMatch(expertClaimPreviewDto -> expertClaimPreviewDto.isClaimed() && !expertClaimPreviewDto.isClaimedByAnotherUser() && expertProfileId.equals(expertClaimPreviewDto.getExpertId()));
    }


    @Override
    public boolean canClaimUserProfile(Long userId) throws IOException {
        if (userContext == null) return false;
        if (isPortalMaintainer()) return true;
        final List<ExpertClaimPreviewDto> expertsToClaim = expertClaimSearchService.findExpertsToClaim(userContext.getUserId());
        return expertsToClaim.stream()
                .anyMatch(expertClaimPreviewDto -> !expertClaimPreviewDto.isClaimed() && userId.equals(expertClaimPreviewDto.getUserId()));
    }

    @Override
    public boolean canUnclaimUserProfile(Long userId) throws IOException {
        if (userContext == null) return false;
        if (isPortalMaintainer()) return true;
        final List<ExpertClaimPreviewDto> expertsToClaim = expertClaimSearchService.findExpertsToClaim(userContext.getUserId());
        return expertsToClaim.stream()
                .anyMatch(expertClaimPreviewDto -> expertClaimPreviewDto.isClaimed() && expertClaimPreviewDto.isClaimedByAnotherUser() && userId.equals(expertClaimPreviewDto.getUserId()));
    }

    @Override
    public boolean canCreateUserItem(Long userId) {
        if (userContext == null) return false;
        return isPortalMaintainer() || userContext.getUserId().equals(userId);
    }

    @Override
    public boolean canSeeItem(Long itemId) {
        if (isPortalMaintainer()) return true;
        return storeSecurityHelperService.canUserSeeItem(itemId, userContext);
    }

    @Override
    public boolean canSeeItemsWithConfidentiality(Long organizationId, Set<Confidentiality> confidentiality) {
        if (!canSeeOrganizationEvidence(organizationId)) return false;
        if (confidentiality.contains(Confidentiality.SECRET)) {
            return isOrganizationEditor(organizationId);
        }
        if (confidentiality.contains(Confidentiality.CONFIDENTIAL)) {
            return isOrganizationViewer(organizationId);
        }
        return true;
    }

    @Override
    public boolean canSeeProject(Long projectId) {
        if (isPortalAdmin()) return true;
        if (projectId != null) {
            return storeSecurityHelperService.canUserSeeProject(projectId, userContext);
        }
        return isPortalMaintainer() || isPremiumUser();
    }

    @Override
    public boolean canSeePrivateOrganizationProject(Long organizationId) {
        return isPortalMaintainer() || isOrganizationViewer(organizationId);
    }

    @Override
    public boolean canEditPrivateOrganizationProject(Long projectId, Long organizationId) {
        return canEditProject(projectId) && isOrganizationEditor(organizationId);
    }

    @Override
    public boolean canSeeOrganizationCollaborations(Long organizationId) {
        return isPortalMaintainer() || (isInfinityUser() && isOrganizationViewer(organizationId));
    }

    @Override
    public boolean canSeeAllProjects() {
        return isPortalMaintainer();
    }

    @Override
    public boolean canSeeUserSearchHistory(Long userId) {
        if (userContext == null) return false;
        return isPortalMaintainer() || userId.equals(userContext.getUserId());
    }

    @Override
    public boolean canUserEditSearchHistory(Long searchHistId) {
        final boolean basicUser = isBasicUser();
        final Boolean userOwnerOfSearchHistoryItem = storeSecurityHelperService.isUserOwnerOfSearchHistoryItem(searchHistId, userContext);
        return isPortalAdmin() || (basicUser && userOwnerOfSearchHistoryItem);
    }

    @Override
    public boolean canSeeUserFollowedProfiles(Long userId) {
        if (userContext == null) return false;
        return isPortalAdmin() || userId.equals(userContext.getUserId());
    }

    @Override
    public boolean canManageUserFollowedProfiles(Long userId) {
        if (userContext == null) return false;
        return isPortalAdmin() || (userId.equals(userContext.getUserId()) && isBasicUser());
    }

    @Override
    public boolean canSeeUserWorkspaces(Long userId) {
        if (userContext == null) return false;
        return isPortalAdmin() || userId.equals(userContext.getUserId());
    }

    @Override
    public boolean canManageBillingInfo(Long userId) {
        if (userContext == null) return false;
        return isPortalAdmin() || userId.equals(userContext.getUserId());
    }

    @Override
    public boolean canVerifyExpert() {
        if (userContext == null) return false;
        return isPortalMaintainer() || userContext.getOrganizationRoles().stream()
                .anyMatch(x -> x.getRoles().stream()
                        .anyMatch(y -> y.getRole().equals(OrganizationRole.ORGANIZATION_EDITOR)));
    }

    @Override
    public boolean canSearchWithFilters() {
        return isBasicUser();
    }

    @Override
    public boolean canExportSearch() {
        return isBasicUser();
    }

    @Override
    public boolean canPaginate() {
        return isBasicUser();
    }

    @Override
    public boolean canContactIndirectly() {
        return isBasicUser();
    }

    @Override
    public boolean canContactDirectly() {
        return isBasicUser();
    }

    @Override
    public boolean canManagePromoCodes() {
        return isPortalMaintainer();
    }

    @Override
    public boolean canManageUserOrganization(Long userId, Long organizationId) {
        return canEditProfile(IdUtil.convertToIntDataCode(userId)) || isOrganizationEditor(organizationId);
    }

    @Override
    public boolean canVerifyUserOrganization(Long organizationId) {
        return isOrganizationEditor(organizationId);
    }

    @Override
    public boolean canVerifyItemOrganization(Long organizationId) {
        return isOrganizationEditor(organizationId);
    }

    @Override
    public boolean canAdminOrganizations() {
        return isPortalMaintainer() || hasUserOneOfRoles(Role.DATA_CLEANER);
    }

    @Override
    public boolean canManageOrganizationLicences() {
        return isPortalMaintainer();
    }

    @Override
    public boolean canManageOrganizationRoles(Long organizationId) {
        if (userContext == null) return false;
        return isPortalMaintainer() || userContext.getOrganizationRoles().stream()
                .anyMatch(x -> x.getRoles().stream()
                        .anyMatch(y -> y.getRole().equals(OrganizationRole.ORGANIZATION_ADMIN)));
    }

    @Override
    public boolean canImpersonate() {
        return isPortalAdmin();
    }

    @Override
    public boolean canAdminUsers() {
        return isPortalMaintainer();
    }

    @Override
    public boolean canMangeOrganizationExperts(Long organizationId) {
        return isOrganizationEditor(organizationId);
    }

    @Override
    public boolean canCreateUser() {
        return userContext == null || isPortalMaintainer() || userContext.getOrganizationRoles().stream()
                .anyMatch(x -> x.getRoles().stream()
                        .anyMatch(y -> y.getRole().equals(OrganizationRole.ORGANIZATION_EDITOR)));
    }

    @Override
    public boolean checkUserCanAddCountriesOfInterestForUser(Long userId, Set<String> countryOfInterestCodes) {
        if (userId != null && (userContext == null || !canEditProfile(IdUtil.convertToIntDataCode(userId))))
            return false;
        if (isPortalMaintainer() || isBasicUser()) return true;
        return (userContext == null || (userContext.getCountryOfInterestCodes().isEmpty()) && countryOfInterestCodes.size() == 1) || userContext.getCountryOfInterestCodes().containsAll(countryOfInterestCodes);
    }

    @Override
    public boolean canVerifyProjectOrganization(Long organizationId) {
        return isOrganizationEditor(organizationId);
    }

    @Override
    public boolean canConfirmProject() {
        return isPortalMaintainer() || userContext.getOrganizationRoles().stream()
                .anyMatch(x -> x.getRoles().stream()
                        .anyMatch(y -> y.getRole().equals(OrganizationRole.ORGANIZATION_EDITOR)));
    }

    @Override
    public boolean canAccessCountries(Set<String> countryCodes) {
        if (isBasicUser()) {
            return true;
        }
        return userContext.getCountryOfInterestCodes().containsAll(countryCodes);
    }

    @Override
    public boolean canSetExpertRetired() {
        return isOrganizationEditor(null);
    }

    @Override
    public boolean canManagePersonalNotes() {
        return isBasicUser();
    }

    @Override
    public boolean canSeeAllInvestProjects() {
        return isPortalMaintainer();
    }

    @Override
    public boolean canSeeCommercializationProjectPreview(Long commercializationProjectId) {
        if (userContext == null) return false;
        if (isCommercializationAdmin()) return true;
        return false;
    }

    @Override
    public boolean canSeeCommercializationProjectDetail(Long commercializationProjectId) {
        if (isCommercializationAdmin()) return true;
        return storeSecurityHelperService.canUserSeeCommercializationProjectDetail(commercializationProjectId, userContext);
    }

    @Override
    public boolean canSetCommercializationProjectStatus(Long commercializationProjectId, Long stateId) {
        if (userContext == null) return false;
        if (isCommercializationAdmin()) return true;
        return false;
    }

    @Override
    public boolean canSeeAllCommercializationProjects() {
        if (userContext == null) return false;
        if (isCommercializationAdmin()) return true;
        return isPortalAdmin();
    }

    @Override
    public boolean canAdminCommercialization() {
        if (userContext == null) return false;
        if (isCommercializationAdmin()) return true;
        return false;
    }

    @Override
    public boolean canEditCommercializationProject(Long commercializationProjectId) {
        if (userContext == null) return false;
        if (isCommercializationAdmin()) return true;
        return storeSecurityHelperService.canUserEditCommercializationProject(commercializationProjectId, userContext);
    }

    @Override
    public boolean canCreateOrganizationCommercializationProject(Long organizationId) {
        if (userContext == null) return false;
        return isOrganizationEditor(organizationId);
    }

    @Override
    public boolean canSeeActiveCommercializationProjects() {
        return hasCommercializationAccess();
    }

    @Override
    public boolean canCreateCommercializationProject() {
        return isPortalMaintainer() || isCommercializationAdmin();
    }

    @Override
    public boolean canClaimExpertForAnotherUser(Long expertId, Long userId) {
        return isOrganizationEditor();
    }

    @Override
    public boolean canEditOrganizationStructure(Long organizationId) {
        return isOrganizationEditor(organizationId);
    }

    @Override
    public boolean canManageExpertClaims() {
        return isOrganizationEditor();
    }

    @Override
    public boolean canEditItem(ItemDetailDto itemDetailDto) {
        if (isPortalMaintainer()) return true;
        return (storeSecurityHelperService.canUserEditItem(itemDetailDto, userContext));
    }

    @Override
    public boolean canManageItemOrganization(Long itemId, Long organizationId) {
        if (isPortalMaintainer()) return true;
        if (isOrganizationEditor(organizationId)) return true;
        return storeSecurityHelperService.canUserManageItemOrganization(itemId, organizationId, userContext);
    }

    @Override
    public boolean canManageItemUser(Long itemId, Long userId) {
        if (isPortalMaintainer()) return true;
        return storeSecurityHelperService.canUserManageItemExperts(itemId, userId, userContext);
    }

    @Override
    public boolean canManageItemExpert(Long itemId, Long expertId) {
        if (isPortalMaintainer()) return true;
        return storeSecurityHelperService.canUserManageItemExperts(itemId, null, userContext);
    }

    @Override
    public boolean canSeeAllItems() {
        return isPortalMaintainer() || hasUserOneOfRoles(Role.DATA_CLEANER);
    }

    @Override
    public boolean canSeeAllExperts() {
        return isPortalMaintainer() || hasUserOneOfRoles(Role.DATA_CLEANER);
    }

    @Override
    public boolean canCreateOrganizationItem(Long organizationId) {
        return isOrganizationEditor(organizationId);
    }

    @Override
    public boolean canSeeOrganizationEvidence(Long organizationId) {
        return isOrganizationViewer(organizationId);
    }

    @Override
    public boolean canConfirmItem(Long originalItemId) throws IOException {
        return isPortalMaintainer() || userContext.getOrganizationRoles().stream()
                .anyMatch(x -> x.getRoles().stream()
                        .anyMatch(y -> y.getRole().equals(OrganizationRole.ORGANIZATION_EDITOR))) ||
                storeSecurityHelperService.doesUserOwnOriginalItem(originalItemId, userContext);
    }

    @Override
    public boolean canCreateProject() {
        return isPortalMaintainer();
    }

    @Override
    public boolean canCreateOrganizationProject(Long organizationId) {
        return isPortalMaintainer() || isOrganizationEditor(organizationId);
    }

    @Override
    public boolean canEditProject(Long projectId) {
        return isPortalMaintainer() || storeSecurityHelperService.canUserEditProject(projectId, userContext);
    }

    @Override
    public boolean canEditProject(ProjectDto projectDto) {
        return isPortalMaintainer() || storeSecurityHelperService.canUserEditProject(projectDto, userContext);
    }

    @Override
    public boolean canCreateItem() {
        return isPortalMaintainer();
    }

    @Override
    public boolean canEditSearchStatistics() {
        return isPortalMaintainer();
    }

    @Override
    public boolean canImportEvidence(Long organizationId) {
        return isOrganizationEditor(organizationId) || hasUserOneOfRoles(Role.DATA_IMPORTER);
    }

    @Override
    public boolean canDeleteImportEvidence(Long organizationId) {
        return isOrganizationEditor(organizationId) || hasUserOneOfRoles(Role.DATA_IMPORTER);
    }

    @Override
    public boolean canSeeSearchStatistics() {
        return isPortalMaintainer();
    }

    @Override
    public boolean canSeeSearchTranslations() {
        return isPortalMaintainer();
    }

    @Override
    public boolean canEditSearchTranslations() {
        return isPortalMaintainer();
    }

    @Override
    public boolean canSeeExpertsStatistics() {
        return isPortalMaintainer();
    }

    @Override
    public boolean canSeeOrganizationPublicProfile(Long organizationId) {
        return storeSecurityHelperService.hasOrganizationPublicProfile(organizationId);
    }

    @Override
    public boolean canEditOrganizationPublicProfile(Long organizationId) {
        return isOrganizationEditor(organizationId) || isPortalAdmin();
    }

    @Override
    public boolean canSeeWidget(Long organizationId, WidgetType widgetType) {
        return true;
        //return storeSecurityHelperService.hasWidget(organizationId, widgetType);
    }

    @Override
    public boolean canEditWidgetSettings(Long organizationId) {
        return isOrganizationEditor(organizationId) || isPortalAdmin();
    }

    @Override
    public boolean canSetSearchAllowance() {
        return isPortalMaintainer();
    }


    private boolean isPortalAdmin() {
        return hasUserOneOfRoles(Role.PORTAL_ADMIN);
    }

    private boolean isCommercializationAdmin() {
        return hasUserOneOfRoles(Role.PORTAL_ADMIN, Role.COMMERCIALIZATION_ADMIN);
    }

    private boolean hasCommercializationAccess() {
        return isCommercializationAdmin() || hasUserOneOfRoles(Role.COMMERCIALIZATION_INVESTOR);
    }

    private boolean isPortalMaintainer() {
        return hasUserOneOfRoles(Role.PORTAL_MAINTAINER, Role.PORTAL_ADMIN);
    }

    private boolean isBasicUser() {
        return isPortalMaintainer() || hasUserOneOfRoles(Role.USER, Role.BASIC_USER, Role.PREMIUM_USER, Role.INFINITY_USER, Role.DATA_CLEANER);
    }

    private boolean isPremiumUser() {
        return isPortalMaintainer() || hasUserOneOfRoles(Role.PREMIUM_USER, Role.INFINITY_USER, Role.DATA_CLEANER);
    }

    private boolean isInfinityUser() {
        return isPortalAdmin() || hasUserOneOfRoles(Role.INFINITY_USER);
    }

    private boolean hasUserOneOfRoles(Role... roles) {
        if (userContext == null) return false;
        for (Role role : roles) {
            if (userContext.getRoles().contains(role)) return true;
        }
        return false;
    }

    private boolean isOrganizationEditor() {
        return isOrganizationEditor(null);
    }

    private boolean isOrganizationEditor(Long organizationId) {
        if (userContext == null) return false;
        if (isPortalMaintainer()) return true;
        return userContext.getOrganizationRoles().stream()
                .anyMatch(x -> x.getRoles().stream()
                        .anyMatch(y -> OrganizationRole.ORGANIZATION_EDITOR.equals(y.getRole()) && (organizationId == null || x.getOrganizationId().equals(organizationId))));
    }

    private boolean isOrganizationViewer(Long organizationId) {
        if (userContext == null) return false;
        if (isPortalMaintainer() || hasUserOneOfRoles(Role.DATA_CLEANER)) return true;
        if (isOrganizationEditor(organizationId)) return true;
        return userContext.getOrganizationRoles().stream()
                .anyMatch(x -> x.getRoles().stream()
                        .anyMatch(y -> OrganizationRole.ORGANIZATION_VIEWER.equals(y.getRole()) && (organizationId == null || x.getOrganizationId().equals(organizationId))));
    }
}
