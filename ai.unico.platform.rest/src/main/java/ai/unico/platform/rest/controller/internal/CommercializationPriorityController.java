package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.CommercializationPriorityTypeDto;
import ai.unico.platform.store.api.service.CommercializationPriorityService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("internal/commercialization/priorities")
public class CommercializationPriorityController {

    private final CommercializationPriorityService commercializationPriorityService = ServiceRegistryProxy.createProxy(CommercializationPriorityService.class);

    @RequestMapping(method = RequestMethod.GET)
    public List<CommercializationPriorityTypeDto> findAllPriorities(@RequestParam(required = false) boolean includeInactive) {
        return commercializationPriorityService.findAvailableCommercializationPriorityTypes(includeInactive);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Long createPriority(@RequestBody CommercializationPriorityTypeDto commercializationPriorityTypeDto,
                               UserContext userContext,
                               @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canAdminCommercialization()) throw new UserUnauthorizedException();
        return commercializationPriorityService.createPriority(commercializationPriorityTypeDto, userContext);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{priorityId}")
    public void updatePriority(@RequestBody CommercializationPriorityTypeDto commercializationPriorityTypeDto,
                               @PathVariable Long priorityId,
                               UserContext userContext,
                               @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canAdminCommercialization()) throw new UserUnauthorizedException();
        commercializationPriorityService.updatePriority(priorityId, commercializationPriorityTypeDto, userContext);
    }
}
