package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.ProjectBudgetDto;
import ai.unico.platform.store.api.service.ProjectBudgetService;
import ai.unico.platform.store.api.service.ProjectService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("internal/projects/{projectId}/budget")
public class ProjectBudgetController {
    private final ProjectBudgetService projectBudgetService = ServiceRegistryProxy.createProxy(ProjectBudgetService.class);
    private final ProjectService projectService = ServiceRegistryProxy.createProxy(ProjectService.class);

    @RequestMapping(method = RequestMethod.PUT)
    public void updateProjectBudget(@PathVariable Long projectId,
                                    @RequestParam Long organizationId,
                                    @RequestParam Integer year,
                                    @RequestBody ProjectBudgetDto projectBudgetDto,
                                    @ModelAttribute UserSecurityHelper userSecurityHelper,
                                    UserContext userContext) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditProject(projectId)) throw new UserUnauthorizedException();
        projectBudgetService.updateProjectBudget(projectId, organizationId, year, projectBudgetDto, userContext);
        projectService.loadAndIndexProject(projectId);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteProjectBudget(@PathVariable Long projectId,
                                    @RequestParam Long organizationId,
                                    @RequestParam Integer year,
                                    @ModelAttribute UserSecurityHelper userSecurityHelper,
                                    UserContext userContext) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditProject(projectId)) throw new UserUnauthorizedException();
        projectBudgetService.deleteProjectBudget(projectId, organizationId, year, userContext);
        projectService.loadAndIndexProject(projectId);
    }
}
