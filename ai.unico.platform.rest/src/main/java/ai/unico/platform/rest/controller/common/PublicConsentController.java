package ai.unico.platform.rest.controller.common;

import ai.unico.platform.store.api.dto.ConsentDto;
import ai.unico.platform.store.api.service.UserConsentService;
import ai.unico.platform.util.ServiceRegistryUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("common/consents")
public class PublicConsentController {
    @RequestMapping(method = RequestMethod.GET)
    List<ConsentDto> findConsents() {
        final UserConsentService service = ServiceRegistryUtil.getService(UserConsentService.class);
        return service.findAllConsents();
    }
}
