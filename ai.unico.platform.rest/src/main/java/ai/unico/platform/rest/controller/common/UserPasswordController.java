package ai.unico.platform.rest.controller.common;

import ai.unico.platform.ume.api.service.UserPasswordResetService;
import ai.unico.platform.ume.api.service.UserPasswordService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NoLoggedInUserException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.OutdatedEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("common/password")
public class UserPasswordController {

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity createPassword(@RequestBody Map<String, String> parameters,
                                         UserContext userContext) throws OutdatedEntityException, NonexistentEntityException {
        final String token = parameters.get("token");
        final String email = parameters.get("email");
        final String password = parameters.get("password");
        final UserPasswordService userPasswordService = ServiceRegistryUtil.getService(UserPasswordService.class);

        userPasswordService.doSetPassword(email, token, password, userContext);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity changePassword(UserContext userContext, @RequestBody Map<String, String> parameters) throws NonexistentEntityException, NoLoggedInUserException {
        if (userContext == null) throw new NoLoggedInUserException();
        final String currentPassword = parameters.get("currentPassword");
        final String newPassword = parameters.get("password");
        final Long userId = userContext.getUserId();
        final UserPasswordService userPasswordService = ServiceRegistryUtil.getService(UserPasswordService.class);

        userPasswordService.doChangePassword(userId, currentPassword, newPassword, userContext);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(path = "/new", method = RequestMethod.POST)
    public ResponseEntity applyForResetPassword(@RequestBody Map<String, String> parameters) throws NonexistentEntityException {
        final String email = parameters.get("email");
        final UserPasswordResetService service = ServiceRegistryUtil.getService(UserPasswordResetService.class);
        service.applyForResetPassword(email);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
