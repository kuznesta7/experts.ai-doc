package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.ProjectUserDto;
import ai.unico.platform.store.api.service.ProjectService;
import ai.unico.platform.store.api.service.ProjectUserService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("internal")
public class ProjectUserController {

    private final ProjectUserService projectUserService = ServiceRegistryProxy.createProxy(ProjectUserService.class);
    private final ProjectService projectService = ServiceRegistryProxy.createProxy(ProjectService.class);

    @RequestMapping(method = RequestMethod.PUT, path = "/projects/{projectId}/users/{userId}")
    public void saveProjectUser(@PathVariable Long projectId,
                                @PathVariable Long userId,
                                @RequestParam(required = false) String label,
                                UserContext userContext,
                                @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NonexistentEntityException {
        if (!userSecurityHelper.canEditProject(projectId)) throw new UserUnauthorizedException();
        final ProjectUserDto projectUserDto = new ProjectUserDto();
        projectUserDto.setProjectId(projectId);
        projectUserDto.setUserId(userId);
        projectUserDto.setLabel(label);
        projectUserService.saveUserToProject(projectUserDto, userContext);
        projectService.loadAndIndexProject(projectId);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/projects/{projectId}/users/{userId}")
    public void deleteProjectUser(@PathVariable Long projectId,
                                  @PathVariable Long userId,
                                  UserContext userContext,
                                  @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NonexistentEntityException {
        if (!userSecurityHelper.canEditProject(projectId)) throw new UserUnauthorizedException();
        final ProjectUserDto projectUserDto = new ProjectUserDto();
        projectUserDto.setProjectId(projectId);
        projectUserDto.setUserId(userId);
        projectUserService.removeUserFromProject(projectUserDto, userContext);
        projectService.loadAndIndexProject(projectId);
    }
}
