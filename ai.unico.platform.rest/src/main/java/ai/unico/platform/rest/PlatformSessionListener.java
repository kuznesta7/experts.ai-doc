package ai.unico.platform.rest;

import ai.unico.platform.rest.factory.UserContextFactory;
import ai.unico.platform.util.model.UserContext;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.ArrayList;
import java.util.List;

public class PlatformSessionListener implements HttpSessionListener {

    private static List<HttpSession> sessions = new ArrayList<>();

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        final HttpSession session = se.getSession();
        //With 0 value session should never timeout.
        session.setMaxInactiveInterval(0);
        sessions.add(session);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        final HttpSession session = se.getSession();
        sessions.remove(session);
    }

    public static void logoutRedundantUsers(Long userId, Integer maxUserSessions) {
        int sessionCount = 0;
        for (int i = sessions.size() - 1; i >= 0; i--) {
            final HttpSession session = sessions.get(i);
            if (session == null) continue;
            final Object attribute = session.getAttribute(UserContextFactory.USER_CONTEXT_SESSION_KEY);
            if (attribute == null) continue;
            final UserContext userContext = (UserContext) attribute;
            if (userContext.getImpersonatorUserId() != null) continue;
            if (userContext.getUserId().equals(userId)) {
                sessionCount++;
                if (sessionCount > maxUserSessions) {
                    session.removeAttribute(UserContextFactory.USER_CONTEXT_SESSION_KEY);
                    session.invalidate();
                }
            }
        }
    }
}
