package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.CommercializationStatusTypeDto;
import ai.unico.platform.store.api.service.CommercializationStatusService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("internal/commercialization/statuses")
public class CommercializationStatusController {

    private final CommercializationStatusService commercializationStatusService = ServiceRegistryProxy.createProxy(CommercializationStatusService.class);

    @RequestMapping(method = RequestMethod.POST)
    public Long createStatus(@RequestBody CommercializationStatusTypeDto commercializationStatusTypeDto,
                             UserContext userContext,
                             @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canAdminCommercialization()) throw new UserUnauthorizedException();
        return commercializationStatusService.createStatus(commercializationStatusTypeDto, userContext);
    }

    @RequestMapping(path = "{commercializationStatusTypeId}", method = RequestMethod.PUT)
    public void updateStatus(@RequestBody CommercializationStatusTypeDto commercializationStatusTypeDto,
                             @PathVariable Long commercializationStatusTypeId,
                             UserContext userContext,
                             @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canAdminCommercialization()) throw new UserUnauthorizedException();
        commercializationStatusService.updateStatus(commercializationStatusTypeId, commercializationStatusTypeDto, userContext);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<CommercializationStatusTypeDto> findAllStatuses(@RequestParam(required = false) boolean includeInactive) {
        return commercializationStatusService.findAvailableStatuses(includeInactive);
    }
}
