package ai.unico.platform.rest.security;

import ai.unico.platform.search.api.filter.ExpertSearchFilter;
import ai.unico.platform.util.exception.NoLoggedInUserException;
import ai.unico.platform.util.exception.UserUnauthorizedException;

public interface SecurityHelper {

    void checkAccessibility(ExpertSearchFilter expertSearchFilter, UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NoLoggedInUserException;

}
