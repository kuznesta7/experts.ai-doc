package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.search.api.dto.UserExpertProfileSearchDetailDto;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.service.ExpertSearchService;
import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.service.*;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NoLoggedInUserException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@RestController
@RequestMapping("internal/users/{userId}/organizations")
public class UserOrganizationController {

    private final UserOrganizationService userOrganizationService = ServiceRegistryProxy.createProxy(UserOrganizationService.class);
    private final UserProfileService userProfileService = ServiceRegistryProxy.createProxy(UserProfileService.class);
    private final OrganizationService organizationService = ServiceRegistryProxy.createProxy(OrganizationService.class);
    private final ItemService itemService = ServiceRegistryProxy.createProxy(ItemService.class);
    private final ClaimProfileService claimProfileService = ServiceRegistryProxy.createProxy(ClaimProfileService.class);

    @RequestMapping(method = RequestMethod.PUT)
    public void updateUserOrganizations(@PathVariable Long userId,
                                        @RequestBody List<OrganizationBaseDto> organizationDtos,
                                        UserContext userContext,
                                        @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException, NoLoggedInUserException {
        if (!userSecurityHelper.canEditUserProfile(userId)) throw new UserUnauthorizedException();
        userOrganizationService.updateOrganizationsForUser(userId, organizationDtos, userContext);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/{organizationId}")
    public void addOrganization(@PathVariable("userId") String expertCode,
                                @PathVariable Long organizationId,
                                @RequestParam(required = false, defaultValue = "false") boolean verify,
                                UserContext userContext,
                                @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException, NoLoggedInUserException, IOException {
        Long expertId, userId = null;
        if (IdUtil.isIntId(expertCode)) {
            userId = IdUtil.getIntDataId(expertCode);
            if (!userSecurityHelper.canManageUserOrganization(userId, organizationId)
                    && (!verify || userSecurityHelper.canVerifyUserOrganization(organizationId)))
                throw new UserUnauthorizedException();
        }
        if (IdUtil.isExtId(expertCode)) {
            expertId = IdUtil.getExtDataId(expertCode);
            if (!userSecurityHelper.canManageUserOrganization(expertId, organizationId)
                    && (!verify || userSecurityHelper.canVerifyUserOrganization(organizationId)))
                throw new UserUnauthorizedException();
            // create user profile from expert
            ExpertSearchResultFilter filter = new ExpertSearchResultFilter();
            final ExpertSearchService service = ServiceRegistryUtil.getService(ExpertSearchService.class);
            assert service != null;
            final UserExpertProfileSearchDetailDto expertProfileSearch = service.findSearchByExpertiseDetail(expertCode, filter);
            userId = claimProfileService.DWHExpertToSTUser(expertId, expertProfileSearch.getName(), userContext);
        }

        // FIXME - ST profiles has only one organization
        userOrganizationService.addOrganization(userId, organizationId, verify, userContext);
        List<Long> itemIds = userOrganizationService.addUserItemsToOrganization(userId, organizationId, verify, userContext);
        organizationService.loadAndIndexOrganization(organizationId);
        if (! itemIds.isEmpty())
            itemService.loadAndIndexItems(new HashSet<>(itemIds));
    }
//
//    @RequestMapping(method = RequestMethod.PUT, path = "/{organizationId}")
//    public void updateOrganization(@PathVariable Long userId,
//                                   @PathVariable Long organizationId,
//                                   @RequestParam(required = false, defaultValue = "false") boolean favorite,
//                                   UserContext userContext,
//                                   @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException, NoLoggedInUserException {
//        userSecurityHelper.canEditUserProfile(userId);
//        userOrganizationService.setFavoriteUserUserOrganization(userId, organizationId, favorite, userContext);
//    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{organizationId}")
    public void removeOrganization(@PathVariable Long userId,
                                   @PathVariable Long organizationId,
                                   UserContext userContext,
                                   @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException, NoLoggedInUserException {
        if (!userSecurityHelper.canManageUserOrganization(userId, organizationId))
            throw new UserUnauthorizedException();
        userOrganizationService.removeOrganization(Collections.singleton(userId), organizationId, userContext);
        userProfileService.loadAndIndexUserProfile(userId);
    }
}
