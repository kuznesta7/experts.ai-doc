package ai.unico.platform.rest.util;

import ai.unico.platform.util.dto.FileDto;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.net.URLEncoder;

public class ControllerUtil {

    public static ResponseEntity<String> toCSVFileResponse(String content, String fileName) {
        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "text/csv; charset=Windows-1250");
        addFileNameHeader(fileName, responseHeaders);
        return new ResponseEntity<String>(content, responseHeaders, HttpStatus.OK);
    }

    public static HttpEntity<byte[]> toFileResponse(FileDto fileDto) {
        if (fileDto == null) return null;
        return toFileResponse(fileDto.getName(), fileDto.getContent());
    }

    public static HttpEntity<byte[]> toFileResponse(String fileName, byte[] content) {
        final HttpHeaders responseHeaders = new HttpHeaders();
        addFileNameHeader(fileName, responseHeaders);

        if (fileName.endsWith(".docx")) {
            responseHeaders.add("Content-Type", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        } else {
            final String type = URLConnection.guessContentTypeFromName(fileName);
            responseHeaders.add("Content-Type", type);
        }

        return new HttpEntity<byte[]>(content, responseHeaders);
    }

    private static void addFileNameHeader(String fileName, HttpHeaders responseHeaders) {
        try {
            responseHeaders.add("content-disposition", "attachment; filename*=UTF-8''" + URLEncoder.encode(fileName.replace(" ", "_"), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace(System.err);
        }
    }
}