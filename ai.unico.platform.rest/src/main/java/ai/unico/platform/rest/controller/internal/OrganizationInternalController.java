package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.service.OrganizationService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("internal/organizations/{organizationId}/types/")
public class OrganizationInternalController {

    private final OrganizationService organizationService = ServiceRegistryProxy.createProxy(OrganizationService.class);

    @RequestMapping(method = RequestMethod.PUT, path = "/{organizationType}")
    public void changeOrganizationType(@PathVariable Long organizationId,
                                       @PathVariable Long organizationType,
                                       UserContext userContext,
                                       @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (! userSecurityHelper.canEditOrganizationPublicProfile(organizationId))
            throw new UserUnauthorizedException();
        organizationService.changeOrganizationType(organizationId, organizationType);
    }
}
