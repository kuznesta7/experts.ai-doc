package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.service.ProjectItemService;
import ai.unico.platform.store.api.service.ProjectService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.enums.PlatformDataSource;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("internal/projects/{projectId}/items")
public class ProjectItemController {

    private final ProjectItemService projectItemService = ServiceRegistryProxy.createProxy(ProjectItemService.class);
    private final ProjectService projectService = ServiceRegistryProxy.createProxy(ProjectService.class);

    @RequestMapping(method = RequestMethod.PUT, path = {"/st/{itemId}", "/dwh/{originalItemId}"})
    public void saveProjectUser(@PathVariable Long projectId,
                                @PathVariable(required = false) Long itemId,
                                @PathVariable(required = false) Long originalItemId,
                                UserContext userContext,
                                @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NonexistentEntityException {
        if (!userSecurityHelper.canEditProject(projectId)) throw new UserUnauthorizedException();
        if (itemId != null) {
            projectItemService.addItemToProject(projectId, itemId, PlatformDataSource.STORE, userContext);
        } else {
            projectItemService.addItemToProject(projectId, originalItemId, PlatformDataSource.DATA_WAREHOUSE, userContext);
        }
        projectService.loadAndIndexProject(projectId);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = {"/st/{itemId}", "/dwh/{originalItemId}"})
    public void deleteProjectUser(@PathVariable Long projectId,
                                  @PathVariable(required = false) Long itemId,
                                  @PathVariable(required = false) Long originalItemId,
                                  UserContext userContext,
                                  @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NonexistentEntityException {
        if (!userSecurityHelper.canEditProject(projectId)) throw new UserUnauthorizedException();
        if (itemId != null) {
            projectItemService.removeItemFromProject(projectId, itemId, PlatformDataSource.STORE, userContext);
        } else {
            projectItemService.removeItemFromProject(projectId, originalItemId, PlatformDataSource.DATA_WAREHOUSE, userContext);
        }
        projectService.loadAndIndexProject(projectId);
    }

}
