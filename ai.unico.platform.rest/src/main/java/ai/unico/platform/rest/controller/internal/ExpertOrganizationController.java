package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.service.ExpertOrganizationService;
import ai.unico.platform.store.api.service.ItemService;
import ai.unico.platform.store.api.service.OrganizationService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@RestController
@RequestMapping("internal/experts/{expertId}/organizations")
public class ExpertOrganizationController {

    private final ExpertOrganizationService expertOrganizationService = ServiceRegistryProxy.createProxy(ExpertOrganizationService.class);
    private final OrganizationService organizationService = ServiceRegistryProxy.createProxy(OrganizationService.class);
    private final ItemService itemService = ServiceRegistryProxy.createProxy(ItemService.class);

    @RequestMapping(method = RequestMethod.PUT, path = "/{organizationId}")
    public void addExpertOrganization(@PathVariable Long expertId,
                                      @PathVariable Long organizationId,
                                      UserContext userContext,
                                      @ModelAttribute UserSecurityHelper userSecurityHelper,
                                      @RequestParam boolean verified) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canMangeOrganizationExperts(organizationId)) throw new UserUnauthorizedException();
        expertOrganizationService.addExpertOrganization(expertId, organizationId, verified, userContext);
        List<Long> itemIds = expertOrganizationService.addExpertItemsToOrganization(expertId, organizationId, true, userContext);
        organizationService.loadAndIndexOrganization(organizationId);
        if (! itemIds.isEmpty())
            itemService.loadAndIndexItems(new HashSet<>(itemIds));
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{organizationId}")
    public void removeExpertFromOrganization(@PathVariable Long expertId,
                                             @PathVariable Long organizationId,
                                             UserContext userContext,
                                             @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canMangeOrganizationExperts(organizationId)) throw new UserUnauthorizedException();
        expertOrganizationService.removeOrganizationExperts(Collections.singleton(expertId), organizationId, userContext);
    }

}
