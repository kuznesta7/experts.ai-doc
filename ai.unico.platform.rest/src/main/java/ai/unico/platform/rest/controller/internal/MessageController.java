package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.InternalMessageDto;
import ai.unico.platform.store.api.dto.MessageDto;
import ai.unico.platform.store.api.filter.MessageFilter;
import ai.unico.platform.store.api.service.MessageService;
import ai.unico.platform.store.api.wrapper.MessageWrapper;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("internal/messages")
public class MessageController {

    private final MessageService messageService = ServiceRegistryProxy.createProxy(MessageService.class);

    @RequestMapping(method = RequestMethod.POST)
    public void sendMessage(@RequestBody MessageDto messageDto,
                            UserContext userContext,
                            @ModelAttribute UserSecurityHelper userSecurityHelper,
                            @RequestParam(required = false) Long searchId) throws NonexistentEntityException, UserUnauthorizedException {
        if ((messageDto.isDirect() && !userSecurityHelper.canContactDirectly()) ||
                (!messageDto.isDirect() && !userSecurityHelper.canContactIndirectly())) {
            throw new UserUnauthorizedException();
        }
        messageService.sendMessage(messageDto, userContext, searchId);
    }

    @RequestMapping(path = "/request", method = RequestMethod.POST)
    public void sendInternalMessage(@RequestBody InternalMessageDto messageDto,
                            UserContext userContext) throws NonexistentEntityException {
        messageService.sendInternalMessage(messageDto, userContext);
    }

    @RequestMapping(path = "/toOrganization", method = RequestMethod.POST)
    public void sendMessageToOrganization(@RequestBody InternalMessageDto messageDto,
                            UserContext userContext,
                            @ModelAttribute UserSecurityHelper userSecurityHelper) {
        messageService.sendMessageToOrganization(messageDto, userContext);
    }

    @RequestMapping(path = "/{messageId}", method = RequestMethod.GET)
    public MessageDto getMessage(@PathVariable("messageId") Long messageId,
                                 UserContext userContext) throws NonexistentEntityException, UserUnauthorizedException {
        return messageService.findMessage(messageId, userContext);
    }

    @RequestMapping(method = RequestMethod.GET)
    public MessageWrapper getMessages(UserContext userContext,
                                      @ModelAttribute UserSecurityHelper userSecurityHelper,
                                      @RequestParam(required = false) String query,
                                      @RequestParam(required = false) String userFrom,
                                      @RequestParam(required = false) String userTo,
                                      @RequestParam Integer limit,
                                      @RequestParam Integer page) {
        final MessageFilter messageFilter = new MessageFilter(query, userFrom, userTo, limit, page);
        return new MessageWrapper();
    }
}
