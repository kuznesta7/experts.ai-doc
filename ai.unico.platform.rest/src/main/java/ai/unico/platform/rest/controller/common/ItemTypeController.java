package ai.unico.platform.rest.controller.common;

import ai.unico.platform.extdata.dto.DWHItemTypeDto;
import ai.unico.platform.extdata.service.DWHItemTypeService;
import ai.unico.platform.store.api.dto.ItemTypeDto;
import ai.unico.platform.store.api.dto.ItemTypeGroupDto;
import ai.unico.platform.store.api.service.ItemTypeService;
import ai.unico.platform.util.ServiceRegistryUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("common/item-types/")
public class ItemTypeController {

    @RequestMapping(method = RequestMethod.GET)
    public List<ItemTypeDto> findResultTypes() {
        final ItemTypeService service = ServiceRegistryUtil.getService(ItemTypeService.class);
        return service.findItemTypes();
    }

    @RequestMapping(method = RequestMethod.GET, path = "groups/")
    public List<ItemTypeGroupDto> findResultTypeGroups() {
        final ItemTypeService service = ServiceRegistryUtil.getService(ItemTypeService.class);
        return service.findItemTypeGroups();
    }

    @RequestMapping(method =  RequestMethod.GET, path = "dwh")
    public List<DWHItemTypeDto> findResultTypesDWH() {
        final DWHItemTypeService service = ServiceRegistryUtil.getService(DWHItemTypeService.class);
        return service.findItemTypes();
    }}
