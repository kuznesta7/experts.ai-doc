package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.factory.UserContextFactory;
import ai.unico.platform.ume.api.exception.OrderAlreadyDeliveredException;
import ai.unico.platform.ume.api.exception.TrialExpiredException;
import ai.unico.platform.ume.api.exception.TrialUnavailableException;
import ai.unico.platform.ume.api.exception.WebPayUnavailableException;
import ai.unico.platform.ume.api.service.ProductService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path = "internal/users/{userId}/products")
public class UserProductController {

    private ProductService productService = ServiceRegistryProxy.createProxy(ProductService.class);

    @RequestMapping(method = RequestMethod.POST, path = "/{productVariantId}")
    public void buyProduct(@PathVariable Long userId, @PathVariable Long productVariantId, UserContext userContext) throws WebPayUnavailableException, OrderAlreadyDeliveredException {
        productService.purchaseProduct(productVariantId, userId, userContext);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/{productId}/trials")
    public void tryProduct(@PathVariable Long userId, @PathVariable Long productId, UserContext userContext,
                           HttpServletRequest request) throws TrialUnavailableException, TrialExpiredException {
        productService.tryProduct(productId, userId, userContext);
        request.getSession().setAttribute(UserContextFactory.USER_CONTEXT_SESSION_KEY, null);
    }
}
