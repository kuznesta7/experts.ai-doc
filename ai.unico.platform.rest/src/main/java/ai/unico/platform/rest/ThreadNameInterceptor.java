package ai.unico.platform.rest;

import ai.unico.platform.store.api.dto.HttpRequestDto;
import ai.unico.platform.store.api.service.HttpRequestPersistService;
import ai.unico.platform.util.ServiceRegistryUtil;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

public class ThreadNameInterceptor extends HandlerInterceptorAdapter {

    public static final String OLD_THREAD_NAME = "OLD_THREAD_NAME";
    public static final String START_REQUEST_HANDLING_TIME = "START_REQUEST_HANDLING_TIME";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        final Thread currentThread = Thread.currentThread();

        final String oldName = currentThread.getName();
        request.setAttribute(OLD_THREAD_NAME, oldName);
        request.setAttribute(START_REQUEST_HANDLING_TIME, System.currentTimeMillis());

        final String newName = request.getMethod() + " " + request.getRequestURI() + " " + request.getRemoteUser();

        currentThread.setName(newName);

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        final Thread currentThread = Thread.currentThread();

        final String oldName = request.getAttribute(OLD_THREAD_NAME).toString();

        final Long startTime = (Long) request.getAttribute(START_REQUEST_HANDLING_TIME);

        final HttpRequestPersistService httpRequestPersistService = ServiceRegistryUtil.getService(HttpRequestPersistService.class);
        final HttpRequestDto httpRequestDto = new HttpRequestDto();
        httpRequestDto.setRemoteUser(request.getRemoteUser());
        httpRequestDto.setMethod(request.getMethod());
        httpRequestDto.setSystem(request.getServletPath());
        httpRequestDto.setDuration((int) (System.currentTimeMillis() - startTime));
        httpRequestDto.setAccepted(new Date(startTime));
        httpRequestDto.setResult(Integer.toString(response.getStatus()));
        httpRequestDto.setPath(request.getRequestURI());
        httpRequestDto.setQuery(request.getQueryString());
        httpRequestPersistService.save(httpRequestDto);

        currentThread.setName(oldName);
    }
}