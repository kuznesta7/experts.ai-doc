package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.ume.api.filter.AdminUserFilter;
import ai.unico.platform.ume.api.service.UserAdministrationService;
import ai.unico.platform.ume.api.wrapper.AdminUserWrapper;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("internal/users")
public class AdminUserController {

    private final UserAdministrationService userAdministrationService = ServiceRegistryProxy.createProxy(UserAdministrationService.class);

    @RequestMapping(method = RequestMethod.GET)
    public AdminUserWrapper findUsersForAdmin(@ModelAttribute AdminUserFilter filter,
                                              UserContext userContext,
                                              @ModelAttribute UserSecurityHelper userSecurityHelper) throws IOException, UserUnauthorizedException {
        if (!userSecurityHelper.canAdminUsers()) throw new UserUnauthorizedException();
        return userAdministrationService.findUsersForAdmin(filter);
    }
}
