package ai.unico.platform.rest.controller.common;

import ai.unico.platform.store.api.service.TagService;
import ai.unico.platform.util.ServiceRegistryUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("common/tags")
public class TagController {

    @RequestMapping(method = RequestMethod.GET)
    public List<String> findTags(@RequestParam("query") String query) {
        final TagService service = ServiceRegistryUtil.getService(TagService.class);
        return service.findRelevantTags(query);
    }
}
