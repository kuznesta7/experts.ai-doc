package ai.unico.platform.rest.controller.common;

import ai.unico.platform.ume.api.dto.UserLoginDto;
import ai.unico.platform.util.exception.NoLoggedInUserException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("common/login")
public class LoginController {

    @Autowired
    private HttpServletRequest httpServletRequest;

    @RequestMapping(method = RequestMethod.POST)
    public void doLogin(@RequestBody UserLoginDto userLoginDto, UserContext userContext) throws NoLoggedInUserException {
        if (userContext != null && userContext.getUserId() != null) {
            return;
        }
        try {
            httpServletRequest.login(userLoginDto.getUsername(), userLoginDto.getPassword());
        } catch (Exception e) {
            throw new NoLoggedInUserException();
        }
    }
}
