package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.service.ExpertOrganizationService;
import ai.unico.platform.store.api.service.ExpertToUserService;
import ai.unico.platform.store.api.service.UserOrganizationService;
import ai.unico.platform.store.api.service.UserProfileService;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Set;

@RestController
@RequestMapping("internal/evidence/organizations/{organizationId}/")
public class OrganizationExpertController {

    private final UserProfileService userProfileService = ServiceRegistryProxy.createProxy(UserProfileService.class);
    private final ExpertOrganizationService expertOrganizationService = ServiceRegistryProxy.createProxy(ExpertOrganizationService.class);
    private final UserOrganizationService userOrganizationService = ServiceRegistryProxy.createProxy(UserOrganizationService.class);
    private final ExpertToUserService expertToUserService = ServiceRegistryProxy.createProxy(ExpertToUserService.class);


    @RequestMapping(path = "expert/{expertCode}/retired", method = RequestMethod.PUT)
    public void retireExpertsUsers(@PathVariable Long organizationId,
                                   @ModelAttribute UserSecurityHelper userSecurityHelper,
                                   @PathVariable("expertCode") String expertCode,
                                   @RequestParam boolean retired,
                              UserContext userContext) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canMangeOrganizationExperts(organizationId)) throw new UserUnauthorizedException();
        if (IdUtil.isIntId(expertCode)) {
            Long userId = IdUtil.getIntDataId(expertCode);
            userOrganizationService.retireUserOrganization(userId, organizationId, retired, userContext);
            userProfileService.loadAndIndexUserProfile(userId);
        } else if (IdUtil.isExtId(expertCode)) {
            Long expertId = IdUtil.getExtDataId(expertCode);
            expertOrganizationService.retireExpertOrganization(expertId, organizationId, retired, userContext);
        }
    }

    @RequestMapping(path = "expert/{expertCode}/visible", method = RequestMethod.PUT)
    public void changeExpertUserVisibility(@PathVariable Long organizationId,
                                           @ModelAttribute UserSecurityHelper userSecurityHelper,
                                           @PathVariable("expertCode") String expertCode,
                                           @RequestParam boolean visible,
                                           UserContext userContext) throws UserUnauthorizedException, IOException, IllegalAccessException, InstantiationException {
        if (!userSecurityHelper.canMangeOrganizationExperts(organizationId)) throw new UserUnauthorizedException();
        Long userId;
        if (IdUtil.isIntId(expertCode)) {
            userId = IdUtil.getIntDataId(expertCode);
        } else {
            userId = expertToUserService.expertToUser(expertCode, userContext);
        }
        userOrganizationService.changeExpertOrganizationVisibility(userId, organizationId, visible, userContext);
        userProfileService.loadAndIndexUserProfile(userId);

    }

    @RequestMapping(path = "expert/{expertCode}/active", method = RequestMethod.PUT)
    public void changeExpertUserActivity(@PathVariable Long organizationId,
                                           @ModelAttribute UserSecurityHelper userSecurityHelper,
                                           @PathVariable("expertCode") String expertCode,
                                           @RequestParam boolean active,
                                           UserContext userContext) throws UserUnauthorizedException, IOException, IllegalAccessException, InstantiationException {
        if (!userSecurityHelper.canMangeOrganizationExperts(organizationId)) throw new UserUnauthorizedException();
        Long userId;
        if (IdUtil.isIntId(expertCode)) {
            userId = IdUtil.getIntDataId(expertCode);
        } else {
            userId = expertToUserService.expertToUser(expertCode, userContext);
        }
        userOrganizationService.changeExpertOrganizationActivity(userId, organizationId, active, userContext);
        userProfileService.loadAndIndexUserProfile(userId);

    }


    @RequestMapping(path = "experts/verified", method = RequestMethod.PUT)
    public void verifyExperts(@PathVariable Long organizationId,
                              @ModelAttribute UserSecurityHelper userSecurityHelper,
                              @RequestParam Set<Long> expertIds,
                              @RequestParam boolean verify,
                              UserContext userContext) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canMangeOrganizationExperts(organizationId)) throw new UserUnauthorizedException();
        for (Long expertId : expertIds) {
            expertOrganizationService.verifyExpertOrganization(expertId, organizationId, verify, userContext);
        }
    }

    @RequestMapping(path = "users/verified", method = RequestMethod.PUT)
    public void verifyUser(@PathVariable Long organizationId,
                           @ModelAttribute UserSecurityHelper userSecurityHelper,
                           @RequestParam Set<Long> userIds,
                           @RequestParam boolean verify,
                           UserContext userContext) throws UserUnauthorizedException {
        if (!userSecurityHelper.canMangeOrganizationExperts(organizationId)) throw new UserUnauthorizedException();
        userOrganizationService.verifyUsers(userIds, organizationId, verify, userContext);
        userProfileService.loadAndIndexUserProfiles(userIds);
    }

    @RequestMapping(path = "users", method = RequestMethod.DELETE)
    public void deleteOrganizationUsers(@PathVariable Long organizationId,
                                        @ModelAttribute UserSecurityHelper userSecurityHelper,
                                        @RequestParam Set<Long> userIds,
                                        @ModelAttribute UserContext userContext) throws UserUnauthorizedException {
        if (!userSecurityHelper.canMangeOrganizationExperts(organizationId)) throw new UserUnauthorizedException();
        userOrganizationService.removeOrganization(userIds, organizationId, userContext);
        userProfileService.loadAndIndexUserProfiles(userIds);
    }

    @RequestMapping(path = "experts", method = RequestMethod.DELETE)
    public void deleteOrganizationExperts(@PathVariable Long organizationId,
                                          @ModelAttribute UserSecurityHelper userSecurityHelper,
                                          @RequestParam Set<Long> expertIds,
                                          @ModelAttribute UserContext userContext) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canMangeOrganizationExperts(organizationId)) throw new UserUnauthorizedException();
        expertOrganizationService.removeOrganizationExperts(expertIds, organizationId, userContext);
    }

}
