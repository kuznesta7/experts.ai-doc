package ai.unico.platform.rest.controller.common;

import ai.unico.platform.rest.factory.UserContextFactory;
import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.search.api.service.ElasticsearchService;
import ai.unico.platform.search.api.service.ExpertEvaluationService;
import ai.unico.platform.store.api.dto.UserProfileDto;
import ai.unico.platform.store.api.dto.UserRegistrationDto;
import ai.unico.platform.store.api.service.ClaimProfileService;
import ai.unico.platform.store.api.service.UserProfileService;
import ai.unico.platform.store.api.service.UserRegistrationService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NoLoggedInUserException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Set;

@RestController
@RequestMapping("common/users")
public class UserController {

    @Autowired
    private UserContextFactory userContextFactory;

    private final UserRegistrationService userRegistrationService = ServiceRegistryProxy.createProxy(UserRegistrationService.class);
    private final UserProfileService userProfileService = ServiceRegistryProxy.createProxy(UserProfileService.class);
    private final ClaimProfileService claimProfileService = ServiceRegistryProxy.createProxy(ClaimProfileService.class);
    private final ExpertEvaluationService expertEvaluationService = ServiceRegistryProxy.createProxy(ExpertEvaluationService.class);
    private final ElasticsearchService elasticsearchService = ServiceRegistryProxy.createProxy(ElasticsearchService.class);

    @RequestMapping(method = RequestMethod.POST)
    public Long createUser(@RequestBody UserRegistrationDto userRegistrationDto,
                           @ModelAttribute UserSecurityHelper userSecurityHelper,
                           @RequestParam(required = false) Set<Long> expertIdsToClaim,
                           @RequestParam(required = false) Set<Long> userIdsToClaim,
                           UserContext userContext) throws EntityAlreadyExistsException, NonexistentEntityException, UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canCreateUser()) throw new UserUnauthorizedException();
        checkAuthorization(userSecurityHelper, userRegistrationDto);
        final Long userId = userRegistrationService.registerUser(userRegistrationDto, userContext);
        claimProfileService.preClaimProfiles(userId, userIdsToClaim, expertIdsToClaim, userContext);
        final ElasticsearchService elasticsearchService = ServiceRegistryUtil.getService(ElasticsearchService.class);
        elasticsearchService.flushSyncedAll();
        claimProfileService.claimAllPreClaimedProfiles(userId, userContext);
        return userId;
    }

    @RequestMapping(method = RequestMethod.POST, path = "claimable")
    public Long createClaimableUser(@RequestBody UserProfileDto userProfileDto,
                                    @ModelAttribute UserSecurityHelper userSecurityHelper,
                                    UserContext userContext) throws EntityAlreadyExistsException, NonexistentEntityException, UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canManageExpertClaims()) throw new UserUnauthorizedException();
        userProfileDto.setClaimable(true);
        final Long userId = userProfileService.createProfile(userProfileDto, userContext);
        userProfileService.loadAndIndexUserProfile(userId);
        return userId;
    }

    @RequestMapping(path = "/{userId}", method = RequestMethod.PUT)
    public void updateUser(@RequestBody UserRegistrationDto userRegistrationDto,
                           @PathVariable Long userId,
                           UserContext userContext,
                           @ModelAttribute UserSecurityHelper userSecurityHelper) throws
            EntityAlreadyExistsException, NonexistentEntityException, UserUnauthorizedException, NoLoggedInUserException, IOException {
        userRegistrationDto.setId(userId);
        if (userContext == null) throw new NoLoggedInUserException();
        if (!userSecurityHelper.canEditUserProfile(userRegistrationDto.getId())) throw new UserUnauthorizedException();
        checkAuthorization(userSecurityHelper, userRegistrationDto);
        userRegistrationService.updateUser(userRegistrationDto, userContext);
        try {
            expertEvaluationService.recalculateComputedFieldsForUser(userId);
        } catch (Exception ignore) {
        }
        userContextFactory.reloadUserContext();
    }

    @RequestMapping(path = "/{userId}", method = RequestMethod.GET)
    public UserRegistrationDto findUser(@PathVariable Long userId,
                                        @ModelAttribute UserSecurityHelper userSecurityHelper) throws
            EntityAlreadyExistsException, NonexistentEntityException, UserUnauthorizedException {
        if (!userSecurityHelper.canEditUserProfile(userId)) throw new UserUnauthorizedException();
        return userRegistrationService.findUser(userId);
    }

    @RequestMapping(path = "/reinvite", method = RequestMethod.POST)
    public void resentInvite(@RequestBody UserRegistrationDto userRegistrationDto,
                             @ModelAttribute UserSecurityHelper userSecurityHelper,
                             UserContext userContext) throws EntityAlreadyExistsException, NonexistentEntityException, UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canCreateUser()) throw new UserUnauthorizedException();
        checkAuthorization(userSecurityHelper, userRegistrationDto);
        userRegistrationService.reinviteUser(userRegistrationDto, userContext);
    }

    private void checkAuthorization(UserSecurityHelper userSecurityHelper, UserRegistrationDto userRegistrationDto) throws
            UserUnauthorizedException {
        final Long id = userRegistrationDto.getId();
        final Set<String> countryOfInterestCodes = userRegistrationDto.getCountryOfInterestCodes();
        if (!userSecurityHelper.checkUserCanAddCountriesOfInterestForUser(id, countryOfInterestCodes))
            throw new UserUnauthorizedException();
    }
}
