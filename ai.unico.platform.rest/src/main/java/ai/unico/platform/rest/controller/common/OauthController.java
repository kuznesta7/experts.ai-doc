package ai.unico.platform.rest.controller.common;

import ai.unico.platform.ume.api.service.GoogleOauthService;
import ai.unico.platform.ume.api.service.LinkedInOauthService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("common/oauth2")
public class OauthController {

    @Autowired
    private HttpServletRequest httpServletRequest;

    @RequestMapping(method = RequestMethod.GET)
    public Map<String, String> getApplicationUrl() {
        final GoogleOauthService googleOauthService = ServiceRegistryUtil.getService(GoogleOauthService.class);
        final LinkedInOauthService linkedInOauthService = ServiceRegistryUtil.getService(LinkedInOauthService.class);
        final Map<String, String> resultMap = new HashMap<>();
        resultMap.put("google", googleOauthService.getApplicationUrl());
        resultMap.put("linkedin", linkedInOauthService.getApplicationUrl());
        return resultMap;
    }

    @RequestMapping(method = RequestMethod.POST, path = "google")
    public void loginWithGoogle(@RequestParam("code") String code, UserContext userContext) throws Exception {
        if (userContext != null) return;
        httpServletRequest.login(GoogleOauthService.GOOGLE_OAUTH, code);
    }

    @RequestMapping(method = RequestMethod.POST, path = "linkedin")
    public void loginWithLinkedin(@RequestParam("code") String code, UserContext userContext) throws Exception {
        if (userContext != null) return;
        httpServletRequest.login(LinkedInOauthService.LINKEDIN_OAUTH, code);
    }
}
