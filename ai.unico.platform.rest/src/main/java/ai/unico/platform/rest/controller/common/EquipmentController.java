package ai.unico.platform.rest.controller.common;

import ai.unico.platform.extdata.dto.CategoryDwhDto;
import ai.unico.platform.extdata.service.EquipmentCategoryService;
import ai.unico.platform.search.api.dto.EquipmentPreviewDto;
import ai.unico.platform.search.api.service.EquipmentSearchService;
import ai.unico.platform.store.api.dto.EquipmentRegisterDto;
import ai.unico.platform.store.api.dto.MultilingualDto;
import ai.unico.platform.store.api.service.EquipmentService;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("common/equipment")
public class EquipmentController {

    private final EquipmentSearchService equipmentSearchService = ServiceRegistryProxy.createProxy(EquipmentSearchService.class);
    private final EquipmentCategoryService equipmentCategoryService = ServiceRegistryProxy.createProxy(EquipmentCategoryService.class);
    private final EquipmentService equipmentService = ServiceRegistryProxy.createProxy(EquipmentService.class);

    @RequestMapping(method = RequestMethod.GET, path = "{equipmentCode}")
    public MultilingualDto<EquipmentPreviewDto> getEquipment(@PathVariable String equipmentCode) throws IOException {
        return equipmentSearchService.getEquipment(equipmentCode);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Map<String, Object> saveEquipment(@RequestBody MultilingualDto<EquipmentRegisterDto> multilingualDto,
                                             @ModelAttribute UserContext userContext) throws IOException {
        Long equipmentId = equipmentService.saveEquipment(multilingualDto, userContext);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("id", IdUtil.convertToIntDataCode(equipmentId));
        return returnMap;
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Map<String, Object> updateEquipment(@RequestBody MultilingualDto<EquipmentRegisterDto> multilingualDto,
                                               @ModelAttribute UserContext userContext) throws IOException {
        equipmentService.updateEquipment(multilingualDto, userContext);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("id", IdUtil.convertToIntDataCode(multilingualDto.getId()));
        return returnMap;
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{equipmentCode}")
    public void deleteEquipment(@PathVariable String equipmentCode,
                                @ModelAttribute UserContext userContext) throws IOException {
        equipmentService.deleteEquipment(equipmentCode, userContext);
    }

    @RequestMapping(method = RequestMethod.POST, path = "hidden")
    public void toggleHiddenGroup(@RequestParam String equipmentCode,
                                  @RequestParam boolean hidden,
                                  @ModelAttribute UserContext userContext) throws IOException {
        equipmentService.toggleHidden(equipmentCode, hidden, userContext);
    }

    @RequestMapping(method = RequestMethod.GET, path = "domain")
    public List<CategoryDwhDto> getDomains() {
        return equipmentCategoryService.getEquipmentDomain();
    }

    @RequestMapping(method = RequestMethod.GET, path = "type")
    public List<CategoryDwhDto> getTypes() {
        return equipmentCategoryService.getEquipmentType();
    }
}
