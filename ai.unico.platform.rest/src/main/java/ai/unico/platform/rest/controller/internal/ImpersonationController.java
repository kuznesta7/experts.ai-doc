package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.factory.UserContextFactory;
import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.util.exception.InvalidParameterException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("internal/impersonation")
public class ImpersonationController {

    @Autowired
    UserContextFactory userContextFactory;

    @RequestMapping(method = RequestMethod.POST, path = "{userId}")
    public void impersonate(@PathVariable Long userId,
                            UserContext userContext,
                            @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canImpersonate()) throw new UserUnauthorizedException();
        userContextFactory.impersonate(userId, userContext);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void stopImpersonation(UserContext userContext) {
        if (userContext.getImpersonatorUserId() == null)
            throw new InvalidParameterException("impersonator user is null");
        userContextFactory.stopImpersonation();
    }
}
