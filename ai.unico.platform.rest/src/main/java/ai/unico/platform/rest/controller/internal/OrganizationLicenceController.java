package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.OrganizationLicenceDto;
import ai.unico.platform.store.api.service.OrganizationLicenceService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("internal/organizations/{organizationId}/licences")
public class OrganizationLicenceController {

    private final OrganizationLicenceService organizationLicenceService = ServiceRegistryProxy.createProxy(OrganizationLicenceService.class);

    @RequestMapping(method = RequestMethod.GET)
    public List<OrganizationLicenceDto> getOrganizationLicences(@PathVariable Long organizationId,
                                                                @RequestParam(required = false, defaultValue = "true") boolean validOnly,
                                                                UserContext userContext,
                                                                @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManageOrganizationLicences()) throw new UserUnauthorizedException();
        return organizationLicenceService.getOrganizationLicences(organizationId, validOnly);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Long addOrganizationLicence(@PathVariable Long organizationId,
                                       @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date validUntil,
                                       @RequestParam OrganizationRole licenceRole,
                                       @RequestParam Integer numberOfUserLicences,
                                       UserContext userContext,
                                       @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManageOrganizationLicences()) throw new UserUnauthorizedException();
        return organizationLicenceService.addOrganizationLicence(organizationId, licenceRole, validUntil, numberOfUserLicences, userContext);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{licenceId}")
    public void addOrganizationLicence(@PathVariable Long organizationId,
                                       @PathVariable Long licenceId,
                                       UserContext userContext,
                                       @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManageOrganizationLicences()) throw new UserUnauthorizedException();
        organizationLicenceService.invalidateLicence(licenceId, userContext);
    }
}
