package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.service.CommercializationProjectService;
import ai.unico.platform.store.api.service.CommercializationTeamMemberService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("internal/commercialization/projects/{commercializationProjectId}/team-members")
public class CommercializationTeamMemberController {

    private final CommercializationTeamMemberService commercializationTeamMemberService = ServiceRegistryProxy.createProxy(CommercializationTeamMemberService.class);
    private final CommercializationProjectService commercializationProjectService = ServiceRegistryProxy.createProxy(CommercializationProjectService.class);

    @RequestMapping(method = RequestMethod.PUT)
    public void addMember(@PathVariable Long commercializationProjectId,
                          @RequestParam(required = false) Long userId,
                          @RequestParam(required = false) Long expertId,
                          @RequestParam(required = false) boolean teamLeader,
                          UserContext userContext,
                          @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditCommercializationProject(commercializationProjectId))
            throw new UserUnauthorizedException();
        if (userId != null)
            commercializationTeamMemberService.addTeamMemberUser(commercializationProjectId, userId, teamLeader, userContext);
        else if (expertId != null)
            commercializationTeamMemberService.addTeamMemberExpert(commercializationProjectId, expertId, teamLeader, userContext);
        commercializationProjectService.loadAndIndexCommercializationProject(commercializationProjectId);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void removeMember(@PathVariable Long commercializationProjectId,
                             @RequestParam(required = false) Long userId,
                             @RequestParam(required = false) Long expertId,
                             UserContext userContext,
                             @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditCommercializationProject(commercializationProjectId))
            throw new UserUnauthorizedException();
        if (userId != null)
            commercializationTeamMemberService.removeTeamMemberUser(commercializationProjectId, userId, userContext);
        else if (expertId != null)
            commercializationTeamMemberService.removeTeamMemberExpert(commercializationProjectId, expertId, userContext);
        commercializationProjectService.loadAndIndexCommercializationProject(commercializationProjectId);
    }

}
