package ai.unico.platform.rest.controller.common;

import ai.unico.platform.store.api.dto.CountryDto;
import ai.unico.platform.store.api.service.CountryService;
import ai.unico.platform.util.ServiceRegistryProxy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("common/countries")
public class CountryController {

    private final CountryService countryService = ServiceRegistryProxy.createProxy(CountryService.class);

    @RequestMapping(method = RequestMethod.GET)
    public List<CountryDto> findCountries() {
        return countryService.findAvailableCountries();
    }
}
