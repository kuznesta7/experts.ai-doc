package ai.unico.platform.rest.controller.common;

import ai.unico.platform.rest.factory.UserContextFactory;
import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.rest.util.ControllerUtil;
import ai.unico.platform.search.api.dto.OrganizationAutocompleteDto;
import ai.unico.platform.search.api.filter.OrganizationAutocompleteFilter;
import ai.unico.platform.search.api.filter.OrganizationSearchFilter;
import ai.unico.platform.search.api.service.OrganizationSearchService;
import ai.unico.platform.search.api.wrapper.OrganizationWrapper;
import ai.unico.platform.store.api.dto.*;
import ai.unico.platform.store.api.enums.OrganizationRelationType;
import ai.unico.platform.store.api.service.OrganizationLicenceService;
import ai.unico.platform.store.api.service.OrganizationLogoService;
import ai.unico.platform.store.api.service.OrganizationService;
import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.store.api.wrapper.OrganizationTypeWrapper;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.exception.NoLoggedInUserException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("common/organizations")
public class OrganizationController {

    @Autowired
    private UserContextFactory userContextFactory;

    private final OrganizationSearchService organizationSearchService = ServiceRegistryProxy.createProxy(OrganizationSearchService.class);
    private final OrganizationService organizationService = ServiceRegistryProxy.createProxy(OrganizationService.class);
    private final OrganizationStructureService organizationStructureService = ServiceRegistryProxy.createProxy(OrganizationStructureService.class);
    private final OrganizationLogoService organizationLogoService = ServiceRegistryProxy.createProxy(OrganizationLogoService.class);
    private final OrganizationLicenceService organizationLicenceService = ServiceRegistryProxy.createProxy(OrganizationLicenceService.class);

    @RequestMapping(method = RequestMethod.GET, path = "autocomplete")
    public List<OrganizationAutocompleteDto> findOrganizations(@ModelAttribute OrganizationAutocompleteFilter filter) throws IOException {
        return organizationSearchService.autocompleteOrganizations(filter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{organizationId}/autocomplete/opportunity")
    public List<OrganizationAutocompleteDto> autocompleteOrganizationsOpportunity(@PathVariable Long organizationId, @ModelAttribute OrganizationAutocompleteFilter filter) throws IOException {
        List<Long> lowerOrganizationStructure = organizationStructureService.findMemberOrganizations(organizationId);
        lowerOrganizationStructure.add(organizationId);
        filter.setEnableOrganizationIds(new HashSet<>(lowerOrganizationStructure));
        return organizationSearchService.autocompleteOrganizations(filter);
    }


    @RequestMapping(method = RequestMethod.GET, path = "{organizationId}/types")
    public OrganizationTypeWrapper getOrganizationTypes(@PathVariable Long organizationId) {
        return organizationService.getOrganizationTypes(organizationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "types")
    public List<OrganizationTypeDto> getOrganizationsTypes() {
        return organizationService.getOrganizationsTypes();
    }

    @RequestMapping(method = RequestMethod.GET, path = "{organizationId}/autocomplete/suborganizations")
    public List<OrganizationAutocompleteDto> autocompleteSuborganizations(@PathVariable Long organizationId, @ModelAttribute OrganizationAutocompleteFilter filter) throws IOException {
        Set<Long> lowerOrganizationStructure = organizationStructureService.findChildOrganizationIds(organizationId, true);
        lowerOrganizationStructure.add(organizationId);
        filter.setEnableOrganizationIds(lowerOrganizationStructure);
        return organizationSearchService.autocompleteOrganizations(filter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{organizationId}/autocomplete/project")
    public List<OrganizationAutocompleteDto> autocompleteOrganizationsProject(@PathVariable Long organizationId, @ModelAttribute OrganizationAutocompleteFilter filter) throws IOException {
        return organizationSearchService.autocompleteOrganizationsForResearchProjects(filter, organizationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{organizationId}/autocomplete/outcomes")
    public List<OrganizationAutocompleteDto> autocompleteOrganizationsOutcome(@PathVariable Long organizationId, @ModelAttribute OrganizationAutocompleteFilter filter) throws IOException {
        return organizationSearchService.autocompleteOrganizationsForResearchProjects(filter, organizationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{organizationId}/memberSearch")
    public OrganizationWrapper findOrganizationsForMembers(@PathVariable Long organizationId, @ModelAttribute OrganizationSearchFilter organizationSearchFilter) throws IOException {
        List<Long> lowerOrganizationStructure = organizationStructureService.findMemberOrganizations(organizationId);
        if (lowerOrganizationStructure.isEmpty())
            return new OrganizationWrapper();
        organizationSearchFilter.setOrganizationIds(new HashSet<>(lowerOrganizationStructure));
        return organizationSearchService.findOrganizations(organizationSearchFilter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{organizationId}/memberIds")
    public List<Long> getMemberIds(@PathVariable Long organizationId) {
        return organizationStructureService.findMemberOrganizations(organizationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{organizationId}/myMemberships")
    public List<OrganizationBaseDto> findMyMemberships(@PathVariable Long organizationId) throws IOException {
        Set<Long> myMemberships = organizationStructureService.findMyMemberships(organizationId);
        return organizationSearchService.findBaseOrganizations(myMemberships);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{organizationId}/findParents")
    public List<OrganizationBaseDto> findParents(@PathVariable Long organizationId) throws IOException {
        Set<Long> parentIds = organizationStructureService.findParents(organizationId);
        return organizationSearchService.findBaseOrganizations(parentIds);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{organizationId}")
    public OrganizationDetailDto findOrganization(@PathVariable("organizationId") Long organizationId) {
        return organizationService.findOrganizationDetail(organizationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{organizationId}/hasLicense")
    public Boolean organizationHasLicense(@PathVariable("organizationId") Long organizationId) {
        return organizationLicenceService.organizationHasLicense(organizationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/hasLicense")
    public Boolean organizationHasLicense(@RequestParam("orgIds") Set<Long> organizationIds) {
        return organizationLicenceService.organizationsHasLicenses(organizationIds);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<OrganizationAutocompleteDto> findOrganization(@RequestParam("organizationIds") Set<Long> organizationIds) throws IOException {
        return organizationSearchService.findAutocompleteOrganizations(organizationIds);
    }

    @RequestMapping(method = RequestMethod.GET, path="suborganizations")
    public List<OrganizationAutocompleteDto> findSuborganizations(@RequestParam("organizationIds") Set<Long> organizationIds) throws IOException {
        Set<Long> lowerOrgIds = organizationStructureService.findChildOrganizationIds(organizationIds, true);
        return organizationSearchService.findAutocompleteOrganizations(lowerOrgIds);
    }

    @RequestMapping(method = RequestMethod.GET, path = "structure")
    public List<OrganizationStructureDto> findOrganizationStructure(@RequestParam("organizationIds") Set<Long> organizationIds,
                                                                    @RequestParam(value = "includeMembers", required = false, defaultValue = "true") Boolean includeMembers) throws IOException {
        return organizationStructureService.findOrganizationStructure(organizationIds, includeMembers);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{organizationId}/children")
    public OrganizationWrapper findOrganizationWithChildren(@PathVariable Long organizationId, @ModelAttribute OrganizationSearchFilter organizationSearchFilter) throws IOException {
        final Set<Long> organizationToLoadIds = organizationStructureService.findChildOrganizationIds(new HashSet<>(Collections.singletonList(organizationId)), true);
        organizationSearchFilter.setOrganizationIds(organizationToLoadIds);
        return organizationSearchService.findOrganizations(organizationSearchFilter);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{organizationId}/children/{childOrganizationId}")
    public void removeOrganizationStructure(@PathVariable Long organizationId, UserContext userContext,
                                            @PathVariable Long childOrganizationId,
                                            @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditOrganizationStructure(organizationId)) throw new UserUnauthorizedException();
        organizationStructureService.deleteOrganizationStructure(organizationId, childOrganizationId, userContext);
        organizationService.loadAndIndexOrganization(organizationId);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{organizationId}/children/{childOrganizationId}")
    public void updateOrganizationStructure(@PathVariable Long organizationId, UserContext userContext,
                                            @PathVariable Long childOrganizationId,
                                            @RequestParam OrganizationRelationType relation,
                                            @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canAdminOrganizations()) throw new UserUnauthorizedException();
        organizationStructureService.addOrganizationStructure(organizationId, childOrganizationId, relation, userContext);
        organizationService.loadAndIndexOrganization(organizationId);
    }

    @RequestMapping(method = RequestMethod.POST, path = "{parentOrganizationId}/children")
    public Long saveOrganizationToStructure(@PathVariable Long parentOrganizationId, UserContext userContext,
                                            @ModelAttribute UserSecurityHelper userSecurityHelper,
                                            @RequestBody AdminOrganizationUpdateDto organizationDto) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditOrganizationStructure(parentOrganizationId))
            throw new UserUnauthorizedException();
        organizationDto.setParentOrganizationId(parentOrganizationId);
        final Long organizationId = organizationService.createOrganization(organizationDto, userContext);
        organizationService.loadAndIndexOrganization(parentOrganizationId);
        userContextFactory.reloadUserContext();
        return organizationId;
    }

    @RequestMapping(value = "{organizationId}/img", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    public void uploadProfileImg(UserContext userContext,
                                 @ModelAttribute UserSecurityHelper userSecurityHelper,
                                 @PathVariable final Long organizationId,
                                 @RequestParam String fileName,
                                 @RequestParam OrganizationLogoService.ImgType type,
                                 @RequestParam(value = "content", required = false) MultipartFile multipartFile) throws IOException, UserUnauthorizedException, NoLoggedInUserException {
        if (userContext == null) throw new NoLoggedInUserException();
        if (!userSecurityHelper.canEditOrganizationStructure(organizationId)) throw new UserUnauthorizedException();

        final byte[] bytes = multipartFile.getBytes();
        organizationLogoService.saveImg(userContext, organizationId, type, new FileDto(bytes, fileName));
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{organizationId}")
    public void updateOrganizationStructure(@PathVariable Long organizationId,
                                            UserContext userContext,
                                            @RequestBody OrganizationDetailDto organizationDetailDto,
                                            @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditOrganizationStructure(organizationId)) throw new UserUnauthorizedException();
        organizationService.saveProfile(organizationId, organizationDetailDto, userContext);
        organizationService.loadAndIndexOrganization(organizationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{organizationId}/gdpr")
    public Boolean hasGdpr(@PathVariable Long organizationId,
                           UserContext userContext) {
        return organizationService.hasGdpr(organizationId);
    }

    @ResponseBody
    @RequestMapping(value = "{organizationId}/img", method = RequestMethod.GET)
    public HttpEntity<byte[]> getLogo(@PathVariable final Long organizationId,
                                      @RequestParam(required = false) OrganizationLogoService.ImgType type,
                                      @RequestParam(required = false) String size) {
        final FileDto fileDto = organizationLogoService.getImg(organizationId, type, size);
        return ControllerUtil.toFileResponse(fileDto);
    }
}
