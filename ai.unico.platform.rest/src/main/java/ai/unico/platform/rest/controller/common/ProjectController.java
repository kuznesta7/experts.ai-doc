package ai.unico.platform.rest.controller.common;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.rest.util.ControllerUtil;
import ai.unico.platform.search.api.dto.ProjectDetailDto;
import ai.unico.platform.search.api.dto.ProjectPreviewDto;
import ai.unico.platform.search.api.filter.ProjectFilter;
import ai.unico.platform.search.api.service.EvidenceProjectService;
import ai.unico.platform.store.api.dto.ProjectDocumentDto;
import ai.unico.platform.store.api.dto.ProjectDocumentPreviewDto;
import ai.unico.platform.store.api.dto.ProjectDto;
import ai.unico.platform.store.api.service.*;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("common/projects")
public class ProjectController {

    private final ProjectDocumentService projectDocumentService = ServiceRegistryProxy.createProxy(ProjectDocumentService.class);
    private final ProjectService projectService = ServiceRegistryProxy.createProxy(ProjectService.class);


    @RequestMapping(method = RequestMethod.GET)
    public List<ProjectPreviewDto> lookupProjects(@ModelAttribute ProjectFilter filter,
                                                  UserContext userContext,
                                                  @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, NonexistentEntityException {
        final EvidenceProjectService projectService = ServiceRegistryUtil.getService(EvidenceProjectService.class);
        filter.setPage(1);
        filter.setLimit(10);
        filter.setConfidentiality(Collections.singleton(Confidentiality.PUBLIC));
        return projectService.findProjects(filter).getProjectPreviewDtos();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Long createProject(@RequestBody ProjectDto projectDto,
                              UserContext userContext,
                              @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException, NonexistentEntityException {
        if (!userSecurityHelper.canCreateProject()) throw new UserUnauthorizedException();
        final Long projectId = projectService.createProject(projectDto, userContext);
        projectDto.setProjectId(projectId);
        final ProjectUserService projectUserService = ServiceRegistryUtil.getService(ProjectUserService.class);
        final ProjectOrganizationService projectOrganizationService = ServiceRegistryUtil.getService(ProjectOrganizationService.class);
        projectUserService.switchUsers(projectDto, userContext);
        projectOrganizationService.switchOrganizations(projectId, projectDto.getOrganizationIds(), userContext);
        projectService.loadAndIndexProject(projectId);
        return projectId;
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/{projectId}")
    public void updateProject(@PathVariable Long projectId,
                              @RequestBody ProjectDto projectDto,
                              UserContext userContext,
                              @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException {
        if (!userSecurityHelper.canEditProject(projectId)) throw new UserUnauthorizedException();
        projectDto.setProjectId(projectId);
        projectService.updateProject(projectDto, userContext);
        final ProjectUserService projectUserService = ServiceRegistryUtil.getService(ProjectUserService.class);
        final ProjectOrganizationService projectOrganizationService = ServiceRegistryUtil.getService(ProjectOrganizationService.class);
        projectUserService.switchUsers(projectDto, userContext);
        projectOrganizationService.switchOrganizations(projectId, projectDto.getOrganizationIds(), userContext);
        projectService.loadAndIndexProject(projectId);
    }

    @RequestMapping(method = RequestMethod.POST, path = "register/dwh/{originalProjectId}")
    public Long registerProject(@PathVariable Long originalProjectId,
                                UserContext userContext,
                                @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NonexistentEntityException {
        if (!userSecurityHelper.canConfirmProject()) throw new UserUnauthorizedException();
        final ProjectVerificationService projectVerificationService = ServiceRegistryUtil.getService(ProjectVerificationService.class);
        final Long projectId = projectVerificationService.verifyDWHProject(originalProjectId, userContext);
        projectService.loadAndIndexProject(projectId);
        return projectId;
    }

    @RequestMapping(method = RequestMethod.GET, path = "{projectCode}")
    public ProjectDetailDto getProject(@PathVariable String projectCode,
                                       UserContext userContext) throws NonexistentEntityException, UserUnauthorizedException, IOException {
        final EvidenceProjectService projectService = ServiceRegistryUtil.getService(EvidenceProjectService.class);
        return projectService.findProject(userContext, projectCode);
    }

    @RequestMapping(path = "{projectId}/documents", method = RequestMethod.GET)
    public List<ProjectDocumentPreviewDto> getProjectDocuments(@PathVariable Long projectId,
                                                               @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canSeeProject(projectId))
            throw new UserUnauthorizedException();
        return projectDocumentService.findProjectDocuments(projectId);
    }

    @RequestMapping(path = "{projectId}/documents/{fileId}", method = RequestMethod.GET)
    public HttpEntity<byte[]> getProjectDocument(@PathVariable Long projectId,
                                                 @PathVariable Long fileId,
                                                 @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                 UserContext userContext) throws UserUnauthorizedException {
        if (!userSecurityHelper.canSeeProject(projectId))
            throw new UserUnauthorizedException();
        final FileDto fileDto = projectDocumentService.getProjectDocument(projectId, fileId, userContext);
        return ControllerUtil.toFileResponse(fileDto);
    }

    @RequestMapping(path = "{projectId}/documents", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public Long addProjectDocument(@PathVariable Long projectId,
                                   @ModelAttribute UserSecurityHelper userSecurityHelper,
                                   @RequestParam String fileName,
                                   @RequestParam(value = "content", required = false) MultipartFile multipartFile,
                                   UserContext userContext) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canEditProject(projectId))
            throw new UserUnauthorizedException();
        final ProjectDocumentDto documentDto = new ProjectDocumentDto();
        documentDto.setContent(multipartFile.getBytes());
        documentDto.setName(fileName);
        return projectDocumentService.addProjectDocument(projectId, documentDto, userContext);
    }

    @RequestMapping(path = "{projectId}/documents/{fileId}", method = RequestMethod.DELETE)
    public void deleteProjectDocument(@PathVariable Long projectId,
                                      @PathVariable Long fileId,
                                      @ModelAttribute UserSecurityHelper userSecurityHelper,
                                      UserContext userContext) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditProject(projectId))
            throw new UserUnauthorizedException();
        projectDocumentService.deleteProjectDocument(projectId, fileId, userContext);
    }
}
