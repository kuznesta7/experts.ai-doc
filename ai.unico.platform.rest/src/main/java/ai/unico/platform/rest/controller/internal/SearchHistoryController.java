package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.filter.ExpertSearchHistoryFilter;
import ai.unico.platform.store.api.service.SearchHistoryService;
import ai.unico.platform.store.api.wrapper.ExpertSearchHistoryWrapper;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("internal")
public class SearchHistoryController {

    private final SearchHistoryService searchHistoryService = ServiceRegistryProxy.createProxy(SearchHistoryService.class);

    @RequestMapping(method = RequestMethod.GET, path = "/users/{userId}/search/history")
    public ExpertSearchHistoryWrapper getSearchHistory(@PathVariable Long userId,
                                                       @ModelAttribute ExpertSearchHistoryFilter expertSearchHistoryFilter,
                                                       UserContext userContext,
                                                       @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canSeeUserSearchHistory(userId)) throw new UserUnauthorizedException();
        final ExpertSearchHistoryWrapper searchHistoryForUser = searchHistoryService.findSearchHistoryForUser(userId, expertSearchHistoryFilter);
        return searchHistoryForUser;
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/search/history/{searchHistId}")
    public void updateSearchHistory(@PathVariable Long searchHistId,
                                    @RequestParam boolean favorite,
                                    @RequestParam(required = false) Set<String> workspaces,
                                    UserContext userContext,
                                    @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canUserEditSearchHistory(searchHistId)) throw new UserUnauthorizedException();
        if (workspaces == null) workspaces = new HashSet<>();
        searchHistoryService.saveFavoriteSearch(searchHistId, favorite, workspaces, userContext);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/search/history/{searchHistId}")
    public void deleteSearchHistory(@PathVariable Long searchHistId,
                                    UserContext userContext,
                                    @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canUserEditSearchHistory(searchHistId)) throw new UserUnauthorizedException();
        searchHistoryService.deleteSearchHistory(searchHistId, userContext);
    }
}
