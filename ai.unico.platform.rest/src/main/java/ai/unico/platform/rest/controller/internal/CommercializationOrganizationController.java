package ai.unico.platform.rest.controller.internal;


import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.enums.CommercializationOrganizationRelation;
import ai.unico.platform.store.api.service.CommercializationOrganizationService;
import ai.unico.platform.store.api.service.CommercializationProjectService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("internal/commercialization/projects/{commercializationId}/organizations/{organizationId}")
public class CommercializationOrganizationController {

    private final CommercializationOrganizationService commercializationOrganizationService = ServiceRegistryProxy.createProxy(CommercializationOrganizationService.class);

    private final CommercializationProjectService commercializationProjectService = ServiceRegistryProxy.createProxy(CommercializationProjectService.class);

    @RequestMapping(method = RequestMethod.PUT)
    public void addOrganization(@PathVariable Long commercializationId,
                                @PathVariable Long organizationId,
                                @RequestParam CommercializationOrganizationRelation relation,
                                @ModelAttribute UserSecurityHelper userSecurityHelper,
                                UserContext userContext) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditCommercializationProject(commercializationId))
            throw new UserUnauthorizedException();
        commercializationOrganizationService.addOrganization(commercializationId, organizationId, relation, userContext);
        commercializationProjectService.loadAndIndexCommercializationProject(commercializationId);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void removeOrganization(@PathVariable Long commercializationId,
                                   @PathVariable Long organizationId,
                                   @RequestParam CommercializationOrganizationRelation relation,
                                   @ModelAttribute UserSecurityHelper userSecurityHelper,
                                   UserContext userContext) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditCommercializationProject(commercializationId))
            throw new UserUnauthorizedException();
        commercializationOrganizationService.removeOrganization(commercializationId, organizationId, relation, userContext);
        commercializationProjectService.loadAndIndexCommercializationProject(commercializationId);
    }
}
