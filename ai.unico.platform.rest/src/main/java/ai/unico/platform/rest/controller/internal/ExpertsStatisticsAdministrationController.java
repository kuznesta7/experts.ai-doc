package ai.unico.platform.rest.controller.internal;


import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.ExpertsStatisticsDto;
import ai.unico.platform.store.api.service.ExpertsStatisticsAdministrationService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("internal/expertsStatistics")
public class ExpertsStatisticsAdministrationController {


    @RequestMapping(method = RequestMethod.GET)
    public List<ExpertsStatisticsDto> getExpertsStatistics(@ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeExpertsStatistics()) {
            throw new UserUnauthorizedException();
        }
        ExpertsStatisticsAdministrationService expertsStatisticsAdministrationService = ServiceRegistryUtil.getService(ExpertsStatisticsAdministrationService.class);
        return expertsStatisticsAdministrationService.getCountryExpertsStatistics();
    }

    @RequestMapping(method = RequestMethod.GET, path = "all")
    public ExpertsStatisticsDto getAllExpertsStatistics(@ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canSeeExpertsStatistics()) {
            throw new UserUnauthorizedException();
        }
        ExpertsStatisticsAdministrationService expertsStatisticsAdministrationService = ServiceRegistryUtil.getService(ExpertsStatisticsAdministrationService.class);
        return expertsStatisticsAdministrationService.getAllExpertsStatistics();
    }
}
