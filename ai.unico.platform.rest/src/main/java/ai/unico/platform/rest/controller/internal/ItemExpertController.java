package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.service.UserItemService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("internal/items/{itemId}/experts/{expertId}")
public class ItemExpertController {

    private final UserItemService userItemService = ServiceRegistryProxy.createProxy(UserItemService.class);

    @RequestMapping(method = RequestMethod.PUT)
    public void addExpertItem(@PathVariable Long itemId,
                              @PathVariable Long expertId,
                              UserContext userContext,
                              @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException, IOException {
        if (!userSecurityHelper.canManageItemExpert(itemId, expertId)) throw new UserUnauthorizedException();
        userItemService.addExpertItem(expertId, itemId, userContext);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void removeExpertItem(@PathVariable Long expertId,
                                 @PathVariable Long itemId,
                                 @RequestParam(required = false) String message,
                                 UserContext userContext,
                                 @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException {
        if (!userSecurityHelper.canManageItemExpert(itemId, expertId)) throw new UserUnauthorizedException();
        userItemService.removeExpertItem(expertId, itemId, message, userContext);
    }
}
