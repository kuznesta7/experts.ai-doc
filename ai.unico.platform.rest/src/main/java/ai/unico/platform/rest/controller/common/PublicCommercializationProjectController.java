package ai.unico.platform.rest.controller.common;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.rest.util.ControllerUtil;
import ai.unico.platform.store.api.dto.PublicCommercializationRequestDto;
import ai.unico.platform.store.api.service.CommercializationProjectImgService;
import ai.unico.platform.store.api.service.PublicCommercializationRequestService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("common/commercialization/projects/")
public class PublicCommercializationProjectController {

    private final CommercializationProjectImgService commercializationProjectImgService = ServiceRegistryProxy.createProxy(CommercializationProjectImgService.class);
    private final PublicCommercializationRequestService publicCommercializationRequestService = ServiceRegistryProxy.createProxy(PublicCommercializationRequestService.class);

    @RequestMapping(path = "{commercializationProjectId}/image", method = RequestMethod.GET)
    public HttpEntity<byte[]> getInvestmentImage(@PathVariable Long commercializationProjectId,
                                                 @RequestParam(required = true) String checkSum,
                                                 @RequestParam(required = false, defaultValue = "M") String size,
                                                 @ModelAttribute UserSecurityHelper userSecurityHelper) throws IOException, UserUnauthorizedException {
        final FileDto fileDto = commercializationProjectImgService.getImg(commercializationProjectId, checkSum, size);
        return ControllerUtil.toFileResponse(fileDto);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @RequestMapping(path = "{commercializationProjectId}/requests", method = RequestMethod.POST)
    public void createPublicCommercializationRequest(@PathVariable Long commercializationProjectId,
                                                     @RequestParam String recaptchaToken,
                                                     @RequestBody PublicCommercializationRequestDto requestDto) throws IOException {
        publicCommercializationRequestService.createPublicRequest(commercializationProjectId, requestDto, recaptchaToken);
    }
}
