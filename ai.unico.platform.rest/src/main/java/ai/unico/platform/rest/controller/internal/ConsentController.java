package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.store.api.dto.ConsentDto;
import ai.unico.platform.store.api.service.UserConsentService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("internal/consents")
public class ConsentController {

    @RequestMapping(method = RequestMethod.PUT)
    void editConsent(@RequestBody List<ConsentDto> consentDtos,
                     UserContext userContext) throws NonexistentEntityException {
        final UserConsentService service = ServiceRegistryUtil.getService(UserConsentService.class);
        service.updateUserConsents(userContext.getUserId(), consentDtos, userContext);
    }

    @RequestMapping(method = RequestMethod.GET)
    List<ConsentDto> findUserConsents(UserContext userContext) {
        final UserConsentService service = ServiceRegistryUtil.getService(UserConsentService.class);
        return service.findUserConsents(userContext.getUserId());
    }
}
