package ai.unico.platform.rest.controller.common;

import ai.unico.platform.store.api.dto.GlobalSearchStatisticsDto;
import ai.unico.platform.store.api.service.GlobalStatisticsService;
import ai.unico.platform.util.ServiceRegistryUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("common/statistics/search")
public class GlobalStatisticsController {

    @RequestMapping(method = RequestMethod.GET)
    public GlobalSearchStatisticsDto findResultTypes(@RequestParam(required = false) Long userId) {
        final GlobalStatisticsService service = ServiceRegistryUtil.getService(GlobalStatisticsService.class);
        return service.findSearchStatistics(userId);
    }
}
