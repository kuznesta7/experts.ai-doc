package ai.unico.platform.rest.controller.common;

import ai.unico.platform.store.api.enums.Currency;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("common/currencies/")
public class CurrencyController {

    @RequestMapping(method = RequestMethod.GET)
    public List<Currency> findResultTypes() {
        return Arrays.asList(Currency.values());
    }
}
