package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.CommercializationDomainDto;
import ai.unico.platform.store.api.service.CommercializationDomainService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("internal/commercialization/domains")
public class CommercializationDomainController {

    private final CommercializationDomainService commercializationDomainService = ServiceRegistryProxy.createProxy(CommercializationDomainService.class);

    @RequestMapping(method = RequestMethod.POST)
    public Long createStatus(@RequestBody CommercializationDomainDto commercializationDomainDto,
                             UserContext userContext,
                             @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canAdminCommercialization()) throw new UserUnauthorizedException();
        return commercializationDomainService.createCommercializationDomain(commercializationDomainDto, userContext);
    }

    @RequestMapping(path = "{commercializationDomainId}", method = RequestMethod.PUT)
    public void updateStatus(@RequestBody CommercializationDomainDto commercializationDomainDto,
                             @PathVariable Long commercializationDomainId,
                             UserContext userContext,
                             @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canAdminCommercialization()) throw new UserUnauthorizedException();
        commercializationDomainService.updateCommercializationDomain(commercializationDomainId, commercializationDomainDto, userContext);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<CommercializationDomainDto> findAllStatuses(@RequestParam(required = false) boolean includeInactive) {
        return commercializationDomainService.findAvailableCommercializationDomains(includeInactive);
    }
}
