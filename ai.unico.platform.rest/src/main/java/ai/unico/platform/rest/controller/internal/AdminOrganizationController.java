package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.factory.UserContextFactory;
import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.search.api.filter.AdminOrganizationFilter;
import ai.unico.platform.search.api.service.ElasticsearchService;
import ai.unico.platform.search.api.service.OrganizationSearchService;
import ai.unico.platform.search.api.wrapper.AdminOrganizationWrapper;
import ai.unico.platform.store.api.dto.AdminOrganizationUpdateDto;
import ai.unico.platform.store.api.service.OrganizationService;
import ai.unico.platform.store.api.service.SearchAllowanceService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("internal/organizations")
public class AdminOrganizationController {

    @Autowired
    private UserContextFactory userContextFactory;

    private final OrganizationSearchService organizationSearchService = ServiceRegistryProxy.createProxy(OrganizationSearchService.class);
    private final OrganizationService organizationService = ServiceRegistryProxy.createProxy(OrganizationService.class);
    private final ElasticsearchService elasticsearchService = ServiceRegistryProxy.createProxy(ElasticsearchService.class);
    private final SearchAllowanceService searchAllowanceService = ServiceRegistryProxy.createProxy(SearchAllowanceService.class);

    @RequestMapping(method = RequestMethod.GET)
    public AdminOrganizationWrapper findOrganizationsForAdmin(@ModelAttribute AdminOrganizationFilter filter,
                                                              UserContext userContext,
                                                              @ModelAttribute UserSecurityHelper userSecurityHelper) throws IOException, UserUnauthorizedException {
        if (!userSecurityHelper.canAdminOrganizations()) throw new UserUnauthorizedException();
        return organizationSearchService.findOrganizationsForAdmin(filter);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void updateOrganization(@RequestBody AdminOrganizationUpdateDto organizationDto,
                                   UserContext userContext,
                                   @ModelAttribute UserSecurityHelper userSecurityHelper) throws IOException, UserUnauthorizedException {
        if (!userSecurityHelper.canAdminOrganizations()) {
            throw new UserUnauthorizedException();
        }
        organizationService.updateOrganization(organizationDto, userContext);
        elasticsearchService.flushSyncedAll();
    }

    @RequestMapping(method = RequestMethod.POST, path = "{substituteOrganizationId}/replaced/{organizationId}")
    public void replaceOrganization(@PathVariable Long substituteOrganizationId,
                                    @PathVariable Long organizationId,
                                    UserContext userContext,
                                    @ModelAttribute UserSecurityHelper userSecurityHelper) throws IOException, UserUnauthorizedException {
        if (!userSecurityHelper.canAdminOrganizations()) {
            throw new UserUnauthorizedException();
        }
        organizationService.replaceOrganization(organizationId, substituteOrganizationId, userContext);
        elasticsearchService.flushSyncedAll();
    }

    @RequestMapping(method = RequestMethod.PATCH, path = "{organizationId}")
    public void restoreOrganization(@PathVariable Long organizationId,
                                    UserContext userContext,
                                    @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canAdminOrganizations()) throw new UserUnauthorizedException();
        organizationService.restoreOrganization(organizationId, userContext);
        elasticsearchService.flushSyncedAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Long createOrganization(@RequestBody AdminOrganizationUpdateDto organizationDto,
                                   UserContext userContext,
                                   @ModelAttribute UserSecurityHelper userSecurityHelper) throws IOException, UserUnauthorizedException {
        if (!userSecurityHelper.canAdminOrganizations()) throw new UserUnauthorizedException();
        final Long organizationId = organizationService.createOrganization(organizationDto, userContext);
        elasticsearchService.flushSyncedAll();
        userContextFactory.reloadUserContext();
        return organizationId;
    }


    @RequestMapping(method = RequestMethod.POST, path = "{organizationId}/search/{allow}")
    public void toggleOrganizationSearchAllowance(@PathVariable Long organizationId,
                                                 @PathVariable Boolean allow,
                                                 @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canSetSearchAllowance())
            throw new UserUnauthorizedException();
        searchAllowanceService.updateSearchAllowance(organizationId,allow);
    }

    @RequestMapping(method = RequestMethod.POST, path = "{organizationId}/member/{allow}")
    public void toggleOrganizationMemberAllowance(@PathVariable Long organizationId,
                                                  @PathVariable Boolean allow,
                                                  @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditSearchSettings())
            throw new UserUnauthorizedException();
        organizationService.toggleMembers(organizationId, allow);
    }

    @RequestMapping(method = RequestMethod.POST, path = "{organizationId}/searchable/{allow}")
    public void toggleSearchableOrganization(@PathVariable Long organizationId,
                                             @PathVariable Boolean allow,
                                             @ModelAttribute UserSecurityHelper securityHelper) throws UserUnauthorizedException {
        if (!securityHelper.canManageOrganizationRoles(organizationId))
            throw new UserUnauthorizedException();
        organizationService.toggleSearchable(organizationId, allow);
    }
}
