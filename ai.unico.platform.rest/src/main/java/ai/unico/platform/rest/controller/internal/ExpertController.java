package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.search.api.dto.UserExpertPreviewDto;
import ai.unico.platform.search.api.filter.ExpertLookupFilter;
import ai.unico.platform.search.api.service.ElasticsearchService;
import ai.unico.platform.search.api.service.ExpertAutocompleteService;
import ai.unico.platform.search.api.service.ExpertLookupService;
import ai.unico.platform.store.api.dto.UserExpertBaseDto;
import ai.unico.platform.store.api.service.ExpertVerificationService;
import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.store.api.service.RetirementService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("internal/experts")
public class ExpertController {

    @RequestMapping(method = RequestMethod.GET)
    public List<UserExpertPreviewDto> findUserExperts(@ModelAttribute ExpertLookupFilter filter) throws IllegalAccessException, IOException, InstantiationException {
        final ExpertAutocompleteService service = ServiceRegistryUtil.getService(ExpertAutocompleteService.class);
        Set<Long> organizationIds = filter.getOrganizationIds();
        if (organizationIds.size() == 1) {
            final OrganizationStructureService organizationStructureService = ServiceRegistryUtil.getService(OrganizationStructureService.class);
            List<Long> l = organizationStructureService.findMemberOrganizations(organizationIds.iterator().next());
            organizationIds.addAll(l);
        }
        return service.findUserExpertPreviews(filter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{expertCode}")
    public UserExpertBaseDto getUserExpert(@PathVariable("expertCode") String expertCode) throws IOException, NonexistentEntityException {
        //todo load by expertId
        final ExpertLookupService service = ServiceRegistryUtil.getService(ExpertLookupService.class);
        return service.findUserExpertBase(expertCode);
    }

    @RequestMapping(method = RequestMethod.PATCH, path = "/{expertId}")
    public Long verifyExpert(@PathVariable Long expertId,
                             @ModelAttribute UserSecurityHelper userSecurityHelper,
                             UserContext userContext) throws IOException, UserUnauthorizedException {
        final ExpertVerificationService service = ServiceRegistryUtil.getService(ExpertVerificationService.class);
        if (!userSecurityHelper.canVerifyExpert()) throw new UserUnauthorizedException();
        final Long userId = service.createUserFromExpertProfile(expertId, userContext);
        final ElasticsearchService elasticsearchService = ServiceRegistryUtil.getService(ElasticsearchService.class);
        elasticsearchService.flushSyncedAll();
        return userId;
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/{expertId}")
    public void updateExpert(@PathVariable Long expertId,
                             @RequestParam(required = false) Boolean retired,
                             @ModelAttribute UserSecurityHelper userSecurityHelper,
                             UserContext userContext) throws IOException, UserUnauthorizedException {
        if (retired != null) {
            if (!userSecurityHelper.canSetExpertRetired()) throw new UserUnauthorizedException();
            final RetirementService retirementService = ServiceRegistryUtil.getService(RetirementService.class);
            retirementService.setExpertRetired(expertId, retired, userContext);
        }
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void updateExperts(@RequestParam Set<Long> expertIds,
                              @RequestParam(required = false) Boolean retired,
                              @ModelAttribute UserSecurityHelper userSecurityHelper,
                              UserContext userContext) throws IOException, UserUnauthorizedException {
        if (retired != null) {
            if (!userSecurityHelper.canSetExpertRetired()) throw new UserUnauthorizedException();
            final RetirementService retirementService = ServiceRegistryUtil.getService(RetirementService.class);
            for (Long expertId : expertIds) {
                retirementService.setExpertRetired(expertId, retired, userContext);
            }
        }
    }
}
