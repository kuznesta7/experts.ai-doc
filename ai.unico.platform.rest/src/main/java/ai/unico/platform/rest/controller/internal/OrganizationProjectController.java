package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.ProjectDto;
import ai.unico.platform.store.api.service.ProjectOrganizationService;
import ai.unico.platform.store.api.service.ProjectService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("internal/organizations/{organizationId}/projects")
public class OrganizationProjectController {

    private final ProjectOrganizationService projectOrganizationService = ServiceRegistryProxy.createProxy(ProjectOrganizationService.class);
    private final ProjectService projectService = ServiceRegistryProxy.createProxy(ProjectService.class);

    @RequestMapping(method = RequestMethod.POST)
    public Long saveOrganizationProject(@PathVariable Long organizationId,
                                        @RequestBody ProjectDto projectDto,
                                        UserContext userContext,
                                        @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NonexistentEntityException {
        if (!userSecurityHelper.canCreateOrganizationProject(organizationId)) throw new UserUnauthorizedException();
        projectDto.setProjectOwnerOrganizationId(organizationId);
        final Long projectId = projectService.createProject(projectDto, userContext);
        projectDto.setProjectId(projectId);
        projectService.loadAndIndexProject(projectId);
        return projectId;
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void verifyProjectOrganization(@PathVariable Long organizationId,
                                          @RequestParam Set<Long> projectIds,
                                          @RequestParam(required = false, defaultValue = "true") Boolean verify,
                                          UserContext userContext,
                                          @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (verify != null) {
            if (!userSecurityHelper.canVerifyProjectOrganization(organizationId)) throw new UserUnauthorizedException();
            projectOrganizationService.verifyOrganizationProjects(projectIds, organizationId, verify, userContext);
        }
        projectService.loadAndIndexProjects(projectIds);
    }


    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteProjectOrganization(@RequestParam Set<Long> projectIds,
                                          @PathVariable Long organizationId,
                                          UserContext userContext,
                                          @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NonexistentEntityException {
        if (!userSecurityHelper.canVerifyProjectOrganization(organizationId)) throw new UserUnauthorizedException();
        projectOrganizationService.removeOrganizationProjects(projectIds, organizationId, userContext);
        projectService.loadAndIndexProjects(projectIds);
    }
}
