package ai.unico.platform.rest.controller.common;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.rest.util.ControllerUtil;
import ai.unico.platform.store.api.dto.UserProfileDto;
import ai.unico.platform.store.api.service.RetirementService;
import ai.unico.platform.store.api.service.UserProfileImgService;
import ai.unico.platform.store.api.service.UserProfileService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.exception.NoLoggedInUserException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Set;

@RestController
@RequestMapping("common/profiles/users")
public class UserProfileController {

    final private UserProfileService userProfileService = ServiceRegistryProxy.createProxy(UserProfileService.class);
    final private UserProfileImgService userProfileImgService = ServiceRegistryProxy.createProxy(UserProfileImgService.class);
    final private RetirementService retirementService = ServiceRegistryProxy.createProxy(RetirementService.class);

    @RequestMapping(path = "{userId}", method = RequestMethod.PUT)
    public void updateProfile(@PathVariable("userId") Long userId,
                              @RequestBody UserProfileDto userProfileDto,
                              UserContext userContext,
                              @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException, NoLoggedInUserException {
        if (userContext == null) throw new NoLoggedInUserException();
        if (!userSecurityHelper.canEditUserProfile(userId)) throw new UserUnauthorizedException();
        userProfileDto.setUserId(userId);
        userProfileService.updateProfile(userProfileDto, userContext);
        userProfileService.loadAndIndexUserProfile(userId);
    }

    @RequestMapping(path = "{userId}/retired", method = RequestMethod.PUT)
    public void updateProfileRetirement(@PathVariable("userId") Long userId,
                                        boolean retired,
                                        UserContext userContext,
                                        @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException, NoLoggedInUserException {
        if (!userSecurityHelper.canSetExpertRetired()) throw new UserUnauthorizedException();
        retirementService.setUserRetired(userId, retired, userContext);
        userProfileService.loadAndIndexUserProfile(userId);
    }


    @RequestMapping(method = RequestMethod.PUT)
    public void updateProfiles(@RequestParam Set<Long> userIds,
                               @RequestParam(required = false) Boolean retired,
                               UserContext userContext,
                               @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException, NoLoggedInUserException {
        if (userContext == null) throw new NoLoggedInUserException();
        if (retired != null) {
            if (!userSecurityHelper.canSetExpertRetired()) throw new UserUnauthorizedException();
            for (Long userId : userIds) {
                retirementService.setUserRetired(userId, retired, userContext);
            }
        }
        userProfileService.loadAndIndexUserProfiles(userIds);
    }

    @RequestMapping(value = "{userId}/img", method = RequestMethod.PUT, consumes = {"multipart/form-data"})
    public void uploadProfileImg(UserContext userContext,
                                 @ModelAttribute UserSecurityHelper userSecurityHelper,
                                 @PathVariable("userId") final Long userId,
                                 @RequestParam UserProfileImgService.ImgType type,
                                 @RequestParam String fileName,
                                 @RequestParam(value = "content", required = false) MultipartFile multipartFile) throws IOException, UserUnauthorizedException, NoLoggedInUserException {
        if (userContext == null) throw new NoLoggedInUserException();
        if (!userSecurityHelper.canEditUserProfile(userId)) throw new UserUnauthorizedException();

        final byte[] bytes = multipartFile.getBytes();
        userProfileImgService.saveImg(userContext, userId, type, new FileDto(bytes, fileName));
    }

    @ResponseBody
    @RequestMapping(value = "{userId}/img", method = RequestMethod.GET)
    public HttpEntity<byte[]> getLogo(@PathVariable("userId") final Long userId,
                                      @RequestParam(required = false) UserProfileImgService.ImgType type,
                                      @RequestParam(required = false) String size) {
        final FileDto fileDto = userProfileImgService.getImg(userId, type, size);
        return ControllerUtil.toFileResponse(fileDto);
    }
}
