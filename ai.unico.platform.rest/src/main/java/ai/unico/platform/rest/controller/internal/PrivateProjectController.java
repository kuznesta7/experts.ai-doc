package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.PrivateProjectDto;
import ai.unico.platform.store.api.service.ProjectService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("internal/organizations/{organizationId}/projects/{projectId}")
public class PrivateProjectController {

    @RequestMapping(method = RequestMethod.GET)
    public PrivateProjectDto getProject(@PathVariable Long projectId,
                                        @PathVariable Long organizationId,
                                        UserContext userContext,
                                        @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException, IOException {

        if (!userSecurityHelper.canSeePrivateOrganizationProject(organizationId))
            throw new UserUnauthorizedException();
        final ProjectService projectService = ServiceRegistryUtil.getService(ProjectService.class);
        return projectService.findPrivateProject(projectId, organizationId);
    }
}
