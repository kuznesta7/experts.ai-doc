package ai.unico.platform.rest.controller.common;

import ai.unico.platform.rest.security.SecurityHelper;
import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.search.api.dto.UserExpertProfileSearchDetailDto;
import ai.unico.platform.search.api.filter.CommercializationFilter;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.filter.ProjectFilter;
import ai.unico.platform.search.api.service.*;
import ai.unico.platform.search.api.wrapper.*;
import ai.unico.platform.store.api.service.CommercializationStatusService;
import ai.unico.platform.store.api.service.ProfileVisitationService;
import ai.unico.platform.store.api.service.ShareCodeService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.exception.NoLoggedInUserException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Collections;

@RestController
@RequestMapping("common/profiles")
public class SearchProfileController {

    @Autowired
    private SecurityHelper securityHelper;

    private final ShareCodeService shareCodeService = ServiceRegistryProxy.createProxy(ShareCodeService.class);

    @RequestMapping(path = "experts/{expertCode}", method = RequestMethod.GET)
    UserExpertProfileSearchDetailDto findExpertProfile(
            @PathVariable String expertCode,
            @RequestParam(required = false) Long searchId,
            UserContext userContext,
            @ModelAttribute ExpertSearchResultFilter filter) throws IOException, NonexistentEntityException {
        final ExpertSearchService service = ServiceRegistryUtil.getService(ExpertSearchService.class);
        final ProfileVisitationService visitationService = ServiceRegistryUtil.getService(ProfileVisitationService.class);

        final UserExpertProfileSearchDetailDto expertProfileSearch = service.findSearchByExpertiseDetail(expertCode, filter);

        if (expertProfileSearch.getUserId() != null) {
            visitationService.saveUserProfileVisitation(userContext, expertProfileSearch.getUserId(), searchId);
        } else {
            visitationService.saveExpertProfileVisitation(userContext, expertProfileSearch.getExpertId(), searchId);
        }
        return expertProfileSearch;
    }

    @RequestMapping(path = "experts/{expertCode}/items", method = RequestMethod.GET)
    EvidenceItemWrapper findExpertItems(
            @PathVariable String expertCode,
            @RequestParam(required = false) String shareCode,
            UserContext userContext,
            @ModelAttribute UserSecurityHelper userSecurityHelper,
            @ModelAttribute EvidenceFilter filter) throws IOException, NonexistentEntityException, UserUnauthorizedException, NoLoggedInUserException {
        //todo resolve security and hidden items
//        checkShareCodeOrAccessibility(expertCode, shareCode, userSecurityHelper, filter);
//        filter.setShowHidden(userSecurityHelper.canEditProfile(expertCode));
        filter.setConfidentiality(Collections.singleton(Confidentiality.PUBLIC));
        filter.setExpertCodes(Collections.singleton(expertCode));
        final EvidenceItemService service = ServiceRegistryUtil.getService(EvidenceItemService.class);
        assert service != null;
        return service.findAnalyticsItemsWithOrder(filter, ExpertSearchResultFilter.Order.YEAR_DESC);
    }

    @RequestMapping(path = "experts/{expertCode}/commercialization", method = RequestMethod.GET)
    EvidenceCommercializationWrapper findExpertInvestProjects(
            @PathVariable String expertCode,
            @RequestParam(required = false) String shareCode,
            UserContext userContext,
            @ModelAttribute UserSecurityHelper userSecurityHelper,
            @ModelAttribute CommercializationFilter filter) throws IOException, NonexistentEntityException, UserUnauthorizedException, NoLoggedInUserException {
        final EvidenceCommercializationService evidenceCommercializationService = ServiceRegistryUtil.getService(EvidenceCommercializationService.class);
        final CommercializationStatusService commercializationStatusService = ServiceRegistryUtil.getService(CommercializationStatusService.class);
        //todo resolve security
//        checkShareCodeOrAccessibility(expertCode, shareCode, userSecurityHelper, filter);
        filter.setExpertCodes(Collections.singleton(expertCode));
        filter.setCommercializationStatusIds(commercializationStatusService.findStatusIdsForSearch());
        return evidenceCommercializationService.findEvidenceCommercialization(filter);
    }

    @RequestMapping(path = "experts/{expertCode}/projects", method = RequestMethod.GET)
    ProjectWrapper findExpertProjects(
            @PathVariable String expertCode,
            @RequestParam(required = false) String shareCode,
            UserContext userContext,
            @ModelAttribute UserSecurityHelper userSecurityHelper,
            @ModelAttribute ProjectFilter filter) throws IOException, NonexistentEntityException, UserUnauthorizedException, NoLoggedInUserException {
        //todo resolve security
//        checkShareCodeOrAccessibility(expertCode, shareCode, userSecurityHelper, filter);
        filter.setExpertCodes(Collections.singleton(expertCode));
        filter.setConfidentiality(Collections.singleton(Confidentiality.PUBLIC));
        final EvidenceProjectService service = ServiceRegistryUtil.getService(EvidenceProjectService.class);
        return service.findProjects(filter);
    }

    @RequestMapping(path = "experts/{expertCode}/thesis", method = RequestMethod.GET)
    ThesisWrapper findExpertThesis(
            @PathVariable String expertCode,
            @RequestParam(required = false) String shareCode,
            UserContext userContext,
            @ModelAttribute UserSecurityHelper userSecurityHelper,
            @ModelAttribute EvidenceFilter filter) throws IOException, NonexistentEntityException, UserUnauthorizedException, NoLoggedInUserException {
        filter.setConfidentiality(Collections.singleton(Confidentiality.PUBLIC));
        filter.setExpertCodes(Collections.singleton(expertCode));
        final ThesisSearchService service = ServiceRegistryUtil.getService(ThesisSearchService.class);
        return service.findThesis(filter, ExpertSearchResultFilter.Order.QUALITY);
    }


    @RequestMapping(path = "experts/{expertCode}/lectures", method = RequestMethod.GET)
    LectureWrapper findExpertLectures(
            @PathVariable String expertCode,
            @RequestParam(required = false) String shareCode,
            UserContext userContext,
            @ModelAttribute UserSecurityHelper userSecurityHelper,
            @ModelAttribute EvidenceFilter filter) throws IOException, NonexistentEntityException, UserUnauthorizedException, NoLoggedInUserException {
        filter.setConfidentiality(Collections.singleton(Confidentiality.PUBLIC));
        filter.setExpertCodes(Collections.singleton(expertCode));
        final LectureSearchService service = ServiceRegistryUtil.getService(LectureSearchService.class);
        return service.findLecture(filter, ExpertSearchResultFilter.Order.QUALITY);
    }

    private void checkShareCodeOrAccessibility(String expertCode, String shareCode, UserSecurityHelper userSecurityHelper, ExpertSearchResultFilter filter) throws UserUnauthorizedException, NoLoggedInUserException {
        final boolean expertCodeValid = shareCode != null && shareCodeService.verifyExpertProfileCode(shareCode, expertCode);
        if (shareCode == null || !expertCodeValid) {
            securityHelper.checkAccessibility(filter, userSecurityHelper);
        }
    }
}
