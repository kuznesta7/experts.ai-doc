package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.ProjectProgramPreviewDto;
import ai.unico.platform.store.api.service.ProjectProgramService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("internal/projects/programs")
public class ProjectProgramController {
    private ProjectProgramService projectProgramService = ServiceRegistryProxy.createProxy(ProjectProgramService.class);

    @RequestMapping(method = RequestMethod.GET)
    public List<ProjectProgramPreviewDto> findAllProjectPrograms(UserContext userContext,
                                                                 @ModelAttribute UserSecurityHelper userSecurityHelper) {
        return projectProgramService.findPreviews();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/autocomplete")
    public List<ProjectProgramPreviewDto> autocompleteProjects(UserContext userContext,
                                                               @RequestParam String query,
                                                               @ModelAttribute UserSecurityHelper userSecurityHelper) {
        List<ProjectProgramPreviewDto> l = projectProgramService.autocompleteProjectProgram(query);
        return l;
    }
}
