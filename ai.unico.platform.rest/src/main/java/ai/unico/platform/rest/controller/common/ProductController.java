package ai.unico.platform.rest.controller.common;

import ai.unico.platform.ume.api.dto.FeatureDto;
import ai.unico.platform.ume.api.dto.ProductDto;
import ai.unico.platform.ume.api.service.ProductService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("common/products")
public class ProductController {

    private ProductService productService = ServiceRegistryProxy.createProxy(ProductService.class);

    @RequestMapping(method = RequestMethod.GET)
    public List<ProductDto> getProducts(UserContext userContext) {
        return productService.findProducts(userContext);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/features")
    public List<FeatureDto> getFeatures() {
        return productService.findAllFeatures();
    }
}
