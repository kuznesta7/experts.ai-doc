package ai.unico.platform.rest.factory;

import ai.unico.platform.rest.PlatformSessionListener;
import ai.unico.platform.store.api.dto.UserProfileDto;
import ai.unico.platform.store.api.service.CountryOfInterestService;
import ai.unico.platform.store.api.service.OrganizationRoleService;
import ai.unico.platform.store.api.service.SearchAllowanceService;
import ai.unico.platform.store.api.service.UserProfileService;
import ai.unico.platform.ume.api.dto.UserDto;
import ai.unico.platform.ume.api.service.UserActivityService;
import ai.unico.platform.ume.api.service.UserRoleService;
import ai.unico.platform.ume.api.service.UserService;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.dto.OrganizationRolesDto;
import ai.unico.platform.util.enums.Role;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.RuntimeExceptionWithMessage;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@ControllerAdvice
public class UserContextFactory {

    public static final String USER_CONTEXT_SESSION_KEY = "USER_CONTEXT_SESSION_KEY";

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Value("${app.security.maxUserSessions}")
    private Integer maxUserSessions;

    @ModelAttribute
    public UserContext getUserContext() throws NonexistentEntityException {
        final HttpSession session = httpServletRequest.getSession();
        final Object userContextFromSession = session.getAttribute(USER_CONTEXT_SESSION_KEY);
        if (userContextFromSession != null)
            return (UserContext) userContextFromSession;

        final UserContext userContext = createUserContext();
        session.setAttribute(USER_CONTEXT_SESSION_KEY, userContext);

        if (userContext != null && userContext.getUserId() != null) {
            PlatformSessionListener.logoutRedundantUsers(userContext.getUserId(), maxUserSessions);
        }
        return userContext;
    }

    public void reloadUserContext() {
        httpServletRequest.getSession().removeAttribute(UserContextFactory.USER_CONTEXT_SESSION_KEY);
    }

    public void logout() throws ServletException {
        final UserActivityService userActivityService = ServiceRegistryUtil.getService(UserActivityService.class);
        final UserContext userContext = getUserContext();
        if (userContext == null) return;
        userActivityService.saveActivity(userContext.getUserId(), UserActivityService.USER_LOGOUT, null);
        httpServletRequest.logout();
        httpServletRequest.getSession().removeAttribute(UserContextFactory.USER_CONTEXT_SESSION_KEY);
    }

    public void impersonate(Long userId, UserContext currentUserContext) {
        if (currentUserContext.getImpersonatorUserId() != null)
            throw new RuntimeExceptionWithMessage("Multiple impersonation disabled ", "Multiple impersonation disabled");
        final UserActivityService userActivityService = ServiceRegistryUtil.getService(UserActivityService.class);
        final UserContext userContext = createUserContext(userId, currentUserContext);
        final HttpSession session = httpServletRequest.getSession();
        userActivityService.saveActivity(userId, UserActivityService.IMPERSONATION_STARTED, "impersonator: " + currentUserContext.getUserId());
        session.setAttribute(USER_CONTEXT_SESSION_KEY, userContext);
    }

    public void stopImpersonation() {
        final UserActivityService userActivityService = ServiceRegistryUtil.getService(UserActivityService.class);
        final HttpSession session = httpServletRequest.getSession();
        final UserContext userContext = (UserContext) session.getAttribute(USER_CONTEXT_SESSION_KEY);
        userActivityService.saveActivity(userContext.getUserId(), UserActivityService.IMPERSONATION_ENDED, "impersonator: " + userContext.getImpersonatorUserId());
        session.removeAttribute(USER_CONTEXT_SESSION_KEY);
    }

    private UserContext createUserContext() throws NonexistentEntityException {
        final String remoteUser = httpServletRequest.getRemoteUser();
        if (remoteUser == null) {
            return null;
        }

        final long userId = Long.parseLong(remoteUser);
        return createUserContext(userId, null);
    }


    private UserContext createUserContext(Long userId, UserContext currentUserContext) {
        final UserService userService = ServiceRegistryUtil.getService(UserService.class);
        final UserProfileService userProfileService = ServiceRegistryUtil.getService(UserProfileService.class);
        final UserRoleService userRoleService = ServiceRegistryUtil.getService(UserRoleService.class);
        final UserActivityService userActivityService = ServiceRegistryUtil.getService(UserActivityService.class);
        final OrganizationRoleService organizationRoleService = ServiceRegistryUtil.getService(OrganizationRoleService.class);
        final CountryOfInterestService countryOfInterestService = ServiceRegistryUtil.getService(CountryOfInterestService.class);
        final SearchAllowanceService searchAllowanceService = ServiceRegistryUtil.getService(SearchAllowanceService.class);
        final List<Role> roles = userRoleService.findRolesForUser(userId);
        final Date lastLoginDate = userActivityService.getLastLogin(userId);
        final UserProfileDto userProfileDto = userProfileService.loadProfile(userId);
        final List<OrganizationRolesDto> organizationsForUser = organizationRoleService.findOrganizationsForUser(userId);
        final Set<String> countryOfInterestCodesForUser = countryOfInterestService.findCountryOfInterestCodesForUser(userId);
        final UserDto userDto = userService.find(userId);

        final String username = userDto.getUsername();
        final String email = userDto.getEmail();
        final String fullName = userProfileDto.getName();
        final Long impersonatorUserId = currentUserContext != null ? currentUserContext.getUserId() : null;
        if (!organizationsForUser.isEmpty()) {
            for (OrganizationRolesDto organizationRolesDto : organizationsForUser) {
                for (OrganizationRolesDto.OrganizationRoleDto organizationRole : organizationRolesDto.getRoles()) {
                    for (Role role : organizationRole.getRole().getConnectedRoles()) {
                        if (roles.contains(role) || !organizationRole.isValid()) continue;
                        roles.add(role);
                    }
                }
            }
        }

        final String usernameOrEmail = username != null ? username : email;
        final String expertCode = IdUtil.convertToIntDataCode(userId);
        final boolean canSearch = roles.contains(Role.PORTAL_ADMIN) ||
                roles.contains(Role.PORTAL_MAINTAINER) ||
                searchAllowanceService.isAllowed(organizationsForUser.stream().map(OrganizationRolesDto::getOrganizationId).collect(Collectors.toSet()));
        final UserContext userContext = new UserContext(userId, expertCode, usernameOrEmail, fullName, roles, lastLoginDate, organizationsForUser, impersonatorUserId, countryOfInterestCodesForUser, canSearch);
        if (userProfileDto.getOrganizationDtos().isEmpty() || !userDto.isTermsOfUseAccepted() || countryOfInterestCodesForUser.isEmpty()) {
            userContext.setProfileNeedsToBeCompleted(true);
        }
        return userContext;
    }
}
