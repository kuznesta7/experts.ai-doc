package ai.unico.platform.rest.controller.common;

import ai.unico.platform.store.api.dto.SearchSuggestionDto;
import ai.unico.platform.store.api.service.SearchAutocompleteService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("common/search")
public class SearchSuggestionController {

    @RequestMapping(path = "/suggestions", method = RequestMethod.GET)
    public List<SearchSuggestionDto> getSuggestions(@RequestParam(required = false) String query,
                                                    @RequestParam(required = false) Integer limit,
                                                    @RequestParam(required = false) boolean randomize,
                                                    UserContext userContext) {
        final SearchAutocompleteService searchAutocompleteService = ServiceRegistryUtil.getService(SearchAutocompleteService.class);
        return searchAutocompleteService.findRelevantSuggestions(query, limit, randomize, userContext);
    }

}
