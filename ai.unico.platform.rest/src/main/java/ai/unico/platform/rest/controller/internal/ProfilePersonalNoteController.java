package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.ProfilePersonalNoteDto;
import ai.unico.platform.store.api.service.ProfileNoteService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("internal")
public class ProfilePersonalNoteController {

    private ProfileNoteService profileNoteService = ServiceRegistryProxy.createProxy(ProfileNoteService.class);

    @RequestMapping(method = RequestMethod.PUT, path = {"/users/{userId}/personal-note", "/experts/{expertId}/personal-note"})
    public void followProfile(@PathVariable(required = false) Long userId,
                              @PathVariable(required = false) Long expertId,
                              @RequestBody ProfilePersonalNoteDto personalNoteDto,
                              UserContext userContext,
                              @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManagePersonalNotes()) throw new UserUnauthorizedException();
        personalNoteDto.setUserId(userId);
        personalNoteDto.setExpertId(expertId);
        profileNoteService.saveProfileNote(userContext.getUserId(), personalNoteDto, userContext);
    }

    @RequestMapping(method = RequestMethod.GET, path = {"/users/{userId}/personal-note", "/experts/{expertId}/personal-note"})
    public ProfilePersonalNoteDto findPersonalNote(@PathVariable(required = false) Long userId,
                                                   @PathVariable(required = false) Long expertId,
                                                   UserContext userContext,
                                                   @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManagePersonalNotes()) throw new UserUnauthorizedException();
        if (userId != null) {
            return profileNoteService.findUserProfileNote(userContext.getUserId(), userId);
        } else {
            return profileNoteService.findExpertProfileNote(userContext.getUserId(), expertId);
        }
    }
}
