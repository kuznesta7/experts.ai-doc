package ai.unico.platform.rest.controller.common;

import ai.unico.platform.recommendation.api.service.RecombeeRecommendationService;
import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.search.api.dto.*;
import ai.unico.platform.search.api.filter.*;
import ai.unico.platform.search.api.service.*;
import ai.unico.platform.search.api.wrapper.*;
import ai.unico.platform.store.api.dto.*;
import ai.unico.platform.store.api.enums.WidgetType;
import ai.unico.platform.store.api.service.*;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.enums.ResultCode;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("common/widgets")
public class PublicWidgetController {

    private final EvidenceStatisticsService evidenceStatisticsService = ServiceRegistryProxy.createProxy(EvidenceStatisticsService.class);
    private final EvidenceProjectService evidenceProjectService = ServiceRegistryProxy.createProxy(EvidenceProjectService.class);
    private final EvidenceItemService evidenceItemService = ServiceRegistryProxy.createProxy(EvidenceItemService.class);
    private final RecombeeRecommendationService recombeeRecommendationService = ServiceRegistryProxy.createProxy(RecombeeRecommendationService.class);
    private final EvidenceExpertService evidenceExpertService = ServiceRegistryProxy.createProxy(EvidenceExpertService.class);
    private final OrganizationKeywordService organizationKeywordService = ServiceRegistryProxy.createProxy(OrganizationKeywordService.class);
    private final OrganizationStructureService organizationStructureService = ServiceRegistryProxy.createProxy(OrganizationStructureService.class);
    private final EvidenceCommercializationService evidenceCommercializationService = ServiceRegistryProxy.createProxy(EvidenceCommercializationService.class);
    private final CommercializationProjectSearchService commercializationProjectSearchService = ServiceRegistryProxy.createProxy(CommercializationProjectSearchService.class);
    private final LectureSearchService lectureSearchService = ServiceRegistryProxy.createProxy(LectureSearchService.class);
    private final EquipmentSearchService equipmentSearchService = ServiceRegistryProxy.createProxy(EquipmentSearchService.class);
    private final ExpertSearchService expertSearchService = ServiceRegistryProxy.createProxy(ExpertSearchService.class);
    private final OrganizationService organizationService = ServiceRegistryProxy.createProxy(OrganizationService.class);
    private final WidgetService widgetService = ServiceRegistryProxy.createProxy(WidgetService.class);
    private final InteractionService interactionService = ServiceRegistryProxy.createProxy(InteractionService.class);
    private final OpportunitySearchService opportunitySearchService = ServiceRegistryProxy.createProxy(OpportunitySearchService.class);
    private final StudentService studentService = ServiceRegistryProxy.createProxy(StudentService.class);
    private final SubscriptionService subscriptionService = ServiceRegistryProxy.createProxy(SubscriptionService.class);

    @RequestMapping(method = RequestMethod.POST, path = "subscribe")
    public BaseResultDto WidgetSubscribeNewsLetter(@RequestBody SubscribeDto subscribeDto,
                                                   @RequestParam String recaptchaToken) throws IOException {
//        this.recaptchaValidationService.validateToken(recaptchaToken);  todo - uncomment for recaptcha
        String userToken;
        try {
            userToken = subscriptionService.subscribeForNewsletter(subscribeDto);
        } catch (EntityAlreadyExistsException ex) {
            return new BaseResultDto<>(ResultCode.ALREADY_EXISTS, "Email: " + subscribeDto.getEmail() + " already subscribed!");
        }
        final BaseResultDto<String> result = new BaseResultDto<>(ResultCode.OK, "Email: " + subscribeDto.getEmail() + " successfully subscribed");
        result.setData(userToken);
        return result;
    }

    @RequestMapping(method = RequestMethod.GET, path = "organizations/{organizationId}")
    public WidgetOrganizationOverviewDto findOrganizationOverview(@PathVariable Long organizationId,
                                                                  @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                                  HttpSession httpSession) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeWidget(organizationId, WidgetType.ORGANIZATION_OVERVIEW))
            throw new UserUnauthorizedException();
        final Set<Long> childOrganizationIds = organizationStructureService.findChildOrganizationIds(organizationId, true);
        final WidgetOrganizationOverviewDto dto = new WidgetOrganizationOverviewDto();
        dto.setEvidenceItemStatisticsDto(evidenceStatisticsService.findItemStatistics(organizationId, false));
        dto.setKeywords(organizationKeywordService.findOrganizationKeywords(childOrganizationIds));
        dto.setOrganizationDetailDto(organizationService.findOrganizationDetail(organizationId));
        interactionService.saveInteraction(create(httpSession, organizationId, WidgetType.ORGANIZATION_OVERVIEW));
        return dto;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/experts/autocomplete")
    public List<UserExpertPreviewDto> findUserExperts(@ModelAttribute ExpertLookupFilter filter) throws IllegalAccessException, IOException, InstantiationException {
        final ExpertAutocompleteService service = ServiceRegistryUtil.getService(ExpertAutocompleteService.class);
        return service.findUserExpertPreviews(filter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "organizations/{organizationId}/experts")
    public EvidenceExpertWrapper findOrganizationExperts(@PathVariable Long organizationId,
                                                         @ModelAttribute EvidenceFilter filter,
                                                         @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                         HttpSession httpSession) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeWidget(organizationId, WidgetType.ORGANIZATION_EXPERTS))
            throw new UserUnauthorizedException();
        final InteractionWidgetDto interactionWidgetDto = create(httpSession, organizationId, WidgetType.ORGANIZATION_EXPERTS);
        interactionWidgetDto.setInteractionDetails(filter.getQuery());
        interactionService.saveInteraction(interactionWidgetDto);
        updateFilter(filter, organizationId);
        filter.setIgnoreVerifications(true);
        filter.setRetirementIgnored(false);
        return evidenceExpertService.findAnalyticsExperts(filter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "organizations/{organizationId}/experts/recombee")
    public EvidenceExpertWrapper findOrganizationExpertsRecombee(@PathVariable Long organizationId,
                                                                 @ModelAttribute EvidenceFilter filter,
                                                                 @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                                 HttpSession httpSession) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (!userSecurityHelper.canSeeWidget(organizationId, WidgetType.ORGANIZATION_EXPERTS))
            throw new UserUnauthorizedException();
        final InteractionWidgetDto interactionWidgetDto = create(httpSession, organizationId, WidgetType.ORGANIZATION_EXPERTS);
        interactionWidgetDto.setInteractionDetails(filter.getQuery());
        interactionService.saveInteraction(interactionWidgetDto);
        updateFilter(filter, organizationId);
        filter.setIgnoreVerifications(true);

        OrganizationDetailDto organizationDetailDto = organizationService.findOrganizationDetail(organizationId);
        EvidenceExpertWrapper evidenceExpertWrapper;
        if (organizationDetailDto.getRecombeeDbIdentifier() != null && filter.getUser() != null){
            evidenceExpertWrapper = evidenceExpertService.findAnalyticsExpertsWithRecommendation(filter, organizationDetailDto);
        } else {
            filter.setRetirementIgnored(false);
            evidenceExpertWrapper = evidenceExpertService.findAnalyticsExperts(filter);
        }
        recombeeRecommendationService.fillRecommendationWrapperDto(evidenceExpertWrapper.getRecommendationWrapperDto(), organizationDetailDto);
        return evidenceExpertWrapper;
    }

    @RequestMapping(method = RequestMethod.GET, path = {"organizations/{organizationId}/items", "experts/{expertCode}/items"})
    public EvidenceItemWrapper findOrganizationItems(@PathVariable(required = false) Long organizationId,
                                                     @PathVariable(required = false) String expertCode,
                                                     @ModelAttribute EvidenceFilter filter,
                                                     @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                     HttpSession httpSession) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (organizationId != null) {
            if (!userSecurityHelper.canSeeWidget(organizationId, WidgetType.ORGANIZATION_ITEMS))
                throw new UserUnauthorizedException();
            updateFilter(filter, organizationId);
            final InteractionWidgetDto interactionWidgetDto = create(httpSession, organizationId, WidgetType.ORGANIZATION_ITEMS);
            interactionWidgetDto.setInteractionDetails(filter.getQuery());
            interactionService.saveInteraction(interactionWidgetDto);
        }
        if (expertCode != null) filter.setExpertCodes(Collections.singleton(expertCode));
        filter.setConfidentiality(Collections.singleton(Confidentiality.PUBLIC));
        return evidenceItemService.findAnalyticsItemsWithOrder(filter, ExpertSearchResultFilter.Order.YEAR_DESC);
    }

    @RequestMapping(method = RequestMethod.GET, path = {"organizations/{organizationId}/items/recombee"})
    public EvidenceItemWrapper findOrganizationItemsRecombee(@PathVariable(required = false) Long organizationId,
                                                     @PathVariable(required = false) String expertCode,
                                                     @ModelAttribute EvidenceFilter filter,
                                                     @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                     HttpSession httpSession) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        OrganizationDetailDto organizationDetailDto = organizationService.findOrganizationDetail(organizationId);
        if (organizationId != null) {
            if (!userSecurityHelper.canSeeWidget(organizationId, WidgetType.ORGANIZATION_ITEMS))
                throw new UserUnauthorizedException();
            updateFilter(filter, organizationId);
            final InteractionWidgetDto interactionWidgetDto = create(httpSession, organizationId, WidgetType.ORGANIZATION_ITEMS);
            interactionWidgetDto.setInteractionDetails(filter.getQuery());
            interactionService.saveInteraction(interactionWidgetDto);
        }
        if (expertCode != null) filter.setExpertCodes(Collections.singleton(expertCode));

        filter.setConfidentiality(Collections.singleton(Confidentiality.PUBLIC));
        EvidenceItemWrapper evidenceItemWrapper;
        if (organizationDetailDto.getRecombeeDbIdentifier() != null && filter.getUser() != null){
            evidenceItemWrapper = evidenceItemService.findAnalyticsItemsWithOrderAndRecommendation(filter, ExpertSearchResultFilter.Order.YEAR_DESC, organizationDetailDto);
        } else {
            evidenceItemWrapper = evidenceItemService.findAnalyticsItemsWithOrder(filter, ExpertSearchResultFilter.Order.YEAR_DESC);
        }
        recombeeRecommendationService.fillRecommendationWrapperDto(evidenceItemWrapper.getRecommendationWrapperDto(), organizationDetailDto);
        return evidenceItemWrapper;
    }

    @RequestMapping(method = RequestMethod.GET, path = "organizations/{organizationId}/detail")
    public OrganizationDetailDto getOrganizationDetail(@PathVariable("organizationId") Long organizationId) {
        return organizationService.findOrganizationDetail(organizationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "items/{outcomeCode}")
    public ItemDetailDto getItemDetail(@PathVariable String outcomeCode,
                                       @RequestParam(required = false) Long organizationId,
                                       HttpSession session) throws NonexistentEntityException, IOException, UserUnauthorizedException {

        final ItemLookupService service = ServiceRegistryUtil.getService(ItemLookupService.class);
        InteractionIndividualDto interaction = new InteractionIndividualDto();
        fill(interaction, session, organizationId);
        interaction.setVisitedOutcomeCode(outcomeCode);
        interactionService.saveInteraction(interaction);
        return service.findItemDetail(null, outcomeCode);
    }

    @RequestMapping(method = RequestMethod.GET, path = {"organizations/{organizationId}/projects", "experts/{expertCode}/projects"})
    public ProjectWrapper findOrganizationProjects(@PathVariable(required = false) Long organizationId,
                                                   @PathVariable(required = false) String expertCode,
                                                   @ModelAttribute ProjectFilter filter,
                                                   @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                   HttpSession httpSession) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (organizationId != null) {
            if (!userSecurityHelper.canSeeWidget(organizationId, WidgetType.ORGANIZATION_PROJECTS))
                throw new UserUnauthorizedException();
            updateFilter(filter, organizationId);
            final InteractionWidgetDto interactionWidgetDto = create(httpSession, organizationId, WidgetType.ORGANIZATION_PROJECTS);
            interactionWidgetDto.setInteractionDetails(filter.getQuery());
            interactionService.saveInteraction(interactionWidgetDto);
        }
        if (expertCode != null) filter.setExpertCodes(Collections.singleton(expertCode));
        filter.setConfidentiality(Collections.singleton(Confidentiality.PUBLIC));
        return evidenceProjectService.findProjects(filter);
    }

    @RequestMapping(method = RequestMethod.GET, path = {"organizations/{organizationId}/projects/recombee", "experts/{expertCode}/projects"})
    public ProjectWrapper findOrganizationProjectsRecombee(@PathVariable(required = false) Long organizationId,
                                                   @PathVariable(required = false) String expertCode,
                                                   @ModelAttribute ProjectFilter filter,
                                                   @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                   HttpSession httpSession) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        OrganizationDetailDto organizationDetailDto = organizationService.findOrganizationDetail(organizationId);
        if (organizationId != null) {
            if (!userSecurityHelper.canSeeWidget(organizationId, WidgetType.ORGANIZATION_PROJECTS))
                throw new UserUnauthorizedException();
            updateFilter(filter, organizationId);
            final InteractionWidgetDto interactionWidgetDto = create(httpSession, organizationId, WidgetType.ORGANIZATION_PROJECTS);
            interactionWidgetDto.setInteractionDetails(filter.getQuery());
            interactionService.saveInteraction(interactionWidgetDto);
        }
        if (expertCode != null) filter.setExpertCodes(Collections.singleton(expertCode));

        filter.setConfidentiality(Collections.singleton(Confidentiality.PUBLIC));
        ProjectWrapper projectWrapper;
        if (organizationDetailDto.getRecombeeDbIdentifier() != null && filter.getUser() != null){
            projectWrapper = evidenceProjectService.findProjectsWithRecommendation(filter, organizationDetailDto);
        } else {
            projectWrapper = evidenceProjectService.findProjects(filter);
        }
        recombeeRecommendationService.fillRecommendationWrapperDto(projectWrapper.getRecommendationWrapperDto(), organizationDetailDto);
        return projectWrapper;
    }


    @RequestMapping(method = RequestMethod.GET, path = "projects/{projectCode}")
    public ProjectDetailDto getProject(@PathVariable String projectCode,
                                       @RequestParam(required = false) Long organizationId,
                                       HttpSession session) throws NonexistentEntityException, UserUnauthorizedException, IOException {
        final EvidenceProjectService projectService = ServiceRegistryUtil.getService(EvidenceProjectService.class);
        InteractionIndividualDto interaction = new InteractionIndividualDto();
        fill(interaction, session, organizationId);

        interaction.setVisitedProjectCode(projectCode);
        interactionService.saveInteraction(interaction);
        return projectService.findProject(null, projectCode);
    }

    @RequestMapping(method = RequestMethod.GET, path = {"organizations/{organizationId}/commercialization", "experts/{expertCode}/commercialization"})
    public EvidenceCommercializationWrapper findOrganizationCommercialization(@PathVariable(required = false) Long organizationId,
                                                                              @PathVariable(required = false) String expertCode,
                                                                              @ModelAttribute CommercializationFilter filter,
                                                                              @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                                              HttpSession httpSession) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (organizationId != null) {
            if (!userSecurityHelper.canSeeWidget(organizationId, WidgetType.ORGANIZATION_COMMERCIALIZATION))
                throw new UserUnauthorizedException();
            updateFilter(filter, organizationId);
            final InteractionWidgetDto interactionWidgetDto = create(httpSession, organizationId, WidgetType.ORGANIZATION_COMMERCIALIZATION);
            interactionWidgetDto.setInteractionDetails(filter.getQuery());
            interactionService.saveInteraction(interactionWidgetDto);
        }
        if (expertCode != null) filter.setExpertCodes(Collections.singleton(expertCode));
        final CommercializationStatusService commercializationStatusService = ServiceRegistryUtil.getService(CommercializationStatusService.class);
        final Set<Long> statusIdsForSearch = commercializationStatusService.findStatusIdsForSearch();
        filter.setCommercializationStatusIds(statusIdsForSearch);
        return evidenceCommercializationService.findEvidenceCommercialization(filter);
    }

    @RequestMapping(method = RequestMethod.GET, path = {"organizations/{organizationId}/lectures", "experts/{expertCode}/lectures"})
    public LectureWrapper findOrganizationLectures(@PathVariable(required = false) Long organizationId,
                                                   @PathVariable(required = false) String expertCode,
                                                   @ModelAttribute EvidenceFilter filter,
                                                   @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                   HttpSession httpSession) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        if (organizationId != null) {
            if (!userSecurityHelper.canSeeWidget(organizationId, WidgetType.ORGANIZATION_LECTURES))
                throw new UserUnauthorizedException();
            updateFilter(filter, organizationId);
            final InteractionWidgetDto interactionWidgetDto = create(httpSession, organizationId, WidgetType.ORGANIZATION_LECTURES);
            interactionWidgetDto.setInteractionDetails(filter.getQuery());
            interactionService.saveInteraction(interactionWidgetDto);
        }
        if (expertCode != null) filter.setExpertCodes(Collections.singleton(expertCode));
        filter.setConfidentiality(Collections.singleton(Confidentiality.PUBLIC));
        return lectureSearchService.findLecture(filter, ExpertSearchResultFilter.Order.QUALITY);
    }


    @RequestMapping(method = RequestMethod.GET, path = "commercialization/{commercializationId}")
    public CommercializationProjectDto getCommercialization(@PathVariable Long commercializationId,
                                                            @RequestParam(required = false) Long organizationId,
                                                            @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                            HttpSession httpSession) throws IOException, UserUnauthorizedException {
        if (!userSecurityHelper.canSeeCommercializationProjectDetail(commercializationId)) {
            throw new UserUnauthorizedException();
        }
        InteractionIndividualDto interaction = new InteractionIndividualDto();
        fill(interaction, httpSession, organizationId);
        interaction.setVisitedCommercializationId(commercializationId);
        interactionService.saveInteraction(interaction);
        return commercializationProjectSearchService.findCommercializationProjectDetail(commercializationId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "organizations/{organizationId}/equipment")
    public EquipmentWrapper findEquipment(@PathVariable Long organizationId,
                                          @ModelAttribute EquipmentFilter equipmentFilter,
                                          @ModelAttribute UserSecurityHelper userSecurityHelper) throws IOException, UserUnauthorizedException {
        if (!userSecurityHelper.canSeeWidget(organizationId,WidgetType.ORGANIZATION_EQUIPMENT))
            throw new UserUnauthorizedException();
        if (equipmentFilter.getOrganizationId() == null)
            equipmentFilter.setOrganizationId(organizationId);
        if (equipmentFilter.getOrganizationIds().isEmpty())
            equipmentFilter.setOrganizationIds(organizationStructureService.findChildOrganizationIds(equipmentFilter.getOrganizationId(),true));

        return equipmentSearchService.findEquipment(equipmentFilter, ExpertSearchResultFilter.Order.QUALITY);
    }

    @RequestMapping(method = RequestMethod.GET, path = "equipment/{equipmentCode}")
    public MultilingualDto<EquipmentPreviewDto> getEquipment(@PathVariable String equipmentCode) throws IOException {
        return equipmentSearchService.getEquipment(equipmentCode);
    }

    @RequestMapping(method = RequestMethod.GET, path = "experts/{expertCode}")
    public UserExpertProfileSearchDetailDto findExpertDetail(@PathVariable String expertCode,
                                                             @RequestParam(required = false) String query,
                                                             @RequestParam(required = false) Long organizationId,
                                                             @ModelAttribute UserSecurityHelper userSecurityHelper,
                                                             HttpSession httpSession) throws UserUnauthorizedException, IOException, InstantiationException, IllegalAccessException {
        InteractionIndividualDto interaction = new InteractionIndividualDto();
        fill(interaction, httpSession, organizationId);
        interaction.setVisitedExpertCode(expertCode);
        interactionService.saveInteraction(interaction);
        final ExpertSearchResultFilter filter = new ExpertSearchResultFilter();
        filter.setQuery(query);
        return expertSearchService.findSearchByExpertiseDetail(expertCode, filter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "lectures/{lectureCode}")
    public LecturePreviewDto getLecture(@PathVariable String lectureCode,
                                        @RequestParam(required = false) Long organizationId,
                                        @ModelAttribute UserSecurityHelper userSecurityHelper,
                                        HttpSession httpSession) throws IOException, UserUnauthorizedException {

        InteractionIndividualDto interaction = new InteractionIndividualDto();
        fill(interaction, httpSession, organizationId);
        interaction.setVisitedLectureCode(lectureCode);
        interactionService.saveInteraction(interaction);
        return lectureSearchService.getLecture(lectureCode);
    }

    @RequestMapping(method = RequestMethod.GET,path = "organizations/{organizationId}/opportunity")
    public OpportunityWrapper findOpportunities(@PathVariable Long organizationId,
                                                @ModelAttribute OpportunityFilter opportunityFilter,
                                                HttpSession httpSession) throws IOException {
        if (opportunityFilter.getOrganizationId() == null)
            opportunityFilter.setOrganizationId(organizationId);
        if (opportunityFilter.getOrganizationIds().isEmpty())
            opportunityFilter.setOrganizationIds(organizationStructureService.findChildOrganizationIds(opportunityFilter.getOrganizationId(),true));
        final InteractionWidgetDto interactionWidgetDto = create(httpSession, organizationId, WidgetType.ORGANIZATION_OPPORTUNITY);
        interactionWidgetDto.setInteractionDetails(opportunityFilter.getQuery());
        interactionService.saveInteraction(interactionWidgetDto);
        return opportunitySearchService.findOpportunities(opportunityFilter);
    }

    @RequestMapping(method = RequestMethod.GET,path = "organizations/{organizationId}/opportunity/recombee")
    public OpportunityWrapper findOpportunitiesRecombee(@PathVariable Long organizationId,
                                                        @ModelAttribute OpportunityFilter opportunityFilter,
                                                        HttpSession httpSession) throws IOException {
        if (opportunityFilter.getOrganizationId() == null)
            opportunityFilter.setOrganizationId(organizationId);
        if (opportunityFilter.getOrganizationIds().isEmpty())
            opportunityFilter.setOrganizationIds(organizationStructureService.findChildOrganizationIds(opportunityFilter.getOrganizationId(),true));
        OrganizationDetailDto organizationDetailDto = organizationService.findOrganizationDetail(organizationId);
        final InteractionWidgetDto interactionWidgetDto = create(httpSession, organizationId, WidgetType.ORGANIZATION_OPPORTUNITY);
        interactionWidgetDto.setInteractionDetails(opportunityFilter.getQuery());
        interactionService.saveInteraction(interactionWidgetDto);
        String studentHash = opportunityFilter.getStudentHash();
        if (studentHash != null && false) {  // todo - change back after DB update
            String group = studentService.getStudentTestGroup(studentHash);
            final List<String> recommendations = studentService.getStudentRecommendations(studentHash);
            OpportunityWrapper opportunityWrapper = opportunitySearchService.findOpportunitiesWithCombinedRecommendation(opportunityFilter, organizationDetailDto, recommendations);
            if (opportunityWrapper != null) {
                recombeeRecommendationService.fillRecommendationWrapperDto(opportunityWrapper.getRecommendationWrapperDto(), organizationDetailDto);
                opportunityWrapper.getRecommendationWrapperDto().setTestGroup(group);
                return opportunityWrapper;
            }
        }
        OpportunityWrapper opportunityWrapper;
        if (organizationDetailDto.getRecombeeDbIdentifier() != null && opportunityFilter.getUser() != null) {
            opportunityWrapper = opportunitySearchService.findOpportunitiesWithRecommendation(opportunityFilter, organizationDetailDto);
        } else {
            opportunityWrapper = opportunitySearchService.findOpportunities(opportunityFilter);
        }
        recombeeRecommendationService.fillRecommendationWrapperDto(opportunityWrapper.getRecommendationWrapperDto(), organizationDetailDto);
        return opportunityWrapper;
    }

    @RequestMapping(method = RequestMethod.GET, path = "opportunity/{opportunityId}")
    public OpportunityPreviewDto getOpportunity(@PathVariable String opportunityId) throws IOException {
        return opportunitySearchService.findOpportunity(opportunityId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "organizations/{organizationId}/allowed")
    public Map<WidgetType, Boolean> getAllowedWidgets(@PathVariable Long organizationId,
                                                      @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canSeeOrganizationEvidence(organizationId))
            throw new UserUnauthorizedException();
        return widgetService.allowed(organizationId);
    }

    @RequestMapping(method = RequestMethod.POST, path = "organizations/{organizationId}/allowed")
    public void setAllowedWidgets(@PathVariable Long organizationId,
                                  @RequestBody Map<WidgetType, Boolean> widgetChanges,
                                  UserContext userContext,
                                  @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditWidgetSettings(organizationId))
            throw new UserUnauthorizedException();
        widgetService.updateWidgets(organizationId, widgetChanges, userContext);

    }

    private void updateFilter(OrganizationEvidenceFilter filter, Long organizationId) {
        final Set<Long> childOrganizationIds = organizationStructureService.findChildOrganizationIds(organizationId, true);
        filter.setOrganizationId(organizationId);
        filter.setOrganizationIds(childOrganizationIds);
    }

    private InteractionWidgetDto create(HttpSession session, Long organizationId, WidgetType widgetType) {
        InteractionWidgetDto widgetDto = new InteractionWidgetDto();
        fill(widgetDto, session, organizationId);
        widgetDto.setInteractionType(widgetType);
        return widgetDto;
    }

    private void fill(InteractionDto interactionDto, HttpSession session, Long organizationId) {
        interactionDto.setSessionId(session.getId());
        interactionDto.setOrganization(organizationId);
    }

    public static class WidgetOrganizationOverviewDto {

        private EvidenceItemStatisticsDto evidenceItemStatisticsDto;

        private Map<String, Long> keywords;

        private OrganizationDetailDto organizationDetailDto;

        public EvidenceItemStatisticsDto getEvidenceItemStatisticsDto() {
            return evidenceItemStatisticsDto;
        }

        public void setEvidenceItemStatisticsDto(EvidenceItemStatisticsDto evidenceItemStatisticsDto) {
            this.evidenceItemStatisticsDto = evidenceItemStatisticsDto;
        }

        public Map<String, Long> getKeywords() {
            return keywords;
        }

        public void setKeywords(Map<String, Long> keywords) {
            this.keywords = keywords;
        }

        public OrganizationDetailDto getOrganizationDetailDto() {
            return organizationDetailDto;
        }

        public void setOrganizationDetailDto(OrganizationDetailDto organizationDetailDto) {
            this.organizationDetailDto = organizationDetailDto;
        }
    }
}
