package ai.unico.platform.rest.controller.common;

import ai.unico.platform.util.GitProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/common/config", produces = "application/json")
public class InitConfigController {

    @Value("${app.analytics.code}")
    private String analyticsCode;

    @Value("${env.type}")
    private String environment;

    @Value("${app.recaptcha.key.public}")
    private String recpatchaPublicKey;

    @RequestMapping(method = RequestMethod.GET)
    public Map<String, Object> getConfig() {
        final Map<String, Object> config = new HashMap<>();
        config.put("googleAnalyticsCode", analyticsCode);
        config.put("platformVersion", GitProperties.getLastCommitId());
        config.put("environment", environment);
        config.put("recpatchaPublicKey", recpatchaPublicKey);
        return config;
    }
}
