package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.CommercializationCategoryDto;
import ai.unico.platform.store.api.service.CommercializationCategoryService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("internal/commercialization/categories")
public class CommercializationCategoryController {

    private final CommercializationCategoryService commercializationCategoryService = ServiceRegistryProxy.createProxy(CommercializationCategoryService.class);

    @RequestMapping(method = RequestMethod.GET)
    public List<CommercializationCategoryDto> findAllCategories(@RequestParam(required = false) boolean includeInactive) {
        return commercializationCategoryService.findAvailableCommercializationCategories(includeInactive);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Long createCategory(@RequestBody CommercializationCategoryDto commercializationCategoryDto,
                               UserContext userContext,
                               @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canAdminCommercialization()) throw new UserUnauthorizedException();
        return commercializationCategoryService.createCommercializationCategory(commercializationCategoryDto, userContext);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{categoryId}")
    public void updateCategory(@RequestBody CommercializationCategoryDto commercializationCategoryDto,
                               @PathVariable Long categoryId,
                               UserContext userContext,
                               @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canAdminCommercialization()) throw new UserUnauthorizedException();
        commercializationCategoryService.updateCommercializationCategory(categoryId, commercializationCategoryDto, userContext);
    }
}
