package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.SearchStatisticsDto;
import ai.unico.platform.store.api.filter.SearchStatisticsFilter;
import ai.unico.platform.store.api.service.SearchStatisticsService;
import ai.unico.platform.store.api.wrapper.SearchStatisticsWrapper;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("common/search/statistics")
public class SearchStatisticsController {

    private final SearchStatisticsService statisticsService = ServiceRegistryProxy.createProxy(SearchStatisticsService.class);

    @RequestMapping(method = RequestMethod.GET)
    public SearchStatisticsWrapper findSearchStatistics(
            @ModelAttribute SearchStatisticsFilter filter,
            @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canSeeSearchStatistics()) throw new UserUnauthorizedException();
        return statisticsService.findSearchedData(filter);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/hot-topics")
    public List<SearchStatisticsDto> findHotTopics(
            @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        return statisticsService.findHotTopics();
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void editSearchWord(
            @RequestBody SearchStatisticsDto statisticsDto,
            @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditSearchStatistics()) throw new UserUnauthorizedException();
        statisticsService.updateSearchedData(statisticsDto);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Long addSearchWord(
            @RequestBody SearchStatisticsDto statisticsDto,
            @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditSearchStatistics()) throw new UserUnauthorizedException();
        return statisticsService.addSearchedData(statisticsDto);
    }


}
