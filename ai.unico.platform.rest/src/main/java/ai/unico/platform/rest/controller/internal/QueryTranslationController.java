package ai.unico.platform.rest.controller.internal;


import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.QueryTranslationDto;
import ai.unico.platform.store.api.service.QueryTranslationService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("internal/queryTranslations")
public class QueryTranslationController {

    private final QueryTranslationService queryTranslationService = ServiceRegistryProxy.createProxy(QueryTranslationService.class);

    @RequestMapping(method = RequestMethod.GET)
    public List<QueryTranslationDto> getAllTransaction(@ModelAttribute UserSecurityHelper userSecurityHelper)
            throws UserUnauthorizedException {
        if (!userSecurityHelper.canSeeSearchTranslations()) throw new UserUnauthorizedException();
        return queryTranslationService.findAllTranslations();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Long createQueryTransaction(@RequestBody QueryTranslationDto queryTranslationDto,
                                       @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditSearchTranslations()) throw new UserUnauthorizedException();
        return queryTranslationService.createTranslation(queryTranslationDto);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void updateQueryTransaction(@RequestBody QueryTranslationDto queryTranslationDto,
                                       @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditSearchTranslations()) throw new UserUnauthorizedException();
        queryTranslationService.updateTranslation(queryTranslationDto);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{translationId}")
    public void deleteQueryTransaction(@PathVariable Long translationId,
                                       @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canEditSearchTranslations()) throw new UserUnauthorizedException();
        queryTranslationService.deleteTranslation(translationId);
    }
}

