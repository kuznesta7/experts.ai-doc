package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.CommercializationProjectDto;
import ai.unico.platform.store.api.service.CommercializationOrganizationService;
import ai.unico.platform.store.api.service.CommercializationProjectService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("internal/organizations/{organizationId}/commercialization/projects")
public class OrganizationCommercializationController {

    private CommercializationProjectService commercializationProjectService = ServiceRegistryProxy.createProxy(CommercializationProjectService.class);
    private final CommercializationOrganizationService commercializationOrganizationService = ServiceRegistryProxy.createProxy(CommercializationOrganizationService.class);

    @RequestMapping(method = RequestMethod.POST)
    public Long createOrganizationCommercialization(@PathVariable Long organizationId,
                                                    @RequestBody CommercializationProjectDto projectDto,
                                                    UserContext userContext,
                                                    @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NonexistentEntityException {
        projectDto.setOwnerOrganizationId(organizationId);
        if (!userSecurityHelper.canCreateOrganizationCommercializationProject(organizationId))
            throw new UserUnauthorizedException();
        projectDto.setOwnerOrganizationId(organizationId);
        return commercializationProjectService.createCommercializationProject(projectDto, userContext);
    }
}
