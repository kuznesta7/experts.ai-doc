package ai.unico.platform.rest.controller.common;


import ai.unico.platform.search.api.dto.UserExpertProfileSearchDetailDto;
import ai.unico.platform.search.api.filter.ExpertSearchFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.service.ExpertSearchService;
import ai.unico.platform.search.api.service.ItemSearchService;
import ai.unico.platform.search.api.wrapper.ExpertItemWrapper;
import ai.unico.platform.search.api.wrapper.SearchResultWrapper;
import ai.unico.platform.store.api.service.PlatformParamService;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("common/demo/search")
public class SearchDemoController {

    private final PlatformParamService platformParamService = ServiceRegistryProxy.createProxy(PlatformParamService.class);
    private final ExpertSearchService expertSearchService = ServiceRegistryProxy.createProxy(ExpertSearchService.class);
    private final ItemSearchService itemSearchService = ServiceRegistryProxy.createProxy(ItemSearchService.class);

    @RequestMapping
    public SearchResultWrapper findSearchDemo(@ModelAttribute ExpertSearchFilter filter, UserContext userContext) throws IOException {
        fillDemoFilter(filter);
        final SearchResultWrapper searchResultWrapper = expertSearchService.searchExperts(filter, userContext);
        return searchResultWrapper;
    }

    @RequestMapping(path = "/filter")
    public ExpertSearchFilter findSearchFilterDemo(@ModelAttribute ExpertSearchResultFilter searchResultFilter) throws IOException {
        fillDemoFilter(searchResultFilter);
        return searchResultFilter;
    }

    @RequestMapping(path = "/result")
    public UserExpertProfileSearchDetailDto findSearchResultDemo(@ModelAttribute ExpertSearchResultFilter searchResultFilter) throws IOException {
        fillDemoFilter(searchResultFilter);
        return expertSearchService.findSearchByExpertiseDetail(getDemoExpertCode(), searchResultFilter);
    }

    @RequestMapping(path = "/result/items")
    public ExpertItemWrapper findSearchResultItemsDemo(@ModelAttribute ExpertSearchResultFilter searchResultFilter) throws IOException {
        fillDemoFilter(searchResultFilter);
        return itemSearchService.findExpertItems(getDemoExpertCode(), searchResultFilter);
    }

    private void fillDemoFilter(final ExpertSearchFilter searchFilter) {
        final String demoQuery = platformParamService.findPlatformParamValue(PlatformParamService.DEMO_SEARCH_QUERY);
        if (demoQuery == null) throw new NonexistentEntityException();
        searchFilter.setQuery(demoQuery);
        searchFilter.setDemoActive(true);
        final String resultUserIdsString = platformParamService.findPlatformParamValue(PlatformParamService.DEMO_SEARCH_RESULT_USER_IDS);
        if (resultUserIdsString != null) {
            final Set<String> expertCodes = Arrays.stream(resultUserIdsString.split(","))
                    .map(Long::parseLong).map(x -> "ST" + x).collect(Collectors.toSet());
            searchFilter.setAvailableExpertCodes(expertCodes);
        }
    }

    private String getDemoExpertCode() {
        final String userIdString = platformParamService.findPlatformParamValue(PlatformParamService.DEMO_USER_ID);
        if (userIdString == null) throw new NonexistentEntityException();
        return IdUtil.convertToIntDataCode(Long.parseLong(userIdString));
    }
}
