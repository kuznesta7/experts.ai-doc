package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.store.api.dto.ProjectTransactionDto;
import ai.unico.platform.store.api.dto.ProjectTransactionPreviewDto;
import ai.unico.platform.store.api.service.ProjectTransactionService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("internal/projects/{projectId}/transactions")
public class ProjectTransactionController {

    private ProjectTransactionService projectTransactionService = ServiceRegistryProxy.createProxy(ProjectTransactionService.class);

    @RequestMapping(method = RequestMethod.POST)
    public Long addProjectTransaction(@RequestBody ProjectTransactionDto projectTransactionDto,
                                      @PathVariable Long projectId,
                                      UserContext userContext,
                                      @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NonexistentEntityException {
        if (!userSecurityHelper.canEditProject(projectId)) throw new UserUnauthorizedException();
        projectTransactionService.addTransaction(projectId, projectTransactionDto, userContext);
        return projectId;
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{transactionId}")
    public void removeProjectTransaction(@PathVariable Long transactionId,
                                         @PathVariable Long projectId,
                                         UserContext userContext,
                                         @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NonexistentEntityException {
        if (!userSecurityHelper.canEditProject(projectId)) throw new UserUnauthorizedException();
        projectTransactionService.removeTransaction(projectId, transactionId, userContext);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<ProjectTransactionPreviewDto> getProjectTransactions(@PathVariable Long projectId,
                                                                     UserContext userContext,
                                                                     @ModelAttribute UserSecurityHelper userSecurityHelper) throws NonexistentEntityException, UserUnauthorizedException {
        if (!userSecurityHelper.canSeeProject(projectId)) throw new UserUnauthorizedException();
        return projectTransactionService.findProjectTransactions(projectId);
    }
}
