package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.ume.api.dto.BillingInfoDto;
import ai.unico.platform.ume.api.service.BillingInfoService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("internal/users/{userId}/billing-info")
public class BillingInfoController {
    private BillingInfoService billingInfoService = ServiceRegistryProxy.createProxy(BillingInfoService.class);

    @RequestMapping(method = RequestMethod.GET)
    public List<BillingInfoDto> findForUser(UserContext userContext,
                                            @ModelAttribute UserSecurityHelper userSecurityHelper,
                                            @PathVariable Long userId) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManageBillingInfo(userId)) throw new UserUnauthorizedException();
        return billingInfoService.findBillingInfoForUser(userId);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Long create(UserContext userContext,
                       @ModelAttribute UserSecurityHelper userSecurityHelper,
                       @PathVariable Long userId,
                       @RequestBody BillingInfoDto billingInfoDto) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManageBillingInfo(userId)) throw new UserUnauthorizedException();
        billingInfoDto.setUserId(userId);
        return billingInfoService.createBillingInfo(billingInfoDto, userContext);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/{billingInfoId}")
    public Long update(UserContext userContext,
                       @ModelAttribute UserSecurityHelper userSecurityHelper,
                       @PathVariable Long userId,
                       @PathVariable Long billingInfoId,
                       @RequestParam(required = false) Boolean favorite,
                       @RequestBody(required = false) BillingInfoDto billingInfoDto) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManageBillingInfo(userId)) throw new UserUnauthorizedException();
        if (favorite != null) {
            billingInfoService.setDefault(userId, billingInfoId, favorite, userContext);
            return billingInfoId;
        }
        if (billingInfoDto != null) {
            billingInfoDto.setBillingInfoId(billingInfoId);
            billingInfoDto.setUserId(userId);
            return billingInfoService.updateBillingInfo(billingInfoDto, userContext);
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{billingInfoId}")
    public void delete(UserContext userContext,
                       @ModelAttribute UserSecurityHelper userSecurityHelper,
                       @PathVariable Long userId,
                       @PathVariable Long billingInfoId) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManageBillingInfo(userId)) throw new UserUnauthorizedException();
        if (billingInfoService.findBillingInfoForUser(userId).stream()
                .noneMatch(billingInfoDto -> billingInfoDto.getUserId().equals(userId)))
            throw new NonexistentEntityException(BillingInfoDto.class);
        billingInfoService.deleteBillingInfo(billingInfoId, userContext);
    }

}
