package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.SecurityHelper;
import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.store.api.dto.ShareCodeDto;
import ai.unico.platform.store.api.enums.ShareCodeType;
import ai.unico.platform.store.api.service.ShareCodeService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NoLoggedInUserException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("internal/share-codes")
public class ShareCodeController {

    @Autowired
    private SecurityHelper securityHelper;

    private final ShareCodeService shareCodeService = ServiceRegistryProxy.createProxy(ShareCodeService.class);

    @RequestMapping(method = RequestMethod.POST, path = "/search")
    public ShareCodeDto generateShareCodeForSearch(@RequestParam Long searchHistId,
                                                   UserContext userContext,
                                                   @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canUserEditSearchHistory(searchHistId)) throw new UserUnauthorizedException();
        final String shareCode = shareCodeService.generateShareCode(ShareCodeType.SEARCH, String.valueOf(searchHistId), userContext);
        return new ShareCodeDto(shareCode);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/experts")
    public ShareCodeDto generateShareCodeForExpertProfile(@RequestParam String expertCode,
                                                          @ModelAttribute ExpertSearchResultFilter filter,
                                                          UserContext userContext,
                                                          @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException, NoLoggedInUserException {
        securityHelper.checkAccessibility(filter, userSecurityHelper);
        final String shareCode = shareCodeService.generateShareCode(ShareCodeType.EXPERT_PROFILE, expertCode, userContext);
        return new ShareCodeDto(shareCode);
    }
}
