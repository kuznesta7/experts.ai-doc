package ai.unico.platform.rest.controller.common;

import ai.unico.platform.search.api.dto.ExpertClaimPreviewDto;
import ai.unico.platform.search.api.service.ExpertClaimSearchService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("common/claims")
public class PublicClaimLookupController {

    @RequestMapping(method = RequestMethod.GET)
    public List<ExpertClaimPreviewDto> findExpertsToClaim(
            @RequestParam String query) throws UserUnauthorizedException, NonexistentEntityException, IOException {
        final ExpertClaimSearchService service = ServiceRegistryUtil.getService(ExpertClaimSearchService.class);
        return service.findExpertsToClaim(query);
    }
}
