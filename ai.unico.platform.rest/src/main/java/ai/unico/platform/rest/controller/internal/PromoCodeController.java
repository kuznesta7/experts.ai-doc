package ai.unico.platform.rest.controller.internal;

import ai.unico.platform.rest.security.UserSecurityHelper;
import ai.unico.platform.ume.api.dto.PromoCodeDto;
import ai.unico.platform.ume.api.service.PromoService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("internal/promo")
public class PromoCodeController {

    private PromoService promoService = ServiceRegistryProxy.createProxy(PromoService.class);

    @RequestMapping(method = RequestMethod.POST)
    public Long createPromo(@RequestBody PromoCodeDto promoCodeDto,
                            UserContext userContext,
                            @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManagePromoCodes()) throw new UserUnauthorizedException();
        return promoService.createPromoCode(promoCodeDto, userContext);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<PromoCodeDto> getAll(UserContext userContext,
                                     @ModelAttribute UserSecurityHelper userSecurityHelper) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManagePromoCodes()) throw new UserUnauthorizedException();
        return promoService.findPromoCodes();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/valid")
    public PromoCodeDto getValid(@RequestParam String promoCode) {
        return promoService.findPromoCode(promoCode);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/valid")
    public void usePromoCode(@RequestParam String promoCode, UserContext userContext) {
        promoService.usePromoCode(promoCode, userContext.getUserId(), userContext);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{promoId}")
    public void getValid(@PathVariable Long promoId,
                         @ModelAttribute UserSecurityHelper userSecurityHelper,
                         UserContext userContext) throws UserUnauthorizedException {
        if (!userSecurityHelper.canManagePromoCodes()) throw new UserUnauthorizedException();
        promoService.deletePromoCode(promoId, userContext);
    }
}
