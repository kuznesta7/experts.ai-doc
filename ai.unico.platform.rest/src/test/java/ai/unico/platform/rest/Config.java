package ai.unico.platform.rest;

import ai.unico.platform.search.api.dto.ExpertClaimPreviewDto;
import ai.unico.platform.search.api.service.ExpertClaimSearchService;
import ai.unico.platform.store.api.dto.ItemDetailDto;
import ai.unico.platform.store.api.dto.ProjectDto;
import ai.unico.platform.store.api.enums.WidgetType;
import ai.unico.platform.store.api.service.StoreSecurityHelperService;
import ai.unico.platform.util.model.UserContext;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.util.List;

@SpringBootTest
public class Config {

    @Bean
    ExpertClaimSearchService expertClaimSearchService() {
        return new ExpertClaimSearchService() {
            @Override
            public List<ExpertClaimPreviewDto> findExpertsToClaim(Long userId) throws IOException {
                return null;
            }

            @Override
            public List<ExpertClaimPreviewDto> findExpertsToClaim(String query) throws IOException {
                return null;
            }
        };
    }

    @Bean
    StoreSecurityHelperService storeSecurityHelperService() {
        return new StoreSecurityHelperService() {
            @Override
            public Boolean isUserOwnerOfSearchHistoryItem(Long searchHistId, UserContext userContext) {
                return true;
            }

            @Override
            public Boolean canUserSeeItem(Long itemId, UserContext userContext) {
                return true;
            }

            @Override
            public Boolean canUserEditItem(ItemDetailDto itemDetailDto, UserContext userContext) {
                return true;
            }

            @Override
            public Boolean canUserManageItemOrganization(Long itemId, Long organizationId, UserContext userContext) {
                return true;
            }

            @Override
            public Boolean canUserManageItemExperts(Long itemId, Long userId, UserContext userContext) {
                return true;
            }

            @Override
            public Boolean canUserSeeProject(Long projectId, UserContext userContext) {
                return true;
            }

            @Override
            public Boolean canUserEditProject(ProjectDto projectDto, UserContext userContext) {
                return true;
            }

            @Override
            public Boolean canUserEditProject(Long projectId, UserContext userContext) {
                return true;
            }

            @Override
            public Boolean canUserSeeCommercializationProjectDetail(Long commercializationId, UserContext userContext) {
                return true;
            }

            @Override
            public Boolean canUserEditCommercializationProject(Long commercializationId, UserContext userContext) {
                return true;
            }

            @Override
            public Boolean doesUserOwnOriginalItem(Long originalItemId, UserContext userContext) throws IOException {
                return true;
            }

            @Override
            public Boolean hasOrganizationPublicProfile(Long organizationId) {
                return true;
            }

            @Override
            public Boolean hasWidget(Long organizationId, WidgetType widgetType) {
                return true;
            }
        };
    }
}
