package ai.unico.platform.rest.tests;

import ai.unico.platform.rest.Config;
import ai.unico.platform.rest.security.UserSecurityHelperImpl;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.mock.IdGeneratorUtil;
import ai.unico.platform.util.mock.UserContextMockUtil;
import ai.unico.platform.util.model.UserContext;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;

@DataJpaTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = Config.class)
public class EvidenceItemsProjectsAccessTest {

    @Before
    public void setUp() {
    }

    @Test
    public void whenBasicUser_shouldNotBeAccessible() {
        final UserContext userContext = UserContextMockUtil.createBaseUserContext();
        final UserSecurityHelperImpl userSecurityHelper = new UserSecurityHelperImpl(userContext);
        final HashSet<Confidentiality> confidentiality = new HashSet<>();
        confidentiality.add(Confidentiality.PUBLIC);
        final Long organizationId = IdGeneratorUtil.generateId();
        final boolean result = userSecurityHelper.canSeeItemsWithConfidentiality(organizationId, confidentiality);
        Assertions.assertThat(result).isFalse();
    }

    @Test
    public void whenEditorFromAnotherOrganization_shouldNotBeAccessible() {
        final Long uninvolvedOrganizationId = IdGeneratorUtil.generateId();
        final UserContext userContext = UserContextMockUtil.createOrganizationUserContextWithOrganizationRoles(uninvolvedOrganizationId, OrganizationRole.ORGANIZATION_EDITOR, OrganizationRole.ORGANIZATION_VIEWER);
        final UserSecurityHelperImpl userSecurityHelper = new UserSecurityHelperImpl(userContext);
        final HashSet<Confidentiality> confidentiality = new HashSet<>();
        confidentiality.add(Confidentiality.PUBLIC);
        final Long organizationId = IdGeneratorUtil.generateId();
        final boolean result = userSecurityHelper.canSeeItemsWithConfidentiality(organizationId, confidentiality);
        Assertions.assertThat(result).isFalse();
    }

    @Test
    public void whenEditorFromInvolvedOrganization_shouldBeAccessible() {
        final Long organizationId = IdGeneratorUtil.generateId();
        final UserContext userContext = UserContextMockUtil.createOrganizationUserContextWithOrganizationRoles(organizationId, OrganizationRole.ORGANIZATION_EDITOR);
        final UserSecurityHelperImpl userSecurityHelper = new UserSecurityHelperImpl(userContext);
        final HashSet<Confidentiality> confidentiality = new HashSet<>();
        confidentiality.add(Confidentiality.PUBLIC);
        final boolean result = userSecurityHelper.canSeeItemsWithConfidentiality(organizationId, confidentiality);
        Assertions.assertThat(result).isTrue();
    }

    @Test
    public void whenViewerFromInvolvedOrganization_shouldBeAccessible() {
        final Long organizationId = IdGeneratorUtil.generateId();
        final UserContext userContext = UserContextMockUtil.createOrganizationUserContextWithOrganizationRoles(organizationId, OrganizationRole.ORGANIZATION_VIEWER);
        final UserSecurityHelperImpl userSecurityHelper = new UserSecurityHelperImpl(userContext);
        final HashSet<Confidentiality> confidentiality = new HashSet<>();
        confidentiality.add(Confidentiality.PUBLIC);
        final boolean result = userSecurityHelper.canSeeItemsWithConfidentiality(organizationId, confidentiality);
        Assertions.assertThat(result).isTrue();
    }

    @Test
    public void whenConfidentialAndEditorFromInvolvedOrganization_shouldBeAccessible() {
        final Long organizationId = IdGeneratorUtil.generateId();
        final UserContext userContext = UserContextMockUtil.createOrganizationUserContextWithOrganizationRoles(organizationId, OrganizationRole.ORGANIZATION_EDITOR);
        final UserSecurityHelperImpl userSecurityHelper = new UserSecurityHelperImpl(userContext);
        final HashSet<Confidentiality> confidentiality = new HashSet<>();
        confidentiality.add(Confidentiality.CONFIDENTIAL);
        final boolean result = userSecurityHelper.canSeeItemsWithConfidentiality(organizationId, confidentiality);
        Assertions.assertThat(result).isTrue();
    }

    @Test
    public void whenConfidentialAndViewerFromInvolvedOrganization_shouldBeAccessible() {
        final Long organizationId = IdGeneratorUtil.generateId();
        final UserContext userContext = UserContextMockUtil.createOrganizationUserContextWithOrganizationRoles(organizationId, OrganizationRole.ORGANIZATION_VIEWER);
        final UserSecurityHelperImpl userSecurityHelper = new UserSecurityHelperImpl(userContext);
        final HashSet<Confidentiality> confidentiality = new HashSet<>();
        confidentiality.add(Confidentiality.CONFIDENTIAL);
        final boolean result = userSecurityHelper.canSeeItemsWithConfidentiality(organizationId, confidentiality);
        Assertions.assertThat(result).isTrue();
    }

    @Test
    public void whenSecretAndEditorFromInvolvedOrganization_shouldBeAccessible() {
        final Long organizationId = IdGeneratorUtil.generateId();
        final UserContext userContext = UserContextMockUtil.createOrganizationUserContextWithOrganizationRoles(organizationId, OrganizationRole.ORGANIZATION_EDITOR);
        final UserSecurityHelperImpl userSecurityHelper = new UserSecurityHelperImpl(userContext);
        final HashSet<Confidentiality> confidentiality = new HashSet<>();
        confidentiality.add(Confidentiality.SECRET);
        final boolean result = userSecurityHelper.canSeeItemsWithConfidentiality(organizationId, confidentiality);
        Assertions.assertThat(result).isTrue();
    }

    @Test
    public void whenSecretAndViewerFromInvolvedOrganization_shouldNotBeAccessible() {
        final Long organizationId = IdGeneratorUtil.generateId();
        final UserContext userContext = UserContextMockUtil.createOrganizationUserContextWithOrganizationRoles(organizationId, OrganizationRole.ORGANIZATION_VIEWER);
        final UserSecurityHelperImpl userSecurityHelper = new UserSecurityHelperImpl(userContext);
        final HashSet<Confidentiality> confidentiality = new HashSet<>();
        confidentiality.add(Confidentiality.SECRET);
        final boolean result = userSecurityHelper.canSeeItemsWithConfidentiality(organizationId, confidentiality);
        Assertions.assertThat(result).isFalse();
    }
}