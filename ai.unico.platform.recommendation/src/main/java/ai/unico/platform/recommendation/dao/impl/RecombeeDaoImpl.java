package ai.unico.platform.recommendation.dao.impl;

import ai.unico.platform.recommendation.api.dto.RecombeeUploadDto;
import ai.unico.platform.recommendation.api.enums.RecombeeColumn;
import ai.unico.platform.recommendation.dao.RecombeeDao;
import com.recombee.api_client.RecombeeClient;
import com.recombee.api_client.api_requests.*;
import com.recombee.api_client.bindings.Item;
import com.recombee.api_client.bindings.PropertyInfo;
import com.recombee.api_client.bindings.RecommendationResponse;
import com.recombee.api_client.bindings.SearchResponse;
import com.recombee.api_client.exceptions.ApiException;

import java.util.*;

public class RecombeeDaoImpl implements RecombeeDao {

    @Override
    public RecommendationResponse recommendToUser(String user, String scenario, String databaseName, String privateToken, String filter, int limit) {
        final RecombeeClient client = new RecombeeClient(databaseName, privateToken);
        RecommendationResponse result;
        if (user == null)
            user = "test";
        try {
            result = client.send(
                    new RecommendItemsToUser(user, limit)
                            .setScenario(scenario)
                            .setFilter(filter)
                            .setReturnProperties(false)
                            .setCascadeCreate(true)
            );
        } catch (ApiException apiException) {
            throw new RuntimeException("Recombee item recommendation failed.");
        }
        return result;
    }

    @Override
    public RecommendationResponse recommendNextItems(String recommId, String databaseName, String privateToken, int limit) {
        final RecombeeClient client = new RecombeeClient(databaseName, privateToken);
        RecommendationResponse result;
        try {
            result = client.send(new RecommendNextItems(recommId, limit));
        } catch (ApiException apiException) {
            throw new RuntimeException("Recombee item recommendation failed.");
        }
        return result;
    }

    @Override
    public SearchResponse search(String user, String query, String scenario, String databaseName, String privateToken, String filter, int limit) {
        final RecombeeClient client = new RecombeeClient(databaseName, privateToken);
        SearchResponse result;
        if (user == null)
            user = "test";
        try {
            result = client.send(
                    new SearchItems(user, query, limit)
                            .setScenario(scenario)
                            .setFilter(filter)
                            .setReturnProperties(false)
                            .setCascadeCreate(true)
            );
        } catch (ApiException e){
            throw new RuntimeException("Recombee item recommendation failed.");
        }
        return result;
    }

    @Override
    public void upload(RecombeeUploadDto entry, String databaseName, String privateToken) {
        if (databaseName == null || privateToken == null)
            return;
        final RecombeeClient client = new RecombeeClient(databaseName, privateToken);
        setDeleted(entry.getSerializedObject());
        try {
            client.send(new SetItemValues(entry.getId(), entry.getSerializedObject())
                    .setCascadeCreate(true));
        } catch (ApiException e) {
            System.out.println("Failed to upload item with code " + entry.getId());
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void upload(Collection<RecombeeUploadDto> entries, String databaseName, String privateToken) {
        if (databaseName == null || privateToken == null){
            System.out.println("Failed to upload items - database name or private token is null");
            return;
        }
        entries.stream().map(RecombeeUploadDto::getSerializedObject).forEach(this::setDeleted);
        final RecombeeClient client = new RecombeeClient(databaseName, privateToken);
        try {
            client.send(new Batch(entries.stream()
                    .map(this::createSetItemValues)
                    .toArray(SetItemValues[]::new)));
        } catch (ApiException e) {
            System.out.println("Failed to upload items to organization: " + databaseName);
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void delete(String id, String databaseName, String privateToken) {
        if (databaseName == null || privateToken == null)
            return;
        final RecombeeClient client = new RecombeeClient(databaseName, privateToken);
        Map<String, Object> properties = new HashMap<>();
        properties.put("deleted", true);
        try {
            client.send(new SetItemValues(id, properties)
                    .setCascadeCreate(true));
        } catch (ApiException e) {
            System.out.println("Failed to delete item with code " + id);
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void addColumns(Set<RecombeeColumn> columns, String databaseName, String privateToken) {
        if (databaseName == null || privateToken == null)
            return;
        final RecombeeClient client = new RecombeeClient(databaseName, privateToken);
        try {
            client.send(new Batch(columns.stream()
                    .map(x -> new AddItemProperty(x.getColumnName(), x.getType()))
                    .toArray(AddItemProperty[]::new)));

        } catch (ApiException e) {
            System.out.println("Failed to add columns to organization: " + databaseName);
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void deleteColumns(Set<RecombeeColumn> columns, String databaseName, String privateToken) {
        if (databaseName == null || privateToken == null)
            return;
        final RecombeeClient client = new RecombeeClient(databaseName, privateToken);
        try {
            client.send(new Batch(columns.stream()
                    .map(x -> new DeleteItemProperty(x.getColumnName()))
                    .toArray(DeleteItemProperty[]::new)));
        } catch (ApiException e) {
            System.out.println("Failed to delete columns from organization: " + databaseName);
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void resetDatabaseColumns(String databaseName, String privateToken) {
        if (databaseName == null || privateToken == null)
            return;
        final RecombeeClient client = new RecombeeClient(databaseName, privateToken);
        List<Request> requests = new ArrayList<>();
        try {
            PropertyInfo[] properties = client.send(new ListItemProperties());
            Set<String> propertiesStrings = new HashSet<>();
            for (PropertyInfo property : properties) {
                if (RecombeeColumn.fromColumnName(property.getName()) == null) {
                    requests.add(new DeleteItemProperty(property.getName()));
                } else {
                    propertiesStrings.add(property.getName());
                }
            }
            for (RecombeeColumn column : RecombeeColumn.values()) {
                if (!propertiesStrings.contains(column.getColumnName())) {
                    requests.add(new AddItemProperty(column.getColumnName(), column.getType()));
                }
            }
            client.send(new Batch(requests.toArray(new Request[0])));
        } catch (ApiException e) {
            System.out.println("Failed to reset database columns for organization: " + databaseName);
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void resetDatabase(String databaseName, String privateToken) {
        if (databaseName == null || privateToken == null)
            return;
        final RecombeeClient client = new RecombeeClient(databaseName, privateToken);
        try {
            Item[] items = client.send(new ListItems());
            List<SetItemValues> requests = new ArrayList<>();
            Map<String, Object> properties = new HashMap<>();
            properties.put("deleted", true);
            Arrays.stream(items).map(Item::getItemId).forEach(id -> {
                requests.add(new SetItemValues(id, properties).setCascadeCreate(true));
            });
            client.send(new Batch(requests.toArray(new SetItemValues[0])));
        } catch (ApiException e) {
            System.out.println("Failed to reset database for organization: " + databaseName);
            System.out.println(e.getMessage());
        }
    }

    private SetItemValues createSetItemValues(RecombeeUploadDto entry) {
        return new SetItemValues(entry.getId(), entry.getSerializedObject())
                .setCascadeCreate(true);
    }

    private void setDeleted(Map<String, Object> serializedObject) {
        if (! serializedObject.containsKey("deleted"))
            serializedObject.put("deleted", false);
    }

}
