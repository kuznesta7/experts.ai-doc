package ai.unico.platform.recommendation.dao;

import ai.unico.platform.recommendation.api.dto.RecombeeUploadDto;
import ai.unico.platform.recommendation.api.enums.RecombeeColumn;
import com.recombee.api_client.bindings.RecommendationResponse;
import com.recombee.api_client.bindings.SearchResponse;

import java.util.Collection;
import java.util.Set;

public interface RecombeeDao {

    RecommendationResponse recommendToUser(String user, String scenario, String databaseName, String privateToken, String filter, int limit);

    RecommendationResponse recommendNextItems(String recommId, String databaseName, String privateToken, int limit);

    SearchResponse search(String user, String query, String scenario, String databaseName, String privateToken, String filter, int limit);

    void upload(RecombeeUploadDto entry, String databaseName, String privateToken);

    void upload(Collection<RecombeeUploadDto> entries, String databaseName, String privateToken);

    void delete(String id, String databaseName, String privateToken);

    void addColumns(Set<RecombeeColumn> columns, String databaseName, String privateToken);

    void deleteColumns(Set<RecombeeColumn> columns, String databaseName, String privateToken);

    void resetDatabaseColumns(String databaseName, String privateToken);

    void resetDatabase(String databaseName, String privateToken);

}
