package ai.unico.platform.recommendation.service;

import ai.unico.platform.recommendation.api.dto.RecombeeFilterDto;
import ai.unico.platform.recommendation.api.enums.RecombeeColumn;
import ai.unico.platform.recommendation.api.enums.RecombeeScenario;
import ai.unico.platform.recommendation.api.service.RecombeeFilterService;
import ai.unico.platform.recommendation.util.RecombeeFilterUtil;
import ai.unico.platform.store.api.service.ItemTypeService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;

import java.time.Year;
import java.util.*;
import java.util.stream.Collectors;

public class RecombeeFilterServiceImpl implements RecombeeFilterService {

    @Override
    public String setupFilters(RecombeeFilterDto filter, RecombeeScenario scenario) {
        switch (scenario){
            case EXPERT:
                return RecombeeFilterUtil.conjunctionOfFilters(setupExpertFilters(filter));
            case ITEM:
                return RecombeeFilterUtil.conjunctionOfFilters(setupItemFilters(filter));
            case OPPORTUNITY:
                return RecombeeFilterUtil.conjunctionOfFilters(setupOpportunityFilters(filter));
            case PROJECT:
                return RecombeeFilterUtil.conjunctionOfFilters(setupProjectFilters(filter));
        }
        return null;
    }

    @Override
    public List<String> setupExpertFilters(RecombeeFilterDto filter) {
        List<String> filters = new ArrayList<>();
        Set<Long> organizationIds = filter.getOrganizationIds();
        Long organizationId = filter.getOrganizationId();

        filters.add(RecombeeFilterUtil.isTrue(RecombeeColumn.IS_EXPERT, true));
        filters.add(RecombeeFilterUtil.isNullFilter(RecombeeColumn.ORGANIZATION_IDS, false));
        filters.add(RecombeeFilterUtil.isNullFilter(RecombeeColumn.CLAIMED_BY_ID, true));

        if (!organizationIds.isEmpty()) {
            filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.ORGANIZATION_IDS, organizationIds, true));
        }
        final Set<Long> affiliatedOrganizationIds = filter.getAffiliatedOrganizationIds();
        if (!affiliatedOrganizationIds.isEmpty()) {
            filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.ORGANIZATION_IDS, organizationIds, false));
            filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.ORGANIZATION_IDS, affiliatedOrganizationIds, true));
        }

        if (!filter.getSuborganizationIds().isEmpty()) {
            filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.ORGANIZATION_IDS, filter.getSuborganizationIds(), true));
        }

        if (filter.isVerifiedOnly()) {
            if (organizationId != null) {
                filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.VERIFIED_ORGANIZATION_IDS, organizationIds, true));
            } else {
                filters.add(RecombeeFilterUtil.isNullFilter(RecombeeColumn.VERIFIED_ORGANIZATION_IDS, false));
            }
        }
        if (filter.isNotVerifiedOnly()) {
            if (organizationId != null) {
                filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.VERIFIED_ORGANIZATION_IDS, organizationIds, false));
            } else {
                filters.add(RecombeeFilterUtil.isNullFilter(RecombeeColumn.VERIFIED_ORGANIZATION_IDS, true));
            }
        }
        if (filter.isRetired()) {
            filters.add(RecombeeFilterUtil.isNullFilter(RecombeeColumn.RETIREMENT_DATE, false));
            filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.RETIRED_ORGANIZATION_IDS, Collections.singleton(organizationId), true));
        }

        if (!filter.isRetirementIgnored()) {
            filters.add(RecombeeFilterUtil.isNullFilter(RecombeeColumn.RETIREMENT_DATE, true));
            filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.RETIRED_ORGANIZATION_IDS, Collections.singleton(organizationId), false));
        }

        if (filter.getCountryCodes() != null && !filter.getCountryCodes().isEmpty()) {
            filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.COUNTRY_CODES, filter.getCountryCodes().stream().map(Object::toString).collect(Collectors.toList()), true));
        }
        return filters;

    }

    @Override
    public List<String> setupItemFilters(RecombeeFilterDto filter) {
        List<String> filters = new ArrayList<>();
        final Long organizationId = filter.getOrganizationId();
        final Set<Long> organizationIds = filter.getOrganizationIds();
        final Set<Confidentiality> confidentiality = filter.getConfidentiality();
        if (confidentiality.isEmpty()) {
            confidentiality.add(Confidentiality.PUBLIC);
        }
        List<String> confs = confidentiality.stream().map(Object::toString).collect(Collectors.toList());


        filters.add(RecombeeFilterUtil.isTrue(RecombeeColumn.IS_ITEM, true));
        filters.add(RecombeeFilterUtil.isNullFilter(RecombeeColumn.NAME, false));
        filters.add(RecombeeFilterUtil.isInStringListFilter(RecombeeColumn.CONFIDENTIALITY, confs, true));

        if (filter.getResultTypeGroupIds().size() > 0) {
            final ItemTypeService itemTypeService = ServiceRegistryUtil.getService(ItemTypeService.class);
            final Set<Long> itemTypeIds = itemTypeService.findItemTypeIds(filter.getResultTypeGroupIds());
            filters.add(RecombeeFilterUtil.isInIntegerListFilter(RecombeeColumn.TYPE_ID, itemTypeIds, true));
        }


        if (filter.getYearFrom() != null || filter.getYearTo() != null) {
            if (filter.getYearFrom() == null) filter.setYearFrom(1950);
            if (filter.getYearTo() == null) filter.setYearTo(Year.now().getValue());
            filters.add(RecombeeFilterUtil.isInRangeFilter(RecombeeColumn.YEAR,
                    Double.valueOf(filter.getYearFrom()),
                    Double.valueOf(filter.getYearTo()), true));
        }

        if (!organizationIds.isEmpty()) {
            filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.ORGANIZATION_IDS, organizationIds, true));
            List<String> disjunctionFilter = new ArrayList<>();
            disjunctionFilter.add(RecombeeFilterUtil.equalityFilter(RecombeeColumn.CONFIDENTIALITY, Confidentiality.PUBLIC.toString(), true));
            disjunctionFilter.add(RecombeeFilterUtil.equalityFilter(RecombeeColumn.CONFIDENTIALITY, Confidentiality.CONFIDENTIAL.toString(), true));
            if (confidentiality.contains(Confidentiality.SECRET)) {
                disjunctionFilter.add(RecombeeFilterUtil.conjunctionOfFilters(Arrays.asList(
                        RecombeeFilterUtil.equalityFilter(RecombeeColumn.CONFIDENTIALITY, Confidentiality.SECRET.toString(), true),
                        RecombeeFilterUtil.isInIntegerListFilter(RecombeeColumn.OWNER_ORGANIZATION_ID, organizationIds, true)
                )));
            }
            filters.add(RecombeeFilterUtil.disjunctionOfFilters(disjunctionFilter));
        }

        final Set<Long> affiliatedOrganizationIds = filter.getAffiliatedOrganizationIds();
        if (!affiliatedOrganizationIds.isEmpty()) {
            filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.ORGANIZATION_IDS, affiliatedOrganizationIds, false));
            filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.AFFILIATED_ORGANIZATION_IDS, affiliatedOrganizationIds, true));
        }
        final Set<Long> participatingOrganizationIds = filter.getParticipatingOrganizationIds();
        final Set<String> participatingExpertCodes = filter.getExpertCodes();
        if (!participatingOrganizationIds.isEmpty()) {
            filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.ORGANIZATION_IDS, participatingOrganizationIds, true));
        }

        if (!participatingExpertCodes.isEmpty()) {
            filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.EXPERT_CODES, participatingExpertCodes.stream().map(Object::toString).collect(Collectors.toList()), true));
        }

        if (filter.isVerifiedOnly()) {
            if (organizationIds.isEmpty()) {
                filters.add(RecombeeFilterUtil.isNullFilter(RecombeeColumn.VERIFIED_ORGANIZATION_IDS, false));
            } else {
                filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.VERIFIED_ORGANIZATION_IDS, organizationIds, true));
            }
        }
        if (filter.isNotVerifiedOnly()) {
            if (organizationIds.isEmpty()) {
                filters.add(RecombeeFilterUtil.isNullFilter(RecombeeColumn.VERIFIED_ORGANIZATION_IDS, true));
            } else {
                filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.VERIFIED_ORGANIZATION_IDS, organizationIds, false));
            }
        }
        if (filter.isActiveOnly()) {
            filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.RETIRED_ORGANIZATION_IDS, Collections.singleton(organizationId), true));
        }
        return filters;
    }

    @Override
    public List<String> setupProjectFilters(RecombeeFilterDto filter) {
        final List<String> filters = new ArrayList<>();
        final Long organizationId = filter.getOrganizationId();
        final Set<Confidentiality> confidentiality = filter.getConfidentiality();
        final Set<Long> organizationIds = filter.getOrganizationIds();

        if (confidentiality.isEmpty()) confidentiality.add(Confidentiality.PUBLIC);

        filters.add(RecombeeFilterUtil.isTrue(RecombeeColumn.IS_PROJECT, true));
        if (organizationId != null && filter.isRestrictOrganization()) {
            filters.add(RecombeeFilterUtil.valueInListColumn(RecombeeColumn.ORGANIZATION_IDS, organizationId.intValue(), true));
        } else if (organizationIds.size() > 0) {
            filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.ORGANIZATION_IDS, organizationIds, true));
        }
        if (filter.getYearFrom() != null) {
            Calendar c = Calendar.getInstance();
            c.set(filter.getYearFrom(), Calendar.JANUARY, 1, 0, 0);
            filters.add(RecombeeFilterUtil.ge(RecombeeColumn.END_DATE, (double) c.getTimeInMillis() / 1000));
        }
        if (filter.getYearTo() != null) {
            Calendar c = Calendar.getInstance();
            c.set(filter.getYearTo(), Calendar.JANUARY, 1, 0, 0);
            filters.add(RecombeeFilterUtil.le(RecombeeColumn.START_DATE, (double) c.getTimeInMillis() / 1000));
        }
        if (filter.isVerifiedOnly()) {
            if (organizationIds.isEmpty()) {
                filters.add(RecombeeFilterUtil.isNullFilter(RecombeeColumn.VERIFIED_ORGANIZATION_IDS, false));
            } else {
                filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.VERIFIED_ORGANIZATION_IDS, organizationIds, true));
            }
        }
        if (filter.isNotVerifiedOnly()) {
            if (organizationIds.isEmpty()) {
                filters.add(RecombeeFilterUtil.isNullFilter(RecombeeColumn.VERIFIED_ORGANIZATION_IDS, true));
            } else {
                filters.add(RecombeeFilterUtil.intersectionOfSetsFilter(RecombeeColumn.VERIFIED_ORGANIZATION_IDS, organizationIds, false));
            }
        }

        if (!organizationIds.isEmpty()) {
            List<String> disjunctionFilter = new ArrayList<>();
            disjunctionFilter.add(RecombeeFilterUtil.equalityFilter(RecombeeColumn.CONFIDENTIALITY, Confidentiality.PUBLIC.toString(), true));
            disjunctionFilter.add(RecombeeFilterUtil.equalityFilter(RecombeeColumn.CONFIDENTIALITY, Confidentiality.CONFIDENTIAL.toString(), true));
            if (confidentiality.contains(Confidentiality.SECRET)) {
                disjunctionFilter.add(RecombeeFilterUtil.conjunctionOfFilters(Arrays.asList(
                        RecombeeFilterUtil.equalityFilter(RecombeeColumn.CONFIDENTIALITY, Confidentiality.SECRET.toString(), true),
                        RecombeeFilterUtil.isInIntegerListFilter(RecombeeColumn.OWNER_ORGANIZATION_ID, organizationIds.stream().map(Long::intValue).collect(Collectors.toList()), true)
                )));
            }
            filters.add(RecombeeFilterUtil.disjunctionOfFilters(disjunctionFilter));
        }
        return filters;
    }

    @Override
    public List<String> setupOpportunityFilters(RecombeeFilterDto filter) {
        List<String> filters = new ArrayList<>();
        filters.add(RecombeeFilterUtil.isTrue(RecombeeColumn.IS_OPPORTUNITY, true));
        if (filter.getJobType() != null)
            filters.add(RecombeeFilterUtil.valueInListColumn(RecombeeColumn.JOB_TYPES, filter.getJobType().toString(), true));
        if (filter.getOpportunityType() != null)
            filters.add(RecombeeFilterUtil.equalityFilter(RecombeeColumn.TYPE_ID, filter.getOpportunityType().intValue(), true));
        if (!filter.getOrganizationIds().isEmpty()){
            filters.add(RecombeeFilterUtil.isInIntegerListFilter(RecombeeColumn.ORGANIZATION_IDS, filter.getOrganizationIds().stream().map(Long::intValue).collect(Collectors.toList()), true));
        }
        return filters;    }
}


