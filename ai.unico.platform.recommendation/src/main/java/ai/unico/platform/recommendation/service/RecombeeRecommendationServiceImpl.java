package ai.unico.platform.recommendation.service;

import ai.unico.platform.recommendation.api.dto.RecombeeFilterDto;
import ai.unico.platform.recommendation.api.dto.RecommendationWrapperDto;
import ai.unico.platform.recommendation.api.enums.RecombeeScenario;
import ai.unico.platform.recommendation.api.service.RecombeeFilterService;
import ai.unico.platform.recommendation.api.service.RecombeeRecommendationService;
import ai.unico.platform.recommendation.dao.RecombeeDao;
import ai.unico.platform.recommendation.util.RecombeeFilterUtil;
import ai.unico.platform.store.api.dto.OrganizationDetailDto;
import ai.unico.platform.store.api.enums.WidgetType;
import ai.unico.platform.store.api.service.RecommendedItemService;
import ai.unico.platform.util.ServiceRegistryProxy;
import com.recombee.api_client.bindings.RecommendationResponse;
import com.recombee.api_client.bindings.SearchResponse;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;

public class RecombeeRecommendationServiceImpl implements RecombeeRecommendationService {

    private final RecommendedItemService recommendedItemService = ServiceRegistryProxy.createProxy(RecommendedItemService.class);

    @Autowired
    private RecombeeDao recombeeDao;

    @Autowired
    private RecombeeFilterService recombeeFilterService;

        private String[] recommendWithRecombee(String user, String databaseName, String privateToken, String scenario,
                                               String filter, RecommendationWrapperDto recommendationWrapperDto, OrganizationDetailDto organizationDetailDto) {
        RecommendationResponse result = recombeeDao.recommendToUser(user, scenario, databaseName, privateToken, filter, RecombeeRecommendationService.RECOMMENDATION_BATCH_SIZE);
        recommendationWrapperDto.setRecommId(result.getRecommId());
        fillRecommendationWrapperDto(recommendationWrapperDto, organizationDetailDto);
        return result.getIds();
    }

    @Override
    public String[] textSearch(OrganizationDetailDto organizationDetailDto, String recombeeFilter, String scenario, RecommendationWrapperDto recommendationWrapperDto,
                               String user, String query, Integer limit, Integer page) throws RuntimeException {
        SearchResponse result = recombeeDao.search(user, query, scenario, organizationDetailDto.getRecombeeDbIdentifier(), organizationDetailDto.getRecombeePrivateToken(), recombeeFilter, RecombeeRecommendationService.SEARCH_BATCH_SIZE);
        fillRecommendationWrapperDto(recommendationWrapperDto, organizationDetailDto);
        recommendationWrapperDto.setRecommId(result.getRecommId());
        return result.getIds();
    }

    @Override
    public String[] recommend(OrganizationDetailDto organizationDetailDto, RecombeeFilterDto filter, RecombeeScenario scenario, RecommendationWrapperDto recommendationWrapperDto) throws RuntimeException {
        String recombeeFilter = recombeeFilterService.setupFilters(filter, scenario);
        recombeeFilter = RecombeeFilterUtil.addOnlyNonDeletedFilter(recombeeFilter);  // sets deleted=false
        String baseScenario = scenario.getBaseScenario();
        String searchScenario = scenario.getSearchScenario();
        WidgetType widgetType = scenario.getWidgetType();
        String recommId = filter.getRecommId();
        int page = filter.getPage();
        int limit = filter.getLimit();
        String[] recomms;
        if (recommId != null && !recommId.isEmpty() && page > 1) {
            // next item recommendation
            recomms = recommendedItemService.getRecommendations(recommId);
            recomms = splitRecommendation(recomms, page, limit);
            recommendationWrapperDto.setRecommId(filter.getRecommId());
            if (recomms.length == 0) {
                // next item recommendation with Recombee
                recomms = recommendNextItems(recommId, limit, organizationDetailDto, recommendationWrapperDto);
                extendRecommendation(recommId, recomms);
                recomms = recommendedItemService.getRecommendations(recommId);
                recomms = splitRecommendation(recomms, page, limit);
            }
        } else {
            // first recommendation
            if (filter.getQuery() != null && !filter.getQuery().isEmpty()) {  // search
                recomms = textSearch(organizationDetailDto, recombeeFilter, searchScenario, recommendationWrapperDto,
                        filter.getUser(), filter.getQuery(), filter.getLimit(), filter.getPage());
            } else {  // basic recommendation
                recomms = recommendWithRecombee(filter.getUser(), organizationDetailDto.getRecombeeDbIdentifier(),
                        organizationDetailDto.getRecombeePrivateToken(), baseScenario, recombeeFilter,
                        recommendationWrapperDto, organizationDetailDto);
            }
            persistRecommendation(recommendationWrapperDto.getRecommId(), recomms, widgetType, filter.getUser());
            recomms = splitRecommendation(recomms, page, limit);
        }
        return removePrefix(scenario.getPrefix(), recomms);
    }

    @Override
    public String[] recommendNextItems(String recommId, Integer limit, OrganizationDetailDto organizationDetailDto, RecommendationWrapperDto recommendationWrapper) {
        RecommendationResponse result = recombeeDao.recommendNextItems(recommId, organizationDetailDto.getRecombeeDbIdentifier(), organizationDetailDto.getRecombeePrivateToken(), RecombeeRecommendationService.RECOMMENDATION_BATCH_SIZE);
        recommendationWrapper.setRecommId(result.getRecommId());
        fillRecommendationWrapperDto(recommendationWrapper, organizationDetailDto);
        return result.getIds();
    }


    private String[] splitRecommendation(String[] recomms, int page, int limit) {
        int lo = (page - 1) * limit;
        int hi = page * limit;
        if (recomms.length < lo)
            return ArrayUtils.toArray();
        if (recomms.length < hi)
            return Arrays.copyOfRange(recomms, lo, recomms.length);
        return Arrays.copyOfRange(recomms, lo, hi);
    }

    private void persistRecommendation(String recommId, String[] recomms, WidgetType widgetType, String user) {
        recommendedItemService.saveRecommendation(recommId, Arrays.stream(recomms).reduce("", (partial, element) -> partial + "," + element).substring(1),
                widgetType, user, recomms.length);
    }

    private void extendRecommendation(String recommId, String[] recomms){
        recommendedItemService.extendRecommendation(recommId, recomms);
    }

    @Override
    public void fillRecommendationWrapperDto(RecommendationWrapperDto recommendationWrapperDto, OrganizationDetailDto organizationDetailDto){
        recommendationWrapperDto.setPublicKey(organizationDetailDto.getRecombeePublicToken());
        recommendationWrapperDto.setDatabaseName(organizationDetailDto.getRecombeeDbIdentifier());
    }

    @Override
    public int numberOfRecomms(String recommId) {
        return recommendedItemService.getNumberOfRecomms(recommId);
    }

    private String[] removePrefix(String prefix, String[] recomms) {
        for (int i = 0; i < recomms.length; i++) {
            recomms[i] = recomms[i].replace(prefix, "");
        }
        return recomms;
    }

}
