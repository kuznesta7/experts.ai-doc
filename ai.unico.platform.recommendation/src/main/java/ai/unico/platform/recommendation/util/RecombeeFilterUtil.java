package ai.unico.platform.recommendation.util;

import ai.unico.platform.recommendation.api.dto.RecommendationFilterDto;
import ai.unico.platform.recommendation.api.enums.RecombeeColumn;
import ai.unico.platform.search.api.filter.RecommendationFilter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class RecombeeFilterUtil {

    public static RecommendationFilterDto convert(RecommendationFilter filter) {
        RecommendationFilterDto dto = new RecommendationFilterDto();
        dto.setLimit(filter.getLimit());
        dto.setPage(filter.getPage());
        dto.setQuery(filter.getQuery());
        dto.setRecommId(filter.getRecommId());
        dto.setUser(filter.getUser());
        return dto;
    }

    public static <T> List<T> sortByRecombee(List<T> toSort, List<String> recombeeIds, List<String> elasticIds)  {
        List<T> result = new ArrayList<>(Collections.nCopies(recombeeIds.size(), null));
        List<Integer> sortList = new ArrayList<>();
        for (String item: elasticIds){
            sortList.add(recombeeIds.indexOf(item));
        }
        int i = 0;
        for (T obj: toSort){
            if (i == sortList.size())
                break;
            result.set(sortList.get(i++), obj);
        }
        result.removeAll(Collections.singleton(null));
        return result;
    }

    public static String addOnlyNonDeletedFilter(String filter) {
        if (filter == null || filter.isEmpty())
            return isTrue(RecombeeColumn.DELETED, false);
        return filter + " and " + isTrue(RecombeeColumn.DELETED, false);
    }

    public static String isTrue(RecombeeColumn column, boolean positiveCondition){
        return "'" + column.getColumnName() + "' == " + positiveCondition;
    }

    public static String equalityFilter(RecombeeColumn column, String value, boolean positiveCondition) {
        if (positiveCondition)
            return "'" + column.getColumnName() + "' == \"" + value + "\"";
        return "'" + column.getColumnName() + "' != \"" + value + "\"";
    }

    public static String equalityFilter(RecombeeColumn column, Integer value, boolean positiveCondition){
        if (positiveCondition)
            return "'" + column.getColumnName() + "' == " + value + "";
        return "'" + column.getColumnName() + "' != " + value + "";
    }

    public static String equalityToNullFilter(RecombeeColumn column, boolean positiveCondition){
        if (positiveCondition)
            return "null == '" + column.getColumnName() + "'";
        return "null != '" + column.getColumnName() + "'";
    }

    public static String booleanColumnFilter(RecombeeColumn column, boolean positiveCondition){
        if (positiveCondition)
            return "'" + column.getColumnName() + "'";
        return "not '" + column.getColumnName() + "'";
    }

    public static String endsWithStringFilter(RecombeeColumn column, String value, boolean positiveCondition){
        if(positiveCondition)
            return "'" + column.getColumnName() + "' ~ \".*" + value + "\"";
        return "not ('" + column.getColumnName() + "' ~ \".*" + value + "\")";
    }

    public static String isNullFilter(RecombeeColumn column, boolean positiveCondition){
        if (positiveCondition)
            return "'" + column.getColumnName() + "' == null";
        return "'" + column.getColumnName() + "' != null";
    }

    public static String isInStringListFilter(RecombeeColumn column, Collection<?> list, boolean positiveCondition){
        StringBuilder mergedFilter = new StringBuilder();
        mergedFilter.append("[");
        list.forEach(item -> mergedFilter.append("\"").append(item).append("\","));
        String stringList = mergedFilter.substring(0, mergedFilter.length() - 1) + "]";
        if (positiveCondition)
            return "'" + column.getColumnName() + "' in " + stringList;
        return "'" + column.getColumnName() + "' not in " + stringList;
    }

    public static String isInIntegerListFilter(RecombeeColumn column, Collection<? extends Number> list, boolean positiveCondition){
        StringBuilder mergedFilter = new StringBuilder();
        mergedFilter.append("[");
        for (Number item : list) {
            mergedFilter.append(item).append(",");
        }
        String stringList = mergedFilter.substring(0, mergedFilter.length() - 1) + "]";
        if (positiveCondition)
            return "'" + column.getColumnName() + "' in " + stringList;
        return "'" + column.getColumnName() + "' not in " + stringList;
    }

    public static String valueInListColumn(RecombeeColumn column, Integer value, boolean positiveCondition){
        if(positiveCondition)
            return "\"" + value + "\" in '" +  column + "'";
        return "\"" + value + "\" not in '" +  column + "'";
    }

    public static String valueInListColumn(RecombeeColumn column, String value, boolean positiveCondition){
        if(positiveCondition)
            return "\"" + value + "\" in '" +  column + "'";
        return "\"" + value + "\" not in '" +  column + "'";
    }

    public static String ge(RecombeeColumn column, Double value) {
        return value + " <= '" + column.getColumnName() + "'";
    }

    public static String gt(RecombeeColumn column, Double value) {
        return value + " < '" + column.getColumnName() + "'";
    }

    public static String le(RecombeeColumn column, Double value) {
        return value + " >= '" + column.getColumnName() + "'";
    }

    public static String lt(RecombeeColumn column, Double value) {
        return value + " > '" + column.getColumnName() + "'";
    }


    public static String isInRangeFilter(RecombeeColumn column, Double from, Double to, boolean positiveCondition){
        if(positiveCondition)
            return from + " <= '"  + column.getColumnName() + "' <= " + to;
        return "'" + column.getColumnName() + "' != null and not (" + from + " <= '"  + column.getColumnName() + "' <= " + to + ")";
    }

    public static String unionOfSetsFilter(RecombeeColumn column, Collection<?> list, boolean positiveCondition){
        StringBuilder mergedFilter = new StringBuilder();
        mergedFilter.append("{");
        list.forEach(item -> mergedFilter.append("\"").append(item).append("\","));
        String stringList = mergedFilter.substring(0, mergedFilter.length() - 1) + "}";
        if (positiveCondition)
            return "'" + column.getColumnName() + "' + " + stringList + " > {}";
        return "'" + column.getColumnName() + "' + " + stringList + " == {}";
    }

    public static String intersectionOfSetsFilter(RecombeeColumn column, Collection<?> list, boolean positiveCondition){
        StringBuilder mergedFilter = new StringBuilder();
        mergedFilter.append("{");
        list.forEach(item -> mergedFilter.append("\"").append(item).append("\","));
        String stringList = mergedFilter.substring(0, mergedFilter.length() - 1) + "}";
        if (positiveCondition)
            return "'" + column.getColumnName() + "' & " + stringList + " > {}";
        return "'" + column.getColumnName() + "' & " + stringList + " == {}";
    }

    public static String conjunctionOfFilters(Collection<?> filters){
        if (filters.isEmpty())
            return "";
        StringBuilder mergedFilter = new StringBuilder();
        mergedFilter.append("(");
        filters.forEach(filter -> mergedFilter.append(filter).append(" and "));
        return mergedFilter.substring(0, mergedFilter.length() - 5) + ")";
    }

    public static String disjunctionOfFilters(Collection<?> filters){
        if (filters.isEmpty())
            return "";
        StringBuilder mergedFilter = new StringBuilder();
        mergedFilter.append("(");
        filters.forEach(filter -> mergedFilter.append(filter).append(" or "));
        return mergedFilter.substring(0, mergedFilter.length() - 4) + ")";
    }
}
