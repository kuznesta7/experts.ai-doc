package ai.unico.platform.recommendation.service;

import ai.unico.platform.recommendation.api.dto.RecombeeReindexOptionDto;
import ai.unico.platform.recommendation.api.dto.RecombeeUploadDto;
import ai.unico.platform.recommendation.api.enums.RecombeeColumn;
import ai.unico.platform.recommendation.api.enums.RecombeeScenario;
import ai.unico.platform.recommendation.api.interfaces.RecombeeSearchEntity;
import ai.unico.platform.recommendation.api.service.RecombeeManipulationService;
import ai.unico.platform.recommendation.dao.RecombeeDao;
import ai.unico.platform.search.api.service.RecommendationIndexService;
import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.dto.OrganizationDetailDto;
import ai.unico.platform.store.api.dto.RecombeeConnectionDto;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.api.service.OrganizationService;
import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.dto.OrganizationDto;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class RecombeeManipulationServiceImpl implements RecombeeManipulationService {

    @Autowired
    private RecombeeDao recombeeDao;

    private final OrganizationService organizationService = ServiceRegistryProxy.createProxy(OrganizationService.class);
    private final OrganizationStructureService organizationStructureService = ServiceRegistryProxy.createProxy(OrganizationStructureService.class);
    private final RecommendationIndexService recommendationIndexService = ServiceRegistryProxy.createProxy(RecommendationIndexService.class);

    @Override
    public <T extends RecombeeSearchEntity> void uploadEntry(T item, String prefix) {
        System.out.println("Uploading item with code " + item.getId());
        Set<Long> organizationIds = item.getOrganizationIds();
        List<RecombeeConnectionDto> recombeeConnectionDtos = organizationService.getRecombeeConnections(organizationIds);
        List<String> organizationNames = organizationService.findOrganizationsByIds(organizationIds).stream()
                .map(OrganizationDto::getOrganizationName)
                .collect(Collectors.toList());

        Map<String, Object> serializedObject = item.serialize();
        // improves the recommendation because of an additional organization data
        if (! serializedObject.containsKey(RecombeeColumn.ORGANIZATION_NAMES.getColumnName()))
            serializedObject.put(RecombeeColumn.ORGANIZATION_NAMES.getColumnName(), organizationNames);
        for (RecombeeConnectionDto conn: recombeeConnectionDtos) {
            if (conn == null || conn.getPrivateToken() == null || conn.getDatabaseName() == null)
                continue;
            System.out.println("Uploading item with code " + item.getId() + " for organization " + conn.getDatabaseName());
            RecombeeUploadDto entry = new RecombeeUploadDto(item.getId(), serializedObject);
            addPrefix(prefix, entry);
            recombeeDao.upload(entry, conn.getDatabaseName(), conn.getPrivateToken());
        }
    }

    @Override
    public <T extends RecombeeSearchEntity> void uploadEntries(Collection<T> items, Long OrganizationId, String prefix) {
        RecombeeConnectionDto conn = organizationService.getRecombeeConnection(OrganizationId);
        if (conn == null || conn.getPrivateToken() == null || conn.getDatabaseName() == null)
            return;
        Set<Long> organizationIds = items.stream().map(RecombeeSearchEntity::getOrganizationIds).flatMap(Set::stream).collect(Collectors.toSet());
        Map<Long, String> namesMapper = organizationService.findOrganizationsByIds(organizationIds).stream()
                .filter(x -> x != null && x.getOrganizationId() != null && x.getOrganizationName() != null)
                .collect(Collectors.toMap(OrganizationDto::getOrganizationId, OrganizationDto::getOrganizationName));

        List<RecombeeUploadDto> recombeeUploadDtos = new ArrayList<>();
        for (T item: items) {
            List<String> organizationNames = new ArrayList<>();
            for (Long orgId: item.getOrganizationIds()) {
                String orgName = namesMapper.get(orgId);
                if (orgName != null) {
                    organizationNames.add(orgName);
                }
            }
            Map<String, Object> serializedObject = item.serialize();
            serializedObject.put(RecombeeColumn.ORGANIZATION_NAMES.getColumnName(), organizationNames);
            RecombeeUploadDto entry = new RecombeeUploadDto(item.getId(), serializedObject);
            addPrefix(prefix, entry);
            recombeeUploadDtos.add(entry);
        }
        recombeeDao.upload(recombeeUploadDtos, conn.getDatabaseName(), conn.getPrivateToken());
    }

    @Override
    public void deleteEntry(String id, Long organizationId, String prefix) {
        RecombeeConnectionDto conn = organizationService.getRecombeeConnection(organizationId);
        if (conn == null || conn.getPrivateToken() == null || conn.getDatabaseName() == null)
            return;
        recombeeDao.delete(addPrefix(prefix, id), conn.getDatabaseName(), conn.getPrivateToken());
    }

    @Override
    public boolean reindexTargetInOrganization(Long organizationId, RecombeeScenario scenario) throws IOException {
        IndexTarget indexTarget = scenario.getTarget();
        RecombeeConnectionDto conn = organizationService.getRecombeeConnection(organizationId);
        if (conn == null || conn.getPrivateToken() == null || conn.getDatabaseName() == null)
            return false;
        Set<Long> organizationIds = organizationStructureService.findChildOrganizationIds(organizationId, true);
        organizationIds.add(organizationId);
        for (int from = 0; true; from += MAX_BATCH_SIZE) {
            Set<RecombeeSearchEntity> entries = recommendationIndexService.findEntriesForOrganizations(organizationIds, indexTarget, MAX_BATCH_SIZE, from);
            if (entries.isEmpty()) break;
            System.out.println("Uploading " + indexTarget + "  from " + from + " to " + (from + MAX_BATCH_SIZE));
            uploadEntries(entries, organizationId, scenario.getPrefix());
        }
        System.out.println("Upload " + indexTarget + " finished");
        return true;
    }

    @Override
    public List<RecombeeReindexOptionDto> findRecombeeReindexOptions() {
        List<RecombeeReindexOptionDto> reindexOptions = new ArrayList<>();
        List<OrganizationDetailDto> organizations = this.organizationService.getAllRecombeeConnections();
        organizations.forEach(organization -> reindexOptions.add(convert(organization)));
        return reindexOptions;
    }

    @Override
    public void resetColumns(Long organizationId) {
        System.out.println("Resetting columns for organization " + organizationId);
        RecombeeConnectionDto conn = organizationService.getRecombeeConnection(organizationId);
        if (conn == null || conn.getPrivateToken() == null || conn.getDatabaseName() == null)
            return;
        recombeeDao.resetDatabaseColumns(conn.getDatabaseName(), conn.getPrivateToken());
    }

    @Override
    public void resetDatabase(Long organizationId) {
        System.out.println("Resetting database for organization " + organizationId);
        RecombeeConnectionDto conn = organizationService.getRecombeeConnection(organizationId);
        if (conn == null || conn.getPrivateToken() == null || conn.getDatabaseName() == null)
            return;
        recombeeDao.resetDatabase(conn.getDatabaseName(), conn.getPrivateToken());
    }

    private void addPrefix(String prefix, RecombeeUploadDto item) {
        item.setId(prefix + item.getId());
    }

    private Collection<RecombeeUploadDto> addPrefix(String prefix, Collection<RecombeeUploadDto> items) {
        for (RecombeeUploadDto item : items) {
            item.setId(prefix + item.getId());
        }
        return items;
    }

    private String addPrefix(String prefix, String id) {
        return prefix + id;
    }

    private RecombeeReindexOptionDto convert(OrganizationDetailDto organization) {
        final RecombeeReindexOptionDto option = new RecombeeReindexOptionDto();
        final OrganizationBaseDto organizationBaseDto = new OrganizationBaseDto();
        organizationBaseDto.setOrganizationId(organization.getOrganizationId());
        organizationBaseDto.setOrganizationName(organization.getOrganizationName());
        organizationBaseDto.setOrganizationAbbrev(organization.getOrganizationAbbrev());
        option.setRecombeeIdentifier(organization.getRecombeeDbIdentifier());
        option.setOrganization(organizationBaseDto);
        option.setTargets(new ArrayList<>(TARGETS));
        option.setAvailableTargets(new ArrayList<>(TARGETS));
        return option;
    }
}
