package ai.unico.platform.search.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;
import java.util.stream.Collectors;

public class InvestmentProjectIndexDto {

    private String id;

    private String name;

    private Integer ideaScore;

    private Integer technologyReadinesLevel;

    private String executiveSummary;

    private String useCaseDescription;

    private String painDescription;

    private String competetiveAdvantageDescription;

    private String technicalPrinciplesDescription;

    @JsonIgnore
    private Long intellectualPropertyOwnerId;

    @JsonIgnore
    private Boolean search;

    @JsonIgnore
    private Boolean commercialization;

    @JsonIgnore
    private String statusId;

    @JsonIgnore
    private String categoryName;

    @JsonIgnore
    private byte[] image;

    @JsonIgnore
    private Set<String> expertCodes = new HashSet<>();

    @JsonIgnore
    private List<String> domainNames = new ArrayList<>();

    @JsonIgnore
    private String investmentRangeName;

    @JsonIgnore
    private String statusName;

    @JsonProperty("profileImage")
    private void unpackProfileImage(Map<String, String> profileImage) {
        if (profileImage == null) return;
        final String data = profileImage.get("data");
        if (data == null) return;
        final String replacedData = data
                .replaceAll("\\n", "")
                .replaceAll("\\r", "")
                .replaceAll("\\\\", "");
        image = Base64.getDecoder().decode(replacedData);
    }

    @JsonProperty("useCase")
    private void unpackUseCase(Map<String, Boolean> useCase) {
        if (useCase == null) return;
        search = useCase.get("search");
        commercialization = useCase.get("commercialisation");
    }

    @JsonProperty("intelectualPropertyOwner")
    private void unpackIntellectualPropertyOwner(Map<String, Object> intellectualPropertyOwner) {
        if (intellectualPropertyOwner == null) return;
        intellectualPropertyOwnerId = new Long((Integer) intellectualPropertyOwner.get("id"));
    }

    @JsonProperty("teamMembers")
    private void unpackTeamMembers(List<Map<String, String>> teamMembers) {
        if (teamMembers == null) return;
        final Set<String> ids = teamMembers.stream().map(x -> x.get("id")).collect(Collectors.toSet());
        expertCodes.addAll(ids);
    }

    @JsonProperty("domains")
    private void unpackDomains(List<Map<String, String>> domains) {
        if (domains == null) return;
        domainNames.addAll(domains.stream().map(x -> x.get("name")).collect(Collectors.toList()));
    }

    @JsonProperty("investmentRange")
    private void unpackInvestmentRange(Map<String, String> range) {
        if (range == null) return;
        investmentRangeName = range.get("name");
    }

    @JsonProperty("status")
    private void unpackStatus(Map<String, String> status) {
        if (status == null) return;
        statusName = status.get("name");
        statusId = status.get("id");
    }

    @JsonProperty("category")
    private void unpackCategory(Map<String, String> category) {
        if (category == null) return;
        categoryName = category.get("name");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdeaScore() {
        return ideaScore;
    }

    public void setIdeaScore(Integer ideaScore) {
        this.ideaScore = ideaScore;
    }

    public Integer getTechnologyReadinesLevel() {
        return technologyReadinesLevel;
    }

    public void setTechnologyReadinesLevel(Integer technologyReadinesLevel) {
        this.technologyReadinesLevel = technologyReadinesLevel;
    }

    public String getExecutiveSummary() {
        return executiveSummary;
    }

    public void setExecutiveSummary(String executiveSummary) {
        this.executiveSummary = executiveSummary;
    }

    public String getUseCaseDescription() {
        return useCaseDescription;
    }

    public void setUseCaseDescription(String useCaseDescription) {
        this.useCaseDescription = useCaseDescription;
    }

    public String getPainDescription() {
        return painDescription;
    }

    public void setPainDescription(String painDescription) {
        this.painDescription = painDescription;
    }

    public String getCompetetiveAdvantageDescription() {
        return competetiveAdvantageDescription;
    }

    public void setCompetetiveAdvantageDescription(String competetiveAdvantageDescription) {
        this.competetiveAdvantageDescription = competetiveAdvantageDescription;
    }

    public String getTechnicalPrinciplesDescription() {
        return technicalPrinciplesDescription;
    }

    public void setTechnicalPrinciplesDescription(String technicalPrinciplesDescription) {
        this.technicalPrinciplesDescription = technicalPrinciplesDescription;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Set<String> getExpertCodes() {
        return expertCodes;
    }

    public void setExpertCodes(Set<String> expertCodes) {
        this.expertCodes = expertCodes;
    }

    public List<String> getDomainNames() {
        return domainNames;
    }

    public void setDomainNames(List<String> domainNames) {
        this.domainNames = domainNames;
    }

    public String getInvestmentRangeName() {
        return investmentRangeName;
    }

    public void setInvestmentRangeName(String investmentRangeName) {
        this.investmentRangeName = investmentRangeName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Long getIntellectualPropertyOwnerId() {
        return intellectualPropertyOwnerId;
    }

    public void setIntellectualPropertyOwnerId(Long intellectualPropertyOwnerId) {
        this.intellectualPropertyOwnerId = intellectualPropertyOwnerId;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Boolean getSearch() {
        return search;
    }

    public void setSearch(Boolean search) {
        this.search = search;
    }

    public Boolean getCommercialization() {
        return commercialization;
    }

    public void setCommercialization(Boolean commercialization) {
        this.commercialization = commercialization;
    }
}

