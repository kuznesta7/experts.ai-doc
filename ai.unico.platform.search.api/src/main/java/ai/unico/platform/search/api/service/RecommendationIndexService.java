package ai.unico.platform.search.api.service;

import ai.unico.platform.recommendation.api.interfaces.RecombeeSearchEntity;
import ai.unico.platform.store.api.enums.IndexTarget;

import java.io.IOException;
import java.util.Set;

public interface RecommendationIndexService {

    Set<RecombeeSearchEntity> findEntriesForOrganizations(Set<Long> organizationIds, IndexTarget indexName, int limit, int from) throws IOException;

}
