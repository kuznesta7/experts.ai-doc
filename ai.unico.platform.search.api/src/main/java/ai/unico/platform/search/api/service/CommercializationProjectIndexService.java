package ai.unico.platform.search.api.service;

import ai.unico.platform.store.api.dto.CommercializationProjectIndexDto;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.Map;

public interface CommercializationProjectIndexService {

    String indexName = "commercialization-project";

    void reindexInvestmentProjects(Integer limit, UserContext userContext) throws IOException;

    void indexCommercializationProject(CommercializationProjectIndexDto commercializationProjectIndexDto) throws IOException;

    void replaceExperts(Map<String, String> replacementMap, boolean originalOnly) throws IOException;
}
