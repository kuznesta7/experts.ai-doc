package ai.unico.platform.search.api.dto;

import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectStatisticsDto {

    private Long numberOfAllProjects;

    private Double allProjectsFinancesSum;

    private Map<Integer, Double> yearProjectFinancesSumHistogram = new LinkedHashMap<>();

    public Long getNumberOfAllProjects() {
        return numberOfAllProjects;
    }

    public void setNumberOfAllProjects(Long numberOfAllProjects) {
        this.numberOfAllProjects = numberOfAllProjects;
    }

    public Double getAllProjectsFinancesSum() {
        return allProjectsFinancesSum;
    }

    public void setAllProjectsFinancesSum(Double allProjectsFinancesSum) {
        this.allProjectsFinancesSum = allProjectsFinancesSum;
    }

    public Map<Integer, Double> getYearProjectFinancesSumHistogram() {
        return yearProjectFinancesSumHistogram;
    }

    public void setYearProjectFinancesSumHistogram(Map<Integer, Double> yearProjectFinancesSumHistogram) {
        this.yearProjectFinancesSumHistogram = yearProjectFinancesSumHistogram;
    }

    public void transformToCumulativeSum() {
        Map<Integer, Double> cumsum = new LinkedHashMap<>();

        Double sum = 0.0;
        for(Map.Entry<Integer, Double> entry : this.yearProjectFinancesSumHistogram.entrySet()){
            sum += entry.getValue();
            cumsum.put(entry.getKey(), sum);
        }
        this.yearProjectFinancesSumHistogram = cumsum;
    }
}
