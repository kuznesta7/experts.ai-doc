package ai.unico.platform.search.api.dto;

import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.dto.UserExpertBaseDto;

import java.util.ArrayList;
import java.util.List;

public class EquipmentPreviewDto {

    private Long id;
    private String equipmentCode;
    private String languageCode;
    private String name;
    private String marking;
    private String description;
    private List<String> specialization = new ArrayList<>();
    private Integer year;
    private Integer serviceLife;
    private Boolean portableDevice;
    private Long domainId;
    private Long type;
    private List<OrganizationBaseDto> organizations = new ArrayList<>();
    private List<UserExpertBaseDto> expertPreviews = new ArrayList<>();
    private Boolean hidden;
    private String goodForDescription;
    private String targetGroup;
    private String preparationTime;
    private String equipmentCost;
    private String infrastructureDescription;
    private String durationTime;
    private String trl;

    public Long getEquipmentId() {
        return id;
    }

    public void setEquipmentId(Long equipmentId) {
        this.id = equipmentId;
    }

    public String getEquipmentCode() {
        return equipmentCode;
    }

    public void setEquipmentCode(String equipmentCode) {
        this.equipmentCode = equipmentCode;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMarking() {
        return marking;
    }

    public void setMarking(String marking) {
        this.marking = marking;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getSpecialization() {
        return specialization;
    }

    public void setSpecialization(List<String> specialization) {
        this.specialization = specialization;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getServiceLife() {
        return serviceLife;
    }

    public void setServiceLife(Integer serviceLife) {
        this.serviceLife = serviceLife;
    }

    public Boolean getPortableDevice() {
        return portableDevice;
    }

    public void setPortableDevice(Boolean portableDevice) {
        this.portableDevice = portableDevice;
    }

    public Long getDomainId() {
        return domainId;
    }

    public void setDomainId(Long domainId) {
        this.domainId = domainId;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public List<OrganizationBaseDto> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(List<OrganizationBaseDto> organizations) {
        this.organizations = organizations;
    }

    public List<UserExpertBaseDto> getExpertPreviews() {
        return expertPreviews;
    }

    public void setExpertPreviews(List<UserExpertBaseDto> expertPreviews) {
        this.expertPreviews = expertPreviews;
    }

    public Boolean isHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public String getGoodForDescription() {
        return goodForDescription;
    }

    public void setGoodForDescription(String goodForDescription) {
        this.goodForDescription = goodForDescription;
    }

    public String getTargetGroup() {
        return targetGroup;
    }

    public void setTargetGroup(String targetGroup) {
        this.targetGroup = targetGroup;
    }

    public String getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(String preparationTime) {
        this.preparationTime = preparationTime;
    }

    public String getEquipmentCost() {
        return equipmentCost;
    }

    public void setEquipmentCost(String equipmentCost) {
        this.equipmentCost = equipmentCost;
    }

    public String getInfrastructureDescription() {
        return infrastructureDescription;
    }

    public void setInfrastructureDescription(String infrastructureDescription) {
        this.infrastructureDescription = infrastructureDescription;
    }

    public String getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(String durationTime) {
        this.durationTime = durationTime;
    }

    public String getTrl() {
        return trl;
    }

    public void setTrl(String trl) {
        this.trl = trl;
    }
}
