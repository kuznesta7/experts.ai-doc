package ai.unico.platform.search.api.dto;

import ai.unico.platform.store.api.dto.CommercializationProjectDto;
import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.enums.CommercializationOrganizationRelation;

import java.util.ArrayList;
import java.util.List;

public class CommercializationProjectDetailDto extends CommercializationProjectDto {

    private String imgCheckSum;

    private String commercializationPriorityName;

    private String commercializationDomainName;

    private Long commercializationStatusId;

    private String commercializationStatusName;

    private List<OrganizationWithRelationBaseDto> projectOrganizations = new ArrayList<>();

    private List<CommercializationTeamMemberDto> teamMemberDtos = new ArrayList<>();

    private OrganizationBaseDto ownerOrganization;

    public String getCommercializationPriorityName() {
        return commercializationPriorityName;
    }

    public void setCommercializationPriorityName(String commercializationPriorityName) {
        this.commercializationPriorityName = commercializationPriorityName;
    }

    public String getCommercializationDomainName() {
        return commercializationDomainName;
    }

    public void setCommercializationDomainName(String commercializationDomainName) {
        this.commercializationDomainName = commercializationDomainName;
    }

    public Long getCommercializationStatusId() {
        return commercializationStatusId;
    }

    public void setCommercializationStatusId(Long commercializationStatusId) {
        this.commercializationStatusId = commercializationStatusId;
    }

    public String getCommercializationStatusName() {
        return commercializationStatusName;
    }

    public void setCommercializationStatusName(String commercializationStatusName) {
        this.commercializationStatusName = commercializationStatusName;
    }

    public List<CommercializationTeamMemberDto> getTeamMemberDtos() {
        return teamMemberDtos;
    }

    public void setTeamMemberDtos(List<CommercializationTeamMemberDto> teamMemberDtos) {
        this.teamMemberDtos = teamMemberDtos;
    }

    public String getImgCheckSum() {
        return imgCheckSum;
    }

    public void setImgCheckSum(String imgCheckSum) {
        this.imgCheckSum = imgCheckSum;
    }

    public OrganizationBaseDto getOwnerOrganization() {
        return ownerOrganization;
    }

    public void setOwnerOrganization(OrganizationBaseDto ownerOrganization) {
        this.ownerOrganization = ownerOrganization;
    }

    public List<OrganizationWithRelationBaseDto> getProjectOrganizations() {
        return projectOrganizations;
    }

    public void setProjectOrganizations(List<OrganizationWithRelationBaseDto> projectOrganizations) {
        this.projectOrganizations = projectOrganizations;
    }

    public static class OrganizationWithRelationBaseDto extends OrganizationBaseDto {

        private CommercializationOrganizationRelation relation;

        public OrganizationWithRelationBaseDto(OrganizationBaseDto organizationBaseDto, CommercializationOrganizationRelation relation) {
            setOrganizationId(organizationBaseDto.getOrganizationId());
            setOrganizationName(organizationBaseDto.getOrganizationName());
            setOrganizationAbbrev(organizationBaseDto.getOrganizationAbbrev());
            setGdpr(organizationBaseDto.getGdpr());
            this.relation = relation;
        }

        public CommercializationOrganizationRelation getRelation() {
            return relation;
        }

        public void setRelation(CommercializationOrganizationRelation relation) {
            this.relation = relation;
        }
    }
}
