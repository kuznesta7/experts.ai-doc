package ai.unico.platform.search.api.filter;

import java.util.HashSet;
import java.util.Set;

public class AdminOrganizationFilter {

    private String query;

    private Integer limit = 30;

    private Integer page = 1;

    private boolean exactMatch = true;

    private boolean invalidLicencesOnly = false;

    private boolean activeLicencesOnly = false;

    private boolean onlyWithoutCountry = false;

    private ReplacementState replacementState;

    private Set<String> countryCodes = new HashSet<>();

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public void setExactMatch(boolean exactMatch) {
        this.exactMatch = exactMatch;
    }

    public boolean isExactMatch() {
        return exactMatch;
    }

    public boolean isInvalidLicencesOnly() {
        return invalidLicencesOnly;
    }

    public void setInvalidLicencesOnly(boolean invalidLicencesOnly) {
        this.invalidLicencesOnly = invalidLicencesOnly;
    }

    public boolean isActiveLicencesOnly() {
        return activeLicencesOnly;
    }

    public void setActiveLicencesOnly(boolean activeLicencesOnly) {
        this.activeLicencesOnly = activeLicencesOnly;
    }

    public ReplacementState getReplacementState() {
        return replacementState;
    }

    public void setReplacementState(ReplacementState replacementState) {
        this.replacementState = replacementState;
    }

    public boolean isOnlyWithoutCountry() {
        return onlyWithoutCountry;
    }

    public void setOnlyWithoutCountry(boolean onlyWithoutCountry) {
        this.onlyWithoutCountry = onlyWithoutCountry;
    }

    public Set<String> getCountryCodes() {
        return countryCodes;
    }

    public void setCountryCodes(Set<String> countryCodes) {
        this.countryCodes = countryCodes;
    }

    public static enum ReplacementState {
        ALL, VALID, REPLACED
    }
}
