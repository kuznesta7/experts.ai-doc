package ai.unico.platform.search.api.dto;

import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.dto.OrganizationLicencePreviewDto;

import java.util.ArrayList;
import java.util.List;

public class AdminOrganizationDto {

    private Long organizationId;

    private String organizationName;

    private String organizationAbbrev;

    private List<OrganizationBaseDto> rootOrganizations = new ArrayList<>();

    private List<OrganizationBaseDto> upperOrganizations = new ArrayList<>();

    private List<OrganizationBaseDto> lowerOrganizations = new ArrayList<>();

    private List<OrganizationBaseDto> affiliatedOrganizations = new ArrayList<>();

    private List<OrganizationBaseDto> membersOrganizations = new ArrayList<>(); //todo add

    private List<OrganizationBaseDto> replacedOrganizations = new ArrayList<>();

    private Long substituteOrganizationId;

    private String substituteOrganizationName;

    private String substituteOrganizationAbbrev;

    private Float rank;

    private String countryCode;

    private boolean hasValidLicence;

    private Boolean hasAllowedSearch;

    private Boolean membersAllowed;

    private List<OrganizationLicencePreviewDto> organizationLicencePreviewDtos = new ArrayList<>();

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Long getSubstituteOrganizationId() {
        return substituteOrganizationId;
    }

    public void setSubstituteOrganizationId(Long substituteOrganizationId) {
        this.substituteOrganizationId = substituteOrganizationId;
    }

    public Float getRank() {
        return rank;
    }

    public void setRank(Float rank) {
        this.rank = rank;
    }

    public String getOrganizationAbbrev() {
        return organizationAbbrev;
    }

    public void setOrganizationAbbrev(String organizationAbbrev) {
        this.organizationAbbrev = organizationAbbrev;
    }

    public List<OrganizationLicencePreviewDto> getOrganizationLicencePreviewDtos() {
        return organizationLicencePreviewDtos;
    }

    public void setOrganizationLicencePreviewDtos(List<OrganizationLicencePreviewDto> organizationLicencePreviewDtos) {
        this.organizationLicencePreviewDtos = organizationLicencePreviewDtos;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public List<OrganizationBaseDto> getReplacedOrganizations() {
        return replacedOrganizations;
    }

    public void setReplacedOrganizations(List<OrganizationBaseDto> replacedOrganizations) {
        this.replacedOrganizations = replacedOrganizations;
    }

    public List<OrganizationBaseDto> getLowerOrganizations() {
        return lowerOrganizations;
    }

    public void setLowerOrganizations(List<OrganizationBaseDto> lowerOrganizations) {
        this.lowerOrganizations = lowerOrganizations;
    }

    public String getSubstituteOrganizationName() {
        return substituteOrganizationName;
    }

    public void setSubstituteOrganizationName(String substituteOrganizationName) {
        this.substituteOrganizationName = substituteOrganizationName;
    }

    public String getSubstituteOrganizationAbbrev() {
        return substituteOrganizationAbbrev;
    }

    public void setSubstituteOrganizationAbbrev(String substituteOrganizationAbbrev) {
        this.substituteOrganizationAbbrev = substituteOrganizationAbbrev;
    }

    public List<OrganizationBaseDto> getUpperOrganizations() {
        return upperOrganizations;
    }

    public void setUpperOrganizations(List<OrganizationBaseDto> upperOrganizations) {
        this.upperOrganizations = upperOrganizations;
    }

    public List<OrganizationBaseDto> getAffiliatedOrganizations() {
        return affiliatedOrganizations;
    }

    public void setAffiliatedOrganizations(List<OrganizationBaseDto> affiliatedOrganizations) {
        this.affiliatedOrganizations = affiliatedOrganizations;
    }

    public List<OrganizationBaseDto> getRootOrganizations() {
        return rootOrganizations;
    }

    public void setRootOrganizations(List<OrganizationBaseDto> rootOrganizations) {
        this.rootOrganizations = rootOrganizations;
    }

    public boolean isHasValidLicence() {
        return hasValidLicence;
    }

    public void setHasValidLicence(boolean hasValidLicence) {
        this.hasValidLicence = hasValidLicence;
    }

    public Boolean getHasAllowedSearch() {
        return hasAllowedSearch;
    }

    public void setHasAllowedSearch(Boolean hasAllowedSearch) {
        if (hasAllowedSearch == null) {
            hasAllowedSearch = false;
        }
        this.hasAllowedSearch = hasAllowedSearch;
    }

    public Boolean isMembersAllowed() {
        return membersAllowed;
    }

    public void setMembersAllowed(Boolean membersAllowed) {

        this.membersAllowed = membersAllowed;
    }

    public List<OrganizationBaseDto> getMembersOrganizations() {
        return membersOrganizations;
    }

    public void setMembersOrganizations(List<OrganizationBaseDto> membersOrganizations) {
        this.membersOrganizations = membersOrganizations;
    }
}
