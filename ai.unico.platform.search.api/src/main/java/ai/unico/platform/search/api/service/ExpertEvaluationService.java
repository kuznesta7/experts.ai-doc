package ai.unico.platform.search.api.service;

import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.Set;

public interface ExpertEvaluationService {

    String indexName = ExpertIndexService.indexName;

    void evaluateExperts(Integer limit, UserContext userContext) throws IOException;

    void recalculateComputedFieldsForExperts(Set<String> expertCodes) throws IOException;

    void recalculateComputedFieldsForUser(Long userId) throws IOException;

}
