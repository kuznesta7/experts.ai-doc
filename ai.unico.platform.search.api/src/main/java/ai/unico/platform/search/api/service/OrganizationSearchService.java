package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.dto.OrganizationAutocompleteDto;
import ai.unico.platform.search.api.filter.AdminOrganizationFilter;
import ai.unico.platform.search.api.filter.OrganizationAutocompleteFilter;
import ai.unico.platform.search.api.filter.OrganizationSearchFilter;
import ai.unico.platform.search.api.wrapper.AdminOrganizationWrapper;
import ai.unico.platform.search.api.wrapper.OrganizationWrapper;
import ai.unico.platform.store.api.dto.OrganizationBaseDto;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface OrganizationSearchService {

    String indexName = OrganizationIndexService.indexName;

    OrganizationWrapper findOrganizations(OrganizationSearchFilter organizationSearchFilter) throws IOException;

    List<OrganizationAutocompleteDto> autocompleteOrganizations(OrganizationAutocompleteFilter filter) throws IOException;

    List<OrganizationAutocompleteDto> autocompleteOrganizationsForResearchProjects(OrganizationAutocompleteFilter filter, Long organizationId) throws IOException;

    List<OrganizationAutocompleteDto> autocompleteOrganizationsForResearchOutcomes(OrganizationAutocompleteFilter filter, Long organizationId) throws IOException;

    List<OrganizationAutocompleteDto> findAutocompleteOrganizations(Set<Long> organizationIds) throws IOException;

    List<OrganizationBaseDto> findBaseOrganizations(Set<Long> organizationIds) throws IOException;

    AdminOrganizationWrapper findOrganizationsForAdmin(AdminOrganizationFilter filter) throws IOException;

    Map<Long, OrganizationBaseDto> findBaseOrganizationsMap(Set<Long> organizationToLoadIds) throws IOException;
}
