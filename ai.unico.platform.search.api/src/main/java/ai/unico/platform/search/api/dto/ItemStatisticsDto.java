package ai.unico.platform.search.api.dto;

import java.util.HashMap;
import java.util.Map;

public class ItemStatisticsDto {

    private Long numberOfAllItems;

    private Map<Long, Long> numberOfItemsByType = new HashMap<>();

    private Map<Integer, Long> allItemsYearHistogram = new HashMap<>();

    private Map<Long, Map<Integer, Long>> itemTypesYearHistogram = new HashMap<>();

    public Long getNumberOfAllItems() {
        return numberOfAllItems;
    }

    public void setNumberOfAllItems(Long numberOfAllItems) {
        this.numberOfAllItems = numberOfAllItems;
    }

    public Map<Long, Long> getNumberOfItemsByType() {
        return numberOfItemsByType;
    }

    public void setNumberOfItemsByType(Map<Long, Long> numberOfItemsByType) {
        this.numberOfItemsByType = numberOfItemsByType;
    }

    public Map<Integer, Long> getAllItemsYearHistogram() {
        return allItemsYearHistogram;
    }

    public void setAllItemsYearHistogram(Map<Integer, Long> allItemsYearHistogram) {
        this.allItemsYearHistogram = allItemsYearHistogram;
    }

    public Map<Long, Map<Integer, Long>> getItemTypesYearHistogram() {
        return itemTypesYearHistogram;
    }

    public void setItemTypesYearHistogram(Map<Long, Map<Integer, Long>> itemTypesYearHistogram) {
        this.itemTypesYearHistogram = itemTypesYearHistogram;
    }
}

