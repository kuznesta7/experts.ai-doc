package ai.unico.platform.search.api.service;

import ai.unico.platform.store.api.dto.ProjectIndexDto;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.Map;

public interface ProjectIndexService {
    String indexName = "project";

    void reindexProjects(Integer limit, boolean clean, String target, UserContext userContext) throws IOException;

    void updateProject(ProjectIndexDto projectIndexDto) throws IOException;

    void replaceExperts(Map<String, String> replacementMap, boolean originalOnly) throws IOException;
}
