package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.filter.CommercializationFilter;
import ai.unico.platform.search.api.wrapper.EvidenceCommercializationWrapper;
import ai.unico.platform.store.api.dto.CommercializationStatisticsDto;
import ai.unico.platform.util.dto.FileDto;

import java.io.IOException;
import java.util.Set;

public interface EvidenceCommercializationService {

    String indexName = CommercializationProjectIndexService.indexName;

    EvidenceCommercializationWrapper findEvidenceCommercialization(CommercializationFilter evidenceFilter) throws IOException;

    FileDto exportEvidenceCommercialization(CommercializationFilter evidenceFilter);

    CommercializationStatisticsDto findCommercializationStatistics(Set<Long> organizationIds) throws IOException;

}
