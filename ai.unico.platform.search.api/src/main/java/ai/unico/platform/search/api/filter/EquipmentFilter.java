package ai.unico.platform.search.api.filter;

import java.util.HashSet;
import java.util.Set;

public class EquipmentFilter extends BaseFilter implements OrganizationEvidenceFilter {
    private Long organizationId;
    private Set<Long> organizationIds = new HashSet<>();
    private Long equipmentDomainId;
    private Long equipmentTypeId;
    private Integer yearFrom;
    private Integer yearTo;
    private Boolean portableDevice;

    private Boolean includeHidden = false;

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Long getEquipmentDomainId() {
        return equipmentDomainId;
    }

    public void setEquipmentDomainId(Long equipmentDomainId) {
        this.equipmentDomainId = equipmentDomainId;
    }

    public Long getEquipmentTypeId() {
        return equipmentTypeId;
    }

    public void setEquipmentTypeId(Long equipmentTypeId) {
        this.equipmentTypeId = equipmentTypeId;
    }

    public Integer getYearFrom() {
        return yearFrom;
    }

    public void setYearFrom(Integer yearFrom) {
        this.yearFrom = yearFrom;
    }

    public Integer getYearTo() {
        return yearTo;
    }

    public void setYearTo(Integer yearTo) {
        this.yearTo = yearTo;
    }

    public Boolean getPortableDevice() {
        return portableDevice;
    }

    public void setPortableDevice(Boolean portableDevice) {
        this.portableDevice = portableDevice;
    }

    public Boolean getIncludeHidden() {
        return includeHidden;
    }

    public void setIncludeHidden(Boolean includeHidden) {
        this.includeHidden = includeHidden;
    }
}
