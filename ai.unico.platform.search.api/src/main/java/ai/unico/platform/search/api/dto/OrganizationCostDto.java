package ai.unico.platform.search.api.dto;

public class OrganizationCostDto {
    private Long organizationId;
    private Double cost;

    public OrganizationCostDto() {
    }

    public OrganizationCostDto(Long organizationId, Double cost) {
        this.organizationId = organizationId;
        this.cost = cost;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }
}
