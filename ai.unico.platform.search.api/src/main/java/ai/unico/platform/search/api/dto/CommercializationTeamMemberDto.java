package ai.unico.platform.search.api.dto;

import ai.unico.platform.store.api.dto.UserExpertBaseDto;

public class CommercializationTeamMemberDto extends UserExpertBaseDto {

    public CommercializationTeamMemberDto(UserExpertBaseDto userExpertBaseDto) {
        setExpertCode(userExpertBaseDto.getExpertCode());
        setName(userExpertBaseDto.getName());
        setUserId(userExpertBaseDto.getUserId());
        setExpertId(userExpertBaseDto.getExpertId());
        setOrganizationIds(userExpertBaseDto.getOrganizationIds());
    }

    private boolean teamLeader;

    public boolean isTeamLeader() {
        return teamLeader;
    }

    public void setTeamLeader(boolean teamLeader) {
        this.teamLeader = teamLeader;
    }
}
