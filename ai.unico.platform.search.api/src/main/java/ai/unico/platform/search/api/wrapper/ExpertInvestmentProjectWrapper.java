package ai.unico.platform.search.api.wrapper;

import ai.unico.platform.search.api.dto.InvestmentProjectPublicPreviewDto;

import java.util.ArrayList;
import java.util.List;

public class ExpertInvestmentProjectWrapper {

    private Long numberOfRelevantInvestmentProjects;

    private Double scoreSum;

    private Integer limit;

    private Integer page;

    private List<InvestmentProjectPublicPreviewDto> investmentProjectPublicPreviewDtos = new ArrayList<>();

    public Long getNumberOfRelevantInvestmentProjects() {
        return numberOfRelevantInvestmentProjects;
    }

    public void setNumberOfRelevantInvestmentProjects(Long numberOfRelevantInvestmentProjects) {
        this.numberOfRelevantInvestmentProjects = numberOfRelevantInvestmentProjects;
    }

    public Double getScoreSum() {
        return scoreSum;
    }

    public void setScoreSum(Double scoreSum) {
        this.scoreSum = scoreSum;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<InvestmentProjectPublicPreviewDto> getInvestmentProjectPublicPreviewDtos() {
        return investmentProjectPublicPreviewDtos;
    }

    public void setInvestmentProjectPublicPreviewDtos(List<InvestmentProjectPublicPreviewDto> investmentProjectPublicPreviewDtos) {
        this.investmentProjectPublicPreviewDtos = investmentProjectPublicPreviewDtos;
    }
}
