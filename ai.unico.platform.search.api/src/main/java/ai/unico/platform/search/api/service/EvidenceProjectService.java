package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.dto.OrganizationCostWrapperDto;
import ai.unico.platform.search.api.dto.ProjectDetailDto;
import ai.unico.platform.search.api.dto.ProjectStatisticsDto;
import ai.unico.platform.search.api.filter.ProjectFilter;
import ai.unico.platform.search.api.wrapper.ProjectWrapper;
import ai.unico.platform.store.api.dto.OrganizationDetailDto;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface EvidenceProjectService {

    String indexName = ProjectIndexService.indexName;

    ProjectWrapper findProjects(ProjectFilter projectFilter) throws IOException;

    ProjectWrapper findProjectsWithRecommendation(ProjectFilter projectFilter, OrganizationDetailDto organizationDetailDto) throws IOException;

    ProjectDetailDto findProject(UserContext userContext, String projectCode) throws IOException;

    ProjectStatisticsDto findProjectStatistics(Set<Long> organizationIds) throws IOException;

    Map<Long, Double> findProjectStatisticsPerCompany(Set<Long> organizationIds, Integer year) throws IOException;

    List<OrganizationCostWrapperDto> findTopPartners(Set<Long> organizationIds, Integer yearFrom, Integer yearTo) throws IOException;
}
