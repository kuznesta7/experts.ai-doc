package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.dto.SearchResultBucketDto;
import ai.unico.platform.search.api.filter.ExpertSearchFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.wrapper.ExpertItemWrapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ItemSearchService {

    String indexName = ItemIndexService.indexName;

    Float minScore = 4.0F;

    List<SearchResultBucketDto> findExpertsForItemSearch(ExpertSearchFilter expertSearchFilter) throws IOException;

    List<SearchResultBucketDto> findOrganizationsForItemSearch(ExpertSearchFilter expertSearchFilter) throws IOException;

    ExpertItemWrapper findExpertItems(String expertCode, ExpertSearchResultFilter expertSearchResultFilter) throws IOException;

    List<ExpertItemStatistics> findItemStatisticsForExperts(Set<String> resultExpertsCodes) throws IOException;

    List<ExpertItemStatistics> findItemStatisticsForExperts(Set<String> resultExpertsCodes, Set<Long> organizationIds) throws IOException;

    ExpertItemStatistics findItemStatisticsForExpert(String expertCode) throws IOException;

    class ExpertItemStatistics {

        private String expertCode;

        private Long numberOfItems = 0L;

        private Map<Long, Long> itemTypeGroupsWithCounts = new HashMap<>();

        private Map<Integer, Long> yearActivityCounts = new HashMap<>();

        public String getExpertCode() {
            return expertCode;
        }

        public void setExpertCode(String expertCode) {
            this.expertCode = expertCode;
        }

        public Long getNumberOfItems() {
            return numberOfItems;
        }

        public void setNumberOfItems(Long numberOfItems) {
            this.numberOfItems = numberOfItems;
        }

        public Map<Long, Long> getItemTypeGroupsWithCounts() {
            return itemTypeGroupsWithCounts;
        }

        public void setItemTypeGroupsWithCounts(Map<Long, Long> itemTypeGroupsWithCounts) {
            this.itemTypeGroupsWithCounts = itemTypeGroupsWithCounts;
        }

        public Map<Integer, Long> getYearActivityCounts() {
            return yearActivityCounts;
        }

        public void setYearActivityCounts(Map<Integer, Long> yearActivityCounts) {
            this.yearActivityCounts = yearActivityCounts;
        }
    }
}
