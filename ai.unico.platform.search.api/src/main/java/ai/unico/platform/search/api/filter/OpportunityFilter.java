package ai.unico.platform.search.api.filter;

import java.util.HashSet;
import java.util.Set;

public class OpportunityFilter extends BaseFilter implements OrganizationEvidenceFilter, RecommendationFilter {

    private String user;

    private String recommId;

    private Long organizationId;

    private Long opportunityType;

    private Long jobType;

    private Set<Long> organizationIds = new HashSet<>();

    private String studentHash;

    private Boolean includeHidden = false;

    @Override
    public Long getOrganizationId() {
        return organizationId;
    }

    @Override
    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Long getOpportunityType() {
        return opportunityType;
    }

    public void setOpportunityType(Long opportunityType) {
        this.opportunityType = opportunityType;
    }

    public Long getJobType() {
        return jobType;
    }

    public void setJobType(Long jobType) {
        this.jobType = jobType;
    }

    @Override
    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    @Override
    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getStudentHash() {
        return studentHash;
    }

    public void setStudentHash(String studentHash) {
        this.studentHash = studentHash;
    }

    public Boolean getIncludeHidden() {
        return includeHidden;
    }

    public void setIncludeHidden(Boolean includeHidden) {
        this.includeHidden = includeHidden;
    }

    @Override
    public String getRecommId() {
        return recommId;
    }

    @Override
    public void setRecommId(String recommId) {
        this.recommId = recommId;
    }
}
