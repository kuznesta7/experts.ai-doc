package ai.unico.platform.search.api.wrapper;

import ai.unico.platform.search.api.dto.OntologyItemSearchDto;
import ai.unico.platform.search.api.dto.UserProfileSearchPreviewDto;
import ai.unico.platform.store.api.dto.ExpertSearchHistoryPreviewDto;

import java.util.ArrayList;
import java.util.List;

public class SearchResultWrapper {

    private Long numberOfResults;

    private Integer limit;

    private Integer page;

    private List<UserProfileSearchPreviewDto> userProfilePreviews = new ArrayList<>();

    private List<OntologyItemSearchDto> ontologyItemSearchDtos = new ArrayList<>();

    private Long searchHistId;

    private ExpertSearchHistoryPreviewDto savedSearch;

    public Long getNumberOfResults() {
        return numberOfResults;
    }

    public void setNumberOfResults(Long numberOfResults) {
        this.numberOfResults = numberOfResults;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<UserProfileSearchPreviewDto> getUserProfilePreviews() {
        return userProfilePreviews;
    }

    public void setUserProfilePreviews(List<UserProfileSearchPreviewDto> userProfilePreviews) {
        this.userProfilePreviews = userProfilePreviews;
    }

    public List<OntologyItemSearchDto> getOntologyItemSearchDtos() {
        return ontologyItemSearchDtos;
    }

    public void setOntologyItemSearchDtos(List<OntologyItemSearchDto> ontologyItemSearchDtos) {
        this.ontologyItemSearchDtos = ontologyItemSearchDtos;
    }

    public Long getSearchHistId() {
        return searchHistId;
    }

    public void setSearchHistId(Long searchHistId) {
        this.searchHistId = searchHistId;
    }

    public ExpertSearchHistoryPreviewDto getSavedSearch() {
        return savedSearch;
    }

    public void setSavedSearch(ExpertSearchHistoryPreviewDto savedSearch) {
        this.savedSearch = savedSearch;
    }
}
