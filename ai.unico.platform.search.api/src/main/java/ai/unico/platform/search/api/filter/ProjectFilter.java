package ai.unico.platform.search.api.filter;

import ai.unico.platform.util.enums.Confidentiality;

import java.util.HashSet;
import java.util.Set;

public class ProjectFilter extends BaseFilter implements OrganizationEvidenceFilter, RecommendationFilter {

    private String user;

    private String recommId;

    private Long organizationId;

    private Integer yearFrom;

    private Integer yearTo;

    private Set<Long> organizationIds = new HashSet<>();

    private Set<Confidentiality> confidentiality = new HashSet<>();

    private Set<String> expertCodes = new HashSet<>();

    private Set<Long> projectProgramIds = new HashSet<>();

    private Set<Long> participatingOrganizationIds = new HashSet<>();

    private boolean restrictOrganization = false;

    private boolean verifiedOnly;

    private boolean notVerifiedOnly;

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Integer getYearFrom() {
        return yearFrom;
    }

    public void setYearFrom(Integer yearFrom) {
        this.yearFrom = yearFrom;
    }

    public Integer getYearTo() {
        return yearTo;
    }

    public void setYearTo(Integer yearTo) {
        this.yearTo = yearTo;
    }

    public boolean isRestrictOrganization() {
        return restrictOrganization;
    }

    public void setRestrictOrganization(boolean restrictOrganization) {
        this.restrictOrganization = restrictOrganization;
    }

    public boolean isVerifiedOnly() {
        return verifiedOnly;
    }

    public void setVerifiedOnly(boolean verifiedOnly) {
        this.verifiedOnly = verifiedOnly;
    }

    public boolean isNotVerifiedOnly() {
        return notVerifiedOnly;
    }

    public void setNotVerifiedOnly(boolean notVerifiedOnly) {
        this.notVerifiedOnly = notVerifiedOnly;
    }

    public Set<Confidentiality> getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Set<Confidentiality> confidentiality) {
        this.confidentiality = confidentiality;
    }

    public Set<Long> getProjectProgramIds() {
        return projectProgramIds;
    }

    public void setProjectProgramIds(Set<Long> projectProgramIds) {
        this.projectProgramIds = projectProgramIds;
    }

    public Set<String> getExpertCodes() {
        return expertCodes;
    }

    public void setExpertCodes(Set<String> expertCodes) {
        this.expertCodes = expertCodes;
    }

    public Set<Long> getParticipatingOrganizationIds() {
        return participatingOrganizationIds;
    }

    public void setParticipatingOrganizationIds(Set<Long> participatingOrganizationIds) {
        this.participatingOrganizationIds = participatingOrganizationIds;
    }

    @Override
    public String getUser() {
        return user;
    }

    @Override
    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String getRecommId() {
        return recommId;
    }

    @Override
    public void setRecommId(String recommId) {
        this.recommId = recommId;
    }
}
