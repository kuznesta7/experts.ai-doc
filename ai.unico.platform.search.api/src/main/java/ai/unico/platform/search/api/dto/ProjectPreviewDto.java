package ai.unico.platform.search.api.dto;

import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.dto.UserExpertBaseDto;
import ai.unico.platform.util.enums.Confidentiality;

import java.util.*;

public class ProjectPreviewDto {

    private String projectCode;

    private String projectNumber;

    private String projectLink;

    private Long projectId;

    private Long originalProjectId;

    private String name;

    private String description;

    private Date startDate;

    private Date endDate;

    private boolean verified;

    private boolean unitVerified;

    private Double budgetTotal;

    private Double organizationBudgetTotal;

    private List<String> keywords = new ArrayList<>();

    private List<UserExpertBaseDto> projectExperts = new ArrayList<>();

    private Map<Long, String> parentProjectIds = new HashMap<>();

    private List<OrganizationBaseDto> organizationBaseDtos = new ArrayList<>();

    private String projectProgramName;

    private Long projectProgramId;

    private Confidentiality confidentiality;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getOriginalProjectId() {
        return originalProjectId;
    }

    public void setOriginalProjectId(Long originalProjectId) {
        this.originalProjectId = originalProjectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public List<UserExpertBaseDto> getProjectExperts() {
        return projectExperts;
    }

    public void setProjectExperts(List<UserExpertBaseDto> projectExperts) {
        this.projectExperts = projectExperts;
    }

    public Map<Long, String> getParentProjectIds() {
        return parentProjectIds;
    }

    public void setParentProjectIds(Map<Long, String> parentProjectIds) {
        this.parentProjectIds = parentProjectIds;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public Double getBudgetTotal() {
        return budgetTotal;
    }

    public void setBudgetTotal(Double budgetTotal) {
        this.budgetTotal = budgetTotal;
    }

    public List<OrganizationBaseDto> getOrganizationBaseDtos() {
        return organizationBaseDtos;
    }

    public void setOrganizationBaseDtos(List<OrganizationBaseDto> organizationBaseDtos) {
        this.organizationBaseDtos = organizationBaseDtos;
    }

    public Double getOrganizationBudgetTotal() {
        return organizationBudgetTotal;
    }

    public void setOrganizationBudgetTotal(Double organizationBudgetTotal) {
        this.organizationBudgetTotal = organizationBudgetTotal;
    }

    public boolean isUnitVerified() {
        return unitVerified;
    }

    public void setUnitVerified(boolean unitVerified) {
        this.unitVerified = unitVerified;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getProjectProgramName() {
        return projectProgramName;
    }

    public void setProjectProgramName(String projectProgramName) {
        this.projectProgramName = projectProgramName;
    }

    public Long getProjectProgramId() {
        return projectProgramId;
    }

    public void setProjectProgramId(Long projectProgramId) {
        this.projectProgramId = projectProgramId;
    }

    public Confidentiality getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Confidentiality confidentiality) {
        this.confidentiality = confidentiality;
    }

    public String getProjectLink() {
        return projectLink;
    }

    public void setProjectLink(String projectLink) {
        this.projectLink = projectLink;
    }
}
