package ai.unico.platform.search.api.filter;

import ai.unico.platform.util.enums.ExpertSearchType;

import java.util.HashSet;
import java.util.Set;

public class OrganizationSearchFilter extends BaseFilter {

    private Set<String> countryCodes = new HashSet<>();
    private ExpertSearchType expertSearchType;
    private Set<Long> organizationIds = new HashSet<>();
    private Set<Long> organizationTypesIds = new HashSet<>();
    boolean useSubstituteOrganization;

    private boolean exactMatch=false;

    public Set<String> getCountryCodes() {
        return countryCodes;
    }

    public void setCountryCodes(Set<String> countryCodes) {
        this.countryCodes = countryCodes;
    }

    public ExpertSearchType getExpertSearchType() {
        return expertSearchType;
    }

    public void setExpertSearchType(ExpertSearchType expertSearchType) {
        this.expertSearchType = expertSearchType;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<Long> getOrganizationTypesIds() {
        return organizationTypesIds;
    }

    public void setOrganizationTypesIds(Set<Long> organizationTypesIds) {
        this.organizationTypesIds = organizationTypesIds;
    }

    public boolean isExactMatch() {
        return exactMatch;
    }

    public void setExactMatch(boolean exactMatch) {
        this.exactMatch = exactMatch;
    }

    public boolean isUseSubstituteOrganization() {
        return useSubstituteOrganization;
    }

    public void setUseSubstituteOrganization(boolean useSubstituteOrganization) {
        this.useSubstituteOrganization = useSubstituteOrganization;
    }
}
