package ai.unico.platform.search.api.wrapper;

import ai.unico.platform.search.api.dto.EvidenceCommercializationPreviewDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class EvidenceCommercializationWrapper {

    private Long numberOfAllItems;

    private Integer limit;

    private Integer page;

    private Long organizationId;

    private Set<Long> organizationUnitIds;

    private List<EvidenceCommercializationPreviewDto> evidenceCommercializationDtos = new ArrayList<>();

    public Long getNumberOfAllItems() {
        return numberOfAllItems;
    }

    public void setNumberOfAllItems(Long numberOfAllItems) {
        this.numberOfAllItems = numberOfAllItems;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<EvidenceCommercializationPreviewDto> getEvidenceCommercializationDtos() {
        return evidenceCommercializationDtos;
    }

    public void setEvidenceCommercializationDtos(List<EvidenceCommercializationPreviewDto> evidenceCommercializationDtos) {
        this.evidenceCommercializationDtos = evidenceCommercializationDtos;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Set<Long> getOrganizationUnitIds() {
        return organizationUnitIds;
    }

    public void setOrganizationUnitIds(Set<Long> organizationUnitIds) {
        this.organizationUnitIds = organizationUnitIds;
    }
}
