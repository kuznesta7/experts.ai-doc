package ai.unico.platform.search.api.dto;

public class OntologyItemSearchDto {

    private String value;

    private Float relevancy;

    private boolean disabled;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Float getRelevancy() {
        return relevancy;
    }

    public void setRelevancy(Float relevancy) {
        this.relevancy = relevancy;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }
}
