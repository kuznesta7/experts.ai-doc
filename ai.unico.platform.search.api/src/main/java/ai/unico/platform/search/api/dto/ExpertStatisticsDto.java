package ai.unico.platform.search.api.dto;

public class ExpertStatisticsDto {

    private Long numberOfAllExperts;

    private Long numberOfVerifiedExperts;

    private Long numberOfNotVerifiedExperts;

    public Long getNumberOfAllExperts() {
        return numberOfAllExperts;
    }

    public void setNumberOfAllExperts(Long numberOfAllExperts) {
        this.numberOfAllExperts = numberOfAllExperts;
    }

    public Long getNumberOfVerifiedExperts() {
        return numberOfVerifiedExperts;
    }

    public void setNumberOfVerifiedExperts(Long numberOfVerifiedExperts) {
        this.numberOfVerifiedExperts = numberOfVerifiedExperts;
    }

    public Long getNumberOfNotVerifiedExperts() {
        return numberOfNotVerifiedExperts;
    }

    public void setNumberOfNotVerifiedExperts(Long numberOfNotVerifiedExperts) {
        this.numberOfNotVerifiedExperts = numberOfNotVerifiedExperts;
    }
}
