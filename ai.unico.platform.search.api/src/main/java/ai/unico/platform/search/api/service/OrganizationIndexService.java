package ai.unico.platform.search.api.service;

import ai.unico.platform.store.api.dto.OrganizationIndexDto;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;

public interface OrganizationIndexService {

    String indexName = "organization";

    void reindexOrganizations(Integer limit, boolean clean, UserContext userContext) throws IOException;

    void indexOrganization(OrganizationIndexDto organizationDto) throws IOException;

    void replaceOrganization(Long originalOrganizationId, Long substituteOrganizationId) throws IOException;

}
