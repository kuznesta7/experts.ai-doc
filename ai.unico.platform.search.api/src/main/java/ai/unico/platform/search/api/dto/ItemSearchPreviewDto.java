package ai.unico.platform.search.api.dto;

import java.util.ArrayList;
import java.util.List;

public class ItemSearchPreviewDto {

    private String itemCode;

    private Long itemId;

    private Long originalItemId;

    private String itemName;

    private String itemDescription;

    private Double itemRank;

    private Long itemTypeId;

    private List<String> keywords = new ArrayList<>();

    private Integer year;

    private Boolean favorite;

    private boolean hidden;

    private Boolean translationUnavailable;

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Double getItemRank() {
        return itemRank;
    }

    public void setItemRank(Double itemRank) {
        this.itemRank = itemRank;
    }

    public Long getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(Long itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Long getOriginalItemId() {
        return originalItemId;
    }

    public void setOriginalItemId(Long originalItemId) {
        this.originalItemId = originalItemId;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public Boolean getTranslationUnavailable() {
        return translationUnavailable;
    }

    public void setTranslationUnavailable(Boolean translationUnavailable) {
        this.translationUnavailable = translationUnavailable;
    }
}
