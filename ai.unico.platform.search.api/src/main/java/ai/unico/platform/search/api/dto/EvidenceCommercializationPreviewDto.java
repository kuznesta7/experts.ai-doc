package ai.unico.platform.search.api.dto;

import ai.unico.platform.store.api.dto.CommercializationCategoryPreviewDto;
import ai.unico.platform.store.api.dto.UserExpertBaseDto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EvidenceCommercializationPreviewDto {

    private Long id;

    private String imgCheckSum;

    private String name;

    private String executiveSummary;

    private Long domainId;

    private String domainName;

    private Integer investmentFrom;

    private Integer investmentTo;

    private String statusName;

    private Long statusId;

    private Integer ideaScore;

    private Integer readinessLevel;

    private List<UserExpertBaseDto> teamMemberDtos = new ArrayList<>();

    private List<String> keywords;

    private List<CommercializationCategoryPreviewDto> commercializationCategoryPreviews = new ArrayList<>();

    private Long commercializationPriorityId;

    private String commercializationPriorityName;

    private Date startDate;

    private Date endDate;

    private Long intellectualPropertyOwnerOrganizationId;

    private Long intellectualPropertyOwnerOrganizationName;

    private Long intermediaryOrganizationId;

    private Long intermediaryOrganizationName;

    private String link;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExecutiveSummary() {
        return executiveSummary;
    }

    public void setExecutiveSummary(String executiveSummary) {
        this.executiveSummary = executiveSummary;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Integer getIdeaScore() {
        return ideaScore;
    }

    public void setIdeaScore(Integer ideaScore) {
        this.ideaScore = ideaScore;
    }

    public Integer getReadinessLevel() {
        return readinessLevel;
    }

    public void setReadinessLevel(Integer readinessLevel) {
        this.readinessLevel = readinessLevel;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public Integer getInvestmentFrom() {
        return investmentFrom;
    }

    public void setInvestmentFrom(Integer investmentFrom) {
        this.investmentFrom = investmentFrom;
    }

    public Integer getInvestmentTo() {
        return investmentTo;
    }

    public void setInvestmentTo(Integer investmentTo) {
        this.investmentTo = investmentTo;
    }

    public List<UserExpertBaseDto> getTeamMemberDtos() {
        return teamMemberDtos;
    }

    public void setTeamMemberDtos(List<UserExpertBaseDto> teamMemberDtos) {
        this.teamMemberDtos = teamMemberDtos;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public void setCommercializationCategoryPreviews(List<CommercializationCategoryPreviewDto> commercializationCategoryPreviews) {
        this.commercializationCategoryPreviews = commercializationCategoryPreviews;
    }

    public List<CommercializationCategoryPreviewDto> getCommercializationCategoryPreviews() {
        return commercializationCategoryPreviews;
    }

    public Long getCommercializationPriorityId() {
        return commercializationPriorityId;
    }

    public void setCommercializationPriorityId(Long commercializationPriorityId) {
        this.commercializationPriorityId = commercializationPriorityId;
    }

    public String getCommercializationPriorityName() {
        return commercializationPriorityName;
    }

    public void setCommercializationPriorityName(String commercializationPriorityName) {
        this.commercializationPriorityName = commercializationPriorityName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getIntellectualPropertyOwnerOrganizationId() {
        return intellectualPropertyOwnerOrganizationId;
    }

    public void setIntellectualPropertyOwnerOrganizationId(Long intellectualPropertyOwnerOrganizationId) {
        this.intellectualPropertyOwnerOrganizationId = intellectualPropertyOwnerOrganizationId;
    }

    public Long getIntellectualPropertyOwnerOrganizationName() {
        return intellectualPropertyOwnerOrganizationName;
    }

    public void setIntellectualPropertyOwnerOrganizationName(Long intellectualPropertyOwnerOrganizationName) {
        this.intellectualPropertyOwnerOrganizationName = intellectualPropertyOwnerOrganizationName;
    }

    public Long getIntermediaryOrganizationId() {
        return intermediaryOrganizationId;
    }

    public void setIntermediaryOrganizationId(Long intermediaryOrganizationId) {
        this.intermediaryOrganizationId = intermediaryOrganizationId;
    }

    public Long getIntermediaryOrganizationName() {
        return intermediaryOrganizationName;
    }

    public void setIntermediaryOrganizationName(Long intermediaryOrganizationName) {
        this.intermediaryOrganizationName = intermediaryOrganizationName;
    }

    public Long getDomainId() {
        return domainId;
    }

    public void setDomainId(Long domainId) {
        this.domainId = domainId;
    }

    public String getImgCheckSum() {
        return imgCheckSum;
    }

    public void setImgCheckSum(String imgCheckSum) {
        this.imgCheckSum = imgCheckSum;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
