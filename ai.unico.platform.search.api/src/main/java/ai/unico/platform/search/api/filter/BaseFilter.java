package ai.unico.platform.search.api.filter;

import org.apache.lucene.queryparser.flexible.standard.QueryParserUtil;

public abstract class BaseFilter {

    private String query;

    private Integer limit = 5;

    private Integer page = 1;

    public String getQuery() {
        return query;
    }

    /**
     * Retrieve the query string and escape it to improve the search results.
     * @return the escaped query string
     */
    public String getQueryForSearch() {
        if (query == null || query.isEmpty()) return "";
        return QueryParserUtil.escape(query);
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

}
