package ai.unico.platform.search.api.dto;

import ai.unico.platform.util.dto.OrganizationDto;

import java.util.ArrayList;
import java.util.List;

public class UserExpertPreviewDto {

    private String expertCode;

    private Long userId;

    private Long expertId;

    private String name;

    private String username;

    private String email;

    private boolean claimable;

    public boolean isClaimable() {
        return claimable;
    }

    public void setClaimable(boolean claimable) {
        this.claimable = claimable;
    }

    private List<OrganizationDto> organizationDtos = new ArrayList<>();

    public String getExpertCode() {
        return expertCode;
    }

    public void setExpertCode(String expertCode) {
        this.expertCode = expertCode;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<OrganizationDto> getOrganizationDtos() {
        return organizationDtos;
    }

    public void setOrganizationDtos(List<OrganizationDto> organizationDtos) {
        this.organizationDtos = organizationDtos;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
