package ai.unico.platform.search.api.service;

import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.Set;

public interface ItemAffiliationService {

    void recalculateItems(Integer limit, UserContext userContext) throws IOException;

    void recalculateComputedFieldsForItems(Set<String> itemsIds) throws IOException;

    void updateItemsForExpert(Set<Long> expertIds, Long organizationId, boolean add) throws IOException;

    void updateItemsForUser(Set<Long> userIds, Long organizationId, boolean add) throws IOException;

    void updateItemsForUser(Set<Long> userIds, Set<Long> organizationId) throws IOException;

}
