package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.dto.OpportunityPreviewDto;
import ai.unico.platform.search.api.filter.OpportunityFilter;
import ai.unico.platform.search.api.wrapper.OpportunityWrapper;
import ai.unico.platform.store.api.dto.OpportunityRegisterDto;
import ai.unico.platform.store.api.dto.OpportunityStatisticsDto;
import ai.unico.platform.store.api.dto.OrganizationDetailDto;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface OpportunitySearchService {
    String indexName = OpportunityIndexService.indexName;

    OpportunityWrapper findOpportunities(OpportunityFilter filter) throws IOException;

    OpportunityWrapper findOpportunitiesWithRecommendation(OpportunityFilter filter, OrganizationDetailDto organizationDetailDto) throws IOException;

    OpportunityWrapper findOpportunitiesWithRecommendation(OpportunityFilter filter, OrganizationDetailDto organizationDetailDto, List<String> recommendations) throws IOException;

    OpportunityWrapper findOpportunitiesWithCombinedRecommendation(OpportunityFilter filter, OrganizationDetailDto organizationDetailDto, List<String> recommendations) throws IOException;

    OpportunityPreviewDto findOpportunity(String opportunityId) throws IOException;

    OpportunityRegisterDto getOpportunity(String opportunityId) throws IOException;

    OpportunityStatisticsDto findOpportunityStatistics(Set<Long> organizationIds) throws IOException;

}
