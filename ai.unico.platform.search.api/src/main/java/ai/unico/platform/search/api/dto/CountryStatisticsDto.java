package ai.unico.platform.search.api.dto;

import java.util.Map;

public class CountryStatisticsDto {

    private String countryCode;

    private Long allExperts;

    private Long verifiedExperts;

    private Long activeExperts;

    private Long retiredExperts;

    private Map<String, Long> roleCount;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Long getAllExperts() {
        return allExperts;
    }

    public void setAllExperts(Long allExperts) {
        this.allExperts = allExperts;
    }

    public Long getVerifiedExperts() {
        return verifiedExperts;
    }

    public void setVerifiedExperts(Long verifiedExperts) {
        this.verifiedExperts = verifiedExperts;
    }

    public Long getActiveExperts() {
        return activeExperts;
    }

    public void setActiveExperts(Long activeExperts) {
        this.activeExperts = activeExperts;
    }

    public Long getRetiredExperts() {
        return retiredExperts;
    }

    public void setRetiredExperts(Long retiredExperts) {
        this.retiredExperts = retiredExperts;
    }

    public Map<String, Long> getRoleCount() {
        return roleCount;
    }

    public void setRoleCount(Map<String, Long> roleCount) {
        this.roleCount = roleCount;
    }
}
