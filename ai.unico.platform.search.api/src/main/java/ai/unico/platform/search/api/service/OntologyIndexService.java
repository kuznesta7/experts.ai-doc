package ai.unico.platform.search.api.service;

import ai.unico.platform.util.model.UserContext;

import java.io.IOException;

public interface OntologyIndexService {

    String indexName = "ontology";

    void reindexOntology(UserContext userContext) throws IOException;

}
