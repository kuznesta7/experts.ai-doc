package ai.unico.platform.search.api.dto;

import ai.unico.platform.store.api.dto.UserExpertBaseDto;

import java.util.ArrayList;
import java.util.List;

public class EvidenceItemPreviewWithExpertsDto extends EvidenceItemPreviewDto {

    private List<UserExpertBaseDto> expertPreviews = new ArrayList<>();

    public List<UserExpertBaseDto> getExpertPreviews() {
        return expertPreviews;
    }

    public void setExpertPreviews(List<UserExpertBaseDto> expertPreviews) {
        this.expertPreviews = expertPreviews;
    }
}
