package ai.unico.platform.search.api.dto;

import ai.unico.platform.store.api.dto.OrganizationBaseDto;

import java.util.ArrayList;
import java.util.List;

public class EvidenceItemPreviewDto {

    private String itemCode;

    private Long itemId;

    private Long originalItemId;

    private String itemBk;

    private String itemName;

    private String itemDescription;

    private String itemLink;

    private String doi;

    private Integer year;

    private Long itemTypeId;

    private List<String> keywords = new ArrayList<>();

    private boolean verified;

    private boolean unitVerified;

    private boolean active;

    private List<OrganizationBaseDto> organizationBaseDtos;

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getOriginalItemId() {
        return originalItemId;
    }

    public void setOriginalItemId(Long originalItemId) {
        this.originalItemId = originalItemId;
    }

    public String getItemBk() {
        return itemBk;
    }

    public void setItemBk(String itemBk) {
        this.itemBk = itemBk;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Long getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(Long itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public String getItemLink() {
        return itemLink;
    }

    public void setItemLink(String itemLink) {
        this.itemLink = itemLink;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public boolean isUnitVerified() {
        return unitVerified;
    }

    public void setUnitVerified(boolean unitVerified) {
        this.unitVerified = unitVerified;
    }

    public List<OrganizationBaseDto> getOrganizationBaseDtos() {
        return organizationBaseDtos;
    }

    public void setOrganizationBaseDtos(List<OrganizationBaseDto> organizationBaseDtos) {
        this.organizationBaseDtos = organizationBaseDtos;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
