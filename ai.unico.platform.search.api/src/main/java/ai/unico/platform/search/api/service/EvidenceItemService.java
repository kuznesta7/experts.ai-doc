package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.dto.EvidenceItemPreviewDto;
import ai.unico.platform.search.api.dto.ItemStatisticsDto;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.wrapper.EvidenceItemWrapper;
import ai.unico.platform.store.api.dto.OrganizationDetailDto;
import ai.unico.platform.util.dto.FileDto;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface EvidenceItemService {

    String indexName = ItemIndexService.indexName;

    EvidenceItemWrapper findAnalyticsItems(EvidenceFilter evidenceFilter) throws IOException;

    Boolean entityHasItem(EvidenceFilter evidenceFilter, String name) throws IOException;

    List<EvidenceItemPreviewDto> findAnalyticsItems(Collection<String> itemCodes) throws IOException;

    ItemStatisticsDto findItemStatistics(Set<Long> organizationIds, boolean verified) throws IOException;

    EvidenceItemWrapper findAnalyticsItemsWithOrder(EvidenceFilter evidenceFilter, ExpertSearchResultFilter.Order order) throws IOException;

    FileDto exportAnalyticsItems(EvidenceFilter evidenceFilter) throws IOException;

    Map<String, Long> findItemExpertStatistics(Set<Long> organizationIds, boolean verified) throws IOException;

    EvidenceItemWrapper findAnalyticsItemsWithOrderAndRecommendation(EvidenceFilter evidenceFilter, ExpertSearchResultFilter.Order order,
                                                                     OrganizationDetailDto organizationDetailDto) throws IOException;
}
