package ai.unico.platform.search.api.filter;

import java.util.HashSet;
import java.util.Set;

public class ExpertLookupFilter {

    private String query;

    private Set<Long> organizationIds = new HashSet<>();

    private Set<Long> affiliatedOrganizationIds = new HashSet<>();

    private Set<Long> disallowedOrganizationIds = new HashSet<>();

    private Set<String> expertCodes = new HashSet<>();

    private Set<Long> userIds = new HashSet<>();

    private Set<Long> expertIds = new HashSet<>();

    private Set<Long> disallowedUserIds = new HashSet<>();

    private Set<Long> disallowedExpertIds = new HashSet<>();

    private Set<String> disallowedExpertCodes = new HashSet<>();

    private boolean usersOnly;

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Set<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }

    public Set<Long> getExpertIds() {
        return expertIds;
    }

    public void setExpertIds(Set<Long> expertIds) {
        this.expertIds = expertIds;
    }

    public Set<Long> getDisallowedOrganizationIds() {
        return disallowedOrganizationIds;
    }

    public void setDisallowedOrganizationIds(Set<Long> disallowedOrganizationIds) {
        this.disallowedOrganizationIds = disallowedOrganizationIds;
    }

    public boolean isUsersOnly() {
        return usersOnly;
    }

    public void setUsersOnly(boolean usersOnly) {
        this.usersOnly = usersOnly;
    }

    public Set<Long> getDisallowedUserIds() {
        return disallowedUserIds;
    }

    public void setDisallowedUserIds(Set<Long> disallowedUserIds) {
        this.disallowedUserIds = disallowedUserIds;
    }

    public Set<Long> getDisallowedExpertIds() {
        return disallowedExpertIds;
    }

    public void setDisallowedExpertIds(Set<Long> disallowedExpertIds) {
        this.disallowedExpertIds = disallowedExpertIds;
    }

    public Set<Long> getAffiliatedOrganizationIds() {
        return affiliatedOrganizationIds;
    }

    public void setAffiliatedOrganizationIds(Set<Long> affiliatedOrganizationIds) {
        this.affiliatedOrganizationIds = affiliatedOrganizationIds;
    }

    public Set<String> getExpertCodes() {
        return expertCodes;
    }

    public void setExpertCodes(Set<String> expertCodes) {
        this.expertCodes = expertCodes;
    }

    public Set<String> getDisallowedExpertCodes() {
        return disallowedExpertCodes;
    }

    public void setDisallowedExpertCodes(Set<String> disallowedExpertCodes) {
        this.disallowedExpertCodes = disallowedExpertCodes;
    }
}
