package ai.unico.platform.search.api.dto;

public class SearchResultBucketDto {

    private String expertCode;

    private Long organizationId;

    private Double rank = 0D;

    private Long appearance = 0L;

    private Long investmentProjectAppearance = 0L;

    public String getExpertCode() {
        return expertCode;
    }

    public void setExpertCode(String expertCode) {
        this.expertCode = expertCode;
    }

    public Double getRank() {
        return rank;
    }

    public void setRank(Double rank) {
        this.rank = rank;
    }

    public Long getAppearance() {
        return appearance;
    }

    public void setAppearance(Long appearance) {
        this.appearance = appearance;
    }

    public Long getInvestmentProjectAppearance() {
        return investmentProjectAppearance;
    }

    public void setInvestmentProjectAppearance(Long investmentProjectAppearance) {
        this.investmentProjectAppearance = investmentProjectAppearance;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }
}
