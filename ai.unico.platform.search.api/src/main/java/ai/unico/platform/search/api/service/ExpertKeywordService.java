package ai.unico.platform.search.api.service;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

public interface ExpertKeywordService {

    String indexName = ItemIndexService.indexName;

    Map<String, Long> expertKeywords(String expertCode, Set<Long> verifiedOrganizationId) throws IOException;
}
