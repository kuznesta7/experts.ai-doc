package ai.unico.platform.search.api.service;

import ai.unico.platform.store.api.dto.OpportunityRegisterDto;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.Map;

public interface OpportunityIndexService {

    String indexName = "opportunity";

    void reindexOpportunity(Integer limit, boolean clean, String target, UserContext userContext) throws IOException;

    void reindexOpportunity(OpportunityRegisterDto opportunityDto) throws IOException;

    void deleteOpportunity(String opportunityCode) throws IOException;

    void replaceExperts(Map<String, String> replacementMap, boolean originalOnly) throws IOException;


}
