package ai.unico.platform.search.api.dto;

import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.dto.UserExpertBaseDto;
import ai.unico.platform.util.enums.Confidentiality;

import java.util.ArrayList;
import java.util.List;

public class LecturePreviewDto {

    private String lectureCode;

    private Long lectureId;

    private String lectureName;

    private String lectureDescription;

    private Long itemTypeId;

    private Integer year;

    private Confidentiality confidentiality;

    private String itemSpecification;

    private String itemDomain;

    private String itemScienceSpecification;

    private List<String> keywords = new ArrayList<>();

    private List<UserExpertBaseDto> expertPreviews = new ArrayList<>();

    private List<OrganizationBaseDto> organizationBaseDtos = new ArrayList<>();

    public String getLectureCode() {
        return lectureCode;
    }

    public void setLectureCode(String lectureCode) {
        this.lectureCode = lectureCode;
    }

    public Long getLectureId() {
        return lectureId;
    }

    public void setLectureId(Long lectureId) {
        this.lectureId = lectureId;
    }

    public String getLectureName() {
        return lectureName;
    }

    public void setLectureName(String lectureName) {
        this.lectureName = lectureName;
    }

    public String getLectureDescription() {
        return lectureDescription;
    }

    public void setLectureDescription(String lectureDescription) {
        this.lectureDescription = lectureDescription;
    }

    public Long getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(Long itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Confidentiality getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Confidentiality confidentiality) {
        this.confidentiality = confidentiality;
    }

    public String getItemSpecification() {
        return itemSpecification;
    }

    public void setItemSpecification(String itemSpecification) {
        this.itemSpecification = itemSpecification;
    }

    public String getItemDomain() {
        return itemDomain;
    }

    public void setItemDomain(String itemDomain) {
        this.itemDomain = itemDomain;
    }

    public String getItemScienceSpecification() {
        return itemScienceSpecification;
    }

    public void setItemScienceSpecification(String itemScienceSpecification) {
        this.itemScienceSpecification = itemScienceSpecification;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public List<UserExpertBaseDto> getExpertPreviews() {
        return expertPreviews;
    }

    public void setExpertPreviews(List<UserExpertBaseDto> expertPreviews) {
        this.expertPreviews = expertPreviews;
    }

    public List<OrganizationBaseDto> getOrganizationBaseDtos() {
        return organizationBaseDtos;
    }

    public void setOrganizationBaseDtos(List<OrganizationBaseDto> organizationBaseDtos) {
        this.organizationBaseDtos = organizationBaseDtos;
    }
}
