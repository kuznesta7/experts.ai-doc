package ai.unico.platform.search.api.dto;

import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.dto.ProjectProgramPreviewDto;
import ai.unico.platform.store.api.dto.UserExpertBaseDto;
import ai.unico.platform.util.enums.Confidentiality;

import java.util.*;

public class ProjectDetailDto {

    private String projectCode;

    private String projectNumber;

    private Long projectId;

    private Long originalProjectId;

    private String name;

    private String description;

    private Date startDate;

    private Date endDate;

    private Double budgetTotal;

    private Long ownerOrganizationId;

    private String ownerOrganizationName;

    private Confidentiality confidentiality;

    private String projectLink;

    private List<String> keywords = new ArrayList<>();

    private List<UserExpertBaseDto> projectExperts = new ArrayList<>();

    private Map<Long, String> parentProjectIds = new HashMap<>();

    private List<OrganizationBaseDto> organizationBaseDtos = new ArrayList<>();

    private List<OrganizationBaseDto> providingOrganizationBaseDtos = new ArrayList<>();

    private List<EvidenceItemPreviewDto> itemPreviewDtos = new ArrayList<>();

    private Map<Long, Map<Integer, Double>> organizationFinanceMap = new HashMap<>();

    private ProjectProgramPreviewDto projectProgram;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getOriginalProjectId() {
        return originalProjectId;
    }

    public void setOriginalProjectId(Long originalProjectId) {
        this.originalProjectId = originalProjectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public String getProjectLink() {
        return projectLink;
    }

    public void setProjectLink(String projectLink) {
        this.projectLink = projectLink;
    }

    public List<UserExpertBaseDto> getProjectExperts() {
        return projectExperts;
    }

    public void setProjectExperts(List<UserExpertBaseDto> projectExperts) {
        this.projectExperts = projectExperts;
    }

    public Map<Long, String> getParentProjectIds() {
        return parentProjectIds;
    }

    public void setParentProjectIds(Map<Long, String> parentProjectIds) {
        this.parentProjectIds = parentProjectIds;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public Double getBudgetTotal() {
        return budgetTotal;
    }

    public void setBudgetTotal(Double budgetTotal) {
        this.budgetTotal = budgetTotal;
    }

    public List<OrganizationBaseDto> getOrganizationBaseDtos() {
        return organizationBaseDtos;
    }

    public void setOrganizationBaseDtos(List<OrganizationBaseDto> organizationBaseDtos) {
        this.organizationBaseDtos = organizationBaseDtos;
    }

    public List<EvidenceItemPreviewDto> getItemPreviewDtos() {
        return itemPreviewDtos;
    }

    public void setItemPreviewDtos(List<EvidenceItemPreviewDto> itemPreviewDtos) {
        this.itemPreviewDtos = itemPreviewDtos;
    }

    public Map<Long, Map<Integer, Double>> getOrganizationFinanceMap() {
        return organizationFinanceMap;
    }

    public void setOrganizationFinanceMap(Map<Long, Map<Integer, Double>> organizationFinanceMap) {
        this.organizationFinanceMap = organizationFinanceMap;
    }

    public ProjectProgramPreviewDto getProjectProgram() {
        return projectProgram;
    }

    public void setProjectProgram(ProjectProgramPreviewDto projectProgram) {
        this.projectProgram = projectProgram;
    }

    public Long getOwnerOrganizationId() {
        return ownerOrganizationId;
    }

    public void setOwnerOrganizationId(Long ownerOrganizationId) {
        this.ownerOrganizationId = ownerOrganizationId;
    }

    public Confidentiality getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Confidentiality confidentiality) {
        this.confidentiality = confidentiality;
    }

    public String getOwnerOrganizationName() {
        return ownerOrganizationName;
    }

    public void setOwnerOrganizationName(String ownerOrganizationName) {
        this.ownerOrganizationName = ownerOrganizationName;
    }

    public List<OrganizationBaseDto> getProvidingOrganizationBaseDtos() {
        return providingOrganizationBaseDtos;
    }

    public void setProvidingOrganizationBaseDtos(List<OrganizationBaseDto> providingOrganizationBaseDtos) {
        this.providingOrganizationBaseDtos = providingOrganizationBaseDtos;
    }
}
