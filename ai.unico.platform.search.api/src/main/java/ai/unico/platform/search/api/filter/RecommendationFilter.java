package ai.unico.platform.search.api.filter;

public interface RecommendationFilter {

    String getUser();

    void setUser(String user);

    String getRecommId();

    void setRecommId(String recommId);

    Integer getPage();

    void setPage(Integer page);

    Integer getLimit();

    void setLimit(Integer limit);

    String getQuery();

    void setQuery(String query);



}
