package ai.unico.platform.search.api.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserExpertProfileSearchDetailDto {

    private String expertCode;

    private Long userId;

    private Long expertId;

    private String name;

    private List<UserOrganizationDto> organizationDtos = new ArrayList<>();

    private Double rank;

    private Long itemAppearance;

    private Long investmentProjectAppearance;

    private Long numberOfAllItems;

    private Long numberOfInvestmentProjects;

    private Long numberOfProjects;

    private Long numberOfAllThesis;

    private Long numberOfAllLectures;

    private List<ItemTypeGroupWithCountsDto> itemTypeGroupsWithCount = new ArrayList<>();

    private boolean verified;

    private boolean retired;

    private boolean claimable;

    private String description;

    private List<String> keywords;

    public boolean isClaimable() {
        return claimable;
    }

    public void setClaimable(boolean claimable) {
        this.claimable = claimable;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UserOrganizationDto> getOrganizationDtos() {
        return organizationDtos;
    }

    public void setOrganizationDtos(List<UserOrganizationDto> organizationDtos) {
        this.organizationDtos = organizationDtos;
    }

    public Double getRank() {
        return rank;
    }

    public void setRank(Double rank) {
        this.rank = rank;
    }

    public Long getItemAppearance() {
        return itemAppearance;
    }

    public void setItemAppearance(Long itemAppearance) {
        this.itemAppearance = itemAppearance;
    }

    public Long getNumberOfAllItems() {
        return numberOfAllItems;
    }

    public void setNumberOfAllItems(Long numberOfAllItems) {
        this.numberOfAllItems = numberOfAllItems;
    }

    public Long getNumberOfAllThesis() {
        return numberOfAllThesis;
    }

    public void setNumberOfAllThesis(Long numberOfAllThesis) {
        this.numberOfAllThesis = numberOfAllThesis;
    }

    public Long getNumberOfAllLectures() {
        return numberOfAllLectures;
    }

    public void setNumberOfAllLectures(Long numberOfAllLectures) {
        this.numberOfAllLectures = numberOfAllLectures;
    }

    public List<ItemTypeGroupWithCountsDto> getItemTypeGroupsWithCount() {
        return itemTypeGroupsWithCount;
    }

    public void setItemTypeGroupsWithCount(List<ItemTypeGroupWithCountsDto> itemTypeGroupsWithCount) {
        this.itemTypeGroupsWithCount = itemTypeGroupsWithCount;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public Long getNumberOfInvestmentProjects() {
        return numberOfInvestmentProjects;
    }

    public void setNumberOfInvestmentProjects(Long numberOfInvestmentProjects) {
        this.numberOfInvestmentProjects = numberOfInvestmentProjects;
    }

    public Long getNumberOfProjects() {
        return numberOfProjects;
    }

    public void setNumberOfProjects(Long numberOfProjects) {
        this.numberOfProjects = numberOfProjects;
    }

    public Long getInvestmentProjectAppearance() {
        return investmentProjectAppearance;
    }

    public void setInvestmentProjectAppearance(Long investmentProjectAppearance) {
        this.investmentProjectAppearance = investmentProjectAppearance;
    }

    public boolean isRetired() {
        return retired;
    }

    public void setRetired(boolean retired) {
        this.retired = retired;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public String getExpertCode() {
        return expertCode;
    }

    public void setExpertCode(String expertCode) {
        this.expertCode = expertCode;
    }

    public static class ItemTypeGroupWithCountsDto {

        private Long itemTypeGroupId;

        private Set<Long> itemTypeIds = new HashSet<>();

        private String itemTypeGroupTitle;

        private Long count = 0L;

        public Long getItemTypeGroupId() {
            return itemTypeGroupId;
        }

        public void setItemTypeGroupId(Long itemTypeGroupId) {
            this.itemTypeGroupId = itemTypeGroupId;
        }

        public Set<Long> getItemTypeIds() {
            return itemTypeIds;
        }

        public void setItemTypeIds(Set<Long> itemTypeIds) {
            this.itemTypeIds = itemTypeIds;
        }

        public String getItemTypeGroupTitle() {
            return itemTypeGroupTitle;
        }

        public void setItemTypeGroupTitle(String itemTypeGroupTitle) {
            this.itemTypeGroupTitle = itemTypeGroupTitle;
        }

        public Long getCount() {
            return count;
        }

        public void setCount(Long count) {
            this.count = count;
        }
    }
}
