package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.dto.ExpertClaimPreviewDto;

import java.io.IOException;
import java.util.List;

public interface ExpertClaimSearchService {

    String indexName = ExpertIndexService.indexName;

    List<ExpertClaimPreviewDto> findExpertsToClaim(Long userId) throws IOException;

    List<ExpertClaimPreviewDto> findExpertsToClaim(String query) throws IOException;
}
