package ai.unico.platform.search.api.service;

import ai.unico.platform.store.api.enums.WidgetTab;
import org.apache.lucene.analysis.core.StopAnalyzer;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

public interface OrganizationKeywordService {

    String itemIndexName = ItemIndexService.indexName;

    String projectIndexName = ProjectIndexService.indexName;

    String opportunityIndexName = OpportunityIndexService.indexName;

    Map<String, Long> findOrganizationKeywords(Set<Long> organizationIds) throws IOException;

    Map<String, Long> findOrganizationProjectKeywords(Set<Long> organizationIds) throws IOException;

    Map<String, Long> findOrganizationItemKeywords(Set<Long> organizationIds) throws IOException;

    Map<String, Long> findOrganizationOpportunityKeywords(Set<Long> organizationIds) throws IOException;

    Map<String, Long> suggestKeywords(Set<Long> organizationIds, Map<String, Long> searchHistory, Map<String, Long> widgetHistory, WidgetTab widgetTab) throws IOException;

    static void removeStopWords(Map<String, Long> keywords) {
        keywords.entrySet().removeIf(entry -> StopAnalyzer.ENGLISH_STOP_WORDS_SET.contains(entry.getKey().toLowerCase()));
    }

}
