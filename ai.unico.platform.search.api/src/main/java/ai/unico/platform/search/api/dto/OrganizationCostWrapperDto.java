package ai.unico.platform.search.api.dto;

import java.util.ArrayList;
import java.util.List;

public class OrganizationCostWrapperDto {
    private Integer year;

    private Double others;

    private List<OrganizationCostDto> values = new ArrayList<>();


    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Double getOthers() {
        return others;
    }

    public void setOthers(Double others) {
        this.others = others;
    }

    public List<OrganizationCostDto> getValues() {
        return values;
    }

    public void setValues(List<OrganizationCostDto> values) {
        this.values = values;
    }
}
