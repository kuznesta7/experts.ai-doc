package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.wrapper.ExpertProjectWrapper;

import java.io.IOException;
import java.util.Set;

public interface ProjectSearchService {
    String indexName = ProjectIndexService.indexName;

    Float minScore = 4.0F;

    ExpertProjectWrapper findExpertProjects(String expertCode, ExpertSearchResultFilter expertSearchResultFilter) throws IOException;

    ProjectSearchService.ExpertProjectStatistics findExpertProjectStatistics(String expertCode, Set<Long> organizationIds, boolean countConfidential) throws IOException;


    class ExpertProjectStatistics {

        private String expertCode;

        private Long numberOfInvestmentProjects;

        public String getExpertCode() {
            return expertCode;
        }

        public void setExpertCode(String expertCode) {
            this.expertCode = expertCode;
        }

        public Long getNumberOfInvestmentProjects() {
            return numberOfInvestmentProjects;
        }

        public void setNumberOfInvestmentProjects(Long numberOfInvestmentProjects) {
            this.numberOfInvestmentProjects = numberOfInvestmentProjects;
        }
    }
}
