package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.dto.ItemLookupPreviewDto;
import ai.unico.platform.search.api.filter.ItemLookupFilter;
import ai.unico.platform.store.api.dto.ItemDetailDto;
import ai.unico.platform.store.api.dto.ItemRegistrationDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface ItemLookupService {

    String indexName = ItemIndexService.indexName;

    ItemDetailDto findItemDetail(UserContext userContext, String itemCode) throws IOException, NonexistentEntityException;

    ItemRegistrationDto findOriginalItem(Long originalItemId) throws IOException, NonexistentEntityException;

    List<ItemRegistrationDto> findItemsForExpertToClaim(Long expertId) throws IOException;

    List<ItemLookupPreviewDto> lookupItems(ItemLookupFilter itemLookupFilter) throws IOException;

    List<ItemLookupPreviewDto> findItems(Set<String> itemCodes) throws IOException;

}
