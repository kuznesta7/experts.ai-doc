package ai.unico.platform.search.api.filter;

import java.util.HashSet;
import java.util.Set;

public class ItemLookupFilter {

    private String query;

    private Set<Long> organizationIds = new HashSet();

    private Set<Long> disallowedOrganizationIds = new HashSet();

    private boolean approvedOnly = false;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public boolean isApprovedOnly() {
        return approvedOnly;
    }

    public void setApprovedOnly(boolean approvedOnly) {
        this.approvedOnly = approvedOnly;
    }

    public Set<Long> getDisallowedOrganizationIds() {
        return disallowedOrganizationIds;
    }

    public void setDisallowedOrganizationIds(Set<Long> disallowedOrganizationIds) {
        this.disallowedOrganizationIds = disallowedOrganizationIds;
    }
}
