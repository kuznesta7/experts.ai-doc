package ai.unico.platform.search.api.dto;

import ai.unico.platform.store.api.dto.CommercializationCategoryPreviewDto;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class InvestmentProjectPublicPreviewDto {

    private String id;

    private String name;

    private String imgCheckSum;

    private String executiveSummary;

    private List<String> keywords;

    private Integer investmentFrom;

    private Integer investmentTo;

    private String statusName;

    private String domainName;

    private Set<CommercializationCategoryPreviewDto> categories = new HashSet<>();

    private Long statusId;

    private Long domainId;

    private Double relevancy;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExecutiveSummary() {
        return executiveSummary;
    }

    public void setExecutiveSummary(String executiveSummary) {
        this.executiveSummary = executiveSummary;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Double getRelevancy() {
        return relevancy;
    }

    public void setRelevancy(Double relevancy) {
        this.relevancy = relevancy;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Integer getInvestmentFrom() {
        return investmentFrom;
    }

    public void setInvestmentFrom(Integer investmentFrom) {
        this.investmentFrom = investmentFrom;
    }

    public Integer getInvestmentTo() {
        return investmentTo;
    }

    public void setInvestmentTo(Integer investmentTo) {
        this.investmentTo = investmentTo;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public Set<CommercializationCategoryPreviewDto> getCategories() {
        return categories;
    }

    public void setCategories(Set<CommercializationCategoryPreviewDto> categories) {
        this.categories = categories;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getDomainId() {
        return domainId;
    }

    public void setDomainId(Long domainId) {
        this.domainId = domainId;
    }

    public String getImgCheckSum() {
        return imgCheckSum;
    }

    public void setImgCheckSum(String imgCheckSum) {
        this.imgCheckSum = imgCheckSum;
    }
}
