package ai.unico.platform.search.api.service;

import ai.unico.platform.store.api.dto.ItemIndexDto;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.Map;

public interface ThesisIndexService {

    String indexName = "thesis";

    void reindexThesis(Integer limit, boolean clean, String target, UserContext userContext) throws IOException;

    void indexThesis(ItemIndexDto itemIndexDto) throws IOException;

    void replaceExperts(Map<String, String> replacementMap, boolean originalOnly) throws IOException;

    void flush() throws IOException;

}
