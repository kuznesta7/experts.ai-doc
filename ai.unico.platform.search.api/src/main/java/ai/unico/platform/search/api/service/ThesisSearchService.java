package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.dto.ThesisPreviewDto;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.wrapper.ThesisWrapper;

import java.io.IOException;
import java.util.Set;

public interface ThesisSearchService {
    String indexName = ThesisIndexService.indexName;

    ThesisWrapper findThesis(EvidenceFilter evidenceFilter, ExpertSearchResultFilter.Order order) throws IOException;

    ThesisPreviewDto getThesis(String thesisCode) throws IOException;

    ExpertThesisStatistics findExpertThesisStatistics(String expertCode, Set<Long> organizationIds, boolean countConfidential) throws IOException;

    class ExpertThesisStatistics {

        private String expertCode;

        private Long numberOfThesis;

        public String getExpertCode() {
            return expertCode;
        }

        public void setExpertCode(String expertCode) {
            this.expertCode = expertCode;
        }

        public Long getNumberOfThesis() {
            return numberOfThesis;
        }

        public void setNumberOfThesis(Long numberOfThesis) {
            this.numberOfThesis = numberOfThesis;
        }
    }
}
