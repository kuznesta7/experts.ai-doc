package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.dto.LecturePreviewDto;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.wrapper.LectureWrapper;

import java.io.IOException;
import java.util.Set;

public interface LectureSearchService {
    String indexName = LectureIndexService.indexName;

    LectureWrapper findLecture(EvidenceFilter evidenceFilter, ExpertSearchResultFilter.Order order) throws IOException;

    LecturePreviewDto getLecture(String lectureCode) throws IOException;

    LectureSearchService.ExpertLectureStatistics findExpertLectureStatistics(String expertCode, Set<Long> organizationIds, boolean countConfidential) throws IOException;

    class ExpertLectureStatistics {

        private String expertCode;

        private Long numberOfLecture;

        public String getExpertCode() {
            return expertCode;
        }

        public void setExpertCode(String expertCode) {
            this.expertCode = expertCode;
        }

        public Long getNumberOfLecture() {
            return numberOfLecture;
        }

        public void setNumberOfLecture(Long numberOfLecture) {
            this.numberOfLecture = numberOfLecture;
        }
    }

}
