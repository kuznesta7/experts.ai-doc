package ai.unico.platform.search.api.filter;

import java.util.Set;

public interface OrganizationEvidenceFilter {

    void setOrganizationId(Long organizationId);

    Long getOrganizationId();

    void setOrganizationIds(Set<Long> organizationIds);

    Set<Long> getOrganizationIds();

}
