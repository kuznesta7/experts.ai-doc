package ai.unico.platform.search.api.dto;

import ai.unico.platform.util.dto.OrganizationDto;

import java.util.ArrayList;
import java.util.List;

public class UserProfileSearchPreviewDto {

    private Long userId;

    private Long expertId;

    private String code;

    private String name;

    private List<OrganizationDto> organizationDtos = new ArrayList<>();

    private Double rank;

    private Long appearance;

    private Long investmentProjectAppearance;

    private boolean verified;

    private boolean claimable;

    private boolean retired;

    private ProfileFollow profileFollow;

    public boolean isClaimable() {
        return claimable;
    }

    public void setClaimable(boolean claimable) {
        this.claimable = claimable;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<OrganizationDto> getOrganizationDtos() {
        return organizationDtos;
    }

    public void setOrganizationDtos(List<OrganizationDto> organizationDtos) {
        this.organizationDtos = organizationDtos;
    }

    public Double getRank() {
        return rank;
    }

    public void setRank(Double rank) {
        this.rank = rank;
    }

    public Long getAppearance() {
        return appearance;
    }

    public void setAppearance(Long appearance) {
        this.appearance = appearance;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public Long getInvestmentProjectAppearance() {
        return investmentProjectAppearance;
    }

    public void setInvestmentProjectAppearance(Long investmentProjectAppearance) {
        this.investmentProjectAppearance = investmentProjectAppearance;
    }

    public boolean isRetired() {
        return retired;
    }

    public void setRetired(boolean retired) {
        this.retired = retired;
    }

    public ProfileFollow getProfileFollow() {
        return profileFollow;
    }

    public void setProfileFollow(ProfileFollow profileFollow) {
        this.profileFollow = profileFollow;
    }

    public static class ProfileFollow {

        private List<String> workspaces;

        public ProfileFollow(List<String> workspaces) {
            this.workspaces = workspaces;
        }

        public List<String> getWorkspaces() {
            return workspaces;
        }

        public void setWorkspaces(List<String> workspaces) {
            this.workspaces = workspaces;
        }
    }
}
