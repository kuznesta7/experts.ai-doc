package ai.unico.platform.search.api.dto;

import java.util.HashSet;
import java.util.Set;

public class OrganizationExpertsDto {
    private Long organizationId;
    private Set<Long> userIds = new HashSet<>();
    private Set<Long> expertIds = new HashSet<>();

    public Set<Long> getUserIds() {
        return userIds;
    }

    public Set<Long> getExpertIds() {
        return expertIds;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }
}
