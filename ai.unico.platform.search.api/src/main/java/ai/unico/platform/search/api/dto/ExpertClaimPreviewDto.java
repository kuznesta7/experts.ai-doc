package ai.unico.platform.search.api.dto;

import ai.unico.platform.store.api.dto.OrganizationBaseDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ExpertClaimPreviewDto {

    private Long expertId;

    private Long userId;

    private String expertCode;

    private String fullName;

    private Set<OrganizationBaseDto> organizationBaseDtos;

    private Long numberOfAllItems;

    private List<EvidenceItemPreviewWithExpertsDto> itemPreviewDtos = new ArrayList<>();

    private boolean claimed;

    private boolean claimedByAnotherUser;

    public String getExpertCode() {
        return expertCode;
    }

    public void setExpertCode(String expertCode) {
        this.expertCode = expertCode;
    }

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Set<OrganizationBaseDto> getOrganizationBaseDtos() {
        return organizationBaseDtos;
    }

    public void setOrganizationBaseDtos(Set<OrganizationBaseDto> organizationBaseDtos) {
        this.organizationBaseDtos = organizationBaseDtos;
    }

    public boolean isClaimed() {
        return claimed;
    }

    public void setClaimed(boolean claimed) {
        this.claimed = claimed;
    }

    public boolean isClaimedByAnotherUser() {
        return claimedByAnotherUser;
    }

    public void setClaimedByAnotherUser(boolean claimedByAnotherUser) {
        this.claimedByAnotherUser = claimedByAnotherUser;
    }

    public List<EvidenceItemPreviewWithExpertsDto> getItemPreviewDtos() {
        return itemPreviewDtos;
    }

    public void setItemPreviewDtos(List<EvidenceItemPreviewWithExpertsDto> itemPreviewDtos) {
        this.itemPreviewDtos = itemPreviewDtos;
    }

    public Long getNumberOfAllItems() {
        return numberOfAllItems;
    }

    public void setNumberOfAllItems(Long numberOfAllItems) {
        this.numberOfAllItems = numberOfAllItems;
    }
}
