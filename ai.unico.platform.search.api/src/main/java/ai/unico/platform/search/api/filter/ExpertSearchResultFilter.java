package ai.unico.platform.search.api.filter;

public class ExpertSearchResultFilter extends ExpertSearchFilter {

    public static final Integer ITEMS_LIMIT_BASE = 5;

    public static final Integer INVESTMENT_PROJECTS_LIMIT_BASE = 5;

    public static final Integer PROJECT_LIMIT_BASE = 5;

    private Integer itemsPage = 1;

    private Integer itemsLimit = ITEMS_LIMIT_BASE;

    private Integer investmentProjectsPage = 1;

    private Integer investmentProjectsLimit = INVESTMENT_PROJECTS_LIMIT_BASE;

    private Integer projectPage = 1;

    private Integer projectLimit = PROJECT_LIMIT_BASE;

    private boolean forceFilterItemTypes = false;

    private boolean showHidden = false;

    private Order orderBy = Order.QUALITY;

    public Integer getProjectPage() {
        return projectPage;
    }

    public void setProjectPage(Integer projectPage) {
        this.projectPage = projectPage;
    }

    public Integer getProjectLimit() {
        return projectLimit;
    }

    public void setProjectLimit(Integer projectLimit) {
        this.projectLimit = projectLimit;
    }

    public Integer getItemsPage() {
        return itemsPage;
    }

    public void setItemsPage(Integer itemsPage) {
        this.itemsPage = itemsPage;
    }

    public Integer getItemsLimit() {
        return itemsLimit;
    }

    public void setItemsLimit(Integer itemsLimit) {
        this.itemsLimit = itemsLimit;
    }

    public boolean isForceFilterItemTypes() {
        return forceFilterItemTypes;
    }

    public void setForceFilterItemTypes(boolean forceFilterItemTypes) {
        this.forceFilterItemTypes = forceFilterItemTypes;
    }

    public boolean isShowHidden() {
        return showHidden;
    }

    public void setShowHidden(boolean showHidden) {
        this.showHidden = showHidden;
    }

    public Order getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Order orderBy) {
        this.orderBy = orderBy;
    }

    public Integer getInvestmentProjectsPage() {
        return investmentProjectsPage;
    }

    public void setInvestmentProjectsPage(Integer investmentProjectsPage) {
        this.investmentProjectsPage = investmentProjectsPage;
    }

    public Integer getInvestmentProjectsLimit() {
        return investmentProjectsLimit;
    }

    public void setInvestmentProjectsLimit(Integer investmentProjectsLimit) {
        this.investmentProjectsLimit = investmentProjectsLimit;
    }

    public static enum Order {
        QUALITY, YEAR_DESC, YEAR_ASC
    }
}
