package ai.unico.platform.search.api.dto;

import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.dto.UserExpertBaseDto;
import ai.unico.platform.util.enums.Confidentiality;

import java.util.ArrayList;
import java.util.List;

public class ThesisPreviewDto {

    private String thesisCode;

    private Long thesisId;

    private String thesisName;

    private String thesisDescription;

    private Long thesisTypeId;

    private Integer year;

    private Confidentiality confidentiality;

    private UserExpertBaseDto assignee;

    private UserExpertBaseDto supervisor;

    private UserExpertBaseDto reviewer;

    private OrganizationBaseDto university;

    private List<String> keywords = new ArrayList<>();

    public String getThesisCode() {
        return thesisCode;
    }

    public void setThesisCode(String thesisCode) {
        this.thesisCode = thesisCode;
    }

    public Long getThesisId() {
        return thesisId;
    }

    public void setThesisId(Long thesisId) {
        this.thesisId = thesisId;
    }

    public String getThesisName() {
        return thesisName;
    }

    public void setThesisName(String thesisName) {
        this.thesisName = thesisName;
    }

    public String getThesisDescription() {
        return thesisDescription;
    }

    public void setThesisDescription(String thesisDescription) {
        this.thesisDescription = thesisDescription;
    }

    public Long getThesisTypeId() {
        return thesisTypeId;
    }

    public void setThesisTypeId(Long thesisTypeId) {
        this.thesisTypeId = thesisTypeId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Confidentiality getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Confidentiality confidentiality) {
        this.confidentiality = confidentiality;
    }

    public UserExpertBaseDto getAssignee() {
        return assignee;
    }

    public void setAssignee(UserExpertBaseDto assignee) {
        this.assignee = assignee;
    }

    public UserExpertBaseDto getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(UserExpertBaseDto supervisor) {
        this.supervisor = supervisor;
    }

    public UserExpertBaseDto getReviewer() {
        return reviewer;
    }

    public void setReviewer(UserExpertBaseDto reviewer) {
        this.reviewer = reviewer;
    }

    public OrganizationBaseDto getUniversity() {
        return university;
    }

    public void setUniversity(OrganizationBaseDto university) {
        this.university = university;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }
}
