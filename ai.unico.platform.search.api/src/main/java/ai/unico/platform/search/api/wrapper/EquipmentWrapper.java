package ai.unico.platform.search.api.wrapper;

import ai.unico.platform.search.api.dto.EquipmentPreviewDto;
import ai.unico.platform.store.api.dto.MultilingualDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class EquipmentWrapper {
    private Long numberOfAllItems;

    private Integer limit;

    private Integer page;

    private Long organizationId;

    private Set<Long> organizationUnitIds;

    private List<EquipmentPreviewDto> equipmentPreviewDtos = new ArrayList<>();  // todo - delete

    private List<MultilingualDto<EquipmentPreviewDto>> multilingualPreviews = new ArrayList<>();

    public Long getNumberOfAllItems() {
        return numberOfAllItems;
    }

    public void setNumberOfAllItems(Long numberOfAllItems) {
        this.numberOfAllItems = numberOfAllItems;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Set<Long> getOrganizationUnitIds() {
        return organizationUnitIds;
    }

    public void setOrganizationUnitIds(Set<Long> organizationUnitIds) {
        this.organizationUnitIds = organizationUnitIds;
    }

    public List<MultilingualDto<EquipmentPreviewDto>> getMultilingualPreviews() {
        return multilingualPreviews;
    }

    public void setMultilingualPreviews(List<MultilingualDto<EquipmentPreviewDto>> multilingualPreviews) {
        this.multilingualPreviews = multilingualPreviews;
    }

    public List<EquipmentPreviewDto> getEquipmentPreviewDtos() {
        return equipmentPreviewDtos;
    }

    public void setEquipmentPreviewDtos(List<EquipmentPreviewDto> equipmentPreviewDtos) {
        this.equipmentPreviewDtos = equipmentPreviewDtos;
    }
}
