package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.dto.OntologyItemSearchDto;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.filter.ExpertSearchFilter;

import java.io.IOException;
import java.util.List;

public interface OntologySearchService {

    void fillItemSearchOntology(ExpertSearchFilter expertSearchFilter);

    void fillItemSearchOntology(EvidenceFilter evidenceFilter);

    List<OntologyItemSearchDto> searchOntology(String query, Double relevancyMin, Integer resultMax) throws IOException;

}
