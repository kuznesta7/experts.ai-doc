package ai.unico.platform.search.api.dto;

import ai.unico.platform.store.api.dto.OrganizationBaseDto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EvidenceExpertPreviewDto {

    private String expertCode;

    private Long userId;

    private Long expertId;

    private String name;

    private Long numberOfItems = 0L;

    private Long numberOfProjects;

    private Long numberOfCommercializationProjects;

    private List<OrganizationBaseDto> organizationBaseDtos;

    private List<OrganizationBaseDto> visibleOrganizationBaseDtos;

    private List<OrganizationBaseDto> activeOrganizationBaseDtos;

    private boolean verified;

    private boolean unitVerified;

    private boolean retired;

    private boolean retiredInOrganization;

    private boolean claimable;

    private String invitationEmail;

    private List<String> keywords = new ArrayList<>();

    private Map<Long, Long> itemTypeGroupCounts = new HashMap<>();

    private Map<Integer, Long> yearActivityHistogram = new HashMap<>();

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<Long, Long> getItemTypeGroupCounts() {
        return itemTypeGroupCounts;
    }

    public void setItemTypeGroupCounts(Map<Long, Long> itemTypeGroupCounts) {
        this.itemTypeGroupCounts = itemTypeGroupCounts;
    }

    public Long getNumberOfItems() {
        return numberOfItems;
    }

    public void setNumberOfItems(Long numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    public Long getNumberOfProjects() {
        return numberOfProjects;
    }

    public void setNumberOfProjects(Long numberOfProjects) {
        this.numberOfProjects = numberOfProjects;
    }

    public Long getNumberOfCommercializationProjects() {
        return numberOfCommercializationProjects;
    }

    public void setNumberOfCommercializationProjects(Long numberOfCommercializationProjects) {
        this.numberOfCommercializationProjects = numberOfCommercializationProjects;
    }

    public String getExpertCode() {
        return expertCode;
    }

    public void setExpertCode(String expertCode) {
        this.expertCode = expertCode;
    }

    public Map<Integer, Long> getYearActivityHistogram() {
        return yearActivityHistogram;
    }

    public void setYearActivityHistogram(Map<Integer, Long> yearActivityHistogram) {
        this.yearActivityHistogram = yearActivityHistogram;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public boolean isUnitVerified() {
        return unitVerified;
    }

    public void setUnitVerified(boolean unitVerified) {
        this.unitVerified = unitVerified;
    }

    public List<OrganizationBaseDto> getOrganizationBaseDtos() {
        return organizationBaseDtos;
    }

    public void setOrganizationBaseDtos(List<OrganizationBaseDto> organizationBaseDtos) {
        this.organizationBaseDtos = organizationBaseDtos;
    }

    public boolean isRetired() {
        return retired;
    }

    public void setRetired(boolean retired) {
        this.retired = retired;
    }

    public boolean isRetiredInOrganization() {
        return retiredInOrganization;
    }

    public void setRetiredInOrganization(boolean retiredInOrganization) {
        this.retiredInOrganization = retiredInOrganization;
    }

    public boolean isClaimable() {
        return claimable;
    }

    public void setClaimable(boolean claimable) {
        this.claimable = claimable;
    }

    public String getInvitationEmail() {
        return invitationEmail;
    }

    public void setInvitationEmail(String invitationEmail) {
        this.invitationEmail = invitationEmail;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public List<OrganizationBaseDto> getVisibleOrganizationBaseDtos() {
        return visibleOrganizationBaseDtos;
    }

    public void setVisibleOrganizationBaseDtos(List<OrganizationBaseDto> visibleOrganizationBaseDtos) {
        this.visibleOrganizationBaseDtos = visibleOrganizationBaseDtos;
    }

    public List<OrganizationBaseDto> getActiveOrganizationBaseDtos() {
        return activeOrganizationBaseDtos;
    }

    public void setActiveOrganizationBaseDtos(List<OrganizationBaseDto> activeOrganizationBaseDtos) {
        this.activeOrganizationBaseDtos = activeOrganizationBaseDtos;
    }
}
