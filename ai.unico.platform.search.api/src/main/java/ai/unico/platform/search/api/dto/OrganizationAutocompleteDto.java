package ai.unico.platform.search.api.dto;

public class OrganizationAutocompleteDto {

    private Long organizationId;

    private String organizationAbbrev;

    private String organizationName;

    private String countryCode;

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationAbbrev() {
        return organizationAbbrev;
    }

    public void setOrganizationAbbrev(String organizationAbbrev) {
        this.organizationAbbrev = organizationAbbrev;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
