package ai.unico.platform.search.api.wrapper;

import ai.unico.platform.store.api.dto.OrganizationBaseDto;

import java.util.ArrayList;
import java.util.List;

public class OrganizationWrapper {

    private Long numberOfAllItems = 0L;

    private Integer limit = 0;

    private Integer page = 0;

    private List<OrganizationBaseDto> organizationBaseDtos = new ArrayList<>();

    public Long getNumberOfAllItems() {
        return numberOfAllItems;
    }

    public void setNumberOfAllItems(Long numberOfAllItems) {
        this.numberOfAllItems = numberOfAllItems;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<OrganizationBaseDto> getOrganizationBaseDtos() {
        return organizationBaseDtos;
    }

    public void setOrganizationBaseDtos(List<OrganizationBaseDto> organizationBaseDtos) {
        this.organizationBaseDtos = organizationBaseDtos;
    }
}
