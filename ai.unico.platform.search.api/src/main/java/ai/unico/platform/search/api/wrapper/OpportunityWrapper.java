package ai.unico.platform.search.api.wrapper;

import ai.unico.platform.recommendation.api.dto.RecommendationWrapperDto;
import ai.unico.platform.search.api.dto.OpportunityPreviewDto;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

public class OpportunityWrapper {
    private Long numberOfAllItems;

    private Integer limit;

    private Integer page;

    private Long organizationId;

    private Set<Long> organizationUnitIds;

    private RecommendationWrapperDto recommendationWrapperDto = new RecommendationWrapperDto();

    private List<OpportunityPreviewDto> opportunityPreviewDtos = new ArrayList<>();

    public Long getNumberOfAllItems() {
        return numberOfAllItems;
    }

    public void setNumberOfAllItems(Long numberOfAllItems) {
        this.numberOfAllItems = numberOfAllItems;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Set<Long> getOrganizationUnitIds() {
        return organizationUnitIds;
    }

    public void setOrganizationUnitIds(Set<Long> organizationUnitIds) {
        this.organizationUnitIds = organizationUnitIds;
    }

    public List<OpportunityPreviewDto> getOpportunityPreviewDtos() {
        return opportunityPreviewDtos;
    }

    public void setOpportunityPreviewDtos(List<OpportunityPreviewDto> opportunityPreviewDtos) {
        this.opportunityPreviewDtos = opportunityPreviewDtos;
    }

    public RecommendationWrapperDto getRecommendationWrapperDto() {
        return recommendationWrapperDto;
    }

    public void setRecommendationWrapperDto(RecommendationWrapperDto recommendationWrapperDto) {
        this.recommendationWrapperDto = recommendationWrapperDto;
    }

    public void prependOpportunityPreviewDtos(List<OpportunityPreviewDto> opportunityPreviews, List<String> order, Integer limit){
        List<String> newOrder = new ArrayList<>(order);
        this.opportunityPreviewDtos.addAll(opportunityPreviews);
        for(OpportunityPreviewDto opportunityPreviewDto: this.opportunityPreviewDtos){
            newOrder.add(opportunityPreviewDto.getOpportunityId().toString());
        }
        this.opportunityPreviewDtos.sort(Comparator.comparing(item -> newOrder.indexOf(item.getOpportunityId().toString())));
        if(this.opportunityPreviewDtos.size() > limit){
            this.opportunityPreviewDtos = this.opportunityPreviewDtos.subList(0, limit);
        }
    }

    public void shuffleOpportunityPreviewDtos() {
        //Collections.shuffle(opportunityPreviewDtos);
    }

    public void sortOpportunitPreviewDtos(List<String> order){
        this.opportunityPreviewDtos.sort(Comparator.comparing(item -> {
            int res = order.indexOf(item.getOpportunityId().toString());
            if(res == -1){
                return Integer.MAX_VALUE;
            }
            return res;
        }));
    }
}
