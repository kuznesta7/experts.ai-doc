package ai.unico.platform.search.api.dto;

import java.util.ArrayList;
import java.util.List;

public class EvidenceExpertPreviewWithItemsDto extends EvidenceExpertPreviewDto {

    private List<EvidenceItemPreviewDto> expertItemPreviewDtos = new ArrayList<>();

    public List<EvidenceItemPreviewDto> getExpertItemPreviewDtos() {
        return expertItemPreviewDtos;
    }

    public void setExpertItemPreviewDtos(List<EvidenceItemPreviewDto> expertItemPreviewDtos) {
        this.expertItemPreviewDtos = expertItemPreviewDtos;
    }
}
