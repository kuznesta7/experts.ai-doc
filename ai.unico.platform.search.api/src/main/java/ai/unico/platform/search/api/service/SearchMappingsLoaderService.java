package ai.unico.platform.search.api.service;

public interface SearchMappingsLoaderService {

    String loadMappings(String key);

}
