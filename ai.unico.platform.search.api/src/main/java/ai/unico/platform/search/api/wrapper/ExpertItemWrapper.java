package ai.unico.platform.search.api.wrapper;

import ai.unico.platform.search.api.dto.ItemSearchPreviewDto;

import java.util.ArrayList;
import java.util.List;

public class ExpertItemWrapper {

    private Long numberOfRelevantItems;

    private Double scoreSum;

    private Integer limit;

    private Integer page;

    private List<ItemSearchPreviewDto> itemSearchPreviewDtos = new ArrayList<>();

    public Long getNumberOfRelevantItems() {
        return numberOfRelevantItems;
    }

    public Double getScoreSum() {
        return scoreSum;
    }

    public void setScoreSum(Double scoreSum) {
        this.scoreSum = scoreSum;
    }

    public void setNumberOfRelevantItems(Long numberOfRelevantItems) {
        this.numberOfRelevantItems = numberOfRelevantItems;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<ItemSearchPreviewDto> getItemSearchPreviewDtos() {
        return itemSearchPreviewDtos;
    }

    public void setItemSearchPreviewDtos(List<ItemSearchPreviewDto> itemSearchPreviewDtos) {
        this.itemSearchPreviewDtos = itemSearchPreviewDtos;
    }
}
