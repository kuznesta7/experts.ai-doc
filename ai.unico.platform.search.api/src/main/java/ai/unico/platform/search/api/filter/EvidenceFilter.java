package ai.unico.platform.search.api.filter;

import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.enums.ExpertSearchType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class EvidenceFilter extends BaseFilter implements OrganizationEvidenceFilter, RecommendationFilter {

    private String user;

    private Integer limit = 100;

    private Long organizationId;

    private Set<Long> organizationIds = new HashSet<>();

    private Set<Long> suborganizationIds = new HashSet<>();

    private Set<Long> affiliatedOrganizationIds = new HashSet<>();

    private Set<Long> resultTypeGroupIds = new HashSet<>();

    private Set<Long> resultTypeIds = new HashSet<>();

    private Set<String> expertCodes = new HashSet<>();

    private Set<Long> participatingOrganizationIds = new HashSet<>();

    private Integer yearFrom;

    private Integer yearTo;

    private boolean ontologyDisabled = false;

    private boolean sortByFullName = false;

    private boolean verifiedOnly;

    private boolean notVerifiedOnly;

    private boolean retired;

    private boolean restrictOrganization;

    private boolean organizationRelatedStatistics;

    private Set<Confidentiality> confidentiality = new HashSet<>();

    private Map<String, Float> alternateQueries = new HashMap<>();

    private ExpertSearchType expertSearchType;

    private Set<String> countryCodes = new HashSet<>();

    private boolean ignoreVerifications = false;

    private boolean retirementIgnored = true;

    private String recommId;

    private boolean activeOnly = true;

    @Override
    public Integer getLimit() {
        return limit;
    }

    @Override
    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationId) {
        this.organizationIds = organizationId;
    }

    public Set<Long> getSuborganizationIds() {
        return suborganizationIds;
    }

    public void setSuborganizationIds(Set<Long> suborganizationIds) {
        this.suborganizationIds = suborganizationIds;
    }

    public Set<Long> getResultTypeGroupIds() {
        return resultTypeGroupIds;
    }

    public void setResultTypeGroupIds(Set<Long> resultTypeGroupIds) {
        this.resultTypeGroupIds = resultTypeGroupIds;
    }

    public Set<Long> getResultTypeIds() {
        return resultTypeIds;
    }

    public void setResultTypeIds(Set<Long> resultTypeIds) {
        this.resultTypeIds = resultTypeIds;
    }

    public Integer getYearFrom() {
        return yearFrom;
    }

    public void setYearFrom(Integer yearFrom) {
        this.yearFrom = yearFrom;
    }

    public Integer getYearTo() {
        return yearTo;
    }

    public void setYearTo(Integer yearTo) {
        this.yearTo = yearTo;
    }

    public boolean isOntologyDisabled() {
        return ontologyDisabled;
    }

    public void setOntologyDisabled(boolean ontologyDisabled) {
        this.ontologyDisabled = ontologyDisabled;
    }

    public boolean isVerifiedOnly() {
        return verifiedOnly;
    }

    public void setVerifiedOnly(boolean verifiedOnly) {
        this.verifiedOnly = verifiedOnly;
    }

    public boolean isNotVerifiedOnly() {
        return notVerifiedOnly;
    }

    public void setNotVerifiedOnly(boolean notVerifiedOnly) {
        this.notVerifiedOnly = notVerifiedOnly;
    }

    public boolean isRetired() { return retired; }

    public void setRetired(boolean retired) { this.retired = retired; }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public boolean isRestrictOrganization() {
        return restrictOrganization;
    }

    public void setRestrictOrganization(boolean restrictOrganization) {
        this.restrictOrganization = restrictOrganization;
    }

    public Set<Confidentiality> getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Set<Confidentiality> confidentiality) {
        this.confidentiality = confidentiality;
    }

    public boolean isOrganizationRelatedStatistics() {
        return organizationRelatedStatistics;
    }

    public void setOrganizationRelatedStatistics(boolean organizationRelatedStatistics) {
        this.organizationRelatedStatistics = organizationRelatedStatistics;
    }

    public Set<Long> getAffiliatedOrganizationIds() {
        return affiliatedOrganizationIds;
    }

    public void setAffiliatedOrganizationIds(Set<Long> affiliatedOrganizationIds) {
        this.affiliatedOrganizationIds = affiliatedOrganizationIds;
    }

    public Set<String> getExpertCodes() {
        return expertCodes;
    }

    public void setExpertCodes(Set<String> expertCodes) {
        this.expertCodes = expertCodes;
    }

    public Set<Long> getParticipatingOrganizationIds() {
        return participatingOrganizationIds;
    }

    public void setParticipatingOrganizationIds(Set<Long> participatingOrganizationIds) {
        this.participatingOrganizationIds = participatingOrganizationIds;
    }

    public Map<String, Float> getAlternateQueries() {
        return alternateQueries;
    }

    public void setAlternateQueries(Map<String, Float> alternateQueries) {
        this.alternateQueries = alternateQueries;
    }

    public ExpertSearchType getExpertSearchType() {
        return expertSearchType;
    }

    public void setExpertSearchType(ExpertSearchType expertSearchType) {
        this.expertSearchType = expertSearchType;
    }

    public Set<String> getCountryCodes() {
        return countryCodes;
    }

    public void setCountryCodes(Set<String> countryCodes) {
        this.countryCodes = countryCodes;
    }

    public boolean getSortByFullName() {
        return sortByFullName;
    }

    public void setSortByFullName(boolean sortByFullName) {
        this.sortByFullName = sortByFullName;
    }

    public boolean isIgnoreVerifications() {
        return ignoreVerifications;
    }

    public void setIgnoreVerifications(boolean ignoreVerifications) {
        this.ignoreVerifications = ignoreVerifications;
    }

    public boolean isRetirementIgnored() {
        return retirementIgnored;
    }

    public void setRetirementIgnored(boolean retirementIgnored) {
        this.retirementIgnored = retirementIgnored;
    }

    public String getRecommId() {
        return recommId;
    }

    public void setRecommId(String recommId) {
        this.recommId = recommId;
    }

    public void setActiveOnly(boolean activeOnly){ this.activeOnly = activeOnly;}

    public boolean isActiveOnly(){
        return this.activeOnly;
    }

    @Override
    public String getUser() {
        return user;
    }

    @Override
    public void setUser(String user) {
        this.user = user;
    }
}
