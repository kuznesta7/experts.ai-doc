package ai.unico.platform.search.api.filter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ExpertSearchFilter extends BaseFilter {

    public static final Integer LIMIT_BASE = 5;

    public ExpertSearchFilter() {
        super.setLimit(LIMIT_BASE);
    }

    private Set<Long> organizationIds = new HashSet<>();

    private Set<Long> resultTypeGroupIds = new HashSet<>();

    private Set<Long> resultTypeIds = new HashSet<>();

    private Set<String> countryCodes = new HashSet<>();

    private boolean excludeInvestProjects;

    private boolean excludeOutcomes;

    private Integer yearFrom;

    private Integer yearTo;

    private String fullNameQuery;

    private List<String> secondaryQueries = new ArrayList<>();

    private List<String> disabledSecondaryQueries = new ArrayList<>();

    private List<Float> secondaryWeights = new ArrayList<>();

    private List<Float> disabledSecondaryWeights = new ArrayList<>();

    private boolean ontologyDisabled = false;

    private Set<String> availableExpertCodes;

    private boolean demoActive = false;

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizations) {
        this.organizationIds = organizations;
    }

    public Set<Long> getResultTypeIds() {
        return resultTypeIds;
    }

    public void setResultTypeIds(Set<Long> resultTypeIds) {
        this.resultTypeIds = resultTypeIds;
    }

    public Integer getYearFrom() {
        return yearFrom;
    }

    public void setYearFrom(Integer yearFrom) {
        this.yearFrom = yearFrom;
    }

    public Integer getYearTo() {
        return yearTo;
    }

    public void setYearTo(Integer yearTo) {
        this.yearTo = yearTo;
    }

    public Set<Long> getResultTypeGroupIds() {
        return resultTypeGroupIds;
    }

    public void setResultTypeGroupIds(Set<Long> resultTypeGroupIds) {
        this.resultTypeGroupIds = resultTypeGroupIds;
    }

    public String getFullNameQuery() {
        return fullNameQuery;
    }

    public void setFullNameQuery(String fullNameQuery) {
        this.fullNameQuery = fullNameQuery;
    }

    public List<String> getSecondaryQueries() {
        return secondaryQueries;
    }

    public void setSecondaryQueries(List<String> secondaryQueries) {
        this.secondaryQueries = secondaryQueries;
    }

    public List<Float> getSecondaryWeights() {
        return secondaryWeights;
    }

    public void setSecondaryWeights(List<Float> secondaryWeights) {
        this.secondaryWeights = secondaryWeights;
    }

    public boolean isOntologyDisabled() {
        return ontologyDisabled;
    }

    public void setOntologyDisabled(boolean ontologyDisabled) {
        this.ontologyDisabled = ontologyDisabled;
    }

    public Set<String> getAvailableExpertCodes() {
        return availableExpertCodes;
    }

    public void setAvailableExpertCodes(Set<String> availableExpertCodes) {
        this.availableExpertCodes = availableExpertCodes;
    }

    public List<String> getDisabledSecondaryQueries() {
        return disabledSecondaryQueries;
    }

    public void setDisabledSecondaryQueries(List<String> disabledSecondaryQueries) {
        this.disabledSecondaryQueries = disabledSecondaryQueries;
    }

    public List<Float> getDisabledSecondaryWeights() {
        return disabledSecondaryWeights;
    }

    public void setDisabledSecondaryWeights(List<Float> disabledSecondaryWeights) {
        this.disabledSecondaryWeights = disabledSecondaryWeights;
    }

    public boolean isDemoActive() {
        return demoActive;
    }

    public void setDemoActive(boolean demoActive) {
        this.demoActive = demoActive;
    }

    public Set<String> getCountryCodes() {
        return countryCodes;
    }

    public void setCountryCodes(Set<String> countryCodes) {
        this.countryCodes = countryCodes;
    }

    public boolean isExcludeInvestProjects() {
        return excludeInvestProjects;
    }

    public void setExcludeInvestProjects(boolean excludeInvestProjects) {
        this.excludeInvestProjects = excludeInvestProjects;
    }

    public boolean isExcludeOutcomes() {
        return excludeOutcomes;
    }

    public void setExcludeOutcomes(boolean excludeOutcomes) {
        this.excludeOutcomes = excludeOutcomes;
    }
}
