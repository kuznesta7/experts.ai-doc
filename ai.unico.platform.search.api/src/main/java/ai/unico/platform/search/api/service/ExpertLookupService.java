package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.dto.OrganizationExpertsDto;
import ai.unico.platform.store.api.dto.ExpertClaimDto;
import ai.unico.platform.store.api.dto.ItemNotRegisteredExpertDto;
import ai.unico.platform.store.api.dto.UserExpertBaseDto;
import ai.unico.platform.util.exception.NonexistentEntityException;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ExpertLookupService {

    String indexName = ExpertIndexService.indexName;

    UserExpertBaseDto findUserExpertBase(String expertCode) throws IOException, NonexistentEntityException;

    UserExpertBaseDto findUserExpertBase(Long expertId) throws IOException, NonexistentEntityException;

    Map<String, UserExpertBaseDto> findUserExpertBasesMap(Set<String> expertCodes) throws IOException;

    List<ItemNotRegisteredExpertDto> findNotRegisteredExperts(Set<Long> expertIds) throws IOException;

    List<UserExpertBaseDto> findUserExpertBases(Set<String> expertCodes) throws IOException;

    List<UserExpertBaseDto> findExpertBases(Set<Long> expertIds) throws IOException;

    Set<String> findOrganizationsExpertCodes(Set<Long> organizationIds, boolean verified) throws IOException;

    OrganizationExpertsDto findOrganizationsExpertIds(Set<Long> organizationIds) throws IOException;

    ExpertClaimDto findExpertToClaim(Long expertId) throws IOException, NonexistentEntityException;

}
