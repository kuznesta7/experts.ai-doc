package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.dto.CommercializationProjectDetailDto;
import ai.unico.platform.search.api.dto.SearchResultBucketDto;
import ai.unico.platform.search.api.filter.ExpertSearchFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.wrapper.ExpertInvestmentProjectWrapper;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface CommercializationProjectSearchService {

    String indexName = CommercializationProjectIndexService.indexName;

    List<SearchResultBucketDto> findExpertsForProjectSearch(ExpertSearchFilter expertSearchFilter) throws IOException;

    List<SearchResultBucketDto> findOrganizationsForProjectSearch(ExpertSearchFilter expertSearchFilter) throws IOException;

    ExpertInvestmentProjectWrapper findExpertInvestmentProjects(String expertCode, ExpertSearchResultFilter expertSearchResultFilter) throws IOException;

    ExpertInvestmentProjectStatistics findExpertInvestmentProjectStatistics(String expertCode, Set<Long> organizationIds) throws IOException;

    CommercializationProjectDetailDto findCommercializationProjectDetail(Long commercializationProjectId) throws IOException;

    class ExpertInvestmentProjectStatistics {

        private String expertCode;

        private Long numberOfInvestmentProjects;

        public String getExpertCode() {
            return expertCode;
        }

        public void setExpertCode(String expertCode) {
            this.expertCode = expertCode;
        }

        public Long getNumberOfInvestmentProjects() {
            return numberOfInvestmentProjects;
        }

        public void setNumberOfInvestmentProjects(Long numberOfInvestmentProjects) {
            this.numberOfInvestmentProjects = numberOfInvestmentProjects;
        }
    }

}
