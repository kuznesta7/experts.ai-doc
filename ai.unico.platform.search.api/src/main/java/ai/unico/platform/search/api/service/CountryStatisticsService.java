package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.dto.CountryStatisticsDto;

import java.io.IOException;

public interface CountryStatisticsService {

    String indexName = ExpertIndexService.indexName;

    CountryStatisticsDto getCountryStatistics(String countryCode) throws IOException;
}
