package ai.unico.platform.search.api.service;

import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

public interface EvidenceSearchService {

    Map<String, Object> search(Set<Long> organizationIds, String query, UserContext userContext) throws IllegalAccessException, IOException, InstantiationException;

}
