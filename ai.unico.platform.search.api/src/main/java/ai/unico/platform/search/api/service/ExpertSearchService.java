package ai.unico.platform.search.api.service;

import ai.unico.platform.recommendation.api.interfaces.RecombeeSearchEntity;
import ai.unico.platform.search.api.dto.UserExpertProfileSearchDetailDto;
import ai.unico.platform.search.api.filter.ExpertSearchFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.wrapper.SearchResultWrapper;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.Set;

public interface ExpertSearchService {

    String indexName = ExpertIndexService.indexName;

    SearchResultWrapper searchExperts(ExpertSearchFilter expertSearchFilter, UserContext userContext) throws IOException;

    UserExpertProfileSearchDetailDto findSearchByExpertiseDetail(String expertCode, ExpertSearchResultFilter searchFilter) throws IOException, NonexistentEntityException, IllegalArgumentException;

    FileDto exportSearch(ExpertSearchFilter expertSearchFilter) throws IOException;

    Set<RecombeeSearchEntity> findExpertsForOrganization(Set<Long> organizationIds, int limit, int from) throws IOException;

}
