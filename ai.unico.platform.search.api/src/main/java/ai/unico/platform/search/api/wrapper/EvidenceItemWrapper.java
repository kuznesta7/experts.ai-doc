package ai.unico.platform.search.api.wrapper;

import ai.unico.platform.recommendation.api.dto.RecommendationWrapperDto;
import ai.unico.platform.search.api.dto.EvidenceItemPreviewWithExpertsDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class EvidenceItemWrapper {

    private Long numberOfAllItems;

    private Integer limit;

    private Integer page;

    private Long organizationId;

    private Set<Long> organizationUnitIds;

    private List<EvidenceItemPreviewWithExpertsDto> itemPreviewDtos = new ArrayList<>();

    final private RecommendationWrapperDto recommendationWrapperDto = new RecommendationWrapperDto();

    public Long getNumberOfAllItems() {
        return numberOfAllItems;
    }

    public void setNumberOfAllItems(Long numberOfAllItems) {
        this.numberOfAllItems = numberOfAllItems;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<EvidenceItemPreviewWithExpertsDto> getItemPreviewDtos() {
        return itemPreviewDtos;
    }

    public void setItemPreviewDtos(List<EvidenceItemPreviewWithExpertsDto> itemPreviewDtos) {
        this.itemPreviewDtos = itemPreviewDtos;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Set<Long> getOrganizationUnitIds() {
        return organizationUnitIds;
    }

    public void setOrganizationUnitIds(Set<Long> organizationUnitIds) {
        this.organizationUnitIds = organizationUnitIds;
    }

    public RecommendationWrapperDto getRecommendationWrapperDto() {
        return recommendationWrapperDto;
    }
}
