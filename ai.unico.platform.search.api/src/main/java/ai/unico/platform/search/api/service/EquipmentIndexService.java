package ai.unico.platform.search.api.service;

import ai.unico.platform.store.api.dto.EquipmentRegisterDto;
import ai.unico.platform.store.api.dto.MultilingualDto;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;

public interface EquipmentIndexService {

    String indexName = "equipment";

    void reindexEquipment(Integer limit, boolean clean, String target, UserContext userContext) throws IOException;

    void reindexEquipment(MultilingualDto<EquipmentRegisterDto> equipmentRegisterDto) throws IOException;

    void deleteEquipment(String equipmentCode) throws IOException;

}
