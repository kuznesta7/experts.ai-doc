package ai.unico.platform.search.api.filter;

import java.util.HashSet;
import java.util.Set;

public class OrganizationAutocompleteFilter {
    private String query;
    private Set<String> countryCodes = new HashSet<>();
    private Set<Long> disabledOrganizationIds = new HashSet<>();
    private Set<Long> enableOrganizationIds = new HashSet<>();

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Set<String> getCountryCodes() {
        return countryCodes;
    }

    public void setCountryCodes(Set<String> countryCodes) {
        this.countryCodes = countryCodes;
    }

    public Set<Long> getDisabledOrganizationIds() {
        return disabledOrganizationIds;
    }

    public void setDisabledOrganizationIds(Set<Long> disabledOrganizationIds) {
        this.disabledOrganizationIds = disabledOrganizationIds;
    }

    public Set<Long> getEnableOrganizationIds() {
        return enableOrganizationIds;
    }

    public void setEnableOrganizationIds(Set<Long> enableOrganizationIds) {
        System.out.println(enableOrganizationIds.toString());
        this.enableOrganizationIds = enableOrganizationIds;
    }
}
