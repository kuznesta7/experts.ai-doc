package ai.unico.platform.search.api.service;

import ai.unico.platform.store.api.dto.ItemIndexDto;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;
import java.util.Map;

public interface ItemIndexService {

    String indexName = "item";

    void reindexItems(Integer limit, boolean clean, String target, UserContext userContext) throws IOException;

    void indexItem(ItemIndexDto itemIndexDto) throws IOException;

    void replaceExperts(Map<String, String> replacementMap, boolean originalOnly) throws IOException;

    void flush() throws IOException;

    void appendOrganizations(String itemCode) throws IOException;
}
