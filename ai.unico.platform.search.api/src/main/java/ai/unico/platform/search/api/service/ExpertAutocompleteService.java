package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.dto.UserExpertPreviewDto;
import ai.unico.platform.search.api.filter.ExpertLookupFilter;

import java.io.IOException;
import java.util.List;

public interface ExpertAutocompleteService {

    String indexName = ExpertIndexService.indexName;

    List<UserExpertPreviewDto> findUserExpertPreviews(ExpertLookupFilter filter) throws IOException;

}
