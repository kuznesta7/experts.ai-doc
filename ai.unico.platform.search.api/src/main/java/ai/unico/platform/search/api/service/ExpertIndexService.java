package ai.unico.platform.search.api.service;

import ai.unico.platform.store.api.dto.ExpertIndexDto;
import ai.unico.platform.store.api.dto.UserProfileIndexDto;
import ai.unico.platform.util.model.UserContext;

import java.io.IOException;

public interface ExpertIndexService {
    String indexName = "expert";

    void reindexExperts(Integer limit, boolean clean, UserContext userContext) throws IOException;

    void updateUser(UserProfileIndexDto userExpertDetail) throws IOException;

    void replaceExpert(Long expertToReplaceId, Long userId) throws IOException;

    void replaceUser(Long userToReplaceId, Long userId) throws IOException;

    void revertReplaceUser(Long userId, Long replacedUserId) throws IOException;

    void revertReplaceExpert(Long userToReplaceId, Long expertId) throws IOException;

    void updateExpertOrganization(Long expertId, Long organizationId, boolean deleted, boolean verified) throws IOException;

    void retireExpertOrganization(Long expertId, Long organizationId, boolean retired) throws IOException;

    void changeVisibilityInOrganization(Long expertId, Long organizationId, boolean visible) throws IOException;

    void changeActivityInOrganization(Long expertId, Long organizationId, boolean active) throws IOException;

    void updateExpert(ExpertIndexDto expertIndexDto) throws IOException;

    void flush() throws IOException;

}
