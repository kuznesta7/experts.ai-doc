package ai.unico.platform.search.api.wrapper;

import ai.unico.platform.search.api.dto.AdminOrganizationDto;

import java.util.ArrayList;
import java.util.List;

public class AdminOrganizationWrapper {

    private Long numberOfAllItems;

    private Integer limit;

    private Integer page;

    private List<AdminOrganizationDto> adminOrganizationDtos = new ArrayList<>();

    public Long getNumberOfAllItems() {
        return numberOfAllItems;
    }

    public void setNumberOfAllItems(Long numberOfAllItems) {
        this.numberOfAllItems = numberOfAllItems;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<AdminOrganizationDto> getAdminOrganizationDtos() {
        return adminOrganizationDtos;
    }

    public void setAdminOrganizationDtos(List<AdminOrganizationDto> adminOrganizationDtos) {
        this.adminOrganizationDtos = adminOrganizationDtos;
    }
}
