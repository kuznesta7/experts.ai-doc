package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.dto.ExpertStatisticsDto;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.wrapper.EvidenceExpertWrapper;
import ai.unico.platform.store.api.dto.OrganizationDetailDto;
import ai.unico.platform.util.dto.FileDto;

import java.io.IOException;
import java.util.Set;

public interface EvidenceExpertService {

    String indexName = ExpertIndexService.indexName;

    EvidenceExpertWrapper findAnalyticsExperts(EvidenceFilter evidenceFilter) throws IOException, IllegalAccessException, InstantiationException;

    ExpertStatisticsDto findExpertStatistics(Set<Long> organizationIds) throws IOException;

    FileDto exportAnalyticsExperts(EvidenceFilter evidenceFilter) throws IOException;

    EvidenceExpertWrapper findAnalyticsExpertsWithRecommendation(EvidenceFilter filter, OrganizationDetailDto organizationDetailDto) throws IOException;

    Set<String> findExpertNamesByCodes(Set<String> expertCodes) throws IOException;
}
