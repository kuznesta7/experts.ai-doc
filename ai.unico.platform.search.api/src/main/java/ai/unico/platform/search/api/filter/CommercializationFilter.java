package ai.unico.platform.search.api.filter;

import ai.unico.platform.store.api.enums.CommercializationOrganizationRelation;

import java.util.HashSet;
import java.util.Set;

public class CommercializationFilter extends BaseFilter implements OrganizationEvidenceFilter {

    private Long organizationId;

    private Set<Long> organizationIds = new HashSet<>();

    private Set<Long> ipOwnerOrganizationIds = new HashSet<>();

    private Set<Long> investorOrganizationIds = new HashSet<>();

    private Set<Long> commercializationPartnerOrganizationIds = new HashSet<>();

    private Set<CommercializationOrganizationRelation> organizationRelations = new HashSet<>();

    private Integer yearFrom;

    private Integer yearTo;

    private Integer budgetFrom;

    private Integer budgetTo;

    private Integer ideaScoreFrom;

    private Integer ideaScoreTo;

    private Set<Long> commercializationStatusIds = new HashSet<>();

    private Set<Long> commercializationDomainIds = new HashSet<>();

    private Set<Long> commercializationCategoryIds = new HashSet<>();

    private Set<Long> commercializationPriorityIds = new HashSet<>();

    private Set<String> expertCodes = new HashSet<>();

    private Set<Long> participatingOrganizationIds = new HashSet<>();

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Set<Long> getIpOwnerOrganizationIds() {
        return ipOwnerOrganizationIds;
    }

    public void setIpOwnerOrganizationIds(Set<Long> ipOwnerOrganizationIds) {
        this.ipOwnerOrganizationIds = ipOwnerOrganizationIds;
    }

    public Integer getYearFrom() {
        return yearFrom;
    }

    public void setYearFrom(Integer yearFrom) {
        this.yearFrom = yearFrom;
    }

    public Integer getYearTo() {
        return yearTo;
    }

    public void setYearTo(Integer yearTo) {
        this.yearTo = yearTo;
    }

    public Integer getBudgetFrom() {
        return budgetFrom;
    }

    public void setBudgetFrom(Integer budgetFrom) {
        this.budgetFrom = budgetFrom;
    }

    public Integer getBudgetTo() {
        return budgetTo;
    }

    public void setBudgetTo(Integer budgetTo) {
        this.budgetTo = budgetTo;
    }

    public Integer getIdeaScoreFrom() {
        return ideaScoreFrom;
    }

    public void setIdeaScoreFrom(Integer ideaScoreFrom) {
        this.ideaScoreFrom = ideaScoreFrom;
    }

    public Integer getIdeaScoreTo() {
        return ideaScoreTo;
    }

    public void setIdeaScoreTo(Integer ideaScoreTo) {
        this.ideaScoreTo = ideaScoreTo;
    }

    public Set<Long> getCommercializationStatusIds() {
        return commercializationStatusIds;
    }

    public void setCommercializationStatusIds(Set<Long> commercializationStatusIds) {
        this.commercializationStatusIds = commercializationStatusIds;
    }

    public Set<Long> getCommercializationDomainIds() {
        return commercializationDomainIds;
    }

    public void setCommercializationDomainIds(Set<Long> commercializationDomainIds) {
        this.commercializationDomainIds = commercializationDomainIds;
    }

    public Set<Long> getCommercializationCategoryIds() {
        return commercializationCategoryIds;
    }

    public void setCommercializationCategoryIds(Set<Long> commercializationCategoryIds) {
        this.commercializationCategoryIds = commercializationCategoryIds;
    }

    public Set<Long> getCommercializationPriorityIds() {
        return commercializationPriorityIds;
    }

    public void setCommercializationPriorityIds(Set<Long> commercializationPriorityIds) {
        this.commercializationPriorityIds = commercializationPriorityIds;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<Long> getInvestorOrganizationIds() {
        return investorOrganizationIds;
    }

    public void setInvestorOrganizationIds(Set<Long> investorOrganizationIds) {
        this.investorOrganizationIds = investorOrganizationIds;
    }

    public Set<Long> getCommercializationPartnerOrganizationIds() {
        return commercializationPartnerOrganizationIds;
    }

    public void setCommercializationPartnerOrganizationIds(Set<Long> commercializationPartnerOrganizationIds) {
        this.commercializationPartnerOrganizationIds = commercializationPartnerOrganizationIds;
    }

    public Set<CommercializationOrganizationRelation> getOrganizationRelations() {
        return organizationRelations;
    }

    public void setOrganizationRelations(Set<CommercializationOrganizationRelation> organizationRelations) {
        this.organizationRelations = organizationRelations;
    }

    public Set<String> getExpertCodes() {
        return expertCodes;
    }

    public void setExpertCodes(Set<String> expertCodes) {
        this.expertCodes = expertCodes;
    }

    public Set<Long> getParticipatingOrganizationIds() {
        return participatingOrganizationIds;
    }

    public void setParticipatingOrganizationIds(Set<Long> participatingOrganizationIds) {
        this.participatingOrganizationIds = participatingOrganizationIds;
    }
}
