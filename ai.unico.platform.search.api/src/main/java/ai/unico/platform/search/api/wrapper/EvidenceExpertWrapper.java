package ai.unico.platform.search.api.wrapper;

import ai.unico.platform.recommendation.api.dto.RecommendationWrapperDto;
import ai.unico.platform.search.api.dto.EvidenceExpertPreviewDto;
import ai.unico.platform.store.api.dto.ExpertSearchHistoryDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class EvidenceExpertWrapper {

    private Long numberOfAllItems;

    private Integer limit;

    private Integer page;

    private Long organizationId;

    private Set<Long> organizationUnitIds;

    final RecommendationWrapperDto recommendationWrapperDto = new RecommendationWrapperDto();

    private List<EvidenceExpertPreviewDto> analyticsExpertPreviewDtos = new ArrayList<>();

    public Long getNumberOfAllItems() {
        return numberOfAllItems;
    }

    public void setNumberOfAllItems(Long numberOfAllItems) {
        this.numberOfAllItems = numberOfAllItems;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<EvidenceExpertPreviewDto> getAnalyticsExpertPreviewDtos() {
        return analyticsExpertPreviewDtos;
    }

    public List<ExpertSearchHistoryDto.ResultDto> transformAndGetResultDtos() {
        List<ExpertSearchHistoryDto.ResultDto> resultDtos = new ArrayList<>();
        int pos = 1;
        for(EvidenceExpertPreviewDto preview: analyticsExpertPreviewDtos){
            resultDtos.add(new ExpertSearchHistoryDto.ResultDto(pos++, preview.getUserId(), preview.getExpertId()));
        }
        return resultDtos;
    }

    public void setAnalyticsExpertPreviewDtos(List<EvidenceExpertPreviewDto> analyticsExpertPreviewDtos) {
        this.analyticsExpertPreviewDtos = analyticsExpertPreviewDtos;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Set<Long> getOrganizationUnitIds() {
        return organizationUnitIds;
    }

    public void setOrganizationUnitIds(Set<Long> organizationUnitIds) {
        this.organizationUnitIds = organizationUnitIds;
    }

    public RecommendationWrapperDto getRecommendationWrapperDto() {
        return recommendationWrapperDto;
    }
}
