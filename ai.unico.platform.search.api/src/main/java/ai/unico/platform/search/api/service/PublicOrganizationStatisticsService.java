package ai.unico.platform.search.api.service;

import java.io.IOException;
import java.util.Collection;

public interface PublicOrganizationStatisticsService {

    PublicOrganizationStatisticsDto getPublicOrganizationStatistics(Collection<Long> organizationIds) throws IOException;

    class PublicOrganizationStatisticsDto {
        private Long numberOfOutcomes;
        private Long numberOfExperts;
        private Long numberOfProjects;

        public Long getNumberOfOutcomes() {
            return numberOfOutcomes;
        }

        public void setNumberOfOutcomes(Long numberOfOutcomes) {
            this.numberOfOutcomes = numberOfOutcomes;
        }

        public Long getNumberOfExperts() {
            return numberOfExperts;
        }

        public void setNumberOfExperts(Long numberOfExperts) {
            this.numberOfExperts = numberOfExperts;
        }

        public Long getNumberOfProjects() {
            return numberOfProjects;
        }

        public void setNumberOfProjects(Long numberOfProjects) {
            this.numberOfProjects = numberOfProjects;
        }
    }

}
