package ai.unico.platform.search.api.service;

import ai.unico.platform.search.api.dto.EquipmentPreviewDto;
import ai.unico.platform.search.api.filter.EquipmentFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.wrapper.EquipmentWrapper;
import ai.unico.platform.store.api.dto.BasicStatisticsDto;
import ai.unico.platform.store.api.dto.EquipmentRegisterDto;
import ai.unico.platform.store.api.dto.MultilingualDto;

import java.io.IOException;
import java.util.Set;

public interface EquipmentSearchService {
    String indexName = EquipmentIndexService.indexName;

    EquipmentWrapper findEquipment(EquipmentFilter equipmentFilter, ExpertSearchResultFilter.Order order) throws IOException;

    MultilingualDto<EquipmentPreviewDto> getEquipment(String equipmentCode) throws IOException;

    EquipmentRegisterDto getEquipmentForRegister(String equipmentCode) throws IOException;

    BasicStatisticsDto findEquipmentStatistics(Set<Long> organizationIds) throws IOException;
}
