package ai.unico.platform.search.api.wrapper;

import ai.unico.platform.search.api.dto.ProjectPreviewDto;

import java.util.ArrayList;
import java.util.List;

public class ExpertProjectWrapper {
    private Long numberOfRelevantItems;

    private Double scoreSum;

    private Integer limit;

    private Integer page;

    private List<ProjectPreviewDto> projectPreviewDtos = new ArrayList<>();

    public Long getNumberOfRelevantItems() {
        return numberOfRelevantItems;
    }

    public void setNumberOfRelevantItems(Long numberOfRelevantItems) {
        this.numberOfRelevantItems = numberOfRelevantItems;
    }

    public Double getScoreSum() {
        return scoreSum;
    }

    public void setScoreSum(Double scoreSum) {
        this.scoreSum = scoreSum;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<ProjectPreviewDto> getProjectPreviewDtos() {
        return projectPreviewDtos;
    }

    public void setProjectPreviewDtos(List<ProjectPreviewDto> projectPreviewDtos) {
        this.projectPreviewDtos = projectPreviewDtos;
    }
}
