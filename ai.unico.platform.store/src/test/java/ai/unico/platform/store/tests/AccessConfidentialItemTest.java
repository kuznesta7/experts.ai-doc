package ai.unico.platform.store.tests;

import ai.unico.platform.store.Config;
import ai.unico.platform.store.api.service.StoreSecurityHelperService;
import ai.unico.platform.store.dao.ItemDao;
import ai.unico.platform.store.entity.ItemEntity;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.mock.ItemMockUtil;
import ai.unico.platform.store.mock.OrganizationMockUtil;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.mock.IdGeneratorUtil;
import ai.unico.platform.util.mock.UserContextMockUtil;
import ai.unico.platform.util.model.UserContext;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@DataJpaTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = Config.class)
public class AccessConfidentialItemTest {

    private ItemEntity itemEntity;

    @MockBean
    private ItemDao itemDao;

    @Autowired
    private StoreSecurityHelperService storeSecurityHelperService;

    @Before
    public void setUp() {
        itemEntity = ItemMockUtil.createConfidentialItem();
        Mockito.when(itemDao.find(itemEntity.getItemId())).thenReturn(itemEntity);
    }

    @Test
    public void whenItemConfidentialAndBasicUser_itemShouldNotBeAccessible() {
        final UserContext userContext = UserContextMockUtil.createBaseUserContext();
        final Boolean result = storeSecurityHelperService.canUserSeeItem(itemEntity.getItemId(), userContext);
        Assertions.assertThat(result).isFalse();
    }

    @Test
    public void whenItemConfidentialAndOrganizationEditorFromUninvolvedOrganization_itemShouldNotBeAccessible() {
        final UserContext userContext = UserContextMockUtil.createOrganizationUserContextWithOrganizationRoles(IdGeneratorUtil.generateId(), OrganizationRole.ORGANIZATION_VIEWER, OrganizationRole.ORGANIZATION_EDITOR);
        final Boolean result = storeSecurityHelperService.canUserSeeItem(itemEntity.getItemId(), userContext);
        Assertions.assertThat(result).isFalse();
    }

    @Test
    public void whenItemConfidentialAndOrganizationViewerFromOwnerOrganization_itemShouldBeAccessible() {
        final Long organizationId = itemEntity.getOwnerOrganization().getOrganizationId();
        final UserContext userContext = UserContextMockUtil.createOrganizationUserContextWithOrganizationRoles(organizationId, OrganizationRole.ORGANIZATION_VIEWER);
        final Boolean result = storeSecurityHelperService.canUserSeeItem(itemEntity.getItemId(), userContext);
        Assertions.assertThat(result).isTrue();
    }

    @Test
    public void whenItemConfidentialAndOrganizationViewerFromInvolvedOrganization_itemShouldBeAccessible() {
        final OrganizationEntity secondOrganization = OrganizationMockUtil.createOrganization();
        ItemMockUtil.addOrganization(itemEntity, secondOrganization);
        final UserContext userContext = UserContextMockUtil.createOrganizationUserContextWithOrganizationRoles(secondOrganization.getOrganizationId(), OrganizationRole.ORGANIZATION_VIEWER);
        final Boolean result = storeSecurityHelperService.canUserSeeItem(itemEntity.getItemId(), userContext);
        Assertions.assertThat(result).isTrue();
    }
}
