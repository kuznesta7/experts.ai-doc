package ai.unico.platform.store.mock;

import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.entity.ProjectEntity;
import ai.unico.platform.store.entity.ProjectOrganizationEntity;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.enums.ProjectOrganizationRole;
import ai.unico.platform.util.mock.IdGeneratorUtil;

import java.util.Date;

public class ProjectMockUtil {

    public static ProjectEntity createProject() {
        final ProjectEntity projectEntity = new ProjectEntity();
        final Long projectId = IdGeneratorUtil.generateId();
        projectEntity.setProjectId(projectId);
        projectEntity.setName("Test project " + projectId);
        projectEntity.setDescription("Lorem ipsum dolor sit amet.");
        projectEntity.setStartDate(new Date());
        projectEntity.setEndDate(new Date());
        projectEntity.setDeleted(false);
        projectEntity.setConfidentiality(Confidentiality.PUBLIC);
        return projectEntity;
    }

    public static ProjectEntity createConfidentialProject() {
        final ProjectEntity projectEntity = createProject();
        projectEntity.setConfidentiality(Confidentiality.CONFIDENTIAL);
        final OrganizationEntity organization = OrganizationMockUtil.createOrganization();
        projectEntity.setOwnerOrganization(organization);
        addOrganization(projectEntity, organization);
        return projectEntity;
    }

    public static ProjectEntity createSecretProject() {
        final ProjectEntity projectEntity = createProject();
        projectEntity.setConfidentiality(Confidentiality.SECRET);
        final OrganizationEntity organization = OrganizationMockUtil.createOrganization();
        projectEntity.setOwnerOrganization(organization);
        addOrganization(projectEntity, organization);
        return projectEntity;
    }

    public static void addOrganization(ProjectEntity projectEntity, OrganizationEntity organizationEntity) {
        final ProjectOrganizationEntity projectOrganizationEntity = new ProjectOrganizationEntity();
        projectOrganizationEntity.setProjectEntity(projectEntity);
        projectOrganizationEntity.setOrganizationEntity(organizationEntity);
        projectOrganizationEntity.setProjectOrganizationRole(ProjectOrganizationRole.PARTICIPANT);
        projectOrganizationEntity.setDeleted(false);
        projectEntity.getProjectOrganizationEntities().add(projectOrganizationEntity);
    }

}
