package ai.unico.platform.store.mock;

import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.util.mock.IdGeneratorUtil;

public class OrganizationMockUtil {

    public static OrganizationEntity createOrganization() {
        final OrganizationEntity organizationEntity = new OrganizationEntity();
        final Long organizationId = IdGeneratorUtil.generateId();
        organizationEntity.setOrganizationId(organizationId);
        organizationEntity.setOrganizationName("Test organization " + organizationId);
        return organizationEntity;
    }

}
