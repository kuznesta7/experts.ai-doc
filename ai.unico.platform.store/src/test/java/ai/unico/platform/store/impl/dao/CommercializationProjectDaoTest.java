package ai.unico.platform.store.impl.dao;

import ai.unico.platform.store.dao.CommercializationProjectDao;
import ai.unico.platform.store.entity.CommercializationProjectEntity;
import ai.unico.platform.store.entity.CommercializationProjectKeywordEntity;
import ai.unico.platform.store.entity.CommercializationProjectStatusEntity;

import java.util.List;

public class CommercializationProjectDaoTest implements CommercializationProjectDao {
    @Override
    public CommercializationProjectEntity find(Long commercializationProjectId) {
        return null;
    }

    @Override
    public void save(CommercializationProjectEntity commercializationProjectEntity) {

    }

    @Override
    public void save(CommercializationProjectStatusEntity commercializationProjectStatusEntity) {

    }

    @Override
    public void save(CommercializationProjectKeywordEntity projectKeywordEntity) {

    }

    @Override
    public List<CommercializationProjectEntity> findAll(Integer limit, Integer offset) {
        return null;
    }

    @Override
    public List<CommercializationProjectStatusEntity> findStatusHistory(Long commercializationProjectId) {
        return null;
    }

    @Override
    public List<CommercializationProjectEntity> findForUser(Long userId) {
        return null;
    }

    @Override
    public List<CommercializationProjectEntity> findForExpert(Long expertId) {
        return null;
    }
}
