package ai.unico.platform.store.impl.dao;

import ai.unico.platform.store.api.dto.ExpertSearchHistoryDto;
import ai.unico.platform.store.api.filter.ExpertSearchHistoryFilter;
import ai.unico.platform.store.dao.SearchHistoryDao;
import ai.unico.platform.store.entity.SearchHistEntity;
import ai.unico.platform.store.entity.SearchHistResultEntity;
import ai.unico.platform.store.entity.SearchHistSecondaryQueryEntity;

import java.util.List;

public class SearchHistoryDaoTest implements SearchHistoryDao {
    @Override
    public void saveSearchHistory(SearchHistEntity searchHistEntity) {

    }

    @Override
    public void saveSearchSecondaryQuery(SearchHistSecondaryQueryEntity searchHistSecondaryQueryEntity) {

    }

    @Override
    public void saveSearchResult(SearchHistResultEntity searchHistResultEntity) {

    }

    @Override
    public SearchHistEntity findSearchHistory(Long searchHistId) {
        return null;
    }

    @Override
    public List<SearchHistEntity> findSearchHistoryForUser(Long userId, ExpertSearchHistoryFilter expertSearchHistoryFilter) {
        return null;
    }

    @Override
    public Long countSearchHistoryForUser(Long userId, ExpertSearchHistoryFilter expertSearchHistoryFilter) {
        return null;
    }

    @Override
    public SearchHistEntity findSavedSearch(Long userId, ExpertSearchHistoryDto expertSearchHistoryDto) {
        return null;
    }

    @Override
    public SearchHistEntity findSameSearchAs(Long searchHistId, ExpertSearchHistoryDto expertSearchHistoryDto) {
        return null;
    }

    @Override
    public Long findNumberOfSearches() {
        return null;
    }

    @Override
    public List<SearchHistResultEntity> findSearchResultsForUser(Long userId) {
        return null;
    }

    @Override
    public List<SearchHistResultEntity> findSearchResultsForExpert(Long expertId) {
        return null;
    }
}
