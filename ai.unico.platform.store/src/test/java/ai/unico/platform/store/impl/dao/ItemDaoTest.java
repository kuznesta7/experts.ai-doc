package ai.unico.platform.store.impl.dao;

import ai.unico.platform.store.dao.ItemDao;
import ai.unico.platform.store.entity.ItemEntity;
import ai.unico.platform.store.entity.ItemTagEntity;

import java.util.List;
import java.util.Set;

public class ItemDaoTest implements ItemDao {
    @Override
    public List<ItemEntity> findItemsByOriginalIds(Set<Long> originalIds) {
        return null;
    }

    @Override
    public void saveItem(ItemEntity itemEntity) {

    }

    @Override
    public void saveItemTag(ItemTagEntity itemTagEntity) {

    }

    @Override
    public List<ItemEntity> findItems(Integer limit, Integer offset) {
        return null;
    }

    @Override
    public ItemEntity find(Long itemId) {
        return null;
    }

    @Override
    public List<ItemEntity> find(Set<Long> itemIds) {
        return null;
    }
}
