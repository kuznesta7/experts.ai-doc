package ai.unico.platform.store.impl.dao;

import ai.unico.platform.store.dao.UserExpertItemDao;
import ai.unico.platform.store.entity.ItemEntity;
import ai.unico.platform.store.entity.ItemNotRegisteredExpertEntity;
import ai.unico.platform.store.entity.UserExpertItemEntity;

import java.util.List;
import java.util.Set;

public class UserExpertItemDaoTest implements UserExpertItemDao {
    @Override
    public void saveUserItem(UserExpertItemEntity userExpertItemEntity) {

    }

    @Override
    public void saveNotRegisteredExpert(ItemNotRegisteredExpertEntity notRegisteredExpert) {

    }

    @Override
    public ItemNotRegisteredExpertEntity findNotRegisteredExpert(Long expertId) {
        return null;
    }

    @Override
    public ItemNotRegisteredExpertEntity findNotRegisteredExpertByItemIdAndName(Long itemId, String expertName) {
        return null;
    }

    @Override
    public void removeNotRegisteredExpert(Long expertId) {

    }

    @Override
    public List<ItemNotRegisteredExpertEntity> findNotRegisteredExperts(Set<Long> expertIds) {
        return null;
    }

    @Override
    public UserExpertItemEntity findForUser(Long userId, Long itemId) {
        return null;
    }

    @Override
    public UserExpertItemEntity findForExpert(Long expertId, Long itemId) {
        return null;
    }

    @Override
    public List<Long> findUserItems(Long userId) {
        return null;
    }

    @Override
    public List<Long> findExpertItems(Long expertId) {
        return null;
    }
}
