package ai.unico.platform.store.impl.dao;

import ai.unico.platform.store.dao.ProjectDao;
import ai.unico.platform.store.entity.ProjectEntity;
import ai.unico.platform.store.entity.ProjectTagEntity;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public class ProjectDaoTest implements ProjectDao {
    @Override
    public void saveProject(ProjectEntity projectEntity) {
    }

    @Override
    public ProjectEntity find(Long projectId) {
        return null;
    }

    @Override
    public ProjectEntity findByOriginalProjectId(Long originalProjectId) {
        return null;
    }

    @Override
    public void saveProjectTag(ProjectTagEntity projectTagEntity) {

    }

    @Override
    public List<ProjectEntity> findAll(Set<Long> projectIds) {
        return null;
    }

    @Override
    public List<ProjectEntity> findAll(Integer limit, Integer offset) {
        return null;
    }

    @Override
    public List<ProjectEntity> findJoinedByIds(Collection<Long> projectIds) {
        return null;
    }
}
