package ai.unico.platform.store;

import ai.unico.platform.store.api.service.StoreSecurityHelperService;
import ai.unico.platform.store.dao.CommercializationProjectDao;
import ai.unico.platform.store.dao.ItemDao;
import ai.unico.platform.store.dao.ProjectDao;
import ai.unico.platform.store.dao.SearchHistoryDao;
import ai.unico.platform.store.impl.dao.CommercializationProjectDaoTest;
import ai.unico.platform.store.impl.dao.ItemDaoTest;
import ai.unico.platform.store.impl.dao.ProjectDaoTest;
import ai.unico.platform.store.impl.dao.SearchHistoryDaoTest;
import ai.unico.platform.store.service.StoreSecurityHelperServiceImpl;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;

@SpringBootTest
public class Config {
    @Bean
    public ItemDao itemDao() {
        return new ItemDaoTest();
    }

    @Bean
    public ProjectDao projectDao() {
        return new ProjectDaoTest();
    }

    @Bean
    public SearchHistoryDao searchHistoryDao() {
        return new SearchHistoryDaoTest();
    }

    @Bean
    public CommercializationProjectDao commercializationProjectDao() {
        return new CommercializationProjectDaoTest();
    }

    @Bean
    public StoreSecurityHelperService storeSecurityHelperService() {
        return new StoreSecurityHelperServiceImpl();
    }
}
