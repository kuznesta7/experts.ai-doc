package ai.unico.platform.store.mock;

import ai.unico.platform.store.entity.ItemEntity;
import ai.unico.platform.store.entity.ItemOrganizationEntity;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.mock.IdGeneratorUtil;

public class ItemMockUtil {

    public static ItemEntity createBasicItem() {
        final ItemEntity itemEntity = new ItemEntity();
        itemEntity.setItemId(IdGeneratorUtil.generateId());
        itemEntity.setItemName("Test outcome");
        itemEntity.setItemDescription("Lorem ipsum dolor sit amet.");
        itemEntity.setYear(2020);
        itemEntity.setConfidentiality(Confidentiality.PUBLIC);
        return itemEntity;
    }

    public static ItemEntity createConfidentialItem() {
        final ItemEntity item = ItemMockUtil.createBasicItem();
        item.setConfidentiality(Confidentiality.CONFIDENTIAL);
        final OrganizationEntity organization = OrganizationMockUtil.createOrganization();
        item.setOwnerOrganization(organization);
        addOrganization(item, organization);
        return item;
    }

    public static ItemEntity createSecretItem() {
        final ItemEntity item = ItemMockUtil.createBasicItem();
        item.setConfidentiality(Confidentiality.SECRET);
        final OrganizationEntity organization = OrganizationMockUtil.createOrganization();
        item.setOwnerOrganization(organization);
        addOrganization(item, organization);
        return item;
    }

    public static void addOrganization(ItemEntity itemEntity, OrganizationEntity organizationEntity) {
        final ItemOrganizationEntity itemOrganizationEntity = new ItemOrganizationEntity();
        itemOrganizationEntity.setOrganizationEntity(organizationEntity);
        itemOrganizationEntity.setItemEntity(itemEntity);
        itemOrganizationEntity.setDeleted(false);
        itemEntity.getItemOrganizationEntities().add(itemOrganizationEntity);
    }
}
