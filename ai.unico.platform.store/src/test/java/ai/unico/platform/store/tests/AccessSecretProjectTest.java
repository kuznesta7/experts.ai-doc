package ai.unico.platform.store.tests;

import ai.unico.platform.store.Config;
import ai.unico.platform.store.api.service.StoreSecurityHelperService;
import ai.unico.platform.store.dao.ProjectDao;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.entity.ProjectEntity;
import ai.unico.platform.store.mock.OrganizationMockUtil;
import ai.unico.platform.store.mock.ProjectMockUtil;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.mock.IdGeneratorUtil;
import ai.unico.platform.util.mock.UserContextMockUtil;
import ai.unico.platform.util.model.UserContext;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@DataJpaTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = Config.class)
public class AccessSecretProjectTest {

    private ProjectEntity projectEntity;

    @MockBean
    private ProjectDao projectDao;

    @Autowired
    private StoreSecurityHelperService storeSecurityHelperService;

    @Before
    public void setUp() {
        projectEntity = ProjectMockUtil.createSecretProject();
        Mockito.when(projectDao.find(projectEntity.getProjectId())).thenReturn(projectEntity);
    }

    @Test
    public void whenProjectSecretAndBasicUser_itemShouldNotBeAccessible() {
        final UserContext userContext = UserContextMockUtil.createBaseUserContext();
        final Boolean result = storeSecurityHelperService.canUserSeeProject(projectEntity.getProjectId(), userContext);
        Assertions.assertThat(result).isFalse();
    }

    @Test
    public void whenProjectSecretAndOrganizationEditorFromUninvolvedOrganization_itemShouldNotBeAccessible() {
        final Long uninvolvedOrganizationId = IdGeneratorUtil.generateId();
        final UserContext userContext = UserContextMockUtil.createOrganizationUserContextWithOrganizationRoles(uninvolvedOrganizationId, OrganizationRole.ORGANIZATION_VIEWER, OrganizationRole.ORGANIZATION_EDITOR);
        final Boolean result = storeSecurityHelperService.canUserSeeProject(projectEntity.getProjectId(), userContext);
        Assertions.assertThat(result).isFalse();
    }

    @Test
    public void whenProjectSecretAndOrganizationEditorFromInvolvedOrganization_itemShouldNotBeAccessible() {
        final OrganizationEntity secondOrganization = OrganizationMockUtil.createOrganization();
        ProjectMockUtil.addOrganization(projectEntity, secondOrganization);
        final UserContext userContext = UserContextMockUtil.createOrganizationUserContextWithOrganizationRoles(secondOrganization.getOrganizationId(), OrganizationRole.ORGANIZATION_EDITOR, OrganizationRole.ORGANIZATION_VIEWER);
        final Boolean result = storeSecurityHelperService.canUserSeeProject(projectEntity.getProjectId(), userContext);
        Assertions.assertThat(result).isFalse();
    }

    @Test
    public void whenProjectSecretAndOrganizationViewerFromOwnerOrganization_itemShouldNotBeAccessible() {
        final Long organizationId = projectEntity.getOwnerOrganization().getOrganizationId();
        final UserContext userContext = UserContextMockUtil.createOrganizationUserContextWithOrganizationRoles(organizationId, OrganizationRole.ORGANIZATION_VIEWER);
        final Boolean result = storeSecurityHelperService.canUserSeeProject(projectEntity.getProjectId(), userContext);
        Assertions.assertThat(result).isFalse();
    }

    @Test
    public void whenProjectSecretAndOrganizationEditorFromOwnerOrganization_itemShouldBeAccessible() {
        final Long organizationId = projectEntity.getOwnerOrganization().getOrganizationId();
        final UserContext userContext = UserContextMockUtil.createOrganizationUserContextWithOrganizationRoles(organizationId, OrganizationRole.ORGANIZATION_EDITOR);
        final Boolean result = storeSecurityHelperService.canUserSeeProject(projectEntity.getProjectId(), userContext);
        Assertions.assertThat(result).isTrue();
    }
}
