INSERT INTO item_type (type_id, title, code, weight, type_group_id)
VALUES (35, 'Ekrit - Výstava s kritickým katalogem', 'Ekrit', 0.2, 4),
       (36, 'Enekrit - Výstava', 'Enekrit', 0.2, 4),
       (37, 'Fprum - Průmyslový vzor', 'Fprum', 5, 3),
       (38, 'Fuzit - Užitný vzor', 'Fuzit', 1, 3),
       (39, 'Gfunk - Funkční vzorek', 'Gfunk', 0.2, 4),
       (40, 'Gprot - Prototyp', 'Gprot', 0.2, 4),
       (41, 'Hkonc - Promítnuto do schválených strategických dokumentů orgánů státní nebo veřejné správy', 'Hkoc', 0.2,
        4),
       (42, 'Hleg - Výsledky promítnuté do právních předpisů a norem', 'Hleg', 0.2, 4),
       (43, 'Hneleg - Promítnuto do směrnic nebo předpisů závazných v rámci příslušného poskytovatele', 'Hneleg', 0.2,
        4),
       (44, 'Jimp - Článek v periodiku v databázi Web of Science', 'Jimp', 0.6, 1),
       (45, 'Jost - Ostatní články v recenzovaných periodicích', 'Jost', 0.2, 1),
       (46, 'JSC - Článek v periodiku v databázi SCOPUS', 'JSC', 0.4, 1),
       (47, 'Jx - Nezařazeno - Článek v odborném periodiku (Jimp, Jsc a Jost)', 'Jx', 0.2, 1),
       (48, 'M - Uspořádání konference', 'M', 0.2, 4),
       (49, 'Nlec - Léčebný postup', 'Nlec', 0.2, 4),
       (50, 'Nmap - Specializovaná mapa s odborným obsahem', 'Nmap', 0.2, 4),
       (51, 'NmetA - Akreditované metodiky a postupy', 'NmetA', 0.2, 4),
       (52, 'NmetC - Metodiky certifikované oprávněným orgánem', 'NmetC', 0.2, 4),
       (53, 'NmetS - Metodiky schválené orgánem státní správy', 'NmetS', 0.2, 4),
       (54, 'Npam - Památkový postup', 'Npam', 0.2, 4),
       (55, 'O - Ostatní výsledky', 'O', 0.2, 4),
       (56, 'Sdb - Specializovaná veřejná databáze', 'Sdb', 0.2, 4),
       (57, 'Vsouhrn - Souhrnná výzkumná zpráva', 'Vsouhrn', 0.2, 4),
       (58, 'Vutaj - Výzkumná zpráva obsahující utajované informace', 'Vutaj', 1.6, 4),
       (59,
        'Vx - Nezařazeno - Výzkumná zpráva obsahující utajované informace (takový výsledek lze do RIV vložit pouze v případě, že zpráva obsahuje utajované informace a pole R12 = U), nebo souhrnná výzkumná zpráva',
        'Vx', 0.2, 4),
       (60, 'W - Uspořádání workshopu', 'W', 0.2, 4),
       (61, 'X - Nezařazeno', 'X', 0.2, 4),
       (62, 'Zodru - Odrůda', 'Zodru', 0.2, 4),
       (63, 'Zplem - Plemeno', 'Zplem', 0.2, 4),
       (64, 'Zpolop - Poloprovoz', 'Zpolop', 0.2, 4),
       (65, 'Ztech - Ověřená technologie', 'Ztech', 0.2, 4),
       (66, 'Zx - Nezařazeno - Poloprovoz, ověřená technologie, odrůda, plemeno', 'Zx', 0.2, 4),
       (67, 'C - Kapitola v odborné knize', 'C', 0.4, 1);

UPDATE item_type
SET weight= 1.2
where type_id = 16;
UPDATE item_type
SET weight= 1
where type_id = 15;
UPDATE item_type
SET weight= 1.8
where type_id = 8;
UPDATE item_type
SET weight= 1.8
where type_id = 6;
UPDATE item_type
SET weight= 1.8
where type_id = 7;
UPDATE item_type
SET weight= 1.8
where type_id = 9;
UPDATE item_type
SET weight= 1
where type_id = 20;
UPDATE item_type
SET weight= 1.6
where type_id = 21;
UPDATE item_type
SET weight= 0.8
where type_id = 17;
UPDATE item_type
SET weight= 0.2
where type_id = 26;
UPDATE item_type
SET weight= 0.2
where type_id = 18;
UPDATE item_type
SET weight= 0.2
where type_id = 19;
UPDATE item_type
SET weight= 0.2
where type_id = 22;
UPDATE item_type
SET weight= 0.2
where type_id = 23;
UPDATE item_type
SET weight= 0.2
where type_id = 24;
UPDATE item_type
SET weight= 0.2
where type_id = 25;
UPDATE item_type
SET weight= 0.4
where type_id = 12;
UPDATE item_type
SET weight= 0.6
where type_id = 11;
UPDATE item_type
SET weight= 0.2
where type_id = 10;
UPDATE item_type
SET weight= 1.8
where type_id = 14;
UPDATE item_type
SET weight= 0.4
where type_id = 13;
UPDATE item_type
SET weight= 1.2
where type_id = 28;
UPDATE item_type
SET weight= 1
where type_id = 29;
UPDATE item_type
SET weight= 0.8
where type_id = 30;
UPDATE item_type
SET weight= 1.4
where type_id = 27;
UPDATE item_type
SET weight= 0.2
where type_id = 31;
UPDATE item_type
SET weight= 0.6
where type_id = 32;
UPDATE item_type
SET weight= 0.4
where type_id = 33;
UPDATE item_type
SET weight= 0.2
where type_id = 34;