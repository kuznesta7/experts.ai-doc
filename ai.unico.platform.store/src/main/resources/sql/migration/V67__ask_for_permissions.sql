create table ask_for_permissions
(
    ask_for_permissions_id bigserial primary key,
    organization_id        bigint references organization,
    message                text,
    source_url             text
) inherits (trackable)