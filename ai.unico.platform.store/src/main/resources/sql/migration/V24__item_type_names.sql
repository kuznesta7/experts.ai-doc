update item_type
set title = 'Peer-review article'
where title = 'ITEM_TYPE_J';
update item_type
set title = 'Peer-review article'
where title = 'ITEM_TYPE_JC';
update item_type
set title = 'Peer-review article in SCOPUS'
where title = 'ITEM_TYPE_JB';
update item_type
set title = 'Peer-review article in WOS'
where title = 'ITEM_TYPE_JA';
update item_type
set title = 'Peer-review article in WOS'
where title = 'ITEM_TYPE_JO';
update item_type
set title = 'Peer-review article in SCIMAGOJR Q4'
where title = 'ITEM_TYPE_J4';
update item_type
set title = 'Peer-review article in SCIMAGOJR Q3'
where title = 'ITEM_TYPE_J3';
update item_type
set title = 'Peer-review article in SCIMAGOJR Q2'
where title = 'ITEM_TYPE_J2';
update item_type
set title = 'Peer-review article in SCIMAGOJR Q1'
where title = 'ITEM_TYPE_J1';
update item_type
set title = 'Patent applilcation -World'
where title = 'ITEM_TYPE_PV-PCT';
update item_type
set title = 'Patent'
where title = 'ITEM_TYPE_PV-ZAH';
update item_type
set title = 'European Patent'
where title = 'ITEM_TYPE_EP';
update item_type
set title = 'Patent applilcation - national'
where title = 'ITEM_TYPE_PV-NAR';
update item_type
set title = 'Professional book'
where title = 'ITEM_TYPE_B';
update item_type
set title = 'Chapter in professional book'
where title = 'ITEM_TYPE_C';
update item_type
set title = 'Article in proceedings'
where title = 'ITEM_TYPE_D';
update item_type
set title = 'Patent'
where title = 'ITEM_TYPE_P';
update item_type
set title = 'Validated technology in real environment, variety, breed'
where title = 'ITEM_TYPE_Z';
update item_type
set title = 'Utility model, industrial design'
where title = 'ITEM_TYPE_F';
update item_type
set title = 'Prototype, functional sample'
where title = 'ITEM_TYPE_G';
update item_type
set title = 'Results projected into legislative, non-legislative and strategic documents'
where title = 'ITEM_TYPE_H';
update item_type
set title = 'Methodology, procedure or maps'
where title = 'ITEM_TYPE_N';
update item_type
set title = 'Seminar'
where title = 'ITEM_TYPE_M';
update item_type
set title = 'Workshop'
where title = 'ITEM_TYPE_W';
update item_type
set title = 'Exhibition'
where title = 'ITEM_TYPE_E';
update item_type
set title = 'Software'
where title = 'ITEM_TYPE_R';
update item_type
set title = 'Research report or contractual research'
where title = 'ITEM_TYPE_V';
update item_type
set title = 'Audiovisual production'
where title = 'ITEM_TYPE_A';
update item_type
set title = 'Other results'
where title = 'ITEM_TYPE_O';

update item_type_group
set title = 'Publications'
where title = 'PUBLICATIONS';
update item_type_group
set title = 'Patents'
where title = 'PATENTS';
update item_type_group
set title = 'Applied results'
where title = 'APPLIED_RESULTS';
update item_type_group
set title = 'Other results'
where title = 'OTHER_RESULTS';