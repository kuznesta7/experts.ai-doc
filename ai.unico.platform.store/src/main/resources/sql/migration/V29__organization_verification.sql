alter table user_organization
  add column verified boolean default false;

alter table organization_admin
  add column manage_admins_permission boolean default false;