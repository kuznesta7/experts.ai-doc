drop table if exists opportunity_tag;
drop table if exists opportunity_job_type;
drop table if exists opportunity;
drop table if exists job_type;
drop table if exists opportunity_type;

create table job_type
(
    id          bigserial primary key,
    original_id bigint,
    deleted     bool default false,
    name        text
);

create table opportunity_type
(
    id          bigserial primary key,
    original_id bigint,
    deleted     bool default false,
    name        text
);

create table opportunity
(
    opportunity_id             bigserial primary key,
    original_opportunity_id    bigint,
    opportunity_name           text,
    opportunity_description    text,
    opportunity_signup_date    timestamp,
    opportunity_location       text,
    opportunity_wage           text,
    opportunity_tech_req       text,
    opportunity_form_req       text,
    opportunity_other_req      text,
    opportunity_benefit        text,
    opportunity_job_start_date timestamp,
    opportunity_ext_link       text,
    opportunity_home_office    text,
    expert_id                  bigint,
    opportunity_type_id        bigint references opportunity_type,
    organization_id            bigint references organization,
    user_profile_id            bigint references user_profile,
    deleted                    bool default false
) inherits (trackable);


create table opportunity_tag
(
    opportunity_tag_id bigserial primary key,
    opportunity_id     bigint references opportunity,
    tag_id             bigint references tag,
    deleted            bool default false
);

create table opportunity_job_type
(
    opportunity_job_type_id bigserial primary key,
    opportunity_id          bigint references opportunity,
    job_type_id             bigint references job_type,
    deleted                 bool
);
