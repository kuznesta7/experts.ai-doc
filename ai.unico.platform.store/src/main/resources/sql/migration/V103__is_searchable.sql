alter table organization
    drop column if exists is_searchabe;
alter table organization
    add column is_searchable boolean default false;
