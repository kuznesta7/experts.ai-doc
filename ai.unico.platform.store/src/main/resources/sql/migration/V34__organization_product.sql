create table organization_licence (
  organization_licence_id bigserial primary key,
  organization_id         bigint references organization not null,
  valid_until             timestamp,
  licence_role            text                           not null,
  deleted                 boolean
)
  inherits (trackable)