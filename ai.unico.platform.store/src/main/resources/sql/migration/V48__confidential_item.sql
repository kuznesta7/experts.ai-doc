alter table item
    rename column owner to owner_user;
alter table item
    add column owner_organization bigint references organization;
alter table item
    add column confidentiality text not null default 'PUBLIC';

