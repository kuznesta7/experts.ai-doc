drop table if exists user_pre_claim;
create table user_pre_claim
(
    user_pre_claim_id  bigserial primary key,
    user_profile_id    bigint references user_profile not null,
    user_to_claim_id   bigint references user_profile,
    expert_to_claim_id bigint,
    resolved           boolean default false          not null,
    deleted            boolean default false          not null
) inherits (trackable);

alter table user_profile
    add column if not exists invitation boolean default false;