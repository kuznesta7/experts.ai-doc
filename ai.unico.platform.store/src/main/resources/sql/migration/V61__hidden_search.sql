alter table search_hist
    add column if not exists hidden boolean not null default false;