drop table if exists claim_request;
create table claim_request (
  claim_request_id    bigserial primary key,
  type                text,
  note                text,
  concerned_user_id   bigint,
  concerned_expert_id bigint
)
  inherits (trackable);