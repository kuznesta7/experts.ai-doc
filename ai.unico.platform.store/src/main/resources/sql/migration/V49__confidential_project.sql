alter table project
    add column owner_organization_id bigint references organization;
alter table project
    add column confidentiality text not null default 'PUBLIC';

