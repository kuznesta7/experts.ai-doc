drop table if exists commercialization_project_category;

create table commercialization_project_category
(
    commercialization_project_category_id bigserial primary key,
    commercialization_project_id        bigint references commercialization_project     not null,
    commercialization_category_id       bigint references commercialization_category    not null,
    deleted                             boolean                                         not null default false
);
insert into commercialization_project_category (commercialization_project_id, commercialization_category_id)
select commercialization_id, commercialization_project.commercialization_category_id
from commercialization_project
         join commercialization_category
              on commercialization_project.commercialization_category_id = commercialization_category.commercialization_category_id;

alter table commercialization_project
drop commercialization_category_id
