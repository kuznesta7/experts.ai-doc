alter table expert_organization
    add column if not exists active bool;

alter table user_organization
    add column if not exists active bool;

update expert_organization
set active = false
where active isnull;

update user_organization
set active = false
where active isnull;
