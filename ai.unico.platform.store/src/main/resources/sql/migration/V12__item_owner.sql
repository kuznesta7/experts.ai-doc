alter table item
  add column owner bigint references user_profile;

create table expert_item (
  expert_item_id BIGSERIAL primary key,
  expert_id      BIGINT                 not null,
  item_id        BIGINT references item not null,
  deleted        BOOLEAN
)
  INHERITS (trackable);

alter table user_item
  rename column item_user_id to user_item_id;