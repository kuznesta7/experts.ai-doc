drop index if exists organization_structure_index;
create unique index organization_structure_index on organization_structure (upper_organization_id, lower_organization_id) where deleted = false;
