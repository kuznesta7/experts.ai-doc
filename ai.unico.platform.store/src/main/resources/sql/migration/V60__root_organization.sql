alter table organization
    rename column root_organization_id to parent_organization_id;
alter table organization
    add column root_organization_id bigint references organization;

select *
into root_organization_tmp
from (WITH RECURSIVE tree (organizationId, rootOrganizationId) AS (
    SELECT o.organization_id, o.parent_organization_id
    FROM organization o
             LEFT JOIN organization p on o.parent_organization_id = p.organization_id
    WHERE p.parent_organization_id ISNULL
    UNION
    SELECT o.organization_id, t.rootOrganizationId
    FROM tree t
             LEFT JOIN organization o on o.parent_organization_id = t.organizationId
    WHERE t.rootOrganizationId NOTNULL
)
      SELECT t.organizationId, t.rootOrganizationId
      FROM tree t
      where t.rootOrganizationId notnull
        and organizationId notnull) s;

alter table root_organization_tmp
    add constraint root_organization_tmp_pk
        primary key (organizationId);

update organization o
set root_organization_id = (select rootOrganizationId
                            from root_organization_tmp r
                            where r.organizationId = o.organization_id)
where parent_organization_id notnull;
drop table root_organization_tmp;
update organization o
set root_organization_id = o.organization_id
where parent_organization_id isnull;

CREATE OR REPLACE FUNCTION set_root_organization()
    RETURNS TRIGGER AS
$body$
BEGIN
    IF NEW.parent_organization_id notnull then
        NEW.root_organization_id = (WITH RECURSIVE tree (organizationId, rootOrganizationId) AS (
            SELECT o.organization_id, o.parent_organization_id
            FROM organization o
                     LEFT JOIN organization p on o.parent_organization_id = p.organization_id
            WHERE p.parent_organization_id ISNULL
            UNION
            SELECT o.organization_id, t.rootOrganizationId
            FROM tree t
                     LEFT JOIN organization o on o.parent_organization_id = t.organizationId
            WHERE t.rootOrganizationId NOTNULL
        )
                                    SELECT rootOrganizationId
                                    FROM tree t
                                    WHERE NEW.parent_organization_id = t.organizationId);
        IF NEW.root_organization_id isnull then
            NEW.root_organization_id = NEW.parent_organization_id;
        END IF;
    ELSE
        NEW.root_organization_id = NEW.organization_id;
    END IF;
    RETURN NEW;
END
$body$
    LANGUAGE plpgsql;

drop trigger if exists organization_update on organization;
CREATE TRIGGER organization_update
    BEFORE UPDATE
    ON organization
    FOR ROW
EXECUTE PROCEDURE set_root_organization();

drop trigger if exists organization_insert on organization;
CREATE TRIGGER organization_insert
    BEFORE INSERT
    ON organization
    FOR ROW
EXECUTE PROCEDURE set_root_organization();