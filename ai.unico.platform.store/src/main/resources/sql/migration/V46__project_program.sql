create table project_program
(
    project_program_id          bigserial primary key,
    original_project_program_id bigint,
    project_program_name        text not null,
    project_program_description text,
    deleted                     boolean default false
);
alter table project
    add column project_program_id bigint references project_program;