create table platform_param (
  platform_param_id bigserial primary key,
  param_key         text unique not null,
  param_value       text
)