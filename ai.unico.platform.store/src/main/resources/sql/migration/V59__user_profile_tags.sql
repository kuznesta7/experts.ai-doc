drop table user_profile_tag;

create table user_profile_keyword
(
    user_profile_keyword_id bigserial primary key,
    user_id                 bigint references user_profile,
    tag_id                  bigint references tag,
    deleted                 boolean not null default false
);

