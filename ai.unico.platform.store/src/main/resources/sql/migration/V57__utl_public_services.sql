create table commercialization_project_referent
(
    commercialization_project_referent_id bigserial primary key,
    user_id                               bigint references user_profile              not null,
    commercialization_id                  bigint references commercialization_project not null,
    deleted                               boolean default false                       not null
) inherits (trackable);

create table public_commercialization_project_request
(
    public_commercialization_project_request_id bigserial primary key,
    commercialization_id                        bigint references commercialization_project not null,
    email                                       text                                        not null,
    first_name                                  text,
    last_name                                   text,
    organization_name                           text,
    content                                     text                                        not null
) inherits (trackable);