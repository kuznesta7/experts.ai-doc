create table if not exists equipment_type
(
    equipment_type_id       bigserial primary key,
    type_name_en            varchar(256) not null
);


create table if not exists equipment
(
    equipment_id        bigserial primary key,
    name                varchar(256),
    name_en             varchar(256),
    marking             text,
    description         text,
    description_en      text,
    specialization      varchar(256),
    year                int,
    service_life        int,
    portable_device     boolean,
    type_id             bigint references equipment_type
) inherits (trackable);

create table if not exists user_equipment
(
    equipment_user_id       bigserial primary key,
    user_profile_id         bigint references user_profile,
    equipment_id            bigint references equipment
) inherits (trackable);


create table if not exists equipment_organization
(
    equipment_organization_id   bigserial primary key,
    organization_id             bigint references organization,
    equipment_id                bigint references equipment
) inherits (trackable);
