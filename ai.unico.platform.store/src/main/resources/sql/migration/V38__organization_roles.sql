alter table organization_admin
    rename to organization_user_role;
alter table organization_user_role
    add column role text;
update organization_user_role
set role='ORGANIZATION_EDITOR';
insert into organization_user_role (organization_id, user_id, role)
select organization_id, user_id, 'ORGANIZATION_ADMIN'
from organization_user_role
where manage_admins_permission = true;
alter table organization_user_role
    drop column manage_admins_permission;

alter table organization
    drop column organization_admin_limit;
alter table organization_licence
    add column number_of_user_licences integer default 3;
alter table organization_user_role
    rename column organization_admin_id to organization_user_role_id;
update organization_licence
set licence_role='ORGANIZATION_EDITOR'
where licence_role = 'PREMIUM_USER';