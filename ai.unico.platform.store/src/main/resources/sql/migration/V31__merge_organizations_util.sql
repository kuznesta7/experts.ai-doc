alter table organization
  add column user_insert bigint,
  add column user_update bigint,
  add column date_insert timestamp,
  add column date_update timestamp,
  inherit trackable;