create table project_document
(
    project_document_id                   bigserial primary key,
    project_id                            bigint references project,
    file_id                               bigint references file,
    deleted                               boolean not null default false
) inherits (trackable);
