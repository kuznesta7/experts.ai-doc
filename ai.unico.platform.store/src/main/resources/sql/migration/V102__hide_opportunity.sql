alter table opportunity
    drop column if exists hidden;
alter table opportunity
    add column hidden bool default false;
