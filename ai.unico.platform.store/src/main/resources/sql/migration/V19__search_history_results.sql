create table search_hist_result (
  search_hist_result_id bigserial primary key,
  search_hist_id        bigint references search_hist not null,
  result_user_id        bigint,
  result_expert_id      bigint,
  position              int
);

alter table user_profile
  add column claimable boolean default false;
alter table user_profile
  add column claimed_by bigint references user_profile;
alter table user_item
  add column claimed_user_id bigint references user_profile;