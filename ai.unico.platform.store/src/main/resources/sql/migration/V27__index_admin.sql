create table index_history (
  index_history_id bigserial primary key,
  target           text,
  clean            boolean,
  status           text
)
  inherits (trackable);