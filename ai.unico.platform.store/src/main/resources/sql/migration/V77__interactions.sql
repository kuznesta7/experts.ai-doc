drop table if exists interaction_widget;
create table interaction_widget
(
    interaction_widget_id bigserial primary key not null,
    user_id               bigint references user_profile,
    organization_id       bigint references organization,
    interaction_type      text,
    date                  timestamp,
    session_id            text,
    interaction_details   text
);
drop table if exists interaction_individual;
create table interaction_individual
(
    interaction_individual_id    bigserial primary key not null,
    user_id                      bigint references user_profile,
    organization_id              bigint references organization,
    date                         timestamp,
    session_id                   text,
    visited_expert_id            bigint,
    visited_user_id              bigint references user_profile,
    visited_outcome_id           bigint,
    visited_project_id           bigint,
    visited_commercialization_id bigint,
    item_location                text
)

