drop table if exists query_translation CASCADE;
create table query_translation
(
    translation_id          bigserial primary key not null,
    translation_phrase      text unique           not null,
    exclude_invest_projects bool default false,
    exclude_outcomes        bool default true,
    year_from               integer,
    year_to                 integer,
    full_name_query         text,
    query                   text,
    deleted                 bool default false
);
drop table if exists organization_query_translation RESTRICT;
create table organization_query_translation
(
    translation_id  bigserial,
    organization_id bigserial,
    foreign key (translation_id) references query_translation,
    foreign key (organization_id) references organization
);

drop table if exists item_type_group_query_translation RESTRICT;
create table item_type_group_query_translation
(
    translation_id bigserial,
    type_group_id  bigserial,
    foreign key (translation_id) references query_translation,
    foreign key (type_group_id) references item_type_group
);
drop table if exists item_type_query_translation RESTRICT;
create table item_type_query_translation
(
    translation_id bigserial,
    type_id        bigserial,
    foreign key (translation_id) references query_translation,
    foreign key (type_id) references item_type
);


drop table if exists country_query_translation RESTRICT;
create table country_query_translation
(
    translation_id bigserial,
    country_code   varchar(2),
    foreign key (translation_id) references query_translation,
    foreign key (country_code) references country
);