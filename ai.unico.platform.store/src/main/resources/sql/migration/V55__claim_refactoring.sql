alter table user_item
    add column expert_id_from_claim bigint;
update user_item
set expert_id_from_claim = expert_id
where expert_id notnull;
update user_item
set expert_id = null
where expert_id notnull;
insert into user_item(item_id, expert_id, deleted)
select item_id, expert_id, deleted
from expert_item;
alter table user_item
    rename to user_expert_item;
drop table expert_item;

alter table project_user
    add column expert_id bigint;
alter table project_user
    alter column user_id drop not null;
insert into project_user(project_id, expert_id, project_organization_id, deleted)
select project_id, expert_id, project_organization_id, deleted
from project_expert;
alter table project_user
    rename to user_expert_project;
drop table project_expert;

