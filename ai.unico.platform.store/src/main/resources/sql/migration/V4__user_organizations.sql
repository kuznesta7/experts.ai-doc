CREATE TABLE organization (
  organization_id          BIGSERIAL PRIMARY KEY,
  organization_name        TEXT,
  organization_description TEXT,
  registration_number      TEXT
);

CREATE TABLE user_organization (
  user_organization_id BIGSERIAL PRIMARY KEY,
  user_id              BIGINT REFERENCES user_profile,
  organization_id      BIGINT REFERENCES organization,
  deleted              BOOLEAN DEFAULT FALSE
)
  INHERITS (trackable);