CREATE TABLE http_request (
  http_request_id BIGSERIAL PRIMARY KEY NOT NULL,
  accepted TIMESTAMP,
  duration int,
  method VARCHAR(8),
  system VARCHAR(8),
  path TEXT,
  remote_user TEXT,
  result VARCHAR(3),
  query TEXT
);