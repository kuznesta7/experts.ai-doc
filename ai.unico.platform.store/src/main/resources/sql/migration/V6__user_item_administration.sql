ALTER TABLE item_user
  RENAME TO user_item;

ALTER TABLE user_item
  ADD COLUMN hidden boolean default false;

ALTER TABLE organization
  add column root_organization_id BIGINT references organization;
ALTER TABLE organization
  add column original_organization_id BIGINT unique;

ALTER TABLE item_tag
  add column deleted boolean default false;