alter table file
    add column hash text;
update file
set hash = md5(content)
where content notnull;

CREATE OR REPLACE FUNCTION set_hash()
    RETURNS TRIGGER AS
$body$
BEGIN
    NEW.hash = md5(NEW.content);
    RETURN NEW;
END
$body$
    LANGUAGE plpgsql;

drop trigger if exists file_change on file;
CREATE TRIGGER file_change
    BEFORE UPDATE
    ON file
    FOR ROW
EXECUTE PROCEDURE set_hash();

drop trigger if exists file_insert on file;
CREATE TRIGGER file_insert
    BEFORE INSERT
    ON file
    FOR ROW
EXECUTE PROCEDURE set_hash();