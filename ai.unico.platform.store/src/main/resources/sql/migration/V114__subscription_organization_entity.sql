create table if not exists subscription_organization
(
    id bigserial primary key,
    widget_email_subscription_id bigint references widget_email_subscription,
    organization_id bigint references organization,
    subscription_type_id bigint references subscription_type,
    deleted boolean default false,
    user_token text
) inherits (trackable);
