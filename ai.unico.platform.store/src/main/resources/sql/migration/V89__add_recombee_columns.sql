alter table organization
    add column if not exists recombee_private_token varchar;
alter table organization
    add column if not exists recombee_db_identifier varchar;
