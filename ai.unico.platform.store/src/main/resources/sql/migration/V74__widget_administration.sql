drop table if exists organization_widget;
create table organization_widget
(
    organization_widget_id bigserial primary key,
    organization_id        bigint references organization,
    widget_type            text               not null,
    allowed                bool default false not null
) inherits (trackable);