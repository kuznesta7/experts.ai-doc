CREATE TABLE item_type_group (
  type_group_id BIGINT PRIMARY KEY,
  title         TEXT
);

CREATE TABLE item_type (
  type_id       BIGINT PRIMARY KEY,
  title         TEXT,
  code          TEXT,
  description   TEXT,
  type_group_id BIGINT REFERENCES item_type_group
);

INSERT INTO item_type_group (type_group_id, title)
VALUES (1, 'PUBLICATIONS');
INSERT INTO item_type_group (type_group_id, title)
VALUES (2, 'PATENTS');
INSERT INTO item_type_group (type_group_id, title)
VALUES (3, 'APPLIED_RESULTS');

INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (7, 'PV-PCT', NULL, 2);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (6, 'PV-zah', NULL, 2);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (9, 'PV-nar', NULL, 2);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (8, 'EP', NULL, NULL);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (15, 'Z', NULL, 3);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (16, 'F', NULL, 3);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (10, 'J', NULL, 1);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (11, 'B', NULL, 1);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (14, 'P', NULL, 2);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (12, 'C', NULL, 1);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (13, 'D', NULL, 1);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (17, 'G', NULL, 3);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (18, 'H', NULL, 3);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (19, 'N', NULL, 3);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (20, 'R', NULL, 3);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (21, 'V', NULL, 1);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (22, 'A', NULL, 3);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (23, 'E', NULL, NULL);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (24, 'M', NULL, NULL);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (25, 'W', NULL, NULL);
INSERT INTO item_type (type_id, code, description, type_group_id)
VALUES (26, 'O', NULL, NULL);

UPDATE item_type
SET title = upper(concat('ITEM_TYPE_', code));

ALTER TABLE item
  ADD COLUMN item_type_id BIGINT REFERENCES item_type;
ALTER TABLE item
  ADD COLUMN year INTEGER;
ALTER TABLE item
  DROP COLUMN item_type;
ALTER TABLE item
  DROP COLUMN date_published;

CREATE TABLE item_tag (
  item_tag_id BIGSERIAL PRIMARY KEY,
  item_id     BIGINT REFERENCES item NOT NULL,
  tag_id      BIGINT REFERENCES tag  NOT NULL,
  outdated    BOOLEAN DEFAULT FALSE
);