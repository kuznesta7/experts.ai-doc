alter table user_organization
    add column if not exists retired bool;

update user_organization
set retired = false
where retired isnull;
