DROP VIEW IF EXISTS organization_structure_view;
CREATE VIEW organization_structure_view as (
    select (case
                when (lower_organization.substitute_organization_id notnull)
                    then lower_organization.substitute_organization_id
                else lower_organization_id end) as lower_organization_id,
           (case
                when (upper_organization.substitute_organization_id notnull)
                    then upper_organization.substitute_organization_id
                else upper_organization_id end) as upper_organization_id,
           relation_type
    from organization_structure
             left join organization lower_organization
                       on organization_structure.lower_organization_id = lower_organization.organization_id
             left join organization upper_organization
                       on organization_structure.upper_organization_id = upper_organization.organization_id
);

