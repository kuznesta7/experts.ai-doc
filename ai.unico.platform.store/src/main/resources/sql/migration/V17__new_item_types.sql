INSERT INTO item_type (type_id, title, code, type_group_id, weight)
VALUES (31, 'ITEM_TYPE_JO', 'JO', 1, 0.1),
       (32, 'ITEM_TYPE_JA', 'JA', 1, 1.2),
       (33, 'ITEM_TYPE_JB', 'JB', 1, 0.8),
       (34, 'ITEM_TYPE_JC', 'JC', 1, 0.1);