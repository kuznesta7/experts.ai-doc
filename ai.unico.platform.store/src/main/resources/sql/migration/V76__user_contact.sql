drop table if exists external_message;
create table external_message
(
    message_id      bigserial primary key not null,
    sender_email    text                  not null,
    message         text                  not null,
    organization_id bigserial             not null,
    user_id         bigint,
    expert_id       bigint,
    time_send       timestamp,
    foreign key (organization_id) references organization,
    foreign key (user_id) references user_profile
);
alter table external_message
    add constraint filled_message_target
        check (
                (user_id is null and expert_id is not null) or
                (user_id is not null and expert_id is null)
            );