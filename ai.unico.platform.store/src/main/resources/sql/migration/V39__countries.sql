create table country
(
    country_code   varchar(2) not null primary key,
    country_name   text       not null,
    country_abbrev text       not null,
    published      boolean default true
);

insert into country(country_code, country_name, country_abbrev, published)
values ('cz', 'Czechia', 'CZ', true),
       ('sk', 'Slovakia', 'SK', false),
       ('pl', 'Poland', 'PL', false),
       ('de', 'Germany', 'DE', false),
       ('us', 'United States of America', 'USA', false),
       ('at', 'Austria', 'AT', false);

alter table user_profile
    drop column country;

create table user_country_of_interest
(
    user_country_of_interest_id bigserial primary key,
    country_code                varchar(2) not null references country,
    user_id                     bigint     not null references user_profile,
    deleted                     boolean default false
) inherits (trackable);

alter table organization
    add column organization_country_code varchar(2) references country;