create table commercialization_project_organization
(
    commercialization_project_organization_id bigserial primary key,
    commercialization_id                      bigint                not null references commercialization_project,
    organization_id                           bigint references organization,
    relation                                  text                  not null,
    deleted                                   boolean default false not null
) inherits (trackable);

insert into commercialization_project_organization (commercialization_id, organization_id, relation)
select commercialization_id, intellectual_property_owner_id, 'INTELLECTUAL_PROPERTY_OWNER'
from commercialization_project
where intellectual_property_owner_id notnull;

alter table commercialization_project
    drop column intellectual_property_owner_id;

alter table commercialization_project
    add column owner_organization_id bigint references organization;