alter table organization
    add column street text;
alter table organization
    add column city text;
alter table organization
    add column phone text;
alter table organization
    add column email text;
alter table organization
    add column web text;
alter table organization
    add column organization_cover_img_file_id bigint references file;

-- create table organization_profile_keyword
-- (
--     organization_profile_keyword_id bigserial primary key,
--     organization_id                 bigint references organization not null,
--     tag_id                          bigint references tag          not null,
--     deleted                         boolean default false
-- );

alter table user_profile
    add column cover_img_file_id bigint references file;
