create table organization_profile_visibility
(
    organization_id  bigint primary key references organization,
    visible          boolean not null default false,
    keywords_visible boolean not null default false,
    graph_visible    boolean not null default false,
    experts_visible  boolean not null default false,
    outcomes_visible boolean not null default false,
    projects_visible boolean not null default false
);
insert into organization_profile_visibility
select organization_id,
       has_public_profile,
       has_public_profile,
       has_public_profile,
       has_public_profile,
       has_public_profile,
       has_public_profile
from organization
where has_public_profile;

alter table organization
    drop column has_public_profile;
