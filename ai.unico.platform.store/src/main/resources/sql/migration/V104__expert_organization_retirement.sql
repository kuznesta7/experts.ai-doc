alter table expert_organization
    add column if not exists retired bool;

update expert_organization
set retired = false
where retired isnull;
