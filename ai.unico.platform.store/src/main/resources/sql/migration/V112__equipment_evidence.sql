alter  table if exists equipment_organization
    add column if not exists deleted bool default false;
alter  table if exists user_equipment
    add column if not exists deleted bool default false;
alter  table if exists equipment
    add column if not exists hidden bool default false;
alter  table if exists equipment
    add column if not exists deleted bool default false;
alter  table if exists equipment
    drop column if exists specialization;

create table if not exists equipment_tag(
                                            equipment_tag_id bigserial primary key,
                                            equipment_id bigserial references equipment,
                                            tag_id bigserial references tag,
                                            outdated bool,
                                            deleted bool
) inherits (trackable);
alter table if exists equipment
    add column if not exists original_id bigint;
