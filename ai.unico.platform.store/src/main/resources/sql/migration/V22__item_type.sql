alter table item_type
  add column addable boolean default true;
update item_type
set addable = false
where type_id >= 27;