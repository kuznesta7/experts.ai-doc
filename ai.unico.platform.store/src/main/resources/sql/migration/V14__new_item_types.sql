INSERT INTO item_type (type_id, title, code, type_group_id, weight)
VALUES (27, 'ITEM_TYPE_J1', 'J1', 1, 1.4),
       (28, 'ITEM_TYPE_J2', 'J2', 1, 0.8),
       (29, 'ITEM_TYPE_J3', 'J3', 1, 0.6),
       (30, 'ITEM_TYPE_J4', 'J4', 1, 0.1);