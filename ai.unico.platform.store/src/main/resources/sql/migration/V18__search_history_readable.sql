alter table search_hist_secondary_query
  add column weight real;
alter table search_hist
  add column number_of_results_found bigint;
alter table search_hist
  add column results_limit integer;
alter table search_hist
  add column deleted boolean;
alter table search_hist
  add column favorite boolean;

update search_hist
set deleted  = false,
    favorite = false;

delete
from search_hist_secondary_query;

create table followed_profile (
  followed_profile_id bigserial primary key,
  follower_user_id    bigint references user_profile not null,
  followed_user_id    bigint references user_profile,
  followed_expert_id  bigint,
  deleted             boolean default false
)
  inherits (trackable);

create table user_workspace (
  user_workspace_id bigserial primary key,
  user_id           bigint references user_profile,
  workspace_name    text,
  workspace_note    text,
  deleted           boolean
)
  inherits (trackable);

create table search_hist_workspace (
  search_hist_workspace_id bigserial primary key,
  user_workspace_id        bigint references user_workspace,
  search_hist_id           bigint references search_hist,
  deleted                  boolean
)
  inherits (trackable);

create table followed_profile_workspace (
  followed_profile_workspace_id bigserial primary key,
  user_workspace_id             bigint references user_workspace,
  followed_profile_id           bigint references followed_profile,
  deleted                       boolean
)
  inherits (trackable);
