insert into item_type_group
values (5, 'Theses')
returning type_group_id;

INSERT
INTO item_type
values (68, 'BC - Bachelor Theses', 'BC', null, 5, 0.2, false, false, false),
       (69, 'MA - Master Theses', 'MA', null, 5, 0.2, false, false, false);

