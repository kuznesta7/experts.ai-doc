CREATE TABLE IF NOT EXISTS item_not_registered_expert
(
    id          BIGSERIAL PRIMARY KEY,
    item_id     BIGINT,
    expert_name VARCHAR(255) NOT NULL,
    FOREIGN KEY (item_id) REFERENCES item (item_id)
);
