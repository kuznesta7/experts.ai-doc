alter table user_profile
    add column retirement_date timestamp default null;

create table expert
(
    expert_id       bigint primary key,
    retirement_date timestamp default null
) inherits (trackable)