create table expert_organization
(
    expert_organization_id bigserial primary key,
    organization_id        bigint not null references organization,
    expert_id              bigint not null,
    verified               boolean default false,
    deleted                boolean default false
) inherits (trackable);