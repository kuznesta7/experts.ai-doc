alter table commercialization_status_type
    add column must_be_filled boolean default true;
alter table commercialization_status_type
    add column status_order bigint;
update commercialization_status_type
set status_order = commercialization_status_id
where commercialization_status_type.status_order IS NULL;
alter table commercialization_project
    alter column commercialization_priority_id drop not null;
