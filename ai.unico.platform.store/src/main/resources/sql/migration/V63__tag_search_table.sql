drop table if exists search_statistics RESTRICT;
create table search_statistics
(
    statistic_id    bigserial primary key not null,
    value           text unique           not null,
    searches        bigint  default 1,
    unique_searches bigint  default 1,
    hidden          boolean default false,
    hot_topic       boolean default false
);


create or replace function update_tag_statistic()
    returns trigger as
$body$
begin
    if new.query is not null and not new.hidden then
        insert into search_statistics (value, hidden)
        values (new.query, new.query_length > 30)
        on conflict (value) do update set searches        = search_statistics.searches + 1,
                                          unique_searches = (select count(*)
                                                             from (select distinct user_insert
                                                                   from search_hist
                                                                   where query = new.query
                                                                     and hidden = new.hidden) as templ);
    end if;
    return new;
end
$body$
    LANGUAGE plpgsql;

drop trigger if exists search_hist_update on search_hist;
create trigger search_hist_update
    after insert
    on search_hist
    for row
execute procedure update_tag_statistic();

truncate search_statistics;
insert into search_statistics (value, searches, unique_searches)
select query, count(user_insert), count(distinct user_insert)
from search_hist
where not hidden
  and query is not null
group by query;

update search_statistics
set hidden = true
where length(value) > 30;

insert into search_statistics (value, hot_topic)
values ('lung ventilation', true)
on conflict (value) do update set hot_topic = true;
insert into search_statistics (value, hot_topic)
values ('3D print ventilation', true)
on conflict (value) do update set hot_topic = true;
insert into search_statistics (value, hot_topic)
values ('crisis management pandemic', true)
on conflict (value) do update set hot_topic = true;
insert into search_statistics (value, hot_topic)
values ('pandemic plan', true)
on conflict (value) do update set hot_topic = true;
insert into search_statistics (value, hot_topic)
values ('p3 filter', true)
on conflict (value) do update set hot_topic = true;
insert into search_statistics (value, hot_topic)
values ('particle filter mask', true)
on conflict (value) do update set hot_topic = true;
insert into search_statistics (value, hot_topic)
values ('nanofibre composite', true)
on conflict (value) do update set hot_topic = true;
insert into search_statistics (value, hot_topic)
values ('low-carbon building', true)
on conflict (value) do update set hot_topic = true;
insert into search_statistics (value, hot_topic)
values ('recommender system', true)
on conflict (value) do update set hot_topic = true;
insert into search_statistics (value, hot_topic)
values ('renewable energy', true)
on conflict (value) do update set hot_topic = true;
insert into search_statistics (value, hot_topic)
values ('computer vision', true)
on conflict (value) do update set hot_topic = true;
insert into search_statistics (value, hot_topic)
values ('remote patient monitoring', true)
on conflict (value) do update set hot_topic = true;