create table if not exists recommended_items (
    recomm_id       varchar(256) primary key,
    widget_type     varchar(64),
    user_hash       varchar(256),
    recommendation  text,
    num_of_recomms  int,
    recomm_time     timestamp
)
