alter table expert_item
  add column claimed boolean default false;
update expert_item
set claimed = deleted;
update expert_item
set deleted = false;