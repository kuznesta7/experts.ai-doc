create table commercialization_project_document
(
    commercialization_project_document_id bigserial primary key,
    commercialization_id                  bigint references commercialization_project,
    file_id                               bigint references file,
    deleted                               boolean not null default false
) inherits (trackable);

create table file_access
(
    file_access_id bigserial primary key,
    access_user_id bigint,
    access_date    timestamp not null,
    file_id        bigint    not null references file
);

alter table file
    add column size integer;

update file
set size = pg_column_size(content)
where content notnull;

drop trigger if exists file_change on file;
drop trigger if exists file_insert on file;
drop function set_hash();

CREATE OR REPLACE FUNCTION set_file_metadata()
    RETURNS TRIGGER AS
$body$
BEGIN
    NEW.hash = md5(NEW.content);
    NEW.size = pg_column_size(NEW.content);
    RETURN NEW;
END
$body$
    LANGUAGE plpgsql;

CREATE TRIGGER file_change
    BEFORE UPDATE
    ON file
    FOR ROW
EXECUTE PROCEDURE set_file_metadata();

CREATE TRIGGER file_insert
    BEFORE INSERT
    ON file
    FOR ROW
EXECUTE PROCEDURE set_file_metadata();