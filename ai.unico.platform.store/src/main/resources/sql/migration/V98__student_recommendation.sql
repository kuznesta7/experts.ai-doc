create table if not exists student
(
    student_hash   text primary key,
    username       text,
    recommendation text,
    test_group     text
);
