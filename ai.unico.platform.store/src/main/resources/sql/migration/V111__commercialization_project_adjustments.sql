alter table commercialization_project add column if not exists link varchar(2048);

update commercialization_status_type
set deleted=true, active_until=now()
where deleted=false and code!='draft';

insert into commercialization_status_type(code, name, system_data, active_from, display_order, deleted, use_in_search, use_in_invest, can_set_organization, must_be_filled, status_order)
values ('public', 'Public', false, now(), 90, false, true, false, false, false, 9),
       ('sold', 'Sold', false, now(), 100, false, true, false, false, false, 10);

insert into commercialization_project_status(date_insert, date_update, commercialization_project_id, commercialization_status_type_id, outdated)
select now() date_insert, now() date_update, commercialization_project_id, 9 commercialization_status_type_id, false outdated from commercialization_project_status
where outdated=false and commercialization_status_type_id!=9;

update commercialization_project_status
set outdated=true
where outdated=false and commercialization_status_type_id!=9;
