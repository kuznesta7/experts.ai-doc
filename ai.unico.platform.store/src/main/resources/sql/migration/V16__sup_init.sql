drop table if exists private_project cascade;
drop table if exists project_budget cascade;
drop table if exists project_state cascade;
drop table if exists project_transaction cascade;
drop table if exists project_type cascade;
drop table if exists project cascade;

create table project_type (
  project_type_id   bigserial primary key     not null,
  project_type_name text                      not null
);

create table project (
  project_id          bigserial primary key  not null,
  original_project_id bigint                 null,
  name                text                   not null,
  description         text,
  start_date          timestamp,
  end_date            timestamp,
  project_type_id     bigint references project_type,
  deleted             boolean default false  not null
)
  INHERITS (trackable);

create table private_project (
  private_project_id     bigserial primary key          not null,
  project_id             bigint references project      not null,
  organization_id        bigint references organization not null,
  parent_private_project bigint references private_project,
  hidden                 boolean default false          not null,
  deleted                boolean default false          not null
)
  INHERITS (trackable);

create table project_budget (
  project_budget_id bigserial primary key  not null,
  date_from         timestamp              not null,
  date_to           timestamp              not null,
  amount            double precision       not null,
  currency          text                   not null,
  deleted           boolean default false  not null
)
  INHERITS (trackable);

create table project_state (
  project_state_id bigserial primary key     not null,
  project_id       bigint references project not null,
  state            text                      not null,
  outdated         boolean                   not null
)
  INHERITS (trackable);


create table project_transaction (
  project_transaction_id bigserial primary key          not null,
  project_id             bigint references project      not null,
  organization_from_id   bigint references organization,
  organization_to_id     bigint references organization,
  transaction_note       text,
  amount                 double precision               not null,
  currency               text                           not null,
  transaction_date       timestamp                      not null,
  deleted                boolean default false          not null
)
  INHERITS (trackable);

create table project_expert (
  project_expert_id  bigserial primary key          not null,
  project_id         bigint references project,
  private_project_id bigint references private_project,
  expert_id          bigint                         not null,
  deleted            boolean default false          not null,
  label              text
)
  inherits (trackable);

create table project_user (
  project_user_id    bigserial primary key          not null,
  user_id            bigint                         not null references user_profile,
  project_id         bigint references project,
  private_project_id bigint references private_project,
  deleted            boolean default false          not null,
  label              text
)
  inherits (trackable);

create table project_item (
  project_item_id bigserial primary key          not null,
  project_id      bigint                         not null references project,
  item_id         bigint                         not null references item,
  deleted         boolean default false          not null
)
  inherits (trackable);

CREATE TABLE project_tag (
  project_tag_id BIGSERIAL PRIMARY KEY,
  project_id     BIGINT REFERENCES project NOT NULL,
  tag_id         BIGINT REFERENCES tag     NOT NULL,
  deleted        BOOLEAN DEFAULT FALSE
)
  inherits (trackable);