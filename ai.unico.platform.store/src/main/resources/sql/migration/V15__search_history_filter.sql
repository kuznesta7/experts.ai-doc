alter table search_hist
  add column full_name_query text,
  add column page integer,
  add column ontology_disabled boolean,
  add column year_from integer,
  add column year_to integer;

alter table search_hist
  rename column search_expression to query;

alter table search_hist
  drop column search_type;

create table search_hist_secondary_query (
  id             BIGSERIAL PRIMARY KEY,
  search_hist_id BIGINT REFERENCES search_hist,
  query          text
);
create table search_hist_disabled_secondary_query (
  id             BIGSERIAL PRIMARY KEY,
  search_hist_id BIGINT REFERENCES search_hist,
  query          text
);
create table search_hist_result_type_group (
  id                   BIGSERIAL PRIMARY KEY,
  search_hist_id       BIGINT REFERENCES search_hist,
  result_type_group_id BIGINT
);
create table search_hist_organization (
  id              BIGSERIAL PRIMARY KEY,
  search_hist_id  BIGINT REFERENCES search_hist,
  organization_id BIGINT
);
