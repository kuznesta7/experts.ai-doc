alter table interaction_individual
    add column if not exists visited_expert_code  text,
    add column if not exists visited_outcome_code text,
    add column if not exists visited_project_code text;
