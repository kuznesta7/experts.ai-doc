alter table expert_organization
    add column if not exists visible bool;

alter table user_organization
    add column if not exists visible bool;

update expert_organization
set visible = true
where visible isnull;

update user_organization
set visible = true
where visible isnull;
