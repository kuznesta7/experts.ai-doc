create table evidence_import
(
    evidence_import_id bigserial primary key,
    file_id            bigint references file,
    organization_id    bigint references organization not null,
    note               text,
    evidence_category  text
) inherits (trackable);