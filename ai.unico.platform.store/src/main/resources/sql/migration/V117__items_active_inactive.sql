alter table if exists item_organization
    add column if not exists active boolean default true;

update item_organization
    set active = true
    where active is null;
