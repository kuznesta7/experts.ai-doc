alter table file
    add column deleted bool default false;
alter table file
    alter content drop not null;