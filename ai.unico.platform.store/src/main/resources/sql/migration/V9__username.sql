DROP EXTENSION IF EXISTS unaccent;
CREATE EXTENSION unaccent;
alter table user_profile
  add column username text;
update user_profile
set username = unaccent(lower(concat(replace(user_profile.name, ' ', '.'), '.', user_profile.user_id)));