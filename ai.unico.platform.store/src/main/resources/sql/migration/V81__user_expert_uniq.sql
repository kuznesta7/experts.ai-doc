drop index if exists user_expert_index;
create unique index user_expert_index on user_expert (expert_id) where deleted = false;
