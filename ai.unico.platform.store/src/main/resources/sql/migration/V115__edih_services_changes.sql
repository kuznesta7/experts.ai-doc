create table language (
    language_code varchar(15) not null primary key,
    language_name varchar(255) not null unique,
    language_name_en varchar(255) not null unique
);
insert into language (language_code, language_name, language_name_en) values ('en', 'English', 'English');
insert into language (language_code, language_name, language_name_en) values ('cz', 'Čeština', 'Czech');

alter table if exists equipment
    add column if not exists good_for_description text,
    add column if not exists target_group text,
    add column if not exists preparation_time text,
    add column if not exists equipment_cost text,
    add column if not exists infrastructure_description text,
    add column if not exists duration_time text,
    add column if not exists language_code varchar(15) references language (language_code) default 'en',
    add column if not exists trl text,
    drop column if exists name_en,
    drop column if exists description_en;

alter table equipment_tag
    drop constraint if exists equipment_tag_equipment_id_fkey;
alter table equipment_organization
    drop constraint if exists equipment_organization_equipment_id_fkey;
alter table user_equipment
    drop constraint if exists user_equipment_equipment_id_fkey;


alter table equipment
    drop constraint equipment_pkey,
    add primary key (equipment_id, language_code);

alter table equipment_tag
    add column if not exists language_code varchar(15) default 'en',
    add constraint equipment_tag_equipment_id_fkey foreign key (equipment_id, language_code) references equipment (equipment_id, language_code) on delete cascade;

alter table equipment_organization
    add column if not exists language_code varchar(15) default 'en',
    add constraint equipment_organization_equipment_id_fkey foreign key (equipment_id, language_code) references equipment (equipment_id, language_code) on delete cascade;

alter table user_equipment
    add column if not exists language_code varchar(15) default 'en',
    add constraint user_equipment_equipment_id_fkey foreign key (equipment_id, language_code) references equipment (equipment_id, language_code) on delete cascade;
