create table share_code
(
    share_code_id    bigserial primary key,
    code             text   not null,
    shared_entity_id bigint not null,
    type             text   not null,
    deleted          boolean default false
) inherits (trackable);

alter table search_hist
    add column share_code text;