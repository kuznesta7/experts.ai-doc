alter table external_message
    drop constraint filled_message_target;
alter table external_message
    add constraint filled_message_target
        check (
            user_id is null or expert_id is null);
