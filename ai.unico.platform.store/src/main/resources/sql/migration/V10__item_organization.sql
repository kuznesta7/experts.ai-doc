create table item_organization (
  item_organization_id BIGSERIAL PRIMARY KEY,
  organization_id      BIGINT REFERENCES organization,
  item_id              BIGINT REFERENCES item,
  deleted              BOOLEAN DEFAULT false
)
  INHERITS (trackable);

create table organization_admin (
  organization_admin_id BIGSERIAL PRIMARY KEY,
  organization_id       BIGINT REFERENCES organization,
  user_id               BIGINT REFERENCES user_profile,
  deleted               BOOLEAN DEFAULT false
)
  INHERITS (trackable)