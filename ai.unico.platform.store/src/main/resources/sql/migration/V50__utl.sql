drop table if exists commercialization_team_member;
drop table if exists commercialization_project_keyword;
drop table if exists commercialization_project_status;
drop table if exists commercialization_project;
drop table if exists commercialization_domain;
drop table if exists commercialization_investment_range;
drop table if exists commercialization_priority;
drop table if exists commercialization_status_category;
drop table if exists commercialization_status_type;
drop table if exists commercialization_category;
create extension if not exists "uuid-ossp";
-- reference_data = template for reference data tables, no data stored here
create table if not exists reference_data
(
    code          varchar(20) unique not null,
    name          varchar(30)        not null,
    system_data   boolean            not null default false,
    -- system_data = record CAN NOT be deleted and "code" CAN NOT be changed by end user
    description   text,
    active_from   timestamp          not null default current_timestamp,
    active_until  timestamp,
    display_order integer                     default 100,
    deleted       boolean            not null default false
    -- how data are presented to end users can be set explicitly; implicit is alphabetically by "name"
    -- => ... ORDER BY display_order, name
);
-- commercialization_category
create table commercialization_category
(
    commercialization_category_id bigserial primary key
) inherits (trackable, reference_data);
-- commercialization_status
create table commercialization_status_type
(
    commercialization_status_id bigserial primary key,
    use_in_search               boolean not null default false,
    use_in_invest               boolean not null default false,
    can_set_organization        boolean not null default false
) inherits (trackable, reference_data);
-- commercialization_status_category = what statuses are relevant for each category
create table commercialization_status_category
(
    commercialization_status_category_id bigserial primary key                           not null,
    commercialization_status_id          bigint references commercialization_status_type not null,
    commercialization_category_id        bigint references commercialization_category    not null,
    deleted                              boolean                                         not null default false
) inherits (trackable);

create unique index commercialization_status_category_un on commercialization_status_category (commercialization_status_id, commercialization_category_id);

-- commercialization_priority
create table commercialization_priority
(
    commercialization_priority_id bigserial primary key
) inherits (trackable, reference_data);
-- commercialization_investment_range
create table commercialization_investment_range
(
    commercialization_investment_range_id bigserial primary key,
    investment_from                       integer,
    investment_to                         integer
) inherits (trackable, reference_data);
-- commercialization_domain
create table commercialization_domain
(
    commercialization_domain_id bigserial primary key
) inherits (trackable, reference_data);
-- commercialization
create table commercialization_project
(
    -- IDs
    commercialization_id                  bigserial primary key                        not null,
    commercialization_uuid                uuid unique                                  not null default uuid_generate_v4(),

    -- reference fields
    commercialization_category_id         bigint references commercialization_category not null,
    commercialization_priority_id         bigint references commercialization_priority not null,
    intellectual_property_owner_id        bigint references organization               not null,
    intermediary_organization_id          bigint references organization,
    commercialization_investment_range_id bigint references commercialization_investment_range,
    commercialization_domain_id           bigint references commercialization_domain,

    -- descriptive fields
    name                                  text,
    executive_summary                     text,
    use_case                              text,
    pain_description                      text,
    competitive_advantage                 text,
    technical_principles                  text,
    technology_readiness_level            integer,
    start_date                            timestamp,
    end_date                              timestamp,
    status_deadline                       timestamp,
    img_file_id                           bigint references file,
    commercialization_investment_from     integer,
    commercialization_investment_to       integer,
    idea_score                            integer,
    deleted                               boolean                                      not null default false
) inherits (trackable);

-- commercialization_team_member
create table commercialization_team_member
(
    -- IDs
    commercialization_team_member_id bigserial primary key                       not null,

    -- reference fields
    commercialization_id             bigint references commercialization_project not null,
    user_profile_id                  bigint references user_profile,
    expert_id                        bigint, -- ?? references expert not null,
    team_leader                      boolean                                     not null default false,
    deleted                          boolean                                     not null default false
) inherits (trackable);

create unique index commercialization_team_member_un on commercialization_team_member (commercialization_id, user_profile_id, expert_id);

create table commercialization_project_status
(
    commercialization_project_status_id bigserial primary key,
    commercialization_project_id        bigint references commercialization_project     not null,
    commercialization_status_type_id    bigint references commercialization_status_type not null,
    outdated                            boolean                                         not null default false
) inherits (trackable);

create table commercialization_project_keyword
(
    commercialization_project_keyword_id bigserial primary key,
    tag_id                               bigint references tag not null,
    commercialization_project_id         bigint references commercialization_project,
    deleted                              boolean               not null default false
);
