alter table followed_profile
    drop column note;

create table profile_personal_note
(
    profile_note_id bigserial primary key,
    from_user_id    bigint references user_profile,
    to_user_id      bigint references user_profile,
    expert_id       bigint,
    note            text,
    deleted         boolean default false
) inherits (trackable)