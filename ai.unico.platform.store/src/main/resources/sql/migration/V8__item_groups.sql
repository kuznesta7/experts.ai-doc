insert into item_type_group (type_group_id, title)
values (4, 'OTHER_RESULTS');

UPDATE public . item_type
SET title         = 'ITEM_TYPE_PV-ZAH',
    code          = 'PV-zah',
    description   = null,
    type_group_id = 2
WHERE type_id = 6;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_PV-PCT',
    code          = 'PV-PCT',
    description   = null,
    type_group_id = 2
WHERE type_id = 7;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_EP',
    code          = 'EP',
    description   = null,
    type_group_id = 2
WHERE type_id = 8;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_PV-NAR',
    code          = 'PV-nar',
    description   = null,
    type_group_id = 2
WHERE type_id = 9;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_J',
    code          = 'J',
    description   = null,
    type_group_id = 1
WHERE type_id = 10;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_B',
    code          = 'B',
    description   = null,
    type_group_id = 1
WHERE type_id = 11;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_C',
    code          = 'C',
    description   = null,
    type_group_id = 1
WHERE type_id = 12;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_D',
    code          = 'D',
    description   = null,
    type_group_id = 1
WHERE type_id = 13;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_P',
    code          = 'P',
    description   = null,
    type_group_id = 2
WHERE type_id = 14;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_Z',
    code          = 'Z',
    description   = null,
    type_group_id = 3
WHERE type_id = 15;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_F',
    code          = 'F',
    description   = null,
    type_group_id = 2
WHERE type_id = 16;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_G',
    code          = 'G',
    description   = null,
    type_group_id = 4
WHERE type_id = 17;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_H',
    code          = 'H',
    description   = null,
    type_group_id = 4
WHERE type_id = 18;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_N',
    code          = 'N',
    description   = null,
    type_group_id = 4
WHERE type_id = 19;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_R',
    code          = 'R',
    description   = null,
    type_group_id = 3
WHERE type_id = 20;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_V',
    code          = 'V',
    description   = null,
    type_group_id = 3
WHERE type_id = 21;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_A',
    code          = 'A',
    description   = null,
    type_group_id = 4
WHERE type_id = 22;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_E',
    code          = 'E',
    description   = null,
    type_group_id = 4
WHERE type_id = 23;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_M',
    code          = 'M',
    description   = null,
    type_group_id = 4
WHERE type_id = 24;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_W',
    code          = 'W',
    description   = null,
    type_group_id = 4
WHERE type_id = 25;
UPDATE public . item_type
SET title         = 'ITEM_TYPE_O',
    code          = 'O',
    description   = null,
    type_group_id = 4
WHERE type_id = 26;


alter table item_type
  add column weight real default 1.0;
UPDATE public . item_type
SET weight = 0.2
WHERE type_id = 22;
UPDATE public . item_type
SET weight = 1.2
WHERE type_id = 15;
UPDATE public . item_type
SET weight = 1.8
WHERE type_id = 9;
UPDATE public . item_type
SET weight = 1.6
WHERE type_id = 21;
UPDATE public . item_type
SET weight = 0.2
WHERE type_id = 8;
UPDATE public . item_type
SET weight = 1.8
WHERE type_id = 14;
UPDATE public . item_type
SET weight = 1.2
WHERE type_id = 17;
UPDATE public . item_type
SET weight = 0.2
WHERE type_id = 26;
UPDATE public . item_type
SET weight = 0.6
WHERE type_id = 12;
UPDATE public . item_type
SET weight = 0.6
WHERE type_id = 13;
UPDATE public . item_type
SET weight = 1.2
WHERE type_id = 20;
UPDATE public . item_type
SET weight = 0.2
WHERE type_id = 18;
UPDATE public . item_type
SET weight = 0.2
WHERE type_id = 25;
UPDATE public . item_type
SET weight = 1.4
WHERE type_id = 16;
UPDATE public . item_type
SET weight = 2
WHERE type_id = 6;
UPDATE public . item_type
SET weight = 2
WHERE type_id = 7;
UPDATE public . item_type
SET weight = 0.2
WHERE type_id = 24;
UPDATE public . item_type
SET weight = 0.2
WHERE type_id = 23;
UPDATE public . item_type
SET weight = 1.2
WHERE type_id = 10;
UPDATE public . item_type
SET weight = 0.2
WHERE type_id = 19;
UPDATE public . item_type
SET weight = 0.8
WHERE type_id = 11;