alter table search_hist
    add column if not exists search_type text;

update search_hist
set search_type='EXPERTISE'
where query notnull;

update search_hist
set query=full_name_query,
    search_type='NAME'
where full_name_query notnull;

alter table search_hist
    drop column if exists full_name_query;

