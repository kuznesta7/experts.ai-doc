alter table search_hist
    add column query_length integer default 0;

update search_hist s
set query_length = length(s.query)
where query notnull;

CREATE OR REPLACE FUNCTION set_query_length()
    RETURNS TRIGGER AS
$body$
BEGIN
    IF (NEW.query notnull) THEN
        NEW.query_length = length(new.query);
    END IF;
    RETURN NEW;
END
$body$
    LANGUAGE plpgsql;

drop trigger if exists search_hist_insert on search_hist;
CREATE TRIGGER search_hist_insert
    BEFORE INSERT
    ON search_hist
    FOR ROW
EXECUTE PROCEDURE set_query_length();