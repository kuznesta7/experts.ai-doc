alter table project
    add column project_code text,
    add column country_code varchar(2) references country;

alter table project
    alter column name drop not null;
alter table project
    alter column description drop not null;

alter table private_project
    rename to project_organization;
alter table project_organization
    rename column parent_private_project to parent_project_organization;
alter table project_organization
    rename column private_project_id to project_organization_id;
alter table project_organization
    add column verified                  boolean default false,
    add column project_organization_role text    default 'PARTICIPANT';

alter table project_item
    alter column item_id drop not null;
alter table project_item
    add column original_item_id bigint;

drop table project_budget;
create table project_budget
(
    project_budget_id       bigserial primary key,
    project_id              bigint references project      not null,
    organization_id         bigint references organization not null,
    year                    integer                        not null,
    total_amount            double precision default 0,
    national_support_amount double precision default 0,
    private_amount          double precision default 0,
    other_amount            double precision default 0,
    currency                text,
    deleted                 boolean          default false
) inherits (trackable);

alter table project_expert
    rename column private_project_id to project_organization_id;
alter table project_user
    rename column private_project_id to project_organization_id;
