create table if not exists organization_type
(
    id          bigserial primary key,
    name        text,
    description text,
    delete      bool default false
);


alter table organization
add column if not exists organization_type_id bigint references organization_type;

insert into organization_type(name, description)
values ('University', null),
       ('Faculty', null),
       ('Department', null),
       ('Laboratory', null),
       ('Research group', null),
       ('Company', null),
       ('Members group', null)

