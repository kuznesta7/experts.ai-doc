create table search_hist_country
(
    id             BIGSERIAL PRIMARY KEY,
    search_hist_id BIGINT REFERENCES search_hist,
    country_code   varchar(2) references country
);