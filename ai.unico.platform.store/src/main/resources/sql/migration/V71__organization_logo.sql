alter table organization
    add column organization_logo_file_id bigint references file;
alter table organization
    add column has_public_profile boolean default false not null;