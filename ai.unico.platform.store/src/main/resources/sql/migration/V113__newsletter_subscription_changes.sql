create table if not exists subscription_type
(
    id bigserial primary key,
    subscription_group text not null,
    description text,
    internal_description text,
    deleted boolean default false
);

alter table if exists widget_email_subscription
    add column if not exists subscription_type_id bigint references subscription_type,
    add column if not exists last_newsletter_sent timestamp;


insert into subscription_type (subscription_group, description, internal_description)
values ('A', 'No recommendation - just new opportunities', 'No recommendation - just new opportunities'),
       ('B', 'Interaction-based recommendation', 'Interaction-based recommendation'),
       ('C', 'Combined recommendation with student profile and interaction-based recommendations', 'Combined recommendation with student profile and interaction-based recommendations');

update widget_email_subscription
set subscription_type_id = 3
where widget_email_subscription.subscription_type_id is null;
