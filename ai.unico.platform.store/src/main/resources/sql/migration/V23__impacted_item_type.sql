alter table item_type
  add column impacted boolean default false;
alter table item_type
  add column utility_model boolean default false;
update item_type
set impacted = true
where type_id >= 27;
update item_type
set utility_model = true
where type_id = 16;
