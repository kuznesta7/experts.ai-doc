drop table if exists organization_structure;

create table organization_structure
(
    organization_structure_id bigserial primary key,
    upper_organization_id     bigint references organization not null,
    lower_organization_id     bigint references organization not null,
    relation_type             text                           not null default 'PARENT',
    deleted                   boolean                        not null default false
) inherits (trackable);

insert into organization_structure (upper_organization_id, lower_organization_id)
    (select parent_organization_id, organization_id from organization where parent_organization_id notnull);

DROP VIEW IF EXISTS organization_structure_view;
CREATE OR REPLACE RECURSIVE VIEW organization_structure_view (extended_parent,
                                                              extended_child_parent,
                                                              extended_parent_child,
                                                              extended_child,
                                                              relation_type,
                                                              path,
                                                              path_string) AS (
    SELECT e.upper_organization_id,
           e.upper_organization_id,
           e.lower_organization_id,
           o.organization_id,
           e.relation_type,
           ARRAY [e.upper_organization_id, o.organization_id],
           array_to_string(ARRAY [e.upper_organization_id, o.organization_id], ',')
    FROM organization o
             left join organization_structure e on o.organization_id = e.lower_organization_id
    WHERE e isnull
       or e.deleted = false
    UNION ALL
    SELECT p.extended_parent,
           e.upper_organization_id,
           p.extended_parent_child,
           e.lower_organization_id,
           case when p.relation_type != 'PARENT' then p.relation_type else e.relation_type end,
           p.path || ARRAY [e.lower_organization_id],
           array_to_string(p.path || ARRAY [e.lower_organization_id], ',')
    FROM organization_structure_view p
             JOIN organization_structure e
                  ON p.extended_child = e.upper_organization_id AND e.lower_organization_id != ALL (p.path) AND
                     e.deleted = false
);

ALTER TABLE organization
    DROP COLUMN IF EXISTS parent_organization_id,
    DROP COLUMN IF EXISTS root_organization_id;
DROP TRIGGER IF EXISTS organization_update ON organization;
DROP TRIGGER IF EXISTS organization_insert ON organization;
DROP FUNCTION IF EXISTS set_root_organization();