create table if not exists widget_email_subscription
(
    sub_id              bigserial
        constraint widget_email_subscription_pk
            primary key,
    email               varchar(255) not null,
    subscription_date   timestamp,
    unsubscription_date timestamp,
    subscribed          boolean default true
);

alter table widget_email_subscription
    owner to postgres;

create unique index if not exists widget_email_subscription_email_uindex
    on widget_email_subscription (email);

create unique index if not exists widget_email_subscription_sub_id_uindex
    on widget_email_subscription (sub_id);

