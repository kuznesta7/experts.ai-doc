CREATE TABLE trackable (
  user_update BIGINT,
  date_update TIMESTAMP,
  user_insert BIGINT,
  date_insert TIMESTAMP
);

CREATE TABLE file (
  file_id     BIGSERIAL   PRIMARY KEY NOT NULL,
  name        TEXT        NOT NULL,
  content     BYTEA       NOT NULL
) INHERITS (trackable);

CREATE TABLE country (
  country_id            BIGSERIAL PRIMARY KEY NOT NULL,
  name                  TEXT UNIQUE
);

CREATE TABLE location (
  location_id           BIGSERIAL PRIMARY KEY NOT NULL,
  city                  TEXT,
  country_id            BIGINT REFERENCES country
);

CREATE TABLE user_profile (
  user_id              BIGSERIAL PRIMARY KEY NOT NULL,
  email                TEXT,
  name                 TEXT,
  description          TEXT,
  institution          TEXT,
  position_description TEXT,
  deleted              BOOLEAN DEFAULT FALSE,
  img_file_id          BIGINT REFERENCES file,
  city                 TEXT,
  country              TEXT
)
  INHERITS (trackable);

CREATE TABLE search_column_settings (
  id          BIGSERIAL   PRIMARY KEY NOT NULL,
  name        TEXT,
  type        TEXT,
  weight      DOUBLE PRECISION
);

CREATE TABLE tag (
  tag_id                BIGSERIAL   PRIMARY KEY NOT NULL,
  value                 TEXT        UNIQUE,
  autocomplete          BOOLEAN     DEFAULT FALSE
);

CREATE TABLE user_profile_tag (
  user_profile_tag_id BIGSERIAL   PRIMARY KEY NOT NULL,
  user_id             BIGINT REFERENCES user_profile,
  tag_id              BIGINT      REFERENCES tag,
  outdated            BOOLEAN     DEFAULT FALSE
);

CREATE TABLE search_hist (
  search_hist_id    BIGSERIAL PRIMARY KEY NOT NULL,
  search_expression TEXT
) INHERITS (trackable);

CREATE TABLE profile_visitation (
  visitation_id  BIGSERIAL PRIMARY KEY NOT NULL,
  user_id        BIGINT REFERENCES user_profile,
  search_hist_id BIGINT REFERENCES search_hist,
  expert_id      BIGINT
) INHERITS (trackable);

CREATE TABLE user_message (
  user_message_id BIGSERIAL PRIMARY KEY NOT NULL,
  user_to_id      BIGINT REFERENCES user_profile,
  user_from_id    BIGINT REFERENCES user_profile,
  search_hist_id  BIGINT REFERENCES search_hist,
  parent_message  BIGINT REFERENCES user_message,
  subject         TEXT,
  content         TEXT,
  read            BOOLEAN,
  deleted         BOOLEAN
)
  INHERITS (trackable);

CREATE TABLE contact_request_state (
  state_id   BIGSERIAL PRIMARY KEY NOT NULL,
  state_name TEXT
);

CREATE TABLE contact_request (
  contact_request_id BIGSERIAL PRIMARY KEY NOT NULL,
  user_to_id         BIGINT REFERENCES user_profile,
  user_from_id       BIGINT REFERENCES user_profile,
  search_hist_id     BIGINT REFERENCES search_hist,
  subject            TEXT,
  content            TEXT,
  state_id           BIGINT REFERENCES contact_request_state
)
  INHERITS (trackable);

CREATE TABLE user_expert (
  user_expert_id BIGSERIAL PRIMARY KEY NOT NULL,
  user_id        BIGINT REFERENCES user_profile,
  expert_id      BIGINT                NOT NULL,
  deleted        BOOLEAN
)
  INHERITS (trackable);

CREATE TABLE item (
  item_id          BIGSERIAL PRIMARY KEY NOT NULL,
  orig_item_id     BIGINT UNIQUE,
  item_name        TEXT,
  item_description TEXT,
  item_type        INT,
  date_published   TIMESTAMP,
  outdated         BOOLEAN DEFAULT FALSE
)
  INHERITS (trackable);

CREATE TABLE item_user (
  item_user_id BIGSERIAL PRIMARY KEY NOT NULL,
  user_id      BIGINT REFERENCES user_profile,
  item_id      BIGINT REFERENCES item,
  expert_id    BIGINT,
  deleted      BOOLEAN DEFAULT FALSE
)
  INHERITS (trackable);