CREATE TABLE consent (
  consent_id BIGINT PRIMARY KEY,
  title      TEXT
);

CREATE TABLE user_consent (
  user_consent_id BIGSERIAL PRIMARY KEY          NOT NULL,
  user_id         BIGINT REFERENCES user_profile NOT NULL,
  consent_id      BIGINT REFERENCES consent      NOT NULL,
  deleted         BOOLEAN DEFAULT FALSE          NOT NULL
)
  INHERITS (trackable);

INSERT INTO consent (consent_id, title) VALUES (1, 'CONTACT_BY_PLATFORM_USER');
INSERT INTO consent (consent_id, title) VALUES (2, 'CONTACT_BY_UNICO_AI');
INSERT INTO consent (consent_id, title)
VALUES (3, 'PERSONALISATION_BASED_ON_LOG_DATA');

DROP TABLE location;
DROP TABLE country;