drop table if exists project_program_copy;
create table project_program_copy (like project_program);
drop table if exists t_project_program;
create table t_project_program (
                                   id bigserial primary key,
                                   name text
);
INSERT INTO public.t_project_program (id, name) VALUES (0, 'program is not defined');
INSERT INTO public.t_project_program (id, name) VALUES (2, 'Výzkumná centra (Národní program výzkumu)');
INSERT INTO public.t_project_program (id, name) VALUES (3, 'INGO II');
INSERT INTO public.t_project_program (id, name) VALUES (4, 'NÁVRAT');
INSERT INTO public.t_project_program (id, name) VALUES (5, 'Podpora projektů sedmého rámcového programu Evropského společenství pro výzkum, technologický rozvoj a demonstrace (2007 až 2013) podle zákona č. 171/2007 Sb.');
INSERT INTO public.t_project_program (id, name) VALUES (6, 'Finanční mechanismy EHP/Norsko');
INSERT INTO public.t_project_program (id, name) VALUES (7, 'GESHER/MOST');
INSERT INTO public.t_project_program (id, name) VALUES (8, 'KONTAKT');
INSERT INTO public.t_project_program (id, name) VALUES (9, 'Komplexní udržitelné systémy v zemědělství 2012-2018 ?KUS?');
INSERT INTO public.t_project_program (id, name) VALUES (10, 'Bezpečnostní výzkum České republiky 2015-2022');
INSERT INTO public.t_project_program (id, name) VALUES (11, 'Doktorské granty');
INSERT INTO public.t_project_program (id, name) VALUES (12, 'INGO');
INSERT INTO public.t_project_program (id, name) VALUES (13, 'Program na podporu aplikovaného výzkumu a experimentálního vývoje ALFA');
INSERT INTO public.t_project_program (id, name) VALUES (14, 'Eurocores');
INSERT INTO public.t_project_program (id, name) VALUES (15, 'Juniorské badatelské grantové projekty');
INSERT INTO public.t_project_program (id, name) VALUES (16, 'Prosperita (Operační program Podnikání a inovace)');
INSERT INTO public.t_project_program (id, name) VALUES (17, 'Program aplikovaného výzkumu a vývoje národní a kulturní identity (NAKI)');
INSERT INTO public.t_project_program (id, name) VALUES (18, 'Sedmý rámcový program Evropského společenství pro atomovou energii (Euratom) v oblasti jaderného výzkumu a vzdělávání');
INSERT INTO public.t_project_program (id, name) VALUES (19, 'Program rozvoje konkurenceschopnosti Karlovarského kraje - Inovační vouchery');
INSERT INTO public.t_project_program (id, name) VALUES (20, 'Program aplikovaného výzkumu Ministerstva zemědělství na období 2017 - 2025, ZEMĚ');
INSERT INTO public.t_project_program (id, name) VALUES (21, 'Vztah rozvoje měst a přípravy územně plánovací dokumentace');
INSERT INTO public.t_project_program (id, name) VALUES (22, 'EUREKA CZ');
INSERT INTO public.t_project_program (id, name) VALUES (23, 'Program na podporu zdravotnického aplikovaného výzkumu na léta 2015 - 2022');
INSERT INTO public.t_project_program (id, name) VALUES (24, 'Výzkumná centra');
INSERT INTO public.t_project_program (id, name) VALUES (25, 'Regionální inovační program Dotačního fondu Libereckého kraje');
INSERT INTO public.t_project_program (id, name) VALUES (26, 'IMPULS');
INSERT INTO public.t_project_program (id, name) VALUES (27, 'COST');
INSERT INTO public.t_project_program (id, name) VALUES (28, 'Výzkumný program Výzkumného fondu pro uhlí a ocel');
INSERT INTO public.t_project_program (id, name) VALUES (29, 'KONTAKT II');
INSERT INTO public.t_project_program (id, name) VALUES (30, 'TRIO');
INSERT INTO public.t_project_program (id, name) VALUES (31, 'Program na podporu aplikovaného výzkumu ZÉTA');
INSERT INTO public.t_project_program (id, name) VALUES (32, 'Program veřejných zakázek ve výzkumu, experimentálním vývoji a inovacích pro potřeby státní správy BETA');
INSERT INTO public.t_project_program (id, name) VALUES (33, 'Národní program udržitelnosti I');
INSERT INTO public.t_project_program (id, name) VALUES (34, 'Operační program Vzdělávání pro konkurenceschopnost');
INSERT INTO public.t_project_program (id, name) VALUES (35, 'Program aplikovaného výzkumu, experimentálního vývoje a inovací GAMA');
INSERT INTO public.t_project_program (id, name) VALUES (36, 'TIP');
INSERT INTO public.t_project_program (id, name) VALUES (37, 'Česko-izraelská spolupráce ve VaV');
INSERT INTO public.t_project_program (id, name) VALUES (38, 'Granty výrazně badatelského charakteru zaměřené na oblast výzkumu rozvíjeného v současné době zejména v AV ČR');
INSERT INTO public.t_project_program (id, name) VALUES (39, 'Rozvoj dosažených operačních schopností ozbrojených sil České republiky');
INSERT INTO public.t_project_program (id, name) VALUES (40, 'Program výzkumu v agrárním sektoru 2007-2012');
INSERT INTO public.t_project_program (id, name) VALUES (41, 'Evropská zájmová skupina pro spolupráci s Japonskem');
INSERT INTO public.t_project_program (id, name) VALUES (42, 'Program veřejných zakázek v aplikovaném výzkumu a inovacích pro potřeby státní správy BETA2');
INSERT INTO public.t_project_program (id, name) VALUES (43, 'Centra kompetence');
INSERT INTO public.t_project_program (id, name) VALUES (44, 'Doplňkový program Evropského společenství pro atomovou energii (EURATOM pro výzkum a odborné přípravu (2014-2018), který doplňuje Horizont 2020 - rámcový program pro výzkum a inovace)');
INSERT INTO public.t_project_program (id, name) VALUES (45, 'EUPRO II');
INSERT INTO public.t_project_program (id, name) VALUES (46, 'Česko-bavorská spolupráce ve VaV');
INSERT INTO public.t_project_program (id, name) VALUES (47, 'Program na podporu aplikovaného výzkumu a experimentálního vývoje EPSILON');
INSERT INTO public.t_project_program (id, name) VALUES (48, 'Grantové projekty excelence v základním výzkumu EXPRO');
INSERT INTO public.t_project_program (id, name) VALUES (49, 'Evropský metrologický program pro inovace a výzkum');
INSERT INTO public.t_project_program (id, name) VALUES (50, 'Podpora mobility výzkumných pracovníků a pracovnic v rámci mezinárodní spolupráce ve VaVaI');
INSERT INTO public.t_project_program (id, name) VALUES (51, 'Program na podporu aplikovaného výzkumu, experimentálního vývoje a inovací Národní centra kompetence ');
INSERT INTO public.t_project_program (id, name) VALUES (52, 'Obranný aplikovaný výzkum, experimentální vývoj a inovace');
INSERT INTO public.t_project_program (id, name) VALUES (53, 'INTER-EXCELLENCE');
INSERT INTO public.t_project_program (id, name) VALUES (54, 'Operační program Podnikání a inovace pro konkurenceschopnost');
INSERT INTO public.t_project_program (id, name) VALUES (55, 'Společné technologické iniciativy');
INSERT INTO public.t_project_program (id, name) VALUES (56, 'EUREKA - evropská spolupráce v oblasti aplikovaného a průmyslového výzkumu a vývoje cílená na podporu nadnárodní kooperace mezi průmyslovými podniky, výzkumnými ústavy a vysokými školami.');
INSERT INTO public.t_project_program (id, name) VALUES (57, 'Projekty na podporu excelence v základním výzkumu');
INSERT INTO public.t_project_program (id, name) VALUES (58, 'Rozvoj center špičkových průmyslových výrobků a technologií');
INSERT INTO public.t_project_program (id, name) VALUES (59, 'EUPRO');
INSERT INTO public.t_project_program (id, name) VALUES (60, 'Program bezpečnostního výzkumu pro potřeby státu 2016 - 2021 (BV III/2 - VZ)');
INSERT INTO public.t_project_program (id, name) VALUES (61, 'Program na podporu aplikovaného společenskovědního a humanitního výzkumu, experimentálního vývoje a inovací ÉTA');
INSERT INTO public.t_project_program (id, name) VALUES (62, 'Program česko-čínské spolupráce ve výzkumu, vývoji a inovacích na podporu mobility výzkumných pracovníků');
INSERT INTO public.t_project_program (id, name) VALUES (63, 'Program na podporu aplikovaného výzkumu a experimentálního vývoje národní a kulturní identity na léta 2016 až 2022 (NAKI II)');
INSERT INTO public.t_project_program (id, name) VALUES (64, 'Makroregionální spolupráce ve výzkumu, vývoji a inovacích');
INSERT INTO public.t_project_program (id, name) VALUES (65, 'ERC CZ');
INSERT INTO public.t_project_program (id, name) VALUES (66, 'Operační program Praha - pól růstu ČR');
INSERT INTO public.t_project_program (id, name) VALUES (67, 'Program na podporu aplikovaného výzkumu, experimentálního vývoje a inovací THÉTA ');
INSERT INTO public.t_project_program (id, name) VALUES (68, 'Program bezpečnostního výzkumu České republiky 2010 - 2015');
INSERT INTO public.t_project_program (id, name) VALUES (69, 'Potenciál (Operační program Podnikání a inovace)');
INSERT INTO public.t_project_program (id, name) VALUES (70, 'Bezpečná a ekonomická doprava (Národní program výzkumu)');
INSERT INTO public.t_project_program (id, name) VALUES (71, 'Spolupráce (Operační program Podnikání a inovace)');
INSERT INTO public.t_project_program (id, name) VALUES (72, 'Mezinárodní grantové projekty hodnocené na principu LEAD Agency');
INSERT INTO public.t_project_program (id, name) VALUES (73, 'Operační program Výzkum a vývoj pro inovace');
INSERT INTO public.t_project_program (id, name) VALUES (74, 'COST CZ');
INSERT INTO public.t_project_program (id, name) VALUES (75, 'Program na podporu aplikovaného společenskovědního výzkumu a experimentálního vývoje OMEGA');
INSERT INTO public.t_project_program (id, name) VALUES (76, 'Program pro financování projektů mnohostranné vědeckotechnické spolupráce v Podunajském regionu');
INSERT INTO public.t_project_program (id, name) VALUES (77, 'Horizont 2020 - rámcový program pro výzkum a inovace');
INSERT INTO public.t_project_program (id, name) VALUES (78, 'Společná technologická iniciativa ECSEL');
INSERT INTO public.t_project_program (id, name) VALUES (79, 'Informace - základ výzkumu');
INSERT INTO public.t_project_program (id, name) VALUES (80, 'Zdravý a kvalitní život');
INSERT INTO public.t_project_program (id, name) VALUES (81, 'Bezpečnostní výzkum pro potřeby státu v letech 2010 až 2015');
INSERT INTO public.t_project_program (id, name) VALUES (82, 'Podpora dosažených operačních schopností ozbrojených sil České republiky');
INSERT INTO public.t_project_program (id, name) VALUES (83, 'Resortní program výzkumu a vývoje Ministerstva zdravotnictví III');
INSERT INTO public.t_project_program (id, name) VALUES (84, 'Šestý rámcový program Evropského společenství pro výzkum, technický rozvoj a demonstrační činnosti');
INSERT INTO public.t_project_program (id, name) VALUES (85, 'Juniorské granty');
INSERT INTO public.t_project_program (id, name) VALUES (86, 'Standardní projekty');
INSERT INTO public.t_project_program (id, name) VALUES (87, 'Výzkum v agrárním sektoru (VAK)');
INSERT INTO public.t_project_program (id, name) VALUES (88, 'Postdoktorandské granty');
INSERT INTO public.t_project_program (id, name) VALUES (89, 'Eurostars');
INSERT INTO public.t_project_program (id, name) VALUES (90, 'Operační program výzkum, vývoj, vzdělávání');
INSERT INTO public.t_project_program (id, name) VALUES (91, 'Nanotechnologie pro společnost');
INSERT INTO public.t_project_program (id, name) VALUES (92, 'Rozvoj ozbrojených sil České republiky');
INSERT INTO public.t_project_program (id, name) VALUES (93, 'Národní program udržitelnosti II');
INSERT INTO public.t_project_program (id, name) VALUES (94, 'Projekty velkých infrastruktur pro VaVaI');
INSERT INTO public.t_project_program (id, name) VALUES (95, 'Program podpory aplikovaného výzkumu a experimentálního vývoje DELTA');
INSERT INTO public.t_project_program (id, name) VALUES (96, 'Mezinárodní projekty');
INSERT INTO public.t_project_program (id, name) VALUES (97, '01.01 2010  Stimuly 2010 MŠVVaŠ SR Stimuly na podporu výskumu a vývoja v roku 2010');
INSERT INTO public.t_project_program (id, name) VALUES (98, '01.01 2015  NCP_WIDE.NET NCP_WIDE.NET – Transnational network of cooperation for WIDESPREAD NCPs');
INSERT INTO public.t_project_program (id, name) VALUES (99, '01.03 2012  SK-RO 2012 APVV Slovensko – Rumunsko Verejná výzva na podávanie žiadostí na riešenie spoločných projektov výskumu a vývoja podporujúcich spoluprácu medzi organizáciami v Slovenskej republike a v Rumunskej republike');
INSERT INTO public.t_project_program (id, name) VALUES (100, '01.03 2013  VEGA-2014-MŠVVŠ-SR Výzvy  na  podávanie žiadostí  o  dotácie  na  nové  projekty  VEGA so
začiatkom riešenia v roku 2014');
INSERT INTO public.t_project_program (id, name) VALUES (101, '01.04 2011  SK-GR 2011 APVV Slovensko – Grécko Verejná výzva na podávanie návrhov na spoločné projekty podporujúce spoluprácu medzi pracoviskami Slovenskej republiky a Gréckej republiky na roky 2012 – 2013');
INSERT INTO public.t_project_program (id, name) VALUES (102, '01.07 2013  Not Neuvedená výzva');
INSERT INTO public.t_project_program (id, name) VALUES (103, '01.08 2008  OPZ 2008 Verejná výzva na predkladanie žiadostí na poskytnutie dotácie Ministerstva zdravotníctva SR na výskum a vývoj v oblasti zdravotníctva pre rok 2008');
INSERT INTO public.t_project_program (id, name) VALUES (104, '01.08 2014  Schéma DM 18/2014 Dotácie pre priemyselné klastre 2014');
INSERT INTO public.t_project_program (id, name) VALUES (105, '01.09 2016  ENAI ENAI – European Network for Academic Integrity  1');
INSERT INTO public.t_project_program (id, name) VALUES (106, '01.09 2017  HubIT HubIT – The HUB for boosting the Responsibility and inclusiveness of ICT enabled Research and Innovation through constructive interactions with SSH research');
INSERT INTO public.t_project_program (id, name) VALUES (107, '01.12 2008  ENV 2009 APVV Verejná výzva na predkladanie žiadostí na tému Humanities Spring 2009');
INSERT INTO public.t_project_program (id, name) VALUES (108, '01.12 2008  OPVaV/K/RKZ/NP/2008-2 Projekt „Infraštruktúra pre výskum a vývoj - Dátové centrum pre výskum a vývoj');
INSERT INTO public.t_project_program (id, name) VALUES (109, '01.12 2010  1.12.2010 OPBK/2010/2.1/06 Inovácie a technologické transfery');
INSERT INTO public.t_project_program (id, name) VALUES (110, '01.12 2010  1.12.2010 OPBK/2010/2.2/06 Informatizácia spoločnosti');
INSERT INTO public.t_project_program (id, name) VALUES (111, '01.12 2010  OPBK-2010-2.2-06 Informatizácia spoločnosti');
INSERT INTO public.t_project_program (id, name) VALUES (112, '02.02 2009  LPP 2009 APVV Verejná výzva na predkladanie žiadostí v rámci programu "Podpora ľudského potenciálu v oblasti výskumu a vývoja a popularizácia vedy"');
INSERT INTO public.t_project_program (id, name) VALUES (113, '02.02 2009  SUSPP 2009 APVV Verejná výzva na predkladanie žiadostí v rámci programu "Podpora spolupráce univerzít a SAV s podnikateľským prostredím"');
INSERT INTO public.t_project_program (id, name) VALUES (114, '02.03 2015  VEGA-2015-MŠVVŠ-SR Výzvy na podávanie žiadostí o dotácie na nové projekty VEGA so začiatkom riešenia v roku 2016');
INSERT INTO public.t_project_program (id, name) VALUES (115, '02.04 2012  SK-AT 2012 APVV Slovensko – Rakúsko Verejná výzva na podávanie žiadostí na riešenie spoločných projektov výskumu a vývoja podporujúcich spoluprácu medzi organizáciami v Slovenskej republike a v Rakúskej republike');
INSERT INTO public.t_project_program (id, name) VALUES (116, '02.04 2013  SK-FR 2013 APVV Slovensko – Francúzsko Verejná výzva na podávanie žiadostí na riešenie projektov podporujúcich spoluprácu medzi organizáciami v Slovenskej republike a vo Francúzskej republike');
INSERT INTO public.t_project_program (id, name) VALUES (117, '02.05 2012  SK-PL 2012 APVV Slovensko – Poľsko Verejná výzva na podávanie žiadostí na riešenie spoločných projektov výskumu a vývoja podporujúcich spoluprácu medzi organizáciami v Slovenskej republike a v Poľskej republike');
INSERT INTO public.t_project_program (id, name) VALUES (118, '02.05 2017  SK - TW 2017 APVV Verejná výzva na podávanie žiadostí na riešenie spoločných projektov výskumu a vývoja podporujúcich spoluprácu medzi organizáciami v Slovenskej republike a v Čínskej republike (Taiwan)');
INSERT INTO public.t_project_program (id, name) VALUES (119, '02.05 2018  SK-PL 2018 APVV Verejná výzva Slovensko – Poľsko 2018');
INSERT INTO public.t_project_program (id, name) VALUES (120, '02.06 2008  SK-HU 2008 APVV Slovensko – Maďarsko Verejná výzva k podávaniu návrhov na spoločné projekty vedecko-technickej spolupráce medzi pracoviskami Slovenskej republiky a Maďarskej republiky na roky 2009-2010');
INSERT INTO public.t_project_program (id, name) VALUES (121, '02.06 2010  OPVaV-2010/2.2/06-SORO 2.6.2010 OPVaV-2010/2.2/06-SORO Podpora výskumu a vývoja, Prenos poznatkov a technológií získaných výskumom a vývojom do praxe');
INSERT INTO public.t_project_program (id, name) VALUES (122, '02.06 2010  OPVaV-2010/2.2/06-SORO Podpora budovania kompetenčných centier pre operačný program Výskum a vývoj a Schéma na podporu výskumu a vývoja');
INSERT INTO public.t_project_program (id, name) VALUES (123, '02.06 2010  OPVaV-2010/4.2/06-SORO Podpora budovania kompetenčných centier pre operačný program Výskum a vývoj a Schéma na podporu výskumu a vývoja');
INSERT INTO public.t_project_program (id, name) VALUES (124, '02.09 2008  OPŽP-PO5-08-3 Zlepšenie infraštruktúry ochrany prírody a krajiny prostredníctvom budovania a rozvoja zariadení ochrany prírody');
INSERT INTO public.t_project_program (id, name) VALUES (125, '02.09 2009  SK-CN 2009 APVV Slovensko – Čína Verejná výzva k podávaniu návrhov na spoločné projekty VaV podporujúce spoluprácu medzi pracoviskami SR a Čínskej ľud. rep. na roky 2010 - 2011');
INSERT INTO public.t_project_program (id, name) VALUES (126, '02.09 2014  VV 2014 APVV Verejná výzva na predkladanie žiadostí na riešenie projektov výskumu a vývoja v jednotlivých skupinách odborov vedy a techniky – VV 2014');
INSERT INTO public.t_project_program (id, name) VALUES (127, '03.03 2014  VEGA 2014 Výzvy na podávanie žiadostí o dotácie na nové projekty VEGA so začiatkom riešenia v roku 2015');
INSERT INTO public.t_project_program (id, name) VALUES (128, '03.04 2017  SK – FR 2017 APVV Verejná výzva na podávanie žiadostí na riešenie spoločných projektov výskumu a vývoja podporujúcich spoluprácu medzi organizáciami v Slovenskej republike a vo Francúzskej republike');
INSERT INTO public.t_project_program (id, name) VALUES (129, '03.05 2013  SK-UA 2013 APVV Slovensko – Ukrajina Verejná výzva na podávanie žiadostí na riešenie projektov podporujúcich spoluprácu medzi organizáciami v Slovenskej republike a na Ukrajine');
INSERT INTO public.t_project_program (id, name) VALUES (130, '03.05 2013  SK-UA APVV Slovensko Ukrajina Verejná výzva na podávanie žiadostí na riešenie projektov podporujúcich spoluprácu medzi organizáciami v Slovenskej republike a na Ukrajine');
INSERT INTO public.t_project_program (id, name) VALUES (131, '03.08 2009  SK-UA 2009 APVV Slovensko – Ukrajina Verejná výzva k podávaniu návrhov na spoločné projekty vedecko-technickej spolupráce medzi pracoviskami Slovenskej republiky a Ukrajiny na roky 2010-2011');
INSERT INTO public.t_project_program (id, name) VALUES (132, '03.10 2017  VV 2017 APVV Verejná výzva na predkladanie žiadostí na riešenie projektov výskumu a vývoja v jednotlivých skupinách odborov vedy a techniky – VV 2017');
INSERT INTO public.t_project_program (id, name) VALUES (133, '04.03 2013  EUREKA SK Výzva MŠVVaŠ SR na predkladanie návrhov projektov na získanie účelovej podpory na spolufinancovanie projektov programu EUREKA SK');
INSERT INTO public.t_project_program (id, name) VALUES (134, '04.08 2008  OPBK/2008/2.1/02 04.08.2008 OPBK/2008/2.1/02 Vedomostná ekonomika, Inovácie a technologické transféry');
INSERT INTO public.t_project_program (id, name) VALUES (135, '04.08 2008  OPBK/2008/2.2/02 04.08.2008 OPBK/2008/2.2/02 Vedomostná ekonomika, Informatizácia spoločnosti');
INSERT INTO public.t_project_program (id, name) VALUES (136, '04.10 2016  BB-05-2017 Bio-based products: Mobilisation and mutual learning action plan');
INSERT INTO public.t_project_program (id, name) VALUES (137, '04.10 2016  BB-07-2017 Plant Molecular Factory');
INSERT INTO public.t_project_program (id, name) VALUES (138, '05.04 2017  kód OPVaI-MH/NP/2017/3.3.1/4.1.1-06 Vyzvanie na predloženie národného projektu s názvom „Podpora rozvoja kreatívneho priemyslu na Slovensku“ žiadateľa Slovenská inovačná a energetická agentúra (SIEA)');
INSERT INTO public.t_project_program (id, name) VALUES (139, '05.05 2016  SK–RS 2016 APVV Verejná výzva Slovensko – Srbská republika na podávanie žiadostí na riešenie projektov podporujúcich spoluprácu medzi organizáciami v Slovenskej republike a v Srbskej republike');
INSERT INTO public.t_project_program (id, name) VALUES (140, '05.10 2012  Stimuly 2012 MŠVVaŠ SR Stimuly na podporu výskumu a vývoja v roku 2012');
INSERT INTO public.t_project_program (id, name) VALUES (141, '05.11 2012  VV 2012 APVV Verejná výzva na predkladanie žiadostí na riešenie projektov výskumu a vývoja v jednotlivých skupinách odborov vedy a techniky – VV 2012');
INSERT INTO public.t_project_program (id, name) VALUES (142, '07.04 2015  SK-PT 2015 APVV Verejná výzva na podávanie žiadostí na riešenie projektov podporujúcich spoluprácu medzi organizáciami v Slovenskej republike a v Portugalskej republike');
INSERT INTO public.t_project_program (id, name) VALUES (143, '07.06 2010  SK-HU 2010 APVV Slovensko – Maďarsko Verejná výzva na podávanie návrhov na spoločné projekty podporujúce spoluprácu medzi pracoviskami Slovenskej republiky a Maďarskej republiky na roky 2011 – 2012');
INSERT INTO public.t_project_program (id, name) VALUES (144, '07.07 2014  SCOPES 2013-2016 Výzva na poskytnutie grantov zo Švajčiarska');
INSERT INTO public.t_project_program (id, name) VALUES (145, '08.03 2012  SK-CN 2012 APVV Slovensko – Čína Verejná výzva na podávanie žiadostí na riešenie spoločných projektov výskumu a vývoja podporujúcich spoluprácu medzi organizáciami v Slovenskej republike a v Čínskej ľudovej republike');
INSERT INTO public.t_project_program (id, name) VALUES (146, '09.01 2017  JPND Výzva na podávanie návrhov medzinárodných projektov vo výskume neurodegeneračných ochorení');
INSERT INTO public.t_project_program (id, name) VALUES (147, '09.03 2009  SK-PL 2009 APVV Slovensko - Poľsko Výzva k podávaniu návrhov na spoločné projekty vedecko-technickej spolupráce medzi pracoviskami Slovenskej republiky a Poľskej republiky na roky 2010-2011');
INSERT INTO public.t_project_program (id, name) VALUES (148, '09.03 2015  Schéma DM – 17/2014 Výzva na predkladanie žiadostí o poskytnutie dotácie prostredníctvom vouchera v rámci Schémy na podporu spolupráce podnikateľských subjektov a vedecko – výskumných pracovísk formou inovačných voucherov');
INSERT INTO public.t_project_program (id, name) VALUES (149, '09.07 2013  OPVaV-2013/2.2/09-RO Priebežná výzva „Vybudovanie univerzitných vedeckých parkov a výskumných centier“ – opatrenie 2.2');
INSERT INTO public.t_project_program (id, name) VALUES (150, '09.07 2013  OPVaV-2013/4.2/09-RO Priebežná výzva „Vybudovanie univerzitných vedeckých parkov a výskumných centier“ – opatrenie 4.2');
INSERT INTO public.t_project_program (id, name) VALUES (151, '09.09 2015  VV 2015 APVV Verejná výzva na predkladanie žiadostí na riešenie projektov výskumu a vývoja v jednotlivých skupinách odborov vedy a techniky – VV 2015');
INSERT INTO public.t_project_program (id, name) VALUES (152, '09.12 2013  OPVaV/RKZ/NP/2013-2 Národný projekt: Podpora zriadenia a rozvoja Národného podnikateľského centra na Slovensku – I. etapa');
INSERT INTO public.t_project_program (id, name) VALUES (153, '10.01 2013  OPVaV/K/RKZ/NP/2013-1 Výzva pre národný projekt „PopVaT – Popularizácia vedy a techniky na Slovensku');
INSERT INTO public.t_project_program (id, name) VALUES (154, '10.05 2013  SK-BG 2013 APVV Slovensko – Bulharsko Verejná výzva na podávanie žiadostí na riešenie projektov podporujúcich spoluprácu medzi organizáciami v Slovenskej republike a v Bulharskej republike');
INSERT INTO public.t_project_program (id, name) VALUES (155, '10.07 2015  2015-15075/33841:1-15E0 Dotácie na vedecko-technické služby');
INSERT INTO public.t_project_program (id, name) VALUES (156, '11.04 2018  SK-KR 2018 APVV Verejná výzva Slovensko – Kórea 2018');
INSERT INTO public.t_project_program (id, name) VALUES (157, '11.12 2013  H2020-BES-2014 Bezpečnosť hraníc a vonkajšia bezpečnosť');
INSERT INTO public.t_project_program (id, name) VALUES (158, '11.12 2013  H2020-DRS-2014 Odolnosť voči katastrofám: ochrana a bezpečnosť spoločnosti, vrátane prispôsobenia sa zmene klímy');
INSERT INTO public.t_project_program (id, name) VALUES (159, '11.12 2013  H2020-EE-2014-2-RIA Výskum a inovácie energetickej účinnosti');
INSERT INTO public.t_project_program (id, name) VALUES (160, '11.12 2013  H2020-EeB-2015 Energeticky účinné budovy');
INSERT INTO public.t_project_program (id, name) VALUES (161, '11.12 2013  H2020-FCT-2014 Boj proti trestnej činnosti a terorizmu');
INSERT INTO public.t_project_program (id, name) VALUES (162, '11.12 2013  H2020-FETFLAG-2014 Technológie budúcnosti a vznikajúce technológie - FET flagships -  riešenie interdisciplinárnych vedeckých a technických úloh');
INSERT INTO public.t_project_program (id, name) VALUES (163, '11.12 2013  H2020-GV-2014 Zelené vozidlá 2014');
INSERT INTO public.t_project_program (id, name) VALUES (164, '11.12 2013  H2020-ICT-2014-2 Informačné a komunikačné technológie');
INSERT INTO public.t_project_program (id, name) VALUES (165, '11.12 2013  H2020-INFRAIA-2014-2015 Európske výskumné infraštruktúry - Integrácia a otvorenie výskumných infraštruktúr európskeho záujmu');
INSERT INTO public.t_project_program (id, name) VALUES (166, '11.12 2013  H2020-INFRASUPP-2014-1 Európske výskumné infraštruktúry - Podpora inovácií, ľudských zdrojov, politiky a medzinárodnej spolupráce');
INSERT INTO public.t_project_program (id, name) VALUES (167, '11.12 2013  H2020-ISIB-2014-1 Inovatívne, udržateľné a inkluzívne biohospodárstvo');
INSERT INTO public.t_project_program (id, name) VALUES (168, '11.12 2013  H2020-MG-2014_SingleStage_B Mobilita pre rast 2014-2015');
INSERT INTO public.t_project_program (id, name) VALUES (169, '11.12 2013  H2020-MSCA-RISE-2014 Marie Skłodowska-Curie actions - Výskum a inovácia výmeny personálu');
INSERT INTO public.t_project_program (id, name) VALUES (170, '11.12 2013  H2020-NFRP-2014-2015 EURATOM - štiepenie');
INSERT INTO public.t_project_program (id, name) VALUES (171, '11.12 2013  H2020-NMP-CSA-2014 Vedúce postavenie v základných a priemyselných technológiách - Výzva pre nanotechnológie, pokročilé materiály a výrobu');
INSERT INTO public.t_project_program (id, name) VALUES (172, '11.12 2013  H2020-REFLECTIVE-SOCIETY-2014 Reflexné spoločnosti: Kultúrne dedičstvo a európske identity');
INSERT INTO public.t_project_program (id, name) VALUES (173, '11.12 2013  H2020-SC5-2015-two-stage Rastúce nízkouhlíkové hospodárstvo, efektívne využívajúce zdroje s udržateľnou zásobou surovín');
INSERT INTO public.t_project_program (id, name) VALUES (174, '11.12 2013  H2020-SMEINST-1-2014 špecializovaný nástroj pre malé a stredné podniky Fáza 1 2014');
INSERT INTO public.t_project_program (id, name) VALUES (175, '11.12 2013  H2020-WIDESPREAD-2014-1 Šírenie excelentnosti a rozširovanie účasti - Widespread -2014- 1 Teaming');
INSERT INTO public.t_project_program (id, name) VALUES (176, '11.12 2013  H2020-WIDESPREAD-2014-2 Šírenie excelentnosti a rozširovanie účasti - WIDESPREAD ERA Chairs');
INSERT INTO public.t_project_program (id, name) VALUES (177, '12.04 2017  SwafS-08-2017 European Community of Practice to support institutional change');
INSERT INTO public.t_project_program (id, name) VALUES (178, '12.08 2009  OPV-2009-2.1-01-SORO Podpora ďalšieho vzdelávania');
INSERT INTO public.t_project_program (id, name) VALUES (179, '12.09 2014  E EA /EHP-SK06-I I-01 Mobilitné projekty medzi vysokými školami');
INSERT INTO public.t_project_program (id, name) VALUES (180, '12.09 2016  VV 2016 Verejná výzva na predkladanie žiadostí na riešenie projektov výskumu a vývoja v jednotlivých skupinách odborov vedy a techniky');
INSERT INTO public.t_project_program (id, name) VALUES (181, '12.10 2016  OPVaI-VA/DP/2016/1.1.3-02 Výzva na predkladanie žiadostí o nenávratný finančný príspevok na fázované projekty Univerzitných vedeckých parkov a výskumných centier (II. fáza) pre menej rozvinuté regióny');
INSERT INTO public.t_project_program (id, name) VALUES (182, '14.02 2012  OPIS-2012-2.1-06-DP Digitalizácia kultúrneho dedičstva rezortných a mimorezortných pamäťových a fondových inštitúcií“');
INSERT INTO public.t_project_program (id, name) VALUES (183, '14.06 2010  SK-AT 2010 APVV Slovensko - Rakúsko Verejná výzva na podávanie návrhov na spoločné projekty podporujúce spoluprácu medzi pracoviskami Slovenskej republiky a Rakúskej republiky na roky 2011 – 2012');
INSERT INTO public.t_project_program (id, name) VALUES (184, '14.07 2008  14.7.2008 KaHR-111DM-0801 Podpora zavádzania inovácií a technologických transferov');
INSERT INTO public.t_project_program (id, name) VALUES (185, '14.08 2009  SK-RS 2009 APVV Slovensko - Srbsko Verejná výzva k podávaniu návrhov na spoločné projekty výskumu a vývoja podporujúce spoluprácu medzi pracoviskami Slovenskej republiky a Srbskej republiky na roky 2010 - 2011');
INSERT INTO public.t_project_program (id, name) VALUES (186, '14.09 2011  VV 2011 APVV Verejná výzva na predkladanie žiadostí na riešenie projektov výskumu a vývoja v jednotlivých skupinách odborov vedy a techniky – VV 2011');
INSERT INTO public.t_project_program (id, name) VALUES (187, '14.10 2015  BB-06-2016 The regional dimension of bio-based industries');
INSERT INTO public.t_project_program (id, name) VALUES (188, '14.10 2015  H2020-BB-2016-2017  BIO-BASED INNOVATION FOR SUSTAINABLE GOODS AND SERVICES - SUPPORTING THE DEVELOPMENT OF A EUROPEAN BIOECONOMY');
INSERT INTO public.t_project_program (id, name) VALUES (189, '15.01 2009  VMSP 2009 APVV Verejná výzva na predkladanie žiadostí v rámci programu "Podpora výskumu a vývoja v malých a stredných podnikoch" VMSP 2009');
INSERT INTO public.t_project_program (id, name) VALUES (190, '15.01 2018  SK-CN-RD 2018 APVV Výskumná bilaterálna výzva Slovensko – Čínska ľudová republika');
INSERT INTO public.t_project_program (id, name) VALUES (191, '15.02 2010  EUROCORES 2010 APVV Výzva EUROCORES pre aktivity ENV v roku 2010');
INSERT INTO public.t_project_program (id, name) VALUES (192, '15.03 2013  SK-CZ 2013 APVV Slovensko – Česká republika Verejná výzva na podávanie žiadostí na riešenie projektov podporujúcich spoluprácu medzi organizáciami v Slovenskej republike a v Českej republike');
INSERT INTO public.t_project_program (id, name) VALUES (193, '15.03 2017  SK – CN  2017 APVV Verejná výzva na podávanie žiadostí na riešenie spoločných projektov výskumu a vývoja podporujúcich spoluprácu medzi organizáciami v Slovenskej republike a v Čínskej ľudovej republike');
INSERT INTO public.t_project_program (id, name) VALUES (194, '15.04 2009  SK-CZ 2009 APVV Slovensko – Česko Verejná výzva k podávaniu návrhov na spoločné projekty vedecko-technickej spolupráce medzi pracoviskami Slovenskej republiky a Českej republiky na roky 2010-2011');
INSERT INTO public.t_project_program (id, name) VALUES (195, '15.07 2010  APVV PP7RP 2010 Verejná výzva na podávanie žiadostí o registráciu a refundáciu nákladov na prípravu projektov 7. rámcového programu Európskej únie – PP7RP 2010');
INSERT INTO public.t_project_program (id, name) VALUES (196, '15.07 2016  APVV – DS 2016 Dunajská stratégia 2016');
INSERT INTO public.t_project_program (id, name) VALUES (197, '15.07 2016  H2020 Tri výzvy v rámci Horizont 2020 pre inovácie v oblasti energetiky');
INSERT INTO public.t_project_program (id, name) VALUES (198, '15.10 2012  OPV-2012/1.2/05-SORO Podpora zlepšenia kvality vysokých škôl a Slovenskej akadémie vied');
INSERT INTO public.t_project_program (id, name) VALUES (199, '16.04 2015  SK-FR 2015 APVV Verejná výzva na podávanie žiadostí na riešenie projektov podporujúcich spoluprácu medzi organizáciami v Slovenskej republike a vo Francúzskej republike');
INSERT INTO public.t_project_program (id, name) VALUES (200, '16.05 2018  SK-SRB 2018 APVV Verejná výzva Slovensko – Srbsko 2018');


insert into project_program_copy
    (select * from project_program);

update project_program_copy
set original_project_program_id= true_orig_id
    from (select tp.id as true_orig_id, pp.original_project_program_id as bad_orig_id, pp.project_program_id as id from t_project_program tp
                                                                                                                        join project_program pp on tp.name = pp.project_program_name
      where pp.original_project_program_id != tp.id) as tt
where project_program_id = tt.id;

drop table if exists project_copy;
create table project_copy (like project);

insert into project_copy
    (select * from project);

update project_copy as pc
set project_program_id = tr.new_id
    from (select pp.project_program_id as old_id, ppc.project_program_id as new_id from project_program pp
                                                                                        join project_program_copy ppc on pp.original_project_program_id = ppc.original_project_program_id
      where pp.project_program_name != ppc.project_program_name) tr
where pc.project_program_id = tr.old_id and pc.original_project_id is not null;

update project
set project_program_id = tr.id
    from (select pn.project_program_id as id,pn.project_id from project_copy pn) tr
where project.project_id = tr.project_id;
update project_program
set original_project_program_id = tr.original_project_program_id
    from (select * from project_program_copy) tr
where tr.project_program_id =  project_program.project_program_id;
drop table project_program_copy;
drop table project_copy;
drop table t_project_program;
