select user_id, name, array_agg(distinct search_expression)
from user_profile
       left join search_hist on user_profile.user_id = search_hist.user_insert
where deleted = false
group by user_id, name
order by user_id;

select search_expression, count(distinct user_insert) as countd, count(search_hist_id)
from search_hist
group by search_expression
order by countd desc;

select user_profile.user_id, name, array_agg(expert_id)
from user_profile
       left join user_expert on user_profile.user_id = user_expert.user_id and user_expert.deleted = false
group by user_profile.user_id, name
order by user_profile.user_id