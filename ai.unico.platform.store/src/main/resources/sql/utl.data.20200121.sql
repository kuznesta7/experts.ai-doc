truncate commercialization_project cascade;
drop table if exists commercialization;
create table commercialization
(
    -- IDs
    commercialization_id                  bigserial primary key                           not null,
    commercialization_uuid                uuid unique                                     not null,

    -- reference fields
    commercialization_category_id         bigint references commercialization_category    not null,
    commercialization_status_id           bigint references commercialization_status_type not null,
    commercialization_priority_id         bigint references commercialization_priority    not null,
    intelectual_property_owner_id         bigint references organization                  not null,
    commercialization_investment_range_id bigint references commercialization_investment_range,
    commercialization_domain_id           bigint references commercialization_domain,

    -- descriptive fields
    name                                  text                                            not null,
    executive_summary                     text,
    use_case                              text,
    pain_description                      text,
    competitive_advantage                 text,
    technical_principles                  text,
    idea_score                            integer,
    technology_readines_level             integer,
    start_date                            timestamp,
    end_date                              timestamp,
    status_deadline                       timestamp,
    profile_image                         bytea -- ?? oid type
) inherits (trackable);


insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '5f8fa7f8-64f5-43b6-a941-02b885e68731'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'reviewBusiness')
     , 3
     , 110216
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '1M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'ENVIRONMENT')
     , 'Modified vermiculite'
     , 'Alternative to activated carbon - sorbent for non-polar compounds (petroleum hydrocarbons, etc.) as well as for metal cations. Vermiculite is modified with organic compounds to improve sorption properties.'
     , 'sorption of non-polar compounds from water in heavy polluted areas'
     , 'ctivated carbon = pH-dependent sorption
different types of activated carbon for specific types of pollution - vermiculite is more universal with better sorption capacities'
     , 'better sorption of non-polar compounds
combat agents (yperite)
antibractterial effects - inhibitory effects against severe bacterial strains Streptococcus agalactiae, Staphylococcus aureus and Yersinia pestis
pH independent (between 2-12)
fine colloidal filter'
     , 'Modified vermiculite is highly potent sorbent of organic non-polar substances, mainly from highly contaminated waters and gases. They also show efficacy for the removal of inorganic cations and substances of a more polar nature. Clay minerals in natural form do not show good sorption properties towards non-polar organic compounds, they are excellent adsorbents for inorganic and hydrophilic substances. After modification by organic cations, containing long carbon chains, there is a significant change in their sorption properties, which can be used for cleaning contaminated environmental compartments.'
     , 7
     , 57
     , '2019-07-28 00:30:34'
     , NULL
     , NULL
     , '2019-07-28 00:30:34'
     , '2020-01-13 16:02:25'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '65233d56-19d0-42b5-8036-0b2fa1fe2aee'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'input')
     , 4
     , 73497
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'IOT')
     , 'Plant''s acoustic signal measurement'
     , 'A device for measuring plant acoustic signals, which can indicate different adverse conditions and subsequently help optimise cultivation.'
     , 'optimizing the environment in greenhouses for better growing conditions'
     , '- plants under stress conditions reduce production/reduce growth rate, etc.
- stress conditions are usually approximated by measurements of external factors'
     , 'direct measurement of plant stress factors'
     , 'measuring devices that can pick up very quiet acoustic signals emitted by plants that are related to the response to stress factors'
     , 3
     , 44
     , '2019-12-03 12:31:11'
     , NULL
     , NULL
     , '2019-12-03 12:31:11'
     , '2019-12-06 12:39:19'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '8f099ad2-b56f-477a-b5b8-69ab1c4c4acf'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'input')
     , 1
     , 128678
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'CIVIL ENGINEERING')
     , 'Concrete with textile shreds'
     , 'A porous concrete that, thanks to its unique composition, can retain water and then gradually evaporate it, cooling the surrounding environment e.g. in cities.'
     , 'Sidewalks and other areas in cities'
     , 'Porous concrete can alleviate rainwater runoff in cities and on roads, but in the context of HEAT ISLANDS in cities, it cannot effectively cool the surrounding environment.'
     , 'Rainwater retention in urban areas and decreasing the HEAT ISLAND effect.'
     , 'Porous concrete filled with soaked non-flammable material'
     , 6
     , 62
     , '2019-12-03 10:16:18'
     , NULL
     , NULL
     , '2019-12-03 10:16:18'
     , '2019-12-06 12:39:19'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '7e55043a-e163-40c4-9e91-9a1231391bfe'
     , 1
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'reviewBusiness')
     , 3
     , 128687
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'AUTOMATION')
     , 'Defective Glass Detection'
     , 'Visual inspection of defects (bubbles, cracks, scratches) of glass panels using modern algorithms and machine vision.'
     , 'Integrace do existuj�c�ch v�robn�ch linek, zejm�na u velk�ch v�robcu ploch�ch sklenen�ch tabul�.'
     , 'N�klady na lidskou pr�ci - soucasne se del� rucne. Pr�padne se daj� por�dit velmi drah� laserov� stroje, kter� jsou ale kvuli cene casto financne nedosa�iteln�.'
     , 'Integration into existing production lines, especially for large producers of flat glass panes.'
     , 'The technology uses machine vision and image processing algorithms in combination with an array of line-based camera sensors, capacitance sensors and high-powered lighting.'
     , 5
     , 57
     , '2019-08-21 13:36:47'
     , NULL
     , NULL
     , '2019-08-21 13:36:47'
     , '2020-01-13 16:04:04'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT 'a7385a35-c8c7-42bf-8638-1c9e4c59823b'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'reviewBusiness')
     , 1
     , 128688
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '1M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'IOT')
     , 'Environment Quality Sensor'
     , 'A sensor (box) for the indoor environment, it measures various quantities: temperature, relative humidity, CO2, barometric pressure, VOC volatile organic matter concentration, solid particles.'
     , 'Measuring air quality in enclosed spaces - industrial enterprises, schools, workplaces, etc.'
     , 'The constant demand for simple smart sensors that can identify harmful substances in the air.'
     , 'A strong advantage lies in the ability of the device to measure multiple quantities at once, including CO2 and NOx, which most others can''t do.'
     , 'Working range:
- Air temperature -40 to +85 �C (-40 to +176 �F)
- Relative air humidity 0 to 90 %RH non-condensation
- CO2 concentration 300-5000 ppm
- VOC Volatile Organic Matter Concentration: IAQ Index 0-500
- Barometric pressure 300-1100 HPa
- Solid content (PM10, PM2.5) 0.0-999.9�g/m3 (not used in current version)

Communication interface:
- WiFi 802.11 b/g/n 2.4GHz (used in current version)
- Bluetooth v4.2 BR/EDR/BLE (not in use)
- Optional LoRaWAN - Class A, 14dBm, SF 7-12, 868MHz (used in current version)
- Optional IQRF (TR 72D, TR 76D modules) (not used)
- Common Installation Bus (CIB) (TECO CIB module) (not in use)
- RS-485 & Modbus RTU (configurable)

Indications and views:
- 2x RGB LEDs for basic sensor status display, internal air quality indication if applicable, controllable brightness and color'
     , 7
     , 72
     , '2019-08-22 07:56:45'
     , NULL
     , NULL
     , '2019-08-22 07:56:45'
     , '2020-01-13 16:03:43'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT 'b15dcd2a-83ea-44c0-86fd-660eccef8f44'
     , 1
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'investActive')
     , 1
     , 73558
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'IOT')
     , 'BlueTooth Spot Tracker'
     , 'A very durable and energy-intensive bluetooth transmitter that, combined with receivers, will allow you to track the position of objects in a linear motion.'
     , 'Mainly in areas where working or natural conditions are complex and where existing RFID or RTLS chips are not usable due to their potential damage.
- mines, quarries
- monitoring animal behaviour in the wild'
     , 'Existing devices are energy-intensive, expensive, cannot withstand difficult conditions or are unable to operate in a closed environment (GPS).'
     , 'Lower price, easy installation and high resistance.'
     , 'The technology uses Bluetooth 4.0 transmitters to approximate positioning based on the known position of the receivers.'
     , 4
     , 49
     , '2019-08-21 14:45:33'
     , NULL
     , NULL
     , '2019-08-21 14:45:33'
     , '2019-12-06 12:39:19'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '67ed025b-33a3-4c36-b655-cbc1f1609c5e'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'reviewBusiness')
     , 1
     , 140347
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '1M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'SOFTWARE')
     , 'G-NUT'
     , 'G-Nut is a software tool that allows for very accurate targeting in both static and kinetic mode, atmospheric monitoring and accurate time determination.'
     , 'Cloud service for
- Autonomous and Common Navigation Systems
- Geodetic systems, GIS
- monitoring systems for predicting natural disasters
- weather forecasting and climate analysis
- Animal monitoring, object tracking systems
- surveying and mapping
Usage in geodesy, seismology, meteorology, climatology, but also for highly accurate navigation systems and object tracking.'
     , ''
     , '- lower price
- time can be accurately measured over GPS (!)
- cloud service -> will be cheaper and available for small businesses'
     , 'The software tool is based on Precise Point Positioning technology and allows very precise targeting using multi-GNSS constellations. The software works with data from all modern GNSS devices GPS, GLONASS, Galileo and BeiDou.'
     , 7
     , 76
     , '2019-08-22 07:38:09'
     , NULL
     , NULL
     , '2019-08-22 07:38:09'
     , '2019-12-06 12:39:19'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT 'edbf2ecc-bb92-43bf-b364-0ca87933f5fe'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'input')
     , 3
     , 110216
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'HEALTHCARE')
     , 'Magneto-optic SPR biochips'
     , 'Technology allows rapid identification of disease due to biomarkers in body fluids. MO-SPR is more sensitive than SPR and have the potential to recognise e.g. cancerous diseases at very early stages.'
     , 'miRNA analysis in femtomoles to detect early stages of disease'
     , '- need to identify disease quickly and from relatively very low concentrations
- routine examinations are long and expensive
- often lengthy tests for just one type of disease'
     , '- rapid (in the order of tens of minutes), repeatable measurement
- more sensitive than SPR biochips
- one biochip can make up to 500 different substances
- automaticity of measurement in the future'
     , 'Magneto-optical surface plasmon resonance (MOSPR) sensors benefit from a magneto-optic enhancement with respect to surface plasmon resonance (SPR) sensors, making these devices attractive for biosensing applications. Typical design compromises seek to balance magneto-optic effects and optical losses associated with surface plasmon waves extending to the ferromagnetic layer. Here, we demonstrate that Co/Au multilayers can yield sizeable MOSPR improvements in spite of the relative high total Co layer thickness.
These structures showed excellent magneto-optic surface plasmon resonance properties. The MOSPR sensitivity is enhanced by a factor of 3 and 4 with respect to the SPR sensor in an Air-He and Water-Methanol media, respectively. Multilayer-based MOSPR sensors provide an advantage over comparable bilayer designs when operated in Water-based media, which targets common biosensing platforms.'
     , 2
     , 42
     , '2019-08-22 08:43:31'
     , NULL
     , NULL
     , '2019-08-22 08:43:31'
     , '2019-12-06 12:39:19'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '7c1e499b-13b2-4e16-b8c0-2fc4352d10e2'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'input')
     , 1
     , 128678
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'CIVIL ENGINEERING')
     , 'Porous fibrobeton from recyclate'
     , 'Porous fibreconcrete from bricks or concrete unsorted recyclate with the use of PET material as fibres.'
     , 'Use of a wide grain scale of recycled bricks and concrete for the production of porous concrete, where bands of PET material (from plastic bottles, etc.) are used as hymning fibres. The practical use of the material is particularly for the singing elements of embankments or waterworks.'
     , 'Concrete made from recyclables is, in the vast majority of cases, dependent on good sorting of the material.'
     , 'With the use of PET bands and unsorted recycled material the concrete is much cheaper then the common recycled concrete.'
     , 'The possibility to use unsorted waste building material (respectively bricks or concrete) for fibrobeton with reinforcement from different materials (including PET bottles, etc.) - the concrete has good mechanical properties comparable to classical fibrobetons. Compared to conventional fibreboards, longer strips are needed as reinforcements - about half as much (110 mm versus 60 mm) - because the concrete has bigger gaps between the grains than for sorted recyclates.'
     , 6
     , 63
     , '2019-12-03 10:30:55'
     , NULL
     , NULL
     , '2019-12-03 10:30:55'
     , '2019-12-06 12:39:19'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '378ca03c-b2c8-45c7-b143-9cee602f6692'
     , 1
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'investActive')
     , 1
     , 110258
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'AUTOMATION')
     , 'System for automatic weld joints inspection'
     , 'The technology for automatic detection of weld failures and deficiencies provides an alternative to manual weld inspection on production line output control.'
     , 'The system is mainly designed for variable production lines of industrially processed welds. The unique machine learning technology allows the technology to be adapted for other applications, where image evaluation would be used to detect defects in materials on production lines with a higher degree of variability.'
     , 'Currently, welds are inspected manually - visually or destructively - by factory workers. However, due to the human factor, the control is not perfect and destructive control causes damage to the pieces produced.'
     , 'Compared to competition, technology is built on a unique system with artificial intelligence that is able to learn how to evaluate results based on external stimuli (the operator). This makes the technology very easy to scale for different types of product design or quality and does not require complex reprogramming for each new application. Although the technology combines SW and HW, the price is an order of magnitude lower than that of the competition. At the same time, the system is very simple to use and there is no need for a specially trained worker.
The system is based on artificial intelligence, thanks to which it is able to learn and adapt to the client''s requirements. Current results show a higher success rate for defect detection than the average employee, while at the same time the whole system is easily scalable for other disciplines thanks to machine learning capabilities.'
     , 'Technology combines SW and HW, with SW using machine learning and convolutional neural networks that learn faster than normal. This is because the system learns based on a person showing them right and wrong pieces over a period of time, or, for example, the length of the grip can determine the likelihood of error or the extent of damage. HW then makes use of existing technology and the uniqueness lies primarily in the design of equipment for a specific environment, or for a specific environment that is relatively standard (e.g. lighting conditions). The technology is primarily developed for around 10% of industrial welds, but can also be applied to inspection in other areas where experience is needed to assess the quality of the outputs. Technology is already more successful than humans (96% vs. 90-95%).'
     , 8
     , 73
     , '2019-07-28 00:33:52'
     , NULL
     , NULL
     , '2019-07-28 00:33:52'
     , '2019-12-06 12:39:19'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT 'fc073185-66f2-4373-bf0d-6cf915dd9177'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'draft')
     , 1
     , 73497
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'HARDWARE')
     , 'E-multicar'
     , 'Purely electric multi-vehicle for urban use - a new propulsion system as well as the entire structure of the rover.'
     , 'small electronic rovers for city maintanance'
     , 'conventional electric vehicles for urban maintenance use hydraulic systems in addition to the electric motor and other separate units to control the components on the vehicle'
     , 'everything is operated with one electric engine which is more effective'
     , ''
     , 4
     , 58
     , '2019-12-03 13:13:34'
     , NULL
     , NULL
     , '2019-12-03 13:13:34'
     , '2019-12-06 12:39:19'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '8eecf45c-26b8-49aa-9817-1066496b3891'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'reviewIdea')
     , 1
     , 73497
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'HARDWARE')
     , 'Three-point plough linkage'
     , 'New type of suspension system for ploughing rails on tractors to allow for significant reduction of asymmetric drawbar loads, reduction of maintenance costs and reduction of petrol consumption.'
     , '- on demand service - it is possible to attach to existing machines

- hydraulically controlled suspension for active tool management and load optimisation

- for farmers, foresters - for towing equipment'
     , 'uneven loads on tractors when ploughing'
     , '- there''s nothing like it on the market

- reduce orb depth variation

- evenly weighted rear wheel loads
'
     , 'The solution concerns the way of subsidising the tractor''s rear wheels when processing the soil with tools, in particular with a plough, attached to a three-point hinge of the tractor, in which the vertical position of the attached tools is set relative to the tractor via the position of the arms of the lifting device and the length of the upper tow of the three-point hinge of the tractor is set. After adjustment of the length of the upper tow of the three-point suspension of the tractor, the axial force exerted in the upper tray is determined and continuously monitored and automatically maintained at the stated value when the tractor is travelling, while the set vertical position of the connected tool is also continuously monitored and maintained, which is also automatically permanently maintained at the stated value, thereby maintaining the vertical position of the connected tool and thus the constant depth of the ploughing during this control. The solution also concerns the equipment to implement the abovementioned way of subsidising rear-wheel tractors when processing land.'
     , 5
     , 53
     , '2019-12-03 14:07:26'
     , NULL
     , NULL
     , '2019-12-03 14:07:26'
     , '2019-12-06 12:39:19'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '93a68a87-b51f-455b-b892-f4bcedc5d498'
     , 1
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'reviewIdea')
     , 2
     , 92023
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'AI')
     , 'Machine translation'
     , 'Unique translation software based on AI and machine learning.'
     , 'Machine translation of websites, eshops, etc. through API.'
     , 'Google and other services are not that acurate.'
     , '- more acurate than google

- EN, ES, Hindu, GE, CZ

- cloud service'
     , 'The corpus is equipped with automatic annotation at a deep syntactic level of representation and alternatively in Universal Dependencies. The processing pipeline includes part-of-speech tagging, parsing, named entity recognition, semantic role labelling, coreference resolution etc.'
     , 6
     , 65
     , '2019-12-03 14:20:36'
     , NULL
     , NULL
     , '2019-12-03 14:20:36'
     , '2019-12-06 12:39:19'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT 'a2824c29-526a-4a88-97b4-7270e4c466ff'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'reviewBusiness')
     , 1
     , 92046
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'ENVIRONMENT')
     , 'Bio-fertilisers from slaughterhouse waste'
     , 'Process for the production of organic fertilisers from waste fats and other residues from slaughterhouse.'
     , '- input material for fertilizer manufacturers based on slaughterhouse wastes

- replacing existing fertiliser additives'
     , 'much unused slaughterhouse waste'
     , ''
     , 'By-product of deproteinisation of waste fats for biodiesel preparation - deproteinisation allows proteins to be used for hydrolysate, which can be the basis for the production of organic fertilisers.'
     , 6
     , 59
     , '2019-12-03 15:18:51'
     , NULL
     , NULL
     , '2019-12-03 15:18:51'
     , '2019-12-06 12:39:19'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '270d5665-02a3-4a19-ae25-ef55ba3f5132'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'input')
     , 1
     , 92046
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'FOOD & HEALTH')
     , 'Poultry gelatine'
     , 'Method of production of gelatine using food enzymes from poultry residues, which are normally processed at the most for compound feed. The technology is particularly interesting due to the low restructuring requirements of existing plants, low energy demand and feedstock.'
     , '- production of gelatine from poultry residues for food and pharmacy

- sale even in asia - kosher/hallal'
     , '- a lot of poultry waste for which there is not much use - max for compound feed

- acidic/alkaline methods are financially and time consuming

- kosher/hallal foods

- beef and pork is expensive and difficult to process'
     , '- cheap inputs, fast process

- there is no need to build a new line, just put other inputs and other chemicals into existing plants (procedure very similar to existing ones)'
     , ''
     , 4
     , 61
     , '2019-12-03 13:56:41'
     , NULL
     , NULL
     , '2019-12-03 13:56:41'
     , '2019-12-06 12:39:19'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '1755ce87-26c4-481d-9aa9-54de446b7cbf'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'reviewIdea')
     , 3
     , 128692
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'HEALTHCARE')
     , 'Smart bed sheet'
     , 'A smart sheet capable of measuring temperature, pressure and humidity.'
     , '- healthcare, home care

- sport clothes'
     , ''
     , '- the thread is not recognizable to the touch - theoretically a very wide use in textures

- cheap to produce

- easy for home use
'
     , ''
     , 6
     , 63
     , '2019-12-03 14:35:46'
     , NULL
     , NULL
     , '2019-12-03 14:35:46'
     , '2020-01-06 15:28:43'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT 'e1bff618-eda3-4b8c-89c7-3ea69f443129'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'reviewBusiness')
     , 2
     , 128678
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'CIVIL ENGINEERING')
     , 'Uniaxial Shear Tester'
     , 'Uniaxial Shear Tester (hereinafter UST) is a tool for testing of asphalt mixtures properties. It is used in combination with Universal Testing Machines (UTM) or Nottingham Asphalt Tester (NAT).'
     , '- Asphalt mixture design

- Pavement maintenance and quality checks 

- Asphalt mixture components optimization 

- Assessment of asphalt mixture resistance to permanent deformations.'
     , ''
     , '- Low price

- Considerably lower operational costs compared to using Hamburg-Wheel Tracking Device or Superpave Shear Tester 

- Simplicity of sample preparation 

- Can be used for testing samples taken directly from roads or samples prepared in a laboratory 

- Only standard laboratory equipment is needed (UTM, NAT, Gyrator) 

- Shear stress is applied in the direction corresponding to the stress created from traffic load in pavement. This enables more suitable testing of shear properties of asphalt mixture. 
'
     , 'Developed UST is capable to measure shear properties of asphalt mixtures and thus the UST may be utilized to determine asphalt mixture resistance to permanent deformations, calculate pavement rutting, mix design and to perform pavement materials quality checks.
Further, unlike other solutions on the market, it can also be used to check the quality of asphalt mixtures from cores taken from the pavement structures.
'
     , 8
     , 61
     , '2019-12-03 15:05:53'
     , NULL
     , NULL
     , '2019-12-03 15:05:53'
     , '2019-12-06 12:39:19'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '20613a0e-b9f6-4c2b-adf4-7fe4d1ab9562'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'draft')
     , 3
     , 136315
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'ENVIRONMENT')
     , 'Bee feed'
     , 'Feed for bees, which, in addition to high protein content, also contains extracts from choroshi and Chlorella algae, which also gives it antibacterial and antiviral effects, and contributes significantly to bee colony resistance.'
     , 'Feed mix for bees for periods of dormancy'
     , 'Common pollen substitutes and other supplements do not contain all the desirable food ingredients for bees - vitamins, proteins, etc., or have favourable antiviral and antibacterial effects.
'
     , 'As well as a balanced composition of proteins, sugars and vitamins, the feed contains extracts from choroshi and Chlorella algae, which make the bees more resistant to pests and parasites.
'
     , 'The desired amount of protein in the compound feed in form acceptable to bees is achieved with the addition of Chlorella algae. At the same time, algae have beneficial preventive effects against bee plague. Scientists are now studying the effects of adding extracts from certain types of mushrooms to help bees fight parasites.'
     , 4
     , 55
     , '2019-12-09 09:43:48'
     , NULL
     , NULL
     , '2019-12-09 09:43:48'
     , '2019-12-10 09:48:04'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '454a99e6-be9a-4156-9ff6-e914f281661a'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'draft')
     , 1
     , 136315
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'HEALTHCARE')
     , 'Lyme borreliosis analytical method'
     , 'A lyme disease analytical method based on new markers to allow for sooner and more accurate diagnosis.'
     , 'Early analysis of Lyme disease transmitted by ticks. The analytical method also allows the production of simple test kits for home use.'
     , 'The current methods can only be used after about 30 days, depending on the markers studied, and even then they are only conclusive in 70% of cases.'
     , 'Methodological development based on new markers appearing in the body about 14 days earlier than those for current methods used (Western Blot, ELISA).'
     , 'Testing on animal samples has shown very good effectiveness, now scientists are examining human samples and developing a methodology applicable in practice.'
     , 4
     , 61
     , '2019-12-09 10:02:39'
     , NULL
     , NULL
     , '2019-12-09 10:02:39'
     , '2019-12-09 10:06:01'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '23dc8b23-8c6a-4ba4-b160-1332a03486e8'
     , 1
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'draft')
     , 1
     , 136315
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'HEALTHCARE')
     , 'Tick breeding facility'
     , 'A facility for breeding clean and infected ticks for pharmaceutical research.'
     , 'The breeding and sale of both pure and infected ticks is crucial for research into new drugs and products for tick-borne diseases.'
     , 'There is a relatively high demand for clean and infected ticks from both pharmaceutical companies and research teams involved in the transmission and treatment of borreliosis or encephalitis. Currently, ticks must be kept on clean or infected laboratory animals, which are expensive.
'
     , 'The proposed device uses a Czech utility model for a semi-automatic line with the possibility of in vitro tick behaviour thanks to a membrane feeding blood intake system. The line thus provides simpler and cheaper tick breeding with the possibility of accurate monitoring of both infected and pure individuals.
'
     , 'The technical specification of the device is described in utility model CZ 31165.'
     , 6
     , 63
     , '2019-12-09 10:52:01'
     , NULL
     , NULL
     , '2019-12-09 10:52:01'
     , '2019-12-09 10:52:20'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '31dd4f81-ded2-467c-980a-af88eb5c7382'
     , 1
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'reviewIdea')
     , 4
     , 73497
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'HARDWARE')
     , 'Portable UV spectrometer'
     , 'A portable UV spectrometer that can detect the composition of e.g. food to determine its formation, method of preparation, or the content of required substances.'
     , 'Quick and cheap composition analysis of food, drugs, and other organic substances.'
     , 'There is not a quick and non-destructive method for quality control of food, drugs etc.'
     , 'Quick, cheap, portable'
     , 'Small, portable UV spectrometer that can identify and compare the composition of foods, medicines, etc. Potentially, the device can be used to quickly and cheaply (!) identify active ingredients in medicines or, conversely, the content of artificial substitutes in foods. The technology should work by comparing the UV spectra of the test substances with the database of required chemical compounds, and by assessing the level of authenticity (or content of the required substances) accordingly.
'
     , 4
     , 40
     , '2019-12-03 12:11:13'
     , NULL
     , NULL
     , '2019-12-03 12:11:13'
     , '2020-01-13 16:04:40'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '19aae17c-1ba4-4cd5-9fbd-238d75d29d2b'
     , 1
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'reviewIdea')
     , 4
     , 92046
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'IOT')
     , 'Baby Shoe Size Prediction'
     , 'Software and hardware for measuring the dimensions of children''s feet and predicting the rate of growth. The system is designed to help parents correctly select their children''s shoes based on algorithms to estimate the growth rate of their children''s feet according to the size, height, weight and size of their parents'' feet.'
     , '- inventory optimization

- notifications for parents to buy new shoes for children in time

- prevention of foot deformities and health problems'
     , '- the problematic choice of shoe sizes for children (especially the livelier ones)

- parents often fail to monitor the condition of children''s shoes and how quickly children''s feet grow (for example, if they are school shoes that a parent cannot follow)

- children can''t tell themselves when a shoe is too small

- health problems - children''s legs are very malleable and often deformities can occur without the child being aware

- parents forget to check children''s shoes regularly'
     , '- complete system, simple, automated from measurement to notification and recommencement

- there is no competition yet

- the system is particularly applicable where there is a large turnover of sales staff, who then fail to give proper advice on the selection - the system does it for them or tells them to do it'
     , '- based on predictive models and algorithms + using AI and collecting large amounts of data, the whole model is being refined (but for that, the big data still needs to be collected somewhere - and that will be when the measuring equipment is ready)

- Prediction is addressed in various studies, but there is no software to implement this in stores

- the measuring device will be wireless so that children can''t destroy it too much and at the same time make it as easy as possible to manipulate

- each child will have a profile in the system, where data will be stored and used to notify parents when shoes need changing (even for different sports, etc.)

- the system will mainly serve vendors, who will also be able to use the system to optimize inventory according to data from the system

- the system is ready to insert data from manufacturers about the size of the shoes on the basis of which it would then be easier to recount - the question is whether manufacturers would be able and willing to provide that data

- the record could then be very accurate (numbering is inputting)'
     , 5
     , 66
     , '2019-12-03 12:51:52'
     , NULL
     , NULL
     , '2019-12-03 12:51:52'
     , '2020-01-13 16:04:30'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '9eccff24-0f3a-46d7-ba83-893453cc7f3a'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'backlog')
     , 4
     , 73487
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '1M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'ENVIRONMENT')
     , 'ANALUM'
     , 'BIOLUMINESCENCE KIT FOR FAST POLLUTION DETECTION - rapid, cheap and simple analysis of inorganic and organic substances in water based on analysis of bioluminescent radiation'
     , 'areas of water pollution with PAU, both for decontamination companies and for industry'
     , '- slow and costly analysis of organic compounds and polutants in water'
     , '- Other portable devices can only perform basic parameters (conductivity, TDS, DO, pH, temperature), chemical analyses are done in laboratories or several reagents and complex field applications are needed

- cheap, fast, simple, portable

- smartphone evaluation

- both quality and quantity within orders

- life approx. 6 months, single storage

- Wide spectrum of analysable substances simultaneously

- rapid bacterial degradation in leakage'
     , 'bacteria shines depending on whether they meet pollutants
'
     , NULL
     , 50
     , '2020-01-14 15:24:46'
     , NULL
     , NULL
     , '2020-01-14 15:24:46'
     , '2020-01-14 15:25:14'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '3f2bf888-217d-4b48-8c17-b8cdcdf007a4'
     , 1
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'backlog')
     , 4
     , 128692
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'IOT')
     , 'Wireless Power Transfer in Rotary Machines'
     , 'A specific part of wireless transmission for systems where information cannot simply be read from a machine that has rotating parts.'
     , '- rotary machines sensoring'
     , 'Quantities inside rotating machines can''t be properly read in real time in operation. Important both for development but also for real operations. Optimizing production and optimizing operations. Main industry-related use 4.0, e.g. somewhere where some variables, e.g. temperature, need to be constantly monitored.
'
     , '- can be added to existing devices
- more accurate than existing devices

- cheaper'
     , ''
     , 6
     , 53
     , '2020-01-14 15:36:38'
     , NULL
     , NULL
     , '2020-01-14 15:36:38'
     , '2020-01-14 15:36:38'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '8b9f67dd-2aea-4990-8c4d-284fff30e21f'
     , 1
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'completed')
     , 2
     , 128711
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'AI')
     , 'Entrant'
     , 'developing methodology of stress measurement based on physiological factors'
     , 'Objectively measuring stress level in harsh environments, e.g. army, space, firefighters, doctors, athletes'
     , ''
     , ''
     , ''
     , 4
     , 68
     , '2020-01-14 15:47:19'
     , NULL
     , NULL
     , '2020-01-14 15:47:19'
     , '2020-01-16 10:07:41'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT 'dfd170fe-6b40-44d2-acf6-144bb8a601ac'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'draft')
     , 4
     , 110258
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'HARDWARE')
     , 'Chalcogenide glasses'
     , 'It is a project to develop chalconide glass technologies with the addition of Selene, Nickel and other special metals that can be used in the electrical industry, both in battery and memory production.'
     , 'battery and PC memory production'
     , ''
     , 'store charge to be conductive or non-conductive in one current direction, to switch, i.e. to have 0 or 1.

- Size (nanometers)

- up to 1000x higher speed (for memories),

- 1000x higher lifetime (i.e. amount of data written)

- up to 10 times higher data write density.

- Technology thus achieves several consistently higher qualities than current flash technologies.
'
     , 'The technology operates on the principle of chalconid glass, i.e., glass with a metallic content (e.g., silver or copper). The combination of these materials allows much better properties for the transmission of DC electricity.
'
     , 2
     , 46
     , '2020-01-14 16:00:51'
     , NULL
     , NULL
     , '2020-01-14 16:00:51'
     , '2020-01-14 16:00:51'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT 'b7f92194-c9d8-4a7f-b3ed-dc9764cca359'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'input')
     , 3
     , 128688
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'CIVIL ENGINEERING')
     , 'Hybrid photovoltaic panel'
     , 'A hybrid photovoltaic panel producing both heat and electricity.'
     , ''
     , 'normal panels only produce electricity or just heat, if the customer wants both, the panels on the roof take up too much space
'
     , '- superior efficacy (up to 30% in combination)

- higher durability (due to glazing)

- tougher in difficult conditions (due to glazing)
'
     , ''
     , 6
     , 53
     , '2020-01-14 16:07:55'
     , NULL
     , NULL
     , '2020-01-14 16:07:55'
     , '2020-01-14 16:07:55'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '7f5b82f6-ff39-448a-87ac-e46ad56e63bf'
     , 1
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'backlog')
     , 4
     , 128679
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'HEALTHCARE')
     , 'HeRo'
     , 'Systemic and diastolic pressure measurement technology.'
     , '- patients monitoring in hospital

- smartwatch app

-  monitoring of patients with pressure problems with local doctors
'
     , 'there''s no other tool that can continuously measure patient pressure'
     , '- Patent technology

- Near the market

- Not dependent on HW itself'
     , 'analysis of systolic and diastolic pressure based on heartbeat meassurement on wrists with smart watch and arm bends'
     , 6
     , 42
     , '2020-01-16 09:59:24'
     , NULL
     , NULL
     , '2020-01-16 09:59:24'
     , '2020-01-16 09:59:25'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT 'b77c357e-a5a0-4eab-acbe-dc794b140c5a'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'backlog')
     , 4
     , 73520
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'HEALTHCARE')
     , 'Molecular permeability methodology'
     , 'Methodology of modeling how molecules will behave in a particular environment, such as how the substance will permeate the skin.'
     , 'There is potential application in the cosmetics industry as there may be no animal testing.
'
     , 'animal testing of drugs and cosmetic products'
     , ''
     , ''
     , 6
     , 45
     , '2020-01-16 10:07:06'
     , NULL
     , NULL
     , '2020-01-16 10:07:06'
     , '2020-01-16 10:08:33'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '6659fd11-5a3e-4883-a084-ddf000cd465f'
     , 1
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'backlog')
     , 4
     , 73520
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'ENVIRONMENT')
     , 'Aqua Chip'
     , 'One-time test for 6 or more chemicals in water with the help of mobile app'
     , '- aquarium

- water drinking test in developing countries'
     , '- costly and time inefective water quality testing in aquariums

- long and costly analysis of pollutants in drinking water'
     , '- cheap
- more reliable than the competition
- can analyse more pollutants than commonly available testers (heavy metals)
'
     , 'mobile appk evaluation of the level of contamination based on the distinct coloration'
     , 4
     , 46
     , '2020-01-16 10:21:26'
     , NULL
     , NULL
     , '2020-01-16 10:21:26'
     , '2020-01-16 10:21:26'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '68020dfa-c4c8-4f6e-8992-3cf2347b221b'
     , 1
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'completed')
     , 2
     , 128670
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'SOFTWARE')
     , 'Naviterier'
     , 'mobile applications for the blind or visually impaired.'
     , 'Navigation that helps an individual move safely within the city.'
     , ''
     , '
- cooperation with guide centres for blind people (typhoids) - they also exist abroad

- favoured use of map materials from CEDA'
     , ''
     , 7
     , 55
     , '2020-01-16 10:35:50'
     , NULL
     , NULL
     , '2020-01-16 10:35:50'
     , '2020-01-16 10:35:50'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '04d8d652-a29e-4e94-906f-d315a97c19f5'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'reviewIdea')
     , 1
     , 128692
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'AUTOMATION')
     , 'Monitoring the resin curing process'
     , 'early detection technology when the resin is sufficiently hardened'
     , 'optimization service to manufacturing companies that saves costs in resin production
'
     , 'manual checking of correct product parameters'
     , '- saves costs during manufacturing
- final resin has exactly the specified parameters'
     , ''
     , 4
     , 57
     , '2020-01-16 10:47:54'
     , NULL
     , NULL
     , '2020-01-16 10:47:54'
     , '2020-01-16 10:47:54'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT 'a863c755-c7eb-4d4f-a40e-5cb89e979821'
     , 1
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'completed')
     , 2
     , 128670
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'IOT')
     , 'Smart buoy'
     , 'Brand new smart buoy for position monitoring of anchoring boats'
     , 'smart anchoring device with GPS module for monitoring anchor movements'
     , ''
     , ''
     , ''
     , NULL
     , 70
     , '2020-01-16 16:03:42'
     , NULL
     , NULL
     , '2020-01-16 16:03:42'
     , '2020-01-16 16:07:12'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '091f97ad-687a-49af-b84e-95b6e114b98f'
     , 1
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'completed')
     , 2
     , 128688
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'IOT')
     , 'Moisture Guard'
     , 'MoistureGuard humidity sensors allow you to track the humidity directly in the building structure of the object. They also detect hidden small leaks of water, preventing fatal consequences. This unique system is developed by experts of the University Centre of Energy Efficient Buildings of CTU as a precaution against prolonged exposure to increased humidity.'
     , '- monitoring moisture in wooden constructions
- monitor the moisture inside the beams.'
     , '- increased moisture helps fungi and mold to grow, which can lead to serious health problems for the whole family.

- Some types of fungi can damage the mechanical properties of wood and permanently degrade it.

- Excessive moisture contributes to woody processes that damage a solid or structured wood structure.

- Excessive moisture or total flooding will leave a variety of secondary damage to plaster, electronics, interior fittings, etc.'
     , '- continual monitoring inside the structure

- humidity and temperature sensors

- immediate notification locally by alarm, via email or via a mobile app'
     , '- combined sensors for building into the structure,

- sensors to measure temperature and humidity in the interior and exterior,

- central unit for evaluating and collecting data. This stores the data in both local memory and remote server. As a result, the system is able to detect any deviation from the permitted values and alert in time to any potential problem with increased humidity. This has been the case since the very beginning of construction'
     , 9
     , 75
     , '2020-01-14 08:41:00'
     , NULL
     , NULL
     , '2020-01-14 08:41:00'
     , '2020-01-14 08:41:00'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT 'df402f94-6d7d-4666-8754-b0aae9f182ad'
     , 1
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'completed')
     , 2
     , 128688
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'IOT')
     , 'InoSens'
     , 'InoSens focuses mainly on sensory systems with a high degree of innovation and custom development of electronic systems and mechanics. We also provide consulting services on the use of IoT technologies or sensor systems.'
     , '- Complex design of electronic systems
- Developments in electronics from schema to platform design
- Developing new sensors to measure physical quantities
- Design embedded SW
- Design algorithms for data processing and presentation
- Mechanical element design and prototyping
- Use of IoT communication technologies'
     , ''
     , 'technology startup composed of CTU � UCEEB experts focused on sensory systems development and data processing
'
     , ''
     , 9
     , 71
     , '2020-01-14 08:48:12'
     , NULL
     , NULL
     , '2020-01-14 08:48:12'
     , '2020-01-14 08:52:16'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT 'a5c3ecf5-009c-4987-86c2-011c02f0e70c'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'reviewIdea')
     , 3
     , 128688
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'IOT')
     , 'Smart GLT'
     , 'Unique system that allows monitoring of mechanical stresses in the structure of wooden girders.'
     , '- serves as a precaution against damage to bonded wood support structures

- serves to overload roof structures of small gradient due to effects of accumulated snow or water

- warns of degradation of wood elements due to atmospheric and biological corrosion of wood
'
     , ''
     , '- allows long-term data storage and tracking through a web browser

- measure real-time loads

- can significantly reduce the cost of repairing the roof structure through early warning

- reduces the need for manual controls
'
     , ''
     , 6
     , 65
     , '2020-01-14 09:51:19'
     , NULL
     , NULL
     , '2020-01-14 09:51:19'
     , '2020-01-14 09:51:19'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '7b060b38-b2fa-4473-8604-529e931732a5'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'input')
     , 3
     , 128688
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'CIVIL ENGINEERING')
     , 'ClimaWindow'
     , 'ClimaWindow is a controlled environment creating suitable conditions for the growth of plants that interact with the surrounding environment in which they are placed.'
     , 'Window automatically allows modification of plant growing conditions, in particular ventilation, CO2 concentration, light, nutrient supply and humidity. Positioning in a construction structure (this solution has been gradually abandoned) or placing in an indoor environment as a solitary decorative and practical element (e.g. an element for interior subdivision) is an option.'
     , ''
     , '- Positive influence on environmental quality and aesthetics
- Enabling the cultivation of even more demanding plants in unsuitable environmental conditions or in highly sensitive environments (hospitals, schools, etc.)
- Easy system maintenance - need for service intervention once every 2 months (including watering), all controlled by an automatic sensory system
- Hydroponic system for continuity of nutrient supply'
     , ''
     , 4
     , 49
     , '2020-01-16 16:54:50'
     , NULL
     , NULL
     , '2020-01-16 16:54:50'
     , '2020-01-16 16:56:31'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '55b4c4ce-f0f8-44bc-9fd4-2e33497d31cd'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'input')
     , 3
     , 128688
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'CIVIL ENGINEERING')
     , 'Smart solar bench'
     , 'Technology is a smart device or object as part of the city''s mobiliary to improve the quality of public space'
     , 'This is an urban bench (possibly a bench intended for other outdoor environments)'
     , ''
     , '
- WiFi connectivity

- Contains solar panel for ecological recharging

- The material used is durable and durable recyclable concrete (up to 30 %)

- Aestheticism

- The specially treated surface prevents various  inscriptions from vandals

- Anchoring the basis to prevent theft

- '
     , ''
     , 4
     , 51
     , '2020-01-16 17:03:49'
     , NULL
     , NULL
     , '2020-01-16 17:03:49'
     , '2020-01-16 17:03:49'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '8205322b-453f-4704-99c2-ef20293bb4e4'
     , 2
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'input')
     , 3
     , 128688
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'HARDWARE')
     , 'Water flow sensor'
     , ''
     , 'The main use of the warhead is for water monitoring with the possibility of wireless data transmission using IoT, in particular more accurate determination of water consumption, status checks and planning of water network maintenance or predicating pricing policy according to current collection.'
     , ''
     , 'warhead compatibility with different types of water meters (of those commonly used, for example, Elster, Sensus and Intron). Moreover, simply by creating a new mounting adapter for the holder, it is possible to achieve easy use of the head to other types of water meters operating on the basis of a pulse-inducing sensing principle. Other advantages of the technological solution include a long battery life (12+ years) and wireless and continuous transmission of LoRaWAN long-range data, or a local LoRaWAN gateway to cover remote locations.
'
     , ''
     , 4
     , 51
     , '2020-01-16 17:10:05'
     , NULL
     , NULL
     , '2020-01-16 17:10:05'
     , '2020-01-16 17:10:05'
     , NULL
     , NULL
;
insert into commercialization (commercialization_uuid, -- as is
    -- reference data
                               commercialization_category_id, -- as is
                               commercialization_status_id, -- map
                               commercialization_priority_id, -- as is
                               intelectual_property_owner_id, -- as is
                               commercialization_investment_range_id, -- map
                               commercialization_domain_id, -- map
    -- content data
                               name,
                               executive_summary,
                               use_case,
                               pain_description,
                               competitive_advantage,
                               technical_principles,
                               technology_readines_level,
                               idea_score,
                               start_date, -- CREATED
                               end_date, -- always NULL
                               status_deadline,
    -- tracking
                               date_insert, -- CREATED
                               date_update, -- UPDATED
                               user_insert,
                               user_update)
SELECT '55d0022a-e534-48b9-baaa-69aaa743b851'
     , 1
     , (SELECT commercialization_status_id FROM commercialization_status_type WHERE code = 'draft')
     , 3
     , 128688
     , (SELECT commercialization_investment_range_id FROM commercialization_investment_range WHERE code = '3M+')
     , (SELECT commercialization_domain_id FROM commercialization_domain WHERE upper(code) = 'IOT')
     , 'IAQ sensors'
     , 'Platform for comprehensive evaluation of internal environment quality - hardware (devices) and software (evaluation) solutions'
     , 'evaluation of internal environment quality in schools and public places'
     , ''
     , 'The main functionalities are the monitoring of the following variables, which are part of one (combined) device:
�	temperature,
�	CO2 (carbon dioxide),
�	RH (relative humidity),
�	VOC (volatil organic compounds such as dust particles)
The advantage of a technical solution is the possibility of working with the parent system (e.g. driving VZT units). The communication interface is Modbus RTU and an optional wireless interface: IQRF MHz, LoRaWan, Sigfox. In addition to software evaluation, a big advantage is signalling alarms using simple RGB LEDs. These evaluations are based on pre-set limit values based on health standards'
     , ''
     , 4
     , 52
     , '2020-01-16 17:15:37'
     , NULL
     , NULL
     , '2020-01-16 17:15:37'
     , '2020-01-16 17:17:20'
     , NULL
     , NULL
;

alter table commercialization_project
    add column status_tmp bigint references commercialization_status_type;
insert into commercialization_project (commercialization_uuid, commercialization_category_id,
                                       commercialization_priority_id,
                                       intellectual_property_owner_id, commercialization_domain_id, name,
                                       executive_summary, use_case, pain_description, competitive_advantage,
                                       technical_principles, technology_readiness_level, start_date, end_date,
                                       status_deadline, commercialization_investment_from,
                                       commercialization_investment_to,
                                       idea_score, status_tmp) (select commercialization_uuid,
                                                                       commercialization_category_id,
                                                                       commercialization_priority_id,
                                                                       intelectual_property_owner_id,
                                                                       commercialization_domain_id,
                                                                       c.name,
                                                                       executive_summary,
                                                                       use_case,
                                                                       pain_description,
                                                                       competitive_advantage,
                                                                       technical_principles,
                                                                       technology_readines_level,
                                                                       start_date,
                                                                       end_date,
                                                                       status_deadline,
                                                                       cir.investment_from,
                                                                       cir.investment_to,
                                                                       idea_score,
                                                                       commercialization_status_id
                                                                from commercialization c
                                                                         left join commercialization_investment_range cir
                                                                                   on c.commercialization_investment_range_id =
                                                                                      cir.commercialization_investment_range_id);

insert into commercialization_project_status(commercialization_project_id, commercialization_status_type_id) (select commercialization_id, status_tmp
                                                                                                              from commercialization_project
                                                                                                              where status_tmp notnull);

alter table commercialization_project
    drop column status_tmp;
drop table commercialization;


---- Modified vermiculite
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '5f8fa7f8-64f5-43b6-a941-02b885e68731'), 330940, '2019-07-28 00:30:34',
        '2020-01-13 16:02:25');
---- Plant's acoustic signal measurement
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '65233d56-19d0-42b5-8036-0b2fa1fe2aee'), 332473, '2019-12-03 12:31:11',
        '2019-12-06 12:39:19');
---- Concrete with textile shreds
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '8f099ad2-b56f-477a-b5b8-69ab1c4c4acf'), 150172, '2019-12-03 10:16:18',
        '2019-12-06 12:39:19');
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '8f099ad2-b56f-477a-b5b8-69ab1c4c4acf'), 335044, '2019-12-03 10:16:18',
        '2019-12-06 12:39:19');
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '8f099ad2-b56f-477a-b5b8-69ab1c4c4acf'), 538756, '2019-12-03 10:16:18',
        '2019-12-06 12:39:19');
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '8f099ad2-b56f-477a-b5b8-69ab1c4c4acf'), 335393, '2019-12-03 10:16:18',
        '2019-12-06 12:39:19');
---- Defective Glass Detection
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '7e55043a-e163-40c4-9e91-9a1231391bfe'), 336530, '2019-08-21 13:36:47',
        '2020-01-13 16:04:04');
---- Environment Quality Sensor
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = 'a7385a35-c8c7-42bf-8638-1c9e4c59823b'), 337031, '2019-08-22 07:56:45',
        '2020-01-13 16:03:43');
---- BlueTooth Spot Tracker
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = 'b15dcd2a-83ea-44c0-86fd-660eccef8f44'), 326425, '2019-08-21 14:45:33',
        '2019-12-06 12:39:19');
---- G-NUT
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '67ed025b-33a3-4c36-b655-cbc1f1609c5e'), 6170, '2019-08-22 07:38:09',
        '2019-12-06 12:39:19');
---- Magneto-optic SPR biochips
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = 'edbf2ecc-bb92-43bf-b364-0ca87933f5fe'), 3220, '2019-08-22 08:43:31',
        '2019-12-06 12:39:19');
---- Porous fibrobeton from recyclate
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '7c1e499b-13b2-4e16-b8c0-2fc4352d10e2'), 150172, '2019-12-03 10:30:55',
        '2019-12-06 12:39:19');
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '7c1e499b-13b2-4e16-b8c0-2fc4352d10e2'), 238551, '2019-12-03 10:30:55',
        '2019-12-06 12:39:19');
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '7c1e499b-13b2-4e16-b8c0-2fc4352d10e2'), 335464, '2019-12-03 10:30:55',
        '2019-12-06 12:39:19');
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '7c1e499b-13b2-4e16-b8c0-2fc4352d10e2'), 150134, '2019-12-03 10:30:55',
        '2019-12-06 12:39:19');
---- System for automatic weld joints inspection
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '378ca03c-b2c8-45c7-b143-9cee602f6692'), 265589, '2019-07-28 00:33:52',
        '2019-12-06 12:39:19');
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '378ca03c-b2c8-45c7-b143-9cee602f6692'), 265588, '2019-07-28 00:33:52',
        '2019-12-06 12:39:19');
---- E-multicar
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = 'fc073185-66f2-4373-bf0d-6cf915dd9177'), 501291, '2019-12-03 13:13:34',
        '2019-12-06 12:39:19');
---- Three-point plough linkage
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '8eecf45c-26b8-49aa-9817-1066496b3891'), 501291, '2019-12-03 14:07:26',
        '2019-12-06 12:39:19');
---- Machine translation
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '93a68a87-b51f-455b-b892-f4bcedc5d498'), 261968, '2019-12-03 14:20:36',
        '2019-12-06 12:39:19');
---- Bio-fertilisers from slaughterhouse waste
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = 'a2824c29-526a-4a88-97b4-7270e4c466ff'), 5699, '2019-12-03 15:18:51',
        '2019-12-06 12:39:19');
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = 'a2824c29-526a-4a88-97b4-7270e4c466ff'), 337424, '2019-12-03 15:18:51',
        '2019-12-06 12:39:19');
---- Poultry gelatine
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '270d5665-02a3-4a19-ae25-ef55ba3f5132'), 5755, '2019-12-03 13:56:41',
        '2019-12-06 12:39:19');
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '270d5665-02a3-4a19-ae25-ef55ba3f5132'), 337152, '2019-12-03 13:56:41',
        '2019-12-06 12:39:19');
---- Smart bed sheet
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '1755ce87-26c4-481d-9aa9-54de446b7cbf'), 325139, '2019-12-03 14:35:46',
        '2020-01-06 15:28:43');
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '1755ce87-26c4-481d-9aa9-54de446b7cbf'), 325069, '2019-12-03 14:35:46',
        '2020-01-06 15:28:43');
---- Uniaxial Shear Tester
INSERT INTO commercialization_team_member(commercialization_id, user_profile_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = 'e1bff618-eda3-4b8c-89c7-3ea69f443129'), 159, '2019-12-03 15:05:53',
        '2019-12-06 12:39:19');
---- Bee feed
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '20613a0e-b9f6-4c2b-adf4-7fe4d1ab9562'), 984, '2019-12-09 09:43:48',
        '2019-12-10 09:48:04');
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '20613a0e-b9f6-4c2b-adf4-7fe4d1ab9562'), 4314, '2019-12-09 09:43:48',
        '2019-12-10 09:48:04');
---- Lyme borreliosis analytical method
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '454a99e6-be9a-4156-9ff6-e914f281661a'), 326064, '2019-12-09 10:02:39',
        '2019-12-09 10:06:01');
---- Tick breeding facility
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '23dc8b23-8c6a-4ba4-b160-1332a03486e8'), 1054, '2019-12-09 10:52:01',
        '2019-12-09 10:52:20');
---- Portable UV spectrometer
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '31dd4f81-ded2-467c-980a-af88eb5c7382'), 8433, '2019-12-03 12:11:13',
        '2020-01-13 16:04:40');
---- Baby Shoe Size Prediction
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '19aae17c-1ba4-4cd5-9fbd-238d75d29d2b'), 5746, '2019-12-03 12:51:52',
        '2020-01-13 16:04:30');
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '19aae17c-1ba4-4cd5-9fbd-238d75d29d2b'), 337226, '2019-12-03 12:51:52',
        '2020-01-13 16:04:30');
---- ANALUM
INSERT INTO commercialization_team_member(commercialization_id, user_profile_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '9eccff24-0f3a-46d7-ba83-893453cc7f3a'), 269, '2020-01-14 15:24:46',
        '2020-01-14 15:25:14');
---- Wireless Power Transfer in Rotary Machines
INSERT INTO commercialization_team_member(commercialization_id, user_profile_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '3f2bf888-217d-4b48-8c17-b8cdcdf007a4'), 261, '2020-01-14 15:36:38',
        '2020-01-14 15:36:38');
---- Entrant
INSERT INTO commercialization_team_member(commercialization_id, user_profile_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '8b9f67dd-2aea-4990-8c4d-284fff30e21f'), 235, '2020-01-14 15:47:19',
        '2020-01-16 10:07:41');
---- Chalcogenide glasses
INSERT INTO commercialization_team_member(commercialization_id, user_profile_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = 'dfd170fe-6b40-44d2-acf6-144bb8a601ac'), 264, '2020-01-14 16:00:51',
        '2020-01-14 16:00:51');
---- Hybrid photovoltaic panel
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = 'b7f92194-c9d8-4a7f-b3ed-dc9764cca359'), 335871, '2020-01-14 16:07:55',
        '2020-01-14 16:07:55');
---- HeRo
INSERT INTO commercialization_team_member(commercialization_id, expert_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '7f5b82f6-ff39-448a-87ac-e46ad56e63bf'), 503218, '2020-01-16 09:59:24',
        '2020-01-16 09:59:25');
---- Molecular permeability methodology
INSERT INTO commercialization_team_member(commercialization_id, user_profile_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = 'b77c357e-a5a0-4eab-acbe-dc794b140c5a'), 263, '2020-01-16 10:07:06',
        '2020-01-26 20:33:13');
---- Aqua Chip
INSERT INTO commercialization_team_member(commercialization_id, user_profile_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '6659fd11-5a3e-4883-a084-ddf000cd465f'), 283, '2020-01-16 10:21:26',
        '2020-01-16 10:21:26');
---- Naviterier
---- Monitoring the resin curing process
INSERT INTO commercialization_team_member(commercialization_id, user_profile_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '04d8d652-a29e-4e94-906f-d315a97c19f5'), 266, '2020-01-16 10:47:54',
        '2020-01-16 10:47:54');
---- Smart buoy
INSERT INTO commercialization_team_member(commercialization_id, user_profile_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = 'a863c755-c7eb-4d4f-a40e-5cb89e979821'), 231, '2020-01-16 16:03:42',
        '2020-01-16 16:07:12');
---- Moisture Guard
INSERT INTO commercialization_team_member(commercialization_id, user_profile_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = '091f97ad-687a-49af-b84e-95b6e114b98f'), 231, '2020-01-14 08:41:00',
        '2020-01-26 20:33:29');
---- InoSens
INSERT INTO commercialization_team_member(commercialization_id, user_profile_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = 'df402f94-6d7d-4666-8754-b0aae9f182ad'), 231, '2020-01-14 08:48:12',
        '2020-01-26 20:33:47');
---- Smart GLT
INSERT INTO commercialization_team_member(commercialization_id, user_profile_id, date_insert, date_update)
VALUES ((select commercialization_id
         from commercialization_project
         where commercialization_uuid = 'a5c3ecf5-009c-4987-86c2-011c02f0e70c'), 231, '2020-01-14 09:51:19',
        '2020-01-14 09:51:19');
---- ClimaWindow
---- Smart solar bench
---- Water flow sensor
---- IAQ sensors

update commercialization_team_member ctm
set user_profile_id=ue.user_id
from (select user_id, expert_id from user_expert) as ue
where user_profile_id isnull
  and ctm.expert_id = ue.expert_id;