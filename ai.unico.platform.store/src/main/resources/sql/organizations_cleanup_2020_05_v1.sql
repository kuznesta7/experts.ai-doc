UPDATE "country"
SET "country_abbrev" = 'US',
    "published"      = true
WHERE "country_code" = 'us';
INSERT INTO "country" ("country_code", "country_name", "country_abbrev", "published")
VALUES ('be', 'Belgium', 'BE', true);
INSERT INTO "country" ("country_code", "country_name", "country_abbrev", "published")
VALUES ('es', 'Spain', 'ES', true);
INSERT INTO "country" ("country_code", "country_name", "country_abbrev", "published")
VALUES ('lu', 'Luxembourg', 'LU', true);
INSERT INTO "country" ("country_code", "country_name", "country_abbrev", "published")
VALUES ('nl', 'Netherlands', 'NL', true);
INSERT INTO "country" ("country_code", "country_name", "country_abbrev", "published")
VALUES ('ie', 'Ireland', 'IE', true);
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Gesellschaft für Schwerionenforschung mbH Planckstrasse 1 64291 Darmstadt',
    organization_country_code  = 'de',
    substitute_organization_id = 158670
where original_organization_id = 232097;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'EBN - European Business Innovation Centre Network',
    organization_country_code  = 'be',
    substitute_organization_id = null
where original_organization_id = 222017;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'K4I -Knowledge for Innovation',
    organization_country_code  = 'be',
    substitute_organization_id = null
where original_organization_id = 221084;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Minerva Česká republika, a.s.',
    organization_country_code  = 'cz',
    substitute_organization_id = null
where original_organization_id = 224758;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'European Space Agency - ESTEC',
    organization_country_code  = 'nl',
    substitute_organization_id = null
where original_organization_id = 222193;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'International Association for the Study of Pain',
    organization_country_code  = 'sk',
    substitute_organization_id = 175354
where original_organization_id = 221460;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MTU AERO ENGINES GMBH  Dachaeur Strasse 665, 80995 München',
    organization_country_code  = 'de',
    substitute_organization_id = 156156
where original_organization_id = 239125;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOMBARDIER TRANSPORTAITON GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = 155870
where original_organization_id = 227613;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOMBARDIER PRIMOVE GMBH Neustadter Str. 62 68309 Mannheim',
    organization_country_code  = 'de',
    substitute_organization_id = 156020
where original_organization_id = 227604;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SMA Technologie AG',
    organization_country_code  = 'de',
    substitute_organization_id = 155938
where original_organization_id = 245198;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TRW AUTOMOTIVE GMBH Industriestraße 20 73553 Alfdorf',
    organization_country_code  = 'de',
    substitute_organization_id = 155945
where original_organization_id = 247389;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DIEHL AKO STIFTUNG & CO. KG Pfannerstr. 75 - 83 88239 Wangen',
    organization_country_code  = 'de',
    substitute_organization_id = 156004
where original_organization_id = 229883;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Max-Planck-Gesellschaft zur Förderung der Wissenschaften e.V.',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 238199;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MAX-PLANCK GESELLSCHAFT ZUR FOERDERUNG DER WISSENSCHAFTEN E.V. Hofgartenstrasse 8 80539',
    organization_country_code  = 'de',
    substitute_organization_id = 157763
where original_organization_id = 238178;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MAX-PLANCK-GESELLSCHAFT ZUR FÖRDERUNG DER WISSENSCHAFTEN E.V.  Hofgartenstr. 2, 80539 München',
    organization_country_code  = 'de',
    substitute_organization_id = 157763
where original_organization_id = 238196;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'VOLKSWAGEN AKTIENGESELLSCHAFT Berliner Ring 2',
    organization_country_code  = 'de',
    substitute_organization_id = 157039
where original_organization_id = 248649;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Eto Magnetic GmbH Hardtring 8',
    organization_country_code  = 'de',
    substitute_organization_id = 156498
where original_organization_id = 230927;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ABB PATENT GMBH  [DE/ DE] Wallstadter Strasse 59, 68526 Ladenburg',
    organization_country_code  = 'de',
    substitute_organization_id = 156421
where original_organization_id = 225083;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ABB AG Kallstadter Str. 1',
    organization_country_code  = 'de',
    substitute_organization_id = 156421
where original_organization_id = 225059;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TENNECO GMBH Luitpoldstraße 83 67480 Edenkoben',
    organization_country_code  = 'de',
    substitute_organization_id = 156595
where original_organization_id = 246822;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Nokia Siemens Networks GmbH & Co. KG',
    organization_country_code  = 'de',
    substitute_organization_id = 156616
where original_organization_id = 239736;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Nokia Siemens Networks GmbH & Co. KG  Otto-Hahn-Ring 6, D-80506 Munchen',
    organization_country_code  = 'de',
    substitute_organization_id = 156616
where original_organization_id = 239737;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ROCHE DIAGNOSTICS GMBH Sandhofer Strasse 116 68305 Mannheim',
    organization_country_code  = 'de',
    substitute_organization_id = 178476
where original_organization_id = 243895;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Roche Diagnostics GmbH 68298 Mannheim',
    organization_country_code  = 'de',
    substitute_organization_id = 178476
where original_organization_id = 243893;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FRAUNHOFER-GESELLSCHAFT ZUR FÖRDERUNG DER ANGWANDTEN FORSCHUNG E.V. Hansastraße 27c 80686 München',
    organization_country_code  = 'de',
    substitute_organization_id = 156506
where original_organization_id = 231604;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FRAUNHOFER-GESELLSCHAFT ZUR FORDERUNG DER ANGEWANDTEN FORSCHUNG E V HANSASTRASSE 27C, D-80686 MUNCHEN',
    organization_country_code  = 'de',
    substitute_organization_id = 156506
where original_organization_id = 231593;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FRAUNHOFER-GESELLSCHAFT ZUR FORDERUNG DER ANGEWANDTEN FORSCHUNG E.V. HANSASTRASSE 27C 80686',
    organization_country_code  = 'de',
    substitute_organization_id = 156506
where original_organization_id = 231595;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BASF Aktiengesellschaft',
    organization_country_code  = 'de',
    substitute_organization_id = 157250
where original_organization_id = 226840;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BASF SE 67056 Ludwigshafen',
    organization_country_code  = 'de',
    substitute_organization_id = 157250
where original_organization_id = 226856;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BASF SE Carl-Bosch-Str. 38 67056 Ludwigshafen am Rhein',
    organization_country_code  = 'de',
    substitute_organization_id = 157250
where original_organization_id = 226859;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BASF PLANT SCIENCE GMBH 67056',
    organization_country_code  = 'de',
    substitute_organization_id = 156620
where original_organization_id = 226847;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BASF COATINGS GMBH Glasuritstraße 1 48165 Münster',
    organization_country_code  = 'de',
    substitute_organization_id = 161080
where original_organization_id = 226844;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'RÜTGERS CHEMICALS AG  [DE/ DE] Kekuléstrasse 30, 44579 Castrop-Rauxel',
    organization_country_code  = 'de',
    substitute_organization_id = 158616
where original_organization_id = 244198;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Rütgers Kureha Solvents GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 244200;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Rütgers Kureha Solvents GmbH Varziner Strasse 49 47138 Duisburg',
    organization_country_code  = 'de',
    substitute_organization_id = 159785
where original_organization_id = 244201;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'RUTGERS KUREHA SOLVENTS GMBH VARZINER STR. 49,47138 DUISBURG',
    organization_country_code  = 'de',
    substitute_organization_id = 159785
where original_organization_id = 244114;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DOOSAN LENTJES GMBH Daniel-Goldbach-Straße 19 40880 Ratingen',
    organization_country_code  = 'de',
    substitute_organization_id = 157233
where original_organization_id = 230049;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DOOSAN LENTJES GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = 157233
where original_organization_id = 230046;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FORSCHUNGSZENTRUM JÜLICH GMBH Wilhelm-Johnen-Strasse 52425 Jülich',
    organization_country_code  = 'de',
    substitute_organization_id = 156722
where original_organization_id = 231519;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Forschungszentrum Jülich GmbH 52425 Jülich',
    organization_country_code  = 'de',
    substitute_organization_id = 156722
where original_organization_id = 231514;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FORSCHUNGSZENTRUM JÜLICH GMBH [DE / DE]  Willhem-Johnen-Strasse D-52425 Jülich',
    organization_country_code  = 'de',
    substitute_organization_id = 156722
where original_organization_id = 231517;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FORSCHUNGSZENTRUM BORSTEL Parkalle 1-40 23845',
    organization_country_code  = 'de',
    substitute_organization_id = 160472
where original_organization_id = 231506;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FORSCHUNGSZENTRUM KARLSRUHE GMBH  Weberstrasse 5, 76133 Karlsruhe',
    organization_country_code  = 'de',
    substitute_organization_id = 159366
where original_organization_id = 231522;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'EVONIK DEGUSSA GMBH  Rellinghauser Strasse 1-11, 45128 Essen',
    organization_country_code  = 'de',
    substitute_organization_id = 156826
where original_organization_id = 230985;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Evonik Degussa GmbH Rellinghauser Strasse 1-11',
    organization_country_code  = 'de',
    substitute_organization_id = 156826
where original_organization_id = 230987;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SCHLUTER, HARTWIG Trankegasse 1 34260',
    organization_country_code  = 'de',
    substitute_organization_id = 158580
where original_organization_id = 244532;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Mahle International GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = 157356
where original_organization_id = 237761;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MAHLE INTERNATIONAL GMBH PRAGSTR. 26-46',
    organization_country_code  = 'de',
    substitute_organization_id = 157356
where original_organization_id = 237762;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MAHLE INT GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = 157356
where original_organization_id = 237760;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MAHLE INTERNATIONAL GMBH Pragstraße 26-46 70376 Stuttgart',
    organization_country_code  = 'de',
    substitute_organization_id = 157356
where original_organization_id = 237765;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BAYERISCHE MOTOREN WERKE AKTIENGESELLSCHAFT Petuelring 130 80809 München',
    organization_country_code  = 'de',
    substitute_organization_id = 157097
where original_organization_id = 226907;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Bayerische Motoren Werke Aktiengesellschaft Petuelring 130',
    organization_country_code  = 'de',
    substitute_organization_id = 157097
where original_organization_id = 226906;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BAYERISCHE MOTOREN WERKE AG',
    organization_country_code  = 'de',
    substitute_organization_id = 157097
where original_organization_id = 226904;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Recaro Aircraft Seating GmbH & Co. KG Daimlerstrasse 21',
    organization_country_code  = 'de',
    substitute_organization_id = 169605
where original_organization_id = 243698;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ROBERT BOSCH GMBH WERNERSTRASSE 1',
    organization_country_code  = 'de',
    substitute_organization_id = 157022
where original_organization_id = 243876;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ROBERT BOSCH GMBH [DE / DE]  Postfach 30 02 20 D-70442 Stuttgart',
    organization_country_code  = 'de',
    substitute_organization_id = 157022
where original_organization_id = 243873;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Robert Bosch GmbH Postfach 30 02 20',
    organization_country_code  = 'de',
    substitute_organization_id = 157022
where original_organization_id = 243874;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ARMACELL ENTERPRISE GMBH Robert-Bosch-Str. 10 48153',
    organization_country_code  = 'de',
    substitute_organization_id = 157883
where original_organization_id = 226244;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'KG Armacell Enterprise GmbH & Co Zeppelinstrasse 1 Schonefeld 12529 OT  Waltersdorf',
    organization_country_code  = 'de',
    substitute_organization_id = 157883
where original_organization_id = 235788;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Westfälische Wilhelms-Universität Münster',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 249058;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WESTFÄLISCHE WILHELMS-UNIVERSITÄT MÜNSTER Domagstrasse 5 48149 Münster',
    organization_country_code  = 'de',
    substitute_organization_id = 178633
where original_organization_id = 249057;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HeidelbergCement AG Berliner Straße 6 69120 Heidelberg',
    organization_country_code  = 'de',
    substitute_organization_id = 157247
where original_organization_id = 232889;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WILHELM HERM. MÜLLER GMBH & CO KG Postkamp 14',
    organization_country_code  = 'de',
    substitute_organization_id = 157609
where original_organization_id = 249180;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WILHELM HERM. MÜLLER GMBH & CO KG Postkamp 14 D-30159 Hannover',
    organization_country_code  = 'de',
    substitute_organization_id = 157609
where original_organization_id = 249181;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Roto Frank AG',
    organization_country_code  = 'de',
    substitute_organization_id = 166065
where original_organization_id = 244045;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ROTO FRANK AG  Stuttgarter Strasse 145-149, 70771 Leinfelden-Echterdingen',
    organization_country_code  = 'de',
    substitute_organization_id = 166065
where original_organization_id = 244042;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Roto Frank DST Marken GmbH & Co. KG Wilhelm-Frank-Str. 38-40',
    organization_country_code  = 'de',
    substitute_organization_id = 166065
where original_organization_id = 244047;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOSCH & SIEMENS HOME APPLIANCES ; BSH',
    organization_country_code  = 'de',
    substitute_organization_id = 155913
where original_organization_id = 227718;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Siemens Networks GmbH & Co. KG St. Martin-Str. 76',
    organization_country_code  = 'de',
    substitute_organization_id = 156616
where original_organization_id = 244897;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Aircloak GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 225597;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Umicore AG & Co. KG Rodenbacher Chaussee 4',
    organization_country_code  = 'de',
    substitute_organization_id = 159206
where original_organization_id = 247624;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UMICORE AG & CO. KG  Rodenbacher Chaussee 4, 63457 Hanau',
    organization_country_code  = 'de',
    substitute_organization_id = 159206
where original_organization_id = 247622;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UMICORE AG & CO. KG Rodenbacher Chaussee 4 63457',
    organization_country_code  = 'de',
    substitute_organization_id = 159206
where original_organization_id = 247625;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UMICORE AG & CO. KG Rodenbacher Chaussee 4 63457 Hanau-Wolfgang',
    organization_country_code  = 'de',
    substitute_organization_id = 159206
where original_organization_id = 247626;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BSH Hausgeräte GmbH Carl-Wery-Strasse 34',
    organization_country_code  = 'de',
    substitute_organization_id = 155913
where original_organization_id = 227901;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BSH Bosch und Siemens Hausgeräte GmbH Carl-Wery-Strasse 34',
    organization_country_code  = 'de',
    substitute_organization_id = 155913
where original_organization_id = 227897;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BSH BOSCH UND SIEMENS HAUSGERÄTE GMBH Carl-Wery-Str. 34 81739 München',
    organization_country_code  = 'de',
    substitute_organization_id = 155913
where original_organization_id = 227895;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BSH HAUSGERÄTE GMBH Carl-Wery-Str. 34 81739 München',
    organization_country_code  = 'de',
    substitute_organization_id = 155913
where original_organization_id = 227900;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BSH Bosch und Siemens Hausgeräte GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = 155913
where original_organization_id = 227893;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BSH Bosch und Siemens Hausgeräte GmbH Carl-Wery-Strasse 34 81739 München',
    organization_country_code  = 'de',
    substitute_organization_id = 155913
where original_organization_id = 227898;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Hvězdárna Valašské Meziříčí',
    organization_country_code  = 'cz',
    substitute_organization_id = null
where original_organization_id = 12147;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'The International Association for the Study of Pain',
    organization_country_code  = 'us',
    substitute_organization_id = null
where original_organization_id = 220836;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MTU Aero Engines AG Dachauer Strasse 665',
    organization_country_code  = 'de',
    substitute_organization_id = 156156
where original_organization_id = 239123;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MTU Aero Engines GmbH Dachauer Strasse 665',
    organization_country_code  = 'de',
    substitute_organization_id = 156156
where original_organization_id = 239129;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MTU AERO ENGINES GMBH Dachauer Strasse 665 80995 München',
    organization_country_code  = 'de',
    substitute_organization_id = 156156
where original_organization_id = 239131;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MTU AERO ENGINES GMBH Dachauer Strasse 665 80995',
    organization_country_code  = 'de',
    substitute_organization_id = 156156
where original_organization_id = 239130;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MTU AERO ENGINES GMBH  Dachauer Strasse 665, 80955 München',
    organization_country_code  = 'de',
    substitute_organization_id = 156156
where original_organization_id = 239126;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SITECH SITZTECHNIK GMBH Stellfelder Str. 46 38442 Wolfsburg',
    organization_country_code  = 'de',
    substitute_organization_id = 155751
where original_organization_id = 245005;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Sitech Sitztechnik GmbH Stellfelder Strasse 46',
    organization_country_code  = 'de',
    substitute_organization_id = 155751
where original_organization_id = 245006;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Sitech Sitztechnik Stellfelder Str. 46',
    organization_country_code  = 'de',
    substitute_organization_id = 155751
where original_organization_id = 245007;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Bombardier Transportation GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 227626;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOMBARDIER TRANSPORTATION GMBH SCHONEBERGER UFER 1 10785',
    organization_country_code  = 'de',
    substitute_organization_id = 155870
where original_organization_id = 227625;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOMBARDIER TRANSPORTATION GMBH Schoeneberger Ufer 1 10785',
    organization_country_code  = 'de',
    substitute_organization_id = 155870
where original_organization_id = 227624;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOMBARDIER TRANSPORTATION GMBH POSTFACH 100135 16748',
    organization_country_code  = 'de',
    substitute_organization_id = 155870
where original_organization_id = 227623;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Bombardier Primove GmbH Eichhornstrasse 3',
    organization_country_code  = 'de',
    substitute_organization_id = 156020
where original_organization_id = 227600;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOMBARDIER PRIMOVE GMBH EICHHORNSTRASSE 3 10785',
    organization_country_code  = 'de',
    substitute_organization_id = 156020
where original_organization_id = 227601;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOMBARDIER PRIMOVE GMBH SCHONEBERGER UFER 1 10785',
    organization_country_code  = 'de',
    substitute_organization_id = 156020
where original_organization_id = 227606;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Bombardier Primove GmbH Neustadter Str. 62',
    organization_country_code  = 'de',
    substitute_organization_id = 156020
where original_organization_id = 227603;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SIEMENS AKTIENGESELLSCHAFT  Wittelsbacherplatz 2, 80333 München',
    organization_country_code  = 'de',
    substitute_organization_id = 158261
where original_organization_id = 244889;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Siemens Aktiengesellschaft Wittelsbacherplatz 2',
    organization_country_code  = 'de',
    substitute_organization_id = 158261
where original_organization_id = 244891;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Siemens Aktiengesellschaft Werner-von-Siemens-Straße 1',
    organization_country_code  = 'de',
    substitute_organization_id = 158261
where original_organization_id = 244890;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Siemens Aktiengesellschaft',
    organization_country_code  = 'de',
    substitute_organization_id = 158261
where original_organization_id = 244887;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SIEMENS AKTIENGESELLSCHAFT  [DE/ DE] Wittelsbacherplatz 2, 80333 München',
    organization_country_code  = 'de',
    substitute_organization_id = 158261
where original_organization_id = 244888;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SMA Solar Technology AG Sonnenallee 1',
    organization_country_code  = 'de',
    substitute_organization_id = 155938
where original_organization_id = 245194;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SMA SOLAR TECHNOLOGY AG Sonnenallee 1 34266 Sonnenallee 1',
    organization_country_code  = 'de',
    substitute_organization_id = 155938
where original_organization_id = 245196;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TRW AUTOMOTIVE GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = 155945
where original_organization_id = 247383;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TRW AUTOMOTIVE GMBH ; TRW',
    organization_country_code  = 'de',
    substitute_organization_id = 155945
where original_organization_id = 247385;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TRW AUTOMOTIVE ELECTRONICS & COMPONENTS GMBH Industriestrasse 2-8 78315 Radolfzell',
    organization_country_code  = 'de',
    substitute_organization_id = 156873
where original_organization_id = 247381;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TRW AUTOMOTIVE ELECT & COMPONENTS GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = 156873
where original_organization_id = 247378;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TRW AUTOMOTIVE ELECT & COMPONENTS GMBH ; BCS',
    organization_country_code  = 'de',
    substitute_organization_id = 156873
where original_organization_id = 247379;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TRW Automotive Safety Systems GmbH Hefner-Alteneck-Strasse 11',
    organization_country_code  = 'de',
    substitute_organization_id = 160358
where original_organization_id = 247392;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TRW Automotive Safety Systems GmbH & Co. KG',
    organization_country_code  = 'de',
    substitute_organization_id = 160358
where original_organization_id = 247391;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TRW Automotive Safety Systems GmbH Hefner-Alteneck-Strasse 11 63743 Aschaffenburg',
    organization_country_code  = 'de',
    substitute_organization_id = 160358
where original_organization_id = 247393;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Diehl AKO Stiftung & Co. KG Pfannerstrasse 75',
    organization_country_code  = 'de',
    substitute_organization_id = 156004
where original_organization_id = 229885;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DIEHL AKO STIFTLING & CO. KG',
    organization_country_code  = 'de',
    substitute_organization_id = 156004
where original_organization_id = 229881;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Diehl AKO Stiftung & Co. KG Pfannerstrasse 75 88239 Wangen',
    organization_country_code  = 'de',
    substitute_organization_id = 156004
where original_organization_id = 229886;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MAX PLANCK GESELLSCHAFT ZUR FÖRDERUNG DER WISSENSCHAFTEN E.V.',
    organization_country_code  = 'de',
    substitute_organization_id = 157763
where original_organization_id = 238174;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MAX-PLANCK-GESELLSCHAFT ZUR FÖRDERUNG DER WISSENSCHAFTEN E.V.  [DE/ DE] Hofgartenstr. 8, 80539 München',
    organization_country_code  = 'de',
    substitute_organization_id = 157763
where original_organization_id = 238195;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Max Planck Gesellschaft Zur Foerderung der Wissenschaften E.V.',
    organization_country_code  = 'de',
    substitute_organization_id = 157763
where original_organization_id = 238173;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Max-Planck-Gesellschaft Zur Forderund Der Wissenschaften E.V.',
    organization_country_code  = 'de',
    substitute_organization_id = 157763
where original_organization_id = 238184;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Max-Planck-Gesellschaft zur Förderung der Wissenschaften eV',
    organization_country_code  = 'de',
    substitute_organization_id = 157763
where original_organization_id = 238201;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Max-Planck Gesellschaft Zur Forderlung Der Wissenschaften',
    organization_country_code  = 'de',
    substitute_organization_id = 157763
where original_organization_id = 238179;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MAX-PLANCK GESELLSCHAFT ZUR FÖRDERUNG DER WISSENSCHAFTEN Hofgartenstrasse 8 80539 Munich',
    organization_country_code  = 'de',
    substitute_organization_id = 157763
where original_organization_id = 238181;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MAX-PLANCK-GESELLSCHAFT ZUR FORDERUNG DER WSS E V',
    organization_country_code  = 'de',
    substitute_organization_id = 157763
where original_organization_id = 238189;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Max-Planck-Gesellschaft zur Förderung der Wissenschaften e.V. Hofgartenstrasse 8 80539 München',
    organization_country_code  = 'de',
    substitute_organization_id = 157763
where original_organization_id = 238198;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MAX-PLANCK-GESELLSCHAFT ZURFÖRDERUNG DER WISSENSCHAFTEN E.V. Hofgartenstrasse 8 80539 München',
    organization_country_code  = 'de',
    substitute_organization_id = 157763
where original_organization_id = 238202;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Max-Planck-Gesellschaft zur Förderung der  Wissenschaften E.V. Hofgartenstrasse, 2',
    organization_country_code  = 'de',
    substitute_organization_id = 157763
where original_organization_id = 238193;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MAX-PLANCK-GESELLSCHAFT ZUR FORDERUNG DER WISSENSCHAFTEN E V HOFGARTENSTRASSE 8, D-80539, MUNICH',
    organization_country_code  = 'de',
    substitute_organization_id = 157763
where original_organization_id = 238187;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Faurecia Autositze GmbH Nordsehler Strasse 38',
    organization_country_code  = 'de',
    substitute_organization_id = 156079
where original_organization_id = 231188;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Faurecia Autosize GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = 156079
where original_organization_id = 231189;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SGL CARBON SE Rheingaustr. 182 65203',
    organization_country_code  = 'de',
    substitute_organization_id = 156113
where original_organization_id = 244809;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SGL Carbon SE Rheingaustrasse 182',
    organization_country_code  = 'de',
    substitute_organization_id = 156113
where original_organization_id = 244811;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SGL CARBON SE Rheingaustr. 182 65203 Wiesbaden',
    organization_country_code  = 'de',
    substitute_organization_id = 156113
where original_organization_id = 244810;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WABCO GMBH & CO. OHG Am Lindener Hafen 21 30453 Hannover',
    organization_country_code  = 'de',
    substitute_organization_id = 156261
where original_organization_id = 248732;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'VOLKSWAGEN AKTIENGESELLSCHAFT Brieffach 1770 38436 Wolfsburg',
    organization_country_code  = 'de',
    substitute_organization_id = 157039
where original_organization_id = 248651;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Roche Diabetes Care GmbH Sandhofer Strasse 116',
    organization_country_code  = 'de',
    substitute_organization_id = 178476
where original_organization_id = 243891;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Fraunhofer-Gesellschaft zur Förderung der  angewandten Forschung e.V. Hansastrasse 27c',
    organization_country_code  = 'de',
    substitute_organization_id = 156506
where original_organization_id = 231596;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FRAUNHOFER-GESELLSCHAFT ZUR FÖRDERUNG DER ANGEWANDTEN FORSCHUNG E.V. Hansastraße 27c 80686 München',
    organization_country_code  = 'de',
    substitute_organization_id = 156506
where original_organization_id = 231603;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Fraunhofer Gesellschaft zur Förderung der angewandten Forschung e.V. Hansastrasse 27 c',
    organization_country_code  = 'de',
    substitute_organization_id = 156506
where original_organization_id = 231590;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BASF SE 67063 Ludwigshafen',
    organization_country_code  = 'de',
    substitute_organization_id = 157250
where original_organization_id = 226858;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BASF AG',
    organization_country_code  = 'de',
    substitute_organization_id = 157250
where original_organization_id = 226839;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BASF PLANT SCIENCE GMBH  [DE/ DE] 67056 Ludwigshafen',
    organization_country_code  = 'de',
    substitute_organization_id = 156620
where original_organization_id = 226846;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BASF Plant Science GmbH Carl-Bosch-Strasse 38 67056 Ludwigshafen',
    organization_country_code  = 'de',
    substitute_organization_id = 156620
where original_organization_id = 226852;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BASF PLANT SCIENCE GMBH D-67056 Ludwigshafen',
    organization_country_code  = 'de',
    substitute_organization_id = 156620
where original_organization_id = 226853;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BASF Plant Science GmbH Carl-Bosch-Str-38',
    organization_country_code  = 'de',
    substitute_organization_id = 156620
where original_organization_id = 226851;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOMBARDIER TRANSPORTATION GMBH Schöneberger Ufer 1 10785 Berlin',
    organization_country_code  = 'de',
    substitute_organization_id = 155870
where original_organization_id = 227627;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Bombardier Primove GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 227607;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SGL Carbon SE Söhnleinstrasse 8',
    organization_country_code  = 'de',
    substitute_organization_id = 156113
where original_organization_id = 244817;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'VOLKSWAGEN AKTIENGESELLSCHAFT',
    organization_country_code  = 'de',
    substitute_organization_id = 157039
where original_organization_id = 248647;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SGL CARBON SE Söhnleinstr. 8 65201 Wiesbaden',
    organization_country_code  = 'de',
    substitute_organization_id = 156113
where original_organization_id = 244816;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ETO MAGNETIC GMBH Hardtring 8 78333 Stockach',
    organization_country_code  = 'de',
    substitute_organization_id = 156498
where original_organization_id = 230928;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ABB AG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 225081;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WABCO GmbH Am Lindener Hafen 21',
    organization_country_code  = 'de',
    substitute_organization_id = 156261
where original_organization_id = 248733;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Schlüter, Hartwig Tränkegasse 1',
    organization_country_code  = 'de',
    substitute_organization_id = 158580
where original_organization_id = 244536;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Doosan Lentjes GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 230047;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HeidelbergCement AG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 232888;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WESTFÄLISCHE WILHELMS-UNIVERSITÄT MÜNSTER Schlossplatz 2 48149 Münster',
    organization_country_code  = 'de',
    substitute_organization_id = 178633
where original_organization_id = 249059;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WABCO GMBH Am Lindener Hafen 21 30453 Hannover',
    organization_country_code  = 'de',
    substitute_organization_id = 156261
where original_organization_id = 248734;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SCHLÜTER, Hartwig Tränkegasse 1 34260 Kaufungen',
    organization_country_code  = 'de',
    substitute_organization_id = 158580
where original_organization_id = 244537;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Airbus Defence and Space GmbH Willy-Messerschmitt-Straße 1',
    organization_country_code  = 'de',
    substitute_organization_id = 161257
where original_organization_id = 225590;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Armacell Enterprise GmbH Robert-Bosch-Str. 10',
    organization_country_code  = 'de',
    substitute_organization_id = 157883
where original_organization_id = 226243;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ABB PATENT GMBH  [DE/ DE] Wallstadter Str. 59, 68526 Ladenburg',
    organization_country_code  = 'de',
    substitute_organization_id = 156421
where original_organization_id = 225082;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Forschungszentrum Karlsruhe GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 231523;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Spartherm Feuerungstechnik GmbH Maschweg 38',
    organization_country_code  = 'de',
    substitute_organization_id = 156925
where original_organization_id = 245474;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FORSCHUNGSZENTRUM JÜLICH GMBH [DE / DE]  Wilhelm-Johnen-Strasse D-52425 Jülich',
    organization_country_code  = 'de',
    substitute_organization_id = 156722
where original_organization_id = 231516;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Airbus Operations GmbH Kreetslag 10',
    organization_country_code  = 'de',
    substitute_organization_id = 172697
where original_organization_id = 225593;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ABB PATENT GmbH Wallstadter Strasse 59',
    organization_country_code  = 'de',
    substitute_organization_id = 156421
where original_organization_id = 225084;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FORSCHUNGSZENTRUM JÜLICH GMBH Wilhelm-Johnen-Strasse D-52425 Jülich',
    organization_country_code  = 'de',
    substitute_organization_id = 156722
where original_organization_id = 231520;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ROTO FRANK Aktiengesellschaft',
    organization_country_code  = 'de',
    substitute_organization_id = 166065
where original_organization_id = 244046;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'NOKIA SIEMENS NETWORKS GMBH & CO. KG St. Martin Str. 76 81541 Munich',
    organization_country_code  = 'de',
    substitute_organization_id = 156616
where original_organization_id = 239739;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Aug. Winkhaus GmbH & Co. KG August-Winkhaus-Strasse 31',
    organization_country_code  = 'de',
    substitute_organization_id = 158165
where original_organization_id = 226410;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MITTELDEUTSCHE TRESORBAU GMBH Marktplatz 1 06108 Halle',
    organization_country_code  = 'de',
    substitute_organization_id = 157834
where original_organization_id = 238854;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MERCK PATENT GMBH FRANKFURTER STRASSE 250, D-64293 DARMSTADT',
    organization_country_code  = 'de',
    substitute_organization_id = 159625
where original_organization_id = 238457;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MERCK PATENT GMBH [DE / DE]  Frankfurter Strasse 250 D-64293 Darmstadt',
    organization_country_code  = 'de',
    substitute_organization_id = 159625
where original_organization_id = 238454;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MERCK PATENT GESELLSCHAFT MIT BESCHRAENKTER HAFTUNG Frankfurter Strasse 250 D-64293',
    organization_country_code  = 'de',
    substitute_organization_id = 159625
where original_organization_id = 238452;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER INGELHEIM PHARMA KG Binger Strasse 173, 55216, INGELHEIM AM RHEIN',
    organization_country_code  = 'de',
    substitute_organization_id = 171559
where original_organization_id = 227545;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER INGELHEIM PHARMA KG Binger Strasse 173 55216',
    organization_country_code  = 'de',
    substitute_organization_id = 171559
where original_organization_id = 227544;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER INGELHEIM PHARMA GMBH & CO KG D-55216 INGELHEIM AM RHEIN',
    organization_country_code  = 'de',
    substitute_organization_id = 171559
where original_organization_id = 227537;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER INGELHEIM PHARMA KG [DE / DE]  D-55216 Ingelheim',
    organization_country_code  = 'de',
    substitute_organization_id = 171559
where original_organization_id = 227541;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER INGELHEIM PHARMA GMBH &  CO. KG BINGER STRASSE 173, D-55216 INGELHEIM AM RHEIN,',
    organization_country_code  = 'de',
    substitute_organization_id = 171559
where original_organization_id = 227536;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Boehringer Ingelheim Pharma GmbH & Co.KG Binger Strasse 173 55218 Ingelheim am Rhein',
    organization_country_code  = 'de',
    substitute_organization_id = 171559
where original_organization_id = 227539;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Boehringer Ingelheim Pharma KG Binger Strasse 173',
    organization_country_code  = 'de',
    substitute_organization_id = 171559
where original_organization_id = 227543;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER INGELHEIM PHARMA KG [DE / DE]  Postfach 200 D-55216 Ingelheim am Rhein',
    organization_country_code  = 'de',
    substitute_organization_id = 171559
where original_organization_id = 227542;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER INGELHEIM INTERNATIONAL GMBH Binger Strasse 173 55216',
    organization_country_code  = 'de',
    substitute_organization_id = 159495
where original_organization_id = 227525;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER INGELHEIM INTERNATIONAL GMBH  [DE/ DE] Binger Strasse 173, 55216 Ingelheim',
    organization_country_code  = 'de',
    substitute_organization_id = 159495
where original_organization_id = 227521;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER INGELHEIM INTERNATIONAL GMBH BINGER STRASSE 173, 55216 INGELHEIM',
    organization_country_code  = 'de',
    substitute_organization_id = 159495
where original_organization_id = 227529;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER INGELHEIM INTERNATIONAL GMBH BINGER STRASSE 173, 55216 INGELHEIM AM RHEIN',
    organization_country_code  = 'de',
    substitute_organization_id = 159495
where original_organization_id = 227530;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER INGELHEIM INTERNATIONAL  GMBH BINGER STRASSE 173 D-55218 INGELHEIM AM RHEIN',
    organization_country_code  = 'de',
    substitute_organization_id = 159495
where original_organization_id = 227519;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER INGELHEIM INTERNATIONAL GMBH Binger Strasse 173 D-55216',
    organization_country_code  = 'de',
    substitute_organization_id = 159495
where original_organization_id = 227528;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER MANNHEIM GMBH  [DE / DE]  Sandhoferstr. 116 D-6800 Mannheim 31',
    organization_country_code  = 'de',
    substitute_organization_id = 158424
where original_organization_id = 227550;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER MANNHEIM GMBH Patentabteilung, Abt. E Sandhofer Strasse 112-132 Postfach 31 01 20',
    organization_country_code  = 'de',
    substitute_organization_id = 158424
where original_organization_id = 227553;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER MANNHEIM GMBH Sandhoferstrasse 116, Postfach 31 01 20, D-6800 31, MANNHEIM',
    organization_country_code  = 'de',
    substitute_organization_id = 158424
where original_organization_id = 227555;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER MANNHEIM GMBH D-68298 Mannheim',
    organization_country_code  = 'de',
    substitute_organization_id = 158424
where original_organization_id = 227552;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER MANNHEIM GMBH Sandhoferstrasse 116 Postfach 31 01 20 D-6800 31',
    organization_country_code  = 'de',
    substitute_organization_id = 158424
where original_organization_id = 227554;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'KNAUF GIPS KG Am Bahnhof 7 97346 Iphofen',
    organization_country_code  = 'de',
    substitute_organization_id = 160384
where original_organization_id = 235956;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Knauf Gips KG Am Bahnhof 7',
    organization_country_code  = 'de',
    substitute_organization_id = 160384
where original_organization_id = 235954;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Gesellschaft für Schwerionenforschung mbH Planckstrasse 1',
    organization_country_code  = 'de',
    substitute_organization_id = 158670
where original_organization_id = 232096;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'GESELLSCHAFT FÜR SCHWERIONENFORSCHUNG MBH  [DE/ DE] Planckstrasse 1, 64291 Darmstadt',
    organization_country_code  = 'de',
    substitute_organization_id = 158670
where original_organization_id = 232094;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Siltronic AG Hanns-Seidel-Platz 4',
    organization_country_code  = 'de',
    substitute_organization_id = 158882
where original_organization_id = 244961;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BAYER AKTIENGESELLSCHAFT D-51368 LEVERKUSEN-BAYERWERK',
    organization_country_code  = 'de',
    substitute_organization_id = 157919
where original_organization_id = 226902;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BAYER AG Konzernverwaltung RP Patentabteilung',
    organization_country_code  = 'de',
    substitute_organization_id = 157919
where original_organization_id = 226898;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Bayer Schering Pharma AG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 226903;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Hartwig Schleuter',
    organization_country_code  = 'de',
    substitute_organization_id = 158580
where original_organization_id = 232843;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Frithjof W. Schepke GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 231634;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITÄT HAMBURG  Moorweidenstrasse 18, 20148 Hamburg',
    organization_country_code  = 'de',
    substitute_organization_id = 161691
where original_organization_id = 247976;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITÄT HAMBURG  [DE/ DE] Moorweidenstrasse 18, 20148 Hamburg',
    organization_country_code  = 'de',
    substitute_organization_id = 161691
where original_organization_id = 247975;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Technische Universität Hamburg-Harburg Schwarzenbergstrasse 95',
    organization_country_code  = 'de',
    substitute_organization_id = 167761
where original_organization_id = 246687;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Universitätsklinikum Hamburg-Eppendorf',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 247994;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TRUMPF HÜTTINGER GMBH + CO. KG Bötzinger Strasse 80 79111 Freiburg',
    organization_country_code  = 'de',
    substitute_organization_id = 158809
where original_organization_id = 247361;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TRUMPF Hüttinger GmbH + Co. KG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 247360;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Universitätsklinikum Freiburg Hugstetter Strasse 49',
    organization_country_code  = 'de',
    substitute_organization_id = 161534
where original_organization_id = 247991;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITATSKLINIKUM FREIBURG Hugstetterstrasse 49 79106',
    organization_country_code  = 'de',
    substitute_organization_id = 161534
where original_organization_id = 247780;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITÄTSKLINIKUM FREIBURG Hugstetter Strasse 49 79106 Freiburg',
    organization_country_code  = 'de',
    substitute_organization_id = 161534
where original_organization_id = 247992;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MEC CONTAINER SAFETY SYSTEMS GMBH Kuehnehoefe 1 22761 Hamburg',
    organization_country_code  = 'de',
    substitute_organization_id = 158603
where original_organization_id = 238288;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MEC Container Safety Systems GmbH Kuehnehoefe 1',
    organization_country_code  = 'de',
    substitute_organization_id = 158603
where original_organization_id = 238287;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'KESO PATENTVERWERTUNGSGES. d. b. R. Robert-Nhil-Strasse 4 D-20099 Hamburg',
    organization_country_code  = 'de',
    substitute_organization_id = 168973
where original_organization_id = 235774;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Henkel AG & Co. KGaA Henkelstrasse 67',
    organization_country_code  = 'de',
    substitute_organization_id = 159223
where original_organization_id = 232913;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HENKEL-ECOLAB GMBH & CO. OHG [DE / DE]  Reisholzer Werftstrasse 38-40 D-40589 Düsseldorf',
    organization_country_code  = 'de',
    substitute_organization_id = 162296
where original_organization_id = 232921;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HENKEL-ECOLAB GMBH & CO. OHG Reisholzer Werfestrasse 38-40 D-40589',
    organization_country_code  = 'de',
    substitute_organization_id = 162296
where original_organization_id = 232922;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Radove GmbH Schuchardtweg 2',
    organization_country_code  = 'de',
    substitute_organization_id = 177166
where original_organization_id = 243560;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BeeWaTec AG Kunstmühlestr. 16',
    organization_country_code  = 'de',
    substitute_organization_id = 159919
where original_organization_id = 226970;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BEEWA TEC GMBH Kunstmühlestr. 16 72793 Pfullingen',
    organization_country_code  = 'de',
    substitute_organization_id = 159919
where original_organization_id = 226969;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Beewa Tec GmbH Kunstmühlestr. 16',
    organization_country_code  = 'de',
    substitute_organization_id = 159919
where original_organization_id = 226968;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Bury Henryk',
    organization_country_code  = 'de',
    substitute_organization_id = 160276
where original_organization_id = 228067;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Bury GmbH & Co. KG Robert-Koch-Strasse 1-7',
    organization_country_code  = 'de',
    substitute_organization_id = 160276
where original_organization_id = 228066;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BURY, Henryk  Vor der Egge 8, 32584 Löhne',
    organization_country_code  = 'de',
    substitute_organization_id = 160276
where original_organization_id = 228098;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Pfeifer Holding GmbH & Co. KG Dr.-Karl-Lenz-Strasse 66',
    organization_country_code  = 'de',
    substitute_organization_id = 159794
where original_organization_id = 241206;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'PFEIFER HOLDING GMBH & CO. KG DR.-KARL-LENZ-STRASSE 66 87700',
    organization_country_code  = 'de',
    substitute_organization_id = 159794
where original_organization_id = 241207;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'EPCOS AG St.-Martin-Straße 53 81669 München',
    organization_country_code  = 'de',
    substitute_organization_id = 172192
where original_organization_id = 230799;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'PIONTEK, Georg  [DE/ DE] Grüner Talstr. 22-24, 58644 Iserlohn',
    organization_country_code  = 'de',
    substitute_organization_id = 160663
where original_organization_id = 241401;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Susilo  Rudy Dr.',
    organization_country_code  = 'de',
    substitute_organization_id = 159529
where original_organization_id = 246130;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Deutsche Post AG Charles-de-Gaulle-Strasse 20',
    organization_country_code  = 'de',
    substitute_organization_id = 171563
where original_organization_id = 229848;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DEUTSCHE BP (Aktiengesellschaft) MAX BORN STRASSE 2 D 22761 HAMBOURG',
    organization_country_code  = 'de',
    substitute_organization_id = 165937
where original_organization_id = 229843;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DKFZ Deutsches Krebsforschungszentrum Stiftung des öffentlichen Rechts,  Im Neuenheimer Feld 280',
    organization_country_code  = 'de',
    substitute_organization_id = 171176
where original_organization_id = 229929;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DKFZ  [DE/ DE] Deutsches Krebsforschungszentrum, Stiftung des öffentlichen Rechts, Im Neuenheimer Feld 280, 69120 Heidelberg',
    organization_country_code  = 'de',
    substitute_organization_id = 171176
where original_organization_id = 229927;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DKFZ DEUTSCHES KREBSFORSCHUNGSZENTRUM, STIFTUNG DES OFFENTLICHEN RECHTS Im Neuenheimer Feld 280 69120',
    organization_country_code  = 'de',
    substitute_organization_id = 171176
where original_organization_id = 229930;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'IHP GmbH-Innovations for High Performance  Microelectronics / Leibniz-Institut für innovative  Mikroelektronik Im Technologiepark 25 / 15236 Frankfurt',
    organization_country_code  = 'de',
    substitute_organization_id = 177125
where original_organization_id = 233374;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'IHP GmbH—Innovations for High Performance Microelectronics',
    organization_country_code  = 'de',
    substitute_organization_id = 177125
where original_organization_id = 233376;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'IHP GmbH - Innovations for High Performance Microelectronics / Leibniz-Institut für innovative Mikroelektronik',
    organization_country_code  = 'de',
    substitute_organization_id = 177125
where original_organization_id = 233372;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SMS DEMAG AG Eduard-Schloemann-Strasse 4 40237',
    organization_country_code  = 'de',
    substitute_organization_id = 169705
where original_organization_id = 245243;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SMS DEMAG AG [DE / DE]  Eduard-Schloemann-Strasse 4 D-40237 Düsseldorf',
    organization_country_code  = 'de',
    substitute_organization_id = 169705
where original_organization_id = 245241;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'atg test systems GmbH & Co. KG Reicholzheim, Zum Schlag 3 97877 Wertheim',
    organization_country_code  = 'de',
    substitute_organization_id = 176712
where original_organization_id = 226364;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Universität Kassel Mönchebergstrasse 19',
    organization_country_code  = 'de',
    substitute_organization_id = 177194
where original_organization_id = 247983;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Helmholtz-Zentrum Berlin für Materialien und Energie GmbH Hahn-Meitner-Platz 1',
    organization_country_code  = 'de',
    substitute_organization_id = 160817
where original_organization_id = 232901;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TOMTOM GERMANY GMBH & CO. KG Am Neuen Horizont 1 31177 Harsum',
    organization_country_code  = 'de',
    substitute_organization_id = 167851
where original_organization_id = 247180;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Carl Zeiss Microscopy GmbH Carl-Zeiss-Promenade 10',
    organization_country_code  = 'de',
    substitute_organization_id = 161152
where original_organization_id = 228294;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Carl Zeiss Industrielle Messtechnik GmbH Carl-Zeiss-Strasse 22',
    organization_country_code  = 'de',
    substitute_organization_id = 159288
where original_organization_id = 228290;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'CARL ZEISS INDUSTRIELLE MESSTECHNIK GMBH Carl-Zeiss-Str. 22 73447 Oberkochen',
    organization_country_code  = 'de',
    substitute_organization_id = 159288
where original_organization_id = 228289;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'AEG Niederspannungstechnik GmbH & Co. KG Berliner Platz 2-6',
    organization_country_code  = 'de',
    substitute_organization_id = 161245
where original_organization_id = 225473;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DR. SCHUMACHER GMBH ZUM STEEGER 3 34323 MALSFELD/ BEISEFORTH',
    organization_country_code  = 'de',
    substitute_organization_id = 177804
where original_organization_id = 230103;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'CHARITE- UNIVERSITÄTSMEDIZIN BERLIN, GEMEINSAME EINRICHTUNG VON FREIR UNIVERSITÄT BERLIN UND HUMBOLDT-UNIVERSITÄT ZU BERLIN KÖRPERSCHAFT DES ÖFFENTLI CHEN RECHTS',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 228714;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MOMENTIVE PERFORMANCE MATERIALS GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 238938;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'INTERTRACTOR AKTIENGESELLSCHAFT HAGENER STR 323 D 58285 GEVELSBERG',
    organization_country_code  = 'de',
    substitute_organization_id = 159586
where original_organization_id = 234871;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Intertractor Zweigniederlassung der Wirtgen GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = 159586
where original_organization_id = 234872;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Schaefer Kalk GmbH & Co. KG Louise-Seher-Strasse 6',
    organization_country_code  = 'de',
    substitute_organization_id = 159948
where original_organization_id = 244496;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SCHAEFER KALK GMBH & CO. KG  Louise-Seher-Str. 6, 65582 Diez',
    organization_country_code  = 'de',
    substitute_organization_id = 159948
where original_organization_id = 244494;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Phoenix Contact GmbH & Co. KG Flachsmarktstrasse 8',
    organization_country_code  = 'de',
    substitute_organization_id = 161653
where original_organization_id = 241263;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'PHOENIX CONTACT GMBH & CO. KG Flachsmarktstraße 8 32825 Blomberg',
    organization_country_code  = 'de',
    substitute_organization_id = 161653
where original_organization_id = 241264;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ROMMELSPACHER, Hans [DE / DE]  Salzbrunner Strasse 42 D-14193 Berlin',
    organization_country_code  = 'de',
    substitute_organization_id = 160154
where original_organization_id = 243997;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Rommelspacher, Hans Salzbrunner Strasse 42',
    organization_country_code  = 'de',
    substitute_organization_id = 160154
where original_organization_id = 243998;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Chaloun, Dieter Klaistower Strasse 51',
    organization_country_code  = 'de',
    substitute_organization_id = 160857
where original_organization_id = 228709;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FEDERAL-MOGUL WIESBADEN GMBH STIELSTRAßE 11, 65201 WIESBADEN',
    organization_country_code  = 'de',
    substitute_organization_id = 173615
where original_organization_id = 231215;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Federal-Mogul Nürnberg GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 231209;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FEDERAL-MOGUL NÜRNBERG GMBH  Nopitschstrasse 67, 90441 Nürnberg',
    organization_country_code  = 'de',
    substitute_organization_id = 173615
where original_organization_id = 231208;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Johannes Gutenberg-Universität Mainz',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 235328;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'JOHANNES GUTENBERG UNIVERSITAET MAINZ Sarrstr. 21 55122',
    organization_country_code  = 'de',
    substitute_organization_id = 172839
where original_organization_id = 235326;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TRON - Translationale Onkologie an der Universitätsmedizin der Johannes Gutenberg- Universität Mainz gemeinnützige GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 247337;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TRON-TRANSLATIONALE ONKOLOGIE AN DER UNIVERSITATSMEDIZIN DER JOHANNES GUTENBERG-UNIVERSITAT MAINZ GEMEINNUTZIGE GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = 168590
where original_organization_id = 247339;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BROWN, BOVERI & CIE Aktiengesellschaft Kallstadter Strasse 1',
    organization_country_code  = 'de',
    substitute_organization_id = 161036
where original_organization_id = 227839;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BROWN, BOVERI & CIE Aktiengesellschaft Kallstadter Strasse 1 D-68309 Mannheim',
    organization_country_code  = 'de',
    substitute_organization_id = 161036
where original_organization_id = 227840;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Wader-Wittis GmbH Justus-von-Liebig-Strasse 3 42477 Radevormwald',
    organization_country_code  = 'de',
    substitute_organization_id = 159634
where original_organization_id = 248752;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITÄT DES SAARLANDES Wissens-und Technologietransfer GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 247971;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Altana Electrical Insulation Gmbh Abelstrasse 45',
    organization_country_code  = 'de',
    substitute_organization_id = 163073
where original_organization_id = 225905;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Altana Electrical Insulation GmbH Abelstrasse 45 46483 Wesel',
    organization_country_code  = 'de',
    substitute_organization_id = 163073
where original_organization_id = 225907;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ALTANA ELECTRICAL INSULATION GMBH  [DE/ DE] Abelstrasse 45, 46483 Wesel',
    organization_country_code  = 'de',
    substitute_organization_id = 163073
where original_organization_id = 225904;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Schott Glas',
    organization_country_code  = 'de',
    substitute_organization_id = 150272
where original_organization_id = 244552;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'LUDWIG-MAXIMILIANS-UNIVERSITÄT MÜNCHEN Geschwister-Scholl-Platz 1 80539 München',
    organization_country_code  = 'de',
    substitute_organization_id = 168864
where original_organization_id = 237597;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'LUDWIG-MAXIMILIANS-UNIVERSITÄT MUNCHEN Geschwister-Scholl-Platz 1 80539 Münich',
    organization_country_code  = 'de',
    substitute_organization_id = 168864
where original_organization_id = 237594;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'PTW-Freiburg Physikalisch-Technische Werkstätten  Dr. Pychlau GmbH  Lörracher Strasse 7',
    organization_country_code  = 'de',
    substitute_organization_id = 169004
where original_organization_id = 243366;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Continental Automotive GmbH Vahrenwalder Strasse 9',
    organization_country_code  = 'de',
    substitute_organization_id = 175584
where original_organization_id = 229144;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'CONTINENTAL TEVES AG & CO. OHG [DE / DE]  Guerickestrasse 7 D-60488 Frankfurt',
    organization_country_code  = 'de',
    substitute_organization_id = 165075
where original_organization_id = 229149;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Continental Teves AG & Co. oHG Guerickestrasse 7',
    organization_country_code  = 'de',
    substitute_organization_id = 165075
where original_organization_id = 229150;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BUNDESREPUBLIK DEUTSCHLAND, VERTR. D. D. BUNDESMINISTERIUM F. WIRTSCHAFT UND ARBEIT, D. VERTR. D.D. PRÄSIDENTEN DER PHYSIKALISCHTECHNISCHEN BUNDESANSTALT',
    organization_country_code  = 'de',
    substitute_organization_id = 172701
where original_organization_id = 228041;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BIONTECH AG AN DER GOLDGRUBE 12 55131',
    organization_country_code  = 'de',
    substitute_organization_id = 163815
where original_organization_id = 227268;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Biontech AG Hölderlinstrasse 8',
    organization_country_code  = 'de',
    substitute_organization_id = 163815
where original_organization_id = 227270;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BioNTech AG Philipp-von-Zabern-Platz 1',
    organization_country_code  = 'de',
    substitute_organization_id = 163815
where original_organization_id = 227272;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MAHLA, Sylvio  Leipziger Str. 9, 04683 Naunhof',
    organization_country_code  = 'de',
    substitute_organization_id = 164989
where original_organization_id = 237759;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BYK Gulden Lomberg Chemische Fabrik Gesellschaft mit beschrankter Haftung',
    organization_country_code  = 'de',
    substitute_organization_id = 168743
where original_organization_id = 228144;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MS-Bad Innovationen GmbH Dr.-Weidig-Strasse 4 36320 Kirtorf',
    organization_country_code  = 'de',
    substitute_organization_id = 175751
where original_organization_id = 239107;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Ampler, Klaus Norderneyer Weg 148 04157 Leipzig',
    organization_country_code  = 'de',
    substitute_organization_id = 165723
where original_organization_id = 225992;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'B. Braun Melsungen AG Carl-Braun-Strasse 1',
    organization_country_code  = 'de',
    substitute_organization_id = 169924
where original_organization_id = 226656;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'JUSTUS-LIEBIG-UNIVERSITÄT GIESSEN',
    organization_country_code  = 'de',
    substitute_organization_id = 164113
where original_organization_id = 235401;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITÄT KARLSRUHE (TH)  [DE/ DE] Kaiserstrasse 12, 76128 Karlsruhe',
    organization_country_code  = 'de',
    substitute_organization_id = 170135
where original_organization_id = 247978;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Universitaet Karlsruhe.',
    organization_country_code  = 'de',
    substitute_organization_id = 170135
where original_organization_id = 247766;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Universität Karlsruhe (TH) - Körperschaft des öffentlichen Rechts',
    organization_country_code  = 'de',
    substitute_organization_id = 170135
where original_organization_id = 247979;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'GRIGORIEV, Eugene  [DE/ CH] c/o Universitaet Karlsruhe, Institut for Experimental Kernphysik, Postfach 6980, 76128 Karlsruhe',
    organization_country_code  = 'de',
    substitute_organization_id = 170135
where original_organization_id = 232536;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DE BOER, Wim  [DE/ DE] c/o Universitaet Karlsruhe, Institut for Experimental Kernphysik, Postfach 6980, 76128 Karlsruhe',
    organization_country_code  = 'de',
    substitute_organization_id = 170135
where original_organization_id = 229661;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Beto-Tornado GmbH Meinhardstrasse 4 D-44379 Dortmund',
    organization_country_code  = 'de',
    substitute_organization_id = 167433
where original_organization_id = 227090;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'GEBHART, Hans',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 231969;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BAUSTOFFWERKE GEBHART & SÖHNE GMBH & CO. KG  Hochstrasse 2, 88317 Aichstetten',
    organization_country_code  = 'de',
    substitute_organization_id = 169902
where original_organization_id = 226887;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'IBM DEUTSCHLAND GMBH c/o IBM Deutschland Management & Business Support GmbH Patentwesen und Urheberrecht Postfach 71137 Ehningen',
    organization_country_code  = 'de',
    substitute_organization_id = 168393
where original_organization_id = 233291;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BÖSHA GMBH & CO. KG [DE / DE]  Industriegebiet Heidberg 10 D-59602 Rüthen',
    organization_country_code  = 'de',
    substitute_organization_id = 176438
where original_organization_id = 228162;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'NORMA GERMANY GMBH Edisonstraße 4 63477 Maintal',
    organization_country_code  = 'de',
    substitute_organization_id = 169349
where original_organization_id = 239823;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SIEMAG TECBERG GMBH Kalteiche-Ring 28-32 35708 Haiger',
    organization_country_code  = 'de',
    substitute_organization_id = 174411
where original_organization_id = 244881;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SIEMAG TECBERG GMBH Kalteiche-Ring 28-32 35708',
    organization_country_code  = 'de',
    substitute_organization_id = 174411
where original_organization_id = 244880;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MUCOS PHARMA GMBH & CO. [DE / DE]  Malvenweg 2 D-82538 Geretsried',
    organization_country_code  = 'de',
    substitute_organization_id = 176011
where original_organization_id = 239137;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FOS MESSTECHNIK GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = 163857
where original_organization_id = 231531;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FOS Messtechnik GMBH Ruetgersstrasse, 40 24790 Schacht-audorf',
    organization_country_code  = 'de',
    substitute_organization_id = 163857
where original_organization_id = 231533;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'STRUCTREPAIR GMBH Am Dürrbachgraben 1 01945 Ruhland',
    organization_country_code  = 'de',
    substitute_organization_id = 164987
where original_organization_id = 246027;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Adelholzener Alpenquellen GmbH St.-Primus-Strasse 1-5',
    organization_country_code  = 'de',
    substitute_organization_id = 172706
where original_organization_id = 225368;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ADELHOLZENER ALPENQUELLEN GMBH  [DE/ DE] St.-Primus-Strasse 1-5, 83313 Siegsdorf',
    organization_country_code  = 'de',
    substitute_organization_id = 172706
where original_organization_id = 225367;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FM Systeme Förder-und Montagetechnik Schmalzhofer GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 231454;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FM SYSTEME FÖRDER- UND MONTAGETECHNIK Schmalzhofer GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = 168700
where original_organization_id = 231453;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BUDZINSKY + HÖR VERWALTUNGS-GMBH  [DE/ DE] Industriestrasse 15, 70565 Stuttgart',
    organization_country_code  = 'de',
    substitute_organization_id = 174518
where original_organization_id = 227987;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ArianeGroup GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 226222;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'CSL Behring GmbH Emil-von-Behring-Straße 76 35041 Marburg',
    organization_country_code  = 'de',
    substitute_organization_id = 170259
where original_organization_id = 229259;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WESTFALIA SEPARATOR AG Werner-Habig-Strasse 1 59302 Oelde',
    organization_country_code  = 'de',
    substitute_organization_id = 172920
where original_organization_id = 249055;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WESTFALIA SEPARATOR AG Werner-Habig-Strasse 1',
    organization_country_code  = 'de',
    substitute_organization_id = 172920
where original_organization_id = 249054;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FREIE UNIVERSITÄT BERLIN  Thielallee 63, 14195 Berlin',
    organization_country_code  = 'de',
    substitute_organization_id = 176184
where original_organization_id = 231613;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FREIE UNIVERSITAET BERLIN',
    organization_country_code  = 'de',
    substitute_organization_id = 176184
where original_organization_id = 231610;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ACCUMULATORENWERKE HOPPECKE CARL ZOELLNER & SOHN GMBH & CO. KG Bontkirchener Strasse 2 D-59929',
    organization_country_code  = 'de',
    substitute_organization_id = 166262
where original_organization_id = 225238;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'KIRCHHOFF AUTOMOTIVE DEUTSCHLAND GMBH Am Eckenbach 10-14 57439 Attendorn',
    organization_country_code  = 'de',
    substitute_organization_id = 171767
where original_organization_id = 235857;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Pulverer, Gerhard, Prof. Dr. med. Mohnweg 25',
    organization_country_code  = 'de',
    substitute_organization_id = 174891
where original_organization_id = 243388;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MIKOLAICZIK, Gerhard Michael',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 238720;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'POLYTEX SPORTBELAGE PRODUKTIONS-GMBH VINKRATHER STRASSE 43 47929',
    organization_country_code  = 'de',
    substitute_organization_id = 163527
where original_organization_id = 242232;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'VALLEN, Hartmut  [DE/ DE] Schäftlarner Weg 26, D-82057 Icking',
    organization_country_code  = 'de',
    substitute_organization_id = 169041
where original_organization_id = 248469;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BHS-Sonthofen GmbH Hans-Böckler-Strasse 7 87527 Sonthofen',
    organization_country_code  = 'de',
    substitute_organization_id = 177189
where original_organization_id = 227096;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BHS SONTHOFEN MASCHINEN & ANLAGENBAU GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = 177189
where original_organization_id = 227095;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Hoerbiger Automotive Komfortsysteme GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 233027;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HOERBIGER AUTOMOTIVE KOMFORTSYSTEME GMBH Martina-Hörbiger-Strasse 5 86956 Schongau',
    organization_country_code  = 'de',
    substitute_organization_id = 168056
where original_organization_id = 233028;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Stumpf, Manfred',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 246060;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Fujitsu Technology Solutions  Intellectual Property GmbH Mies-van-der-Rohe-Strasse 8',
    organization_country_code  = 'de',
    substitute_organization_id = 177024
where original_organization_id = 231668;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Asco Numatics GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 226300;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TEXAS INSTRUMENTS DEUTSCHLAND GMBH  Haggertystrasse 1, 85356 Freising',
    organization_country_code  = 'de',
    substitute_organization_id = 170495
where original_organization_id = 246878;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BALS ELEKTROTECHNIK GMBH & CO. KG Burgweg 22 57399 Kirchhundem-Albaum',
    organization_country_code  = 'de',
    substitute_organization_id = 177340
where original_organization_id = 226728;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Wicinski, Mariusz Mühlenstrasse 10',
    organization_country_code  = 'de',
    substitute_organization_id = 164099
where original_organization_id = 249085;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'EDAG ENGINEERING GMBH Kreuzberger Ring 40 65205 Wiesbaden',
    organization_country_code  = 'de',
    substitute_organization_id = 174344
where original_organization_id = 230423;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ALFRED TEVES GMBH Guerickestrasse 7',
    organization_country_code  = 'de',
    substitute_organization_id = 174389
where original_organization_id = 225838;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'INTERATOM Gesellschaft mit beschrænkter Haftung Friedrich-Ebert-Strasse',
    organization_country_code  = 'de',
    substitute_organization_id = 174438
where original_organization_id = 234734;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'LUCAS AUTOMOTIVE GMBH  Carl-Spaeter-Strasse 8, 56070 Koblenz',
    organization_country_code  = 'de',
    substitute_organization_id = 174555
where original_organization_id = 237565;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOART LONGYEAR GMBH & CO. KG STADEWG 18-24, 36151',
    organization_country_code  = 'de',
    substitute_organization_id = 174945
where original_organization_id = 227481;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOART LONGYEAR GMBH & CO KG STADEWG 18-24, 36151, BURGHAUN',
    organization_country_code  = 'de',
    substitute_organization_id = 174945
where original_organization_id = 227479;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITAETSKLINIKUM FREIBURG Hugstetterstrasse 49 79106',
    organization_country_code  = 'de',
    substitute_organization_id = 176958
where original_organization_id = 247771;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ALBERT-LUDWIGS-UNIVERSITÄT FREIBURG  Fahnenbergplatz, 79085 Freiburg',
    organization_country_code  = 'de',
    substitute_organization_id = 169604
where original_organization_id = 225800;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Albert-Ludwigs-Universität Freiburg Fahnenbergplatz',
    organization_country_code  = 'de',
    substitute_organization_id = 169604
where original_organization_id = 225801;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Universitaetsklinikum Freiburg',
    organization_country_code  = 'de',
    substitute_organization_id = 161534
where original_organization_id = 247769;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SYMRISE GMBH & CO. KG  [DE/ DE] Mühlenfeldstr. 1, 37603 Holzminden',
    organization_country_code  = 'de',
    substitute_organization_id = 178391
where original_organization_id = 246219;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'OSRAM GESELLSCHAFT MIT BESCHRÄNKTER HAFTUNG  Hellabrunner Str. 1, 81543 München',
    organization_country_code  = 'de',
    substitute_organization_id = 175195
where original_organization_id = 240631;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BAUMANN-SCHILP, Lucia  [DE / DE]  Maistr. 8 D-8031 Wörthsee',
    organization_country_code  = 'de',
    substitute_organization_id = 174230
where original_organization_id = 226879;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Szymos, Heinrich',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 246537;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SECARNA PHARMACEUTICALS GMBH & CO. KG Bismarckstraße 7 35037 Marburg',
    organization_country_code  = 'de',
    substitute_organization_id = 169322
where original_organization_id = 244614;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Kernforschungsanlage Jülich Gesellschaft mit beschrænkter Haftung Postfach 1913',
    organization_country_code  = 'de',
    substitute_organization_id = 165835
where original_organization_id = 235768;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'KLINGENBURG GMBH Boystrasse 115 45968',
    organization_country_code  = 'de',
    substitute_organization_id = 169373
where original_organization_id = 235918;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Protectimmun GmbH Ückendorfer Str. 237e',
    organization_country_code  = 'de',
    substitute_organization_id = 169414
where original_organization_id = 242601;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'GAMEEL, Mohamed',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 231797;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DOSCH, Günther [DE / DE]  Bocksbeutelstrasse 2 D-97337 Dettelbach',
    organization_country_code  = 'de',
    substitute_organization_id = 176752
where original_organization_id = 230065;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Holst, Otto, Prof. Dr. rer. nat.',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 233054;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HEINRICH GILLET GMBH Luitpoldstraße 83 67480 Edenkoben',
    organization_country_code  = 'de',
    substitute_organization_id = 165551
where original_organization_id = 232891;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'InVerTec Innovative Verfahrenstechnik e.V.',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 234911;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Bufe, Albrecht, Prof. Dr. med.',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 227995;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITÄT KONSTANZ  Universitätsstrasse 10, 78464 Konstanz',
    organization_country_code  = 'de',
    substitute_organization_id = 163903
where original_organization_id = 247984;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TechniSat Digital GmbH Julius-Saxler-Strasse 3',
    organization_country_code  = 'de',
    substitute_organization_id = 176701
where original_organization_id = 246681;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ArcelorMittal Hamburg GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 226191;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ARSANIS Biosciences GmbH',
    organization_country_code  = 'at',
    substitute_organization_id = 158099
where original_organization_id = 226265;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'GE JENBACHER GMBH & CO OG Achenseestrasse 1-3 6200 Jenbach',
    organization_country_code  = 'at',
    substitute_organization_id = 160209
where original_organization_id = 231959;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'CROMA-PHARMA GMBH Industriezeile 15 A-2100 Leobendorf',
    organization_country_code  = 'at',
    substitute_organization_id = 160666
where original_organization_id = 229243;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Croma-Pharma GmbH Industriezeile 15',
    organization_country_code  = 'at',
    substitute_organization_id = 160666
where original_organization_id = 229241;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'CROMA-PHARMA GMBH INDUSTRIEZEILE 15A-2100 LEOBENDORF,',
    organization_country_code  = 'at',
    substitute_organization_id = 160666
where original_organization_id = 229244;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Messer Austria GmbH Am Kanal 2 2352 Gumpoldskirchen',
    organization_country_code  = 'at',
    substitute_organization_id = 159692
where original_organization_id = 238477;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Med''Arom Prof. Dr. Franz OG',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 238300;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Med''Arom Prof. Dr. Franz OG Sieveringerstr. 169 1190 Wien',
    organization_country_code  = 'at',
    substitute_organization_id = 162657
where original_organization_id = 238301;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'JOANNEUM RESEARCH FORSCHUNGSGESELLSCHAFT MBH  Steyrergasse 17, A-8010 Graz',
    organization_country_code  = 'at',
    substitute_organization_id = 165817
where original_organization_id = 235324;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'CONVERGENT INFORMATION TECHNOLOGIES GMBH Schulstrasse 2 4053 Haid',
    organization_country_code  = 'at',
    substitute_organization_id = 168431
where original_organization_id = 229168;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'KNORR-BREMSE GESELLSCHAFT MIT BESCHRÄNKTER HAFTUNG Beethovengasse 43-45 A-2340 Mödling',
    organization_country_code  = 'at',
    substitute_organization_id = 172808
where original_organization_id = 235959;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'NOVOMATIC AG',
    organization_country_code  = 'at',
    substitute_organization_id = 166467
where original_organization_id = 239888;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'VICTUS LEBENSMITTELINDUSTRIEBEDARF VERTRIEBSGESELLSCHAFT MBH Eduard Kittenbergergasse 97 AT-1230',
    organization_country_code  = 'at',
    substitute_organization_id = 173816
where original_organization_id = 248538;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Victus Lebensmittelindustriebedarf Vertriebsgesellschaft mbh Eduard-Kittenbergergasse 97 1230 Wien',
    organization_country_code  = 'at',
    substitute_organization_id = 173816
where original_organization_id = 248540;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Victus Lebensmittelindustriebedarf Vertriebsgesellschaft mbh Eduard-Kittenbergergasse 97',
    organization_country_code  = 'at',
    substitute_organization_id = 173816
where original_organization_id = 248539;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Kubica, Wladyslaw Hauergasse 12/1 A-2410 Hainburg',
    organization_country_code  = 'at',
    substitute_organization_id = 162313
where original_organization_id = 236769;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Mayr-Melnhof Karton AG',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 238209;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Mundigler, Norbert, Dipl.-Ing. Dr. Hötzendorfgasse 3 A-3423 St. Andrä/Wördern',
    organization_country_code  = 'at',
    substitute_organization_id = 165737
where original_organization_id = 239153;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Novartis-Erfindungen Verwaltungsgesellschaft m.b.H.',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 239866;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Innovaphyt GmbH Institut für Angewandte Botanik  und Pharmakognosie',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 233539;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Figo GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 221910;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Katolícka Univerzita v Lubline',
    organization_country_code  = 'pl',
    substitute_organization_id = null
where original_organization_id = 222119;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TRW Automotive GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 247386;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Diehl AKO Stiftung & Co. KG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 229887;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SMA SOLAR TECHNOLOGY AG Sonnenallee 1 34266 Niestetal',
    organization_country_code  = 'de',
    substitute_organization_id = 155938
where original_organization_id = 245195;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MTU Aero Engines GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = 156156
where original_organization_id = 239124;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WYZR LTD.',
    organization_country_code  = 'ie',
    substitute_organization_id = null
where original_organization_id = 224742;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'e-Content Store S.a.r.l.',
    organization_country_code  = 'lu',
    substitute_organization_id = null
where original_organization_id = 223747;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOMBARDIER PRIMOVE GMBH Schöneberger Ufer 1 10785 Berlin',
    organization_country_code  = 'de',
    substitute_organization_id = 156020
where original_organization_id = 227608;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Nokia Networks B.V.',
    organization_country_code  = 'nl',
    substitute_organization_id = null
where original_organization_id = 239740;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FORSCHUNGSZENTRUM JÜLICH GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 231518;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SGL CARBON SE SOHNLEINSTR. 8 65201',
    organization_country_code  = 'de',
    substitute_organization_id = 156113
where original_organization_id = 244815;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DIEHL AKO STIFTUNG & CO. KG',
    organization_country_code  = 'de',
    substitute_organization_id = 156004
where original_organization_id = 229882;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Bayerische Motoren Werke AG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 226905;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FAURECIA AUTOSITZE GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = 156079
where original_organization_id = 231185;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BASF SE',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 226860;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Mahle International GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 237764;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'EADS Deutschland GmbH Willy-Messerschmitt-Strasse',
    organization_country_code  = 'de',
    substitute_organization_id = 160126
where original_organization_id = 230339;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'RUETGERSWERKE AG',
    organization_country_code  = 'de',
    substitute_organization_id = 156621
where original_organization_id = 244103;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SGL CFL CE GmbH Werner-von-Siemens-Strasse 18',
    organization_country_code  = 'de',
    substitute_organization_id = 176723
where original_organization_id = 244820;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'NOKIA SIEMENS NETWORKS GMBH & CO. KG  St. Martin Str. 76, 81541 München',
    organization_country_code  = 'de',
    substitute_organization_id = 156616
where original_organization_id = 239738;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Bayer AG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 226899;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'PAS Deutschland GmbH Wilhelm-Bartelt-Strasse 10-14',
    organization_country_code  = 'de',
    substitute_organization_id = 158042
where original_organization_id = 240996;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ROTO FRANK AG Stuttgarter Strasse 145-149',
    organization_country_code  = 'de',
    substitute_organization_id = 166065
where original_organization_id = 244043;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MERCK PATENT GMBH Frankfurter Strasse 250 64293 Darmstadt',
    organization_country_code  = 'de',
    substitute_organization_id = 159625
where original_organization_id = 238456;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BAYER AG D-51368 Leverkusen',
    organization_country_code  = 'de',
    substitute_organization_id = 157919
where original_organization_id = 226897;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SCHLUETER, HARTWIG Traenkegasse 1 34260',
    organization_country_code  = 'de',
    substitute_organization_id = 158580
where original_organization_id = 244525;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ROBERT BOSCH GMBH Postfach 30 02 20 70442 Stuttgart',
    organization_country_code  = 'de',
    substitute_organization_id = 157022
where original_organization_id = 243875;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SGL CARBON SE Rheingaustraße 182 65203 Wiesbaden',
    organization_country_code  = 'de',
    substitute_organization_id = 156113
where original_organization_id = 244812;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'KESO PATENTVERWERTUNGSGES. d. b. R. Robert-Nhil-Strasse 4',
    organization_country_code  = 'de',
    substitute_organization_id = 168973
where original_organization_id = 235773;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ETO MAGNETIC GMBH ; ETO',
    organization_country_code  = 'de',
    substitute_organization_id = 156498
where original_organization_id = 230926;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Wolf, Daniel',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 249618;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Nocon, Jacek',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 239682;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ARSANIS BIOSCIENCES GMBH Helmut-Qualtinger-Gasse 2 A-1030 Vienna',
    organization_country_code  = 'at',
    substitute_organization_id = 158099
where original_organization_id = 226268;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Henkel AG&Co. KGAA Henkelstr. 67',
    organization_country_code  = 'de',
    substitute_organization_id = 159223
where original_organization_id = 232914;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HEIDELBERGCEMENT AG Berliner Str. 6 69120 Heidelberg',
    organization_country_code  = 'de',
    substitute_organization_id = 157247
where original_organization_id = 232887;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Deutsches Elektronen-Synchrotron DESY Notkestrasse 85',
    organization_country_code  = 'de',
    substitute_organization_id = 164584
where original_organization_id = 229852;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'RADOVE GMBH  Schuchardtweg 2, 14109 Berlin',
    organization_country_code  = 'de',
    substitute_organization_id = 177166
where original_organization_id = 243559;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'RUTGERS CHEMICALS AG KEKULESTRASSE 30, 44579 CASTROP-RAUXEL',
    organization_country_code  = 'de',
    substitute_organization_id = 158616
where original_organization_id = 244112;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Visual Vertigo Software Technologies GmbH Triester Strasse 340',
    organization_country_code  = 'at',
    substitute_organization_id = 173699
where original_organization_id = 248588;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HERBERG GmbH & CO. KG. Meinhardstrasse 4',
    organization_country_code  = 'de',
    substitute_organization_id = 160222
where original_organization_id = 232953;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MOMENTIVE PERFORMANCE MATERIALS GMBH  Building V 7, 51536 Leverkusen',
    organization_country_code  = 'de',
    substitute_organization_id = 178108
where original_organization_id = 238937;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITÄT KASSEL  [DE/ DE] Mönchebergstrasse 19, 34125 Kassel',
    organization_country_code  = 'de',
    substitute_organization_id = 177194
where original_organization_id = 247982;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'RÜTGERS Chemicals AG Kekuléstrasse 30',
    organization_country_code  = 'de',
    substitute_organization_id = 158616
where original_organization_id = 244199;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Universität Hamburg Moorweidenstrasse 18',
    organization_country_code  = 'de',
    substitute_organization_id = 161691
where original_organization_id = 247977;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Med-Plast GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 238302;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HARTWIG SCHLÜTER',
    organization_country_code  = 'de',
    substitute_organization_id = 158580
where original_organization_id = 232844;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Forschungszentrum Borstel Parkallee 1-40',
    organization_country_code  = 'de',
    substitute_organization_id = 160472
where original_organization_id = 231508;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ABB AG Kallstadter Strasse 1',
    organization_country_code  = 'de',
    substitute_organization_id = 156421
where original_organization_id = 225060;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MITTELDEUTSCHE TRESORBAU GMBH Orionstraße 3, Star Park / Halle-Queis 06184 Kabelsketal',
    organization_country_code  = 'de',
    substitute_organization_id = 157834
where original_organization_id = 238855;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Cardionovum GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 228272;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'PFEIFER HOLDING GMBH & CO. KG Dr.- Karl-Lenz-Straße 66 87700 Memmingen',
    organization_country_code  = 'de',
    substitute_organization_id = 159794
where original_organization_id = 241205;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'NEC Europe Ltd.',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 239448;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'NEC EUROPE LTD. Kurfürsten-Anlage 36 69115 Heidelberg',
    organization_country_code  = 'de',
    substitute_organization_id = 160241
where original_organization_id = 239449;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'IHP GmbH-Innovations for High Performance Microelectronics / Leibniz-Institut für innovative Mikroelektronik Im Technologiepark 25',
    organization_country_code  = 'de',
    substitute_organization_id = 177125
where original_organization_id = 233375;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'GE Jenbacher GmbH & Co. OG Achenseestrasse 1-3',
    organization_country_code  = 'at',
    substitute_organization_id = 160209
where original_organization_id = 231962;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'VOLKSWAGEN AKTIENGESELLSCHAFT Berliner Ring 2 38440 Wolfsburg',
    organization_country_code  = 'de',
    substitute_organization_id = 157039
where original_organization_id = 248650;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Boehringer Ingelheim International GmbH Binger Strasse 173',
    organization_country_code  = 'de',
    substitute_organization_id = 159495
where original_organization_id = 227523;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Merck Patent GmbH Frankfurter Strasse 250',
    organization_country_code  = 'de',
    substitute_organization_id = 159625
where original_organization_id = 238455;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'AEG Niederspannungstechnik GmbH & Co. KG Berliner Platz 2-6 24534 Neumünster',
    organization_country_code  = 'de',
    substitute_organization_id = 161245
where original_organization_id = 225474;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HENKEL AG & CO. KGAA Henkelstr. 67 40589 Düsseldorf',
    organization_country_code  = 'de',
    substitute_organization_id = 159223
where original_organization_id = 232912;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Susilo Rudy',
    organization_country_code  = 'de',
    substitute_organization_id = 159529
where original_organization_id = 246132;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Tomtom Germany GmbH & Co. KG Am Neuen Horizont 1',
    organization_country_code  = 'de',
    substitute_organization_id = 167851
where original_organization_id = 247179;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITÄT DES SAARLANDES Campus Saarbrücken 66123 Saarbrücken',
    organization_country_code  = 'de',
    substitute_organization_id = 165508
where original_organization_id = 247970;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SMS Demag AG Eduard-Schloemann-Strasse 4',
    organization_country_code  = 'de',
    substitute_organization_id = 169705
where original_organization_id = 245242;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Technische Universität Dresden',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 246685;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Roche Diagnostics GmbH Sandhofer Straße 116',
    organization_country_code  = 'de',
    substitute_organization_id = 178476
where original_organization_id = 243896;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Deutsche Telekom AG Friedrich-Ebert-Allee 140',
    organization_country_code  = 'de',
    substitute_organization_id = 169740
where original_organization_id = 229850;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Micronas Munich GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 238607;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Thalermaier GmbH Mitterweg 45',
    organization_country_code  = 'at',
    substitute_organization_id = 168851
where original_organization_id = 246886;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'H F INDUSTRIE - TEILE - VERTRIEB GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 232760;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Wincor Nixdorf International GmbH Heinz-Nixdorf-Ring 1',
    organization_country_code  = 'de',
    substitute_organization_id = 164505
where original_organization_id = 249211;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'NOKIA SOLUTIONS AND NETWORKS MANAGEMENT INTERNATIONAL GMBH St. Martin Str. 76 81541 Munich',
    organization_country_code  = 'de',
    substitute_organization_id = 156616
where original_organization_id = 239761;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Wysota, Christof',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 249868;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Noisy Unit GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 239697;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Kubinski, Piotr Hülchratherstrasse 33 50670 Köln',
    organization_country_code  = 'de',
    substitute_organization_id = 170694
where original_organization_id = 236780;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MAX-PANCK-GESELLSCHAFT ZUR FÖRDERUNG DER  WISSENSCHAFTEN E.V.',
    organization_country_code  = 'de',
    substitute_organization_id = 157763
where original_organization_id = 238177;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Henkel-Ecolab GmbH & Co oHG',
    organization_country_code  = 'de',
    substitute_organization_id = 162702
where original_organization_id = 232919;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Kubica, Wladyslaw',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 236768;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'VOEST AG',
    organization_country_code  = 'at',
    substitute_organization_id = 160773
where original_organization_id = 248636;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Berghofer, Emmerich, Dr. Dipl.-Ing. Guggenbergerstrasse 14 A-3021 Pressbaum',
    organization_country_code  = 'at',
    substitute_organization_id = 174604
where original_organization_id = 227040;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TÜV ÖSTERREICH (TECHNISCHER ÜBERWACHUNGS-VEREIN ÖSTERREICH)',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 247596;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Leichtbau-Zentrum Sachsen GmbH Marschnerstrasse 39',
    organization_country_code  = 'de',
    substitute_organization_id = 175519
where original_organization_id = 237161;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Steico AG Hans-Riedl-StraBe 21',
    organization_country_code  = 'de',
    substitute_organization_id = 174777
where original_organization_id = 245851;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BRADEL, Joanna',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 227748;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ESG Elektroniksystem- und Logistik-GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 230879;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'STUMPF, Manfred [DE / DE]  Breitbach 23 D-97516 Oberschwarzach',
    organization_country_code  = 'de',
    substitute_organization_id = 168824
where original_organization_id = 246059;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Ecolab GmbH & CO. oHG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 230390;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BASF PLANT SCIENCE GMBH [DE / DE]  D-67056 Ludwigshafen',
    organization_country_code  = 'de',
    substitute_organization_id = 156620
where original_organization_id = 226850;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'PFLEGER, Beata  [AT / AT]  Hörlgasse 8/1 A-1090 Wien',
    organization_country_code  = 'at',
    substitute_organization_id = 178617
where original_organization_id = 241210;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'NAPOLIN OST-WEST HANDEL GmbH Barkhausenweg 3',
    organization_country_code  = 'de',
    substitute_organization_id = 168375
where original_organization_id = 239340;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BASF PLANT SCIENCE GMBH 67056, LUDWIGSHAFEN',
    organization_country_code  = 'de',
    substitute_organization_id = 156620
where original_organization_id = 226849;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Thymed GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 247081;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Kernforschungsanlage Julich Gesellschaft mit beschrankter Haftung',
    organization_country_code  = 'de',
    substitute_organization_id = 165835
where original_organization_id = 235766;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Heraeus Additive Manufacturing GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 232944;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Prommer Friedbert',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 242580;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HANDLER, Norbert',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 232822;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HENKEL-ECOLAB GMBH & CO OHG Reisholzer Werfestrasse 38-40, D-40589, DUESSELDORF',
    organization_country_code  = 'de',
    substitute_organization_id = 162296
where original_organization_id = 232920;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Universität Bremen',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 247967;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Continental Teves AG & Co. oHG',
    organization_country_code  = 'de',
    substitute_organization_id = 165075
where original_organization_id = 229151;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'PARTEMA GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 240989;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'GE JENBACHER GMBH & CO OG ACHENSEESTRASSE 1-3 TIROL 6200',
    organization_country_code  = 'at',
    substitute_organization_id = 160209
where original_organization_id = 231960;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Zimmer Aktiengesellschaft',
    organization_country_code  = 'de',
    substitute_organization_id = 174093
where original_organization_id = 251234;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BAUMANN-SCHILP, Lucia Maistrasse 8 D-82237 Wörthsee',
    organization_country_code  = 'de',
    substitute_organization_id = 174230
where original_organization_id = 226881;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'CROMA-PHARMA GMBH Industriezeile 15 A-2100',
    organization_country_code  = 'at',
    substitute_organization_id = 160666
where original_organization_id = 229242;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Edelmann, Hans-Joachim',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 230432;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Secarna Pharmaceuticals GmbH & Co. KG Bismarckstrasse 7',
    organization_country_code  = 'de',
    substitute_organization_id = 169322
where original_organization_id = 244612;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER MANNHEIM GMBH 68298 Mannheim',
    organization_country_code  = 'de',
    substitute_organization_id = 158424
where original_organization_id = 227551;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ITT Automotive Europe GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 234976;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Schleining, Gerhard, Dr. Dipl.-Ing. Schiffamtsgasse 9/26 A-1020 Wien',
    organization_country_code  = 'at',
    substitute_organization_id = 166646
where original_organization_id = 244518;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Susilo, Rudy Münstereifelerstrasse 39',
    organization_country_code  = 'de',
    substitute_organization_id = 159529
where original_organization_id = 246133;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Gerhard Rieder GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 232087;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MARLEY SPOON AG Paul-Lincke-Ufer 39/40 10999 Berlin',
    organization_country_code  = 'de',
    substitute_organization_id = 176075
where original_organization_id = 237985;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Federal Institute for Materials  Research and Testing',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 231203;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Tenovis GmbH & Co. KG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 246823;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WOBBEN PROPERTIES GMBH Borsigstraße 26 26607 Aurich',
    organization_country_code  = 'de',
    substitute_organization_id = 176072
where original_organization_id = 249407;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TSCHEULIN-ROTHAL GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = 166235
where original_organization_id = 247514;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BIONTECH AG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 227271;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Fos Messtechnik GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 231532;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Pulverer, Gerhard, Prof. Dr.Dr.h.c. Mohnweg 25 D-50858 Köln',
    organization_country_code  = 'de',
    substitute_organization_id = 174891
where original_organization_id = 243389;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ZIZALA LICHTSYSTEME GMBH Scheibbser Str. 17 A-3250 Wieselburg',
    organization_country_code  = 'at',
    substitute_organization_id = 172721
where original_organization_id = 251249;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITATSKLINIKUM FREIBURG HUGSTETTER STR. 49 79106',
    organization_country_code  = 'de',
    substitute_organization_id = 161534
where original_organization_id = 247779;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Nano Analytik GmbH Ehrenbergstr. 1',
    organization_country_code  = 'de',
    substitute_organization_id = 164621
where original_organization_id = 239282;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Universität Konstanz',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 247985;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'POLYTEX SPORTBELÄGE PRODUKTIONS-GMBH Vinkrather Str. 43 47929 Grefrath',
    organization_country_code  = 'de',
    substitute_organization_id = 163527
where original_organization_id = 242233;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Boart Longyear GmbH & Co. KG Städeweg 18-24',
    organization_country_code  = 'de',
    substitute_organization_id = 174945
where original_organization_id = 227482;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TECHNISCHE UNIVERSITÄT ILMENAU',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 246689;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Justus-Liebig-Universität Giessen',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 235402;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Zimmer Aktiengesellschaft Borsigallee 1',
    organization_country_code  = 'de',
    substitute_organization_id = 166838
where original_organization_id = 251235;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Helm AG  [DE/ DE] Nordkanalstrasse 28, 20097 Hamburg',
    organization_country_code  = 'de',
    substitute_organization_id = 172810
where original_organization_id = 232896;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SCHNORRENBERGER, Frank',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 244548;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Dipl.-Ing. Henryk Bury GmbH Robert-Koch-strasse 1.7',
    organization_country_code  = 'de',
    substitute_organization_id = 160276
where original_organization_id = 229908;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Miljoevern Umwelt-Technik GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 238748;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TSCHEULIN-ROTHAL GMBH Friedrich-Meyer-Strasse 23 79331 Teningen',
    organization_country_code  = 'de',
    substitute_organization_id = 166235
where original_organization_id = 247516;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ALFRED TEVES GMBH  [DE / DE]  Guerickestrasse 7 D-6000 Frankfurt am Main 90',
    organization_country_code  = 'de',
    substitute_organization_id = 174389
where original_organization_id = 225837;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'RUETGERS KUREHA SOLVENTS GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = 159785
where original_organization_id = 244102;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Susilo, Rudy Münstereifelerstrasse 39 50937 Köln',
    organization_country_code  = 'de',
    substitute_organization_id = 159529
where original_organization_id = 246134;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Bundesrepublik Deutschland  vertr. d. d. Bundesministerium für Wirtschaft und Technologie dieses vertr. d. d. Präsidenten der Physikalisch-Technischen Bundesanstalt',
    organization_country_code  = 'de',
    substitute_organization_id = 172701
where original_organization_id = 228040;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ABB AG',
    organization_country_code  = 'de',
    substitute_organization_id = 156421
where original_organization_id = 225058;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Exide Technologies GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 231001;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SYTY, Zdzis&lstrok aw',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 246275;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'STS SPEZIAL-TRANSFORMATOREN-STOCKACH GMBH & CO. KG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 246049;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ARSANIS BIOSCIENCS GMBH',
    organization_country_code  = 'at',
    substitute_organization_id = 158099
where original_organization_id = 226270;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'COOPER STANDARD GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = 172102
where original_organization_id = 229169;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SMS DEMAG AKITENGESELLSCHAFT EDUARD - SCHLOEMANN- STRASSE 4, 40237 DUSSELDORF',
    organization_country_code  = 'de',
    substitute_organization_id = 169705
where original_organization_id = 245245;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Max-Plank-Gesselschaft zur Forderung der Wissenschaften E.V.',
    organization_country_code  = 'de',
    substitute_organization_id = 157763
where original_organization_id = 238203;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Greenbrier Germany GmbH Stenekestrasse 5',
    organization_country_code  = 'de',
    substitute_organization_id = 174664
where original_organization_id = 232519;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Technologiekontor Bremerhaven GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 246696;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Uwatech Umwelt- und Wassertechnik GmbH Maisfeld 12',
    organization_country_code  = 'de',
    substitute_organization_id = 171656
where original_organization_id = 248380;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MSA Auer GmbH Thiemannstrasse 1',
    organization_country_code  = 'de',
    substitute_organization_id = 164498
where original_organization_id = 239110;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Tuhh Technologies GmbH Harburger Schlossstrasse 6-12',
    organization_country_code  = 'de',
    substitute_organization_id = 166894
where original_organization_id = 247541;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'LUZ, Helmut  [DE/ DE] Fasanenweg 26, 31191 Algermissen',
    organization_country_code  = 'de',
    substitute_organization_id = 166319
where original_organization_id = 237643;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'RECTICEL Automobilsysteme GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 243721;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Federal-Mogul Wiesbaden GmbH Stielstrasse 11',
    organization_country_code  = 'de',
    substitute_organization_id = 173615
where original_organization_id = 231212;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Universität des Saarlandes',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 247969;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Byk Gulden Lomberg Chemische Fabrik GmbH Postfach 10 03 10 D-78403 Konstanz',
    organization_country_code  = 'de',
    substitute_organization_id = 168743
where original_organization_id = 228147;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Schade, Horst Dipl.-Ing.',
    organization_country_code  = 'de',
    substitute_organization_id = 168845
where original_organization_id = 244491;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Greenbrier Germany GmbH Stenekestrasse 5 31789 Hameln',
    organization_country_code  = 'de',
    substitute_organization_id = 174664
where original_organization_id = 232520;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'CARL ZEISS MEDITEC AG',
    organization_country_code  = 'de',
    substitute_organization_id = 161904
where original_organization_id = 228292;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BAYER AKTIENGESELLSCHAFT  [DE/ DE] 51368 Leverkusen',
    organization_country_code  = 'de',
    substitute_organization_id = 157919
where original_organization_id = 226900;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'NORMA Germany GmbH Edisonstrasse 4',
    organization_country_code  = 'de',
    substitute_organization_id = 169349
where original_organization_id = 239822;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WITASEK Pflanzenschutz GmbH',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 249344;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Mundigler, Norbert, Dipl.-Ing.',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 239154;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Joanneum Research Forschungsgesellschaft mbH',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 235325;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Tenovis GmbH & Co. KG Kleyerstrasse 94 60362 Frankfurt am Main',
    organization_country_code  = 'de',
    substitute_organization_id = 163751
where original_organization_id = 246824;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Piasczynski Ewa Maria',
    organization_country_code  = 'de',
    substitute_organization_id = 169680
where original_organization_id = 241283;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DEUTSCHE BP AG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 229844;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Dr. Schumacher GmbH Zum Steeger 3',
    organization_country_code  = 'de',
    substitute_organization_id = 177804
where original_organization_id = 230102;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Sander, Oswald',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 244389;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Isringhausen GmbH & Co. KG An der Bega 58',
    organization_country_code  = 'de',
    substitute_organization_id = 179008
where original_organization_id = 234970;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Accumulatorenwerke Hoppecke  Carl Zoellner & Sohn GmbH & Co. KG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 225236;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Steico AG Hans-Riedl-Straße 21',
    organization_country_code  = 'de',
    substitute_organization_id = 174777
where original_organization_id = 245853;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Tscheulin-Rothal GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 247515;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Melsungen AG, B., Braun Carl-Braun-Strasse 1',
    organization_country_code  = 'de',
    substitute_organization_id = 169924
where original_organization_id = 238399;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FORSCHUNGSZENTRUM BORSTEL Parkalle 1-40 23845 Borstel',
    organization_country_code  = 'de',
    substitute_organization_id = 160472
where original_organization_id = 231507;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Luz  Helmut Dipl.-Ing.',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 237642;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITÄT FÜR BODENKULTUR WIEN',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 247972;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'PULVERER, Gerhard  [DE / DE]  Mohnweg 25 D-5000 Köln 40',
    organization_country_code  = 'de',
    substitute_organization_id = 174891
where original_organization_id = 243387;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOMBARDIER PRIMOVE GMBH Eichhornstraße 3 10785 Berlin',
    organization_country_code  = 'de',
    substitute_organization_id = 156020
where original_organization_id = 227602;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'EPCOS AG St.-Martin-Str. 53 81669 München',
    organization_country_code  = 'de',
    substitute_organization_id = 172192
where original_organization_id = 230798;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'PTW-Freiburg Physikalisch-Technische Werkstaetten, Dr. Pychlau GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = 169004
where original_organization_id = 243365;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Diehl Metering GmbH Industriestrasse 13',
    organization_country_code  = 'de',
    substitute_organization_id = 166629
where original_organization_id = 229889;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Isringhausen GmbH & Co. KG An der Bega 58 32657 Lemgo',
    organization_country_code  = 'de',
    substitute_organization_id = 179008
where original_organization_id = 234971;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'atg test systems GmbH & Co. KG Reicholzheim, Zum Schlag 3',
    organization_country_code  = 'de',
    substitute_organization_id = 176712
where original_organization_id = 226363;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Karlsruher Institut für Technologie',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 235624;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Zöller-Kipper GmbH Hans-Zöller-Strasse 50-68',
    organization_country_code  = 'de',
    substitute_organization_id = 178466
where original_organization_id = 251408;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Schleining, Gerhard, Dr. Dipl.-Ing.',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 244517;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'JUSTUS-LIEBIG-UNIVERSITAET GIESSEN Ludwigstrasse 23 35390',
    organization_country_code  = 'de',
    substitute_organization_id = 164113
where original_organization_id = 235400;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'KARLSRUHER INSTITUT FUER TECHNOLOGIE',
    organization_country_code  = 'de',
    substitute_organization_id = 166602
where original_organization_id = 235623;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WILO SE Nortkirchenstrasse 100',
    organization_country_code  = 'de',
    substitute_organization_id = 178198
where original_organization_id = 249205;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SoundCall GmbH Winterfeldtstrasse 21',
    organization_country_code  = 'de',
    substitute_organization_id = 166613
where original_organization_id = 245452;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SMS Siemag Aktiengesellschaft',
    organization_country_code  = 'de',
    substitute_organization_id = 169705
where original_organization_id = 245251;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'GE JENBACHER GMBH & CO. OG ACHENSEESTRASSE 1-3 6200',
    organization_country_code  = 'at',
    substitute_organization_id = 160209
where original_organization_id = 231963;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Lucia Baumann-Schilp',
    organization_country_code  = 'de',
    substitute_organization_id = 174230
where original_organization_id = 237571;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Lisa Dräxlmaier GmbH Landshuter Strasse 100',
    organization_country_code  = 'de',
    substitute_organization_id = 161300
where original_organization_id = 237365;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Peissig  Werner',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 241118;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Universität Gesamthochschule Kassel',
    organization_country_code  = 'de',
    substitute_organization_id = 177194
where original_organization_id = 247973;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FEHLING MEDICAL AG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 231221;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DU PONT DE NEMOURS (DEUTSCHLAND) GMBH Du-Pont-Strasse 1 61343 Bad Homburg v.d.H.',
    organization_country_code  = 'de',
    substitute_organization_id = 170216
where original_organization_id = 230169;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Helmholtz-Zentrum Berlin für Materialien  und Energie GmbH Glienicker Strasse 100',
    organization_country_code  = 'de',
    substitute_organization_id = 160817
where original_organization_id = 232899;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BHS-Sonthofen Maschinen- und Anlagenbau GmbH Hans-Böckler-Strasse 7',
    organization_country_code  = 'de',
    substitute_organization_id = 177189
where original_organization_id = 227099;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Zenz, Helmut, Dr. Dipl.-Ing. Abt Karlgasse 22-24, Siege II/4 A-1180 Wien',
    organization_country_code  = 'at',
    substitute_organization_id = 178356
where original_organization_id = 251132;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Hagleitner Hygiene International GmbH',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 232770;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HOERBIGER AUTOMOTIVE KOMFORSYSTEME GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = 168056
where original_organization_id = 233025;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Lurgi Zimmer Aktiengesellschaft Borsigallee 1',
    organization_country_code  = 'de',
    substitute_organization_id = 166838
where original_organization_id = 237630;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'VISUAL VERTIGO SOFTWARE TECHNOLOGIES GMBH Triester Strasse 340 1230 Wien',
    organization_country_code  = 'at',
    substitute_organization_id = 173699
where original_organization_id = 248589;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Budzinsky + Hör Verwaltungs-GmbH Industriestrasse 15',
    organization_country_code  = 'de',
    substitute_organization_id = 174518
where original_organization_id = 227988;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DU PONT DE NEMOURS (DEUTSCHLAND) GMBH Du-Pont-Strasse 1',
    organization_country_code  = 'de',
    substitute_organization_id = 170216
where original_organization_id = 230168;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'IBM DEUTSCHLAND GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 233290;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Polytex Sportbeläge Produktions-GmbH Vinkrather Strasse 43',
    organization_country_code  = 'de',
    substitute_organization_id = 163527
where original_organization_id = 242234;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Symrise GmbH & Co. KG Mühlenfeldstrasse 1',
    organization_country_code  = 'de',
    substitute_organization_id = 178391
where original_organization_id = 246220;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'NOVOMATIC AG Wiener Strasse 158 A-2352 Gumpoldskirchen',
    organization_country_code  = 'at',
    substitute_organization_id = 166467
where original_organization_id = 239891;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Universität Ulm',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 247987;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BASF COATINGS GMBH Glasuritstr.1 48165 Münster',
    organization_country_code  = 'de',
    substitute_organization_id = 161080
where original_organization_id = 226843;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Convergent Information Technologies GmbH',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 229167;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOSHA GMBH & CO. KG Heidberg 10',
    organization_country_code  = 'de',
    substitute_organization_id = 176438
where original_organization_id = 227719;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Kirchhoff Automotive Deutschland GmbH Am Eckenbach 10-14',
    organization_country_code  = 'de',
    substitute_organization_id = 171767
where original_organization_id = 235855;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FU Berlin Kaiserswertherstrasse 16-18',
    organization_country_code  = 'de',
    substitute_organization_id = 175075
where original_organization_id = 231655;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MURIAS, Arkadiusz, Marek',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 239162;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Byk Gulden Lomberg Chemische Fabrik GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 228145;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Schade, Horst Dipl.-Ing.',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 244492;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SEMPERIT AG HOLDING Modecenterstraße 22 1031 Wien',
    organization_country_code  = 'at',
    substitute_organization_id = 170551
where original_organization_id = 244759;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MUT Aero Engines GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = 156156
where original_organization_id = 239184;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ENDRESS+HAUSER GMBH+CO. KG Hauptstr. 1 79689 Maulburg',
    organization_country_code  = 'de',
    substitute_organization_id = 168997
where original_organization_id = 230717;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MAN Truck & Bus AG Dachauer Straße 667',
    organization_country_code  = 'de',
    substitute_organization_id = 165400
where original_organization_id = 237878;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'LONMARK Deutschland e.V.',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 237459;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'VALLEN SYSTEME GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 248468;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ALBERT-LUDWIGS UNIVERSITY FREIBURG Fahnenbergplatz 79085 Freiburg',
    organization_country_code  = 'de',
    substitute_organization_id = 169604
where original_organization_id = 225797;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'JOHNSON CONTROLS GMBH Industriestrasse 20-30 51399 Burscheid',
    organization_country_code  = 'de',
    substitute_organization_id = 172752
where original_organization_id = 235334;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Klingenburg GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 235917;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Bals Elektrotechnik GmbH & Co. KG Burgweg 22',
    organization_country_code  = 'de',
    substitute_organization_id = 177340
where original_organization_id = 226727;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Armacell Enterprise GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = 157883
where original_organization_id = 226240;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Henkel KGaA',
    organization_country_code  = 'de',
    substitute_organization_id = 159223
where original_organization_id = 232915;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MOHR, Mirko',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 238913;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MUCOS Pharma GmbH & Co. Malvenweg 2',
    organization_country_code  = 'de',
    substitute_organization_id = 176011
where original_organization_id = 239138;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SMS group GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 245248;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Siemag Tecberg GmbH Kalteiche-Ring 28-32',
    organization_country_code  = 'de',
    substitute_organization_id = 174411
where original_organization_id = 244878;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Technische Hochschule Zittau',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 246682;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'LEITGEB, Rainer',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 237165;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'PTW-Freiburg Physikalisch-Technische Werkstätten Dr. Pychlau GmbH Lörracher Strasse 7 79115 Freiburg',
    organization_country_code  = 'de',
    substitute_organization_id = 169004
where original_organization_id = 243368;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BYK GULDEN LOMBERG CHEM FAB',
    organization_country_code  = 'de',
    substitute_organization_id = 168743
where original_organization_id = 228143;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MILJOEVERN UMWELT TECHNIK ANLA',
    organization_country_code  = 'de',
    substitute_organization_id = 164300
where original_organization_id = 238747;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Wader-Wittis GmbH Justus-von-Liebig-Strasse 3',
    organization_country_code  = 'de',
    substitute_organization_id = 159634
where original_organization_id = 248751;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'I Tech OHG Kantstrasse 34',
    organization_country_code  = 'de',
    substitute_organization_id = 173187
where original_organization_id = 233278;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'AMO GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 225985;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Universität Karlsruhe',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 247980;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Green Life Berlin GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 232513;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BITOP AG  Stockumer Strasse 28, D-58453 Witten',
    organization_country_code  = 'de',
    substitute_organization_id = 168440
where original_organization_id = 227336;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Technische Universität Dresden  O-8027 Dresden',
    organization_country_code  = 'de',
    substitute_organization_id = 161466
where original_organization_id = 246684;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'LONMARK Deutschland e.V. Theaterstrasse 74 52062 Aachen',
    organization_country_code  = 'de',
    substitute_organization_id = 169020
where original_organization_id = 237460;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'CSL Behring GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 229258;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ARSANIS BIOSCIENCES GMBH Helmut-Qualtinger-Gasse 2 1030 Vienna',
    organization_country_code  = 'at',
    substitute_organization_id = 158099
where original_organization_id = 226266;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HENKEL KOMMANDITGESELLSCHAFT AUF AKTIEN  [DE / DE]  Henkelstrasse 67 D-4000 Düsseldorf 13',
    organization_country_code  = 'de',
    substitute_organization_id = 159223
where original_organization_id = 232916;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Liquidsun AG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 237355;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SKW Trostberg Aktiengesellschaft',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 245111;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'VAREX MEDIZINTECHNIK UND PHARMA GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 248479;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER INGELHEIM PHARMA KG BINGER STRASSE 173, D-55216 INGELHEIM AM RHEIN',
    organization_country_code  = 'de',
    substitute_organization_id = 171559
where original_organization_id = 227546;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Bruker AXS GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = 167789
where original_organization_id = 227850;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Kubinski, Piotr Hülchratherstrasse 33',
    organization_country_code  = 'de',
    substitute_organization_id = 170694
where original_organization_id = 236779;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'TETRA GMBH Herrenteich 78 49324 Melle',
    organization_country_code  = 'de',
    substitute_organization_id = 168146
where original_organization_id = 246869;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Blaes, Arne',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 227407;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Michal Urbanowicz und Wieslaw Klein GbR',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 238561;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Renz, Harald, Prof. Dr. med.',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 243795;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Auf der Heide, Christian Bühlwiese 3',
    organization_country_code  = 'de',
    substitute_organization_id = 164809
where original_organization_id = 226408;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Szekeres, Thomas',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 246347;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DSM Chemie Linz GmbH',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 230156;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HALFEN GmbH Liebigstrasse 14',
    organization_country_code  = 'de',
    substitute_organization_id = 166049
where original_organization_id = 232794;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BCS Automotive Interface Solutions GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 226920;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HAMACHER LEUCHTEN GMBH Westerholter Strasse 791 D-45701 Herten',
    organization_country_code  = 'de',
    substitute_organization_id = 163479
where original_organization_id = 232805;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Tetra GmbH Herrenteich 78',
    organization_country_code  = 'de',
    substitute_organization_id = 168146
where original_organization_id = 246868;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FISCHER, Eberhard',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 231400;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSIT TSKLINIKUM FREIBURG',
    organization_country_code  = 'de',
    substitute_organization_id = 161534
where original_organization_id = 247760;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'KIRCHHOFF AUTOMOTIVE DEUTSCHLAND GMBH Am Eckenbach 10-14 57439',
    organization_country_code  = 'de',
    substitute_organization_id = 171767
where original_organization_id = 235856;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Semperit AG Holding Modecenterstraße 22',
    organization_country_code  = 'at',
    substitute_organization_id = 170551
where original_organization_id = 244758;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Miljovern Umwelt-Technik Anlagen-Service GmbH In der Marpe 16',
    organization_country_code  = 'de',
    substitute_organization_id = 164300
where original_organization_id = 238749;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'KLINGENBURG GMBH Boystraße 115 45968 Gladbeck',
    organization_country_code  = 'de',
    substitute_organization_id = 169373
where original_organization_id = 235919;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'JOHANNES GUTENBERG-UNIVERSITÄT MAINZ Saarstrasse 21 55122 Mainz',
    organization_country_code  = 'de',
    substitute_organization_id = 172839
where original_organization_id = 235329;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MELSUNGEN AG, B., Braun  Carl-Braun-Strasse 1, 34212 Melsungen',
    organization_country_code  = 'de',
    substitute_organization_id = 169924
where original_organization_id = 238398;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Novomatic AG Wiener Strasse 158',
    organization_country_code  = 'at',
    substitute_organization_id = 166467
where original_organization_id = 239889;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WESTFALIA SEPARATOR AKTIENGESELLSCHAFT [DE / DE]  Werner-Habig-Strasse 1 D-59302 Oelde',
    organization_country_code  = 'de',
    substitute_organization_id = 172920
where original_organization_id = 249056;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MEC CONTAINER SAFETY SYSTEMS GMBH ; MEC',
    organization_country_code  = 'de',
    substitute_organization_id = 158603
where original_organization_id = 238285;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SMS DEMAG AG Eduard-Schloemann-Strasse 4, 40237, DUSSELDORF',
    organization_country_code  = 'de',
    substitute_organization_id = 169705
where original_organization_id = 245244;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HOERBIGER AUTOMOTIVE KOMFORTSYSTEME GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = 168056
where original_organization_id = 233026;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MEIBES HOLDING GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 238380;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Cooper Standard GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 229170;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HENKEL-ECOLAB GmbH & CO. OHG Reisholzer Werftstrasse 38-42',
    organization_country_code  = 'de',
    substitute_organization_id = 162296
where original_organization_id = 232923;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'EPCOS AG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 230797;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Structrepair GmbH Am Dürrbachgraben 1',
    organization_country_code  = 'de',
    substitute_organization_id = 164987
where original_organization_id = 246026;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Bruker AXS GmbH Östliche Rheinbrückenstrasse 50',
    organization_country_code  = 'de',
    substitute_organization_id = 167789
where original_organization_id = 227849;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Morcinek Roman Haussmannstrasse 65',
    organization_country_code  = 'de',
    substitute_organization_id = 171230
where original_organization_id = 238980;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'JOTEC GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 235343;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FEHLING MEDICAL AG Frankenstrasse 21 D-63791 Karlstein',
    organization_country_code  = 'de',
    substitute_organization_id = 167238
where original_organization_id = 231222;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SMS Demag AG',
    organization_country_code  = 'de',
    substitute_organization_id = 169705
where original_organization_id = 245239;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITAET KARLSRUHE  [DE/ DE] Institut for Experimental Kernphysik, Postfach 6980, 76128 Karlsruhe',
    organization_country_code  = 'de',
    substitute_organization_id = 170135
where original_organization_id = 247765;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SCHAEFFLER TECHNOLOGIES AG & CO. KG Industriestraße 1-3 91074 Herzogenaurach',
    organization_country_code  = 'de',
    substitute_organization_id = 172872
where original_organization_id = 244500;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Zimmer Aktiengesellschaft Borsigallee 1 D-60388 Frankfurt',
    organization_country_code  = 'de',
    substitute_organization_id = 166838
where original_organization_id = 251236;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Gauff, Uwe',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 231853;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Zizala Lichtsysteme GmbH',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 251250;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Gossler Envitec GmbH Borsigstrasse 4-6',
    organization_country_code  = 'de',
    substitute_organization_id = 175191
where original_organization_id = 232431;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Johnson Controls GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 235333;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Helm AG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 232897;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Schaeffler Technologies AG & Co. KG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 244499;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'EXIDE TECHNOLOGIES GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = 164680
where original_organization_id = 231000;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'NOCON Jacek  An der Ochsenkuhle 25, 40699 Erkhart',
    organization_country_code  = 'de',
    substitute_organization_id = 159108
where original_organization_id = 239677;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Polo Industrie GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 242034;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Ludwig-Maximilians-Universität München Geschwister-Scholl-Platz 1',
    organization_country_code  = 'de',
    substitute_organization_id = 168864
where original_organization_id = 237596;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITÄTSKLINIKUM FREIBURG  Hugstetter Str. 49',
    organization_country_code  = 'de',
    substitute_organization_id = 161534
where original_organization_id = 247989;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Luz, Helmut Fasanenweg 26',
    organization_country_code  = 'de',
    substitute_organization_id = 166319
where original_organization_id = 237644;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SHI, Jianmin, M., Sc.',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 244840;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'AUSTRIAMICROSYSTEMS AG  Schloss Premstätten, A-8141 Unterpremstätten',
    organization_country_code  = 'at',
    substitute_organization_id = 178298
where original_organization_id = 226417;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ABP Induction Systems GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 225219;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'F-H-S International GmbH & Co. KG An der Eickesmühle 35',
    organization_country_code  = 'de',
    substitute_organization_id = 174932
where original_organization_id = 231013;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FU BERLIN  Kaiserswertherstrasse 16-18, 14195 Berlin',
    organization_country_code  = 'de',
    substitute_organization_id = 175075
where original_organization_id = 231654;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WOLF, Gabi',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 249619;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HAMACHER LEUCHTEN GMBH Westerholter Strasse 791 / D-4352 Herten',
    organization_country_code  = 'de',
    substitute_organization_id = 163479
where original_organization_id = 232804;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITÄT LINZ',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 247986;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Innio Jenbacher GmbH & Co OG Achenseestrasse 1-3',
    organization_country_code  = 'at',
    substitute_organization_id = 160209
where original_organization_id = 233533;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MSA AUER GMBH Thiemannstr. 1 12059 Berlin',
    organization_country_code  = 'de',
    substitute_organization_id = 164498
where original_organization_id = 239109;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Susilo, Rudy, Dr. Peterstrasse 14 a 50999 Köln',
    organization_country_code  = 'de',
    substitute_organization_id = 159529
where original_organization_id = 246135;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Knorr-Bremse Gesellschaft mit beschränkter Haftung Beethovengasse 43-45',
    organization_country_code  = 'at',
    substitute_organization_id = 172808
where original_organization_id = 235958;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WITASEK PFLANZENSCHUZ GMBH Mozartstrasse 1 a A-9560 Feldkirchen',
    organization_country_code  = 'at',
    substitute_organization_id = 165693
where original_organization_id = 249345;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MASTER-SPORT-AUTOMOBILTECHNIK (MS) GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 238049;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Berghofer, Emmerich, Dr. Dipl.-Ing.',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 227039;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ACCUMULATORENWERKE HOPPECKE CARL ZOELLNER & SOHN GMBH & CO. KG  [DE/ DE] Bontkirchener Strasse 2, 59929 Brilon',
    organization_country_code  = 'de',
    substitute_organization_id = 166262
where original_organization_id = 225237;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Dr. Scheller Cosmetics AG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 230093;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'LIQUIDSUN AG',
    organization_country_code  = 'de',
    substitute_organization_id = 170473
where original_organization_id = 237354;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITAET LINZ Altenberger Strasse 69 A-4040 Linz',
    organization_country_code  = 'at',
    substitute_organization_id = 174130
where original_organization_id = 247768;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UWATECH Membran- Umwelt- und Wassertechnik GmbH Bruchweg 30 29313 Hambühren',
    organization_country_code  = 'de',
    substitute_organization_id = 171656
where original_organization_id = 248378;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SZEKERES, Thomas  [AT/ AT] Gugitzgasse 8/41, A-1190 Wien',
    organization_country_code  = 'at',
    substitute_organization_id = 171018
where original_organization_id = 246346;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'IVT - Industrie Vertrieb Technik GmbH & Co. KG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 234985;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Steico AG Hans-Riedl-Strasse 21 85622 Feldkirchen',
    organization_country_code  = 'de',
    substitute_organization_id = 174777
where original_organization_id = 245852;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Keull, Christoph Dieselstrasse 95',
    organization_country_code  = 'de',
    substitute_organization_id = 178742
where original_organization_id = 235783;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Szekeres, Thomas Schottengasse 3A/1/25',
    organization_country_code  = 'at',
    substitute_organization_id = 171018
where original_organization_id = 246348;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ERKER, Thomas',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 230846;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Boehringer Ingelheim Pharma KG',
    organization_country_code  = 'de',
    substitute_organization_id = 171559
where original_organization_id = 227540;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'MTU AERO ENGINES GMBH Dachaeur Strasse 665 80995',
    organization_country_code  = 'de',
    substitute_organization_id = 156156
where original_organization_id = 239128;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Intertractor Aktiengesellschaft',
    organization_country_code  = 'de',
    substitute_organization_id = 159586
where original_organization_id = 234870;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Ampler, Klaus Norderneyer Weg 148',
    organization_country_code  = 'de',
    substitute_organization_id = 165723
where original_organization_id = 225991;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Nokia Solutions and Networks Management  International GmbH St. Martin Str. 76',
    organization_country_code  = 'de',
    substitute_organization_id = 156616
where original_organization_id = 239759;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'PWA Geräte- und Apparatebau GmbH Köhler Strasse 41-43 46286 Dorsten',
    organization_country_code  = 'de',
    substitute_organization_id = 175801
where original_organization_id = 243415;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BRAUN MELSUNGEN AG',
    organization_country_code  = 'de',
    substitute_organization_id = 169924
where original_organization_id = 227772;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Nokia Solutions and Networks Management International GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = 156616
where original_organization_id = 239760;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'CONTINENTAL AUTOMOTIVE GMBH Vahrenwalder Straße 9 30165 Hannover',
    organization_country_code  = 'de',
    substitute_organization_id = 175584
where original_organization_id = 229145;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'PWA Geräte- und Apparatebau GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 243414;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'WITASEK PFLANZENSCHUTZ GMBH Mozartstrasse 1 a A-9560 Feldkirchen',
    organization_country_code  = 'at',
    substitute_organization_id = 165693
where original_organization_id = 249343;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BioNTech AG An der Goldgrube 12',
    organization_country_code  = 'de',
    substitute_organization_id = 163815
where original_organization_id = 227267;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'INA WÄLZLAGER SCHAEFFLER OHG',
    organization_country_code  = 'de',
    substitute_organization_id = 168304
where original_organization_id = 233445;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'JÄGER, Walter',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 235410;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ALTANA ELECTRICAL INSULATION GMBH Abelstrasse 45 46483',
    organization_country_code  = 'de',
    substitute_organization_id = 163073
where original_organization_id = 225906;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DU PONT DE NEMOURS (DEUTSCHLAND) GMBH [DE / DE]  Du-Pont-Strasse 1 D-61343 Bad Homburg',
    organization_country_code  = 'de',
    substitute_organization_id = 170216
where original_organization_id = 230167;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SILTRONIC AG Hanns-Seidel-Platz 4 81737 München',
    organization_country_code  = 'de',
    substitute_organization_id = 158882
where original_organization_id = 244962;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HELMHOLTZ ZENTRUM BERLIN FÜR MATERIALIEN UND ENERGIE GMBH  Glienicker Strasse 100, 14109 Berlin',
    organization_country_code  = 'de',
    substitute_organization_id = 160817
where original_organization_id = 232898;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Freie Universität Berlin',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 231614;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'NOVOMATIC AG Wiener Strasse 158 A-2352',
    organization_country_code  = 'at',
    substitute_organization_id = 166467
where original_organization_id = 239890;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FREIE UNIVERSITAET BERLIN Thielallee 63 14195',
    organization_country_code  = 'de',
    substitute_organization_id = 176184
where original_organization_id = 231611;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ifm electronic gmbh Friedrichstrasse 1',
    organization_country_code  = 'de',
    substitute_organization_id = 157418
where original_organization_id = 233359;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Universitaet Kassel',
    organization_country_code  = 'de',
    substitute_organization_id = 177194
where original_organization_id = 247767;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BSH Bosch und Siemens Hausgeraete GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = 155913
where original_organization_id = 227891;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BYK GULDEN LOMBERG CHEMISCHE FABRIK GMBH Konstanz',
    organization_country_code  = 'de',
    substitute_organization_id = 168743
where original_organization_id = 228146;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'KNAUF GIPS KG Am Bahnhof 7 97346',
    organization_country_code  = 'de',
    substitute_organization_id = 160384
where original_organization_id = 235955;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ATG TEST SYSTEMS GMBH & CO. KG  [DE/ DE] Reicholzheim, Zum Schlag 3, 97877 Wertheim',
    organization_country_code  = 'de',
    substitute_organization_id = 176712
where original_organization_id = 226362;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'JOTEC GMBH Lotzenaecker 23 72379 Hechingen',
    organization_country_code  = 'de',
    substitute_organization_id = 172313
where original_organization_id = 235342;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Von Mutius, Erika, Prof. Dr. med.',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 248669;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Dosch, Günther',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 230066;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Technisat Digital Gmbh',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 246680;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'CHEMIE LINZ GMBH St. Peter-Straße 25 A-4021',
    organization_country_code  = 'at',
    substitute_organization_id = 170962
where original_organization_id = 228744;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'FEV MOTORENTECHNIK GMBH  Neuenhofstrasse 181, 52078 Aachen',
    organization_country_code  = 'de',
    substitute_organization_id = 161747
where original_organization_id = 231285;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Rangelow, Ivo W.',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 243630;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Messer Austria GmbH Industriestrasse 5',
    organization_country_code  = 'at',
    substitute_organization_id = 159692
where original_organization_id = 238478;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Lucas Automotive GmbH Carl-Spaeter-Strasse 8',
    organization_country_code  = 'de',
    substitute_organization_id = 174555
where original_organization_id = 237566;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Gigaset Communications GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 232135;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITÄTSKLINIKUM FREIBURG Hugstetter Str. 49 79106 Freiburg',
    organization_country_code  = 'de',
    substitute_organization_id = 161534
where original_organization_id = 247990;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'SCHAEFER KALK GMBH & CO. KG Louise-Seher-Str. 6 65582',
    organization_country_code  = 'de',
    substitute_organization_id = 159948
where original_organization_id = 244495;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'AMPLER, Klaus  [DE/ DE] Norderneyer Weg 148, 04157 Leipzig',
    organization_country_code  = 'de',
    substitute_organization_id = 165723
where original_organization_id = 225990;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'GESELLSCHAFT FUER SCHWERIONENFORSCHUNG MBH  [DE/ DE] Planckstrasse 1, 64291 Darmstadt',
    organization_country_code  = 'de',
    substitute_organization_id = 176226
where original_organization_id = 232092;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BHS-SONTHOFEN MASCHINEN- UND ANLAGENBAU GMBH  [DE/ DE] Hans-Böckler-Strasse 7, 87527 Sonthofen',
    organization_country_code  = 'de',
    substitute_organization_id = 177189
where original_organization_id = 227098;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Dipl.-Ing. Henryk Bury GmbH',
    organization_country_code  = 'de',
    substitute_organization_id = 160276
where original_organization_id = 229907;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOMBARDIER TRANSPORATION GMBH',
    organization_country_code  = 'de',
    substitute_organization_id = 155870
where original_organization_id = 227612;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ACCUMULATORENWERKE HOPPECKE  [DE/ DE] Carl Zoellner & Sohn GmbH & Co. KG, Bontkirchener Strasse 2, 59929 Brilon',
    organization_country_code  = 'de',
    substitute_organization_id = 166262
where original_organization_id = 225235;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'KEULL, Christoph  Dieselstrasse 95, 42719 Solingen',
    organization_country_code  = 'de',
    substitute_organization_id = 178742
where original_organization_id = 235782;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Bauer Josef',
    organization_country_code  = 'de',
    substitute_organization_id = 159190
where original_organization_id = 226872;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BOEHRINGER INGELHEIM INTERNATIONAL GMBH BINGER STRASSE 173, D-55216 INGELHEIM',
    organization_country_code  = 'de',
    substitute_organization_id = 159495
where original_organization_id = 227533;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'ASK Industries GmbH Hauptstrasse 73',
    organization_country_code  = 'de',
    substitute_organization_id = 165199
where original_organization_id = 226315;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'IBM DEUTSCHLAND GMBH Patentwesen und Urheberrecht Postfach 71137 Ehningen',
    organization_country_code  = 'de',
    substitute_organization_id = 168393
where original_organization_id = 233292;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'BAUMANN-SCHILP, Lucia Maistrasse 8',
    organization_country_code  = 'de',
    substitute_organization_id = 174230
where original_organization_id = 226880;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Universitat Freiburg',
    organization_country_code  = 'de',
    substitute_organization_id = 169604
where original_organization_id = 247773;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Messer Austria GmbH Am Kanal 2',
    organization_country_code  = 'at',
    substitute_organization_id = 159692
where original_organization_id = 238476;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'PAS DEUTSCHLAND GMBH  Wilhelm-Bartelt-Strasse 10-14, 16816 Neuruppin',
    organization_country_code  = 'de',
    substitute_organization_id = 158042
where original_organization_id = 240995;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Topf, Norbert Grüne Hoffnung 9',
    organization_country_code  = 'de',
    substitute_organization_id = 160460
where original_organization_id = 247218;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'GE JENBACHER GMBH AND CO OG ; GE',
    organization_country_code  = 'at',
    substitute_organization_id = 160209
where original_organization_id = 231964;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'UNIVERSITAETSKLINIKUM FREIBURG HUGSTETTER STR. 49 79106',
    organization_country_code  = 'de',
    substitute_organization_id = 176958
where original_organization_id = 247770;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Zenz, Helmut, Dr. Dipl.-Ing.',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 251131;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'Hermann Hemscheidt Maschinenfabrik GmbH & Co. KG',
    organization_country_code  = 'de',
    substitute_organization_id = null
where original_organization_id = 232908;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'PFLEGER, Beata',
    organization_country_code  = 'at',
    substitute_organization_id = null
where original_organization_id = 241211;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'DEGUSSA',
    organization_country_code  = 'de',
    substitute_organization_id = 156826
where original_organization_id = 229712;
UPDATE organization
SET locked                     = true,
    user_update=75,
    date_update='2020-05-12'::date,
    organization_name          = 'HERMANN HEMSCHEID MASCHINENFAB',
    organization_country_code  = 'de',
    substitute_organization_id = 178659
where original_organization_id = 232972;