-- commercialization_category
INSERT INTO commercialization_category (code, name, system_data,
                                        display_order)
VALUES ('spinOff', 'Spin-off', true, 10),
       ('licenseOffer', 'License offer', true, 20),
       ('other', 'Project', true, 30)
;
-- commercialization_status
INSERT INTO commercialization_status_type (code, name, system_data, display_order, use_in_search, use_in_invest)
VALUES ('draft', 'Draft', true, 10, false, false),
       ('input', 'Input', true, 20, false, false),
       ('reviewIdea', 'Review - Idea', false, 30, false, false),
       ('reviewBusiness', 'Review - Business', false, 40, true, false),
       ('investActive', 'Invest - Active', false, 50, true, true),
       ('investNegotiate', 'Invest - Negotiation', false, 60, true, false),
       ('completed', 'Completed', false, 70, true, false),
       ('backlog', 'Backlog', false, 80, false, false)
;
-- commercialization_status_category
INSERT INTO commercialization_status_category(commercialization_status_id, commercialization_category_id)
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (2, 1),
       (2, 2),
       (2, 3),
       (3, 1),
       (3, 2),
       (4, 1),
       (4, 2),
       (5, 1),
       (6, 1),
       (7, 1),
       (7, 2),
       (7, 3),
       (8, 1),
       (8, 2),
       (8, 3)
;
-- commercialization_priority
INSERT INTO commercialization_priority (code, name, system_data, display_order)
VALUES ('softRun', 'Soft Run', true, 10),
       ('run', 'Run', true, 20),
       ('softKill', 'Soft Kill', true, 30),
       ('backlog', 'Backlog', true, 40)
;
-- commercialization_investment_range
INSERT INTO commercialization_investment_range (code, name, system_data,
                                                display_order, investment_from, investment_to)
VALUES ('1M', '<1M CZK', false, 10, null, 1000000),
       ('3M', '1-3M CZK', false, 20, 1000000, 3000000),
       ('3M+', '>3M CZK', false, 30, 3000000, null)
;
-- commercialization_domain
INSERT INTO commercialization_domain (code, name, system_data)
VALUES ('AI', 'Artificial Intelligence', false),
       ('automation', 'Automation', false),
       ('environment', 'Environment', false),
       ('IOT', 'Internet of Things', false),
       ('healthcare', 'Healthcare', false),
       ('engines', 'Engines', false),
       ('software', 'Software', false);