package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.CommercializationTeamMemberEntity;

public interface CommercializationTeamMemberDao {

    void save(CommercializationTeamMemberEntity commercializationTeamMemberEntity);

    CommercializationTeamMemberEntity findForUser(Long userId, Long commercializationProjectId);

    CommercializationTeamMemberEntity findForExpert(Long expertId, Long commercializationProjectId);

}
