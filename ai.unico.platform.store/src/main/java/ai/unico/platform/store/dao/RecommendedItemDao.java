package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.RecommendedItemEntity;

public interface RecommendedItemDao {

    void save(RecommendedItemEntity item);

    RecommendedItemEntity find(String id);

    String[] getRecommendations(String id);

    void extendRecommendation(String id, String[] recomms);
}
