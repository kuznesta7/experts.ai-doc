package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.AskForPermissionsDao;
import ai.unico.platform.store.entity.AskForPermissionsEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class AskForPermissionsDaoImpl implements AskForPermissionsDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(AskForPermissionsEntity askForPermissionsEntity) {
        entityManager.persist(askForPermissionsEntity);
    }
}
