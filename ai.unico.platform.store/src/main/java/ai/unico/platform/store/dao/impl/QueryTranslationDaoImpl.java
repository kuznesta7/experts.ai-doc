package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.QueryTranslationDao;
import ai.unico.platform.store.entity.QueryTranslationEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class QueryTranslationDaoImpl implements QueryTranslationDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public QueryTranslationEntity translate(String query) {
        if (query == null) return null;
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(QueryTranslationEntity.class, "queryTranslation");
        criteria.add(Restrictions.eq("translationPhrase", query.toLowerCase()));
        criteria.add(Restrictions.eq("deleted", false));
        List<QueryTranslationEntity> list = (List<QueryTranslationEntity>) criteria.list();
        if (list.size() == 0) return null;
        return list.get(0);
    }

    @Override
    public List<QueryTranslationEntity> findAllTranslations() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(QueryTranslationEntity.class, "queryTranslation");
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.add(Restrictions.eq("deleted", false));
        return criteria.list();
    }

    @Override
    public void saveTranslations(QueryTranslationEntity translationEntity) {
        entityManager.persist(translationEntity);
    }

    @Override
    public QueryTranslationEntity find(Long id) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(QueryTranslationEntity.class, "queryTranslation");
        criteria.add(Restrictions.eq("translationId", id));
        return (QueryTranslationEntity) criteria.list().get(0);
    }
}
