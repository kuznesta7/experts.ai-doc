package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class CommercializationStatusCategoryEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commercializationStatusCategoryId;

    private boolean deleted;

    @ManyToOne
    @JoinColumn(name = "commercializationStatusId")
    private CommercializationStatusTypeEntity commercializationStatusTypeEntity;

    @ManyToOne
    @JoinColumn(name = "commercializationCategoryId")
    private CommercializationCategoryEntity commercializationCategoryEntity;

    public Long getCommercializationStatusCategoryId() {
        return commercializationStatusCategoryId;
    }

    public void setCommercializationStatusCategoryId(Long commercializationStatusCategoryId) {
        this.commercializationStatusCategoryId = commercializationStatusCategoryId;
    }

    public CommercializationStatusTypeEntity getCommercializationStatusTypeEntity() {
        return commercializationStatusTypeEntity;
    }

    public void setCommercializationStatusTypeEntity(CommercializationStatusTypeEntity commercializationStatusTypeEntity) {
        this.commercializationStatusTypeEntity = commercializationStatusTypeEntity;
    }

    public CommercializationCategoryEntity getCommercializationCategoryEntity() {
        return commercializationCategoryEntity;
    }

    public void setCommercializationCategoryEntity(CommercializationCategoryEntity commercializationCategoryEntity) {
        this.commercializationCategoryEntity = commercializationCategoryEntity;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
