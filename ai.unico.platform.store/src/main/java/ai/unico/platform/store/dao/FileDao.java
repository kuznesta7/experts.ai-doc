package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.FileAccessEntity;
import ai.unico.platform.store.entity.FileEntity;

public interface FileDao {

    void save(FileEntity privateDocumentEntity);

    void save(FileAccessEntity fileAccessEntity);

    FileEntity find(Long fileId);
}
