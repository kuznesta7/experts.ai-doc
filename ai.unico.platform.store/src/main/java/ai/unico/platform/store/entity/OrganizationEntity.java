package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "organization")
public class OrganizationEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long organizationId;

    private Long originalOrganizationId;

    private String organizationName;

    private String organizationAbbrev;

    private String organizationDescription;

    private String registrationNumber;

    private Long rank = 1L;

    private boolean locked;

    private String street;

    private String city;

    private String phone;

    private String email;

    private String web;

    private String recombeePrivateToken;

    private String recombeeDbIdentifier;

    private String recombeeScenario;

    private String recombeePublicToken;

    private Boolean allowedSearch = false;

    private Boolean allowedMembers = false;

    private Boolean isSearchable = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organization_type_id")
    private OrganizationTypeEntity organizationTypeEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organization_country_code")
    private CountryEntity countryEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "substitute_organization_id")
    private OrganizationEntity substituteOrganizationEntity;

    @ManyToOne
    @JoinColumn(name = "organization_logo_file_id")
    private FileEntity organizationLogoEntity;

    @ManyToOne
    @JoinColumn(name = "organization_cover_img_file_id")
    private FileEntity organizationCoverEntity;

    @OneToOne(mappedBy = "organizationEntity")
    private OrganizationProfileVisibilityEntity organizationProfileVisibilityEntity;

    @OneToMany(mappedBy = "organizationEntity")
    private Set<OrganizationUserRoleEntity> organizationUserRoleEntities = new HashSet<>();

    @OneToMany(mappedBy = "organizationEntity")
    private Set<ProjectOrganizationEntity> privateProjectEntities;

    @OneToMany(mappedBy = "organizationEntity")
    private Set<OrganizationLicenceEntity> organizationLicenceEntities = new HashSet<>();

    @OneToMany(mappedBy = "organizationEntity")
    private Set<ItemOrganizationEntity> organizationItemEntities = new HashSet<>();

    @OneToMany(mappedBy = "organizationEntity")
    private Set<UserOrganizationEntity> organizationUserEntities = new HashSet<>();

    @OneToMany(mappedBy = "upperOrganizationEntity")
    private Set<OrganizationStructureEntity> lowerStructureEntities = new HashSet<>();

    @OneToMany(mappedBy = "lowerOrganizationEntity")
    private Set<OrganizationStructureEntity> upperStructureEntities = new HashSet<>();

    @OneToMany(mappedBy = "organizationEntity")
    private Set<SubscriptionOrganizationEntity> subscriptionOrganizationEntities = new HashSet<>();

    public Set<ProjectOrganizationEntity> getPrivateProjectEntities() {
        return privateProjectEntities;
    }

    public void setPrivateProjectEntities(Set<ProjectOrganizationEntity> privateProjectEntities) {
        this.privateProjectEntities = privateProjectEntities;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationDescription() {
        return organizationDescription;
    }

    public void setOrganizationDescription(String organizationDescription) {
        this.organizationDescription = organizationDescription;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Long getOriginalOrganizationId() {
        return originalOrganizationId;
    }

    public void setOriginalOrganizationId(Long originalOrganizationId) {
        this.originalOrganizationId = originalOrganizationId;
    }

    public OrganizationEntity getSubstituteOrganizationEntity() {
        return substituteOrganizationEntity;
    }

    public void setSubstituteOrganizationEntity(OrganizationEntity substituteOrganizationEntity) {
        this.substituteOrganizationEntity = substituteOrganizationEntity;
    }

    public Long getRank() {
        return rank;
    }

    public void setRank(Long rank) {
        this.rank = rank;
    }

    public String getOrganizationAbbrev() {
        return organizationAbbrev;
    }

    public void setOrganizationAbbrev(String organizationAbbrev) {
        this.organizationAbbrev = organizationAbbrev;
    }

    public Set<OrganizationLicenceEntity> getOrganizationLicenceEntities() {
        return organizationLicenceEntities;
    }

    public void setOrganizationLicenceEntities(Set<OrganizationLicenceEntity> organizationLicenceEntities) {
        this.organizationLicenceEntities = organizationLicenceEntities;
    }

    public Set<OrganizationUserRoleEntity> getOrganizationUserRoleEntities() {
        return organizationUserRoleEntities;
    }

    public void setOrganizationUserRoleEntities(Set<OrganizationUserRoleEntity> organizationUserRoleEntities) {
        this.organizationUserRoleEntities = organizationUserRoleEntities;
    }

    public Set<ItemOrganizationEntity> getOrganizationItemEntities() {
        return organizationItemEntities;
    }

    public void setOrganizationItemEntities(Set<ItemOrganizationEntity> organizationItemEntities) {
        this.organizationItemEntities = organizationItemEntities;
    }

    public Set<UserOrganizationEntity> getOrganizationUserEntities() {
        return organizationUserEntities;
    }

    public void setOrganizationUserEntities(Set<UserOrganizationEntity> organizationUserEntities) {
        this.organizationUserEntities = organizationUserEntities;
    }

    public Boolean getAllowedMembers() {
        return allowedMembers;
    }

    public void setAllowedMembers(Boolean membersAllowed) {
        this.allowedMembers = membersAllowed;
    }

    public CountryEntity getCountryEntity() {
        return countryEntity;
    }

    public void setCountryEntity(CountryEntity countryEntity) {
        this.countryEntity = countryEntity;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public Set<OrganizationStructureEntity> getLowerStructureEntities() {
        return lowerStructureEntities;
    }

    public void setLowerStructureEntities(Set<OrganizationStructureEntity> lowerStructureEntities) {
        this.lowerStructureEntities = lowerStructureEntities;
    }

    public Set<OrganizationStructureEntity> getUpperStructureEntities() {
        return upperStructureEntities;
    }

    public void setUpperStructureEntities(Set<OrganizationStructureEntity> upperStructureEntities) {
        this.upperStructureEntities = upperStructureEntities;
    }

    public FileEntity getOrganizationLogoEntity() {
        return organizationLogoEntity;
    }

    public void setOrganizationLogoEntity(FileEntity organizationLogoEntity) {
        this.organizationLogoEntity = organizationLogoEntity;
    }

    public OrganizationProfileVisibilityEntity getOrganizationProfileVisibilityEntity() {
        return organizationProfileVisibilityEntity;
    }

    public void setOrganizationProfileVisibilityEntity(OrganizationProfileVisibilityEntity organizationProfileVisibilityEntity) {
        this.organizationProfileVisibilityEntity = organizationProfileVisibilityEntity;
    }

    public FileEntity getOrganizationCoverEntity() {
        return organizationCoverEntity;
    }

    public void setOrganizationCoverEntity(FileEntity organizationCoverEntity) {
        this.organizationCoverEntity = organizationCoverEntity;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getRecombeePrivateToken() {
        return recombeePrivateToken;
    }

    public String getRecombeeDbIdentifier() {
        return recombeeDbIdentifier;
    }

    public String getRecombeeScenario() {
        return recombeeScenario;
    }

    public String getRecombeePublicToken() {
        return recombeePublicToken;
    }

    public Boolean getAllowedSearch() {
        return allowedSearch;
    }

    public void setAllowedSearch(Boolean allowedSearch) {
        this.allowedSearch = allowedSearch;
    }

    public OrganizationTypeEntity getOrganizationTypeEntity() {
        return organizationTypeEntity;
    }

    public void setOrganizationTypeEntity(OrganizationTypeEntity organizationTypeEntity) {
        this.organizationTypeEntity = organizationTypeEntity;
    }

    public Set<SubscriptionOrganizationEntity> getSubscriptionOrganizationEntities() {
        return subscriptionOrganizationEntities;
    }

    public void setSubscriptionOrganizationEntities(Set<SubscriptionOrganizationEntity> subscriptionOrganizationEntities) {
        this.subscriptionOrganizationEntities = subscriptionOrganizationEntities;
    }

    public Boolean getSearchable() {
        return isSearchable;
    }

    public void setSearchable(Boolean searchable) {
        isSearchable = searchable;
    }
}
