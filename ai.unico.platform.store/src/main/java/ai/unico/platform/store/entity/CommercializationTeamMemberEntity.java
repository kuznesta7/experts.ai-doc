package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class CommercializationTeamMemberEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commercializationTeamMemberId;

    @ManyToOne
    @JoinColumn(name = "commercializationId")
    private CommercializationProjectEntity commercializationProjectEntity;

    @ManyToOne
    @JoinColumn(name = "userProfileId")
    private UserProfileEntity userProfileEntity;

    private Long expertId;

    private boolean teamLeader;

    private boolean deleted;

    public Long getCommercializationTeamMemberId() {
        return commercializationTeamMemberId;
    }

    public void setCommercializationTeamMemberId(Long commercializationTeamMemberId) {
        this.commercializationTeamMemberId = commercializationTeamMemberId;
    }

    public CommercializationProjectEntity getCommercializationProjectEntity() {
        return commercializationProjectEntity;
    }

    public void setCommercializationProjectEntity(CommercializationProjectEntity commercializationProjectEntity) {
        this.commercializationProjectEntity = commercializationProjectEntity;
    }

    public UserProfileEntity getUserProfileEntity() {
        return userProfileEntity;
    }

    public void setUserProfileEntity(UserProfileEntity userProfileEntity) {
        this.userProfileEntity = userProfileEntity;
    }

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public boolean isTeamLeader() {
        return teamLeader;
    }

    public void setTeamLeader(boolean teamLeader) {
        this.teamLeader = teamLeader;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
