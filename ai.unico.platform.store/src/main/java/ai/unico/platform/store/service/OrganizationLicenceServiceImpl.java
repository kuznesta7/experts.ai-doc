package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.OrganizationLicenceDto;
import ai.unico.platform.store.api.service.OrganizationLicenceService;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.dao.OrganizationLicenceDao;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.entity.OrganizationLicenceEntity;
import ai.unico.platform.store.util.OrganizationIndexUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

public class OrganizationLicenceServiceImpl implements OrganizationLicenceService {

    @Autowired
    private OrganizationLicenceDao organizationLicenceDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Override
    @Transactional
    public Long addOrganizationLicence(Long organizationId, OrganizationRole licenceRole, Date validUntil, Integer numberOfUserLicences, UserContext userContext) {
        final OrganizationLicenceEntity organizationLicenceEntity = new OrganizationLicenceEntity();
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
        organizationEntity.setLocked(true);

        if (validUntil != null) {
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(validUntil);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            organizationLicenceEntity.setValidUntil(calendar.getTime());
        }

        organizationLicenceEntity.setOrganizationEntity(organizationEntity);
        organizationLicenceEntity.setLicenceRole(licenceRole);
        organizationLicenceEntity.setNumberOfUserLicences(numberOfUserLicences);
        TrackableUtil.fillCreate(organizationLicenceEntity, userContext);
        organizationLicenceDao.save(organizationLicenceEntity);
        OrganizationIndexUtil.indexOrganization(organizationEntity);
        return organizationLicenceEntity.getOrganizationLicenceId();
    }

    @Override
    public Boolean organizationHasLicense(Long organizationId) {
        return organizationLicenceDao.organizationHasLicense(organizationId);
    }

    @Override
    public Boolean organizationsHasLicenses(Set<Long> organizationIds) {
        for (Long orgId : organizationIds) {
            if (organizationHasLicense(orgId)) {
                return true;
            }
        }
        return false;
    }

    @Override
    @Transactional
    public void invalidateLicence(Long organizationLicenceId, UserContext userContext) {
        final OrganizationLicenceEntity organizationLicenceEntity = organizationLicenceDao.find(organizationLicenceId);
        if (organizationLicenceEntity == null) throw new NonexistentEntityException(OrganizationLicenceEntity.class);
        organizationLicenceEntity.setDeleted(true);
        TrackableUtil.fillUpdate(organizationLicenceEntity, userContext);
        OrganizationIndexUtil.indexOrganization(organizationLicenceEntity.getOrganizationEntity());
    }

    @Override
    @Transactional
    public List<OrganizationLicenceDto> getOrganizationLicences(Long organizationId, boolean validOnly) {
        return getOrganizationLicences(Collections.singleton(organizationId), validOnly);
    }

    @Override
    @Transactional
    public List<OrganizationLicenceDto> getOrganizationLicences(Set<Long> organizationIds, boolean validOnly) {
        final List<OrganizationLicenceDto> organizationLicenceDtos = new ArrayList<>();
        final List<OrganizationLicenceEntity> organizationLicences = organizationLicenceDao.getOrganizationLicences(organizationIds, validOnly);
        for (OrganizationLicenceEntity organizationLicence : organizationLicences) {
            final OrganizationLicenceDto organizationLicenceDto = new OrganizationLicenceDto();
            organizationLicenceDto.setOrganizationLicenceId(organizationLicence.getOrganizationLicenceId());
            organizationLicenceDto.setLicenceRole(organizationLicence.getLicenceRole());
            organizationLicenceDto.setOrganizationId(organizationLicence.getOrganizationEntity().getOrganizationId());
            organizationLicenceDto.setValidUntil(organizationLicence.getValidUntil());
            organizationLicenceDto.setNumberOfUserLicences(organizationLicence.getNumberOfUserLicences());
            organizationLicenceDto.setValid(organizationLicence.getValidUntil() == null || new Date().before(organizationLicence.getValidUntil()));
            organizationLicenceDtos.add(organizationLicenceDto);
        }
        return organizationLicenceDtos;
    }
}
