package ai.unico.platform.store.util;

import ai.unico.platform.search.api.service.ProjectIndexService;
import ai.unico.platform.store.api.dto.ProjectIndexDto;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.util.KeywordsUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.ProjectOrganizationRole;

import java.io.IOException;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class ProjectIndexUtil {

    public static void indexProject(ProjectEntity projectEntity) {
        final ProjectIndexService service = ServiceRegistryUtil.getService(ProjectIndexService.class);
        final ProjectIndexDto projectIndexDto = convert(projectEntity);
        try {
            service.updateProject(projectIndexDto);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ProjectIndexDto convert(ProjectEntity projectEntity) {
        final ProjectIndexDto projectIndexDto = new ProjectIndexDto();
        projectIndexDto.setName(projectEntity.getName());
        projectIndexDto.setDescription(projectEntity.getDescription());
        projectIndexDto.setEndDate(projectEntity.getEndDate());
        projectIndexDto.setStartDate(projectEntity.getStartDate());
        projectIndexDto.setProjectId(projectEntity.getProjectId());
        projectIndexDto.setOriginalProjectId(projectEntity.getOriginalProjectId());
        projectIndexDto.setConfidentiality(projectEntity.getConfidentiality());
        projectIndexDto.setProjectNumber(projectEntity.getProjectNumber());
        projectIndexDto.setOwnerOrganizationId(HibernateUtil.getId(projectEntity, ProjectEntity::getOwnerOrganization, OrganizationEntity::getOrganizationId));
        projectIndexDto.setProjectProgramId(HibernateUtil.getId(projectEntity, ProjectEntity::getProjectProgramEntity, ProjectProgramEntity::getProjectProgramId));

        projectIndexDto.setKeywords(KeywordsUtil.sortKeywordList(projectEntity.getProjectTagEntities().stream()
                .filter(x -> !x.isDeleted())
                .map(ProjectTagEntity::getTagEntity)
                .map(TagEntity::getValue)
                .collect(Collectors.toList())));

        projectIndexDto.setItemIds(projectEntity.getProjectItemEntities().stream()
                .filter(x -> !x.isDeleted())
                .map(x -> HibernateUtil.getId(x, ProjectItemEntity::getItemEntity, ItemEntity::getItemId))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet()));

        projectIndexDto.setOriginalItemIds(projectEntity.getProjectItemEntities().stream()
                .filter(x -> !x.isDeleted())
                .map(ProjectItemEntity::getOriginalItemId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet()));

        final Set<ProjectOrganizationEntity> projectOrganizationEntities = projectEntity.getProjectOrganizationEntities();

        for (UserExpertProjectEntity userExpertProjectEntity : projectEntity.getProjectUserEntities()) {
            if (userExpertProjectEntity.isDeleted()) continue;
            final Long userId = HibernateUtil.getId(userExpertProjectEntity, UserExpertProjectEntity::getUserProfileEntity, UserProfileEntity::getUserId);
            if (userId != null) {
                projectIndexDto.getUserIds().add(userId);
            } else {
                projectIndexDto.getExpertIds().add(userExpertProjectEntity.getExpertId());
            }
        }
        for (ProjectOrganizationEntity projectOrganizationEntity : projectOrganizationEntities) {
            if (projectOrganizationEntity.isDeleted()) continue;
            final OrganizationEntity organizationEntity = projectOrganizationEntity.getOrganizationEntity();
            final Long organizationId = OrganizationUtil.findOrganizationIdAfterSubstitution(organizationEntity);

            if (ProjectOrganizationRole.PARTICIPANT.equals(projectOrganizationEntity.getProjectOrganizationRole())) {
                projectIndexDto.getOrganizationIds().add(organizationId);
            } else if (ProjectOrganizationRole.PROVIDER.equals(projectOrganizationEntity.getProjectOrganizationRole())) {
                projectIndexDto.getProvidingOrganizationIds().add(organizationId);
            }
            if (projectOrganizationEntity.isVerified()) {
                projectIndexDto.getVerifiedOrganizationIds().add(organizationId);
            }
            final ProjectOrganizationEntity parentProjectOrganizationEntity = projectOrganizationEntity.getParentProjectOrganizationEntity();
            if (parentProjectOrganizationEntity != null) {
                final ProjectEntity parentProjectEntity = parentProjectOrganizationEntity.getProjectEntity();
                projectIndexDto.getOrganizationParentProjectIdMap().put(organizationId, parentProjectEntity.getProjectId());
            }
        }

        final Set<ProjectBudgetEntity> projectBudgetEntities = projectEntity.getProjectBudgetEntities();
        for (ProjectBudgetEntity projectBudgetEntity : projectBudgetEntities) {
            if (projectBudgetEntity.isDeleted()) continue;
            final ProjectIndexDto.ProjectOrganizationBudget projectOrganizationBudget = new ProjectIndexDto.ProjectOrganizationBudget();
            projectOrganizationBudget.setOrganizationId(OrganizationUtil.findOrganizationIdAfterSubstitution(projectBudgetEntity.getOrganizationEntity()));
            projectOrganizationBudget.setYear(projectBudgetEntity.getYear());
            projectOrganizationBudget.setTotal(projectBudgetEntity.getTotalAmount());
            projectOrganizationBudget.setNationalSupport(projectBudgetEntity.getNationalSupportAmount());
            projectOrganizationBudget.setPrivateFinances(projectBudgetEntity.getPrivateAmount());
            projectOrganizationBudget.setOtherFinances(projectBudgetEntity.getOtherAmount());
            projectIndexDto.getProjectOrganizationBudgetList().add(projectOrganizationBudget);
        }
        return projectIndexDto;
    }
}
