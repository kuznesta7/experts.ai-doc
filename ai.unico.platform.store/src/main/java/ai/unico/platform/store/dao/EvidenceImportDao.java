package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.EvidenceImportEntity;

public interface EvidenceImportDao {

    void save(EvidenceImportEntity evidenceImportEntity);

    EvidenceImportEntity getEvidenceImportEntity(Long evidenceImportId);

}
