package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ProjectEntity;
import ai.unico.platform.store.entity.ProjectTagEntity;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface ProjectDao {

    void saveProject(ProjectEntity projectEntity);

    ProjectEntity find(Long projectId);

    ProjectEntity findByOriginalProjectId(Long originalProjectId);

    void saveProjectTag(ProjectTagEntity projectTagEntity);

    List<ProjectEntity> findAll(Set<Long> projectIds);

    List<ProjectEntity> findAll(Integer limit, Integer offset);

    List<ProjectEntity> findJoinedByIds(Collection<Long> projectIds);
}
