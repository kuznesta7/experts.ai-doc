package ai.unico.platform.store.util;

import ai.unico.platform.store.api.dto.*;
import ai.unico.platform.store.api.enums.OrganizationRelationType;
import ai.unico.platform.store.entity.CountryEntity;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.entity.OrganizationLicenceEntity;
import ai.unico.platform.store.entity.OrganizationStructureEntity;
import ai.unico.platform.util.dto.OrganizationDto;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

public class OrganizationUtil {


    public static OrganizationIndexDto convertIndex(OrganizationEntity organizationEntity, Map<Long, Long> substitutionMap, Collection<OrganizationStructureEntity> lowerStructure, Collection<OrganizationLicenceEntity> licences) {
        final OrganizationIndexDto organizationDto = new OrganizationIndexDto();
        final Long organizationId = organizationEntity.getOrganizationId();
        final Long substituteOrganizationId = substitutionMap.get(organizationId);

        organizationDto.setOrganizationId(organizationId);
        organizationDto.setOrganizationName(organizationEntity.getOrganizationName());
        organizationDto.setOrganizationAbbrev(organizationEntity.getOrganizationAbbrev());
        organizationDto.setOriginalOrganizationId(organizationEntity.getOriginalOrganizationId());
        organizationDto.setRank(organizationEntity.getRank());
        organizationDto.setAllowedSearch(organizationEntity.getAllowedSearch());
        organizationDto.setSearchable(organizationEntity.getSearchable());
        organizationDto.setCountryCode(HibernateUtil.getId(organizationEntity, OrganizationEntity::getCountryEntity, CountryEntity::getCountryCode));
        organizationDto.setSubstituteOrganizationId(organizationId.equals(substituteOrganizationId) ? null : substituteOrganizationId);
        organizationDto.setAllowedMembers(organizationEntity.getAllowedMembers());
        organizationDto.setOrganizationTypeId(organizationEntity.getOrganizationTypeEntity() == null ? null : organizationEntity.getOrganizationTypeEntity().getId());

        organizationDto.setChildrenOrganizationIds(lowerStructure.stream()
                .filter(x -> x.getRelationType().equals(OrganizationRelationType.PARENT))
                .filter(x -> !x.isDeleted())
                .map(x -> HibernateUtil.getId(x, OrganizationStructureEntity::getLowerOrganizationEntity, OrganizationEntity::getOrganizationId))
                .map(x -> substitutionMap.getOrDefault(x, x))
                .collect(Collectors.toSet()));

        organizationDto.setAffiliatedOrganizationIds(lowerStructure.stream()
                .filter(x -> x.getRelationType().equals(OrganizationRelationType.AFFILIATED))
                .filter(x -> !x.isDeleted())
                .map(x -> HibernateUtil.getId(x, OrganizationStructureEntity::getLowerOrganizationEntity, OrganizationEntity::getOrganizationId))
                .map(x -> substitutionMap.getOrDefault(x, x))
                .collect(Collectors.toSet()));

        organizationDto.setMemberOrganizationIds(lowerStructure.stream()
                .filter(x -> x.getRelationType().equals(OrganizationRelationType.MEMBER))
                .filter(x -> !x.isDeleted())
                .map(x -> HibernateUtil.getId(x, OrganizationStructureEntity::getLowerOrganizationEntity, OrganizationEntity::getOrganizationId))
                .map(x -> substitutionMap.getOrDefault(x, x))
                .collect(Collectors.toSet()));

        for (OrganizationLicenceEntity organizationLicenceEntity : licences) {
            if (organizationLicenceEntity.isDeleted()) continue;
            final OrganizationLicencePreviewDto licenceDto = new OrganizationLicencePreviewDto();
            licenceDto.setOrganizationLicenceId(organizationLicenceEntity.getOrganizationLicenceId());
            licenceDto.setLicenceRole(organizationLicenceEntity.getLicenceRole());
            licenceDto.setValidUntil(organizationLicenceEntity.getValidUntil());
            organizationDto.getOrganizationLicencePreviewDtos().add(licenceDto);
        }
        return organizationDto;
    }

    public static OrganizationDto convert(OrganizationEntity organizationEntity) {
        final OrganizationDto organizationDto = new OrganizationDto();
        organizationDto.setOrganizationId(organizationEntity.getOrganizationId());
        organizationDto.setOrganizationName(organizationEntity.getOrganizationName());
        organizationDto.setOrganizationAbbrev(organizationEntity.getOrganizationAbbrev());
        organizationDto.setGdpr(organizationEntity.getSearchable());
        return organizationDto;
    }

    public static OrganizationBaseDto convertBase(OrganizationEntity organizationEntity) {
        final OrganizationBaseDto organizationDto = new OrganizationBaseDto();
        organizationDto.setOrganizationId(organizationEntity.getOrganizationId());
        organizationDto.setOrganizationName(organizationEntity.getOrganizationName());
        organizationDto.setOrganizationAbbrev(organizationEntity.getOrganizationAbbrev());
        organizationDto.setGdpr(organizationEntity.getSearchable());
        return organizationDto;
    }

    public static OrganizationBaseDto convertBase(OrganizationDetailDto organizationDetailDto) {
        final OrganizationBaseDto organizationDto = new OrganizationBaseDto();
        organizationDto.setOrganizationId(organizationDetailDto.getOrganizationId());
        organizationDto.setOrganizationName(organizationDetailDto.getOrganizationName());
        organizationDto.setOrganizationAbbrev(organizationDetailDto.getOrganizationAbbrev());
        return organizationDto;
    }

    public static OrganizationDetailDto convertToDetail(OrganizationEntity organizationEntity) {
        final OrganizationDetailDto organizationDetailDto = new OrganizationDetailDto();
        organizationDetailDto.setOrganizationId(organizationEntity.getOrganizationId());
        organizationDetailDto.setOrganizationName(organizationEntity.getOrganizationName());
        organizationDetailDto.setOrganizationAbbrev(organizationEntity.getOrganizationAbbrev());
        organizationDetailDto.setOrganizationDescription(organizationEntity.getOrganizationDescription());
        organizationDetailDto.setStreet(organizationEntity.getStreet());
        organizationDetailDto.setCity(organizationEntity.getCity());
        organizationDetailDto.setEmail(organizationEntity.getEmail());
        organizationDetailDto.setPhone(organizationEntity.getPhone());
        organizationDetailDto.setWeb(organizationEntity.getWeb());
        organizationDetailDto.setRecombeeDbIdentifier(organizationEntity.getRecombeeDbIdentifier());
        organizationDetailDto.setRecombeePrivateToken(organizationEntity.getRecombeePrivateToken());
        organizationDetailDto.setRecombeeScenario(organizationEntity.getRecombeeScenario());
        organizationDetailDto.setRecombeePublicToken(organizationEntity.getRecombeePublicToken());
        organizationDetailDto.setGdpr(organizationEntity.getSearchable());
        final CountryEntity countryEntity = organizationEntity.getCountryEntity();
        if (countryEntity != null) {
            organizationDetailDto.setCountryCode(countryEntity.getCountryCode());
            organizationDetailDto.setCountryName(countryEntity.getCountryName());
        }
        return organizationDetailDto;
    }

    public static Long findOrganizationIdAfterSubstitution(OrganizationEntity organizationEntity) {
        if (organizationEntity.getSubstituteOrganizationEntity() == null) return organizationEntity.getOrganizationId();
        return organizationEntity.getSubstituteOrganizationEntity().getOrganizationId();
    }

    public static OrganizationIndexDto convertIndex(OrganizationEntity organizationEntity) {
        return convertIndex(organizationEntity, Collections.emptyMap(), organizationEntity.getLowerStructureEntities(), organizationEntity.getOrganizationLicenceEntities());
    }

    public static RecombeeConnectionDto convertToConnectionDto(OrganizationEntity organizationEntity) {
        final RecombeeConnectionDto dto = new RecombeeConnectionDto();
        dto.setDatabaseName(organizationEntity.getRecombeeDbIdentifier());
        dto.setPrivateToken(organizationEntity.getRecombeePrivateToken());
        dto.setPublicToken(organizationEntity.getRecombeePublicToken());
        return dto;
    }
}
