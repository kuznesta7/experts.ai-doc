package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.UserConsentEntity;

import java.util.List;

public interface UserConsentDao {

    void save(UserConsentEntity userConsentEntity);

    UserConsentEntity findUserConsentEntity(Long userId, Long consentId);

    List<UserConsentEntity> findUserConsentEntities(Long userId);

}
