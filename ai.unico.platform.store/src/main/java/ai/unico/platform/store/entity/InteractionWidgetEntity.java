package ai.unico.platform.store.entity;

import ai.unico.platform.store.api.enums.WidgetType;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
public class InteractionWidgetEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long interactionWidgetId;
    @ManyToOne
    private UserProfileEntity userProfileEntity;
    @ManyToOne
    private OrganizationEntity organization;

    @Enumerated(EnumType.STRING)
    private WidgetType interactionType;


    private Date date = new Timestamp(new Date().getTime());
    private String sessionId;
    private String interactionDetails;

    public Long getInteractionWidgetId() {
        return interactionWidgetId;
    }

    public void setInteractionWidgetId(Long interactionWidgetId) {
        this.interactionWidgetId = interactionWidgetId;
    }

    public UserProfileEntity getUserProfileEntity() {
        return userProfileEntity;
    }

    public void setUserProfileEntity(UserProfileEntity userProfileEntity) {
        this.userProfileEntity = userProfileEntity;
    }

    public OrganizationEntity getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationEntity organization) {
        this.organization = organization;
    }

    public WidgetType getInteractionType() {
        return interactionType;
    }

    public void setInteractionType(WidgetType interactionType) {
        this.interactionType = interactionType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getInteractionDetails() {
        return interactionDetails;
    }

    public void setInteractionDetails(String interactionDetails) {
        this.interactionDetails = interactionDetails;
    }
}
