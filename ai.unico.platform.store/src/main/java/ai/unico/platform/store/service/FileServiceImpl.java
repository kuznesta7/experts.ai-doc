package ai.unico.platform.store.service;

import ai.unico.platform.store.api.service.FileService;
import ai.unico.platform.store.dao.FileDao;
import ai.unico.platform.store.entity.FileAccessEntity;
import ai.unico.platform.store.entity.FileEntity;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

public class FileServiceImpl implements FileService {

    @Autowired
    private FileDao fileDao;

    @Override
    @Transactional
    public FileDto getFile(Long fileId, UserContext userContext) {
        final FileEntity fileEntity = fileDao.find(fileId);
        if (fileEntity == null) return null;

        final FileAccessEntity fileAccessEntity = new FileAccessEntity();
        fileAccessEntity.setAccessDate(new Date());
        fileAccessEntity.setFileEntity(fileEntity);
        fileAccessEntity.setAccessUserId(userContext.getUserId());
        fileDao.save(fileAccessEntity);

        return new FileDto(fileEntity.getContent(), fileEntity.getName());
    }
}
