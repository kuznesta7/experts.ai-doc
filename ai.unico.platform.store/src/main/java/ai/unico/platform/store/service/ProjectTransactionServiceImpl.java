package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.dto.ProjectTransactionDto;
import ai.unico.platform.store.api.dto.ProjectTransactionPreviewDto;
import ai.unico.platform.store.api.service.ProjectTransactionService;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.dao.ProjectDao;
import ai.unico.platform.store.dao.ProjectTransactionDao;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.entity.ProjectEntity;
import ai.unico.platform.store.entity.ProjectTransactionEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.InvalidParameterException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

public class ProjectTransactionServiceImpl implements ProjectTransactionService {

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private ProjectTransactionDao projectTransactionDao;

    @Override
    @Transactional
    public Long addTransaction(Long projectId, ProjectTransactionDto projectTransactionDto, UserContext userContext) {
        final Long organizationFromId = projectTransactionDto.getOrganizationFromId();
        final Long organizationToId = projectTransactionDto.getOrganizationToId();
        if (projectId == null || (organizationFromId == null && organizationToId == null)
                || projectTransactionDto.getAmount() == null || projectTransactionDto.getCurrency() == null)
            throw new InvalidParameterException("Missing parameter while creating project transaction");

        final ProjectTransactionEntity projectTransactionEntity = new ProjectTransactionEntity();
        final ProjectEntity projectEntity = projectDao.find(projectId);
        if (projectEntity == null) throw new NonexistentEntityException(ProjectEntity.class);
        projectTransactionEntity.setProjectEntity(projectEntity);

        if (organizationFromId != null) {
            final OrganizationEntity organizationFromEntity = organizationDao.find(organizationFromId);
            if (organizationFromEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
            projectTransactionEntity.setOrganizationFrom(organizationFromEntity);
        }

        if (organizationToId != null) {
            final OrganizationEntity organizationToEntity = organizationDao.find(organizationToId);
            if (organizationToEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
            projectTransactionEntity.setOrganizationTo(organizationToEntity);
        }

        projectTransactionEntity.setAmount(projectTransactionDto.getAmount());
        projectTransactionEntity.setCurrency(projectTransactionDto.getCurrency());
        projectTransactionEntity.setTransactionDate(projectTransactionDto.getTransactionDate());
        projectTransactionEntity.setTransactionNote(projectTransactionDto.getTransactionNote());
        TrackableUtil.fillCreateTrackable(projectTransactionEntity, userContext.getUserId());
        projectTransactionDao.saveTransaction(projectTransactionEntity);
        return projectTransactionEntity.getProjectTransactionId();
    }

    @Override
    @Transactional
    public void removeTransaction(Long projectId, Long transactionId, UserContext userContext) {
        final ProjectTransactionEntity transaction = projectTransactionDao.findTransaction(transactionId);
        final Long transactionProjectId = transaction.getProjectEntity().getProjectId();
        if (!transactionProjectId.equals(projectId))
            throw new InvalidParameterException("invalid or missing projectId while removing project transaction; received projectId:" + projectId + "transaction projectId:" + transactionProjectId);
        transaction.setDeleted(true);
        TrackableUtil.fillUpdateTrackable(transaction, userContext.getUserId());
    }

    @Override
    @Transactional
    public List<ProjectTransactionPreviewDto> findProjectTransactions(Long projectId) {
        final List<ProjectTransactionEntity> projectTransactions = projectTransactionDao.findProjectTransactions(projectId);
        return projectTransactions.stream().map(this::convert).collect(Collectors.toList());
    }

    private ProjectTransactionPreviewDto convert(ProjectTransactionEntity projectTransactionEntity) {
        final ProjectTransactionPreviewDto projectTransactionDto = new ProjectTransactionPreviewDto();
        final OrganizationEntity organizationFrom = projectTransactionEntity.getOrganizationFrom();
        final OrganizationEntity organizationTo = projectTransactionEntity.getOrganizationTo();

        projectTransactionDto.setTransactionId(projectTransactionEntity.getProjectTransactionId());
        projectTransactionDto.setAmount(projectTransactionEntity.getAmount());
        projectTransactionDto.setCurrency(projectTransactionEntity.getCurrency());
        projectTransactionDto.setProjectId(projectTransactionEntity.getProjectEntity().getProjectId());
        projectTransactionDto.setTransactionDate(projectTransactionEntity.getTransactionDate());
        projectTransactionDto.setTransactionNote(projectTransactionEntity.getTransactionNote());

        if (organizationFrom != null) {
            final OrganizationBaseDto organizationFromDto = new OrganizationBaseDto(organizationFrom.getOrganizationId(), organizationFrom.getOrganizationName());
            projectTransactionDto.setOrganizationFrom(organizationFromDto);
        }
        if (organizationTo != null) {
            final OrganizationBaseDto organizationToDto = new OrganizationBaseDto(organizationTo.getOrganizationId(), organizationTo.getOrganizationName());
            projectTransactionDto.setOrganizationTo(organizationToDto);
        }

        return projectTransactionDto;
    }
}
