package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.PrivateProjectDto;
import ai.unico.platform.store.api.dto.ProjectDto;
import ai.unico.platform.store.api.dto.ProjectIndexDto;
import ai.unico.platform.store.api.dto.ProjectPreviewDto;
import ai.unico.platform.store.api.enums.Currency;
import ai.unico.platform.store.api.service.ProjectService;
import ai.unico.platform.store.dao.*;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.store.util.ProjectIndexUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.KeywordsUtil;
import ai.unico.platform.util.exception.InvalidParameterException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private ProjectOrganizationDao projectOrganizationDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private TagDao tagDao;

    @Autowired
    private ProjectProgramDao projectProgramDao;

    @Autowired
    private ProjectBudgetDao projectBudgetDao;

    @Override
    @Transactional
    public ProjectDto findProject(Long projectId) throws NonexistentEntityException {
        final ProjectEntity projectEntity = projectDao.find(projectId);
        if (projectEntity == null) {
            throw new NonexistentEntityException(ProjectEntity.class);
        }
        return transform(projectEntity);
    }

    @Override
    @Transactional
    public PrivateProjectDto findPrivateProject(Long projectId, Long organizationId) throws NonexistentEntityException, IOException {
        final ProjectOrganizationEntity privateProject = projectOrganizationDao.findProjectOrganization(projectId, organizationId);
        if (privateProject == null || privateProject.isHidden())
            throw new NonexistentEntityException(ProjectOrganizationEntity.class, "Public private project for this organization doesn't exist");
        final PrivateProjectDto privateProjectDto = new PrivateProjectDto();
        privateProjectDto.setOrganizationId(privateProject.getOrganizationEntity().getOrganizationId());

        final ProjectOrganizationEntity parentProjectOrganizationEntity = privateProject.getParentProjectOrganizationEntity();
        if (parentProjectOrganizationEntity != null) {
            privateProjectDto.setParentProjectPreview(transformPreview(parentProjectOrganizationEntity.getProjectEntity()));
        }

        final Set<ProjectOrganizationEntity> subProjectEntities = privateProject.getSubProjectEntities();
        final List<ProjectPreviewDto> subProjectPreviews = subProjectEntities.stream()
                .filter(x -> !x.isDeleted())
                .map(ProjectOrganizationEntity::getProjectEntity)
                .map(this::transformPreview)
                .collect(Collectors.toList());
        privateProjectDto.setSubProjectPreviews(subProjectPreviews);
        return privateProjectDto;
    }

    private ProjectPreviewDto transformPreview(ProjectEntity projectEntity) {
        final ProjectPreviewDto projectPreviewDto = new ProjectPreviewDto();
        projectPreviewDto.setProjectId(projectEntity.getProjectId());
        projectPreviewDto.setName(projectEntity.getName());
        return projectPreviewDto;
    }

    private ProjectDto transform(ProjectEntity projectEntity) {
        final ProjectDto projectDto = new ProjectDto();
        projectDto.setDescription(projectEntity.getDescription());
        projectDto.setName(projectEntity.getName());
        projectDto.setEndDate(projectEntity.getEndDate());
        projectDto.setStartDate(projectEntity.getStartDate());
        projectDto.setProjectId(projectEntity.getProjectId());
        projectDto.setProjectOwnerOrganizationId(HibernateUtil.getId(projectEntity, ProjectEntity::getOwnerOrganization, OrganizationEntity::getOrganizationId));
        projectDto.setConfidentiality(projectEntity.getConfidentiality());
        projectDto.setProjectProgramId(HibernateUtil.getId(projectEntity, ProjectEntity::getProjectProgramEntity, ProjectProgramEntity::getProjectProgramId));
        final Set<ProjectTagEntity> projectTagEntities = projectEntity.getProjectTagEntities();
        final List<String> keywords = projectTagEntities.stream()
                .filter(t -> !t.isDeleted())
                .map(ProjectTagEntity::getTagEntity)
                .map(TagEntity::getValue)
                .collect(Collectors.toList());
        projectDto.setKeywords(KeywordsUtil.sortKeywordList(keywords));
        return projectDto;
    }

    @Override
    @Transactional
    public Long createProject(ProjectDto projectDto, UserContext userContext) {
        final ProjectEntity projectEntity = new ProjectEntity();
        fillEntity(projectEntity, projectDto, userContext);
        final Long projectOwnerOrganizationId = projectDto.getProjectOwnerOrganizationId();
        if (projectOwnerOrganizationId != null) {
            final OrganizationEntity ownerOrganization = organizationDao.find(projectOwnerOrganizationId);
            projectEntity.setOwnerOrganization(ownerOrganization);
        }
        TrackableUtil.fillCreate(projectEntity, userContext);
        projectDao.saveProject(projectEntity);
        return projectEntity.getProjectId();
    }

    @Override
    @Transactional
    public void updateProject(ProjectDto projectDto, UserContext userContext) throws NonexistentEntityException {
        final ProjectEntity projectEntity = projectDao.find(projectDto.getProjectId());
        if (projectEntity == null) {
            throw new NonexistentEntityException(ProjectEntity.class);
        }
        fillEntity(projectEntity, projectDto, userContext);
        TrackableUtil.fillUpdate(projectEntity, userContext);
    }

    private void fillEntity(ProjectEntity projectEntity, ProjectDto projectDto, UserContext userContext) {
        if (projectDto.getEndDate() != null && projectDto.getStartDate() != null && projectDto.getEndDate().before(projectDto.getStartDate()))
            throw new InvalidParameterException("End date of the project is before start date. projectId:" + projectDto.getProjectId());
        projectEntity.setStartDate(projectDto.getStartDate());
        projectEntity.setEndDate(projectDto.getEndDate());
        projectEntity.setName(projectDto.getName());
        projectEntity.setDescription(projectDto.getDescription());
        projectEntity.setConfidentiality(projectDto.getConfidentiality());
        projectEntity.setProjectNumber(projectDto.getProjectNumber());
        final Long projectProgramId = projectDto.getProjectProgramId();
        if (projectProgramId != null) {
            final ProjectProgramEntity projectProgramEntity = projectProgramDao.find(projectProgramId);
            projectEntity.setProjectProgramEntity(projectProgramEntity);
        }
        projectDao.saveProject(projectEntity);
        final List<TagEntity> tagToAddEntities = tagDao.findOrCreateTagsByValues(projectDto.getKeywords());
        final Set<ProjectTagEntity> projectTagEntities = projectEntity.getProjectTagEntities();
        for (ProjectTagEntity projectTagEntity : projectTagEntities) {
            final TagEntity tagEntity = projectTagEntity.getTagEntity();
            if (!tagToAddEntities.contains(tagEntity)) {
                projectTagEntity.setDeleted(true);
            } else {
                tagToAddEntities.remove(tagEntity);
            }
        }
        for (TagEntity tagToAddEntity : tagToAddEntities) {
            final ProjectTagEntity projectTagEntity = new ProjectTagEntity();
            projectTagEntity.setProjectEntity(projectEntity);
            projectTagEntity.setTagEntity(tagToAddEntity);
            projectDao.saveProjectTag(projectTagEntity);
        }
        projectEntity.getProjectBudgetEntities().stream()
                .filter(x -> !x.isDeleted())
                .forEach(projectBudgetEntity -> {
                    projectBudgetEntity.setDeleted(true);
                    TrackableUtil.fillUpdate(projectBudgetEntity, userContext);
                });
        for (Map.Entry<Long, Map<Integer, Double>> organizationYearFinanceMapEntry : projectDto.getOrganizationFinanceMap().entrySet()) {
            for (Map.Entry<Integer, Double> yearFinanceMapEntry : organizationYearFinanceMapEntry.getValue().entrySet()) {
                final Integer year = yearFinanceMapEntry.getKey();
                final Long organizationId = organizationYearFinanceMapEntry.getKey();
                if (yearFinanceMapEntry.getValue() == null || yearFinanceMapEntry.getValue().equals(0D)) continue;

                final ProjectBudgetEntity projectBudgetEntity = new ProjectBudgetEntity();
                TrackableUtil.fillCreate(projectBudgetEntity, userContext);
                projectBudgetEntity.setYear(year);
                projectBudgetEntity.setTotalAmount(yearFinanceMapEntry.getValue());
                projectBudgetEntity.setOrganizationEntity(organizationDao.find(organizationId));
                projectBudgetEntity.setProjectEntity(projectEntity);
                projectBudgetEntity.setCurrency(Currency.CZK);
                TrackableUtil.fillCreate(projectBudgetEntity, userContext);
                projectBudgetDao.save(projectBudgetEntity);
            }
        }
    }

    @Override
    @Transactional
    public void deleteProject(Long projectId, UserContext userContext) throws NonexistentEntityException {
        final ProjectEntity projectEntity = projectDao.find(projectId);
        if (projectEntity == null) {
            throw new NonexistentEntityException(ProjectEntity.class);
        }
        projectEntity.setDeleted(true);
        TrackableUtil.fillUpdate(projectEntity, userContext);
    }

    @Override
    @Transactional
    public void loadAndIndexProject(Long projectId) throws NonexistentEntityException {
        final ProjectEntity projectEntity = projectDao.find(projectId);
        if (projectEntity == null) throw new NonexistentEntityException(ProjectEntity.class);
        ProjectIndexUtil.indexProject(projectEntity);
    }

    @Override
    @Transactional
    public void loadAndIndexProjects(Set<Long> projectToReindexIds) {
        if (projectToReindexIds.isEmpty()) return;
        final List<ProjectEntity> projects = projectDao.findAll(projectToReindexIds);
        for (ProjectEntity project : projects) {
            ProjectIndexUtil.indexProject(project);
        }
    }

    @Override
    @Transactional
    public List<ProjectIndexDto> findAllProjects(Integer limit, Integer offset) {
        final List<ProjectEntity> all = projectDao.findAll(limit, offset);
        return all.stream().map(ProjectIndexUtil::convert).collect(Collectors.toList());
    }
}
