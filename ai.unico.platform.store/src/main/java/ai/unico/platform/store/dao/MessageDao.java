package ai.unico.platform.store.dao;

import ai.unico.platform.store.api.filter.MessageFilter;
import ai.unico.platform.store.entity.UserMessageEntity;

import java.util.List;

public interface MessageDao {

    void saveMessage(UserMessageEntity userMessageEntity);

    UserMessageEntity findMessage(Long messageId);

    List<UserMessageEntity> findMessages(Long userId, MessageFilter messageFilter);

}
