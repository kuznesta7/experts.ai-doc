package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.InteractionIndividualDao;
import ai.unico.platform.store.entity.InteractionIndividualEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class InteractionIndividualDaoImpl implements InteractionIndividualDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(InteractionIndividualEntity interactionIndividualEntity) {
        entityManager.persist(interactionIndividualEntity);
    }
}
