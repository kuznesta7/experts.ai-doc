package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.UserOrganizationEntity;

public interface UserOrganizationDao {

    void saveUserOrganization(UserOrganizationEntity userOrganizationEntity);

    UserOrganizationEntity find(Long organizationId, Long userId);

    UserOrganizationEntity findNotDeleted(Long organizationId, Long userId);

}
