package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.PlatformParamDao;
import ai.unico.platform.store.entity.PlatformParamEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class PlatformParamDaoImpl implements PlatformParamDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public List<PlatformParamEntity> findPlatformParams() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(PlatformParamEntity.class, "platformParam");
        return criteria.list();
    }
}
