package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.CommercializationStatusCategoryEntity;
import ai.unico.platform.store.entity.CommercializationStatusTypeEntity;
import ai.unico.platform.util.enums.CommercializationStatus;

import java.util.List;

public interface CommercializationStatusDao {

    CommercializationStatusTypeEntity find(Long statusTypeId);

    CommercializationStatusTypeEntity find(CommercializationStatus commercializationStatus);

    List<CommercializationStatusTypeEntity> findAll();

    void save(CommercializationStatusTypeEntity commercializationStatusTypeEntity);

    void save(CommercializationStatusCategoryEntity commercializationStatusCategoryEntity);

}
