package ai.unico.platform.store.service;

import ai.unico.platform.extdata.dto.CategoryDwhDto;
import ai.unico.platform.extdata.service.OpportunityCategoryDwhService;
import ai.unico.platform.store.api.dto.CategoryDto;
import ai.unico.platform.store.api.service.OpportunityCategoryService;
import ai.unico.platform.store.dao.OpportunityTypeDao;
import ai.unico.platform.store.entity.OpportunityTypeEntity;
import ai.unico.platform.util.ServiceRegistryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class OpportunityCategoryServiceImpl implements OpportunityCategoryService {

    @Autowired
    OpportunityTypeDao opportunityTypeDao;

    private List<CategoryDto> categories;

    @Override
    @Transactional
    public void updateOpportunityCategoriesFromDWH() {
        final OpportunityCategoryDwhService service = ServiceRegistryUtil.getService(OpportunityCategoryDwhService.class);
        List<CategoryDwhDto> opportunityTypes = service.getOpportunityTypes();
        Set<Long> types = opportunityTypeDao.getAllOpportunityTypes().stream().map(OpportunityTypeEntity::getId).filter(Objects::nonNull).collect(Collectors.toSet());
        List<CategoryDwhDto> newCategories = opportunityTypes.stream().filter(x -> !types.contains(x.getId())).collect(Collectors.toList());
        for (CategoryDwhDto c : newCategories) {
            OpportunityTypeEntity entity = new OpportunityTypeEntity();
            entity.setOriginalId(c.getId());
            entity.setName(c.getName());
            opportunityTypeDao.save(entity);
        }
    }

    @Override
    @Transactional
    public List<CategoryDto> getOpportunityCategories() {
        if (categories == null || categories.isEmpty())
            categories = opportunityTypeDao.getAllOpportunityTypes().stream().map(this::convert).collect(Collectors.toList());
        return categories;
    }

    @Override
    @Transactional
    public CategoryDto getOpportunityCategory(Long id) {
        if (categories == null) {
            getOpportunityCategories();
        }
        return categories.stream().filter(x -> Objects.equals(x.getId(), id)).findFirst().get();
    }

    @Override
    @Transactional
    public Map<Long, Long> translateOpportunities() {
        return opportunityTypeDao.getAllOpportunityTypes().stream()
                .filter(x -> x.getOriginalId() != null)
                .collect(Collectors.toMap(OpportunityTypeEntity::getOriginalId, OpportunityTypeEntity::getId));
    }

    private CategoryDto convert(OpportunityTypeEntity entity) {
        CategoryDto dto = new CategoryDto();
        dto.setName(entity.getName());
        dto.setId(entity.getId());
        return dto;
    }
}
