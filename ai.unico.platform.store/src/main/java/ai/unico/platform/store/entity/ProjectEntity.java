package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;
import ai.unico.platform.util.enums.Confidentiality;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class ProjectEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long projectId;
    private Long originalProjectId;
    private String projectNumber;
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;
    private boolean deleted;

    @ManyToOne
    @JoinColumn(name = "owner_organization_id")
    private OrganizationEntity ownerOrganization;

    @Enumerated(EnumType.STRING)
    private Confidentiality confidentiality;

    @ManyToOne
    private ProjectTypeEntity projectTypeEntity;

    @OneToMany(mappedBy = "projectEntity")
    private Set<ProjectOrganizationEntity> projectOrganizationEntities = new HashSet<>();

    @OneToMany(mappedBy = "projectEntity")
    private Set<ProjectStateEntity> projectStateEntities = new HashSet<>();

    @OneToMany(mappedBy = "projectEntity")
    private Set<ProjectTagEntity> projectTagEntities = new HashSet<>();

    @OneToMany(mappedBy = "projectEntity")
    private Set<ProjectItemEntity> projectItemEntities = new HashSet<>();

    @OneToMany(mappedBy = "projectEntity")
    private Set<UserExpertProjectEntity> projectUserEntities = new HashSet<>();

    @OneToMany(mappedBy = "projectEntity")
    private Set<ProjectTransactionEntity> projectTransactionEntities = new HashSet<>();

    @OneToMany(mappedBy = "projectEntity")
    private Set<ProjectBudgetEntity> projectBudgetEntities = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "country_code")
    private CountryEntity countryEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_program_id")
    private ProjectProgramEntity projectProgramEntity;

    public Set<ProjectItemEntity> getProjectItemEntities() {
        return projectItemEntities;
    }

    public void setProjectItemEntities(Set<ProjectItemEntity> projectItemEntities) {
        this.projectItemEntities = projectItemEntities;
    }

    public Set<UserExpertProjectEntity> getProjectUserEntities() {
        return projectUserEntities;
    }

    public void setProjectUserEntities(Set<UserExpertProjectEntity> projectUserEntities) {
        this.projectUserEntities = projectUserEntities;
    }

    public Set<ProjectStateEntity> getProjectStateEntities() {
        return projectStateEntities;
    }

    public void setProjectStateEntities(Set<ProjectStateEntity> projectStateEntities) {
        this.projectStateEntities = projectStateEntities;
    }

    public Set<ProjectTagEntity> getProjectTagEntities() {
        return projectTagEntities;
    }

    public void setProjectTagEntities(Set<ProjectTagEntity> projectTagEntities) {
        this.projectTagEntities = projectTagEntities;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Set<ProjectOrganizationEntity> getProjectOrganizationEntities() {
        return projectOrganizationEntities;
    }

    public void setProjectOrganizationEntities(Set<ProjectOrganizationEntity> projectOrganizationEntities) {
        this.projectOrganizationEntities = projectOrganizationEntities;
    }

    public ProjectTypeEntity getProjectTypeEntity() {
        return projectTypeEntity;
    }

    public void setProjectTypeEntity(ProjectTypeEntity projectTypeEntity) {
        this.projectTypeEntity = projectTypeEntity;
    }

    public Set<ProjectTransactionEntity> getProjectTransactionEntities() {
        return projectTransactionEntities;
    }

    public void setProjectTransactionEntities(Set<ProjectTransactionEntity> projectTransactionEntities) {
        this.projectTransactionEntities = projectTransactionEntities;
    }

    public CountryEntity getCountryEntity() {
        return countryEntity;
    }

    public void setCountryEntity(CountryEntity countryEntity) {
        this.countryEntity = countryEntity;
    }

    public Long getOriginalProjectId() {
        return originalProjectId;
    }

    public void setOriginalProjectId(Long originalProjectId) {
        this.originalProjectId = originalProjectId;
    }

    public Set<ProjectBudgetEntity> getProjectBudgetEntities() {
        return projectBudgetEntities;
    }

    public void setProjectBudgetEntities(Set<ProjectBudgetEntity> projectBudgetEntities) {
        this.projectBudgetEntities = projectBudgetEntities;
    }

    public ProjectProgramEntity getProjectProgramEntity() {
        return projectProgramEntity;
    }

    public void setProjectProgramEntity(ProjectProgramEntity projectProgramEntity) {
        this.projectProgramEntity = projectProgramEntity;
    }

    public Confidentiality getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Confidentiality confidentiality) {
        this.confidentiality = confidentiality;
    }

    public OrganizationEntity getOwnerOrganization() {
        return ownerOrganization;
    }

    public void setOwnerOrganization(OrganizationEntity ownerOrganization) {
        this.ownerOrganization = ownerOrganization;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }
}
