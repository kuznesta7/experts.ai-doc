package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ProjectDao;
import ai.unico.platform.store.entity.ProjectEntity;
import ai.unico.platform.store.entity.ProjectTagEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class ProjectDaoImpl implements ProjectDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void saveProject(ProjectEntity projectEntity) {
        entityManager.persist(projectEntity);
    }

    @Override
    public ProjectEntity find(Long projectId) {
        final ProjectEntity projectEntity = entityManager.find(ProjectEntity.class, projectId);
        if (projectEntity.isDeleted()) {
            return null;
        } else {
            return projectEntity;
        }
    }

    @Override
    public ProjectEntity findByOriginalProjectId(Long originalProjectId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProjectEntity.class, "project");
        criteria.add(Restrictions.eq("originalProjectId", originalProjectId));
        criteria.add(Restrictions.eq("deleted", true));
        return (ProjectEntity) criteria.uniqueResult();
    }

    @Override
    public void saveProjectTag(ProjectTagEntity projectTagEntity) {
        entityManager.persist(projectTagEntity);
    }

    @Override
    public List<ProjectEntity> findAll(Set<Long> projectIds) {
        if (projectIds.isEmpty()) return Collections.emptyList();
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProjectEntity.class, "project");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.in("projectId", projectIds));
        return criteria.list();
    }

    @Override
    public List<ProjectEntity> findAll(Integer limit, Integer offset) {
        final Criteria idCriteria = hibernateCriteriaCreator.createCriteria(ProjectEntity.class, "project");
        idCriteria.setFirstResult(offset);
        idCriteria.setMaxResults(limit);
        idCriteria.add(Restrictions.eq("project.deleted", false));
        idCriteria.setProjection(Projections.property("projectId"));
        final List<Long> projectIds = idCriteria.list();
        return findJoinedByIds(projectIds);
    }

    @Override
    public List<ProjectEntity> findJoinedByIds(Collection<Long> projectIds) {
        if (projectIds.isEmpty()) return Collections.emptyList();

        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProjectEntity.class, "project");
        criteria.add(Restrictions.in("project.projectId", projectIds));
        criteria.createAlias("project.projectOrganizationEntities", "projectOrganizationEntity", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("project.projectTagEntities", "projectTagEntities", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("project.projectItemEntities", "projectItemEntity", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("project.projectUserEntities", "projectUserEntity", JoinType.LEFT_OUTER_JOIN);

        criteria.setFetchMode("projectOrganizationEntity", FetchMode.JOIN);
        criteria.setFetchMode("projectTagEntities", FetchMode.JOIN);
        criteria.setFetchMode("projectItemEntity", FetchMode.JOIN);
        criteria.setFetchMode("projectUserEntity", FetchMode.JOIN);
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

        return criteria.list();
    }


}
