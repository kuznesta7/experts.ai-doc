package ai.unico.platform.store.service;

import ai.unico.platform.extdata.dto.DWHProjectDto;
import ai.unico.platform.extdata.dto.ProjectFinanceDto;
import ai.unico.platform.extdata.service.DWHProjectService;
import ai.unico.platform.store.api.enums.Currency;
import ai.unico.platform.store.api.service.ProjectVerificationService;
import ai.unico.platform.store.dao.*;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.enums.ProjectOrganizationRole;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProjectVerificationServiceImpl implements ProjectVerificationService {

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private ProjectItemDao projectItemDao;

    @Autowired
    private ProjectOrganizationDao projectOrganizationDao;

    @Autowired
    private ProjectBudgetDao projectBudgetDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private TagDao tagDao;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private ProjectProgramDao projectProgramDao;

    @Override
    @Transactional
    public Long verifyDWHProject(Long originalProjectId, UserContext userContext) {
        final DWHProjectService dwhProjectService = ServiceRegistryUtil.getService(DWHProjectService.class);
        final ProjectEntity existingProjectEntity = projectDao.findByOriginalProjectId(originalProjectId);
        if (existingProjectEntity != null) throw new EntityAlreadyExistsException(ProjectEntity.class);
        final DWHProjectDto dwhProjectDto = dwhProjectService.findProject(originalProjectId);
        final ProjectEntity projectEntity = new ProjectEntity();
        projectEntity.setOriginalProjectId(originalProjectId);
        projectEntity.setDescription(dwhProjectDto.getProjectDescription());
        projectEntity.setName(dwhProjectDto.getProjectName());
        projectEntity.setEndDate(dwhProjectDto.getEndDate());
        projectEntity.setStartDate(dwhProjectDto.getStartDate());
        projectEntity.setProjectNumber(dwhProjectDto.getProjectNumber());
        projectEntity.setConfidentiality(dwhProjectDto.isConfidential() ? Confidentiality.CONFIDENTIAL : Confidentiality.PUBLIC);
        if (dwhProjectDto.getProjectProgramId() != null) {
            final ProjectProgramEntity projectProgramEntity = projectProgramDao.find(dwhProjectDto.getProjectProgramId());
            projectEntity.setProjectProgramEntity(projectProgramEntity);
        }
        projectDao.saveProject(projectEntity);

        final List<TagEntity> tagEntities = tagDao.findOrCreateTagsByValues(dwhProjectDto.getKeywords());
        for (TagEntity tagEntity : tagEntities) {
            final ProjectTagEntity projectTagEntity = new ProjectTagEntity();
            projectTagEntity.setProjectEntity(projectEntity);
            projectTagEntity.setTagEntity(tagEntity);
            projectDao.saveProjectTag(projectTagEntity);
        }

        final Set<Long> organizationToLoadIds = new HashSet<>();
        organizationToLoadIds.addAll(dwhProjectDto.getParticipatingOriginalOrganizationIds());
        organizationToLoadIds.addAll(dwhProjectDto.getProvidingOriginalOrganizationIds());
        organizationToLoadIds.addAll(dwhProjectDto.getProjectFinances().stream().map(ProjectFinanceDto::getOriginalOrganizationId).collect(Collectors.toSet()));
        final List<OrganizationEntity> organizationEntities = organizationDao.findByOriginalIds(organizationToLoadIds);
        for (OrganizationEntity organizationEntity : organizationEntities) {
            final Long originalOrganizationId = organizationEntity.getOriginalOrganizationId();
            final ProjectOrganizationEntity projectOrganizationEntity = new ProjectOrganizationEntity();
            projectOrganizationEntity.setOrganizationEntity(organizationEntity);
            projectOrganizationEntity.setProjectEntity(projectEntity);
            if (dwhProjectDto.getParticipatingOriginalOrganizationIds().contains(originalOrganizationId)) {
                projectOrganizationEntity.setProjectOrganizationRole(ProjectOrganizationRole.PARTICIPANT);
            } else if (dwhProjectDto.getProvidingOriginalOrganizationIds().contains(originalOrganizationId)) {
                projectOrganizationEntity.setProjectOrganizationRole(ProjectOrganizationRole.PROVIDER);
            } else continue;
            projectOrganizationDao.saveProjectOrganization(projectOrganizationEntity);
        }

        final Set<Long> projectOriginalItemIds = dwhProjectDto.getProjectOriginalItemIds();
        final List<ItemEntity> itemsByOriginalIds = itemDao.findItemsByOriginalIds(projectOriginalItemIds);
        final Map<Long, ItemEntity> originalItemIdEntityMap = itemsByOriginalIds.stream().collect(Collectors.toMap(ItemEntity::getOrigItemId, Function.identity()));
        for (Long projectOriginalItemId : projectOriginalItemIds) {
            final ProjectItemEntity projectItemEntity = new ProjectItemEntity();
            projectItemEntity.setProjectEntity(projectEntity);
            if (originalItemIdEntityMap.containsKey(projectOriginalItemId)) {
                projectItemEntity.setItemEntity(originalItemIdEntityMap.get(projectOriginalItemId));
            } else {
                projectItemEntity.setOriginalItemId(projectOriginalItemId);
            }
            projectItemDao.saveProjectItem(projectItemEntity);
        }

        final Map<Long, OrganizationEntity> originalOrganizationIdEntityMap = organizationEntities.stream().collect(Collectors.toMap(OrganizationEntity::getOriginalOrganizationId, Function.identity()));
        for (ProjectFinanceDto projectFinance : dwhProjectDto.getProjectFinances()) {
            if (projectFinance.getOriginalOrganizationId() == null) continue;
            final OrganizationEntity organizationEntity = originalOrganizationIdEntityMap.get(projectFinance.getOriginalOrganizationId());
            final ProjectBudgetEntity projectBudgetEntity = new ProjectBudgetEntity();
            projectBudgetEntity.setProjectEntity(projectEntity);
            projectBudgetEntity.setOrganizationEntity(organizationEntity);
            projectBudgetEntity.setYear(projectFinance.getYear());
            projectBudgetEntity.setTotalAmount(projectFinance.getTotal());
            projectBudgetEntity.setPrivateAmount(projectFinance.getPrivateFinances());
            projectBudgetEntity.setOtherAmount(projectFinance.getOtherFinances());
            projectBudgetEntity.setNationalSupportAmount(projectFinance.getNationalSupport());
            projectBudgetEntity.setCurrency(Currency.CZK);//should be DWH value
            projectBudgetDao.save(projectBudgetEntity);
        }

        return projectEntity.getProjectId();
    }
}
