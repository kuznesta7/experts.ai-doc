package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.OrganizationLicenceEntity;

import java.util.List;
import java.util.Set;

public interface OrganizationLicenceDao {

    List<OrganizationLicenceEntity> getOrganizationLicences(Set<Long> organizationIds, boolean validOnly);

    List<OrganizationLicenceEntity> getOrganizationLicences();

    OrganizationLicenceEntity find(Long organizationLicenceId);

    void save(OrganizationLicenceEntity organizationLicenceEntity);

    Boolean organizationHasLicense(Long organizationId);
}
