package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.CommercializationProjectStatusPreviewDto;
import ai.unico.platform.store.api.dto.CommercializationStatusTypeDto;
import ai.unico.platform.store.api.exception.InvalidCommercializationStatusException;
import ai.unico.platform.store.api.service.CommercializationStatusService;
import ai.unico.platform.store.dao.CommercializationCategoryDao;
import ai.unico.platform.store.dao.CommercializationProjectDao;
import ai.unico.platform.store.dao.CommercializationStatusDao;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.store.entity.superclass.ReferenceData;
import ai.unico.platform.store.util.*;
import ai.unico.platform.util.exception.RuntimeExceptionWithMessage;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CommercializationStatusServiceImpl implements CommercializationStatusService {

    @Autowired
    private CommercializationStatusDao commercializationStatusDao;

    @Autowired
    private CommercializationCategoryDao commercializationCategoryDao;

    @Autowired
    private CommercializationProjectDao commercializationProjectDao;

    @Override
    @Transactional
    public Long createStatus(CommercializationStatusTypeDto commercializationStatusTypeDto, UserContext userContext) {
        final CommercializationStatusTypeEntity commercializationStatusTypeEntity = new CommercializationStatusTypeEntity();
        commercializationStatusTypeEntity.setCode(commercializationStatusTypeDto.getStatusCode());
        fillEntity(commercializationStatusTypeEntity, commercializationStatusTypeDto, userContext);
        TrackableUtil.fillCreate(commercializationStatusTypeEntity, userContext);
        return commercializationStatusTypeEntity.getCommercializationStatusId();
    }

    @Override
    @Transactional
    public void updateStatus(Long statusId, CommercializationStatusTypeDto commercializationStatusTypeDto, UserContext userContext) {
        final CommercializationStatusTypeEntity commercializationStatusTypeEntity = commercializationStatusDao.find(statusId);
        final String code = commercializationStatusTypeDto.getStatusCode();
        if (commercializationStatusTypeEntity.isSystemData() && !code.equals(commercializationStatusTypeEntity.getCode()))
            throw new RuntimeExceptionWithMessage("Status code for system data could not be changed", "Status code could not be changed");
        fillEntity(commercializationStatusTypeEntity, commercializationStatusTypeDto, userContext);
        TrackableUtil.fillUpdate(commercializationStatusTypeEntity, userContext);
    }

    @Override
    @Transactional
    public void setStatus(Long commercializationProjectId, Long statusTypeId, UserContext userContext) {
        final CommercializationStatusTypeEntity newStatusTypeEntity = commercializationStatusDao.find(statusTypeId);
        final CommercializationProjectEntity projectEntity = commercializationProjectDao.find(commercializationProjectId);
        if (!ReferenceDataUtil.validateReferenceData(newStatusTypeEntity))
            throw new InvalidCommercializationStatusException(statusTypeId);
        final boolean mustBeFilled = newStatusTypeEntity.isMustBeFilled();
        ParameterValidationUtil.checkIfRequiredParametersPresent(projectEntity, mustBeFilled ? "allRequired" : "baseRequired");
        final Set<CommercializationProjectStatusEntity> currentStatusEntities = projectEntity.getCommercializationProjectStatusEntities();
        for (CommercializationProjectStatusEntity currentStatusEntity : currentStatusEntities) {
            if (!currentStatusEntity.isOutdated()) {
                currentStatusEntity.setOutdated(true);
                TrackableUtil.fillUpdate(currentStatusEntity, userContext);
            }
        }
        final CommercializationProjectStatusEntity newStatusEntity = new CommercializationProjectStatusEntity();
        newStatusEntity.setCommercializationProjectEntity(projectEntity);
        newStatusEntity.setCommercializationStatusTypeEntity(newStatusTypeEntity);
        TrackableUtil.fillCreate(newStatusEntity, userContext);
        commercializationProjectDao.save(newStatusEntity);

        projectEntity.getCommercializationProjectStatusEntities().add(newStatusEntity);
        CommercializationProjectIndexUtil.index(projectEntity);
    }

    @Override
    @Transactional
    public List<CommercializationStatusTypeDto> findAvailableStatuses(boolean includeInactive) {
        final List<CommercializationStatusTypeEntity> all = commercializationStatusDao.findAll();
        all.sort(Comparator.comparing(ReferenceData::getDisplayOrder, Comparator.nullsLast(Comparator.naturalOrder())));
        final List<CommercializationStatusTypeDto> commercializationStatusTypeDtos = new ArrayList<>();
        for (CommercializationStatusTypeEntity entity : all) {
            if (!includeInactive && !ReferenceDataUtil.validateReferenceData(entity)) continue;
            final CommercializationStatusTypeDto dto = new CommercializationStatusTypeDto();
            dto.setStatusId(entity.getCommercializationStatusId());
            dto.setStatusName(entity.getName());
            dto.setUseInSearch(entity.isUseInSearch());
            dto.setUseInInvest(entity.isUseInInvest());
            dto.setStatusCode(entity.getCode());
            dto.setDeleted(entity.isDeleted());
            dto.setIndex(entity.getDisplayOrder());
            dto.setCanSetOrganization(entity.isCanSetOrganization());
            dto.setMustBeFilled(entity.isMustBeFilled());
            dto.setStatusOrder(entity.getStatusOrder());
            dto.setCategoryIds(entity.getCommercializationStatusCategoryEntities().stream()
                    .map(x -> HibernateUtil.getId(x, CommercializationStatusCategoryEntity::getCommercializationCategoryEntity, CommercializationCategoryEntity::getCommercializationCategoryId))
                    .collect(Collectors.toSet()));
            commercializationStatusTypeDtos.add(dto);
        }
        return commercializationStatusTypeDtos;
    }

    @Override
    @Transactional
    public List<CommercializationProjectStatusPreviewDto> findProjectStatusHistory(Long commercializationProjectId) {
        final List<CommercializationProjectStatusEntity> historyStatuses = commercializationProjectDao.findStatusHistory(commercializationProjectId);
        return historyStatuses.stream().map(this::convert).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Set<Long> findStatusIdsForInvest() {
        final List<CommercializationStatusTypeEntity> all = commercializationStatusDao.findAll();
        return all.stream()
                .filter(CommercializationStatusTypeEntity::isUseInInvest)
                .map(CommercializationStatusTypeEntity::getCommercializationStatusId)
                .collect(Collectors.toSet());
    }

    @Override
    @Transactional
    public Set<Long> findStatusIdsForSearch() {
        final List<CommercializationStatusTypeEntity> all = commercializationStatusDao.findAll();
        return all.stream()
                .filter(CommercializationStatusTypeEntity::isUseInSearch)
                .map(CommercializationStatusTypeEntity::getCommercializationStatusId)
                .collect(Collectors.toSet());
    }

    private CommercializationProjectStatusPreviewDto convert(CommercializationProjectStatusEntity entity) {
        final CommercializationProjectStatusPreviewDto dto = new CommercializationProjectStatusPreviewDto();
        final CommercializationStatusTypeEntity statusEntity = entity.getCommercializationStatusTypeEntity();
        dto.setStatusTypeId(statusEntity.getCommercializationStatusId());
        dto.setStatusName(statusEntity.getName());
        dto.setUserUpdateId(entity.getUserInsert());
        dto.setDateUpdate(entity.getDateInsert());
        dto.setActive(!entity.isOutdated());
        dto.setMustBeFilled(statusEntity.isMustBeFilled());
        dto.setStatusOrder(statusEntity.getStatusOrder());
        return dto;
    }

    private void fillEntity(CommercializationStatusTypeEntity entity, CommercializationStatusTypeDto dto, UserContext userContext) {
        entity.setUseInInvest(dto.isUseInInvest());
        entity.setUseInSearch(dto.isUseInSearch());
        entity.setName(dto.getStatusName());
        entity.setCanSetOrganization(dto.isCanSetOrganization());
        entity.setDeleted(dto.isDeleted());
        entity.setMustBeFilled(dto.isMustBeFilled());
        entity.setStatusOrder(dto.getStatusOrder());
        commercializationStatusDao.save(entity);
        final Set<CommercializationStatusCategoryEntity> currentCategories = entity.getCommercializationStatusCategoryEntities();
        final List<CommercializationCategoryEntity> allCategories = commercializationCategoryDao.findAll();
        final Set<Long> currentCategoryIds = currentCategories.stream()
                .filter(c -> !c.isDeleted())
                .map(CommercializationStatusCategoryEntity::getCommercializationCategoryEntity)
                .map(CommercializationCategoryEntity::getCommercializationCategoryId)
                .collect(Collectors.toSet());
        final Set<Long> categoryToRemoveIds = currentCategoryIds.stream()
                .filter(cid -> !dto.getCategoryIds().contains(cid)).collect(Collectors.toSet());
        final Set<Long> categoryToAddIds = dto.getCategoryIds().stream()
                .filter(cid -> !currentCategoryIds.contains(cid)).collect(Collectors.toSet());
        for (CommercializationStatusCategoryEntity currentCategory : currentCategories) {
            final Long currentCategoryId = currentCategory.getCommercializationCategoryEntity().getCommercializationCategoryId();
            if (categoryToRemoveIds.contains(currentCategoryId)) {
                currentCategory.setDeleted(true);
                TrackableUtil.fillUpdate(currentCategory, userContext);
            }
        }
        for (CommercializationCategoryEntity category : allCategories) {
            if (categoryToAddIds.contains(category.getCommercializationCategoryId())) {
                final CommercializationStatusCategoryEntity statusCategoryEntity = new CommercializationStatusCategoryEntity();
                statusCategoryEntity.setCommercializationCategoryEntity(category);
                statusCategoryEntity.setCommercializationStatusTypeEntity(entity);
                TrackableUtil.fillCreate(statusCategoryEntity, userContext);
                commercializationStatusDao.save(statusCategoryEntity);
            }
        }
    }
}
