package ai.unico.platform.store.service;

import ai.unico.platform.store.api.service.ItemOrganizationService;
import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.store.dao.ItemDao;
import ai.unico.platform.store.dao.ItemOrganizationDao;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.entity.ItemEntity;
import ai.unico.platform.store.entity.ItemOrganizationEntity;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

public class ItemOrganizationServiceImpl implements ItemOrganizationService {

    @Autowired
    private ItemOrganizationDao itemOrganizationDao;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private OrganizationStructureService organizationStructureService;

    @Override
    @Transactional
    public void addItemOrganization(Long organizationId, Set<Long> itemIds, boolean verify, UserContext userContext) throws NonexistentEntityException {
        final List<ItemOrganizationEntity> itemOrganization = itemOrganizationDao.findItemOrganizations(organizationId, itemIds);
        if (!itemOrganization.isEmpty())
            throw new EntityAlreadyExistsException("ITEM_ORGANIZATION_ALREADY_ADDED", "ItemOrganization already exists; organizationId: " + organizationId + ", itemId: " + itemOrganization.get(0).getItemEntity().getItemId() + ", authorId: " + userContext.getUserId());
        final List<ItemEntity> itemEntities = itemDao.find(itemIds);
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        if (itemEntities.isEmpty()) throw new NonexistentEntityException(ItemEntity.class);
        if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
        for (ItemEntity itemEntity : itemEntities) {
            final ItemOrganizationEntity itemOrganizationEntity = new ItemOrganizationEntity();
            itemOrganizationEntity.setVerified(verify);
            itemOrganizationEntity.setItemEntity(itemEntity);
            itemOrganizationEntity.setOrganizationEntity(organizationEntity);
            TrackableUtil.fillCreate(itemOrganizationEntity, userContext);
            itemOrganizationDao.saveItemOrganization(itemOrganizationEntity);
        }
    }

    @Override
    @Transactional
    public void removeItemOrganization(Long organizationId, Long itemId, UserContext userContext) throws NonexistentEntityException {
        final ItemEntity itemEntity = itemDao.find(itemId);
        if (itemEntity == null) throw new NonexistentEntityException(ItemEntity.class);
        final Set<Long> childOrganizationIds = organizationStructureService.findChildOrganizationIds(organizationId, false);
        doRemoveItemOrganization(userContext, itemEntity, childOrganizationIds);
    }

    private void doRemoveItemOrganization(UserContext userContext, ItemEntity itemEntity, Set<Long> childOrganizationIds) {
        for (ItemOrganizationEntity itemOrganizationEntity : itemEntity.getItemOrganizationEntities()) {
            if (itemOrganizationEntity.isDeleted()) continue;
            final OrganizationEntity organizationEntity = itemOrganizationEntity.getOrganizationEntity();
            if (childOrganizationIds.contains(organizationEntity.getOrganizationId())) {
                itemOrganizationEntity.setDeleted(true);
                TrackableUtil.fillUpdate(itemOrganizationEntity, userContext);
            }
        }
    }

    @Override
    @Transactional
    public void removeOrganizationItems(Long organizationId, Set<Long> itemIds, UserContext userContext) throws NonexistentEntityException {
        final List<ItemEntity> itemEntities = itemDao.find(itemIds);
        final Set<Long> childOrganizationIds = organizationStructureService.findChildOrganizationIds(organizationId, false);
        for (ItemEntity itemEntity : itemEntities) {
            doRemoveItemOrganization(userContext, itemEntity, childOrganizationIds);
        }
    }

    @Override
    @Transactional
    public void verifyOrganizationItems(Long organizationId, Set<Long> itemIds, boolean verify, UserContext userContext) {
        final List<ItemEntity> itemEntities = itemDao.find(itemIds);
        final Set<Long> childOrganizationIds = organizationStructureService.findChildOrganizationIds(organizationId, false);
        for (ItemEntity itemEntity : itemEntities) {
            if (itemEntity == null) throw new NonexistentEntityException(ItemEntity.class);
            for (ItemOrganizationEntity itemOrganizationEntity : itemEntity.getItemOrganizationEntities()) {
                if (itemOrganizationEntity.isDeleted()) continue;
                final OrganizationEntity organizationEntity = itemOrganizationEntity.getOrganizationEntity();
                if (childOrganizationIds.contains(organizationEntity.getOrganizationId())) {
                    itemOrganizationEntity.setVerified(verify);
                    TrackableUtil.fillUpdate(itemOrganizationEntity, userContext);
                }
            }
        }
    }

    @Override
    @Transactional
    public void acrivateOrganizationItems(Long organizationId, Set<Long> itemIds, boolean active, UserContext userContext) {
        final List<ItemEntity> itemEntities = itemDao.find(itemIds);
        final Set<Long> childOrganizationIds = organizationStructureService.findChildOrganizationIds(organizationId, false);
        for (ItemEntity itemEntity : itemEntities) {
            if (itemEntity == null) throw new NonexistentEntityException(ItemEntity.class);
            for (ItemOrganizationEntity itemOrganizationEntity : itemEntity.getItemOrganizationEntities()) {
                if (itemOrganizationEntity.isDeleted()) continue;
                final OrganizationEntity organizationEntity = itemOrganizationEntity.getOrganizationEntity();
                if (childOrganizationIds.contains(organizationEntity.getOrganizationId())) {
                    itemOrganizationEntity.setActive(active);
                    TrackableUtil.fillUpdate(itemOrganizationEntity, userContext);
                }
            }
        }
    }

    @Override
    @Transactional
    public Map<Long, Set<Long>> getExternalItemOrganizations(Set<Long> itemIds) {
        List<ItemOrganizationEntity> itemOrganizationEntities = itemOrganizationDao.findExternalItemOrganization(itemIds);
        return itemOrganizationEntities
                .stream()
                .collect(Collectors.groupingBy(
                        ItemOrganizationEntity::getExternalItemId,
                        Collectors.mapping(x -> x.getOrganizationEntity().getOrganizationId(), Collectors.toSet())
                ));
    }

    @Override
    @Transactional
    public Set<Long> getExternalItemOrganization(Long itemId) {
        return itemOrganizationDao.findExternalItemOrganization(Collections.singleton(itemId))
                .stream()
                .map(x -> x.getOrganizationEntity().getOrganizationId())
                .collect(Collectors.toSet());
    }

    @Override
    @Transactional
    public Set<Long> switchOrganizationItems(Set<Long> dtoIds, Set<Long> entityIds, Long itemId, UserContext userContext) throws NonexistentEntityException {
        Map<Long, Boolean> newOrganizations = new HashMap<>();
        entityIds.forEach(id -> newOrganizations.put(id, false));
        dtoIds.forEach(id -> newOrganizations.put(id, true));
        final ItemEntity itemEntity = itemDao.find(itemId);
        for (Map.Entry<Long, Boolean> entry : newOrganizations.entrySet()) {
            ItemOrganizationEntity itemOrganizationEntity = itemOrganizationDao.findItemOrganization(entry.getKey(), itemId);
            if (itemOrganizationEntity == null) {
                final OrganizationEntity organizationEntity = organizationDao.find(entry.getKey());
                if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
                itemOrganizationEntity = new ItemOrganizationEntity();
                itemOrganizationEntity.setVerified(entry.getValue());
                itemOrganizationEntity.setItemEntity(itemEntity);
                itemOrganizationEntity.setOrganizationEntity(organizationEntity);
                TrackableUtil.fillCreate(itemOrganizationEntity, userContext);
            }
            itemOrganizationEntity.setDeleted(! entry.getValue());
            itemOrganizationEntity.setVerified(entry.getValue());
            TrackableUtil.fillUpdate(itemOrganizationEntity, userContext);
            itemOrganizationDao.saveItemOrganization(itemOrganizationEntity);
        }
        itemDao.saveItem(itemEntity);
        return newOrganizations.entrySet().stream()
                .filter(Map.Entry::getValue)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    @Override
    @Transactional
    public void addExternalItemOrganization(Long organizationId, Set<Long> externalItemIds, boolean verify, UserContext userContext) throws NonexistentEntityException {
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
        for (Long id : externalItemIds) {
            final ItemOrganizationEntity itemOrganizationEntity = new ItemOrganizationEntity();
            itemOrganizationEntity.setVerified(verify);
            itemOrganizationEntity.setExternalItemId(id);
            itemOrganizationEntity.setOrganizationEntity(organizationEntity);
            TrackableUtil.fillCreate(itemOrganizationEntity, userContext);
            itemOrganizationDao.saveItemOrganization(itemOrganizationEntity);
        }
    }
}
