package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.IndexHistoryEntity;

import java.util.List;

public interface IndexHistoryDao {

    void save(IndexHistoryEntity indexHistoryEntity);

    List<IndexHistoryEntity> findLatest();

    IndexHistoryEntity find(Long elasticsearchIndexHistoryId);

}
