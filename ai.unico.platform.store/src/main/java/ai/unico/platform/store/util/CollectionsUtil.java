package ai.unico.platform.store.util;

import java.util.*;

public class CollectionsUtil {

    public static final int HIBERNATE_LIMIT = 30000;

    public static <T> List<List<T>> toSubCollection(Collection<T> collection, int limit) {
        if (collection.isEmpty()) return Collections.emptyList();
        final int size = collection.size();
        final List<T> list = new ArrayList<>(collection);
        final List<List<T>> result = new ArrayList<>();
        for (int i = 0; i < size; i += limit) {
            result.add(list.subList(i, Math.min(i + limit, size)));
        }
        return result;
    }

    public static <T> List<List<T>> toSubCollectionForHibernate(Collection<T> list) {
        return toSubCollection(list, HIBERNATE_LIMIT);
    }

    public static <T> Set<T> setDifference(Set<T> a, Set<T> b) {
        if (a == null)
            return new HashSet<>();
        Set<T> copy = new HashSet<>(a);
        if (b != null && ! b.isEmpty() && ! a.isEmpty())
            copy.removeAll(b);
        copy.remove(null);
        return copy;
    }
}
