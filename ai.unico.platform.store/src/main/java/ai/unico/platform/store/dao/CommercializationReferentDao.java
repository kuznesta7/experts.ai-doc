package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.CommercializationProjectReferentEntity;

import java.util.List;

public interface CommercializationReferentDao {

    void save(CommercializationProjectReferentEntity commercializationProjectReferentEntity);

    List<CommercializationProjectReferentEntity> findForCommercializationProject(Long commercializationId);

}
