package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.api.enums.OrganizationRelationType;
import ai.unico.platform.store.dao.OrganizationStructureDao;
import ai.unico.platform.store.entity.OrganizationStructureEntity;
import ai.unico.platform.store.entity.OrganizationStructureViewEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class OrganizationStructureDaoImpl implements OrganizationStructureDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void save(OrganizationStructureEntity organizationStructureEntity) {
        entityManager.persist(organizationStructureEntity);
    }

    @Override
    public List<OrganizationStructureEntity> findAll() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationStructureEntity.class, "os");
        criteria.add(Restrictions.eq("deleted", false));
        return criteria.list();
    }

    @Override
    public List<OrganizationStructureEntity> findParentOrganizationStructure(Long organizationId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationStructureEntity.class, "os");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("lowerOrganizationEntity.organizationId", organizationId));
        return criteria.list();
    }

    @Override
    public List<OrganizationStructureEntity> findChildOrganizationStructure(Long organizationId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationStructureEntity.class, "os");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("upperOrganizationEntity.organizationId", organizationId));
        return criteria.list();
    }

    @Override
    public OrganizationStructureEntity find(Long parentOrganizationId, Long childOrganizationId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationStructureEntity.class, "os");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("upperOrganizationEntity.organizationId", parentOrganizationId));
        criteria.add(Restrictions.eq("lowerOrganizationEntity.organizationId", childOrganizationId));
        return (OrganizationStructureEntity) criteria.uniqueResult();
    }

    @Override
    public List<OrganizationStructureViewEntity> findOrganizationStructure(Long organizationId) {
        return findOrganizationStructure(Collections.singleton(organizationId));
    }

    @Override
    public List<OrganizationStructureViewEntity> findOrganizationStructure(Set<Long> organizationIds) {
        if (organizationIds.isEmpty()) return Collections.emptyList();
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationStructureViewEntity.class, "os");
        criteria.add(Restrictions.or(
                Restrictions.in("upperOrganizationId", organizationIds),
                Restrictions.in("lowerOrganizationId", organizationIds)
        ));
        return criteria.list();
    }

    @Override
    public List<OrganizationStructureViewEntity> findMembers(Long organizationId) {
        Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationStructureEntity.class, "os");
        criteria.add(Restrictions.and(Restrictions.eq("upperOrganizationId", organizationId),
                Restrictions.eq("relationType", OrganizationRelationType.MEMBER)));
        return criteria.list();

    }
}
