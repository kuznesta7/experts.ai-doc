package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.LanguageEntity;

import java.util.List;

public interface LanguageDao {

    LanguageEntity find(String languageCode);
    List<LanguageEntity> findAll();
}
