package ai.unico.platform.store.service;

import ai.unico.platform.extdata.dto.DWHProjectProgramDto;
import ai.unico.platform.extdata.service.DWHProjectProgramService;
import ai.unico.platform.store.api.dto.ProjectProgramPreviewDto;
import ai.unico.platform.store.api.service.ProjectProgramService;
import ai.unico.platform.store.dao.ProjectProgramDao;
import ai.unico.platform.store.entity.ProjectProgramEntity;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ProjectProgramServiceImpl implements ProjectProgramService {

    @Autowired
    private ProjectProgramDao projectProgramDao;

    @Override
    @Transactional
    public Map<Long, Long> updateProgramsFromDWH() {
        final List<ProjectProgramEntity> all = projectProgramDao.findAll();
        final Map<Long, Long> projectProgramOriginalIdNewIdMap = all.stream()
                .collect(Collectors.toMap(ProjectProgramEntity::getOriginalProjectProgramId, ProjectProgramEntity::getProjectProgramId));

        final DWHProjectProgramService service = ServiceRegistryUtil.getService(DWHProjectProgramService.class);
        final List<DWHProjectProgramDto> projectPrograms = service.findProjectPrograms();
        for (DWHProjectProgramDto projectProgram : projectPrograms) {
            final Long originalProjectProgramId = projectProgram.getProjectProgramId();
            if (projectProgramOriginalIdNewIdMap.containsKey(originalProjectProgramId)) continue;

            final ProjectProgramEntity projectProgramEntity = new ProjectProgramEntity();
            projectProgramEntity.setOriginalProjectProgramId(originalProjectProgramId);
            final String projectProgramName = projectProgram.getProjectProgramName();
            projectProgramEntity.setProjectProgramName(projectProgramName != null ? projectProgramName : "Unnamed program");
            projectProgramDao.save(projectProgramEntity);
            projectProgramOriginalIdNewIdMap.put(originalProjectProgramId, projectProgramEntity.getProjectProgramId());
        }
        return projectProgramOriginalIdNewIdMap;
    }

    @Override
    @Transactional
    public List<ProjectProgramPreviewDto> findPreviews(Set<Long> projectProgramIds) {
        final List<ProjectProgramEntity> projectProgramEntities = projectProgramDao.find(projectProgramIds);
        return projectProgramEntities.stream().map(this::convertToPreview).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<ProjectProgramPreviewDto> findPreviews() {
        final List<ProjectProgramEntity> all = projectProgramDao.findAll();
        return all.stream().map(this::convertToPreview).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public ProjectProgramPreviewDto findPreview(Long projectProgramId) {
        final ProjectProgramEntity projectProgramEntity = projectProgramDao.find(projectProgramId);
        if (projectProgramEntity == null) throw new NonexistentEntityException(ProjectProgramEntity.class);
        return convertToPreview(projectProgramEntity);
    }

    @Override
    @Transactional
    public Long createProjectProgram(String projectProgramName, String projectProgramDescription) {
        final ProjectProgramEntity projectProgramEntity = new ProjectProgramEntity();
        projectProgramEntity.setProjectProgramName(projectProgramName);
        projectProgramEntity.setProjectProgramDescription(projectProgramDescription);
        projectProgramDao.save(projectProgramEntity);
        return projectProgramEntity.getProjectProgramId();
    }

    @Override
    @Transactional
    public void updateProjectProgram(Long projectProgramId, String projectProgramName, String projectProgramDescription) {
        final ProjectProgramEntity projectProgramEntity = projectProgramDao.find(projectProgramId);
        if (projectProgramEntity == null) throw new NonexistentEntityException(ProjectProgramEntity.class);
        projectProgramEntity.setProjectProgramDescription(projectProgramDescription);
        projectProgramEntity.setProjectProgramName(projectProgramName);
    }

    private ProjectProgramPreviewDto convertToPreview(ProjectProgramEntity projectProgramEntity) {
        final ProjectProgramPreviewDto projectProgramPreviewDto = new ProjectProgramPreviewDto();
        projectProgramPreviewDto.setProjectProgramId(projectProgramEntity.getProjectProgramId());
        projectProgramPreviewDto.setProjectProgramName(projectProgramEntity.getProjectProgramName());
        return projectProgramPreviewDto;
    }

    @Override
    @Transactional
    public List<ProjectProgramPreviewDto> autocompleteProjectProgram(String query) {
        return projectProgramDao.autocomplete(query).stream().map(this::convertToPreview).collect(Collectors.toList());
    }
}
