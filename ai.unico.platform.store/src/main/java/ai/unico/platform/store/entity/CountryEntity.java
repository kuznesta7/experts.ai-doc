package ai.unico.platform.store.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CountryEntity {

    @Id
    private String countryCode;

    private String countryName;

    private String countryAbbrev;

    private boolean published;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryAbbrev() {
        return countryAbbrev;
    }

    public void setCountryAbbrev(String countryAbbrev) {
        this.countryAbbrev = countryAbbrev;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }
}
