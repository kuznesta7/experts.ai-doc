package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.CommercializationDomainDao;
import ai.unico.platform.store.entity.CommercializationDomainEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class CommercializationDomainDaoImpl implements CommercializationDomainDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public CommercializationDomainEntity findDomain(Long domainId) {
        return entityManager.find(CommercializationDomainEntity.class, domainId);
    }

    @Override
    public void save(CommercializationDomainEntity commercializationDomainEntity) {
        entityManager.persist(commercializationDomainEntity);
    }

    @Override
    public List<CommercializationDomainEntity> findAll() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(CommercializationDomainEntity.class, "domain");
        return criteria.list();
    }
}
