package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.SubscribeDto;
import ai.unico.platform.store.api.dto.SubscriptionTypeDto;
import ai.unico.platform.store.api.service.SubscriptionService;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.dao.SubscriptionOrganizationDao;
import ai.unico.platform.store.dao.SubscriptionTypeDao;
import ai.unico.platform.store.dao.WidgetEmailSubDao;
import ai.unico.platform.store.entity.SubscriptionOrganizationEntity;
import ai.unico.platform.store.entity.SubscriptionTypeEntity;
import ai.unico.platform.store.entity.WidgetEmailSubEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class SubscriptionServiceImpl implements SubscriptionService {

    @Autowired
    private WidgetEmailSubDao widgetEmailSubDao;

    @Autowired
    private SubscriptionTypeDao subscriptionTypeDao;

    @Autowired
    private SubscriptionOrganizationDao subscriptionOrganizationDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Override
    @Transactional
    public String subscribeForNewsletter(SubscribeDto subscribeDto) {
        WidgetEmailSubEntity widgetEmailSubEntity = widgetEmailSubDao.getWidgetEmailSubEntity(subscribeDto.getEmail());
        if (widgetEmailSubEntity == null) {
            widgetEmailSubEntity = new WidgetEmailSubEntity();
            widgetEmailSubEntity.setEmail(subscribeDto.getEmail());
            // todo - change the following lines, once it is save to delete (the logic is in the subscription_organization entity)
            widgetEmailSubEntity.setOrganizationId(subscribeDto.getOrganizationId());
        }
        if (subscribeDto.getSubscriptionTypeId() != null) {
            SubscriptionTypeEntity subscriptionTypeEntity = subscriptionTypeDao.getSubscriptionType(subscribeDto.getSubscriptionTypeId());
            widgetEmailSubEntity.setSubscriptionTypeEntity(subscriptionTypeEntity);
        }
        if (widgetEmailSubEntity.getUserToken() == null || widgetEmailSubEntity.getUserToken().isEmpty())
            widgetEmailSubEntity.setUserToken(subscribeDto.getUserToken());
        widgetEmailSubEntity.setSubscribed(true);
        widgetEmailSubEntity.setSubscriptionDate(new Date());
        setSubscriptionOrganizationEntry(widgetEmailSubEntity, subscribeDto);
        widgetEmailSubDao.save(widgetEmailSubEntity);
        return widgetEmailSubEntity.getUserToken();
    }

    @Override
    @Transactional
    public void unsubscribe(SubscribeDto subscribeDto) {
        WidgetEmailSubEntity widgetEmailSubEntity = widgetEmailSubDao.getWidgetEmailSubEntity(subscribeDto.getEmail());
        if (widgetEmailSubEntity == null) throw new RuntimeException("Email not found");
        SubscriptionOrganizationEntity subscriptionOrganizationEntity = subscriptionOrganizationDao.find(subscribeDto.getOrganizationId(), widgetEmailSubEntity.getSubId(), subscribeDto.getUserToken());
        if (subscriptionOrganizationEntity == null) throw new RuntimeException("Email not found");
        subscriptionOrganizationEntity.setDeleted(true);
        subscriptionOrganizationEntity.setDateUpdate(new Timestamp(new Date().getTime()));
        subscriptionOrganizationDao.save(subscriptionOrganizationEntity);
    }

    private void setSubscriptionOrganizationEntry(WidgetEmailSubEntity widgetEmailSubEntity, SubscribeDto subscribeDto) {
        SubscriptionOrganizationEntity subscriptionOrganizationEntity = subscriptionOrganizationDao.find(subscribeDto.getOrganizationId(), widgetEmailSubEntity.getSubId());
        if (subscriptionOrganizationEntity == null) {
            subscriptionOrganizationEntity = new SubscriptionOrganizationEntity();
            subscriptionOrganizationEntity.setOrganizationEntity(organizationDao.find(subscribeDto.getOrganizationId()));
            subscriptionOrganizationEntity.setWidgetEmailSubEntity(widgetEmailSubEntity);
            widgetEmailSubEntity.getSubscriptionOrganizationEntities().add(subscriptionOrganizationEntity);
            subscriptionOrganizationEntity.setDateInsert(new Timestamp(new Date().getTime()));
        }
        if (subscribeDto.getUserToken() != null && !subscribeDto.getUserToken().isEmpty())
            subscriptionOrganizationEntity.setUserToken(subscribeDto.getUserToken());
        else
            subscriptionOrganizationEntity.setUserToken(generateUserToken());
        subscriptionOrganizationEntity.setDateUpdate(new Timestamp(new Date().getTime()));
        subscriptionOrganizationEntity.setDeleted(false);
        if (subscribeDto.getSubscriptionTypeId() != null) {
            SubscriptionTypeEntity subscriptionTypeEntity = subscriptionTypeDao.getSubscriptionType(subscribeDto.getSubscriptionTypeId());
            subscriptionOrganizationEntity.setSubscriptionTypeEntity(subscriptionTypeEntity);
        }
        subscriptionOrganizationDao.save(subscriptionOrganizationEntity);
    }

    private String generateUserToken() {
        return UUID.randomUUID().toString();
    }

    @Override
    @Transactional
    public List<SubscriptionTypeDto> listSubscriptionTypes() {
        return subscriptionTypeDao.getAllSubscriptionTypes().stream()
                .filter(x -> !x.isDeleted() && x.getSubscriptionGroup() != null)
                .sorted(Comparator.comparing(SubscriptionTypeEntity::getSubscriptionGroup))
                .map(this::convert)
                .collect(Collectors.toList());
    }

    private SubscriptionTypeDto convert(SubscriptionTypeEntity entity) {
        SubscriptionTypeDto dto = new SubscriptionTypeDto();
        dto.setId(entity.getId());
        dto.setSubscriptionGroup(entity.getSubscriptionGroup());
        dto.setDescription(entity.getDescription());
        return dto;
    }
}
