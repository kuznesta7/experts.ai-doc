package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.OrganizationUserRoleEntity;
import ai.unico.platform.util.enums.OrganizationRole;

import java.util.List;

public interface OrganizationRoleDao {

    List<OrganizationUserRoleEntity> findOrganizationsRolesForUser(Long userId);

    OrganizationUserRoleEntity find(Long userId, Long organizationId, OrganizationRole role);

    List<OrganizationUserRoleEntity> findForOrganization(Long organizationId);

    List<OrganizationUserRoleEntity> findForOrganization(Long organizationId, OrganizationRole role);

    void save(OrganizationUserRoleEntity organizationUserRoleEntity);
}
