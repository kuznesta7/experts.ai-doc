package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ExternalMessageDao;
import ai.unico.platform.store.entity.ExternalMessageEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ExternalMessageDaoImpl implements ExternalMessageDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void saveMessage(ExternalMessageEntity externalMessageEntity) {
        entityManager.persist(externalMessageEntity);
    }
}
