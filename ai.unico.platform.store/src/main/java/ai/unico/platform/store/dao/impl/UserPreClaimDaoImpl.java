package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.UserPreClaimDao;
import ai.unico.platform.store.entity.UserPreClaimEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.util.enums.OrderDirection;
import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class UserPreClaimDaoImpl implements UserPreClaimDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public List<UserPreClaimEntity> findPreClaimsForUser(Long userId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserPreClaimEntity.class, "preClaim");
        criteria.add(Restrictions.eq("userProfileEntity.userId", userId));
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("resolved", false));
        return criteria.list();
    }

    @Override
    public void save(UserPreClaimEntity userPreClaimEntity) {
        entityManager.persist(userPreClaimEntity);
    }

    @Override
    public List<PreClaim> findPreClaims(Collection<Long> userToClaimIds, Collection<Long> expertToClaimIds) {
        if (userToClaimIds.isEmpty() && expertToClaimIds.isEmpty()) return Collections.emptyList();
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserPreClaimEntity.class, "preClaim");
        final ProjectionList projectionList = Projections.projectionList();
        final Disjunction disjunction = Restrictions.disjunction();
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("resolved", false));
        if (!userToClaimIds.isEmpty()) {
            disjunction.add(Restrictions.in("userToClaimEntity.userId", userToClaimIds));
        }
        if (!expertToClaimIds.isEmpty()) {
            disjunction.add(Restrictions.in("expertToClaimId", expertToClaimIds));
        }
        criteria.add(disjunction);
        criteria.createAlias("userProfileEntity", "user");
        projectionList.add(Projections.property("user.email").as("email"));
        projectionList.add(Projections.property("user.userId").as("userId"));
        projectionList.add(Projections.property("userToClaimEntity.userId").as("userToClaimId"));
        projectionList.add(Projections.property("expertToClaimId").as("expertToClaimId"));
        HibernateUtil.setOrder(criteria, "dateInsert", OrderDirection.DESC);
        criteria.setProjection(projectionList);
        criteria.setResultTransformer(new AliasToBeanResultTransformer(PreClaim.class));
        return criteria.list();
    }
}
