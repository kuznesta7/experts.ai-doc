package ai.unico.platform.store.service;

import ai.unico.platform.search.api.service.ExpertLookupService;
import ai.unico.platform.store.api.dto.FollowProfileDto;
import ai.unico.platform.store.api.dto.FollowedProfilePreviewDto;
import ai.unico.platform.store.api.dto.UserExpertBaseDto;
import ai.unico.platform.store.api.dto.UserWorkspaceDto;
import ai.unico.platform.store.api.filter.FollowedProfilesFilter;
import ai.unico.platform.store.api.service.FollowProfileService;
import ai.unico.platform.store.api.service.UserWorkspaceService;
import ai.unico.platform.store.api.wrapper.FollowedProfilesWrapper;
import ai.unico.platform.store.dao.FollowProfileDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.dao.UserWorkspaceDao;
import ai.unico.platform.store.entity.FollowedProfileEntity;
import ai.unico.platform.store.entity.FollowedProfileWorkspaceEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.entity.UserWorkspaceEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.InvalidParameterException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class FollowProfileServiceImpl implements FollowProfileService {

    @Autowired
    private FollowProfileDao followProfileDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private UserWorkspaceDao userWorkspaceDao;

    @Autowired
    private UserWorkspaceService userWorkspaceService;

    @Override
    @Transactional
    public void followProfile(FollowProfileDto followProfileDto, UserContext userContext) {
        final Long followingUserId = followProfileDto.getFollowingUserId();
        final FollowedProfileEntity existingFollowedProfileEntity;
        final Long followedUserId = followProfileDto.getFollowedUserId();
        if (followingUserId == null) throw new InvalidParameterException("missing follower userId");
        if (followedUserId != null) {
            existingFollowedProfileEntity = followProfileDao.findUserFollow(followingUserId, followedUserId);
        } else if (followProfileDto.getFollowedExpertId() != null) {
            existingFollowedProfileEntity = followProfileDao.findExpertFollow(followingUserId, followProfileDto.getFollowedExpertId());
        } else {
            throw new InvalidParameterException("missing followed expertId or userId");
        }
        if (existingFollowedProfileEntity != null) {
            fillWorkspaces(existingFollowedProfileEntity, followProfileDto, userContext);
            TrackableUtil.fillUpdate(existingFollowedProfileEntity, userContext);
        } else {
            final FollowedProfileEntity followedProfileEntity = new FollowedProfileEntity();
            if (followedUserId != null) {
                final UserProfileEntity userProfile = userProfileDao.findUserProfile(followedUserId);
                if (userProfile == null) throw new NonexistentEntityException(UserProfileEntity.class);
                followedProfileEntity.setFollowedUser(userProfile);
            } else {
                followedProfileEntity.setFollowedExpertId(followProfileDto.getFollowedExpertId());
            }
            final UserProfileEntity userProfile = userProfileDao.findUserProfile(followingUserId);
            if (userProfile == null) throw new NonexistentEntityException(UserProfileEntity.class);
            followedProfileEntity.setFollowerUser(userProfile);
            TrackableUtil.fillCreate(followedProfileEntity, userContext);
            followProfileDao.save(followedProfileEntity);
            fillWorkspaces(followedProfileEntity, followProfileDto, userContext);
        }
    }

    @Override
    @Transactional
    public void unfollowUserProfile(Long followingUserId, Long followedUserId, UserContext userContext) {
        final FollowedProfileEntity userFollow = followProfileDao.findUserFollow(followingUserId, followedUserId);
        userFollow.setDeleted(true);
        TrackableUtil.fillUpdate(userFollow, userContext);
    }

    @Override
    @Transactional
    public void unfollowExpertProfile(Long followingUserId, Long followedExpertId, UserContext userContext) {
        final FollowedProfileEntity expertFollow = followProfileDao.findExpertFollow(followingUserId, followedExpertId);
        expertFollow.setDeleted(true);
        TrackableUtil.fillUpdate(expertFollow, userContext);
    }

    @Override
    @Transactional
    public FollowedProfilesWrapper findFollowedProfilesForUser(Long userId, FollowedProfilesFilter followedProfilesFilter) throws IOException {
        final FollowedProfilesWrapper followedProfilesWrapper = new FollowedProfilesWrapper();
        followedProfilesWrapper.setNumberOfResults(followProfileDao.countFollowedProfilesForUser(userId, followedProfilesFilter));
        final List<FollowedProfileEntity> followedProfilesForUser = followProfileDao.findFollowedProfilesForUser(userId, followedProfilesFilter);
        final ExpertLookupService service = ServiceRegistryUtil.getService(ExpertLookupService.class);
        final Set<Long> followedExpertIds = followedProfilesForUser.stream()
                .map(FollowedProfileEntity::getFollowedExpertId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        final List<UserExpertBaseDto> expertBases = service.findExpertBases(followedExpertIds);
        final Map<Long, String> expertMap = expertBases.stream()
                .collect(Collectors.toMap(UserExpertBaseDto::getExpertId, UserExpertBaseDto::getName));
        final List<FollowedProfilePreviewDto> followedProfilePreviewDtos = new ArrayList<>();
        for (FollowedProfileEntity followedProfileEntity : followedProfilesForUser) {
            followedProfilePreviewDtos.add(convert(followedProfileEntity, expertMap));
        }
        followedProfilesWrapper.setFollowedProfilePreviewDtos(followedProfilePreviewDtos);
        followedProfilesWrapper.setLimit(followedProfilesFilter.getLimit());
        followedProfilesWrapper.setPage(followedProfilesFilter.getPage());
        return followedProfilesWrapper;
    }

    @Override
    @Transactional
    public FollowedProfilePreviewDto findFollowedUserProfile(Long userId, Long followedUserId) {
        final FollowedProfileEntity userFollow = followProfileDao.findUserFollow(userId, followedUserId);
        if (userFollow == null) return null;
        return convert(userFollow, null);
    }

    @Override
    @Transactional
    public FollowedProfilePreviewDto findFollowedExpertProfile(Long userId, Long followedExpertId) throws IOException {
        final FollowedProfileEntity expertFollow = followProfileDao.findExpertFollow(userId, followedExpertId);
        if (expertFollow == null) return null;
        final ExpertLookupService service = ServiceRegistryUtil.getService(ExpertLookupService.class);
        final List<UserExpertBaseDto> expertBases = service.findExpertBases(Collections.singleton(followedExpertId));
        final Map<Long, String> expertMap = expertBases.stream()
                .collect(Collectors.toMap(UserExpertBaseDto::getExpertId, UserExpertBaseDto::getName));
        return convert(expertFollow, expertMap);
    }

    private FollowedProfilePreviewDto convert(FollowedProfileEntity followedProfileEntity, Map<Long, String> expertMap) {
        final FollowedProfilePreviewDto followedProfilePreviewDto = new FollowedProfilePreviewDto();
        final UserProfileEntity followedUser = followedProfileEntity.getFollowedUser();
        if (followedUser != null) {
            followedProfilePreviewDto.setUserId(followedUser.getUserId());
            followedProfilePreviewDto.setName(followedUser.getName());
        } else {
            final Long followedExpertId = followedProfileEntity.getFollowedExpertId();
            followedProfilePreviewDto.setExpertId(followedExpertId);
            followedProfilePreviewDto.setName(expertMap.get(followedExpertId));
        }
        final Set<FollowedProfileWorkspaceEntity> followedProfileWorkspaceEntities = followedProfileEntity.getFollowedProfileWorkspaceEntities();
        final List<String> workspaces = followedProfileWorkspaceEntities.stream()
                .filter(x -> !x.isDeleted())
                .map(FollowedProfileWorkspaceEntity::getUserWorkspaceEntity)
                .filter(userWorkspaceEntity -> !userWorkspaceEntity.isDeleted())
                .map(UserWorkspaceEntity::getWorkspaceName)
                .collect(Collectors.toList());
        followedProfilePreviewDto.setWorkspaces(workspaces);
        return followedProfilePreviewDto;
    }

    private void fillWorkspaces(FollowedProfileEntity followedProfileEntity, FollowProfileDto followProfileDto, UserContext userContext) {
        final Set<String> workspaces = new HashSet<>(followProfileDto.getWorkspaces());
        final Set<FollowedProfileWorkspaceEntity> followedProfileWorkspaceEntities = followedProfileEntity.getFollowedProfileWorkspaceEntities();
        for (FollowedProfileWorkspaceEntity followedProfileWorkspaceEntity : followedProfileWorkspaceEntities) {
            if (followedProfileWorkspaceEntity.isDeleted()) continue;
            final String workspaceName = followedProfileWorkspaceEntity.getUserWorkspaceEntity().getWorkspaceName();
            if (!workspaces.contains(workspaceName)) {
                followedProfileWorkspaceEntity.setDeleted(true);
                continue;
            }
            workspaces.remove(workspaceName);
        }
        for (String workspace : workspaces) {
            final UserWorkspaceEntity userWorkspace = userWorkspaceDao.findUserWorkspace(followProfileDto.getFollowingUserId(), workspace);
            final FollowedProfileWorkspaceEntity followedProfileWorkspaceEntity = new FollowedProfileWorkspaceEntity();
            followedProfileWorkspaceEntity.setFollowedProfileEntity(followedProfileEntity);
            if (userWorkspace != null) {
                followedProfileWorkspaceEntity.setUserWorkspaceEntity(userWorkspace);
            } else {
                final UserWorkspaceDto userWorkspaceDto = new UserWorkspaceDto();
                userWorkspaceDto.setWorkspaceName(workspace);
                userWorkspaceDto.setUserId(followProfileDto.getFollowingUserId());
                final Long userWorkspaceId = userWorkspaceService.createUserWorkspace(userWorkspaceDto, userContext);
                followedProfileWorkspaceEntity.setUserWorkspaceEntity(userWorkspaceDao.findUserWorkspace(userWorkspaceId));
            }
            userWorkspaceDao.save(followedProfileWorkspaceEntity);
        }
    }
}
