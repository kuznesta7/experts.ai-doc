package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.JobTypeEntity;

import java.util.List;

public interface JobTypeDao {

    List<JobTypeEntity> getAllJobTypes();

    JobTypeEntity getJobTypeEntity(Long id);

    void save(JobTypeEntity jobTypeEntity);
}
