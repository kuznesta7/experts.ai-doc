package ai.unico.platform.store.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class FlywaySchemaHistoryEntity {

    @Id
    private Integer installedRank;

    private String version;

    public Integer getInstalledRank() {
        return installedRank;
    }

    public void setInstalledRank(Integer installedRank) {
        this.installedRank = installedRank;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
