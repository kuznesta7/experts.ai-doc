package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.OrganizationLicenceDto;
import ai.unico.platform.store.api.dto.OrganizationLicenceUsersDto;
import ai.unico.platform.store.api.dto.OrganizationUserDto;
import ai.unico.platform.store.api.service.OrganizationLicenceService;
import ai.unico.platform.store.api.service.OrganizationRoleService;
import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.dao.OrganizationRoleDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.entity.OrganizationLicenceEntity;
import ai.unico.platform.store.entity.OrganizationUserRoleEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.dto.OrganizationRolesDto;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.RuntimeExceptionWithMessage;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

public class OrganizationRoleServiceImpl implements OrganizationRoleService {

    @Autowired
    private OrganizationRoleDao organizationRoleDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private OrganizationLicenceService organizationLicenceService;

    @Autowired
    private OrganizationStructureService organizationStructureService;

    @Override
    @Transactional
    public void addUserOrganizationRole(Long userId, Long organizationId, OrganizationRole organizationRole, UserContext userContext) {
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        final UserProfileEntity userProfile = userProfileDao.findUserProfile(userId);
        if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
        if (userProfile == null) throw new NonexistentEntityException(UserProfileEntity.class);
        final List<OrganizationUserRoleEntity> organizationUserRoleEntities = organizationRoleDao.findForOrganization(organizationId, organizationRole);
        for (OrganizationUserRoleEntity organizationUserRoleEntity : organizationUserRoleEntities) {
            if (organizationUserRoleEntity.isDeleted()) continue;
            if (organizationUserRoleEntity.getUserProfileEntity().getUserId().equals(userId))
                throw new EntityAlreadyExistsException(OrganizationUserRoleEntity.class);
        }

        final List<OrganizationLicenceDto> organizationLicences = organizationLicenceService.getOrganizationLicences(organizationId, true);
        final List<OrganizationLicenceDto> validLicences = organizationLicences.stream()
                .filter(x -> x.getLicenceRole().equals(organizationRole))
                .collect(Collectors.toList());
        final long numberOfUsedLicences = organizationUserRoleEntities.stream()
                .filter(x -> !x.isDeleted())
                .count();
        if (!organizationRole.isAlwaysPresent() && validLicences.stream().noneMatch(x -> x.getLicenceRole().equals(organizationRole))) {
            throw new RuntimeExceptionWithMessage("NO_VALID_LICENCE_FOR_THIS_ROLE", "No valid licence for role");
        }
        if (!organizationRole.isAlwaysPresent() && validLicences.stream().noneMatch(x -> x.getNumberOfUserLicences() == null || numberOfUsedLicences < x.getNumberOfUserLicences())) {
            throw new RuntimeExceptionWithMessage("ORGANIZATION_USER_ROLE_LIMIT_EXCEEDED", "Organization user role limit exceeded");
        }
        final OrganizationUserRoleEntity organizationUserRoleEntity = new OrganizationUserRoleEntity();
        organizationUserRoleEntity.setUserProfileEntity(userProfile);
        organizationUserRoleEntity.setOrganizationEntity(organizationEntity);
        organizationUserRoleEntity.setRole(organizationRole);
        TrackableUtil.fillCreate(organizationUserRoleEntity, userContext);
        organizationRoleDao.save(organizationUserRoleEntity);
    }

    @Override
    @Transactional
    public void removeUserOrganizationRole(Long userId, Long organizationId, OrganizationRole organizationRole, UserContext userContext) {
        final OrganizationUserRoleEntity organizationUserRoleEntity = organizationRoleDao.find(userId, organizationId, organizationRole);
        if (organizationUserRoleEntity == null) throw new NonexistentEntityException(OrganizationUserRoleEntity.class);
        organizationUserRoleEntity.setDeleted(true);
        TrackableUtil.fillUpdate(organizationUserRoleEntity, userContext);
    }

    @Override
    @Transactional
    public List<OrganizationRolesDto> findOrganizationsForUser(Long userId) {
        final List<OrganizationUserRoleEntity> organizationsAdminForUser = organizationRoleDao.findOrganizationsRolesForUser(userId);
        final Set<Long> organizationIds = organizationsAdminForUser.stream()
                .map(x -> HibernateUtil.getId(x, OrganizationUserRoleEntity::getOrganizationEntity, OrganizationEntity::getOrganizationId))
                .collect(Collectors.toSet());
        final List<OrganizationLicenceDto> organizationLicences = organizationLicenceService.getOrganizationLicences(organizationIds, true);
        final Map<Long, List<OrganizationLicenceDto>> organizationIdToRolesMap = organizationLicences.stream()
                .collect(Collectors.groupingBy(OrganizationLicenceDto::getOrganizationId));

        final Map<Long, OrganizationRolesDto> organizationIdRolesDtoMap = new HashMap<>();
        for (OrganizationUserRoleEntity organizationUserRoleEntity : organizationsAdminForUser) {
            final OrganizationRole role = organizationUserRoleEntity.getRole();
            final Long organizationId = HibernateUtil.getId(organizationUserRoleEntity, OrganizationUserRoleEntity::getOrganizationEntity, OrganizationEntity::getOrganizationId);
            final Set<Long> childOrganizationIds = organizationStructureService.findChildOrganizationIds(organizationId, false);
            if (!organizationIdToRolesMap.containsKey(organizationId)) continue;
            final List<OrganizationLicenceDto> licenceRolesDto = organizationIdToRolesMap.get(organizationId);
            for (Long childOrganizationId : childOrganizationIds) {
                final Optional<OrganizationLicenceDto> organizationLicenceForRole = licenceRolesDto.stream().filter(lr -> lr.getLicenceRole().equals(role)).findFirst();
                if (role.isAlwaysPresent() || organizationLicenceForRole.isPresent()) {
                    final OrganizationRolesDto organizationRolesDto = organizationIdRolesDtoMap.computeIfAbsent(childOrganizationId, id -> {
                        final OrganizationRolesDto newOrganizationRolesDto = new OrganizationRolesDto();
                        newOrganizationRolesDto.setOrganizationId(childOrganizationId);
                        return newOrganizationRolesDto;
                    });

                    final OrganizationRolesDto.OrganizationRoleDto roleDto = new OrganizationRolesDto.OrganizationRoleDto();
                    roleDto.setRole(role);
                    if (organizationLicenceForRole.isPresent()) {
                        final OrganizationLicenceDto organizationLicenceDto = organizationLicenceForRole.get();
                        roleDto.setValid(organizationLicenceDto.isValid());
                        roleDto.setValidUntil(organizationLicenceDto.getValidUntil());
                    } else {
                        roleDto.setValid(true);
                    }
                    organizationRolesDto.getRoles().add(roleDto);
                }
            }
        }
        final ArrayList<OrganizationRolesDto> organizationRolesDtos = new ArrayList<>();
        final Set<Long> allOrganizationIds = organizationIdRolesDtoMap.keySet();
        final List<OrganizationEntity> allOrganizationEntities = organizationDao.findByIds(allOrganizationIds);
        allOrganizationEntities.forEach(e -> {
            final OrganizationRolesDto organizationRolesDto = organizationIdRolesDtoMap.get(e.getOrganizationId());
            if (organizationRolesDto == null) return;
            organizationRolesDto.setOrganizationName(e.getOrganizationName());
            organizationRolesDto.setOrganizationAbbrev(e.getOrganizationAbbrev());
            organizationRolesDtos.add(organizationRolesDto);
        });
        organizationRolesDtos.sort(Comparator.comparing(OrganizationRolesDto::getOrganizationName));
        return organizationRolesDtos;
    }

    @Override
    @Transactional
    public OrganizationLicenceUsersDto findOrganizationUsers(Long organizationId, OrganizationRole licenceRole, Integer page, Integer limit) {
        final OrganizationLicenceUsersDto organizationLicenceUsersDto = new OrganizationLicenceUsersDto();
        final List<OrganizationUserRoleEntity> organizationRoleUsers = organizationRoleDao.findForOrganization(organizationId, licenceRole);
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
        final List<OrganizationLicenceDto> organizationLicences = organizationLicenceService.getOrganizationLicences(organizationId, false);
        final Optional<OrganizationLicenceDto> optionalValidLicence = organizationLicences.stream().max(Comparator.comparing(OrganizationLicenceDto::getValidUntil));
        if (optionalValidLicence.isPresent()) {
            final OrganizationLicenceDto organizationLicenceDto = optionalValidLicence.get();
            organizationLicenceUsersDto.setValid(organizationLicenceDto.isValid());
            organizationLicenceUsersDto.setValidUntil(organizationLicenceDto.getValidUntil());
            organizationLicenceUsersDto.setLicenceRole(organizationLicenceDto.getLicenceRole());
            organizationLicenceUsersDto.setNumberOfAvailableLicences(organizationLicenceDto.getNumberOfUserLicences());
            organizationLicenceUsersDto.setNumberOfUsedLicences(organizationRoleUsers.size());
        } else {
            throw new NonexistentEntityException(OrganizationLicenceEntity.class);
        }
        final List<OrganizationUserDto> organizationUserDtos = organizationRoleUsers.stream().map(this::convertToOrganizationUser).collect(Collectors.toList());
        organizationLicenceUsersDto.setOrganizationUserDtos(createSublist(organizationUserDtos, page, limit));
        return organizationLicenceUsersDto;
    }

    @Override
    @Transactional
    public List<OrganizationLicenceUsersDto> findOrganizationUsers(Long organizationId, Integer limit) {
        final List<OrganizationUserRoleEntity> forOrganization = organizationRoleDao.findForOrganization(organizationId);
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
        final HashMap<OrganizationRole, List<OrganizationUserDto>> licenceUserMap = new HashMap<>();
        for (OrganizationUserRoleEntity organizationUserRoleEntity : forOrganization) {
            final List<OrganizationUserDto> organizationUserDtos = licenceUserMap.computeIfAbsent(organizationUserRoleEntity.getRole(), x -> new ArrayList<>());
            final OrganizationUserDto organizationUserDto = convertToOrganizationUser(organizationUserRoleEntity);
            organizationUserDtos.add(organizationUserDto);
        }
        final List<OrganizationLicenceDto> organizationLicences = organizationLicenceService.getOrganizationLicences(organizationId, false);
        final List<OrganizationLicenceDto> bestOrganizationLicences = findBestOrganizationLicences(organizationLicences);
        for (OrganizationRole role : OrganizationRole.values()) {
            if (!role.isAlwaysPresent()) continue;
            if (bestOrganizationLicences.stream().anyMatch(x -> x.getLicenceRole().equals(role))) continue;
            final OrganizationLicenceDto organizationLicenceDto = new OrganizationLicenceDto();
            organizationLicenceDto.setLicenceRole(role);
            organizationLicenceDto.setOrganizationId(organizationId);
            organizationLicenceDto.setValid(true);
            bestOrganizationLicences.add(organizationLicenceDto);
        }

        final List<OrganizationLicenceUsersDto> organizationLicenceUsersDtos = new ArrayList<>();
        for (OrganizationLicenceDto organizationLicenceDto : bestOrganizationLicences) {
            final OrganizationLicenceUsersDto organizationLicenceUsersDto = new OrganizationLicenceUsersDto();
            final List<OrganizationUserDto> organizationUserDtos = licenceUserMap.computeIfAbsent(organizationLicenceDto.getLicenceRole(), x -> new ArrayList<>());
            organizationLicenceUsersDto.setValid(organizationLicenceDto.isValid());
            organizationLicenceUsersDto.setValidUntil(organizationLicenceDto.getValidUntil());
            organizationLicenceUsersDto.setLicenceRole(organizationLicenceDto.getLicenceRole());
            organizationLicenceUsersDto.setNumberOfAvailableLicences(organizationLicenceDto.getNumberOfUserLicences());
            organizationLicenceUsersDto.setNumberOfUsedLicences(organizationUserDtos.size());
            organizationLicenceUsersDto.setOrganizationId(organizationLicenceDto.getOrganizationId());
            organizationLicenceUsersDto.setOrganizationLicenceId(organizationLicenceDto.getOrganizationLicenceId());
            organizationLicenceUsersDto.setOrganizationUserDtos(createSublist(organizationUserDtos, 1, limit));
            organizationLicenceUsersDtos.add(organizationLicenceUsersDto);
        }
        return organizationLicenceUsersDtos;
    }

    private OrganizationRolesDto convert(OrganizationEntity organization) {
        final OrganizationRolesDto organizationRolesDto = new OrganizationRolesDto();
        organizationRolesDto.setOrganizationId(organization.getOrganizationId());
        organizationRolesDto.setOrganizationName(organization.getOrganizationName());
        organizationRolesDto.setOrganizationAbbrev(organization.getOrganizationAbbrev());
        return organizationRolesDto;
    }

    private List<OrganizationUserDto> createSublist(List<OrganizationUserDto> organizationUserDtos, Integer page, Integer limit) {
        final Integer start;
        if (limit == null) {
            return organizationUserDtos;
        }
        if (organizationUserDtos.size() > (page - 1) * limit) {
            return Collections.emptyList();
        } else {
            start = (page - 1) * limit;
        }
        final int end = Math.min(start + limit, organizationUserDtos.size() - 1);
        return organizationUserDtos.subList(start, end);
    }

    private OrganizationUserDto convertToOrganizationUser(OrganizationUserRoleEntity organizationUserRoleEntity) {
        final OrganizationUserDto organizationUserDto = new OrganizationUserDto();
        final UserProfileEntity userProfileEntity = organizationUserRoleEntity.getUserProfileEntity();
        organizationUserDto.setUserId(userProfileEntity.getUserId());
        organizationUserDto.setName(userProfileEntity.getName());
        organizationUserDto.setUsername(userProfileEntity.getUsername() != null ? userProfileEntity.getUsername() : userProfileEntity.getEmail());
        return organizationUserDto;
    }

    private List<OrganizationLicenceDto> findBestOrganizationLicences(List<OrganizationLicenceDto> allOrganizationLicences) {
        final Map<OrganizationRole, OrganizationLicenceDto> roleLicenceMap = new HashMap<>();
        for (OrganizationLicenceDto organizationLicence : allOrganizationLicences) {
            final OrganizationRole licenceRole = organizationLicence.getLicenceRole();
            if (roleLicenceMap.containsKey(licenceRole)) {
                final OrganizationLicenceDto currentBestLicence = roleLicenceMap.get(licenceRole);
                if (currentBestLicence.getValidUntil() == null
                        || (organizationLicence.getValidUntil() != null && currentBestLicence.getValidUntil().after(organizationLicence.getValidUntil()))) {
                    continue;
                }
            }
            roleLicenceMap.put(licenceRole, organizationLicence);
        }
        return allOrganizationLicences.stream()
                .filter(roleLicenceMap::containsValue)
                .collect(Collectors.toList());
    }
}

