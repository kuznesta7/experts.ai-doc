package ai.unico.platform.store.util;

import ai.unico.platform.store.api.annotation.RequiredField;
import ai.unico.platform.store.api.exception.MissingParameterException;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class ParameterValidationUtil {

    public static <T> void checkIfRequiredParametersPresent(T entity, String condition) {
        try {
            final Class<?> aClass = entity.getClass();
            final Field[] declaredFields = aClass.getDeclaredFields();
            for (Field declaredField : declaredFields) {
                if (!declaredField.isAnnotationPresent(RequiredField.class)) continue;
                final PropertyDescriptor propertyDescriptor = new PropertyDescriptor(declaredField.getName(), aClass);
                final Method getter = propertyDescriptor.getReadMethod();
                final Object value = getter.invoke(entity);
                if (!checkValueIsValid(value)) {
                    final RequiredField annotation = declaredField.getAnnotation(RequiredField.class);
                    final List<String> conditions = Arrays.asList(annotation.conditions());
                    if (condition == null || condition.isEmpty() || conditions.isEmpty() || conditions.contains(condition)) {
                        if (annotation.fieldName().isEmpty()) {
                            throw new MissingParameterException(declaredField.getName());
                        } else {
                            throw new MissingParameterException(annotation.fieldName());
                        }
                    }
                }
            }
        } catch (IntrospectionException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static <T> void checkIfRequiredParametersPresent(T dto) {
        checkIfRequiredParametersPresent(dto, null);
    }

    private static boolean checkValueIsValid(Object value) {
        if (value == null) return false;
        if (value instanceof String) return !((String) value).isEmpty();
        if (value instanceof Collection) {
            for (Object v : (Collection) value) {
                final Class<?> aClass = v.getClass();
                final Field[] declaredFields = aClass.getDeclaredFields();
                final Optional<Field> first = Arrays.stream(declaredFields).filter(f -> "deleted".equals(f.getName())).findFirst();
                if (first.isPresent()) {
                    final Field field = first.get();
                    try {
                        final PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), aClass);
                        final Method getter = propertyDescriptor.getReadMethod();
                        final Object deleted = getter.invoke(v);
                        if (deleted == null || (field.getType() == Boolean.class || field.getType() == boolean.class) && !((boolean) deleted))
                            return true;
                    } catch (IntrospectionException | IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                } else {
                    return true; //element without deleted parameter
                }
            }
            return false; //empty collection
        }
        return true; //other nonnull cases
    }
}
