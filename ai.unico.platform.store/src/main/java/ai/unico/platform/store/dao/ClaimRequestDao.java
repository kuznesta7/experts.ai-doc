package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ClaimRequestEntity;

public interface ClaimRequestDao {

    void save(ClaimRequestEntity claimRequestEntity);

}
