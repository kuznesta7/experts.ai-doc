package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.CommercializationProjectDocumentDto;
import ai.unico.platform.store.api.dto.CommercializationProjectDocumentPreviewDto;
import ai.unico.platform.store.api.service.CommercializationDocumentService;
import ai.unico.platform.store.api.service.FileService;
import ai.unico.platform.store.dao.CommercializationProjectDao;
import ai.unico.platform.store.dao.CommercializationProjectDocumentDao;
import ai.unico.platform.store.dao.FileDao;
import ai.unico.platform.store.entity.CommercializationProjectDocumentEntity;
import ai.unico.platform.store.entity.CommercializationProjectEntity;
import ai.unico.platform.store.entity.FileEntity;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

public class CommercializationDocumentServiceImpl implements CommercializationDocumentService {

    @Autowired
    private CommercializationProjectDocumentDao commercializationProjectDocumentDao;

    @Autowired
    private CommercializationProjectDao commercializationProjectDao;

    @Autowired
    private FileDao fileDao;

    @Autowired
    private FileService fileService;

    @Override
    @Transactional
    public List<CommercializationProjectDocumentPreviewDto> findCommercializationProjectDocuments(Long commercializationProjectId) {
        final List<CommercializationProjectDocumentEntity> commercializationDocumentEntities = commercializationProjectDocumentDao.findForCommercializationProject(commercializationProjectId);
        return commercializationDocumentEntities.stream().map(this::convert).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public FileDto getCommercializationProjectDocument(Long commercializationProjectId, Long fileId, UserContext userContext) {
        final CommercializationProjectDocumentEntity document = commercializationProjectDocumentDao.findDocument(commercializationProjectId, fileId);
        if (document == null) throw new NonexistentEntityException(CommercializationProjectDocumentEntity.class);
        return fileService.getFile(fileId, userContext);
    }

    @Override
    @Transactional
    public Long addCommercializationProjectDocument(Long commercializationProjectId, CommercializationProjectDocumentDto documentDto, UserContext userContext) {
        final CommercializationProjectEntity commercializationProjectEntity = commercializationProjectDao.find(commercializationProjectId);
        if (commercializationProjectEntity == null)
            throw new NonexistentEntityException(CommercializationProjectEntity.class);
        final FileEntity fileEntity = new FileEntity();
        fileEntity.setContent(documentDto.getContent());
        fileEntity.setName(documentDto.getName());
        TrackableUtil.fillCreate(fileEntity, userContext);
        fileDao.save(fileEntity);
        final CommercializationProjectDocumentEntity documentEntity = new CommercializationProjectDocumentEntity();
        documentEntity.setCommercializationProjectEntity(commercializationProjectEntity);
        documentEntity.setFileEntity(fileEntity);
        TrackableUtil.fillCreate(documentEntity, userContext);
        commercializationProjectDocumentDao.save(documentEntity);
        return fileEntity.getFileId();
    }

    @Override
    @Transactional
    public void deleteCommercializationProjectDocument(Long commercializationProjectId, Long fileId, UserContext userContext) {
        final CommercializationProjectDocumentEntity document = commercializationProjectDocumentDao.findDocument(commercializationProjectId, fileId);
        if (document == null) throw new NonexistentEntityException(CommercializationProjectDocumentEntity.class);
        document.setDeleted(true);
        TrackableUtil.fillUpdate(document, userContext);
    }

    private CommercializationProjectDocumentPreviewDto convert(CommercializationProjectDocumentEntity entity) {
        CommercializationProjectDocumentPreviewDto dto = new CommercializationProjectDocumentPreviewDto();
        dto.setCommercializationId(HibernateUtil.getId(entity, CommercializationProjectDocumentEntity::getCommercializationProjectEntity, CommercializationProjectEntity::getCommercializationId));
        final FileEntity fileEntity = entity.getFileEntity();
        dto.setDocumentId(fileEntity.getFileId());
        dto.setDocumentName(fileEntity.getName());
        dto.setSize(fileEntity.getSize());
        dto.setAuthorUserId(entity.getUserInsert());
        dto.setDateCreated(entity.getDateInsert());
        if (fileEntity.getName() != null && fileEntity.getName().contains(".")) {
            final int i = fileEntity.getName().lastIndexOf('.');
            dto.setDocumentType(fileEntity.getName().substring(i + 1).toUpperCase());
        }
        return dto;
    }
}
