package ai.unico.platform.store.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "widget_email_subscription")
public class WidgetEmailSubEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sub_id")
    private Long subId;

    @Column(name = "email")
    private String email;

    @Column(name = "subscription_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date subscriptionDate;

    @Column(name = "unsubscription_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date unsubscriptionDate;

    @Column(name = "organization_id")
    private Long organizationId;

    @Column(name = "subscribed")
    private boolean isSubscribed;

    @Column(name = "user_token")
    private String userToken;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "subscription_type_id")
    private SubscriptionTypeEntity subscriptionTypeEntity;

    @OneToMany(mappedBy = "widgetEmailSubEntity")
    private Set<SubscriptionOrganizationEntity> subscriptionOrganizationEntities = new HashSet<>();

    public Long getSubId() {
        return subId;
    }

    public void setSubId(Long subId) {
        this.subId = subId;
    }

    public Date getSubscriptionDate() {
        return subscriptionDate;
    }

    public void setSubscriptionDate(Date subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
    }

    public Date getUnsubscriptionDate() {
        return unsubscriptionDate;
    }

    public void setUnsubscriptionDate(Date unsubscriptionDate) {
        this.unsubscriptionDate = unsubscriptionDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public boolean isSubscribed() {
        return isSubscribed;
    }

    public void setSubscribed(boolean subscribed) {
        isSubscribed = subscribed;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public SubscriptionTypeEntity getSubscriptionTypeEntity() {
        return subscriptionTypeEntity;
    }

    public void setSubscriptionTypeEntity(SubscriptionTypeEntity subscriptionTypeEntity) {
        this.subscriptionTypeEntity = subscriptionTypeEntity;
    }

    public Set<SubscriptionOrganizationEntity> getSubscriptionOrganizationEntities() {
        return subscriptionOrganizationEntities;
    }

    public void setSubscriptionOrganizationEntities(Set<SubscriptionOrganizationEntity> subscriptionOrganizationEntities) {
        this.subscriptionOrganizationEntities = subscriptionOrganizationEntities;
    }
}
