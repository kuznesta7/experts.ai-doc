package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ItemOrganizationDao;
import ai.unico.platform.store.entity.ItemOrganizationEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Set;

public class ItemOrganizationDaoImpl implements ItemOrganizationDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void saveItemOrganization(ItemOrganizationEntity itemOrganizationEntity) {
        entityManager.persist(itemOrganizationEntity);
    }

    @Override
    public List<ItemOrganizationEntity> findItemOrganizations(Long itemId) {
        final Criteria criteria = prepareCriteria(itemId);
        return criteria.list();
    }

    @Override
    public ItemOrganizationEntity findItemOrganization(Long organizationId, Long itemId) {
        final Criteria criteria = prepareCriteria(itemId);
        criteria.add(Restrictions.eq("organizationEntity.organizationId", organizationId));
        return (ItemOrganizationEntity) criteria.uniqueResult();
    }

    @Override
    public List<ItemOrganizationEntity> findItemOrganizations(Long organizationId, Set<Long> itemIds) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ItemOrganizationEntity.class, "itemOrganization");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.in("itemEntity.itemId", itemIds));
        criteria.add(Restrictions.eq("organizationEntity.organizationId", organizationId));
        return (List<ItemOrganizationEntity>) criteria.list();
    }

    private Criteria prepareCriteria(Long itemId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ItemOrganizationEntity.class, "itemOrganization");
        criteria.add(Restrictions.eq("itemEntity.itemId", itemId));
        criteria.add(Restrictions.eq("deleted", false));
        return criteria;
    }

    @Override
    public List<ItemOrganizationEntity> findExternalItemOrganization(Set<Long> itemIds) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ItemOrganizationEntity.class, "itemOrganization");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.in("externalItemId", itemIds));
        return (List<ItemOrganizationEntity>) criteria.list();
    }
}
