package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.PublicCommercializationProjectRequestEntity;

public interface PublicCommercializationRequestDao {

    void save(PublicCommercializationProjectRequestEntity entity);

}
