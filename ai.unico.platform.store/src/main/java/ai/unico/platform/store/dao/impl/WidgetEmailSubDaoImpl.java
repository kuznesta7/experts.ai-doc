package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.WidgetEmailSubDao;
import ai.unico.platform.store.entity.WidgetEmailSubEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Beka.Saldadze
 * Date: 21.03.2022
 */
public class WidgetEmailSubDaoImpl implements WidgetEmailSubDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void save(WidgetEmailSubEntity widgetEmailSubEntity) {
        entityManager.persist(widgetEmailSubEntity);
    }

    @Override
    public void widgetSubscribe(WidgetEmailSubEntity widgetEmailSubEntity) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(WidgetEmailSubEntity.class, "widgetEmailSubEntity");
        criteria.add(Restrictions.eq("widgetEmailSubEntity.email", widgetEmailSubEntity.getEmail()).ignoreCase());
        criteria.add(Restrictions.eq("widgetEmailSubEntity.organizationId", widgetEmailSubEntity.getOrganizationId()));
        final WidgetEmailSubEntity result = (WidgetEmailSubEntity) criteria.uniqueResult();
        if (result != null) {
            if (!result.isSubscribed()) {
                result.setSubscribed(true);
                entityManager.persist(result);
            } else {
                throw new EntityAlreadyExistsException(widgetEmailSubEntity.getEmail() + " is already subscribed!");
            }
        } else {
            entityManager.persist(widgetEmailSubEntity);
        }
    }

    @Override
    public WidgetEmailSubEntity getWidgetEmailSubEntity(String email) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(WidgetEmailSubEntity.class, "widgetEmailSubEntity");
        criteria.add(Restrictions.eq("widgetEmailSubEntity.email", email).ignoreCase());
        return (WidgetEmailSubEntity) criteria.uniqueResult();    }

    @Override
    public WidgetEmailSubEntity getWidgetEmailSubEntity(String email, Long organizationId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(WidgetEmailSubEntity.class, "widgetEmailSubEntity");
        criteria.add(Restrictions.eq("widgetEmailSubEntity.email", email).ignoreCase());
        criteria.add(Restrictions.eq("widgetEmailSubEntity.organizationId", organizationId));
        return (WidgetEmailSubEntity) criteria.uniqueResult();
    }
}
