package ai.unico.platform.store.service;

import ai.unico.platform.search.api.dto.*;
import ai.unico.platform.search.api.service.*;
import ai.unico.platform.store.api.dto.*;
import ai.unico.platform.store.api.enums.WidgetTab;
import ai.unico.platform.store.api.service.EvidenceStatisticsService;
import ai.unico.platform.store.api.service.ItemTypeService;
import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.store.dao.InteractionWidgetDao;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.dao.ProfileVisitationDao;
import ai.unico.platform.store.dao.SearchHistoryStatisticsDao;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import org.cache2k.Cache;
import org.cache2k.Cache2kBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

public class EvidenceStatisticsServiceImpl implements EvidenceStatisticsService {

    private final EvidenceProjectService evidenceProjectService = ServiceRegistryProxy.createProxy(EvidenceProjectService.class);
    private final EvidenceCommercializationService evidenceCommercializationService = ServiceRegistryProxy.createProxy(EvidenceCommercializationService.class);
    private final OpportunitySearchService opportunityService = ServiceRegistryProxy.createProxy(OpportunitySearchService.class);
    private final EquipmentSearchService equipmentSearchService = ServiceRegistryProxy.createProxy(EquipmentSearchService.class);

    @Autowired
    private ProfileVisitationDao profileVisitationDao;
    @Autowired
    private SearchHistoryStatisticsDao searchHistoryStatisticsDao;
    @Autowired
    private OrganizationDao organizationDao;
    @Autowired
    private ItemTypeService itemTypeService;
    @Autowired
    private OrganizationStructureService organizationStructureService;
    @Autowired
    private InteractionWidgetDao interactionWidgetDao;

    private final Cache<Long, EvidenceStatisticsHeaderDto> headerCache = new Cache2kBuilder<Long, EvidenceStatisticsHeaderDto>() {
    }
            .expireAfterWrite(30, TimeUnit.MINUTES)     // expire/refresh after 30 minutes
            .resilienceDuration(30, TimeUnit.SECONDS)   // cope with at most 30 seconds
            .loader(this::findHeader)                         // auto populating function
            .build();
    private final Cache<Long, OrganizationExpertsDto> organizationExpertIdsCache = new Cache2kBuilder<Long, OrganizationExpertsDto>() {
    }
            .expireAfterWrite(30, TimeUnit.MINUTES)     // expire/refresh after 30 minutes
            .resilienceDuration(30, TimeUnit.SECONDS)   // cope with at most 30 seconds
            .loader(this::findOrganizationExperts)                         // auto populating function
            .build();

    @Override
    @Transactional
    public EvidenceStatisticsHeaderDto findStatisticsHeader(Long organizationId) {
        return headerCache.get(organizationId);
    }

    @Override
    @Transactional
    public EvidenceExpertStatisticsDto findExpertStatistics(Long organizationId) throws IOException {
        final EvidenceExpertStatisticsDto evidenceExpertStatisticsDto = new EvidenceExpertStatisticsDto();
        final EvidenceExpertService evidenceExpertService = ServiceRegistryUtil.getService(EvidenceExpertService.class);
        final ExpertStatisticsDto expertStatistics = evidenceExpertService.findExpertStatistics(findOrganizationWithUnitIds(organizationId));
        evidenceExpertStatisticsDto.setNumberOfExperts(expertStatistics.getNumberOfAllExperts());
        evidenceExpertStatisticsDto.setNumberOfRegisteredExperts(expertStatistics.getNumberOfVerifiedExperts());
        return evidenceExpertStatisticsDto;
    }

    @Override
    @Transactional
    public EvidenceExpertVisitationStatisticsDto findExpertVisitationStatistics(Long organizationId, Integer page, Integer limit) throws IOException {
        final EvidenceExpertVisitationStatisticsDto visitationStatisticsDto = new EvidenceExpertVisitationStatisticsDto();
        final ExpertLookupService expertLookupService = ServiceRegistryUtil.getService(ExpertLookupService.class);
        final OrganizationExpertsDto organizationExperts = organizationExpertIdsCache.get(organizationId);
        final Set<Long> expertIds = organizationExperts.getExpertIds();
        final Set<Long> userIds = organizationExperts.getUserIds();
        visitationStatisticsDto.setPage(page);
        visitationStatisticsDto.setLimit(limit);
        if (!userIds.isEmpty() || !expertIds.isEmpty()) {
            visitationStatisticsDto.setNumberOfProfilesVisited(profileVisitationDao.countProfileVisitationForExperts(userIds, expertIds));
            final List<ProfileVisitationDao.ProfileVisitationStatistics> mostVisitedProfiles = profileVisitationDao.findMostVisitedProfilesForExperts(userIds, expertIds, page, limit);
            final Set<Long> expertToLoadIds = mostVisitedProfiles.stream().map(ProfileVisitationDao.ProfileVisitationStatistics::getExpertId).filter(Objects::nonNull).collect(Collectors.toSet());
            final List<UserExpertBaseDto> expertDtos = expertLookupService.findExpertBases(expertToLoadIds);
            for (ProfileVisitationDao.ProfileVisitationStatistics profileVisitation : mostVisitedProfiles) {
                final EvidenceExpertVisitationStatisticsDto.ProfileVisitationStatisticsDto profileVisitationStatisticsDto = new EvidenceExpertVisitationStatisticsDto.ProfileVisitationStatisticsDto();
                final Long userId = profileVisitation.getUserId();
                final Long expertId = profileVisitation.getExpertId();
                profileVisitationStatisticsDto.setExpertId(expertId);
                profileVisitationStatisticsDto.setUserId(userId);
                profileVisitationStatisticsDto.setNumberOfVisitations(profileVisitation.getVisitationsCount());
                profileVisitationStatisticsDto.setFullName(profileVisitation.getName());
                if (profileVisitationStatisticsDto.getFullName() == null && expertId != null) {
                    expertDtos.stream()
                            .filter(userExpertPreviewDto -> expertId.equals(userExpertPreviewDto.getExpertId()))
                            .findFirst()
                            .ifPresent(userExpertPreviewDto -> profileVisitationStatisticsDto.setFullName(userExpertPreviewDto.getName()));
                }
                visitationStatisticsDto.getMostVisitedProfiles().add(profileVisitationStatisticsDto);
            }
        } else {
            visitationStatisticsDto.setNumberOfProfilesVisited(0L);
        }
        return visitationStatisticsDto;
    }

    @Override
    @Transactional
    public EvidenceItemStatisticsDto findItemStatistics(Long organizationId, boolean verified) throws IOException {
        final EvidenceItemStatisticsDto evidenceStatisticsDto = new EvidenceItemStatisticsDto();
        final EvidenceItemService evidenceItemService = ServiceRegistryUtil.getService(EvidenceItemService.class);
        final ItemStatisticsDto itemStatistics = evidenceItemService.findItemStatistics(findOrganizationWithUnitIds(organizationId), verified);
        final Map<Long, Map<Integer, Long>> itemTypesYearHistogram = itemStatistics.getItemTypesYearHistogram();
        final List<ItemTypeGroupDto> itemTypeGroups = itemTypeService.findItemTypeGroups();
        final List<ItemTypeDto> itemTypeDtos = itemTypeService.findItemTypes();
        final Map<Long, EvidenceItemStatisticsDto.AnalyticsItemTypeGroupDto> groupMap = new HashMap<>();
        for (ItemTypeGroupDto itemTypeGroupDto : itemTypeGroups) {
            final EvidenceItemStatisticsDto.AnalyticsItemTypeGroupDto groupDto = new EvidenceItemStatisticsDto.AnalyticsItemTypeGroupDto();
            final Long groupId = itemTypeGroupDto.getItemTypeGroupId();
            groupDto.setItemTypeGroupId(groupId);
            groupDto.setGroupTitle(itemTypeGroupDto.getItemTypeGroupTitle());
            groupDto.setNumberOfItems(0L);
            groupDto.setNumberOfImpacted(0L);
            groupDto.setNumberOfUtilityModels(0L);
            groupMap.put(groupId, groupDto);
            evidenceStatisticsDto.getItemTypeStatistics().add(groupDto);
            for (Long typeId : itemTypeGroupDto.getItemTypeIds()) {
                final Long numberOfItemsToAdd = itemStatistics.getNumberOfItemsByType().get(typeId);
                if (itemTypesYearHistogram.containsKey(typeId)) {
                    itemTypesYearHistogram.get(typeId).forEach((k, v) -> groupDto.getItemsYearHistogram().merge(k, v, Long::sum));
                }
                if (numberOfItemsToAdd != null) {
                    groupDto.setNumberOfItems(groupDto.getNumberOfItems() + numberOfItemsToAdd);
                    final Optional<ItemTypeDto> typeDtoOptional = itemTypeDtos.stream().filter(x -> x.getTypeId().equals(typeId)).findFirst();
                    if (typeDtoOptional.isPresent()) {
                        final ItemTypeDto itemTypeDto = typeDtoOptional.get();
                        if (itemTypeDto.isImpacted())
                            groupDto.setNumberOfImpacted(groupDto.getNumberOfImpacted() + numberOfItemsToAdd);
                        if (itemTypeDto.isUtilityModel())
                            groupDto.setNumberOfUtilityModels(groupDto.getNumberOfImpacted() + numberOfItemsToAdd);
                    }
                }
            }
        }
        evidenceStatisticsDto.setAllItemsYearHistogram(itemStatistics.getAllItemsYearHistogram());
        evidenceStatisticsDto.setNumberOfAllItems(itemStatistics.getNumberOfAllItems());
        return evidenceStatisticsDto;
    }

    @Override
    @Transactional
    public EvidenceItemExpertStatisticsDto findItemExpertStatistics(Long organizationId, boolean verified) throws IOException {
        final EvidenceItemService evidenceItemService = ServiceRegistryUtil.getService(EvidenceItemService.class);
        final ExpertLookupService expertLookupService = ServiceRegistryUtil.getService(ExpertLookupService.class);
        EvidenceItemExpertStatisticsDto expertStatisticsDto = new EvidenceItemExpertStatisticsDto();
        expertStatisticsDto.setOrganizationId(organizationId);
        Map<String, Long> statistics = evidenceItemService.findItemExpertStatistics(findOrganizationWithUnitIds(organizationId), verified);
        final List<UserExpertBaseDto> expertDtos = expertLookupService.findUserExpertBases(statistics.keySet());
        Long sum = 0L;

        for (UserExpertBaseDto expertDto : expertDtos) {
            EvidenceItemExpertStatisticsDto.Expert expert = new EvidenceItemExpertStatisticsDto.Expert();
            expert.setExpertId(expertDto.getExpertId());
            expert.setName(expertDto.getName());
            expert.setResultCount(statistics.get(expertDto.getExpertCode()));
            sum += expert.getResultCount();
            expertStatisticsDto.getExperts().add(expert);
        }
        expertStatisticsDto.setSum(sum);
        expertStatisticsDto.getExperts().sort((a, b) -> (int) (b.getResultCount() - a.getResultCount()));
        return expertStatisticsDto;
    }

    @Override
    @Transactional
    public EvidenceSearchedExpertStatisticsDto findSearchedExpertStatistics(Long organizationId, Integer page, Integer limit) throws IOException {
        final ExpertLookupService expertLookupService = ServiceRegistryUtil.getService(ExpertLookupService.class);
        final EvidenceSearchedExpertStatisticsDto expertStatisticsDto = new EvidenceSearchedExpertStatisticsDto();
        final OrganizationExpertsDto organizationExperts = organizationExpertIdsCache.get(organizationId);
        final Set<Long> expertIds = organizationExperts.getExpertIds();
        final Set<Long> userIds = organizationExperts.getUserIds();
        expertStatisticsDto.setPage(page);
        expertStatisticsDto.setLimit(limit);
        if (!userIds.isEmpty() || !expertIds.isEmpty()) {
            expertStatisticsDto.setNumberOfProfilesShown(searchHistoryStatisticsDao.countShownProfilesForExperts(userIds, expertIds));
            final List<SearchHistoryStatisticsDao.ShownProfileStatistics> mostShownProfilesForExperts = searchHistoryStatisticsDao.findMostShownProfilesForExperts(userIds, expertIds, page, limit);
            final Set<Long> expertToLoadIds = mostShownProfilesForExperts.stream().map(SearchHistoryStatisticsDao.ShownProfileStatistics::getExpertId).filter(Objects::nonNull).collect(Collectors.toSet());
            final List<UserExpertBaseDto> expertDtos = expertLookupService.findExpertBases(expertToLoadIds);
            for (SearchHistoryStatisticsDao.ShownProfileStatistics shownProfile : mostShownProfilesForExperts) {
                final EvidenceSearchedExpertStatisticsDto.ShownProfileStatisticsDto shownProfileDto = new EvidenceSearchedExpertStatisticsDto.ShownProfileStatisticsDto();
                shownProfileDto.setUserId(shownProfile.getUserId());
                shownProfileDto.setExpertId(shownProfile.getExpertId());
                shownProfileDto.setAvgPosition(shownProfile.getAvgPosition());
                shownProfileDto.setFullName(shownProfile.getName());
                shownProfileDto.setBestPosition(shownProfile.getBestPosition());
                shownProfileDto.setShownCount(shownProfile.getShownCount());
                if (shownProfile.getName() == null && shownProfile.getExpertId() != null) {
                    expertDtos.stream()
                            .filter(userExpertPreviewDto -> shownProfile.getExpertId().equals(userExpertPreviewDto.getExpertId()))
                            .findFirst()
                            .ifPresent(userExpertPreviewDto -> shownProfileDto.setFullName(userExpertPreviewDto.getName()));
                }
                expertStatisticsDto.getMostShownProfiles().add(shownProfileDto);
            }
        } else {
            expertStatisticsDto.setNumberOfProfilesShown(0L);
        }
        return expertStatisticsDto;
    }

    @Override
    @Transactional
    public EvidenceSearchedKeywordStatisticsDto findSearchedKeywordStatistics(Long organizationId, Integer page, Integer limit) {
        final EvidenceSearchedKeywordStatisticsDto keywordStatisticsDto = new EvidenceSearchedKeywordStatisticsDto();
        final OrganizationExpertsDto organizationExperts = organizationExpertIdsCache.get(organizationId);
        final Set<Long> expertIds = organizationExperts.getExpertIds();
        final Set<Long> userIds = organizationExperts.getUserIds();
        keywordStatisticsDto.setLimit(limit);
        keywordStatisticsDto.setPage(page);
        if (!userIds.isEmpty() || !expertIds.isEmpty()) {
            keywordStatisticsDto.setNumberOfSearchedKeywords(searchHistoryStatisticsDao.countSearchedKeywordsForExperts(userIds, expertIds));
            final List<SearchHistoryStatisticsDao.SearchedKeyword> searchedKeywordsStatisticsForExperts = searchHistoryStatisticsDao.findSearchedKeywordsStatisticsForExperts(userIds, expertIds, page, limit);
            for (SearchHistoryStatisticsDao.SearchedKeyword searchedKeyword : searchedKeywordsStatisticsForExperts) {
                final EvidenceSearchedKeywordStatisticsDto.SearchedKeywordDto searchedKeywordDto = new EvidenceSearchedKeywordStatisticsDto.SearchedKeywordDto();
                searchedKeywordDto.setQuery(searchedKeyword.getQuery());
                searchedKeywordDto.setAvgPosition(searchedKeyword.getAvgPosition());
                searchedKeywordDto.setNumberOfExpertsShown(searchedKeyword.getNumberOfExpertsShown());
                searchedKeywordDto.setSearchCount(searchedKeyword.getSearchCount());
                searchedKeywordDto.setBestPosition(searchedKeyword.getBestPosition());
                keywordStatisticsDto.getMostSearchedKeywords().add(searchedKeywordDto);
            }
        } else {
            keywordStatisticsDto.setNumberOfSearchedKeywords(0L);
        }
        return keywordStatisticsDto;
    }

    @Override
    @Transactional
    public EvidenceProjectStatisticsDto findProjectStatistics(Long organizationId) throws IOException {
        final ProjectStatisticsDto projectStatistics = evidenceProjectService.findProjectStatistics(findOrganizationWithUnitIds(organizationId));
        final EvidenceProjectStatisticsDto evidenceProjectStatisticsDto = new EvidenceProjectStatisticsDto();
        evidenceProjectStatisticsDto.setAllProjectsFinancesSum(projectStatistics.getAllProjectsFinancesSum());
        evidenceProjectStatisticsDto.setNumberOfAllProjects(projectStatistics.getNumberOfAllProjects());
        evidenceProjectStatisticsDto.setYearProjectFinancesSumHistogram(projectStatistics.getYearProjectFinancesSumHistogram());
        return evidenceProjectStatisticsDto;
    }

    @Override
    @Transactional
    public EvidenceProjectStatisticsDto findProjectCumulativeStatistics(Long organizationId) throws IOException {
        final ProjectStatisticsDto projectStatistics = evidenceProjectService.findProjectStatistics(findOrganizationWithUnitIds(organizationId));
        final EvidenceProjectStatisticsDto evidenceProjectStatisticsDto = new EvidenceProjectStatisticsDto();
        projectStatistics.transformToCumulativeSum();
        evidenceProjectStatisticsDto.setAllProjectsFinancesSum(projectStatistics.getAllProjectsFinancesSum());
        evidenceProjectStatisticsDto.setNumberOfAllProjects(projectStatistics.getNumberOfAllProjects());
        evidenceProjectStatisticsDto.setYearProjectFinancesSumHistogram(projectStatistics.getYearProjectFinancesSumHistogram());
        return evidenceProjectStatisticsDto;
    }

    @Override
    @Transactional
    public EvidenceCommercializationStatisticsDto findCommercializationStatistics(Long organizationId) throws IOException {
        final CommercializationStatisticsDto commercializationStatistics = evidenceCommercializationService.findCommercializationStatistics(findOrganizationWithUnitIds(organizationId));
        final EvidenceCommercializationStatisticsDto evidenceCommercializationStatisticsDto = new EvidenceCommercializationStatisticsDto();
        evidenceCommercializationStatisticsDto.setNumberOfAllProjects(commercializationStatistics.getNumberOfAllProjects());
        return evidenceCommercializationStatisticsDto;
    }

    @Override
    @Transactional
    public EvidenceOpportunityStatisticsDto findOpportunityStatistics(Long organizationId) throws IOException {
        final OpportunityStatisticsDto opportunityStatisticsDto = opportunityService.findOpportunityStatistics(findOrganizationWithUnitIds(organizationId));
        final EvidenceOpportunityStatisticsDto evidenceOpportunityStatisticsDto = new EvidenceOpportunityStatisticsDto();
        evidenceOpportunityStatisticsDto.setNumberOfAllOpportunities(opportunityStatisticsDto.getNumberOfAllOpportunities());
        return evidenceOpportunityStatisticsDto;
    }

    @Override
    @Transactional
    public EvidenceMemberStatisticsDto findMemberStatistics(Long organizationId) {
        final EvidenceMemberStatisticsDto evidenceMemberStatisticsDto = new EvidenceMemberStatisticsDto();
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        final List<Long> memberIds = organizationStructureService.findMemberOrganizations(organizationId);
        final Set<Long> membershipIds = organizationStructureService.findMyMemberships(organizationId);
        evidenceMemberStatisticsDto.setMemberCount((long) memberIds.size());
        evidenceMemberStatisticsDto.setMembershipCount((long) membershipIds.size());
        evidenceMemberStatisticsDto.setAllowedMembers(organizationEntity.getAllowedMembers());
        return evidenceMemberStatisticsDto;
    }

    @Override
    @Transactional
    public EvidenceBasicStatisticsDto findServiceEquipmentStatistics(Long organizationId) throws IOException {
        final EquipmentSearchService equipmentSearchService = ServiceRegistryUtil.getService(EquipmentSearchService.class);
        final BasicStatisticsDto basicStatisticsDto = equipmentSearchService.findEquipmentStatistics(findOrganizationWithUnitIds(organizationId));
        final EvidenceBasicStatisticsDto evidenceBasicStatisticsDto = new EvidenceBasicStatisticsDto();
        evidenceBasicStatisticsDto.setNumOfAllItems(basicStatisticsDto.getNumOfAllItems());
        return evidenceBasicStatisticsDto;
    }

    @Override
    @Transactional
    public List<OrganizationSumFinancesDto> findUnitsProjectStatistics(Long organizationId, Integer year) throws IOException {
        OrganizationStructureDto structureDtos = organizationStructureService.findLowerOrganizationStructure(organizationId, true);
        Map<Long, Long> upperOrganization = new HashMap<>();
        List<OrganizationSumFinancesDto> result = new ArrayList<>();
        for (OrganizationStructureDto dto : structureDtos.getOrganizationUnits()) {
            upperOrganization.put(dto.getOrganizationId(), dto.getOrganizationId());
            fillStructure(dto, upperOrganization);
        }
        Set<Long> organizationIds = upperOrganization.keySet();
        Map<Long, Double> idAmount = evidenceProjectService.findProjectStatisticsPerCompany(organizationIds, year);
        Map<Long, Double> translatedIdAmount = new HashMap<>();
        idAmount.forEach((x, y) -> translatedIdAmount.merge(upperOrganization.get(x), y, Double::sum));
        for (OrganizationStructureDto dto : structureDtos.getOrganizationUnits()) {
            OrganizationSumFinancesDto financesDto = new OrganizationSumFinancesDto();
            financesDto.setOrganizationId(dto.getOrganizationId());
            financesDto.setOrganizationName(dto.getOrganizationName());
            financesDto.setOrganizationAbbrev(dto.getOrganizationAbbrev());
            financesDto.setAmount(translatedIdAmount.getOrDefault(dto.getOrganizationId(), 0D));
            result.add(financesDto);
        }
        return result;
    }

    @Override
    @Transactional
    public List<YearCooperatingOrganizationsDto> findCooperatingOrganizations(Long organizationId, Integer yearFrom, Integer yearTo) throws IOException {
        final Set<Long> organizationWithUnitIds = findOrganizationWithUnitIds(organizationId);
        final List<OrganizationCostWrapperDto> topPartners = evidenceProjectService.findTopPartners(organizationWithUnitIds, yearFrom, yearTo);
        final Set<Long> organizationsIds = new HashSet<>();
        final List<YearCooperatingOrganizationsDto> result = new ArrayList<>();
        topPartners.forEach(x -> organizationsIds.addAll(x.getValues().stream().map(OrganizationCostDto::getOrganizationId).collect(Collectors.toList())));
        final List<OrganizationEntity> organizations = organizationDao.findByIds(organizationsIds);
        final Map<Long, OrganizationEntity> organizationMap = organizations.stream().collect(Collectors.toMap(OrganizationEntity::getOrganizationId, Function.identity()));
        for (OrganizationCostWrapperDto wrapperDto : topPartners) {
            YearCooperatingOrganizationsDto dto = new YearCooperatingOrganizationsDto();
            dto.setYear(wrapperDto.getYear());
            dto.setOthers(wrapperDto.getOthers());
            for (OrganizationCostDto costDto : wrapperDto.getValues()) {
                OrganizationSumFinancesDto financesDto = new OrganizationSumFinancesDto();
                financesDto.setAmount(costDto.getCost());
                financesDto.setOrganizationId(costDto.getOrganizationId());
                final OrganizationEntity entity = organizationMap.get(costDto.getOrganizationId());
                financesDto.setOrganizationName(entity.getOrganizationName());
                financesDto.setOrganizationAbbrev(entity.getOrganizationAbbrev());
                dto.getOrganizations().add(financesDto);
            }
            result.add(dto);
        }
        result.sort(Comparator.comparing(YearCooperatingOrganizationsDto::getYear));
        return result;
    }


    private void fillStructure(OrganizationStructureDto structureDto, Map<Long, Long> translations) {
        for (OrganizationStructureDto dto : structureDto.getOrganizationUnits()) {
            translations.put(dto.getOrganizationId(), translations.get(structureDto.getOrganizationId()));
            if (!dto.getOrganizationUnits().isEmpty())
                fillStructure(dto, translations);
        }
    }


    private EvidenceStatisticsHeaderDto findHeader(Long organizationId) {
        final EvidenceStatisticsHeaderDto evidenceStatisticsHeaderDto = new EvidenceStatisticsHeaderDto();
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
        final Set<Long> organizationUnitIds = organizationStructureService.findChildOrganizationIds(organizationId, true);
        evidenceStatisticsHeaderDto.setOrganizationId(organizationId);
        evidenceStatisticsHeaderDto.setOrganizationName(organizationEntity.getOrganizationName());
        evidenceStatisticsHeaderDto.setOrganizationUnitIds(organizationUnitIds);
        return evidenceStatisticsHeaderDto;
    }

    private Set<Long> findOrganizationWithUnitIds(Long organizationId) {
        final EvidenceStatisticsHeaderDto evidenceStatisticsHeaderDto = headerCache.get(organizationId);
        final Set<Long> result = new HashSet<>(evidenceStatisticsHeaderDto.getOrganizationUnitIds());
        result.add(evidenceStatisticsHeaderDto.getOrganizationId());
        return result;
    }

    private OrganizationExpertsDto findOrganizationExperts(Long organizationId) throws IOException {
        final ExpertLookupService expertSearchService = ServiceRegistryUtil.getService(ExpertLookupService.class);
        final OrganizationExpertsDto organizationsExpertIds = expertSearchService.findOrganizationsExpertIds(findOrganizationWithUnitIds(organizationId));
        return organizationsExpertIds;
    }

    @Override
    @Transactional
    public Map<String, Long> findWidgetInteractionHistory(Long organizationId, WidgetTab widgetTab) {
        return interactionWidgetDao.findOrganizationInteractionsCount(organizationId, widgetTab);
    }
}
