package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.UserEquipmentDao;
import ai.unico.platform.store.entity.UserEquipmentEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class UserEquipmentDaoImpl implements UserEquipmentDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(UserEquipmentEntity userEquipmentEntity) {
        entityManager.persist(userEquipmentEntity);
    }

    @Override
    public List<UserEquipmentEntity> findUsersForEquipment(Long equipmentId) {
        final Criteria criteria = prepareCriteria();
        criteria.add(Restrictions.eq("userEquipmentEntity.equipmentEntity.id", equipmentId));
        return criteria.list();
    }

    @Override
    public List<UserEquipmentEntity> findEquipmentForUsers(Long userId) {
        final Criteria criteria = prepareCriteria();
        criteria.add(Restrictions.eq("userEquipmentEntity.userProfileEntity.userId", userId));
        return criteria.list();
    }

    @Override
    public UserEquipmentEntity find(Long equipmentId, Long userId) {
        final Criteria criteria = prepareCriteria();
        criteria.add(Restrictions.eq("userEquipmentEntity.equipmentEntity.id", equipmentId));
        criteria.add(Restrictions.eq("userEquipmentEntity.userProfile.userId", userId));
        try {
            return (UserEquipmentEntity) criteria.uniqueResult();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            return null;
        }
    }

    private Criteria prepareCriteria() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserEquipmentEntity.class, "userEquipmentEntity");
        criteria.add(Restrictions.eq("userEquipmentEntity.deleted", false));
        return criteria;
    }
}
