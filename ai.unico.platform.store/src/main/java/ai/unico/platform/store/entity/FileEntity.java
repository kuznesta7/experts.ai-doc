package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "file")
public class FileEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long fileId;

    private String name;

    @Basic(fetch = FetchType.LAZY)
    private byte[] content;

    @Generated(GenerationTime.ALWAYS)
    private String hash;

    @Generated(GenerationTime.ALWAYS)
    private Integer size;

    @OneToMany(mappedBy = "fileEntity")
    private Set<FileAccessEntity> fileAccessEntities;

    private boolean deleted;

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getHash() {
        return hash;
    }

    public Set<FileAccessEntity> getFileAccessEntities() {
        return fileAccessEntities;
    }

    public void setFileAccessEntities(Set<FileAccessEntity> fileAccessEntities) {
        this.fileAccessEntities = fileAccessEntities;
    }

    public Integer getSize() {
        return size;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
