package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.QueryTranslationEntity;

import java.util.List;

public interface QueryTranslationDao {

    QueryTranslationEntity translate(String query);

    List<QueryTranslationEntity> findAllTranslations();

    void saveTranslations(QueryTranslationEntity translationEntity);

    QueryTranslationEntity find(Long id);

}
