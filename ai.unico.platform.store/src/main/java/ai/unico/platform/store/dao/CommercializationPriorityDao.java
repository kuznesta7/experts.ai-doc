package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.CommercializationPriorityEntity;

import java.util.List;

public interface CommercializationPriorityDao {

    CommercializationPriorityEntity findPriority(Long priorityId);

    List<CommercializationPriorityEntity> findAll();

    void save(CommercializationPriorityEntity commercializationPriorityEntity);

}
