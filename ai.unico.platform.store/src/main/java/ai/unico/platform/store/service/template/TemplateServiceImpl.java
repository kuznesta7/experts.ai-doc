package ai.unico.platform.store.service.template;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Collections;
import java.util.Map;
import java.util.regex.Pattern;

public class TemplateServiceImpl implements TemplateService {

    private static final Logger LOG = LoggerFactory.getLogger(TemplateServiceImpl.class);

    @Override
    public String getTemplate(String id) {
        return getReplacedTemplate(id, Collections.emptyMap());
    }

    @Override
    public String getReplacedTemplate(String id, Map<String, String> variables) {
        final String path = "templates/" + id;

        try (final InputStream inputStream = TemplateServiceImpl.class.getClassLoader().getResourceAsStream(path)) {
            final String content = IOUtils.toString(inputStream, "UTF-8");
            return replace(content, variables);
        } catch (Exception e) {
            LOG.error("err", e);
            return "";
        }
    }

    private String replace(String content, Map<String, String> variables) {
        for (Map.Entry<String, String> stringStringEntry : variables.entrySet()) {
            final String value = stringStringEntry.getValue();
            content = content.replaceAll(Pattern.quote("{{" + stringStringEntry.getKey() + "}}"), value != null ? value : "");
        }
        return content;
    }
}
