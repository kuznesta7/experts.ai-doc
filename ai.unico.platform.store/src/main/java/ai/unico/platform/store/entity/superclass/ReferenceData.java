package ai.unico.platform.store.entity.superclass;

import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
public abstract class ReferenceData extends Trackable {

    private String code;

    private String name;

    private String description;

    private boolean systemData;

    private Date activeFrom = new Date();

    private Date activeUntil;

    private Integer displayOrder = 100;

    private boolean deleted;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSystemData() {
        return systemData;
    }

    public void setSystemData(boolean systemData) {
        this.systemData = systemData;
    }

    public Date getActiveFrom() {
        return activeFrom;
    }

    public void setActiveFrom(Date activeFrom) {
        this.activeFrom = activeFrom;
    }

    public Date getActiveUntil() {
        return activeUntil;
    }

    public void setActiveUntil(Date activeUntil) {
        this.activeUntil = activeUntil;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
