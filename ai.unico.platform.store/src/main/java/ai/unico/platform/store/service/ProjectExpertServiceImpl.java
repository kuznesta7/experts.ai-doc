package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.ProjectExpertDto;
import ai.unico.platform.store.api.service.ProjectExpertService;
import ai.unico.platform.store.dao.ProjectDao;
import ai.unico.platform.store.dao.UserExpertProjectDao;
import ai.unico.platform.store.entity.ProjectEntity;
import ai.unico.platform.store.entity.UserExpertProjectEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class ProjectExpertServiceImpl implements ProjectExpertService {

    @Autowired
    private UserExpertProjectDao projectUserDao;

    @Autowired
    private ProjectDao projectDao;

    @Override
    @Transactional
    public void saveExpertToProject(ProjectExpertDto projectExpertDto, UserContext userContext) throws NonexistentEntityException {
        final Long expertId = projectExpertDto.getExpertId();
        final Long projectId = projectExpertDto.getProjectId();
        final String label = projectExpertDto.getLabel();
        final UserExpertProjectEntity existingEntity = projectUserDao.findForExpert(expertId, projectId);
        if (existingEntity != null) throw new EntityAlreadyExistsException(UserExpertProjectEntity.class);

        final UserExpertProjectEntity userExpertProjectEntity = new UserExpertProjectEntity();
        final ProjectEntity projectEntity = projectDao.find(projectId);
        if (projectEntity == null) throw new NonexistentEntityException(ProjectEntity.class);
        userExpertProjectEntity.setLabel(label);
        userExpertProjectEntity.setExpertId(expertId);
        userExpertProjectEntity.setProjectEntity(projectEntity);

        TrackableUtil.fillCreate(userExpertProjectEntity, userContext);
        projectUserDao.saveProjectUserExpert(userExpertProjectEntity);
    }

    @Override
    @Transactional
    public void removeExpertFromProject(ProjectExpertDto projectExpertDto, UserContext userContext) throws NonexistentEntityException {
        final Long expertId = projectExpertDto.getExpertId();
        final Long projectId = projectExpertDto.getProjectId();
        final UserExpertProjectEntity existingEntity = projectUserDao.findForExpert(expertId, projectId);
        if (existingEntity == null) throw new NonexistentEntityException(UserExpertProjectEntity.class);
        existingEntity.setDeleted(true);
        TrackableUtil.fillUpdate(existingEntity, userContext);
    }
}
