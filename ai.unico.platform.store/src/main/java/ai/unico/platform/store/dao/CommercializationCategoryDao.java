package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.CommercializationCategoryEntity;

import java.util.List;

public interface CommercializationCategoryDao {

    CommercializationCategoryEntity findCategory(Long categoryId);

    List<CommercializationCategoryEntity> findAll();

    void save(CommercializationCategoryEntity commercializationCategoryEntity);
}
