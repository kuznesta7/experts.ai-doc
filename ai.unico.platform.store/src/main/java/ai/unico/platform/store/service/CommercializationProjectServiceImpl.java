package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.CommercializationProjectDto;
import ai.unico.platform.store.api.dto.CommercializationProjectIndexDto;
import ai.unico.platform.store.api.service.CommercializationProjectService;
import ai.unico.platform.store.api.service.CommercializationStatusService;
import ai.unico.platform.store.dao.*;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.store.util.CommercializationProjectIndexUtil;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.store.util.ParameterValidationUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.enums.CommercializationStatus;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CommercializationProjectServiceImpl implements CommercializationProjectService {

    @Autowired
    private CommercializationDomainDao commercializationDomainDao;

    @Autowired
    private CommercializationPriorityDao commercializationPriorityDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private CommercializationProjectDao commercializationProjectDao;

    @Autowired
    private CommercializationStatusDao commercializationStatusDao;

    @Autowired
    private TagDao tagDao;

    @Autowired
    private CommercializationProjectCategoryDao commercializationProjectCategoryDao;

    @Autowired
    private CommercializationStatusService commercializationStatusService;

    @Override
    @Transactional
    public Long createCommercializationProject(CommercializationProjectDto commercializationProjectDto, UserContext userContext) {
        final CommercializationProjectEntity commercializationProjectEntity = new CommercializationProjectEntity();
        final Long ownerOrganizationId = commercializationProjectDto.getOwnerOrganizationId();
        if (ownerOrganizationId != null) {
            final OrganizationEntity ownerOrganizationEntity = organizationDao.find(ownerOrganizationId);
            if (ownerOrganizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
            commercializationProjectEntity.setOwnerOrganizationEntity(ownerOrganizationEntity);
        }
        fillEntity(commercializationProjectEntity, commercializationProjectDto);
        ParameterValidationUtil.checkIfRequiredParametersPresent(commercializationProjectEntity, "baseRequired");
        TrackableUtil.fillCreate(commercializationProjectEntity, userContext);

        final CommercializationStatusTypeEntity statusTypeEntity = commercializationStatusDao.find(CommercializationStatus.DRAFT);
        final CommercializationProjectStatusEntity projectStatusEntity = new CommercializationProjectStatusEntity();
        projectStatusEntity.setCommercializationStatusTypeEntity(statusTypeEntity);
        projectStatusEntity.setCommercializationProjectEntity(commercializationProjectEntity);
        TrackableUtil.fillCreate(projectStatusEntity, userContext);
        commercializationProjectDao.save(projectStatusEntity);

        commercializationProjectEntity.getCommercializationProjectStatusEntities().add(projectStatusEntity);
        CommercializationProjectIndexUtil.index(commercializationProjectEntity);
        return commercializationProjectEntity.getCommercializationId();
    }

    @Override
    @Transactional
    public void updateCommercializationProject(Long commercializationProjectId, CommercializationProjectDto commercializationProjectDto, UserContext userContext) {
        final CommercializationProjectEntity commercializationProjectEntity = commercializationProjectDao.find(commercializationProjectId);
        if (commercializationProjectEntity == null)
            throw new NonexistentEntityException(CommercializationProjectEntity.class);
        final CommercializationProjectStatusEntity lastStatus = TrackableUtil.findLast(commercializationProjectEntity.getCommercializationProjectStatusEntities());
        final boolean mustBeFilled = lastStatus.getCommercializationStatusTypeEntity().isMustBeFilled();
        fillEntity(commercializationProjectEntity, commercializationProjectDto);
        ParameterValidationUtil.checkIfRequiredParametersPresent(commercializationProjectEntity, mustBeFilled ? "allRequired" : "baseRequired");
        TrackableUtil.fillUpdate(commercializationProjectEntity, userContext);
        CommercializationProjectIndexUtil.index(commercializationProjectEntity);
        commercializationStatusDao.find(commercializationProjectDto.getCommercializationStatusId());
        commercializationStatusService.setStatus(commercializationProjectId, commercializationProjectDto.getCommercializationStatusId(), userContext);
    }

    @Override
    @Transactional
    public List<CommercializationProjectIndexDto> findCommercializationProjects(Integer limit, Integer offset) {
        final List<CommercializationProjectEntity> allProjects = commercializationProjectDao.findAll(limit, offset);
        return allProjects.stream().map(CommercializationProjectIndexUtil::convertIndex).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void loadAndIndexCommercializationProject(Long commercializationProjectId) {
        final CommercializationProjectEntity commercializationProjectEntity = commercializationProjectDao.find(commercializationProjectId);
        CommercializationProjectIndexUtil.index(commercializationProjectEntity);
    }

    private void fillEntity(CommercializationProjectEntity commercializationProjectEntity, CommercializationProjectDto commercializationProjectDto) {
        CommercializationDomainEntity commercializationDomainEntity = null;
        if (commercializationProjectDto.getCommercializationDomainId() != null) {
            commercializationDomainEntity = commercializationDomainDao.findDomain(commercializationProjectDto.getCommercializationDomainId());
        }
        CommercializationPriorityEntity commercializationPriorityEntity = null;
        if (commercializationProjectDto.getCommercializationPriorityId() != null) {
            commercializationPriorityEntity = commercializationPriorityDao.findPriority(commercializationProjectDto.getCommercializationPriorityId());
        }
        commercializationProjectEntity.setCommercializationDomainEntity(commercializationDomainEntity);
        commercializationProjectEntity.setCommercializationPriorityEntity(commercializationPriorityEntity);

        commercializationProjectEntity.setCommercializationInvestmentFrom(commercializationProjectDto.getCommercializationInvestmentFrom());
        commercializationProjectEntity.setCommercializationInvestmentTo(commercializationProjectDto.getCommercializationInvestmentTo());
        commercializationProjectEntity.setName(commercializationProjectDto.getName());
        commercializationProjectEntity.setExecutiveSummary(commercializationProjectDto.getExecutiveSummary());
        commercializationProjectEntity.setCompetitiveAdvantage(commercializationProjectDto.getCompetitiveAdvantage());
        commercializationProjectEntity.setPainDescription(commercializationProjectDto.getPainDescription());
        commercializationProjectEntity.setTechnicalPrinciples(commercializationProjectDto.getTechnicalPrinciples());
        commercializationProjectEntity.setUseCase(commercializationProjectDto.getUseCase());
        commercializationProjectEntity.setEndDate(commercializationProjectDto.getEndDate());
        commercializationProjectEntity.setStartDate(commercializationProjectDto.getStartDate());
        commercializationProjectEntity.setStatusDeadline(commercializationProjectDto.getStatusDeadline());
        commercializationProjectEntity.setTechnologyReadinessLevel(commercializationProjectDto.getTechnologyReadinessLevel());
        commercializationProjectEntity.setIdeaScore(commercializationProjectDto.getIdeaScore());
        commercializationProjectEntity.setLink(commercializationProjectDto.getLink());
        commercializationProjectDao.save(commercializationProjectEntity);

        final Set<CommercializationProjectCategoryEntity> commercializationProjectCategoryEntities = commercializationProjectEntity.getCommercializationProjectCategoryEntities();
        final Set<Long> newCategories = commercializationProjectDto.getCommercializationCategoryIds();

        for (CommercializationProjectCategoryEntity cpe : commercializationProjectCategoryEntities) {
            final Long commercializationCategoryId = HibernateUtil.getId(cpe, CommercializationProjectCategoryEntity::getCommercializationCategoryEntity, CommercializationCategoryEntity::getCommercializationCategoryId);
            if (!newCategories.contains(commercializationCategoryId)) {
                cpe.setDeleted(true);
            } else {
                cpe.setDeleted(false);
                newCategories.remove(commercializationCategoryId);
            }
        }
        for (Long categoryId : newCategories) {
            final CommercializationProjectCategoryEntity commercializationProjectCategoryEntity = new CommercializationProjectCategoryEntity();
            commercializationProjectCategoryEntity.setCommercializationProjectEntity(commercializationProjectEntity);
            commercializationProjectCategoryEntity.setCommercializationCategoryEntity(commercializationProjectCategoryDao.findCategoryById(categoryId));
            commercializationProjectCategoryDao.save(commercializationProjectCategoryEntity);

            commercializationProjectEntity.getCommercializationProjectCategoryEntities().add(commercializationProjectCategoryEntity);
        }

        final Set<CommercializationProjectKeywordEntity> commercializationProjectKeywordEntities = commercializationProjectEntity.getCommercializationProjectKeywordEntities();
        final List<TagEntity> newTags = tagDao.findOrCreateTagsByValues(commercializationProjectDto.getKeywords());
        final Map<Long, TagEntity> newTagsMap = newTags.stream().collect(Collectors.toMap(TagEntity::getTagId, Function.identity()));
        for (CommercializationProjectKeywordEntity projectKeywordEntity : commercializationProjectKeywordEntities) {
            if (projectKeywordEntity.isDeleted()) continue;
            final Long tagId = HibernateUtil.getId(projectKeywordEntity, CommercializationProjectKeywordEntity::getTagEntity, TagEntity::getTagId);
            if (!newTagsMap.containsKey(tagId)) {
                projectKeywordEntity.setDeleted(true);
            } else {
                newTagsMap.remove(tagId);
            }
        }
        for (TagEntity newTag : newTagsMap.values()) {
            final CommercializationProjectKeywordEntity projectKeywordEntity = new CommercializationProjectKeywordEntity();
            projectKeywordEntity.setCommercializationProjectEntity(commercializationProjectEntity);
            projectKeywordEntity.setTagEntity(newTag);
            commercializationProjectDao.save(projectKeywordEntity);

            commercializationProjectKeywordEntities.add(projectKeywordEntity);
        }
    }
}
