package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.UserEquipmentEntity;

import java.util.List;

public interface UserEquipmentDao {

    void save(UserEquipmentEntity userEquipmentEntity);

    List<UserEquipmentEntity> findUsersForEquipment(Long equipmentId);

    List<UserEquipmentEntity> findEquipmentForUsers(Long userId);

    UserEquipmentEntity find(Long equipmentId, Long userId);

}
