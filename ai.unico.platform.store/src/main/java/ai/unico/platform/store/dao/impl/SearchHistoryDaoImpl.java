package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.api.dto.ExpertSearchHistoryDto;
import ai.unico.platform.store.api.filter.ExpertSearchHistoryFilter;
import ai.unico.platform.store.dao.SearchHistoryDao;
import ai.unico.platform.store.entity.SearchHistEntity;
import ai.unico.platform.store.entity.SearchHistResultEntity;
import ai.unico.platform.store.entity.SearchHistSecondaryQueryEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.util.enums.OrderDirection;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SearchHistoryDaoImpl implements SearchHistoryDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void saveSearchHistory(SearchHistEntity searchHistEntity) {
        entityManager.persist(searchHistEntity);
    }

    @Override
    public void saveSearchSecondaryQuery(SearchHistSecondaryQueryEntity searchHistSecondaryQueryEntity) {
        entityManager.persist(searchHistSecondaryQueryEntity);
    }

    @Override
    public void saveSearchResult(SearchHistResultEntity searchHistResultEntity) {
        entityManager.persist(searchHistResultEntity);
    }

    @Override
    public SearchHistEntity findSearchHistory(Long searchHistId) {
        return entityManager.find(SearchHistEntity.class, searchHistId);
    }

    @Override
    public List<SearchHistEntity> findSearchHistoryForUser(Long userId, ExpertSearchHistoryFilter expertSearchHistoryFilter) {
        final Criteria previewCriteria = prepareSearchHistoryForUserCriteria(userId, expertSearchHistoryFilter);
        previewCriteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        HibernateUtil.setOrder(previewCriteria, "dateInsert", OrderDirection.DESC);
        HibernateUtil.setPagination(previewCriteria, expertSearchHistoryFilter.getPage(), expertSearchHistoryFilter.getLimit());
        final List<SearchHistEntity> previewList = previewCriteria.list();
        final Set<Long> searchHistoryIds = previewList.stream().map(SearchHistEntity::getSearchHistId).collect(Collectors.toSet());
        if (searchHistoryIds.size() == 0) return Collections.emptyList();

        final Criteria criteria = hibernateCriteriaCreator.createCriteria(SearchHistEntity.class, "searchHistory");
        criteria.add(Restrictions.in("searchHistId", searchHistoryIds));
        criteria.createAlias("searchHistory.secondaryQueries", "secondaryQueries", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("searchHistory.resultTypeGroupIds", "resultTypeGroupIds", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("searchHistory.organizationIds", "organizationIds", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("searchHistory.disabledSecondaryQueries", "disabledSecondaryQueries", JoinType.LEFT_OUTER_JOIN);
        criteria.setFetchMode("secondaryQueries", FetchMode.JOIN);
        criteria.setFetchMode("resultTypeGroupIds", FetchMode.JOIN);
        criteria.setFetchMode("organizationIds", FetchMode.JOIN);
        criteria.setFetchMode("disabledSecondaryQueries", FetchMode.JOIN);
        HibernateUtil.setOrder(criteria, "dateInsert", OrderDirection.DESC);
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @Override
    public Long countSearchHistoryForUser(Long userId, ExpertSearchHistoryFilter expertSearchHistoryFilter) {
        final Criteria criteria = prepareSearchHistoryForUserCriteria(userId, expertSearchHistoryFilter);
        criteria.setProjection(Projections.countDistinct("searchHistId"));
        return (Long) criteria.uniqueResult();
    }

    @Override
    public SearchHistEntity findSavedSearch(Long userId, ExpertSearchHistoryDto expertSearchHistoryDto) {
        final ExpertSearchHistoryFilter expertSearchHistoryFilter = new ExpertSearchHistoryFilter();
        expertSearchHistoryFilter.setFavorite(true);
        final Criteria criteria = prepareSearchHistoryForUserCriteria(userId, expertSearchHistoryFilter);
        return findSame(criteria, expertSearchHistoryDto);
    }

    @Override
    public SearchHistEntity findSameSearchAs(Long searchHistId, ExpertSearchHistoryDto expertSearchHistoryDto) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(SearchHistEntity.class, "searchHist");
        criteria.add(Restrictions.eq("searchHistId", searchHistId));
        return findSame(criteria, expertSearchHistoryDto);
    }

    private SearchHistEntity findSame(Criteria criteria, ExpertSearchHistoryDto expertSearchHistoryDto) {
        if (expertSearchHistoryDto.getQuery() != null)
            criteria.add(Restrictions.eq("query", expertSearchHistoryDto.getQuery()).ignoreCase());

        if (expertSearchHistoryDto.getYearFrom() != null)
            criteria.add(Restrictions.eq("yearFrom", expertSearchHistoryDto.getYearFrom()));

        if (expertSearchHistoryDto.getYearTo() != null)
            criteria.add(Restrictions.eq("yearTo", expertSearchHistoryDto.getYearTo()));

        criteria.add(Restrictions.eq("ontologyDisabled", expertSearchHistoryDto.isOntologyDisabled()));

        HibernateUtil.setOrder(criteria, "dateInsert", OrderDirection.DESC);
        final List<SearchHistEntity> list = criteria.list();

        for (SearchHistEntity searchHistEntity : list) {
            if (!expertSearchHistoryDto.getDisabledSecondaryQueries().isEmpty() || !searchHistEntity.getDisabledSecondaryQueries().isEmpty()) {
                final Set<SearchHistSecondaryQueryEntity> secondaryQueries = searchHistEntity.getSecondaryQueries();
                final Set<String> searchedSecondaryQueries = new HashSet<>(expertSearchHistoryDto.getSecondaryQueries());
                final Set<String> searchSecondaryQueries = secondaryQueries.stream().map(SearchHistSecondaryQueryEntity::getQuery).collect(Collectors.toSet());
                if (!searchedSecondaryQueries.equals(searchSecondaryQueries)) continue;
            }
            if (!expertSearchHistoryDto.getOrganizationIds().containsAll(searchHistEntity.getOrganizationIds()) || !searchHistEntity.getOrganizationIds().containsAll(expertSearchHistoryDto.getOrganizationIds()))
                continue;
            if (!searchHistEntity.getResultTypeGroupIds().containsAll(expertSearchHistoryDto.getResultTypeGroupIds()) || !expertSearchHistoryDto.getResultTypeGroupIds().containsAll(searchHistEntity.getResultTypeGroupIds()))
                continue;
            if (!expertSearchHistoryDto.getCountryCodes().containsAll(searchHistEntity.getCountryCodes()) || !searchHistEntity.getCountryCodes().containsAll(expertSearchHistoryDto.getCountryCodes()))
                continue;
            return searchHistEntity;
        }
        return null;
    }

    @Override
    public Long findNumberOfSearches() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(SearchHistEntity.class, "searchHistory");
        criteria.add(Restrictions.eq("page", 1));
        criteria.setProjection(Projections.countDistinct("searchHistId"));
        return (Long) criteria.uniqueResult();
    }

    @Override
    public List<SearchHistResultEntity> findSearchResultsForUser(Long userId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(SearchHistResultEntity.class, "searchHistoryResult");
        criteria.add(Restrictions.eq("resultUserId", userId));
        return criteria.list();
    }

    @Override
    public List<SearchHistResultEntity> findSearchResultsForExpert(Long expertId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(SearchHistResultEntity.class, "searchHistoryResult");
        criteria.add(Restrictions.eq("resultExpertId", expertId));
        return criteria.list();
    }


    private Criteria prepareSearchHistoryForUserCriteria(Long userId, ExpertSearchHistoryFilter expertSearchHistoryFilter) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(SearchHistEntity.class, "searchHistory");
        if (expertSearchHistoryFilter.getWorkspaces() != null && !expertSearchHistoryFilter.getWorkspaces().isEmpty()) {
            criteria.createAlias("searchHistWorkspaceEntities", "searchHistWorkspaceEntity", JoinType.RIGHT_OUTER_JOIN);
            criteria.createAlias("searchHistWorkspaceEntity.userWorkspaceEntity", "userWorkspaceEntity", JoinType.RIGHT_OUTER_JOIN);
            criteria.add(Restrictions.eq("searchHistWorkspaceEntity.deleted", false));
            criteria.add(Restrictions.in("userWorkspaceEntity.workspaceName", expertSearchHistoryFilter.getWorkspaces()));
        }
        criteria.add(Restrictions.eq("userInsert", userId));
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.isNotNull("query"));
        criteria.add(Restrictions.ne("query", ""));
        if (expertSearchHistoryFilter.isFavorite()) {
            criteria.add(Restrictions.eq("favorite", true));
        } else {
            criteria.add(Restrictions.eq("page", 1));
        }
        return criteria;
    }
}
