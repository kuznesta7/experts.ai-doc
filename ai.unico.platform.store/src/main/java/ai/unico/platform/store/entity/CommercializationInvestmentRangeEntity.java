package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.ReferenceData;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CommercializationInvestmentRangeEntity extends ReferenceData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commercializationInvestmentRangeId;

    private Integer investmentFrom;

    private Integer investmentTo;

    public Long getCommercializationInvestmentRangeId() {
        return commercializationInvestmentRangeId;
    }

    public void setCommercializationInvestmentRangeId(Long commercializationInvestmentRangeId) {
        this.commercializationInvestmentRangeId = commercializationInvestmentRangeId;
    }

    public Integer getInvestmentFrom() {
        return investmentFrom;
    }

    public void setInvestmentFrom(Integer investmentFrom) {
        this.investmentFrom = investmentFrom;
    }

    public Integer getInvestmentTo() {
        return investmentTo;
    }

    public void setInvestmentTo(Integer investmentTo) {
        this.investmentTo = investmentTo;
    }
}
