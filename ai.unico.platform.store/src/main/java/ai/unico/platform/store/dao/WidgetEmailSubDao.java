package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.WidgetEmailSubEntity;

public interface WidgetEmailSubDao {

    void save(WidgetEmailSubEntity widgetEmailSubEntity);
    void widgetSubscribe(WidgetEmailSubEntity widgetEmailSubEntity);

    WidgetEmailSubEntity getWidgetEmailSubEntity(String email);

    WidgetEmailSubEntity getWidgetEmailSubEntity(String email, Long organizationId);
}
