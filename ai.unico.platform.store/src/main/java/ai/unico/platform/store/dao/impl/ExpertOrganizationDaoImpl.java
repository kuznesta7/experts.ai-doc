package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ExpertOrganizationDao;
import ai.unico.platform.store.entity.ExpertOrganizationEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ExpertOrganizationDaoImpl implements ExpertOrganizationDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(ExpertOrganizationEntity expertOrganizationEntity) {
        entityManager.persist(expertOrganizationEntity);
    }

    @Override
    public ExpertOrganizationEntity find(Long expertId, Long organizationId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ExpertOrganizationEntity.class, "expertOrganization");
        criteria.add(Restrictions.eq("expertId", expertId));
        criteria.add(Restrictions.eq("organizationEntity.organizationId", organizationId));
        return (ExpertOrganizationEntity) criteria.uniqueResult();
    }

    @Override
    public ExpertOrganizationEntity findNotDeleted(Long expertId, Long organizationId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ExpertOrganizationEntity.class, "expertOrganization");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("expertId", expertId));
        criteria.add(Restrictions.eq("organizationEntity.organizationId", organizationId));
        return (ExpertOrganizationEntity) criteria.uniqueResult();
    }
}
