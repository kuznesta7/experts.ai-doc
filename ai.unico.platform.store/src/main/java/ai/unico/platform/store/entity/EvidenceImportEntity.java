package ai.unico.platform.store.entity;

import ai.unico.platform.store.api.enums.EvidenceCategory;
import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class EvidenceImportEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long evidenceImportId;

    @ManyToOne
    @JoinColumn(name = "fileId")
    private FileEntity fileEntity;

    @ManyToOne
    @JoinColumn(name = "organizationId")
    private OrganizationEntity organizationEntity;

    private String note;

    @Enumerated(EnumType.STRING)
    private EvidenceCategory evidenceCategory;

    public Long getEvidenceImportId() {
        return evidenceImportId;
    }

    public void setEvidenceImportId(Long evidenceImportId) {
        this.evidenceImportId = evidenceImportId;
    }

    public FileEntity getFileEntity() {
        return fileEntity;
    }

    public void setFileEntity(FileEntity fileEntity) {
        this.fileEntity = fileEntity;
    }

    public OrganizationEntity getOrganizationEntity() {
        return organizationEntity;
    }

    public void setOrganizationEntity(OrganizationEntity organizationEntity) {
        this.organizationEntity = organizationEntity;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public EvidenceCategory getEvidenceCategory() {
        return evidenceCategory;
    }

    public void setEvidenceCategory(EvidenceCategory evidenceCategory) {
        this.evidenceCategory = evidenceCategory;
    }
}
