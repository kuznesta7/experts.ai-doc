package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.CommercializationPriorityDao;
import ai.unico.platform.store.entity.CommercializationPriorityEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class CommercializationPriorityDaoImpl implements CommercializationPriorityDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public CommercializationPriorityEntity findPriority(Long priorityId) {
        return entityManager.find(CommercializationPriorityEntity.class, priorityId);
    }

    @Override
    public List<CommercializationPriorityEntity> findAll() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(CommercializationPriorityEntity.class, "priority");
        return criteria.list();
    }

    @Override
    public void save(CommercializationPriorityEntity commercializationPriorityEntity) {
        entityManager.persist(commercializationPriorityEntity);
    }
}
