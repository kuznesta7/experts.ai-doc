package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ProjectDocumentDao;
import ai.unico.platform.store.entity.ProjectDocumentEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class ProjectDocumentDaoImpl implements ProjectDocumentDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void save(ProjectDocumentEntity projectDocumentEntity) {
        entityManager.persist(projectDocumentEntity);
    }

    @Override
    public List<ProjectDocumentEntity> findForProject(Long projectId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProjectDocumentEntity.class, "projectDocument");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("projectEntity.projectId", projectId));
        return criteria.list();
    }

    @Override
    public ProjectDocumentEntity findDocument(Long projectId, Long fileId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProjectDocumentEntity.class, "projectDocument");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("projectEntity.projectId", projectId));
        criteria.add(Restrictions.eq("fileEntity.fileId", fileId));
        return (ProjectDocumentEntity) criteria.uniqueResult();
    }
}
