package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.TagEntity;

import java.util.Collection;
import java.util.List;

public interface TagDao {

    List<TagEntity> findRelevantTags(String query);

    List<TagEntity> findOrCreateTagsByValues(Collection<String> value);

    TagEntity saveTag(String tag);

    TagEntity findOrCreateTagByValues(String value);
}
