package ai.unico.platform.store.entity;

import javax.persistence.*;

@Entity
public class CommercializationProjectKeywordEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commercializationProjectKeywordId;

    @ManyToOne
    @JoinColumn(name = "commercializationProjectId")
    private CommercializationProjectEntity commercializationProjectEntity;

    @ManyToOne
    @JoinColumn(name = "tagId")
    private TagEntity tagEntity;

    private boolean deleted;

    public Long getCommercializationProjectKeywordId() {
        return commercializationProjectKeywordId;
    }

    public void setCommercializationProjectKeywordId(Long commercializationProjectKeywordId) {
        this.commercializationProjectKeywordId = commercializationProjectKeywordId;
    }

    public CommercializationProjectEntity getCommercializationProjectEntity() {
        return commercializationProjectEntity;
    }

    public void setCommercializationProjectEntity(CommercializationProjectEntity commercializationProjectEntity) {
        this.commercializationProjectEntity = commercializationProjectEntity;
    }

    public TagEntity getTagEntity() {
        return tagEntity;
    }

    public void setTagEntity(TagEntity tagEntity) {
        this.tagEntity = tagEntity;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
