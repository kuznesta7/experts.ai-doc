package ai.unico.platform.store.dao;

import ai.unico.platform.store.api.dto.ExpertSearchHistoryDto;
import ai.unico.platform.store.api.filter.ExpertSearchHistoryFilter;
import ai.unico.platform.store.entity.SearchHistEntity;
import ai.unico.platform.store.entity.SearchHistResultEntity;
import ai.unico.platform.store.entity.SearchHistSecondaryQueryEntity;

import java.util.List;

public interface SearchHistoryDao {

    void saveSearchHistory(SearchHistEntity searchHistEntity);

    void saveSearchSecondaryQuery(SearchHistSecondaryQueryEntity searchHistSecondaryQueryEntity);

    void saveSearchResult(SearchHistResultEntity searchHistResultEntity);

    SearchHistEntity findSearchHistory(Long searchHistId);

    List<SearchHistEntity> findSearchHistoryForUser(Long userId, ExpertSearchHistoryFilter expertSearchHistoryFilter);

    Long countSearchHistoryForUser(Long userId, ExpertSearchHistoryFilter expertSearchHistoryFilter);

    SearchHistEntity findSavedSearch(Long userId, ExpertSearchHistoryDto expertSearchHistoryDto);

    SearchHistEntity findSameSearchAs(Long searchHistId, ExpertSearchHistoryDto expertSearchHistoryDto);

    Long findNumberOfSearches();

    List<SearchHistResultEntity> findSearchResultsForUser(Long userId);

    List<SearchHistResultEntity> findSearchResultsForExpert(Long expertId);

}
