package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ExpertsStatisticsAdministrationDao;
import ai.unico.platform.store.entity.CountryClaimedExpertsEntity;
import ai.unico.platform.store.entity.UserExpertEntity;
import ai.unico.platform.store.entity.UserOrganizationEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ExpertsStatisticsAdministrationDaoImpl implements ExpertsStatisticsAdministrationDao {

    @Autowired
    HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public Map<String, Long> getCountriesUserClaims() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserOrganizationEntity.class, "userOrganization");
        criteria.createAlias("userOrganization.organizationEntity", "organization", JoinType.INNER_JOIN);
        criteria.createAlias("userOrganization.userProfileEntity", "userProfile", JoinType.INNER_JOIN);
        criteria.createAlias("organization.countryEntity", "country", JoinType.INNER_JOIN);
        criteria.add(Restrictions.isNotNull("userProfile.claimedBy"));
        criteria.setProjection(Projections.projectionList()
                .add(Projections.groupProperty("country.countryCode"), "countryCode")
                .add(Projections.countDistinct("userProfile.userId"), "numberOfExperts"));
        criteria.setResultTransformer(Transformers.aliasToBean(CountryClaimedExpertsEntity.class));
        final List<CountryClaimedExpertsEntity> list = (List<CountryClaimedExpertsEntity>) criteria.list();

        final Criteria criteria1 = hibernateCriteriaCreator.createCriteria(UserExpertEntity.class, "userExpert");
        criteria1.createAlias("userExpert.userProfileEntity", "userProfile", JoinType.INNER_JOIN);
        criteria1.createAlias("userProfile.userOrganizationEntities", "userOrganization", JoinType.INNER_JOIN);
        criteria1.createAlias("userOrganization.organizationEntity", "organization", JoinType.INNER_JOIN);
        criteria1.createAlias("organization.countryEntity", "country", JoinType.INNER_JOIN);
        criteria1.add(Restrictions.isNull("userProfile.claimedBy"));
        criteria1.setProjection(Projections.projectionList()
                .add(Projections.groupProperty("country.countryCode"), "countryCode")
                .add(Projections.countDistinct("userProfile.userId"), "numberOfExperts"));
        criteria1.setResultTransformer(Transformers.aliasToBean(CountryClaimedExpertsEntity.class));
        final Map<String, Long> map = ((List<CountryClaimedExpertsEntity>) criteria1.list())
                .stream().collect(Collectors
                        .toMap(CountryClaimedExpertsEntity::getCountryCode, CountryClaimedExpertsEntity::getNumberOfExperts));

        for (CountryClaimedExpertsEntity countryClaimedExpertsEntity :
                list) {
            String key = countryClaimedExpertsEntity.getCountryCode();
            if (map.containsKey(countryClaimedExpertsEntity.getCountryCode())) {
                map.put(key, map.get(key) + countryClaimedExpertsEntity.getNumberOfExperts());
            } else {
                map.put(key, countryClaimedExpertsEntity.getNumberOfExperts());
            }
        }
        return map;
    }

    @Override
    public Long getAllCountriesUserClaims() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserProfileEntity.class, "userProfile");
        criteria.add(Restrictions.isNotNull("claimedBy"));
        criteria.setProjection(Projections.projectionList().add(Projections.countDistinct("userId")));
        Long result = (Long) criteria.uniqueResult();

        final Criteria criteria1 = hibernateCriteriaCreator.createCriteria(UserExpertEntity.class, "userExpert");
        criteria1.createAlias("userExpert.userProfileEntity", "userProfile", JoinType.INNER_JOIN);
        criteria1.add(Restrictions.isNull("userProfile.claimedBy"));
        criteria1.setProjection(Projections.countDistinct("userProfile.userId"));
        result += (Long) criteria1.uniqueResult();
        return result;
    }
}
