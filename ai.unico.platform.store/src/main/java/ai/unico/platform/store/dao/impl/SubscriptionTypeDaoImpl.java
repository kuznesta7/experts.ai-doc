package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.SubscriptionTypeDao;
import ai.unico.platform.store.entity.SubscriptionTypeEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class SubscriptionTypeDaoImpl implements SubscriptionTypeDao {


    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;


    @Override
    public List<SubscriptionTypeEntity> getAllSubscriptionTypes() {
        Criteria criteria = hibernateCriteriaCreator.createCriteria(SubscriptionTypeEntity.class, "subscriptionType");
        criteria.add(Restrictions.eq("deleted", false));
        return criteria.list();
    }

    @Override
    public SubscriptionTypeEntity getSubscriptionType(Long id) {
        return entityManager.find(SubscriptionTypeEntity.class, id);
    }
}
