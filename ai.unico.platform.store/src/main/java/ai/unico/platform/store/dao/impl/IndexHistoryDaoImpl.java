package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.dao.IndexHistoryDao;
import ai.unico.platform.store.entity.IndexHistoryEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.util.enums.OrderDirection;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class IndexHistoryDaoImpl implements IndexHistoryDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void save(IndexHistoryEntity indexHistoryEntity) {
        entityManager.persist(indexHistoryEntity);
    }

    @Override
    public List<IndexHistoryEntity> findLatest() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(IndexHistoryEntity.class, "history");
        HibernateUtil.setOrder(criteria, "dateInsert", OrderDirection.DESC);
        final List<IndexHistoryEntity> list = criteria.list();
        final Set<IndexTarget> added = new HashSet<>();
        final List<IndexHistoryEntity> result = new ArrayList<>();
        for (IndexHistoryEntity entity : list) {
            final IndexTarget target = entity.getTarget();
            if (added.contains(target)) continue;
            added.add(target);
            result.add(entity);
        }
        return result;
    }

    @Override
    public IndexHistoryEntity find(Long elasticsearchIndexHistoryId) {
        return entityManager.find(IndexHistoryEntity.class, elasticsearchIndexHistoryId);
    }
}
