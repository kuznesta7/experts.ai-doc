package ai.unico.platform.store.service;

import ai.unico.platform.store.api.exception.InvalidProjectStructureException;
import ai.unico.platform.store.api.service.ProjectStructureService;
import ai.unico.platform.store.dao.ProjectOrganizationDao;
import ai.unico.platform.store.entity.ProjectOrganizationEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class ProjectStructureServiceImpl implements ProjectStructureService {

    @Autowired
    private ProjectOrganizationDao projectOrganizationDao;

    @Override
    @Transactional
    public void addParentProject(Long projectId, Long parentProjectId, Long organizationId, UserContext userContext) {
        final ProjectOrganizationEntity projectOrganizationEntity = projectOrganizationDao.findProjectOrganization(projectId, organizationId);
        final ProjectOrganizationEntity parentPrivateProject = projectOrganizationDao.findProjectOrganization(parentProjectId, organizationId);
        if (projectOrganizationEntity == null || parentPrivateProject == null)
            throw new NonexistentEntityException(ProjectOrganizationEntity.class, "ORGANIZATION_PROJECT_DOESNT_EXIST");
        if (projectOrganizationEntity.getParentProjectOrganizationEntity() != null)
            throw new EntityAlreadyExistsException("PROJECT_ALREADY_HAS_PARENT", "Project already has a parent project");
        checkForLoops(projectOrganizationEntity, parentPrivateProject);

        projectOrganizationEntity.setParentProjectOrganizationEntity(parentPrivateProject);
        TrackableUtil.fillUpdate(projectOrganizationEntity, userContext);
        projectOrganizationDao.saveProjectOrganization(projectOrganizationEntity);
    }

    @Override
    @Transactional
    public void removeParentProject(Long projectId, Long organizationId, UserContext userContext) {
        final ProjectOrganizationEntity privateProject = projectOrganizationDao.findProjectOrganization(projectId, organizationId);
        if (privateProject == null)
            throw new NonexistentEntityException(ProjectOrganizationEntity.class);
        privateProject.setParentProjectOrganizationEntity(null);
        TrackableUtil.fillUpdate(privateProject, userContext);
    }

    private void checkForLoops(ProjectOrganizationEntity children, ProjectOrganizationEntity parent) {
        if (children.equals(parent))
            throw new InvalidProjectStructureException("project has loops; children privateProjectId:" + children.getProjectOrganizationId() + "; parent privateProjectId:" + parent.getProjectOrganizationId());
        if (parent.getParentProjectOrganizationEntity() != null) {
            checkForLoops(children, parent.getParentProjectOrganizationEntity());
        }
    }
}
