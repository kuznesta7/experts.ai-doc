package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.EquipmentOrganizationDao;
import ai.unico.platform.store.entity.EquipmentOrganizationEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class EquipmentOrganizationDaoImpl implements EquipmentOrganizationDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(EquipmentOrganizationEntity equipmentOrganizationEntity) {
        entityManager.persist(equipmentOrganizationEntity);
    }

    @Override
    public List<EquipmentOrganizationEntity> findOrganizationsForEquipment(Long equipmentId) {
        final Criteria criteria = prepareCriteria(equipmentId);
        return criteria.list();
    }

    @Override
    public EquipmentOrganizationEntity find(Long equipmentId, Long organizationId) {
        final Criteria criteria = prepareCriteria(equipmentId);
        criteria.add(Restrictions.eq("organizationEntity.organizationId", organizationId));
        try {
            return (EquipmentOrganizationEntity) criteria.uniqueResult();
        } catch (Exception e) {
            return null;
        }
    }

    private Criteria prepareCriteria(Long equipmentId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(EquipmentOrganizationEntity.class, "equipmentOrganizationEntity");
        criteria.add(Restrictions.eq("equipmentOrganizationEntity.equipmentEntity.id", equipmentId));
        criteria.add(Restrictions.eq("equipmentOrganizationEntity.deleted", false));
        return criteria;
    }
}
