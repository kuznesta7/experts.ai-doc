package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ClaimRequestDao;
import ai.unico.platform.store.entity.ClaimRequestEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ClaimRequestDaoImpl implements ClaimRequestDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(ClaimRequestEntity claimRequestEntity) {
        entityManager.persist(claimRequestEntity);
    }
}
