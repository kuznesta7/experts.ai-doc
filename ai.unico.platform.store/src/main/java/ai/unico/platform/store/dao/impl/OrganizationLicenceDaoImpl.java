package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.OrganizationLicenceDao;
import ai.unico.platform.store.entity.OrganizationLicenceEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.util.enums.OrderDirection;
import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class OrganizationLicenceDaoImpl implements OrganizationLicenceDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public List<OrganizationLicenceEntity> getOrganizationLicences(Set<Long> organizationIds, boolean validOnly) {
        if (organizationIds.isEmpty()) return Collections.emptyList();
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationLicenceEntity.class, "organizationLicence");
        criteria.add(Restrictions.in("organizationEntity.organizationId", organizationIds));
        criteria.add(Restrictions.eq("deleted", false));
        if (validOnly) {
            criteria.add(Restrictions.or(
                    Restrictions.gt("validUntil", new Date()),
                    Restrictions.isNull("validUntil")));
        }
        HibernateUtil.setOrder(criteria, "validUntil", OrderDirection.DESC);
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @Override
    @Transactional
    public Boolean organizationHasLicense(Long organizationId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationLicenceEntity.class, "organizationLicence");
        criteria.add(Restrictions.eq("organizationEntity.organizationId", organizationId));
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.and(
                Restrictions.isNotNull("validUntil"),
                Restrictions.gt("validUntil", new Date())));

        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);


        return criteria.list().size() > 0;
    }

    @Override
    public List<OrganizationLicenceEntity> getOrganizationLicences() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationLicenceEntity.class, "organizationLicence");
        criteria.add(Restrictions.eq("deleted", false));
        return criteria.list();
    }

    @Override
    public OrganizationLicenceEntity find(Long organizationLicenceId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationLicenceEntity.class, "organizationLicence");
        criteria.add(Restrictions.eq("organizationLicenceId", organizationLicenceId));
        criteria.add(Restrictions.eq("deleted", false));
        return (OrganizationLicenceEntity) criteria.uniqueResult();
    }

    @Override
    public void save(OrganizationLicenceEntity organizationLicenceEntity) {
        entityManager.persist(organizationLicenceEntity);
    }
}
