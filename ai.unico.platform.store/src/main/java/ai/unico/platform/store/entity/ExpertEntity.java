package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class ExpertEntity extends Trackable {

    @Id
    private Long expertId;

    private Date retirementDate;

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public Date getRetirementDate() {
        return retirementDate;
    }

    public void setRetirementDate(Date retirementDate) {
        this.retirementDate = retirementDate;
    }
}
