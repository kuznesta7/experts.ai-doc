package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.FileEntity;
import ai.unico.platform.store.entity.OrganizationEntity;

import java.util.Collection;
import java.util.List;

public interface OrganizationDao {

    List<OrganizationEntity> find();

    OrganizationEntity find(Long organizationId);

    List<OrganizationEntity> findByIds(Collection<Long> organizationIds);

    OrganizationEntity findByOriginalId(Long originalOrganizationId);

    List<OrganizationEntity> findByOriginalIds(Collection<Long> originalOrganizationIds);

    List<OrganizationEntity> findBySubstituteId(Long substituteOrganizationId);

    void saveOrganization(OrganizationEntity organizationEntity);

    List<OrganizationRankHelper> findAndComputeOrganizationsRank();

    List<Long> findIdsForCountryCodes(Collection<String> countryCodes);

    FileEntity findOrganizationLogoFile(Long organizationId);

    FileEntity findOrganizationCoverImgFile(Long organizationId);

    List<OrganizationEntity> getRecombeeOrganizations();

    class OrganizationRankHelper {
        private Long organizationId;
        private Long rank = 0L;
        private Long itemCount = 0L;
        private Long userCount = 0L;

        public Long getOrganizationId() {
            return organizationId;
        }

        public void setOrganizationId(Long organizationId) {
            this.organizationId = organizationId;
        }

        public Long getItemCount() {
            return itemCount;
        }

        public void setItemCount(Long itemCount) {
            this.itemCount = itemCount;
        }

        public Long getUserCount() {
            return userCount;
        }

        public void setUserCount(Long userCount) {
            this.userCount = userCount;
        }

        public Long getRank() {
            return rank;
        }

        public void setRank(Long rank) {
            this.rank = rank;
        }
    }
}
