package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ProfilePersonalNoteEntity;

public interface ProfilePersonalNoteDao {

    ProfilePersonalNoteEntity findExpertNote(Long userFromId, Long expertId);

    ProfilePersonalNoteEntity findUserNote(Long userFromId, Long userToId);

    void save(ProfilePersonalNoteEntity profilePersonalNoteEntity);

}

