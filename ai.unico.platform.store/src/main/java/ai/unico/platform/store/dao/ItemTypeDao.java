package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ItemTypeEntity;
import ai.unico.platform.store.entity.ItemTypeGroupEntity;

import java.util.List;

public interface ItemTypeDao {

    ItemTypeEntity find(Long itemTypeId);

    List<ItemTypeEntity> find();

    List<ItemTypeGroupEntity> findGroups();

}
