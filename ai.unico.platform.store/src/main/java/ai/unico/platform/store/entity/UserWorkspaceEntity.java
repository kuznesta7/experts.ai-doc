package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;
import java.util.Set;

@Entity
public class UserWorkspaceEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userWorkspaceId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserProfileEntity userProfileEntity;

    @OneToMany(mappedBy = "userWorkspaceEntity")
    private Set<FollowedProfileWorkspaceEntity> followedProfileWorkspaceEntities;

    @OneToMany(mappedBy = "userWorkspaceEntity")
    private Set<SearchHistWorkspaceEntity> searchHistWorkspaceEntities;

    private String workspaceName;

    private String workspaceNote;

    private boolean deleted;

    public Long getUserWorkspaceId() {
        return userWorkspaceId;
    }

    public void setUserWorkspaceId(Long userWorkspaceId) {
        this.userWorkspaceId = userWorkspaceId;
    }

    public UserProfileEntity getUserProfileEntity() {
        return userProfileEntity;
    }

    public void setUserProfileEntity(UserProfileEntity userProfileEntity) {
        this.userProfileEntity = userProfileEntity;
    }

    public String getWorkspaceName() {
        return workspaceName;
    }

    public void setWorkspaceName(String workspaceName) {
        this.workspaceName = workspaceName;
    }

    public String getWorkspaceNote() {
        return workspaceNote;
    }

    public void setWorkspaceNote(String workspaceNote) {
        this.workspaceNote = workspaceNote;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
