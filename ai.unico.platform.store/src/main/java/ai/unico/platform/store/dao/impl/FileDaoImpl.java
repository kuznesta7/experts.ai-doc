package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.FileDao;
import ai.unico.platform.store.entity.FileAccessEntity;
import ai.unico.platform.store.entity.FileEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class FileDaoImpl implements FileDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(FileEntity fileEntity) {
        entityManager.persist(fileEntity);
    }

    @Override
    public void save(FileAccessEntity fileAccessEntity) {
        entityManager.persist(fileAccessEntity);
    }

    @Override
    public FileEntity find(Long fileId) {
        return entityManager.find(FileEntity.class, fileId);
    }
}
