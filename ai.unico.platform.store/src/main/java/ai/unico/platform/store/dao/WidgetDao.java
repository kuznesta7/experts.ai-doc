package ai.unico.platform.store.dao;

import ai.unico.platform.store.api.enums.WidgetType;
import ai.unico.platform.store.entity.OrganizationWidgetEntity;

import java.util.List;

public interface WidgetDao {

    boolean isAllowed(Long organizationId, WidgetType widgetType);

    List<OrganizationWidgetEntity> findForOrganization(Long organizationId);

    void save(OrganizationWidgetEntity organizationWidgetEntity);

}
