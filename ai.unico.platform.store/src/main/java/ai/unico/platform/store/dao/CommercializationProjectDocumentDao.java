package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.CommercializationProjectDocumentEntity;

import java.util.List;

public interface CommercializationProjectDocumentDao {

    void save(CommercializationProjectDocumentEntity commercializationProjectDocumentEntity);

    List<CommercializationProjectDocumentEntity> findForCommercializationProject(Long commercializationId);

    CommercializationProjectDocumentEntity findDocument(Long commercializationId, Long fileId);

}
