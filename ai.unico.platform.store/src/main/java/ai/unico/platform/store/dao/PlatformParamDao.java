package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.PlatformParamEntity;

import java.util.List;

public interface PlatformParamDao {

    List<PlatformParamEntity> findPlatformParams();

}
