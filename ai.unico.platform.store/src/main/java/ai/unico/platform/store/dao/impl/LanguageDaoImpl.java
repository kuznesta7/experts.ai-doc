package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.LanguageDao;
import ai.unico.platform.store.entity.LanguageEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class LanguageDaoImpl implements LanguageDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public LanguageEntity find(String languageCode) {
        return entityManager.find(LanguageEntity.class, languageCode);
    }

    @Override
    public List<LanguageEntity> findAll() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(LanguageEntity.class, "language");
        return criteria.list();
    }
}
