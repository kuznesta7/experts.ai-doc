package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Entity
public class UserProfileEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    private String name;

    private String username;

    private String description;

    private String email;

    private String positionDescription;

    @OneToMany(mappedBy = "userProfileEntity")
    private Set<UserOrganizationEntity> userOrganizationEntities;

    @OneToMany(mappedBy = "userProfileEntity")
    private Set<UserCountryOfInterestEntity> userCountryOfInterestEntities;

    private boolean deleted;

    private boolean claimable;

    private boolean invitation;

    private Date retirementDate;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "claimed_by")
    private UserProfileEntity claimedBy;

    @OneToMany(mappedBy = "claimedBy")
    private Set<UserProfileEntity> claimedUserProfiles = new HashSet<>();

    @OneToOne
    @JoinColumn(name = "img_file_id")
    private FileEntity imgFile;

    @OneToOne
    @JoinColumn(name = "cover_img_file_id")
    private FileEntity coverImgFile;

    @OneToMany(mappedBy = "userProfileEntity")
    private Set<UserProfileKeywordEntity> userProfileKeywordEntities = new HashSet<>();

    @OneToMany(mappedBy = "userProfileEntity")
    private Set<UserConsentEntity> userConsentEntities = new HashSet<>();

    @OneToMany(mappedBy = "userProfileEntity")
    private Set<UserExpertItemEntity> userItemEntities = new HashSet<>();

    @OneToMany(mappedBy = "userProfileEntity")
    private Set<UserExpertEntity> userExpertEntities = new HashSet<>();

    @OneToMany(mappedBy = "followedUser")
    private Set<FollowedProfileEntity> followerProfileEntities = new HashSet<>();

    @OneToMany(mappedBy = "followerUser")
    private Set<FollowedProfileEntity> followedProfileEntities = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPositionDescription() {
        return positionDescription;
    }

    public void setPositionDescription(String positionDescription) {
        this.positionDescription = positionDescription;
    }

    public Set<UserOrganizationEntity> getUserOrganizationEntities() {
        return userOrganizationEntities;
    }

    public void setUserOrganizationEntities(Set<UserOrganizationEntity> userOrganizationEntities) {
        this.userOrganizationEntities = userOrganizationEntities;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public FileEntity getImgFile() {
        return imgFile;
    }

    public void setImgFile(FileEntity imgFile) {
        this.imgFile = imgFile;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Set<UserProfileKeywordEntity> getUserProfileKeywordEntities() {
        return userProfileKeywordEntities;
    }

    public void setUserProfileKeywordEntities(Set<UserProfileKeywordEntity> userProfileKeywordEntities) {
        this.userProfileKeywordEntities = userProfileKeywordEntities;
    }

    public Set<UserExpertItemEntity> getUserItemEntities() {
        return userItemEntities;
    }

    public void setUserItemEntities(Set<UserExpertItemEntity> userItemEntities) {
        this.userItemEntities = userItemEntities;
    }

    public Set<UserExpertEntity> getUserExpertEntities() {
        return userExpertEntities;
    }

    public void setUserExpertEntities(Set<UserExpertEntity> userExpertEntities) {
        this.userExpertEntities = userExpertEntities;
    }

    public Set<UserConsentEntity> getUserConsentEntities() {
        return userConsentEntities;
    }

    public void setUserConsentEntities(Set<UserConsentEntity> userConsentEntities) {
        this.userConsentEntities = userConsentEntities;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isClaimable() {
        return claimable;
    }

    public void setClaimable(boolean claimable) {
        this.claimable = claimable;
    }

    public UserProfileEntity getClaimedBy() {
        return claimedBy;
    }

    public void setClaimedBy(UserProfileEntity claimedBy) {
        this.claimedBy = claimedBy;
    }

    public Set<FollowedProfileEntity> getFollowedProfileEntities() {
        return followedProfileEntities;
    }

    public void setFollowedProfileEntities(Set<FollowedProfileEntity> followedProfileEntities) {
        this.followedProfileEntities = followedProfileEntities;
    }

    public Set<FollowedProfileEntity> getFollowerProfileEntities() {
        return followerProfileEntities;
    }

    public void setFollowerProfileEntities(Set<FollowedProfileEntity> followerProfileEntities) {
        this.followerProfileEntities = followerProfileEntities;
    }

    public Set<UserCountryOfInterestEntity> getUserCountryOfInterestEntities() {
        return userCountryOfInterestEntities;
    }

    public void setUserCountryOfInterestEntities(Set<UserCountryOfInterestEntity> userCountryOfInterestEntities) {
        this.userCountryOfInterestEntities = userCountryOfInterestEntities;
    }

    public Date getRetirementDate() {
        return retirementDate;
    }

    public void setRetirementDate(Date retirementDate) {
        this.retirementDate = retirementDate;
    }

    public Set<UserProfileEntity> getClaimedUserProfiles() {
        return claimedUserProfiles;
    }

    public void setClaimedUserProfiles(Set<UserProfileEntity> claimedUserProfiles) {
        this.claimedUserProfiles = claimedUserProfiles;
    }

    public boolean isInvitation() {
        return invitation;
    }

    public void setInvitation(boolean invitation) {
        this.invitation = invitation;
    }

    public FileEntity getCoverImgFile() {
        return coverImgFile;
    }

    public void setCoverImgFile(FileEntity coverImgFile) {
        this.coverImgFile = coverImgFile;
    }
}
