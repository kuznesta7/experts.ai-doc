package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.GlobalSearchStatisticsDto;
import ai.unico.platform.store.api.service.GlobalStatisticsService;
import ai.unico.platform.store.dao.ProfileVisitationDao;
import ai.unico.platform.store.dao.SearchHistoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class GlobalStatisticsServiceImpl implements GlobalStatisticsService {

    @Autowired
    private SearchHistoryDao searchHistoryDao;

    @Autowired
    private ProfileVisitationDao profileVisitationDao;

    @Override
    @Transactional
    public GlobalSearchStatisticsDto findSearchStatistics(Long userId) {
        final GlobalSearchStatisticsDto globalSearchStatisticsDto = new GlobalSearchStatisticsDto();
        globalSearchStatisticsDto.setNumberOfSearches(searchHistoryDao.findNumberOfSearches());
        globalSearchStatisticsDto.setNumberOfProfilesVisited(profileVisitationDao.findNumberOfVisitations());
        return globalSearchStatisticsDto;
    }
}
