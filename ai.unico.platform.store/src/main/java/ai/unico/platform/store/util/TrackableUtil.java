package ai.unico.platform.store.util;

import ai.unico.platform.store.entity.superclass.Trackable;
import ai.unico.platform.util.model.UserContext;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Optional;

public class TrackableUtil {

    @Deprecated
    public static void fillCreateTrackable(Trackable trackable, Long userId) {
        trackable.setDateInsert(new Timestamp(new Date().getTime()));
        trackable.setUserInsert(userId);
        fillUpdateTrackable(trackable, userId);
    }

    @Deprecated
    public static void fillUpdateTrackable(Trackable trackable, Long userId) {
        trackable.setDateUpdate(new Timestamp(new Date().getTime()));
        trackable.setUserUpdate(userId);
    }

    public static void fillCreate(Trackable trackable, UserContext userContext) {
        trackable.setDateInsert(new Timestamp(new Date().getTime()));
        trackable.setUserInsert(getAuthorUserId(userContext));
        fillUpdate(trackable, userContext);
    }

    public static void fillUpdate(Trackable trackable, UserContext userContext) {
        trackable.setDateUpdate(new Timestamp(new Date().getTime()));
        trackable.setUserUpdate(getAuthorUserId(userContext));
    }


    public static <T extends Trackable> T findLast(Collection<T> trackables) {
        final Optional<T> max = trackables.stream().max(new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                final Date dateInsert1 = o1.getDateInsert();
                final Date dateInsert2 = o2.getDateInsert();
                if (dateInsert1 == null) return -1;
                if (dateInsert2 == null) return 1;
                return dateInsert1.compareTo(dateInsert2);
            }
        });
        return max.orElse(null);
    }

    private static Long getAuthorUserId(UserContext userContext) {
        if (userContext == null) return null;
        if (userContext.getImpersonatorUserId() != null) return userContext.getImpersonatorUserId();
        return userContext.getUserId();
    }
}
