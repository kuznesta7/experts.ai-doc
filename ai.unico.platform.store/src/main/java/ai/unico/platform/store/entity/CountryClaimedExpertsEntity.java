package ai.unico.platform.store.entity;

public class CountryClaimedExpertsEntity {

    private String countryCode;

    private Long numberOfExperts;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Long getNumberOfExperts() {
        return numberOfExperts;
    }

    public void setNumberOfExperts(Long numberOfExperts) {
        this.numberOfExperts = numberOfExperts;
    }
}
