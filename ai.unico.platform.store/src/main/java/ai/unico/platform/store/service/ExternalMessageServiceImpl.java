package ai.unico.platform.store.service;

import ai.unico.platform.search.api.service.ExpertLookupService;
import ai.unico.platform.search.api.service.LectureSearchService;
import ai.unico.platform.store.api.dto.ExternalMessageDto;
import ai.unico.platform.store.api.dto.MessageDto;
import ai.unico.platform.store.api.enums.MessageType;
import ai.unico.platform.store.api.service.ExternalMessageService;
import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.store.api.service.RecaptchaValidationService;
import ai.unico.platform.store.api.service.UnicoNotifierService;
import ai.unico.platform.store.dao.ExternalMessageDao;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.ExternalMessageEntity;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.exception.InvalidParameterException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

public class ExternalMessageServiceImpl implements ExternalMessageService {

    private final ExpertLookupService expertLookupService = ServiceRegistryProxy.createProxy(ExpertLookupService.class);
    private final LectureSearchService lectureSearchService = ServiceRegistryProxy.createProxy(LectureSearchService.class);

    @Autowired
    private OrganizationDao organizationDao;
    @Autowired
    private ExternalMessageDao externalMessageDao;
    @Autowired
    private UserProfileDao userProfileDao;
    @Autowired
    private UnicoNotifierService unicoNotifierService;
    @Autowired
    private RecaptchaValidationService recaptchaValidationService;
    @Autowired
    private OrganizationStructureService organizationStructureService;

    @Override
    @Transactional
    public void sendMessage(ExternalMessageDto externalMessageDto) throws NonexistentEntityException, InvalidParameterException, IOException {
        recaptchaValidationService.validateToken(externalMessageDto.getRecaptcha());
        ExternalMessageEntity entity = new ExternalMessageEntity();
        entity.setMessage(externalMessageDto.getMessage());
        entity.setSenderEmail(externalMessageDto.getSenderEmail());
        entity.setExpertId(externalMessageDto.getExpertToId());
        entity.setMessageType(externalMessageDto.getMessageType());
        entity.setLectureCode(externalMessageDto.getLectureCode());
        if (externalMessageDto.getMessageType() == null)
            throw new InvalidParameterException("Message type must bee filled.");
        if (externalMessageDto.getUserToId() != null && externalMessageDto.getExpertToId() != null)
            throw new InvalidParameterException("User id or Expert id must be filled not both.");
        if (externalMessageDto.getOrganizationId() == null)
            throw new InvalidParameterException("Organization id must not be null.");
        final OrganizationEntity organizationEntity = organizationDao.find(externalMessageDto.getOrganizationId());
        entity.setOrganizationEntity(organizationEntity);

        if (externalMessageDto.getUserToId() != null)
            entity.setUserProfileEntity(userProfileDao.findUserProfile(externalMessageDto.getUserToId()));
        else
            entity.setExpertId(externalMessageDto.getExpertToId());

        externalMessageDao.saveMessage(entity);

        MessageDto messageDto = new MessageDto();
        messageDto.setUserToId(externalMessageDto.getUserToId());
        messageDto.setExpertToId(externalMessageDto.getExpertToId());
        messageDto.setContent(externalMessageDto.getMessage());
        messageDto.setOpportunityName(externalMessageDto.getOpportunityName());
        messageDto.setOpportunityId(externalMessageDto.getOpportunityId());
        if (MessageType.LECTURE == externalMessageDto.getMessageType()) {
            messageDto.setLectureAboutCode(externalMessageDto.getLectureCode());
            messageDto.setLectureAboutName(lectureSearchService.getLecture(externalMessageDto.getLectureCode()).getLectureName());
            String emails = organizationEntity.getOrganizationUserRoleEntities().stream()
                    .filter(x -> !x.isDeleted() && x.getRole() == OrganizationRole.ORGANIZATION_EDITOR)
                    .map(x -> x.getUserProfileEntity().getEmail())
                    .collect(Collectors.joining(","));
            unicoNotifierService.sendExternalLectureRequest(messageDto, emails, externalMessageDto.getSenderEmail());
        } else if (MessageType.ORGANIZATION == externalMessageDto.getMessageType()) {
            String emails = organizationEntity.getOrganizationUserRoleEntities().stream()
                    .filter(x -> !x.isDeleted() && x.getRole() == OrganizationRole.ORGANIZATION_EDITOR)
                    .map(x -> x.getUserProfileEntity().getEmail())
                    .collect(Collectors.joining(","));
            unicoNotifierService.sendExternalOrganizationContactRequest(messageDto, emails, externalMessageDto.getSenderEmail(), organizationEntity.getOrganizationName());
        } else if (MessageType.EXPERT == externalMessageDto.getMessageType()) {
            String emails = organizationEntity.getOrganizationUserRoleEntities().stream()
                    .filter(x -> !x.isDeleted() && x.getRole() == OrganizationRole.ORGANIZATION_EDITOR)
                    .map(x -> x.getUserProfileEntity().getEmail())
                    .collect(Collectors.joining(","));
            messageDto.setUserToName(externalMessageDto.getUserToId() == null ?
                    expertLookupService.findUserExpertBase(externalMessageDto.getExpertToId()).getName()
                    : userProfileDao.findUserProfile(externalMessageDto.getUserToId()).getName());
            unicoNotifierService.sendExternalContactRequest(messageDto, emails, externalMessageDto.getSenderEmail());
        } else if (MessageType.OUTCOME == externalMessageDto.getMessageType() ||
                MessageType.PROJECT == externalMessageDto.getMessageType()) {
            messageDto.setUserFromName(entity.getSenderEmail());
            messageDto.setOrganizationName(organizationEntity.getOrganizationName());
            String emails = organizationEntity.getOrganizationUserRoleEntities().stream()
                    .filter(x -> !x.isDeleted() && x.getRole() == OrganizationRole.ORGANIZATION_ADMIN)
                    .map(x -> x.getUserProfileEntity().getEmail())
                    .collect(Collectors.joining(","));
            unicoNotifierService.sendExternalOutcomeProjectRequest(externalMessageDto, messageDto, emails, externalMessageDto.getSenderEmail());
        } else if (MessageType.APPLY == externalMessageDto.getMessageType()){
            messageDto.setOrganizationName(organizationEntity.getOrganizationName());

            // Adding emails of organization admins and editors
            Set<String> emails = organizationEntity.getOrganizationUserRoleEntities().stream()
                    .filter(x -> !x.isDeleted() && (x.getRole() == OrganizationRole.ORGANIZATION_EDITOR || x.getRole() == OrganizationRole.ORGANIZATION_ADMIN))
                    .map(x -> x.getUserProfileEntity().getEmail())
                    .collect(Collectors.toSet());

            // Adding emails of organization admins and editors of parent organizations
            Set<Long> myMemberships = organizationStructureService.findMyMemberships(organizationEntity.getOrganizationId());
            for (Long organizationId : myMemberships) {
                final OrganizationEntity organization = organizationDao.find(organizationId);
                emails.addAll(organization.getOrganizationUserRoleEntities().stream()
                        .filter(x -> !x.isDeleted() && (x.getRole() == OrganizationRole.ORGANIZATION_EDITOR || x.getRole() == OrganizationRole.ORGANIZATION_ADMIN))
                        .map(x -> x.getUserProfileEntity().getEmail())
                        .collect(Collectors.toSet()));
            }

            String emailsString = String.join(",", emails);
            unicoNotifierService.sendApplyRequest(messageDto, emailsString, externalMessageDto.getSenderEmail());
        } else if (MessageType.EQUIPMENT == externalMessageDto.getMessageType()) {
            messageDto.setOrganizationName(organizationEntity.getOrganizationName());
            messageDto.setName(externalMessageDto.getName());
            messageDto.setItemCode(externalMessageDto.getItemCode());
            String emails = organizationEntity.getOrganizationUserRoleEntities().stream()
                    .filter(x -> !x.isDeleted() && x.getRole() == OrganizationRole.ORGANIZATION_EDITOR)
                    .map(x -> x.getUserProfileEntity().getEmail())
                    .collect(Collectors.joining(","));
            unicoNotifierService.sendExternalEquipmentRequest(messageDto, emails, externalMessageDto.getSenderEmail());
        }
    }

    @Override
    @Transactional
    public void sendContactUsMessage(ExternalMessageDto externalMessageDto) throws NonexistentEntityException, InvalidParameterException, IOException {
        recaptchaValidationService.validateToken(externalMessageDto.getRecaptcha());
        ExternalMessageEntity entity = new ExternalMessageEntity();
        entity.setMessage(externalMessageDto.getMessage());
        entity.setSenderEmail(externalMessageDto.getSenderEmail());
        entity.setMessageType(externalMessageDto.getMessageType());
        if (externalMessageDto.getMessageType() == null)
            throw new InvalidParameterException("Message type must bee filled.");
        if (externalMessageDto.getUserToId() != null && externalMessageDto.getExpertToId() != null)
            throw new InvalidParameterException("User id or Expert id must be filled not both.");
        if (externalMessageDto.getOrganizationId() == null)
            externalMessageDto.setOrganizationId(146863L);  // Unico ID

        final OrganizationEntity organizationEntity = organizationDao.find(externalMessageDto.getOrganizationId());
        entity.setOrganizationEntity(organizationEntity);
        externalMessageDao.saveMessage(entity);

        MessageDto messageDto = new MessageDto();
        messageDto.setUserFromName(externalMessageDto.getSenderEmail());
        messageDto.setContent(externalMessageDto.getMessage());

        unicoNotifierService.sendRequestInformation(messageDto, null, externalMessageDto.getSenderEmail());
    }

    @Override
    @Transactional
    public void sendRequestInformationMessage(ExternalMessageDto externalMessageDto) throws NonexistentEntityException, InvalidParameterException, IOException {
        ExternalMessageEntity entity = new ExternalMessageEntity();
        entity.setMessage(externalMessageDto.getMessage());
        entity.setSenderEmail(externalMessageDto.getSenderEmail());
        entity.setMessageType(externalMessageDto.getMessageType());

        if (externalMessageDto.getMessageType() == null)
            throw new InvalidParameterException("Message type must bee filled.");

        if (externalMessageDto.getOrganizationId() == null)
            throw new InvalidParameterException("Organization id must not be null.");
        final OrganizationEntity organizationEntity = organizationDao.find(externalMessageDto.getOrganizationId());
        entity.setOrganizationEntity(organizationEntity);

        externalMessageDao.saveMessage(entity);

        MessageDto messageDto = new MessageDto();
        messageDto.setOrganizationName(organizationEntity.getOrganizationName());
        messageDto.setUserFromName(externalMessageDto.getSenderEmail());
        messageDto.setContent(externalMessageDto.getMessage());

        unicoNotifierService.sendRequestInformation(messageDto, null, externalMessageDto.getSenderEmail());

    }

    @Override
    @Transactional
    public void sendApplyRequest(ExternalMessageDto externalMessageDto) throws NonexistentEntityException, InvalidParameterException, IOException {
        ExternalMessageEntity entity = new ExternalMessageEntity();
        entity.setMessage(externalMessageDto.getMessage());
        entity.setSenderEmail(externalMessageDto.getSenderEmail());
        entity.setMessageType(externalMessageDto.getMessageType());

        if (externalMessageDto.getMessageType() == null)
            throw new InvalidParameterException("Message type must bee filled.");

        if (externalMessageDto.getOrganizationId() == null)
            throw new InvalidParameterException("Organization id must not be null.");
        final OrganizationEntity organizationEntity = organizationDao.find(externalMessageDto.getOrganizationId());
        entity.setOrganizationEntity(organizationEntity);

        externalMessageDao.saveMessage(entity);

        MessageDto messageDto = new MessageDto();
        messageDto.setOrganizationName(organizationEntity.getOrganizationName());
        messageDto.setUserFromName(externalMessageDto.getSenderEmail());
        messageDto.setContent(externalMessageDto.getMessage());
        messageDto.setOpportunityId(externalMessageDto.getOpportunityId());
        messageDto.setOpportunityName(externalMessageDto.getOpportunityName());

        unicoNotifierService.sendApplyRequest(messageDto, null, externalMessageDto.getSenderEmail());
    }


}
