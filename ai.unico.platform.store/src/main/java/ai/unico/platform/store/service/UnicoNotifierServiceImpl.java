package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.ExternalMessageDto;
import ai.unico.platform.store.api.dto.InternalMessageDto;
import ai.unico.platform.store.api.dto.MessageDto;
import ai.unico.platform.store.api.enums.ClaimRequestType;
import ai.unico.platform.store.api.enums.MessageType;
import ai.unico.platform.store.api.service.UnicoNotifierService;
import ai.unico.platform.store.api.service.email.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.HashMap;

public class UnicoNotifierServiceImpl implements UnicoNotifierService {

    @Value("${env.host}")
    private String host;

    @Value("${contact.requests}")
    private String unicoEmail;

    @Autowired
    private EmailService emailService;

    @Override
    public void sendUserContactRequest(MessageDto messageDto, String contactEmail) {
        final Long userFromId = messageDto.getUserFromId();
        final String userFromName = messageDto.getUserFromName();

        final Long userToId = messageDto.getUserToId();
        final Long expertToId = messageDto.getExpertToId();
        final String userToName = messageDto.getUserToName();

        final String subject = messageDto.getSubject();
        final String content = messageDto.getContent();

        final HashMap<String, String> variables = new HashMap<>();
        variables.put("userTo", userToName);
        variables.put("urlUserTo", userToId != null ? createLinkToUserProfile(userToId) : createLinkToExpertProfile(expertToId));
        variables.put("userFrom", userFromName);
        variables.put("urlUserFrom", createLinkToUserProfile(userFromId));
//        variables.put("subject", subject);
        variables.put("message", content);
        variables.put("emailUserFrom", contactEmail);

        emailService.sendEmailWithTemplate(unicoEmail, "contact-user-by-unico", variables);

    }

    @Override
    public void sendRegisterNotificationToUnico(String email) {
        final HashMap<String, String> variables = new HashMap<>();
        variables.put("userTo", unicoEmail);
        variables.put("userFrom", email);
        variables.put("message", "User with email: " + email + " has been registered on platform!");
        variables.put("emailUserFrom", email);
        emailService.sendEmailWithTemplate(unicoEmail, "register-new-user", variables);
    }

    @Override
    public void sentRequestMail(InternalMessageDto internalMessageDto, String senderUserName, String organizationName, String emails) {
        emails += "," + unicoEmail;
        final HashMap<String, String> variables = new HashMap<>();
        variables.put("organizationTo", organizationName);
        variables.put("userFromName", senderUserName);
        variables.put("message", internalMessageDto.getMessage());
        variables.put("emailUserFrom", internalMessageDto.getSenderEmail());
        if(internalMessageDto.getMessageType() == MessageType.OUTCOME) {
            variables.put("outcomeName", internalMessageDto.getOutcomeName());
            emailService.sendEmailWithTemplate(emails, "internal-request-outcome", variables);
        } else if(internalMessageDto.getMessageType() == MessageType.PROJECT) {
            variables.put("projectName", internalMessageDto.getProjectName());
            emailService.sendEmailWithTemplate(emails, "internal-request-project", variables);
        }
    }

    @Override
    public void sendInternalOrganizationContactRequest(MessageDto messageDto, String emails, String contactEmail, String userFromName, String organizationName) {
        emails += "," + unicoEmail;
        final HashMap<String, String> variables = new HashMap<>();
        variables.put("organizationTo", organizationName);
        variables.put("userFromName", userFromName);
        variables.put("urlUserTo", messageDto.getUserToId() != null ? createLinkToUserProfile(messageDto.getUserToId()) : createLinkToExpertProfile(messageDto.getExpertToId()));
        variables.put("message", messageDto.getContent());
        variables.put("emailUserFrom", contactEmail);
        emailService.sendEmailWithTemplate(emails, "internal-contact-organization-by-unico", variables);
    }

    @Override
    public void sendExternalContactRequest(MessageDto messageDto, String emails, String contactEmail) {
        emails += "," + unicoEmail;
        final HashMap<String, String> variables = new HashMap<>();
        variables.put("userTo", messageDto.getUserToName());
        variables.put("urlUserTo", messageDto.getUserToId() != null ? createLinkToUserProfile(messageDto.getUserToId()) : createLinkToExpertProfile(messageDto.getExpertToId()));
        variables.put("message", messageDto.getContent());
        variables.put("emailUserFrom", contactEmail);
        emailService.sendEmailWithTemplate(emails, "external-contact-user-by-unico", variables);
    }

    @Override
    public void sendExternalLectureRequest(MessageDto messageDto, String emails, String contactEmail) {
        emails += "," + unicoEmail;
        final HashMap<String, String> variables = new HashMap<>();
        variables.put("lectureAbout", messageDto.getLectureAboutName());
        variables.put("urlLectureAbout", createLinkToLecture(messageDto.getLectureAboutCode()));
        variables.put("message", messageDto.getContent());
        variables.put("emailUserFrom", contactEmail);
        emailService.sendEmailWithTemplate(emails, "external-order-lecture-by-unico", variables);
    }

    @Override
    public void sendRequestInformation(MessageDto messageDto, String emails, String contactEmail) {
        if (emails != null && !emails.isEmpty())
            emails += "," + unicoEmail;
        else
            emails = unicoEmail;
        final HashMap<String, String> variables = new HashMap<>();
        variables.put("message", messageDto.getContent());
        variables.put("organizationTo", messageDto.getOrganizationName());
        variables.put("emailUserFrom", contactEmail);
        emailService.sendEmailWithTemplate(emails, "external-request-information-by-unico", variables);
    }

    @Override
    public void sendApplyRequest(MessageDto messageDto, String emails, String contactEmail) {
        if (emails != null && !emails.isEmpty())
            emails += "," + unicoEmail;
        else
            emails = unicoEmail;
        final HashMap<String, String> variables = new HashMap<>();
        variables.put("message", messageDto.getContent());
        variables.put("organizationTo", messageDto.getOrganizationName());
        variables.put("opportunityName",messageDto.getOpportunityName());
        variables.put("opportunityUrl", createLinkToOpportunityProfile(messageDto.getOpportunityId().toString()));
        variables.put("emailUserFrom", contactEmail);
        emailService.sendEmailWithTemplate(emails, "external-apply-request-by-unico", variables);
    }

    @Override
    public void sendExternalOutcomeProjectRequest(ExternalMessageDto externalMessageDto, MessageDto messageDto, String emails, String contactEmail) {
        emails += "," + unicoEmail;
        final HashMap<String, String> variables = new HashMap<>();
        variables.put("organizationTo", messageDto.getOrganizationName());
        variables.put("userFromName", messageDto.getUserFromName());
        variables.put("message", externalMessageDto.getMessage());
        variables.put("emailUserFrom", messageDto.getUserFromName());
        if(externalMessageDto.getMessageType() == MessageType.OUTCOME) {
            variables.put("outcomeOrRequest", MessageType.OUTCOME.name());
            variables.put("outcomeProjectName", externalMessageDto.getOutcomeName());
            emailService.sendEmailWithTemplate(emails, "external-request-outcome-project", variables);
        } else if(externalMessageDto.getMessageType() == MessageType.PROJECT) {
            variables.put("outcomeOrRequest", MessageType.PROJECT.name());
            variables.put("outcomeProjectName", externalMessageDto.getProjectName());
            emailService.sendEmailWithTemplate(emails, "external-request-outcome-project", variables);
        }
    }

    @Override
    public void sendExternalEquipmentRequest(MessageDto messageDto, String emails, String contactEmail) {
        emails += "," + unicoEmail;
        final HashMap<String, String> variables = new HashMap<>();
        variables.put("organizationTo", messageDto.getOrganizationName());
        variables.put("equipmentName", messageDto.getName());
        variables.put("message", messageDto.getContent());
        variables.put("emailUserFrom", contactEmail);
        variables.put("equipmentUrl", createLinkToEquipment(messageDto.getItemCode()));
        emailService.sendEmailWithTemplate(emails, "external-request-equipment", variables);
    }

    @Override
    public void sendExternalOrganizationContactRequest(MessageDto messageDto, String emails, String contactEmail, String organizationName) {
        emails += "," + unicoEmail;
        final HashMap<String, String> variables = new HashMap<>();
        variables.put("organizationTo", organizationName);
        variables.put("message", messageDto.getContent());
        variables.put("emailUserFrom", contactEmail);
        emailService.sendEmailWithTemplate(emails, "external-contact-organization-by-unico", variables);
    }

    @Override
    public void sendItemUnclaimNotification(Long userId, String userName, Long itemId, String itemName, String message) {

        final HashMap<String, String> variables = new HashMap<>();
        variables.put("userName", userName);
        variables.put("userUrl", createLinkToUserProfile(userId));
        variables.put("itemName", itemName);
        variables.put("itemId", itemId.toString());
        variables.put("message", message);

        emailService.sendEmailWithTemplate(unicoEmail, "item-unclaim-notification", variables);
    }

    @Override
    public void sendClaimRequestNotification(Long userId, String userName, String note, ClaimRequestType type) {
        final HashMap<String, String> variables = new HashMap<>();
        variables.put("userName", userName);
        variables.put("userUrl", createLinkToUserProfile(userId));
        variables.put("note", note);
        variables.put("type", type.name());

        emailService.sendEmailWithTemplate(unicoEmail, "claim-request-notification", variables);
    }

    private String createLinkToLecture(String lectureCode){
        return host + "lecture/" + lectureCode;
    }

    private String createLinkToUserProfile(Long userId) {
        return host + "profiles/" + userId;
    }

    private String createLinkToExpertProfile(Long expertId) {
        return host + "experts/" + expertId;
    }

    private String createLinkToOpportunityProfile(String opportunityId){
        return host + "widgets/opportunity/" + opportunityId;
    }

    private String createLinkToEquipment(String equipmentId){
        return host + "equipment/" + equipmentId;
    }

}
