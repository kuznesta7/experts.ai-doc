package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.ProjectBudgetDto;
import ai.unico.platform.store.api.enums.Currency;
import ai.unico.platform.store.api.service.ProjectBudgetService;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.dao.ProjectBudgetDao;
import ai.unico.platform.store.dao.ProjectDao;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.entity.ProjectBudgetEntity;
import ai.unico.platform.store.entity.ProjectEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class ProjectBudgetServiceImpl implements ProjectBudgetService {

    @Autowired
    private ProjectBudgetDao projectBudgetDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private ProjectDao projectDao;

    @Override
    @Transactional
    public void updateProjectBudget(Long projectId, Long organizationId, Integer year, ProjectBudgetDto projectBudgetDto, UserContext userContext) {
        final ProjectBudgetEntity existingProjectBudget = projectBudgetDao.find(projectId, organizationId, year);
        if (existingProjectBudget != null) {
            existingProjectBudget.setDeleted(true);
            TrackableUtil.fillUpdate(existingProjectBudget, userContext);
        }

        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        final ProjectEntity projectEntity = projectDao.find(projectId);
        if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
        if (projectEntity == null) throw new NonexistentEntityException(ProjectEntity.class);

        final ProjectBudgetEntity projectBudgetEntity = new ProjectBudgetEntity();
        projectBudgetEntity.setYear(year);
        projectBudgetEntity.setProjectEntity(projectEntity);
        projectBudgetEntity.setOrganizationEntity(organizationEntity);
        projectBudgetEntity.setTotalAmount(projectBudgetDto.getTotalAmount());
        projectBudgetEntity.setOtherAmount(projectBudgetDto.getOtherAmount());
        projectBudgetEntity.setPrivateAmount(projectBudgetDto.getPrivateAmount());
        projectBudgetEntity.setNationalSupportAmount(projectBudgetDto.getNationalSupportAmount());
        projectBudgetEntity.setCurrency(Currency.CZK);
        TrackableUtil.fillCreate(projectBudgetEntity, userContext);
        projectBudgetDao.save(projectBudgetEntity);
    }

    @Override
    @Transactional
    public void deleteProjectBudget(Long projectId, Long organizationId, Integer year, UserContext userContext) {
        final ProjectBudgetEntity projectBudgetEntity = projectBudgetDao.find(projectId, organizationId, year);
        projectBudgetEntity.setDeleted(true);
        TrackableUtil.fillUpdate(projectBudgetEntity, userContext);
    }
}
