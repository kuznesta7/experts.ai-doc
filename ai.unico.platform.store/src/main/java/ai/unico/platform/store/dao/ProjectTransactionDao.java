package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ProjectTransactionEntity;

import java.util.List;

public interface ProjectTransactionDao {

    ProjectTransactionEntity findTransaction(Long transactionId);

    void saveTransaction(ProjectTransactionEntity projectTransactionEntity);

    List<ProjectTransactionEntity> findProjectTransactions(Long projectId);

}
