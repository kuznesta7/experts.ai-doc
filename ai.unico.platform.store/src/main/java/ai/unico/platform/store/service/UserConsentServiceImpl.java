package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.ConsentDto;
import ai.unico.platform.store.api.service.UserConsentService;
import ai.unico.platform.store.dao.ConsentDao;
import ai.unico.platform.store.dao.UserConsentDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.ConsentEntity;
import ai.unico.platform.store.entity.UserConsentEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class UserConsentServiceImpl implements UserConsentService {

    @Autowired
    private UserConsentDao userConsentDao;

    @Autowired
    private ConsentDao consentDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Override
    @Transactional
    public List<ConsentDto> findAllConsents() {
        final ArrayList<ConsentDto> consentDtos = new ArrayList<>();
        final List<ConsentEntity> allConsents = consentDao.findAllConsents();
        for (ConsentEntity consent : allConsents) {
            final ConsentDto consentDto = new ConsentDto();
            consentDto.setConsentId(consent.getConsentId());
            consentDto.setConsentTitle(consent.getTitle());
            consentDtos.add(consentDto);
        }
        return consentDtos;
    }

    @Override
    @Transactional
    public List<ConsentDto> findUserConsents(Long userId) {
        final List<ConsentDto> consentDtos = findAllConsents();
        final UserProfileEntity userProfile = userProfileDao.findUserProfile(userId);
        final Set<UserConsentEntity> userConsentEntities = userProfile.getUserConsentEntities();
        for (ConsentDto consentDto : consentDtos) {
            final Optional<UserConsentEntity> consentEntity = userConsentEntities.stream()
                    .filter(userConsentEntity
                            -> !userConsentEntity.getDeleted()
                            && userConsentEntity.getConsentEntity().getConsentId().equals(consentDto.getConsentId())).findFirst();
            consentDto.setValid(consentEntity.isPresent());
        }
        return consentDtos;
    }

    @Override
    @Transactional
    public Boolean hasUserConsent(Long userId, Long consentId) {
        return userConsentDao.findUserConsentEntity(userId, consentId) != null;
    }

    @Override
    @Transactional
    public void addUserConsent(Long userId, Long consentId, UserContext userContext) throws NonexistentEntityException {
        if (hasUserConsent(userId, consentId)) return;

        final UserConsentEntity userConsentEntity = new UserConsentEntity();
        final ConsentEntity consentEntity = consentDao.findConsent(consentId);
        final UserProfileEntity userProfile = userProfileDao.findUserProfile(userId);

        if (consentEntity == null) throw new NonexistentEntityException(ConsentEntity.class);
        if (userProfile == null) throw new NonexistentEntityException(UserProfileEntity.class);

        userConsentEntity.setConsentEntity(consentEntity);
        userConsentEntity.setUserProfileEntity(userProfile);
        userConsentEntity.setDeleted(false);

        TrackableUtil.fillCreate(userConsentEntity, userContext);
        userConsentDao.save(userConsentEntity);
    }

    @Override
    @Transactional
    public void updateUserConsents(Long userId, List<ConsentDto> consentDtos, UserContext userContext) {
        final UserProfileEntity userProfileEntity = userProfileDao.findUserProfile(userId);
        final List<UserConsentEntity> userConsentEntities = userConsentDao.findUserConsentEntities(userId);
        final Map<Long, UserConsentEntity> userConsentMap = userConsentEntities.stream().collect(Collectors.toMap(c -> HibernateUtil.getId(c, UserConsentEntity::getConsentEntity, ConsentEntity::getConsentId), Function.identity()));

        final List<ConsentEntity> allConsents = consentDao.findAllConsents();
        final Map<Long, ConsentEntity> consentEntityMap = allConsents.stream().collect(Collectors.toMap(ConsentEntity::getConsentId, Function.identity()));
        for (ConsentDto consentDto : consentDtos) {
            final Long consentId = consentDto.getConsentId();
            if (consentDto.getValid()) {
                if (!userConsentMap.containsKey(consentId)) {
                    final UserConsentEntity userConsentEntity = new UserConsentEntity();
                    userConsentEntity.setConsentEntity(consentEntityMap.get(consentId));
                    userConsentEntity.setUserProfileEntity(userProfileEntity);
                    userConsentEntity.setDeleted(false);
                    TrackableUtil.fillCreate(userConsentEntity, userContext);
                    userConsentDao.save(userConsentEntity);
                }
            } else {
                if (userConsentMap.containsKey(consentId)) {
                    final UserConsentEntity userConsentEntity = userConsentMap.get(consentId);
                    userConsentEntity.setDeleted(true);
                    TrackableUtil.fillUpdate(userConsentEntity, userContext);
                }
            }
        }
    }
}
