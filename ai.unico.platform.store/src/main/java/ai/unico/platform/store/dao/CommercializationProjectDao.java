package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.CommercializationProjectEntity;
import ai.unico.platform.store.entity.CommercializationProjectKeywordEntity;
import ai.unico.platform.store.entity.CommercializationProjectStatusEntity;

import java.util.List;

public interface CommercializationProjectDao {

    CommercializationProjectEntity find(Long commercializationProjectId);

    void save(CommercializationProjectEntity commercializationProjectEntity);

    void save(CommercializationProjectStatusEntity commercializationProjectStatusEntity);

    void save(CommercializationProjectKeywordEntity projectKeywordEntity);

    List<CommercializationProjectEntity> findAll(Integer limit, Integer offset);

    List<CommercializationProjectStatusEntity> findStatusHistory(Long commercializationProjectId);

    List<CommercializationProjectEntity> findForUser(Long userId);

    List<CommercializationProjectEntity> findForExpert(Long expertId);

}
