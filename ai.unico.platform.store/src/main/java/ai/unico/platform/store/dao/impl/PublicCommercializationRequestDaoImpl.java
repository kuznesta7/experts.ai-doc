package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.PublicCommercializationRequestDao;
import ai.unico.platform.store.entity.PublicCommercializationProjectRequestEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class PublicCommercializationRequestDaoImpl implements PublicCommercializationRequestDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(PublicCommercializationProjectRequestEntity entity) {
        entityManager.persist(entity);
    }
}
