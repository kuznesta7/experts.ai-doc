package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.CommercializationDomainDto;
import ai.unico.platform.store.api.service.CommercializationDomainService;
import ai.unico.platform.store.dao.CommercializationDomainDao;
import ai.unico.platform.store.entity.CommercializationDomainEntity;
import ai.unico.platform.store.util.ReferenceDataUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

public class CommercializationDomainServiceImpl implements CommercializationDomainService {

    @Autowired
    private CommercializationDomainDao commercializationDomainDao;

    @Override
    @Transactional
    public Long createCommercializationDomain(CommercializationDomainDto commercializationDomainDto, UserContext userContext) {
        final CommercializationDomainEntity commercializationDomainEntity = new CommercializationDomainEntity();
        commercializationDomainEntity.setCode(commercializationDomainDto.getDomainCode());
        commercializationDomainEntity.setSystemData(false);
        fillEntity(commercializationDomainEntity, commercializationDomainDto);
        TrackableUtil.fillCreate(commercializationDomainEntity, userContext);
        commercializationDomainDao.save(commercializationDomainEntity);
        return commercializationDomainEntity.getCommercializationDomainId();
    }

    @Override
    @Transactional
    public void updateCommercializationDomain(Long domainId, CommercializationDomainDto commercializationDomainDto, UserContext userContext) {
        final CommercializationDomainEntity domain = commercializationDomainDao.findDomain(domainId);
        if (domain == null) throw new NonexistentEntityException(CommercializationDomainEntity.class);
        fillEntity(domain, commercializationDomainDto);
        TrackableUtil.fillUpdate(domain, userContext);
    }

    @Override
    @Transactional
    public List<CommercializationDomainDto> findAvailableCommercializationDomains(boolean includeInactive) {
        final List<CommercializationDomainEntity> allDomains = commercializationDomainDao.findAll();
        if (includeInactive) return allDomains.stream().map(this::convert).collect(Collectors.toList());
        return ReferenceDataUtil.filterAndConvertReferenceData(allDomains, this::convert);
    }

    private void fillEntity(CommercializationDomainEntity commercializationDomainEntity, CommercializationDomainDto commercializationDomainDto) {
        commercializationDomainEntity.setName(commercializationDomainDto.getDomainName());
        commercializationDomainEntity.setDescription(commercializationDomainDto.getDomainDescription());
        commercializationDomainEntity.setDisplayOrder(commercializationDomainDto.getOrder());
        commercializationDomainEntity.setDeleted(commercializationDomainDto.isDeleted());
    }

    private CommercializationDomainDto convert(CommercializationDomainEntity commercializationDomainEntity) {
        final CommercializationDomainDto commercializationDomainDto = new CommercializationDomainDto();
        commercializationDomainDto.setDomainId(commercializationDomainEntity.getCommercializationDomainId());
        commercializationDomainDto.setDomainName(commercializationDomainEntity.getName());
        commercializationDomainDto.setDomainCode(commercializationDomainEntity.getCode());
        commercializationDomainDto.setDomainDescription(commercializationDomainEntity.getDescription());
        commercializationDomainDto.setDeleted(commercializationDomainEntity.isDeleted());
        return commercializationDomainDto;
    }
}
