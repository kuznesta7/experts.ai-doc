package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.AskForPermissionsDto;
import ai.unico.platform.store.api.service.AskForPermissionsService;
import ai.unico.platform.store.api.service.email.EmailService;
import ai.unico.platform.store.dao.AskForPermissionsDao;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.dao.OrganizationRoleDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.AskForPermissionsEntity;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.entity.OrganizationUserRoleEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.ume.api.dto.UserDto;
import ai.unico.platform.ume.api.service.UserService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.enums.Role;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

public class AskForPermissionsServiceImpl implements AskForPermissionsService {

    @Autowired
    private OrganizationRoleDao organizationRoleDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private EmailService emailService;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private AskForPermissionsDao askForPermissionsDao;

    @Value("${env.host}")
    private String host;


    @Override
    @Transactional
    public void askForPermissions(AskForPermissionsDto askForPermissionsDto, UserContext userContext) {
        final Long organizationId = askForPermissionsDto.getOrganizationId();
        final AskForPermissionsEntity askForPermissionsEntity = new AskForPermissionsEntity();
        askForPermissionsEntity.setMessage(askForPermissionsDto.getMessage());
        askForPermissionsEntity.setSourceUrl(askForPermissionsDto.getSourceUrl());
        TrackableUtil.fillCreate(askForPermissionsEntity, userContext);
        askForPermissionsDao.save(askForPermissionsEntity);

        final Long userId = userContext.getUserId();
        final UserProfileEntity userProfile = userProfileDao.findUserProfile(userId);

        final UserService userService = ServiceRegistryUtil.getService(UserService.class);
        final Set<String> emails = new HashSet<>();
        final Map<String, String> variables = new HashMap<>();
        variables.put("userFullName", userProfile.getName());
        variables.put("userEmail", userProfile.getEmail());
        variables.put("userMessage", askForPermissionsDto.getMessage());
        variables.put("userUrl", host + "users/" + userId);
        if (organizationId != null) {
            final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
            if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
            askForPermissionsEntity.setOrganizationEntity(organizationEntity);
            variables.put("organizationUrl", host + "evidence/organizations/" + organizationId);
            variables.put("organizationName", organizationEntity.getOrganizationName());

            final List<OrganizationUserRoleEntity> organizationUserRoleEntities = organizationRoleDao.findForOrganization(organizationId, OrganizationRole.ORGANIZATION_ADMIN);
            organizationUserRoleEntities.stream()
                    .map(OrganizationUserRoleEntity::getUserProfileEntity)
                    .map(UserProfileEntity::getEmail)
                    .forEach(emails::add);
        } else {
            variables.put("organizationName", "no organization");
        }
        final List<UserDto> usersWithRole = userService.findUsersWithRole(Role.PORTAL_ADMIN);
        usersWithRole.stream()
                .map(UserDto::getEmail)
                .forEach(emails::add);
        final String emailsString = String.join(",", emails);

        emailService.sendEmailWithTemplate(emailsString, "new-permissions-request", variables);
    }
}
