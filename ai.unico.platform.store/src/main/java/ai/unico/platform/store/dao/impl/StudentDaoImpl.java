package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.StudentDao;
import ai.unico.platform.store.entity.StudentEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class StudentDaoImpl implements StudentDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public StudentEntity find(String studentHash) {
        return entityManager.find(StudentEntity.class, studentHash);
    }
}
