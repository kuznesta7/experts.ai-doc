package ai.unico.platform.store.entity;

import ai.unico.platform.util.enums.PlatformDataSource;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
public class InteractionIndividualEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long interactionIndividualId;

    private String visitedExpertCode;

    private Long organizationId;

    private Date date = new Timestamp(new Date().getTime());

    private String sessionId;

    private String visitedOutcomeCode;
    private String visitedProjectCode;
    private Long visitedCommercializationId;
    private String visitedLectureCode;

    @Enumerated(EnumType.STRING)
    private PlatformDataSource itemLocation;

    public Long getInteractionIndividualId() {
        return interactionIndividualId;
    }

    public void setInteractionIndividualId(Long interactionIndividualId) {
        this.interactionIndividualId = interactionIndividualId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getVisitedExpertCode() {
        return visitedExpertCode;
    }

    public void setVisitedExpertCode(String expertCode) {
        this.visitedExpertCode = expertCode;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getVisitedOutcomeCode() {
        return visitedOutcomeCode;
    }

    public void setVisitedOutcomeCode(String visitedOutcomeCode) {
        this.visitedOutcomeCode = visitedOutcomeCode;
    }

    public String getVisitedProjectCode() {
        return visitedProjectCode;
    }

    public void setVisitedProjectCode(String visitedProjectCode) {
        this.visitedProjectCode = visitedProjectCode;
    }

    public Long getVisitedCommercializationId() {
        return visitedCommercializationId;
    }

    public void setVisitedCommercializationId(Long visitedCommercializationId) {
        this.visitedCommercializationId = visitedCommercializationId;
    }

    public PlatformDataSource getItemLocation() {
        return itemLocation;
    }

    public void setItemLocation(PlatformDataSource itemLocation) {
        this.itemLocation = itemLocation;
    }

    public String getVisitedLectureCode() {
        return visitedLectureCode;
    }

    public void setVisitedLectureCode(String visitedLectureCode) {
        this.visitedLectureCode = visitedLectureCode;
    }
}
