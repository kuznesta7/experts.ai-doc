package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.api.enums.ShareCodeType;
import ai.unico.platform.store.dao.ShareCodeDao;
import ai.unico.platform.store.entity.ShareCodeEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ShareCodeDaoImpl implements ShareCodeDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(ShareCodeEntity shareCodeEntity) {
        entityManager.persist(shareCodeEntity);
    }

    @Override
    public ShareCodeEntity find(String shareCode, ShareCodeType type) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ShareCodeEntity.class, "shareCode");
        criteria.add(Restrictions.eq("code", shareCode));
        criteria.add(Restrictions.eq("type", type));
        criteria.add(Restrictions.eq("deleted", false));
        return (ShareCodeEntity) criteria.uniqueResult();
    }
}
