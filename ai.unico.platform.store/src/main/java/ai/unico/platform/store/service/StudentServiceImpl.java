package ai.unico.platform.store.service;

import ai.unico.platform.store.api.service.StudentService;
import ai.unico.platform.store.dao.StudentDao;
import ai.unico.platform.store.entity.StudentEntity;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentDao studentDao;

    public List<String> getStudentRecommendations(String usernameHash){
        final StudentEntity st = studentDao.find(usernameHash);
        String[] recommendations = st.getRecommendation().replaceAll("\\s+","").split(",");
        return Arrays.asList(recommendations);
    }

    public String getStudentTestGroup(String usernameHash){
        final StudentEntity st = studentDao.find(usernameHash);
        return st.getTestGroup();
    }
}
