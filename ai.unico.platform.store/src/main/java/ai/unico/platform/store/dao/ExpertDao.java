package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ExpertEntity;
import ai.unico.platform.util.model.UserContext;

import java.util.List;

public interface ExpertDao {

    ExpertEntity findOrCreate(Long expertId, UserContext userContext);

    ExpertEntity find(Long expertId);

    void save(ExpertEntity expertEntity);

    List<ExpertEntity> findAll();
}
