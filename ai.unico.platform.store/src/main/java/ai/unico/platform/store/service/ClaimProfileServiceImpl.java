package ai.unico.platform.store.service;

import ai.unico.platform.search.api.service.ElasticsearchService;
import ai.unico.platform.search.api.service.ExpertIndexService;
import ai.unico.platform.search.api.service.ExpertLookupService;
import ai.unico.platform.store.api.dto.ClaimRequestDto;
import ai.unico.platform.store.api.dto.ExpertClaimDto;
import ai.unico.platform.store.api.dto.PreClaimDto;
import ai.unico.platform.store.api.dto.UserProfileDto;
import ai.unico.platform.store.api.service.ClaimProfileService;
import ai.unico.platform.store.api.service.UnicoNotifierService;
import ai.unico.platform.store.api.service.UserProfileService;
import ai.unico.platform.store.dao.*;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.store.util.UserProfileIndexUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ClaimProfileServiceImpl implements ClaimProfileService {

    @Autowired
    private UserExpertDao userExpertDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private UserOrganizationDao userOrganizationDao;

    @Autowired
    private FollowProfileDao followProfileDao;

    @Autowired
    private ProfileVisitationDao profileVisitationDao;

    @Autowired
    private SearchHistoryDao searchHistoryDao;

    @Autowired
    private UnicoNotifierService unicoNotifierService;

    @Autowired
    private ClaimRequestDao claimRequestDao;

    @Autowired
    private UserPreClaimDao userPreClaimDao;

    @Autowired
    private UserProfileService userProfileService;

    @Override
    @Transactional
    public void claimExpertProfile(Long expertId, Long userId, UserContext userContext) throws NonexistentEntityException, EntityAlreadyExistsException, IOException {
        final UserExpertEntity userExpertEntity = new UserExpertEntity();
        final UserProfileEntity userProfile = userProfileDao.findUserProfile(userId);

        final List<UserExpertEntity> userExperts = userExpertDao.findForExperts(Collections.singleton(expertId));
        if (userExperts.size() != 0) {
            throw new EntityAlreadyExistsException("EXPERT_ALREADY_CLAIMED", "User " + userContext.getUserId() + "trying to claim expert profile " + expertId);
        }
        final ExpertLookupService service = ServiceRegistryUtil.getService(ExpertLookupService.class);
        final ExpertClaimDto expertProfilePreviewDto = service.findExpertToClaim(expertId);

        userExpertEntity.setUserProfileEntity(userProfile);
        userExpertEntity.setExpertId(expertId);
        userExpertEntity.setDeleted(false);
        TrackableUtil.fillCreate(userExpertEntity, userContext);
        userExpertDao.save(userExpertEntity);

        final Set<Long> addedOrganizationIds = userProfile.getUserOrganizationEntities().stream()
                .filter(x -> !x.isDeleted())
                .map(UserOrganizationEntity::getOrganizationEntity)
                .map(OrganizationEntity::getOrganizationId)
                .collect(Collectors.toSet());

        final Set<Long> verifiedOrganizationIds = expertProfilePreviewDto.getVerifiedOrganizationIds();
        for (Long organizationId : expertProfilePreviewDto.getOrganizationIds()) {
            if (organizationId == null || addedOrganizationIds.contains(organizationId)) continue;
            final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
            final UserOrganizationEntity userOrganizationEntity = new UserOrganizationEntity();
            userOrganizationEntity.setOrganizationEntity(organizationEntity);
            userOrganizationEntity.setUserProfileEntity(userProfile);
            userOrganizationEntity.setVerified(verifiedOrganizationIds.contains(organizationId));
            TrackableUtil.fillCreate(userOrganizationEntity, userContext);
            userOrganizationDao.saveUserOrganization(userOrganizationEntity);
            userProfile.getUserOrganizationEntities().add(userOrganizationEntity);
        }

        for (FollowedProfileEntity followedProfileEntity : followProfileDao.findFollowersForExpert(expertId)) {
//            followedProfileEntity.setFollowedExpertId(null);
            followedProfileEntity.setFollowedUser(userProfile);
            TrackableUtil.fillUpdate(followedProfileEntity, userContext);
        }

        for (SearchHistResultEntity searchHistResultEntity : searchHistoryDao.findSearchResultsForExpert(expertId)) {
//            searchHistResultEntity.setResultExpertId(null);
            searchHistResultEntity.setResultUserId(userId);
        }

        for (ProfileVisitationEntity expertProfileVisitation : profileVisitationDao.findExpertProfileVisitations(expertId)) {
//            expertProfileVisitation.setExpertId(null);
            expertProfileVisitation.setUserProfileEntity(userProfile);
            TrackableUtil.fillUpdate(expertProfileVisitation, userContext);
        }
        final ExpertIndexService expertIndexService = ServiceRegistryUtil.getService(ExpertIndexService.class);
        UserProfileIndexUtil.indexUserProfile(userProfile);
        expertIndexService.replaceExpert(expertId, userId);
    }

    @Override
    @Transactional
    public void claimUserProfile(Long userToClaimId, Long claimingUserId, UserContext userContext) throws IOException {
        final UserProfileEntity userToClaim = userProfileDao.findUserProfile(userToClaimId);
        final UserProfileEntity claimerUserProfile = userProfileDao.findUserProfile(claimingUserId);
        if (claimerUserProfile == null)
            throw new NonexistentEntityException(UserProfileEntity.class, "Claiming user doesn't exist");
        if (userToClaim == null || !userToClaim.isClaimable())
            throw new NonexistentEntityException(UserProfileEntity.class, "User to claim doesn't exist");
        if (userToClaim.getClaimedBy() != null)
            throw new EntityAlreadyExistsException("EXPERT_ALREADY_CLAIMED", "User " + userContext.getUserId() + "trying to claim expert profile " + userToClaim);
        userToClaim.setClaimedBy(claimerUserProfile);

        final Set<Long> addedOrganizationIds = claimerUserProfile.getUserOrganizationEntities().stream()
                .filter(userOrganizationEntity -> !userOrganizationEntity.isDeleted())
                .map(UserOrganizationEntity::getOrganizationEntity)
                .map(OrganizationEntity::getOrganizationId)
                .collect(Collectors.toSet());

        for (UserOrganizationEntity userOrganizationEntity : userToClaim.getUserOrganizationEntities()) {
            if (userOrganizationEntity.isDeleted() || addedOrganizationIds.contains(userOrganizationEntity.getOrganizationEntity().getOrganizationId()))
                continue;
            final UserOrganizationEntity newUserOrganizationEntity = new UserOrganizationEntity();
            newUserOrganizationEntity.setUserProfileEntity(claimerUserProfile);
            newUserOrganizationEntity.setOrganizationEntity(userOrganizationEntity.getOrganizationEntity());
            newUserOrganizationEntity.setVerified(userOrganizationEntity.isVerified());
            userOrganizationDao.saveUserOrganization(newUserOrganizationEntity);
            claimerUserProfile.getUserOrganizationEntities().add(newUserOrganizationEntity);
        }
        for (FollowedProfileEntity followedProfileEntity : userToClaim.getFollowerProfileEntities()) {
            if (followedProfileEntity.isDeleted()) continue;
            followedProfileEntity.setFollowedUser(claimerUserProfile);
            TrackableUtil.fillUpdate(followedProfileEntity, userContext);
        }
        for (SearchHistResultEntity searchHistResultEntity : searchHistoryDao.findSearchResultsForUser(userToClaimId)) {
            searchHistResultEntity.setResultUserId(claimingUserId);
        }
        for (ProfileVisitationEntity expertProfileVisitation : profileVisitationDao.findUserProfileVisitations(userToClaimId)) {
            expertProfileVisitation.setUserProfileEntity(claimerUserProfile);
            TrackableUtil.fillUpdate(expertProfileVisitation, userContext);
        }

        final ExpertIndexService expertIndexService = ServiceRegistryUtil.getService(ExpertIndexService.class);
        UserProfileIndexUtil.indexUserProfile(claimerUserProfile);
        expertIndexService.replaceUser(userToClaimId, claimingUserId);
    }

    @Override
    @Transactional
    public void unclaimExpertProfile(Long expertId, UserContext userContext) throws NonexistentEntityException, IOException {
        final List<UserExpertEntity> forExperts = userExpertDao.findForExperts(Collections.singleton(expertId));
        for (UserExpertEntity userExpertEntity : forExperts) {
            userExpertEntity.setDeleted(true);
            final ExpertIndexService expertIndexService = ServiceRegistryUtil.getService(ExpertIndexService.class);
            expertIndexService.revertReplaceExpert(userExpertEntity.getUserProfileEntity().getUserId(), expertId);
        }
    }

    @Override
    @Transactional
    public void unclaimUserProfile(Long userId, UserContext userContext) throws NonexistentEntityException, IOException {
        final UserProfileEntity claimedProfile = userProfileDao.findUserProfile(userId);
        if (claimedProfile == null || !claimedProfile.isClaimable() || claimedProfile.getClaimedBy() == null)
            throw new NonexistentEntityException(UserProfileEntity.class);
        final UserProfileEntity claimedBy = claimedProfile.getClaimedBy();
        claimedProfile.setClaimedBy(null);
        final ExpertIndexService expertIndexService = ServiceRegistryUtil.getService(ExpertIndexService.class);
        expertIndexService.revertReplaceUser(claimedBy.getUserId(), userId);
    }

    @Override
    @Transactional
    public void preClaimProfiles(Long userId, Set<Long> userToClaimIds, Set<Long> expertToClaimIds, UserContext userContext) throws IOException {
        final UserProfileEntity userProfileEntity = userProfileDao.findUserProfile(userId);
        if (userToClaimIds != null && !userToClaimIds.isEmpty()) {
            final List<UserProfileEntity> userProfilesToClaim = userProfileDao.findByIds(userToClaimIds);
            for (UserProfileEntity userProfileToClaimEntity : userProfilesToClaim) {
                final UserPreClaimEntity userPreClaimEntity = new UserPreClaimEntity();
                userPreClaimEntity.setUserProfileEntity(userProfileEntity);
                userPreClaimEntity.setUserToClaimEntity(userProfileToClaimEntity);
                TrackableUtil.fillCreate(userPreClaimEntity, userContext);
                userPreClaimDao.save(userPreClaimEntity);
            }
        }
        if (expertToClaimIds != null) {
            for (Long expertToClaimId : expertToClaimIds) {
                final UserPreClaimEntity userPreClaimEntity = new UserPreClaimEntity();
                userPreClaimEntity.setUserProfileEntity(userProfileEntity);
                userPreClaimEntity.setExpertToClaimId(expertToClaimId);
                TrackableUtil.fillCreate(userPreClaimEntity, userContext);
                userPreClaimDao.save(userPreClaimEntity);
            }
        }
    }

    @Override
    @Transactional
    public void claimAllPreClaimedProfiles(Long userId, UserContext userContext) throws IOException {
        final List<UserPreClaimEntity> preClaimsForUser = userPreClaimDao.findPreClaimsForUser(userId);
        final Set<Claim> claimsForUser = findClaimsForUser(userId);
        final Set<Long> expertIds = claimsForUser.stream()
                .map(Claim::getExpertId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        final Set<Long> userIds = claimsForUser.stream()
                .map(Claim::getUserId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        for (UserPreClaimEntity userPreClaimEntity : preClaimsForUser) {
            final Long userToClaimId = HibernateUtil.getId(userPreClaimEntity, UserPreClaimEntity::getUserToClaimEntity, UserProfileEntity::getUserId);
            final Long expertToClaimId = userPreClaimEntity.getExpertToClaimId();
            if (userToClaimId != null && !userIds.contains(userToClaimId)) {
                claimUserProfile(userToClaimId, userId, userContext);
                userPreClaimEntity.setResolved(true);
            }
            if (expertToClaimId != null && !expertIds.contains(expertToClaimId)) {
                claimExpertProfile(expertToClaimId, userId, userContext);
                userPreClaimEntity.setResolved(true);
            }
            TrackableUtil.fillUpdate(userPreClaimEntity, userContext);
        }

        final UserProfileEntity userProfile = userProfileDao.findUserProfile(userId);
        userProfile.setInvitation(false);
        TrackableUtil.fillUpdate(userProfile, userContext);
    }

    @Override
    @Transactional
    public Map<Long, Long> findClaimedExpertIdUserIdMap() {
        final List<UserExpertEntity> all = userExpertDao.findAll();
        return createExpertIdUserIdMap(all);
    }

    @Override
    @Transactional
    public Map<Long, Long> findClaimedExpertIdUserIdMap(Set<Long> expertIds) {
        final List<UserExpertEntity> forExperts = userExpertDao.findForExperts(expertIds);
        return createExpertIdUserIdMap(forExperts);
    }

    @Override
    @Transactional
    public Map<Long, Long> findClaimedUserIdUserIdMap() {
        final List<UserProfileEntity> claimedUserProfiles = userProfileDao.findClaimedUserProfiles();
        return createUserIdUserIdMap(claimedUserProfiles);
    }

    @Override
    @Transactional
    public Map<Long, Long> findClaimedUserIdUserIdMap(Set<Long> userIds) {
        final List<UserProfileEntity> all = userProfileDao.findClaimedUserProfiles(userIds);
        return createUserIdUserIdMap(all);
    }

    @Override
    @Transactional
    public Set<Long> findVerifiedProfilesToClaimIds() {
        final List<UserProfileEntity> claimableVerifiedProfiles = userProfileDao.findClaimableVerifiedProfiles();
        return claimableVerifiedProfiles.stream().map(UserProfileEntity::getUserId).collect(Collectors.toSet());
    }

    @Override
    @Transactional
    public void requestClaiming(ClaimRequestDto claimRequestDto, UserContext userContext) {
        final ClaimRequestEntity claimRequestEntity = new ClaimRequestEntity();
        claimRequestEntity.setNote(claimRequestDto.getMessage());
        claimRequestEntity.setType(claimRequestDto.getClaimRequestType());
        claimRequestEntity.setConcernedUserId(claimRequestDto.getConcernedUserId());
        claimRequestEntity.setConcernedExpertId(claimRequestDto.getConcernedExpertId());
        TrackableUtil.fillCreate(claimRequestEntity, userContext);
        claimRequestDao.save(claimRequestEntity);
        unicoNotifierService.sendClaimRequestNotification(userContext.getUserId(), userContext.getFullName(), claimRequestDto.getMessage(), claimRequestDto.getClaimRequestType());
    }

    @Override
    public Set<Claim> findClaimsForUser(Long userId) {
        final UserProfileEntity userProfileEntity = userProfileDao.findUserProfileWithPreloadedClaims(userId);
        final Set<Claim> claims = new HashSet<>();
        final Claim claim = new Claim();
        claim.setUserId(userId);
        claims.add(claim);
        for (UserExpertEntity userExpertEntity : userProfileEntity.getUserExpertEntities()) {
            final Claim expertClaim = new Claim();
            expertClaim.setExpertId(userExpertEntity.getExpertId());
            claims.add(expertClaim);
        }

        for (UserProfileEntity claimedUserProfile : userProfileEntity.getClaimedUserProfiles()) {
            final Claim userClaim = new Claim();
            userClaim.setUserId(claimedUserProfile.getUserId());
            claims.add(userClaim);
            for (UserExpertEntity userExpertEntity : claimedUserProfile.getUserExpertEntities()) {
                final Claim expertClaim = new Claim();
                expertClaim.setExpertId(userExpertEntity.getExpertId());
                claims.add(expertClaim);
            }
        }
        return claims;
    }

    @Override
    @Transactional
    public List<Claim> findPreClaimsForUser(Long userId) {
        final List<UserPreClaimEntity> preClaimsForUser = userPreClaimDao.findPreClaimsForUser(userId);
        final List<Claim> claims = new ArrayList<>();
        for (UserPreClaimEntity userPreClaimEntity : preClaimsForUser) {
            final Claim claim = new Claim();
            claim.setExpertId(userPreClaimEntity.getExpertToClaimId());
            claim.setUserId(HibernateUtil.getId(userPreClaimEntity, UserPreClaimEntity::getUserToClaimEntity, UserProfileEntity::getUserId));
            claims.add(claim);
        }
        return claims;
    }

    @Override
    @Transactional
    public List<PreClaimDto> findPreClaimsForUsersExperts(Set<Long> userIds, Set<Long> expertIds) {
        final List<UserPreClaimDao.PreClaim> preClaims = userPreClaimDao.findPreClaims(userIds, expertIds);
        final List<PreClaimDto> preClaimDtos = new ArrayList<>();
        for (UserPreClaimDao.PreClaim preClaim : preClaims) {
            final PreClaimDto preClaimDto = new PreClaimDto();
            preClaimDto.setUserId(preClaim.getUserId());
            preClaimDto.setUserEmail(preClaim.getEmail());
            preClaimDto.setUserToClaimId(preClaim.getUserToClaimId());
            preClaimDto.setExpertToClaimId(preClaim.getExpertToClaimId());
            preClaimDtos.add(preClaimDto);
        }
        return preClaimDtos;
    }

    @Override
    @Transactional
    public Long DWHExpertToSTUser(Long expertId, String name, UserContext userContext) throws IOException {
        UserProfileDto userProfileDto = new UserProfileDto();
        userProfileDto.setName(name);
        userProfileDto.setClaimable(true);
        return DWHExpertToSTUser(expertId, userProfileDto, userContext);
    }

    @Override
    @Transactional
    public Long DWHExpertToSTUser(Long expertId, UserProfileDto userProfileDto, UserContext userContext) throws IOException {
        final Long userId = userProfileService.createProfile(userProfileDto, userContext);
        claimExpertProfile(expertId, userId, userContext);
        final ElasticsearchService elasticsearchService = ServiceRegistryUtil.getService(ElasticsearchService.class);
        assert elasticsearchService != null;
        userProfileDto.setUserId(userId);
        userProfileService.updateProfile(userProfileDto, userContext);
        userProfileService.loadAndIndexUserProfile(userId);
        elasticsearchService.flushSyncedAll();
        return userId;
    }

    private Map<Long, Long> createExpertIdUserIdMap(List<UserExpertEntity> userExpertEntities) {
        return userExpertEntities.stream()
                .filter(userExpertEntity -> userExpertEntity.getExpertId() != null)
                .collect(Collectors.toMap(
                        UserExpertEntity::getExpertId,
                        userExpertEntity -> userExpertEntity.getUserProfileEntity().getUserId()));
    }

    private Map<Long, Long> createUserIdUserIdMap(List<UserProfileEntity> userProfileEntities) {
        return userProfileEntities.stream()
                .filter(userProfileEntity -> userProfileEntity.getClaimedBy() != null)
                .collect(Collectors.toMap(
                        UserProfileEntity::getUserId,
                        userProfileEntity -> userProfileEntity.getClaimedBy().getUserId()));
    }
}
