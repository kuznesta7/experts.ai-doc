package ai.unico.platform.store.service;

import ai.unico.platform.store.api.service.PlatformVersionService;
import ai.unico.platform.store.entity.FlywaySchemaHistoryEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.util.GitProperties;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class PlatformVersionServiceImpl implements PlatformVersionService {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    @Transactional
    public String getDBVersion() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(FlywaySchemaHistoryEntity.class, "schemaHistory");
        criteria.setProjection(Projections.max("version").as("version"));
        return (String) criteria.uniqueResult();
    }

    @Override
    public String lastBuildDate() {
        return GitProperties.getValue("git.commit.time");
    }
}
