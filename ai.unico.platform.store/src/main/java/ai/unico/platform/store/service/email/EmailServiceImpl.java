package ai.unico.platform.store.service.email;

import ai.unico.platform.store.api.service.email.EmailService;
import ai.unico.platform.store.service.template.TemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EmailServiceImpl implements EmailService {

    private static final Logger LOG = LoggerFactory.getLogger(EmailServiceImpl.class);

    private Session session;

    private final ExecutorService SENDER_SERVICE = Executors.newFixedThreadPool(2);

    @Autowired
    private TemplateService templateService;

    @Value("${email.from}")
    private String emailFrom;

    @Value("${email.smtp.host}")
    private String emailSmtpHost;

    @Value("${email.smtp.port}")
    private String emailSmtpPort;

    @Value("${email.smtp.password}")
    private String emailSmtpPassword;

    @Value("${email.smtp.username}")
    private String emailSmtpUsername;

    public void init() {
        final Properties props = System.getProperties();
        props.setProperty("mail.smtp.host", emailSmtpHost);
        if (emailSmtpHost == null || emailSmtpHost.isEmpty()) return;

        props.setProperty("mail.smtp.port", emailSmtpPort);
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");

        if (emailSmtpUsername != null && !emailSmtpUsername.isEmpty()) {
            props.setProperty("mail.smtp.auth", "true");
            props.setProperty("mail.smtp.starttls.enable", "true");
            session = Session.getDefaultInstance(props, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(emailSmtpUsername, emailSmtpPassword);
                }
            });
        } else {
            session = Session.getDefaultInstance(props);
        }
    }

    @Override
    public void sendEmail(String to, String subject, String body, String replyTo) {
        if (session == null) {
            System.out.println("SENDING EMAIL to = [" + to + "], subject = [" + subject + "], body = [" + body + "]");
        } else {
            SENDER_SERVICE.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        final MimeMessage message = new MimeMessage(session);
                        message.setFrom(new InternetAddress(emailFrom));
                        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
                        message.setSubject(subject);
                        message.setContent(body, "text/html; charset=utf-8");
                        if (replyTo != null) {
                            message.setReplyTo(new InternetAddress[]{new InternetAddress(replyTo), new InternetAddress(emailFrom)});
                        }
                        Transport.send(message);
                    } catch (MessagingException e) {
                        LOG.error("err", e);
                    }
                }
            });
        }
    }

    @Override
    protected void finalize() throws Throwable {
        SENDER_SERVICE.shutdown();
        super.finalize();
    }

    @Override
    public void sendEmailWithTemplate(String to, String templateId, Map<String, String> variables) {
        final String id = "email/" + templateId;
        final String emailBody = templateService.getReplacedTemplate(id + ".body.html", variables);
        final String emailSubject = templateService.getReplacedTemplate(id + ".subject.html", variables);

        // If user applied for an opportunity, email's reply button will be mapped to unico & user's email
        final String replyTo = Objects.equals(templateId, "external-apply-request-by-unico") ? variables.get("userEmail") : null;

        System.out.println(variables);
        System.out.println(emailSubject);
        System.out.println(emailBody);
        sendEmail(to, emailSubject, emailBody, replyTo);
    }
}
