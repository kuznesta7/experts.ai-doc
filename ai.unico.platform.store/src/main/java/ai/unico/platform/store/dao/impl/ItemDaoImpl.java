package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ItemDao;
import ai.unico.platform.store.entity.ItemEntity;
import ai.unico.platform.store.entity.ItemTagEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.util.CollectionsUtil;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ItemDaoImpl implements ItemDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public List<ItemEntity> findItemsByOriginalIds(Set<Long> originalIds) {
        if (originalIds.isEmpty()) return Collections.emptyList();
        final List<List<Long>> lists = CollectionsUtil.toSubCollectionForHibernate(originalIds);

        final List<ItemEntity> result = new ArrayList<>();
        for (List<Long> list : lists) {
            final Criteria criteria = hibernateCriteriaCreator.createCriteria(ItemEntity.class, "item");
            criteria.add(Restrictions.in("origItemId", list));
            final List<ItemEntity> subList = criteria.list();
            result.addAll(subList);
        }
        return result;
    }

    @Override
    public void saveItem(ItemEntity itemEntity) {
        entityManager.persist(itemEntity);
    }

    @Override
    public void saveItemTag(ItemTagEntity itemTagEntity) {
        entityManager.persist(itemTagEntity);
    }

    @Override
    public List<ItemEntity> findItems(Integer limit, Integer offset) {
        final Criteria idCriteria = hibernateCriteriaCreator.createCriteria(ItemEntity.class, "item");
        idCriteria.add(Restrictions.eq("item.outdated", false));
        idCriteria.setFirstResult(offset);
        idCriteria.setMaxResults(limit);
        idCriteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        final List<ItemEntity> list = idCriteria.list();
        final Set<Long> itemIds = list.stream().map(ItemEntity::getItemId).collect(Collectors.toSet());
        if (itemIds.isEmpty()) return Collections.emptyList();

        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ItemEntity.class, "item");
        criteria.add(Restrictions.in("itemId", itemIds));
        setFetch(criteria);
        return criteria.list();
    }

    @Override
    public ItemEntity find(Long itemId) {
        return entityManager.find(ItemEntity.class, itemId);
    }

    @Override
    public List<ItemEntity> find(Set<Long> itemIds) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ItemEntity.class, "item");
        criteria.add(Restrictions.in("itemId", itemIds));
        setFetch(criteria);
        return criteria.list();
    }

    private void setFetch(Criteria criteria) {
        criteria.createAlias("item.itemOrganizationEntities", "itemOrganization", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("itemOrganization.organizationEntity", "organization", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("item.userItemEntities", "userItem", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("userItem.userProfileEntity", "userProfile", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("userProfile.claimedBy", "claimedBy", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("userProfile.userOrganizationEntities", "userOrganization", JoinType.LEFT_OUTER_JOIN);
        criteria.setFetchMode("userItem", FetchMode.JOIN);
        criteria.setFetchMode("userProfile", FetchMode.JOIN);
        criteria.setFetchMode("userOrganization", FetchMode.JOIN);
        criteria.setFetchMode("organization", FetchMode.JOIN);
        criteria.setFetchMode("rootOrganization", FetchMode.JOIN);
        criteria.setFetchMode("claimedBy", FetchMode.JOIN);
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
    }
}
