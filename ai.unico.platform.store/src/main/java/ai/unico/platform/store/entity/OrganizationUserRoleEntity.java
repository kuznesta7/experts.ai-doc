package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;
import ai.unico.platform.util.enums.OrganizationRole;

import javax.persistence.*;

@Entity
public class OrganizationUserRoleEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long organizationUserRoleId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserProfileEntity userProfileEntity;

    @ManyToOne
    @JoinColumn(name = "organization_id")
    private OrganizationEntity organizationEntity;

    private boolean deleted = false;

    @Enumerated(EnumType.STRING)
    private OrganizationRole role;

    public UserProfileEntity getUserProfileEntity() {
        return userProfileEntity;
    }

    public void setUserProfileEntity(UserProfileEntity userProfileEntity) {
        this.userProfileEntity = userProfileEntity;
    }

    public OrganizationEntity getOrganizationEntity() {
        return organizationEntity;
    }

    public void setOrganizationEntity(OrganizationEntity organizationEntity) {
        this.organizationEntity = organizationEntity;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public OrganizationRole getRole() {
        return role;
    }

    public void setRole(OrganizationRole role) {
        this.role = role;
    }

    public Long getOrganizationUserRoleId() {
        return organizationUserRoleId;
    }

    public void setOrganizationUserRoleId(Long organizationUserRoleId) {
        this.organizationUserRoleId = organizationUserRoleId;
    }
}
