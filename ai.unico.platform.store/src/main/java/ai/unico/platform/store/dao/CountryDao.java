package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.CountryEntity;
import ai.unico.platform.store.entity.UserCountryOfInterestEntity;

import java.util.List;

public interface CountryDao {

    List<UserCountryOfInterestEntity> findCountriesOfInterestForUser(Long userId);

    void save(UserCountryOfInterestEntity userCountryOfInterestEntity);

    CountryEntity findCountry(String countryCode);

    List<CountryEntity> findAllCountries();
}
