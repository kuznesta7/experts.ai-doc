package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
@Table(name = "subscription_organization")
public class SubscriptionOrganizationEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "deleted")
    private boolean deleted;

    @Column(name = "user_token")
    private String userToken;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organization_id", nullable = false)
    private OrganizationEntity organizationEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "widget_email_subscription_id")
    private WidgetEmailSubEntity widgetEmailSubEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "subscription_type_id")
    private SubscriptionTypeEntity subscriptionTypeEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public OrganizationEntity getOrganizationEntity() {
        return organizationEntity;
    }

    public void setOrganizationEntity(OrganizationEntity organizationEntity) {
        this.organizationEntity = organizationEntity;
    }

    public WidgetEmailSubEntity getWidgetEmailSubEntity() {
        return widgetEmailSubEntity;
    }

    public void setWidgetEmailSubEntity(WidgetEmailSubEntity widgetEmailSubEntity) {
        this.widgetEmailSubEntity = widgetEmailSubEntity;
    }

    public SubscriptionTypeEntity getSubscriptionTypeEntity() {
        return subscriptionTypeEntity;
    }

    public void setSubscriptionTypeEntity(SubscriptionTypeEntity subscriptionTypeEntity) {
        this.subscriptionTypeEntity = subscriptionTypeEntity;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }
}
