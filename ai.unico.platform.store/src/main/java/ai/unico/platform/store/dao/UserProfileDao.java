package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.UserProfileEntity;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface UserProfileDao {

    UserProfileEntity findUserProfile(Long userId);

    List<UserProfileEntity> findClaimedUserProfiles(Set<Long> userIds);

    List<UserProfileEntity> findClaimedUserProfiles();

    UserProfileEntity createUserProfile(UserProfileEntity userProfileEntity);

    List<UserProfileEntity> findUserProfiles(Integer limit, Integer offset);

    List<UserProfileEntity> findClaimableVerifiedProfiles();

    UserProfileEntity findUserProfileWithPreloadedClaims(Long userId);

    List<UserProfileEntity> findByIds(Collection<Long> userIds);

    void save(UserProfileEntity userProfileEntity);

}
