package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.CountryDao;
import ai.unico.platform.store.entity.CountryEntity;
import ai.unico.platform.store.entity.UserCountryOfInterestEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class CountryDaoImpl implements CountryDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<UserCountryOfInterestEntity> findCountriesOfInterestForUser(Long userId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserCountryOfInterestEntity.class, "userCountry");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("userProfileEntity.userId", userId));
        return criteria.list();
    }

    @Override
    public void save(UserCountryOfInterestEntity userCountryOfInterestEntity) {
        entityManager.persist(userCountryOfInterestEntity);
    }

    @Override
    public CountryEntity findCountry(String countryCode) {
        return entityManager.find(CountryEntity.class, countryCode);
    }

    @Override
    public List<CountryEntity> findAllCountries() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(CountryEntity.class, "country");
        return criteria.list();
    }
}
