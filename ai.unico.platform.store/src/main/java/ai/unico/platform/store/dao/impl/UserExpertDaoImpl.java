package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.UserExpertDao;
import ai.unico.platform.store.entity.UserExpertEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.store.util.CollectionsUtil;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class UserExpertDaoImpl implements UserExpertDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public List<UserExpertEntity> findForExperts(Set<Long> expertIds) {
        if (expertIds.isEmpty()) return Collections.emptyList();
        final List<List<Long>> idsList = CollectionsUtil.toSubCollectionForHibernate(expertIds);
        final List<UserExpertEntity> result = new ArrayList<>();
        for (List<Long> expertIdSublist : idsList) {
            final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserExpertEntity.class, "userExpert");
            criteria.createAlias("userExpert.userProfileEntity", "userProfile");
            criteria.add(Restrictions.in("expertId", expertIdSublist));
            criteria.add(Restrictions.eq("userExpert.deleted", false));
            result.addAll(criteria.list());
        }
        return result;
    }

    @Override
    public List<UserExpertEntity> findForUser(Long userId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserExpertEntity.class, "userExpert");
        criteria.add(Restrictions.eq("userId", userId));
        criteria.add(Restrictions.eq("deleted", false));
        return (List<UserExpertEntity>) criteria.list();
    }

    @Override
    public void save(UserExpertEntity userExpertEntity) {
        entityManager.persist(userExpertEntity);
    }

    @Override
    public List<UserExpertEntity> findAll() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserExpertEntity.class, "userExpert");
        criteria.add(Restrictions.eq("deleted", false));
        return criteria.list();
    }
}
