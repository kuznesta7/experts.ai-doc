package ai.unico.platform.store.util;

import ai.unico.platform.search.api.service.OrganizationIndexService;
import ai.unico.platform.store.api.dto.OrganizationIndexDto;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.util.ServiceRegistryUtil;

import java.io.IOException;

public class OrganizationIndexUtil {

    public static void indexOrganization(OrganizationEntity organizationEntity) {
        final OrganizationIndexService service = ServiceRegistryUtil.getService(OrganizationIndexService.class);
        final OrganizationIndexDto organizationIndexDto = OrganizationUtil.convertIndex(organizationEntity);
        try {
            service.indexOrganization(organizationIndexDto);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
