package ai.unico.platform.store.service;

import ai.unico.platform.store.api.service.SearchAllowanceService;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.util.OrganizationIndexUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

public class SearchAllowanceServiceImpl implements SearchAllowanceService {

    @Autowired
    private OrganizationDao organizationDao;

    @Override
    @Transactional
    public void updateSearchAllowance(Long organizationId, boolean allowed) {
        OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        if (organizationEntity != null) {
            organizationEntity.setAllowedSearch(allowed);
            OrganizationIndexUtil.indexOrganization(organizationEntity);
        }
    }

    @Override
    @Transactional
    public boolean isAllowed(Set<Long> organizationIds) {
        return organizationDao.findByIds(organizationIds).stream().anyMatch(OrganizationEntity::getAllowedSearch);
    }
}
