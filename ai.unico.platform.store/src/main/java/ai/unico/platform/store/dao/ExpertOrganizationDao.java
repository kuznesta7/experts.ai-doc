package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ExpertOrganizationEntity;

public interface ExpertOrganizationDao {

    void save(ExpertOrganizationEntity expertOrganizationEntity);

    ExpertOrganizationEntity find(Long expertId, Long organizationId);

    ExpertOrganizationEntity findNotDeleted(Long expertId, Long organizationId);

}
