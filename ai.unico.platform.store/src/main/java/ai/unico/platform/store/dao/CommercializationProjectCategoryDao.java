package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.CommercializationCategoryEntity;
import ai.unico.platform.store.entity.CommercializationProjectCategoryEntity;

public interface CommercializationProjectCategoryDao {

    void save(CommercializationProjectCategoryEntity commercializationProjectCategorytEntity);

    CommercializationCategoryEntity findCategoryById(Long categoryId);


}