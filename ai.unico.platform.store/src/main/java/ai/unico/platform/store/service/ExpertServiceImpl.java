package ai.unico.platform.store.service;

import ai.unico.platform.search.api.service.ExpertIndexService;
import ai.unico.platform.store.api.dto.ExpertIndexDto;
import ai.unico.platform.store.api.service.ExpertService;
import ai.unico.platform.store.dao.ExpertDao;
import ai.unico.platform.store.entity.ExpertEntity;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class ExpertServiceImpl implements ExpertService {

    @Autowired
    private ExpertDao expertDao;

    @Override
    @Transactional
    public List<ExpertIndexDto> findUpdatedExperts() {
        final List<ExpertEntity> allExperts = expertDao.findAll();
        return allExperts.stream().map(this::convert).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void loadAndUpdateExpert(Long expertId) throws IOException {
        final ExpertEntity expertEntity = expertDao.find(expertId);
        if (expertEntity == null) throw new NonexistentEntityException(ExpertEntity.class);
        final ExpertIndexDto expertIndexDto = convert(expertEntity);

        final ExpertIndexService expertIndexService = ServiceRegistryUtil.getService(ExpertIndexService.class);
        expertIndexService.updateExpert(expertIndexDto);
    }

    private ExpertIndexDto convert(ExpertEntity expertEntity) {
        final ExpertIndexDto expertIndexDto = new ExpertIndexDto();
        expertIndexDto.setExpertId(expertEntity.getExpertId());
        expertIndexDto.setRetirementDate(expertEntity.getRetirementDate());
        return expertIndexDto;
    }
}
