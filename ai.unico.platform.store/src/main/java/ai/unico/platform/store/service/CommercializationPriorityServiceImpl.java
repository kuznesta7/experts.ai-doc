package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.CommercializationPriorityTypeDto;
import ai.unico.platform.store.api.service.CommercializationPriorityService;
import ai.unico.platform.store.dao.CommercializationPriorityDao;
import ai.unico.platform.store.entity.CommercializationPriorityEntity;
import ai.unico.platform.store.util.ReferenceDataUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

public class CommercializationPriorityServiceImpl implements CommercializationPriorityService {

    @Autowired
    private CommercializationPriorityDao commercializationPriorityDao;

    @Override
    @Transactional
    public List<CommercializationPriorityTypeDto> findAvailableCommercializationPriorityTypes(boolean includeInactive) {
        final List<CommercializationPriorityEntity> entities = commercializationPriorityDao.findAll();
        if (includeInactive) return entities.stream().map(this::convert).collect(Collectors.toList());
        return ReferenceDataUtil.filterAndConvertReferenceData(entities, this::convert);
    }

    @Override
    @Transactional
    public Long createPriority(CommercializationPriorityTypeDto commercializationPriorityTypeDto, UserContext userContext) {
        final CommercializationPriorityEntity commercializationPriorityEntity = new CommercializationPriorityEntity();
        commercializationPriorityEntity.setCode(commercializationPriorityTypeDto.getPriorityTypeCode());
        fillEntity(commercializationPriorityEntity, commercializationPriorityTypeDto);
        TrackableUtil.fillCreate(commercializationPriorityEntity, userContext);
        commercializationPriorityDao.save(commercializationPriorityEntity);
        return commercializationPriorityEntity.getCommercializationPriorityId();
    }

    @Override
    @Transactional
    public void updatePriority(Long priorityId, CommercializationPriorityTypeDto commercializationPriorityTypeDto, UserContext userContext) {
        final CommercializationPriorityEntity commercializationPriorityEntity = commercializationPriorityDao.findPriority(priorityId);
        if (commercializationPriorityEntity == null)
            throw new NonexistentEntityException(CommercializationPriorityEntity.class);
        fillEntity(commercializationPriorityEntity, commercializationPriorityTypeDto);
        TrackableUtil.fillUpdate(commercializationPriorityEntity, userContext);
    }

    private CommercializationPriorityTypeDto convert(CommercializationPriorityEntity entity) {
        final CommercializationPriorityTypeDto dto = new CommercializationPriorityTypeDto();
        dto.setPriorityTypeId(entity.getCommercializationPriorityId());
        dto.setPriorityTypeName(entity.getName());
        dto.setPriorityTypeCode(entity.getCode());
        dto.setPriorityTypeDescription(entity.getDescription());
        dto.setDeleted(entity.isDeleted());
        return dto;
    }

    private void fillEntity(CommercializationPriorityEntity entity, CommercializationPriorityTypeDto dto) {
        entity.setName(dto.getPriorityTypeName());
        entity.setDescription(dto.getPriorityTypeDescription());
        entity.setDeleted(dto.isDeleted());
    }
}
