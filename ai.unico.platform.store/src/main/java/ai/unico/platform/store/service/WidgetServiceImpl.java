package ai.unico.platform.store.service;

import ai.unico.platform.store.api.enums.WidgetType;
import ai.unico.platform.store.api.service.WidgetService;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.dao.WidgetDao;
import ai.unico.platform.store.dao.WidgetEmailSubDao;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.entity.OrganizationWidgetEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class WidgetServiceImpl implements WidgetService {

    @Autowired
    private WidgetDao widgetDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private WidgetEmailSubDao widgetEmailSubDao;

    @Override
    @Transactional
    public void updateWidgets(Long organizationId, Map<WidgetType, Boolean> widgetChanges, UserContext userContext) {
        final List<OrganizationWidgetEntity> forOrganization = widgetDao.findForOrganization(organizationId);
        final Map<WidgetType, List<OrganizationWidgetEntity>> widgetTypeListMap = forOrganization.stream().collect(Collectors.groupingBy(OrganizationWidgetEntity::getWidgetType));
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        for (Map.Entry<WidgetType, Boolean> entry : widgetChanges.entrySet()) {
            final WidgetType widgetType = entry.getKey();
            if (entry.getValue()) {
                if (widgetTypeListMap.containsKey(widgetType)) continue;
                final OrganizationWidgetEntity organizationWidgetEntity = new OrganizationWidgetEntity();
                organizationWidgetEntity.setOrganizationEntity(organizationEntity);
                organizationWidgetEntity.setAllowed(true);
                organizationWidgetEntity.setWidgetType(widgetType);
                TrackableUtil.fillCreate(organizationWidgetEntity, userContext);
                widgetDao.save(organizationWidgetEntity);
            } else {
                if (!widgetTypeListMap.containsKey(widgetType)) continue;
                for (OrganizationWidgetEntity organizationWidgetEntity : widgetTypeListMap.get(widgetType)) {
                    organizationWidgetEntity.setAllowed(false);
                    TrackableUtil.fillUpdate(organizationWidgetEntity, userContext);
                }
            }
        }
    }

    @Override
    @Transactional
    public boolean isAllowed(Long organizationId, WidgetType widgetType) {
        return widgetDao.isAllowed(organizationId, widgetType);
    }

    @Override
    @Transactional
    public Map<WidgetType, Boolean> allowed(Long organizationId) {
        Map<WidgetType, Boolean> allowedWidgets = new HashMap<>();
        Arrays.stream(WidgetType.values()).forEach(x -> {
            allowedWidgets.put(x, Boolean.FALSE);
        });
        widgetDao.findForOrganization(organizationId).forEach(x -> {
            allowedWidgets.put(x.getWidgetType(), Boolean.TRUE);
        });
        return allowedWidgets;
    }

}
