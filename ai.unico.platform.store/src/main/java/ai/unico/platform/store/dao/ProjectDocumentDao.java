package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ProjectDocumentEntity;

import java.util.List;

public interface ProjectDocumentDao {

    void save(ProjectDocumentEntity projectDocumentEntity);

    List<ProjectDocumentEntity> findForProject(Long projectId);

    ProjectDocumentEntity findDocument(Long projectId, Long fileId);

}
