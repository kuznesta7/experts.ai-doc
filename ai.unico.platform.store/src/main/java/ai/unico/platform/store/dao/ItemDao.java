package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ItemEntity;
import ai.unico.platform.store.entity.ItemTagEntity;

import java.util.List;
import java.util.Set;

public interface ItemDao {

    List<ItemEntity> findItemsByOriginalIds(Set<Long> originalIds);

    void saveItem(ItemEntity itemEntity);

    void saveItemTag(ItemTagEntity itemTagEntity);

    List<ItemEntity> findItems(Integer limit, Integer offset);

    ItemEntity find(Long itemId);

    List<ItemEntity> find(Set<Long> itemIds);
}
