package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.UserConsentDao;
import ai.unico.platform.store.entity.UserConsentEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class UserConsentDaoImpl implements UserConsentDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void save(UserConsentEntity userConsentEntity) {
        entityManager.persist(userConsentEntity);
    }

    @Override
    public UserConsentEntity findUserConsentEntity(Long userId, Long consentId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserConsentEntity.class, "userConsent");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("userProfileEntity.userId", userId));
        criteria.add(Restrictions.eq("consentEntity.consentId", consentId));

        return (UserConsentEntity) criteria.uniqueResult();
    }

    @Override
    public List<UserConsentEntity> findUserConsentEntities(Long userId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserConsentEntity.class, "userConsent");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("userProfileEntity.userId", userId));
        return criteria.list();
    }
}
