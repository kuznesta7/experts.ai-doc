package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
@Table(name = "profile_visitation")
public class ProfileVisitationEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long visitationId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserProfileEntity userProfileEntity;

    @ManyToOne
    @JoinColumn(name = "search_hist_id")
    private SearchHistEntity searchHistEntity;

    private Long expertId;

    public Long getVisitationId() {
        return visitationId;
    }

    public void setVisitationId(Long visitationId) {
        this.visitationId = visitationId;
    }

    public UserProfileEntity getUserProfileEntity() {
        return userProfileEntity;
    }

    public void setUserProfileEntity(UserProfileEntity userProfileEntity) {
        this.userProfileEntity = userProfileEntity;
    }

    public SearchHistEntity getSearchHistEntity() {
        return searchHistEntity;
    }

    public void setSearchHistEntity(SearchHistEntity searchHistEntity) {
        this.searchHistEntity = searchHistEntity;
    }

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }
}
