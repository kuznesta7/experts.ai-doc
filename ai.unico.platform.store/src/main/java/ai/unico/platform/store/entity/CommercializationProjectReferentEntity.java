package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class CommercializationProjectReferentEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commercializationProjectReferentId;

    @ManyToOne
    private UserProfileEntity userProfileEntity;

    @ManyToOne
    private CommercializationProjectEntity commercializationProjectEntity;

    private boolean deleted;

    public Long getCommercializationProjectReferentId() {
        return commercializationProjectReferentId;
    }

    public void setCommercializationProjectReferentId(Long commercializationProjectReferentId) {
        this.commercializationProjectReferentId = commercializationProjectReferentId;
    }

    public UserProfileEntity getUserProfileEntity() {
        return userProfileEntity;
    }

    public void setUserProfileEntity(UserProfileEntity userProfileEntity) {
        this.userProfileEntity = userProfileEntity;
    }

    public CommercializationProjectEntity getCommercializationProjectEntity() {
        return commercializationProjectEntity;
    }

    public void setCommercializationProjectEntity(CommercializationProjectEntity commercializationProjectEntity) {
        this.commercializationProjectEntity = commercializationProjectEntity;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
