package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.ReferenceData;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CommercializationPriorityEntity extends ReferenceData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commercializationPriorityId;

    public Long getCommercializationPriorityId() {
        return commercializationPriorityId;
    }

    public void setCommercializationPriorityId(Long commercializationPriorityId) {
        this.commercializationPriorityId = commercializationPriorityId;
    }
}
