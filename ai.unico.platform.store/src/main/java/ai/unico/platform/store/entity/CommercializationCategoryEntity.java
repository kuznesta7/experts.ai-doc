package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.ReferenceData;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CommercializationCategoryEntity extends ReferenceData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commercializationCategoryId;

    public Long getCommercializationCategoryId() {
        return commercializationCategoryId;
    }

    public void setCommercializationCategoryId(Long commercializationCategoryId) {
        this.commercializationCategoryId = commercializationCategoryId;
    }
}
