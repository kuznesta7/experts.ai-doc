package ai.unico.platform.store.dao;

import ai.unico.platform.store.api.filter.FollowedProfilesFilter;
import ai.unico.platform.store.entity.FollowedProfileEntity;

import java.util.List;

public interface FollowProfileDao {

    FollowedProfileEntity findUserFollow(Long followingUserId, Long followedUserId);

    FollowedProfileEntity findExpertFollow(Long followingUserId, Long followedExpertId);

    void save(FollowedProfileEntity followedProfileEntity);

    List<FollowedProfileEntity> findFollowedProfilesForUser(Long userId, FollowedProfilesFilter followedProfilesFilter);

    List<FollowedProfileEntity> findFollowersForExpert(Long expertId);

    Long countFollowedProfilesForUser(Long userId, FollowedProfilesFilter followedProfilesFilter);
}
