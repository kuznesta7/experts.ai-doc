package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.EquipmentDao;
import ai.unico.platform.store.entity.EquipmentEntity;
import ai.unico.platform.store.entity.EquipmentId;
import ai.unico.platform.store.entity.EquipmentTagEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class EquipmentDaoImpl implements EquipmentDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void save(EquipmentEntity equipment) {
        entityManager.persist(equipment);
    }

    @Override
    public void saveTag(EquipmentTagEntity equipmentTag) {
        entityManager.persist(equipmentTag);
    }

    @Override
    public EquipmentEntity find(Long equipmentId, String languageCode) {
        EquipmentId id = new EquipmentId(equipmentId, languageCode);
        return entityManager.find(EquipmentEntity.class, id);
    }

    @Override
    public List<EquipmentEntity> find(Long equipmentId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(EquipmentEntity.class, "equipment");
        criteria.add(Restrictions.eq("equipmentId", equipmentId));
        return criteria.list();
    }

    @Override
    public List<EquipmentEntity> findAll(Set<Long> ids) {
        if (ids.isEmpty()) return Collections.emptyList();
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(EquipmentEntity.class, "equipment");
        criteria.add(Restrictions.in("equipmentId", ids));
        return criteria.list();
    }

    @Override
    public List<EquipmentEntity> findAll(Integer limit, Integer offset) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(EquipmentEntity.class, "equipment");
        criteria.addOrder(org.hibernate.criterion.Order.asc("equipmentId"));
        criteria.setFirstResult(offset);
        criteria.setMaxResults(limit);
        return criteria.list();
    }

    @Override
    public List<EquipmentEntity> findByOriginalIds(List<String> originalIds) {
        if (originalIds.isEmpty()) return Collections.emptyList();
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(EquipmentEntity.class, "equipment");
        criteria.add(Restrictions.in("originalId", originalIds));
        return criteria.list();
    }

    @Override
    public List<Long> getAllOriginalIds() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(EquipmentEntity.class, "equipment");
        criteria.setProjection(Projections.distinct(Projections.property("originalId")));
        return criteria.list();
    }
}
