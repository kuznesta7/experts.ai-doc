package ai.unico.platform.store.service;

import ai.unico.platform.search.api.dto.UserExpertProfileSearchDetailDto;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.service.ElasticsearchService;
import ai.unico.platform.search.api.service.ExpertSearchService;
import ai.unico.platform.store.api.dto.UserProfileDto;
import ai.unico.platform.store.api.service.ClaimProfileService;
import ai.unico.platform.store.api.service.ExpertToUserService;
import ai.unico.platform.store.api.service.UserProfileService;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class ExpertToUserServiceImpl implements ExpertToUserService {

    @Autowired
    private UserProfileService userProfileService;

    @Autowired
    private ClaimProfileService claimProfileService;

    @Override
    @Transactional
    public Long expertToUser(Long expertId, UserContext userContext) throws IOException {
        return expertToUser(IdUtil.convertToExtDataCode(expertId), userContext);
    }

    @Override
    @Transactional
    public Long expertToUser(String expertCode, UserContext userContext) throws IOException {
        final ExpertSearchService service = ServiceRegistryUtil.getService(ExpertSearchService.class);
        assert service != null;
        final ExpertSearchResultFilter filter = new ExpertSearchResultFilter();
        final UserExpertProfileSearchDetailDto expertProfileSearch = service.findSearchByExpertiseDetail(expertCode, filter);
        UserProfileDto userProfileDto = new UserProfileDto();
        userProfileDto.setName(expertProfileSearch.getName());
        userProfileDto.setClaimable(true);
        final Long userId = userProfileService.createProfile(userProfileDto, userContext);
        claimProfileService.claimExpertProfile(IdUtil.getExtDataId(expertCode), userId, userContext);
        final ElasticsearchService elasticsearchService = ServiceRegistryUtil.getService(ElasticsearchService.class);
        assert elasticsearchService != null;
        userProfileDto.setUserId(userId);
        elasticsearchService.flushSyncedAll();
        return userId;
    }

    @Override
    @Transactional
    public Set<Long> expertsToUser(Set<String> expertCodes, UserContext userContext) throws IOException {
        Set<Long> userIds = new HashSet<>();
        for (String expertCode: expertCodes) {
            userIds.add(expertToUser(expertCode, userContext));
        }
        return userIds;
    }
}
