package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.UserKeywordDao;
import ai.unico.platform.store.entity.UserProfileKeywordEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class UserKeywordDaoImpl implements UserKeywordDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(UserProfileKeywordEntity userProfileKeywordEntity) {
        entityManager.persist(userProfileKeywordEntity);
    }
}
