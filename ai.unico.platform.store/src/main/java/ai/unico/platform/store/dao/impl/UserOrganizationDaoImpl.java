package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.UserOrganizationDao;
import ai.unico.platform.store.entity.UserOrganizationEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class UserOrganizationDaoImpl implements UserOrganizationDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void saveUserOrganization(UserOrganizationEntity userOrganizationEntity) {
        entityManager.persist(userOrganizationEntity);
    }

    @Override
    public UserOrganizationEntity find(Long organizationId, Long userId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserOrganizationEntity.class, "userOrganization");
        criteria.add(Restrictions.eq("userProfileEntity.userId", userId));
        criteria.add(Restrictions.eq("organizationEntity.organizationId", organizationId));
        criteria.setMaxResults(1);
        return (UserOrganizationEntity) criteria.uniqueResult();
    }

    @Override
    public UserOrganizationEntity findNotDeleted(Long organizationId, Long userId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserOrganizationEntity.class, "userOrganization");
        criteria.add(Restrictions.eq("userProfileEntity.userId", userId));
        criteria.add(Restrictions.eq("organizationEntity.organizationId", organizationId));
        criteria.add(Restrictions.eq("deleted", false));
        return (UserOrganizationEntity) criteria.uniqueResult();
    }
}
