package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;
import ai.unico.platform.util.enums.OrganizationRole;

import javax.persistence.*;
import java.util.Date;

@Entity
public class OrganizationLicenceEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long organizationLicenceId;

    private Date validUntil;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organization_id")
    private OrganizationEntity organizationEntity;

    @Enumerated(EnumType.STRING)
    private OrganizationRole licenceRole;

    private Integer numberOfUserLicences;

    private boolean deleted;

    public Long getOrganizationLicenceId() {
        return organizationLicenceId;
    }

    public void setOrganizationLicenceId(Long organizationLicenceId) {
        this.organizationLicenceId = organizationLicenceId;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    public OrganizationRole getLicenceRole() {
        return licenceRole;
    }

    public void setLicenceRole(OrganizationRole licenceRole) {
        this.licenceRole = licenceRole;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public OrganizationEntity getOrganizationEntity() {
        return organizationEntity;
    }

    public void setOrganizationEntity(OrganizationEntity organizationEntity) {
        this.organizationEntity = organizationEntity;
    }

    public Integer getNumberOfUserLicences() {
        return numberOfUserLicences;
    }

    public void setNumberOfUserLicences(Integer numberOfUserLicences) {
        this.numberOfUserLicences = numberOfUserLicences;
    }
}
