package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.SearchStatisticsDto;
import ai.unico.platform.store.api.filter.SearchStatisticsFilter;
import ai.unico.platform.store.api.service.SearchStatisticsService;
import ai.unico.platform.store.api.wrapper.SearchStatisticsWrapper;
import ai.unico.platform.store.dao.SearchStatisticsDao;
import ai.unico.platform.store.entity.SearchStatisticsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public class SearchStatisticsServiceImpl implements SearchStatisticsService {

    @Autowired
    SearchStatisticsDao searchStatisticsDao;

    @Override
    @Transactional
    public SearchStatisticsWrapper findSearchedData(SearchStatisticsFilter filter) {
        List<SearchStatisticsEntity> data = searchStatisticsDao.findSearchedData(filter);
        SearchStatisticsWrapper statisticsWrapper = new SearchStatisticsWrapper();
        for (SearchStatisticsEntity statisticsEntity :
                data) {
            statisticsWrapper.getSearchStatisticsDtos().add(convert(statisticsEntity));
        }
        statisticsWrapper.setNumberOfResults(searchStatisticsDao.numberOfResults(filter));
        statisticsWrapper.setLimit(filter.getLimit());
        statisticsWrapper.setPage(filter.getPage());
        return statisticsWrapper;
    }

    @Override
    @Transactional
    public List<SearchStatisticsDto> findHotTopics() {
        List<SearchStatisticsEntity> statisticsEntities = searchStatisticsDao.findHotTopics();
        List<SearchStatisticsDto> statisticsDtos = new ArrayList<>();
        for (SearchStatisticsEntity statisticsEntity :
                statisticsEntities) {
            statisticsDtos.add(convert(statisticsEntity));
        }
        return statisticsDtos;
    }

    @Override
    @Transactional
    public void updateSearchedData(SearchStatisticsDto statisticsDto) {
        searchStatisticsDao.updateSearchedData(statisticsDto);
    }

    @Override
    @Transactional
    public Long addSearchedData(SearchStatisticsDto statisticsDto) {
        return searchStatisticsDao.createSearchData(statisticsDto);
    }


    private SearchStatisticsDto convert(SearchStatisticsEntity statisticsEntity) {
        SearchStatisticsDto statisticsDto = new SearchStatisticsDto();
        statisticsDto.setStatisticId(statisticsEntity.getStatisticId());
        statisticsDto.setValue(statisticsEntity.getValue());
        statisticsDto.setSearches(statisticsEntity.getSearches());
        statisticsDto.setUniqueSearches(statisticsEntity.getUniqueSearches());
        statisticsDto.setHidden(statisticsEntity.isHidden());
        statisticsDto.setHotTopic(statisticsEntity.isHotTopic());
        return statisticsDto;
    }

}
