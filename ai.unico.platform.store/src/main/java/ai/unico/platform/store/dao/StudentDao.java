package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.StudentEntity;

import java.util.List;

public interface StudentDao {
    StudentEntity find(String studentId);
}
