package ai.unico.platform.store.entity;

import javax.persistence.*;

@Entity
public class OpportunityTagEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long opportunityTagId;

    @ManyToOne
    @JoinColumn(name = "opportunityId")
    private OpportunityEntity opportunity;

    @ManyToOne
    @JoinColumn(name = "tagId")
    private TagEntity tag;

    private Boolean deleted = false;

    public Long getOpportunityTagId() {
        return opportunityTagId;
    }

    public void setOpportunityTagId(Long opportunityTagId) {
        this.opportunityTagId = opportunityTagId;
    }

    public OpportunityEntity getOpportunity() {
        return opportunity;
    }

    public void setOpportunity(OpportunityEntity opportunity) {
        this.opportunity = opportunity;
    }

    public TagEntity getTag() {
        return tag;
    }

    public void setTag(TagEntity tag) {
        this.tag = tag;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
