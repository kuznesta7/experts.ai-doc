package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.ExpertSearchHistoryDto;
import ai.unico.platform.store.api.dto.ExpertSearchHistoryPreviewDto;
import ai.unico.platform.store.api.dto.UserWorkspaceDto;
import ai.unico.platform.store.api.filter.ExpertSearchHistoryFilter;
import ai.unico.platform.store.api.service.SearchHistoryService;
import ai.unico.platform.store.api.service.UserWorkspaceService;
import ai.unico.platform.store.api.wrapper.ExpertSearchHistoryWrapper;
import ai.unico.platform.store.dao.SearchHistoryDao;
import ai.unico.platform.store.dao.UserWorkspaceDao;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.store.util.SecurityUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SearchHistoryServiceImpl implements SearchHistoryService {

    @Autowired
    private SearchHistoryDao searchHistoryDao;

    @Autowired
    private UserWorkspaceDao userWorkspaceDao;

    @Autowired
    private UserWorkspaceService userWorkspaceService;

    @Override
    @Transactional
    public ExpertSearchHistoryPreviewDto findSavedSearch(ExpertSearchHistoryDto expertSearchHistoryDto, UserContext userContext) {
        if (userContext == null) return null;
        final SearchHistEntity savedSearch = searchHistoryDao.findSavedSearch(userContext.getUserId(), expertSearchHistoryDto);
        if (savedSearch == null) return null;
        return convert(savedSearch);
    }

    @Override
    @Transactional
    public Long saveSearchHistory(ExpertSearchHistoryDto expertSearchHistoryDto, UserContext userContext) {
        final SearchHistEntity searchHistEntity = new SearchHistEntity();
        TrackableUtil.fillCreate(searchHistEntity, userContext);
        if (expertSearchHistoryDto.getQuery() != null)
            searchHistEntity.setQuery(expertSearchHistoryDto.getQuery());
        searchHistoryDao.saveSearchHistory(searchHistEntity);
        fillFilter(searchHistEntity, expertSearchHistoryDto);
        return searchHistEntity.getSearchHistId();
    }

    @Override
    @Transactional
    public ExpertSearchHistoryPreviewDto findSearch(Long searchHistoryId, String shareCode, UserContext userContext) throws UserUnauthorizedException {
        final SearchHistEntity searchHistory = searchHistoryDao.findSearchHistory(searchHistoryId);
        if (!SecurityUtil.canUserSeeSearchHistory(searchHistory, shareCode, userContext)) {
            throw new UserUnauthorizedException();
        }
        final ExpertSearchHistoryPreviewDto expertSearchHistoryPreviewDto = new ExpertSearchHistoryPreviewDto();
        expertSearchHistoryPreviewDto.setQuery(searchHistory.getQuery());
        expertSearchHistoryPreviewDto.getOrganizationIds().addAll(searchHistory.getOrganizationIds());
        expertSearchHistoryPreviewDto.getResultTypeGroupIds().addAll(searchHistory.getResultTypeGroupIds());
        expertSearchHistoryPreviewDto.setYearFrom(searchHistory.getYearFrom());
        expertSearchHistoryPreviewDto.setYearTo(searchHistory.getYearTo());
        expertSearchHistoryPreviewDto.setSecondaryQueries(searchHistory.getSecondaryQueries().stream().map(SearchHistSecondaryQueryEntity::getQuery).collect(Collectors.toList()));
        expertSearchHistoryPreviewDto.setSecondaryWeights(searchHistory.getSecondaryQueries().stream().map(SearchHistSecondaryQueryEntity::getWeight).collect(Collectors.toList()));
        expertSearchHistoryPreviewDto.setOntologyDisabled(searchHistory.getOntologyDisabled());
        expertSearchHistoryPreviewDto.setResultsFound(searchHistory.getNumberOfResultsFound());
        expertSearchHistoryPreviewDto.setDate(searchHistory.getDateInsert());
        expertSearchHistoryPreviewDto.setFavorite(searchHistory.isFavorite());
        expertSearchHistoryPreviewDto.setSearchHistId(searchHistoryId);
        expertSearchHistoryPreviewDto.setWorkspaces(searchHistory.getSearchHistWorkspaceEntities().stream().map(SearchHistWorkspaceEntity::getUserWorkspaceEntity).map(UserWorkspaceEntity::getWorkspaceName).collect(Collectors.toSet()));
        expertSearchHistoryPreviewDto.getCountryCodes().addAll(searchHistory.getCountryCodes());
        expertSearchHistoryPreviewDto.setExpertSearchType(searchHistory.getSearchType());
        return expertSearchHistoryPreviewDto;
    }

    @Override
    @Transactional
    public ExpertSearchHistoryWrapper findSearchHistoryForUser(Long userId, ExpertSearchHistoryFilter expertSearchHistoryFilter) {
        final ExpertSearchHistoryWrapper expertSearchHistoryWrapper = new ExpertSearchHistoryWrapper();
        final List<SearchHistEntity> searchHistoryForUser = searchHistoryDao.findSearchHistoryForUser(userId, expertSearchHistoryFilter);
        expertSearchHistoryWrapper.setNumberOfResults(searchHistoryDao.countSearchHistoryForUser(userId, expertSearchHistoryFilter));
        expertSearchHistoryWrapper.setPage(expertSearchHistoryFilter.getPage());
        expertSearchHistoryWrapper.setLimit(expertSearchHistoryFilter.getLimit());
        expertSearchHistoryWrapper.setHistoryDtos(searchHistoryForUser.stream().map(this::convert).collect(Collectors.toList()));
        return expertSearchHistoryWrapper;
    }

    @Override
    @Transactional
    public void saveFavoriteSearch(Long searchHistId, boolean favorite, Set<String> workspaces, UserContext userContext) {
        final SearchHistEntity searchHistory = searchHistoryDao.findSearchHistory(searchHistId);
        if (searchHistory == null) throw new NonexistentEntityException(SearchHistEntity.class);
        final Long userId = searchHistory.getUserInsert();
        searchHistory.setFavorite(favorite);
        TrackableUtil.fillUpdate(searchHistory, userContext);
        final Set<SearchHistWorkspaceEntity> searchHistWorkspaceEntities = searchHistory.getSearchHistWorkspaceEntities();
        final HashSet<String> workspacesToAdd = new HashSet<>(workspaces);
        for (SearchHistWorkspaceEntity searchHistWorkspaceEntity : searchHistWorkspaceEntities) {
            if (searchHistWorkspaceEntity.isDeleted()) continue;
            final String workspaceName = searchHistWorkspaceEntity.getUserWorkspaceEntity().getWorkspaceName();
            if (!workspacesToAdd.contains(workspaceName)) {
                searchHistWorkspaceEntity.setDeleted(true);
                TrackableUtil.fillUpdate(searchHistWorkspaceEntity, userContext);
            }
            workspacesToAdd.remove(workspaceName);
        }
        for (String workspaceToAdd : workspacesToAdd) {
            final UserWorkspaceEntity userWorkspace = userWorkspaceDao.findUserWorkspace(userId, workspaceToAdd);
            final SearchHistWorkspaceEntity searchHistWorkspaceEntity = new SearchHistWorkspaceEntity();
            searchHistWorkspaceEntity.setSearchHistEntity(searchHistory);
            TrackableUtil.fillCreate(searchHistWorkspaceEntity, userContext);
            if (userWorkspace != null) {
                searchHistWorkspaceEntity.setUserWorkspaceEntity(userWorkspace);
            } else {
                final UserWorkspaceDto userWorkspaceDto = new UserWorkspaceDto();
                userWorkspaceDto.setWorkspaceName(workspaceToAdd);
                userWorkspaceDto.setUserId(userId);
                final Long workspaceId = userWorkspaceService.createUserWorkspace(userWorkspaceDto, userContext);
                searchHistWorkspaceEntity.setUserWorkspaceEntity(userWorkspaceDao.findUserWorkspace(workspaceId));
            }
            userWorkspaceDao.save(searchHistWorkspaceEntity);
        }
    }

    @Override
    @Transactional
    public void deleteSearchHistory(Long searchHistId, UserContext userContext) {
        final SearchHistEntity searchHistory = searchHistoryDao.findSearchHistory(searchHistId);
        searchHistory.setDeleted(true);
        TrackableUtil.fillUpdate(searchHistory, userContext);
    }

    private ExpertSearchHistoryPreviewDto convert(SearchHistEntity searchHistEntity) {
        final ExpertSearchHistoryPreviewDto expertSearchHistoryDto = new ExpertSearchHistoryPreviewDto();
        final Set<String> workspaces = searchHistEntity.getSearchHistWorkspaceEntities().stream()
                .filter(x -> !x.isDeleted())
                .map(SearchHistWorkspaceEntity::getUserWorkspaceEntity)
                .filter(userWorkspaceEntity -> !userWorkspaceEntity.isDeleted())
                .map(UserWorkspaceEntity::getWorkspaceName)
                .collect(Collectors.toSet());
        expertSearchHistoryDto.setSearchHistId(searchHistEntity.getSearchHistId());
        expertSearchHistoryDto.setQuery(searchHistEntity.getQuery());
        expertSearchHistoryDto.setExpertSearchType(searchHistEntity.getSearchType());
        expertSearchHistoryDto.getResultTypeGroupIds().addAll(searchHistEntity.getResultTypeGroupIds());
        expertSearchHistoryDto.getOrganizationIds().addAll(searchHistEntity.getOrganizationIds());
        expertSearchHistoryDto.setOntologyDisabled(searchHistEntity.getOntologyDisabled());
        expertSearchHistoryDto.setYearFrom(searchHistEntity.getYearFrom());
        expertSearchHistoryDto.setYearTo(searchHistEntity.getYearTo());
        expertSearchHistoryDto.setResultsFound(searchHistEntity.getNumberOfResultsFound());
        expertSearchHistoryDto.setDate(searchHistEntity.getDateInsert());
        expertSearchHistoryDto.setFavorite(searchHistEntity.isFavorite());
        expertSearchHistoryDto.setWorkspaces(workspaces);
        expertSearchHistoryDto.getCountryCodes().addAll(searchHistEntity.getCountryCodes());
        if (searchHistEntity.getDisabledSecondaryQueries().size() > 0) {
            for (SearchHistSecondaryQueryEntity secondaryQuery : searchHistEntity.getSecondaryQueries()) {
                expertSearchHistoryDto.getSecondaryQueries().add(secondaryQuery.getQuery());
                expertSearchHistoryDto.getSecondaryWeights().add(secondaryQuery.getWeight());
            }
        } else {
            expertSearchHistoryDto.setSecondaryQueries(null);
            expertSearchHistoryDto.setSecondaryWeights(null);
        }
        return expertSearchHistoryDto;
    }

    @Async
    public void fillFilter(SearchHistEntity searchHistEntity, ExpertSearchHistoryDto expertSearchHistoryDto) {
        searchHistEntity.setSearchType(expertSearchHistoryDto.getSearchType());
        searchHistEntity.setOntologyDisabled(expertSearchHistoryDto.isOntologyDisabled());
        searchHistEntity.setPage(expertSearchHistoryDto.getPage());
        searchHistEntity.setResultTypeGroupIds(new HashSet<>(expertSearchHistoryDto.getResultTypeGroupIds()));
        searchHistEntity.setDisabledSecondaryQueries(new HashSet<>(expertSearchHistoryDto.getDisabledSecondaryQueries()));
        searchHistEntity.setOrganizationIds(new HashSet<>(expertSearchHistoryDto.getOrganizationIds()));
        searchHistEntity.setNumberOfResultsFound(expertSearchHistoryDto.getResultsFound());
        searchHistEntity.setResultsLimit(expertSearchHistoryDto.getLimit());
        searchHistEntity.setYearFrom(expertSearchHistoryDto.getYearFrom());
        searchHistEntity.setYearTo(expertSearchHistoryDto.getYearTo());
        searchHistEntity.setShareCode(expertSearchHistoryDto.getShareCode());
        searchHistEntity.setCountryCodes(expertSearchHistoryDto.getCountryCodes());

        for (int i = 0; i < expertSearchHistoryDto.getSecondaryQueries().size(); i++) {
            final SearchHistSecondaryQueryEntity searchHistSecondaryQueryEntity = new SearchHistSecondaryQueryEntity();
            searchHistSecondaryQueryEntity.setQuery(expertSearchHistoryDto.getSecondaryQueries().get(i));
            searchHistSecondaryQueryEntity.setWeight(expertSearchHistoryDto.getSecondaryWeights().get(i));
            searchHistSecondaryQueryEntity.setSearchHistEntity(searchHistEntity);
            searchHistoryDao.saveSearchSecondaryQuery(searchHistSecondaryQueryEntity);
        }

        for (ExpertSearchHistoryDto.ResultDto resultDto : expertSearchHistoryDto.getResultDtos()) {
            final SearchHistResultEntity searchHistResultEntity = new SearchHistResultEntity();
            searchHistResultEntity.setSearchHistEntity(searchHistEntity);
            searchHistResultEntity.setPosition(resultDto.getPosition());
            searchHistResultEntity.setResultUserId(resultDto.getUserId());
            searchHistResultEntity.setResultExpertId(resultDto.getExpertId());
            searchHistoryDao.saveSearchResult(searchHistResultEntity);
        }
    }
}
