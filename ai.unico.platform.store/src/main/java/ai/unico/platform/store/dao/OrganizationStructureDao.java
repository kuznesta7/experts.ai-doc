package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.OrganizationStructureEntity;
import ai.unico.platform.store.entity.OrganizationStructureViewEntity;

import java.util.List;
import java.util.Set;

public interface OrganizationStructureDao {

    void save(OrganizationStructureEntity organizationStructureEntity);

    List<OrganizationStructureEntity> findAll();

    List<OrganizationStructureEntity> findParentOrganizationStructure(Long organizationId);

    List<OrganizationStructureEntity> findChildOrganizationStructure(Long organizationId);

    OrganizationStructureEntity find(Long parentOrganizationId, Long childOrganizationId);

    List<OrganizationStructureViewEntity> findOrganizationStructure(Long organizationId);

    List<OrganizationStructureViewEntity> findOrganizationStructure(Set<Long> organizationIds);

    List<OrganizationStructureViewEntity> findMembers(Long organizationId);
}
