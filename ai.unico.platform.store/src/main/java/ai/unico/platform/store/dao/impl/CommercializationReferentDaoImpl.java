package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.CommercializationReferentDao;
import ai.unico.platform.store.entity.CommercializationProjectReferentEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class CommercializationReferentDaoImpl implements CommercializationReferentDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void save(CommercializationProjectReferentEntity commercializationProjectReferentEntity) {
        entityManager.persist(commercializationProjectReferentEntity);
    }

    @Override
    public List<CommercializationProjectReferentEntity> findForCommercializationProject(Long commercializationId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(CommercializationProjectReferentEntity.class, "referent");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("commercializationProjectEntity.commercializationId", commercializationId));
        return criteria.list();
    }
}
