package ai.unico.platform.store.dao;

import java.util.List;
import java.util.Set;

public interface SearchHistoryStatisticsDao {

    Long countShownProfilesForExperts(Set<Long> userIds, Set<Long> expertIds);

    List<ShownProfileStatistics> findMostShownProfilesForExperts(Set<Long> userIds, Set<Long> expertIds, Integer page, Integer limit);

    Long countSearchedKeywordsForExperts(Set<Long> userIds, Set<Long> expertIds);

    List<SearchedKeyword> findSearchedKeywordsStatisticsForExperts(Set<Long> userIds, Set<Long> expertIds, Integer page, Integer limit);

    class ShownProfileStatistics {
        private Long expertId;

        private Long userId;

        private String name;

        private Long shownCount;

        private Double avgPosition;

        private Integer bestPosition;

        public Long getExpertId() {
            return expertId;
        }

        public void setExpertId(Long expertId) {
            this.expertId = expertId;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getShownCount() {
            return shownCount;
        }

        public void setShownCount(Long shownCount) {
            this.shownCount = shownCount;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Double getAvgPosition() {
            return avgPosition;
        }

        public void setAvgPosition(Double avgPosition) {
            this.avgPosition = avgPosition;
        }

        public Integer getBestPosition() {
            return bestPosition;
        }

        public void setBestPosition(Integer bestPosition) {
            this.bestPosition = bestPosition;
        }
    }

    class SearchedKeyword {

        private String query;

        private Long searchCount;

        private Long numberOfExpertsShown;

        private Double avgPosition;

        private Integer bestPosition;

        public String getQuery() {
            return query;
        }

        public void setQuery(String query) {
            this.query = query;
        }

        public Long getSearchCount() {
            return searchCount;
        }

        public void setSearchCount(Long searchCount) {
            this.searchCount = searchCount;
        }

        public Double getAvgPosition() {
            return avgPosition;
        }

        public void setAvgPosition(Double avgPosition) {
            this.avgPosition = avgPosition;
        }

        public Long getNumberOfExpertsShown() {
            return numberOfExpertsShown;
        }

        public void setNumberOfExpertsShown(Long numberOfExpertsShown) {
            this.numberOfExpertsShown = numberOfExpertsShown;
        }

        public Integer getBestPosition() {
            return bestPosition;
        }

        public void setBestPosition(Integer bestPosition) {
            this.bestPosition = bestPosition;
        }
    }
}
