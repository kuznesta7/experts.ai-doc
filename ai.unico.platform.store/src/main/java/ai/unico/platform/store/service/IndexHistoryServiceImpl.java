package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.IndexHistoryDto;
import ai.unico.platform.store.api.enums.IndexHistoryStatus;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.api.service.IndexHistoryService;
import ai.unico.platform.store.dao.IndexHistoryDao;
import ai.unico.platform.store.entity.IndexHistoryEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class IndexHistoryServiceImpl implements IndexHistoryService {

    @Autowired
    private IndexHistoryDao indexHistoryDao;

    @Override
    @Transactional
    public List<IndexHistoryDto> findLatest() {
        final List<IndexHistoryEntity> latest = indexHistoryDao.findLatest();
        final List<IndexHistoryDto> indexHistoryDtos = new ArrayList<>();
        final Set<IndexTarget> indexTargets = new HashSet<>();
        for (IndexHistoryEntity indexHistoryEntity : latest) {
            final IndexHistoryDto indexHistoryDto = new IndexHistoryDto();
            indexHistoryDto.setClean(indexHistoryEntity.isClean());
            indexHistoryDto.setHistoryId(indexHistoryEntity.getIndexHistoryId());
            indexHistoryDto.setStatus(indexHistoryEntity.getStatus());
            indexHistoryDto.setDate(indexHistoryEntity.getDateUpdate());
            indexHistoryDto.setUserId(indexHistoryEntity.getUserInsert());
            indexHistoryDto.setTarget(indexHistoryEntity.getTarget());
            indexHistoryDto.setErrorLog(indexHistoryEntity.getErrorLog());
            indexTargets.add(indexHistoryEntity.getTarget());
            indexHistoryDtos.add(indexHistoryDto);
        }
        for (IndexTarget indexTarget : IndexTarget.values()) {
            final IndexHistoryDto indexHistoryDto = new IndexHistoryDto();
            if (indexTargets.contains(indexTarget)) continue;
            indexHistoryDto.setTarget(indexTarget);
            indexHistoryDtos.add(indexHistoryDto);
        }
        return indexHistoryDtos;
    }

    @Override
    @Transactional
    public Long saveIndexHistory(IndexTarget target, boolean clean, UserContext userContext) {
        final IndexHistoryEntity indexHistoryEntity = new IndexHistoryEntity();
        indexHistoryEntity.setTarget(target);
        indexHistoryEntity.setClean(clean);
        indexHistoryEntity.setStatus(IndexHistoryStatus.STARTED);
        TrackableUtil.fillCreate(indexHistoryEntity, userContext);
        indexHistoryDao.save(indexHistoryEntity);
        return indexHistoryEntity.getIndexHistoryId();
    }

    @Override
    @Transactional
    public void updateIndexHistory(Long historyId, IndexHistoryStatus status, UserContext userContext) {
        updateIndexHistory(historyId, status, null, userContext);
    }

    @Override
    @Transactional
    public void updateIndexHistory(Long historyId, IndexHistoryStatus status, String errorLog, UserContext userContext) {
        final IndexHistoryEntity indexHistoryEntity = indexHistoryDao.find(historyId);
        indexHistoryEntity.setStatus(status);
        indexHistoryEntity.setErrorLog(errorLog);
        TrackableUtil.fillUpdate(indexHistoryEntity, userContext);
    }
}
