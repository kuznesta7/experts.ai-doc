package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.api.filter.FollowedProfilesFilter;
import ai.unico.platform.store.dao.FollowProfileDao;
import ai.unico.platform.store.entity.FollowedProfileEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.util.enums.OrderDirection;
import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class FollowProfileDaoImpl implements FollowProfileDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public FollowedProfileEntity findUserFollow(Long followingUserId, Long followedUserId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(FollowedProfileEntity.class, "followedProfile");
        criteria.add(Restrictions.eq("followerUser.userId", followingUserId));
        criteria.add(Restrictions.eq("followedUser.userId", followedUserId));
        criteria.add(Restrictions.eq("deleted", false));
        return (FollowedProfileEntity) criteria.uniqueResult();
    }

    @Override
    public FollowedProfileEntity findExpertFollow(Long followingUserId, Long followedExpertId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(FollowedProfileEntity.class, "followedProfile");
        criteria.add(Restrictions.eq("followerUser.userId", followingUserId));
        criteria.add(Restrictions.eq("followedExpertId", followedExpertId));
        criteria.add(Restrictions.eq("deleted", false));
        return (FollowedProfileEntity) criteria.uniqueResult();
    }

    @Override
    public void save(FollowedProfileEntity followedProfileEntity) {
        entityManager.persist(followedProfileEntity);
    }

    @Override
    public List<FollowedProfileEntity> findFollowedProfilesForUser(Long userId, FollowedProfilesFilter followedProfilesFilter) {
        final Criteria criteria = prepareCriteria(userId, followedProfilesFilter);
        if (followedProfilesFilter != null && followedProfilesFilter.getLimit() != null) {
            HibernateUtil.setPagination(criteria, followedProfilesFilter.getPage(), followedProfilesFilter.getLimit());
        }
        HibernateUtil.setOrder(criteria, "dateInsert", OrderDirection.DESC);
        return criteria.list();
    }

    @Override
    public List<FollowedProfileEntity> findFollowersForExpert(Long expertId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(FollowedProfileEntity.class, "followedProfile");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("followedExpertId", expertId));
        return criteria.list();
    }

    @Override
    public Long countFollowedProfilesForUser(Long userId, FollowedProfilesFilter followedProfilesFilter) {
        final Criteria criteria = prepareCriteria(userId, followedProfilesFilter);
        criteria.setProjection(Projections.countDistinct("followedProfileId"));
        return (Long) criteria.uniqueResult();
    }

    private Criteria prepareCriteria(Long userId, FollowedProfilesFilter followedProfilesFilter) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(FollowedProfileEntity.class, "followedProfile");
        criteria.add(Restrictions.eq("followerUser.userId", userId));
        criteria.add(Restrictions.eq("deleted", false));
        if (followedProfilesFilter.getWorkspaces() != null && !followedProfilesFilter.getWorkspaces().isEmpty()) {
            criteria.createAlias("followedProfileWorkspaceEntities", "followedProfileWorkspaceEntity", JoinType.RIGHT_OUTER_JOIN);
            criteria.createAlias("followedProfileWorkspaceEntity.userWorkspaceEntity", "userWorkspaceEntity", JoinType.RIGHT_OUTER_JOIN);
            criteria.add(Restrictions.and(
                    Restrictions.eq("followedProfileWorkspaceEntity.deleted", false),
                    Restrictions.eq("userWorkspaceEntity.deleted", false),
                    Restrictions.in("userWorkspaceEntity.workspaceName", followedProfilesFilter.getWorkspaces())
            ));
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        }
        return criteria;
    }

}
