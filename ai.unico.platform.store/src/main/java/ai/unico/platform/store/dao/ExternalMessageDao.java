package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ExternalMessageEntity;

public interface ExternalMessageDao {

    void saveMessage(ExternalMessageEntity externalMessageEntity);
}
