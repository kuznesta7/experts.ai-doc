package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ProjectProgramEntity;

import java.util.List;
import java.util.Set;

public interface ProjectProgramDao {

    List<ProjectProgramEntity> findAll();

    ProjectProgramEntity find(Long projectProgramId);

    List<ProjectProgramEntity> find(Set<Long> projectProgramIds);

    void save(ProjectProgramEntity projectProgramEntity);

    List<ProjectProgramEntity> autocomplete(String query);
}
