package ai.unico.platform.store.service;

import ai.unico.platform.store.api.service.UserProfileImgService;
import ai.unico.platform.store.dao.FileDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.FileEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.CacheKey;
import ai.unico.platform.util.ImgUtil;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.cache2k.Cache;
import org.cache2k.Cache2kBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.TimeUnit;

public class UserProfileImgServiceImpl implements UserProfileImgService {

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private FileDao fileDao;

    private final Cache<CacheKey, FileDto> cache = new Cache2kBuilder<CacheKey, FileDto>() {
    }
            .expireAfterWrite(30, TimeUnit.MINUTES)     // expire/refresh after 30 minutes
            .resilienceDuration(30, TimeUnit.SECONDS)   // cope with at most 30 seconds
            .permitNullValues(true)
            .refreshAhead(true)                         // keep fresh when expiring
            .loader(this::load)                         // auto populating function
            .build();

    @Override
    @Transactional
    public void saveImg(UserContext userContext, Long userId, ImgType type, FileDto fileDto) {
        ImgUtil.validateImageIsResizable(fileDto.getContent());
        final UserProfileEntity userProfileEntity = userProfileDao.findUserProfile(userId);
        if (userProfileEntity == null) throw new NonexistentEntityException(UserProfileEntity.class);

        final FileEntity imgFile;
        if (ImgType.COVER.equals(type)) {
            imgFile = userProfileEntity.getCoverImgFile();
        } else {
            imgFile = userProfileEntity.getImgFile();
        }
        final FileEntity fileEntity;

        if (imgFile == null) {
            fileEntity = new FileEntity();
            TrackableUtil.fillCreate(fileEntity, userContext);
        } else {
            fileEntity = imgFile;
            TrackableUtil.fillUpdate(fileEntity, userContext);
        }

        fileEntity.setName(fileDto.getName());
        fileEntity.setContent(fileDto.getContent());
        fileDao.save(fileEntity);
        if (ImgType.COVER.equals(type)) {
            userProfileEntity.setCoverImgFile(fileEntity);
        } else {
            userProfileEntity.setImgFile(fileEntity);
        }
        for (CacheKey key : cache.keys()) {
            if (key.getEntityId().equals(userId)) {
                cache.remove(key);
            }
        }
    }

    @Override
    @Transactional
    public FileDto getImg(Long userId, ImgType type, String size) {
        final String typeDefault = type != null ? type.toString() : ImgType.PORTRAIT.toString();
        final CacheKey cacheKey = new CacheKey(userId, null, typeDefault, size);
        return cache.get(cacheKey);
    }

    private FileDto load(CacheKey cacheKey) {
        final Long userId = cacheKey.getEntityId();

        final UserProfileEntity userProfileEntity = userProfileDao.findUserProfile(userId);
        if (userProfileEntity == null) return null;
        final FileEntity userImgFile;
        if (ImgType.COVER.toString().equals(cacheKey.getType())) {
            userImgFile = userProfileEntity.getCoverImgFile();
        } else {
            userImgFile = userProfileEntity.getImgFile();
        }
        if (userImgFile == null) return null;

        final byte[] logoContent = userImgFile.getContent();

        final byte[] content;
        final String sizeCode = cacheKey.getSize();
        if (sizeCode != null) {
            final int size = ImgUtil.codeToSize(sizeCode);
            content = ImgUtil.resize(logoContent, size, null);
        } else {
            content = logoContent;
        }
        return new FileDto(content, userImgFile.getName());
    }
}
