package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.HttpRequestDao;
import ai.unico.platform.store.entity.HttpRequestEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class HttpRequestDaoImpl implements HttpRequestDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    @Transactional
    public void save(HttpRequestEntity httpRequestEntity) {
        entityManager.persist(httpRequestEntity);
    }
}
