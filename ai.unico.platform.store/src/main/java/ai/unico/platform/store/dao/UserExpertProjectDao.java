package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.UserExpertProjectEntity;

public interface UserExpertProjectDao {

    UserExpertProjectEntity findForExpert(Long expertId, Long projectId);

    UserExpertProjectEntity findForUser(Long userId, Long projectId);

    void saveProjectUserExpert(UserExpertProjectEntity userExpertProjectEntity);
}
