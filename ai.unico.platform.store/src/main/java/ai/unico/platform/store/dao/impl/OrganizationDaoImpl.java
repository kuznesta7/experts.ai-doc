package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.entity.FileEntity;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.store.util.CollectionsUtil;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class OrganizationDaoImpl implements OrganizationDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public List<OrganizationEntity> find() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationEntity.class, "organization");
        return criteria.list();
    }

    @Override
    public OrganizationEntity find(Long organizationId) {
        return entityManager.find(OrganizationEntity.class, organizationId);
    }

    @Override
    public List<OrganizationEntity> findByIds(Collection<Long> organizationIds) {
        if (organizationIds.isEmpty()) return Collections.emptyList();
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationEntity.class, "organization");
        criteria.add(Restrictions.in("organizationId", organizationIds));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @Override
    public OrganizationEntity findByOriginalId(Long originalOrganizationId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationEntity.class, "organization");
        criteria.add(Restrictions.eq("originalOrganizationId", originalOrganizationId));
        return (OrganizationEntity) criteria.uniqueResult();
    }

    @Override
    public List<OrganizationEntity> findBySubstituteId(Long substituteOrganizationId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationEntity.class, "organization");
        criteria.createAlias("organization.substituteOrganizationEntity", "sub");
        criteria.add(Restrictions.eq("sub.organizationId", substituteOrganizationId));
        return  criteria.list();
    }

    @Override
    public List<OrganizationEntity> findByOriginalIds(Collection<Long> originalOrganizationIds) {
        if (originalOrganizationIds.isEmpty()) return Collections.emptyList();
        final List<List<Long>> lists = CollectionsUtil.toSubCollectionForHibernate(originalOrganizationIds);
        final List<OrganizationEntity> result = new ArrayList<>();
        for (List<Long> list : lists) {
            final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationEntity.class, "organization");
            criteria.add(Restrictions.in("originalOrganizationId", list));
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            result.addAll(criteria.list());
        }
        return result;
    }

    @Override
    public void saveOrganization(OrganizationEntity organizationEntity) {
        entityManager.persist(organizationEntity);
    }

    @Override
    public List<OrganizationRankHelper> findAndComputeOrganizationsRank() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationEntity.class, "organization");
        criteria.createAlias("organizationItemEntities", "organizationItemEntity", JoinType.LEFT_OUTER_JOIN, Restrictions.eq("deleted", false));
        criteria.createAlias("organizationUserEntities", "organizationUserEntity", JoinType.LEFT_OUTER_JOIN, Restrictions.eq("deleted", false));
        criteria.setProjection(Projections.projectionList()
                .add(Projections.groupProperty("organizationId").as("organizationId"))
                .add(Projections.groupProperty("rank").as("rank"))
                .add(Projections.countDistinct("organizationItemEntity.itemEntity.itemId").as("itemCount"))
                .add(Projections.countDistinct("organizationUserEntity.userProfileEntity.userId").as("userCount"))
        );
        criteria.setResultTransformer(new AliasToBeanResultTransformer(OrganizationRankHelper.class));
        return criteria.list();
    }

    @Override
    public List<Long> findIdsForCountryCodes(Collection<String> countryCodes) {
        if (countryCodes.isEmpty()) return Collections.emptyList();
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationEntity.class, "organization");
        criteria.add(Restrictions.or(Restrictions.in("countryEntity.countryCode", countryCodes), Restrictions.isNull("countryEntity.countryCode")));
        criteria.setProjection(Projections.property("organizationId"));
        return criteria.list();
    }

    @Override
    public FileEntity findOrganizationLogoFile(Long organizationId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationEntity.class, "organization");
        criteria.add(Restrictions.eq("organization.organizationId", organizationId));
        criteria.createAlias("organization.organizationLogoEntity", "file");
        criteria.setFetchMode("file.content", FetchMode.JOIN);
        final OrganizationEntity organizationEntity = (OrganizationEntity) criteria.uniqueResult();
        if (organizationEntity == null) return null;
        return organizationEntity.getOrganizationLogoEntity();
    }

    @Override
    public FileEntity findOrganizationCoverImgFile(Long organizationId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationEntity.class, "organization");
        criteria.add(Restrictions.eq("organization.organizationId", organizationId));
        criteria.createAlias("organization.organizationCoverEntity", "file");
        criteria.setFetchMode("file.content", FetchMode.JOIN);
        final OrganizationEntity organizationEntity = (OrganizationEntity) criteria.uniqueResult();
        if (organizationEntity == null) return null;
        return organizationEntity.getOrganizationCoverEntity();
    }

    @Override
    public List<OrganizationEntity> getRecombeeOrganizations() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationEntity.class, "organization");
        criteria.add(Restrictions.isNotNull("organization.recombeeDbIdentifier"));
        return criteria.list();
    }
}
