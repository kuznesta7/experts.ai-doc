package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class SearchHistWorkspaceEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long searchHistWorkspaceId;

    @ManyToOne
    @JoinColumn(name = "user_workspace_id")
    private UserWorkspaceEntity userWorkspaceEntity;

    @ManyToOne
    @JoinColumn(name = "search_hist_id")
    private SearchHistEntity searchHistEntity;

    private boolean deleted;

    public Long getSearchHistWorkspaceId() {
        return searchHistWorkspaceId;
    }

    public void setSearchHistWorkspaceId(Long searchHistWorkspaceId) {
        this.searchHistWorkspaceId = searchHistWorkspaceId;
    }

    public UserWorkspaceEntity getUserWorkspaceEntity() {
        return userWorkspaceEntity;
    }

    public void setUserWorkspaceEntity(UserWorkspaceEntity userWorkspaceEntity) {
        this.userWorkspaceEntity = userWorkspaceEntity;
    }

    public SearchHistEntity getSearchHistEntity() {
        return searchHistEntity;
    }

    public void setSearchHistEntity(SearchHistEntity searchHistEntity) {
        this.searchHistEntity = searchHistEntity;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
