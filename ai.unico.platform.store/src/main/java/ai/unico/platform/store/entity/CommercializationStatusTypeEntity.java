package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.ReferenceData;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class CommercializationStatusTypeEntity extends ReferenceData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commercializationStatusId;

    private boolean useInSearch;

    private boolean useInInvest;

    private boolean canSetOrganization;

    private boolean mustBeFilled;

    private Long statusOrder;

    @OneToMany(mappedBy = "commercializationStatusTypeEntity")
    private Set<CommercializationStatusCategoryEntity> commercializationStatusCategoryEntities = new HashSet<>();

    public Long getStatusOrder() {
        return statusOrder;
    }

    public void setStatusOrder(Long statusOrder) {
        this.statusOrder = statusOrder;
    }

    public Long getCommercializationStatusId() {
        return commercializationStatusId;
    }

    public void setCommercializationStatusId(Long commercializationStatusId) {
        this.commercializationStatusId = commercializationStatusId;
    }

    public boolean isMustBeFilled() {
        return mustBeFilled;
    }

    public void setMustBeFilled(boolean mustBeFilled) {
        this.mustBeFilled = mustBeFilled;
    }

    public boolean isUseInSearch() {
        return useInSearch;
    }

    public void setUseInSearch(boolean useInSearch) {
        this.useInSearch = useInSearch;
    }

    public boolean isUseInInvest() {
        return useInInvest;
    }

    public void setUseInInvest(boolean useInInvest) {
        this.useInInvest = useInInvest;
    }

    public Set<CommercializationStatusCategoryEntity> getCommercializationStatusCategoryEntities() {
        return commercializationStatusCategoryEntities;
    }

    public void setCommercializationStatusCategoryEntities(Set<CommercializationStatusCategoryEntity> commercializationStatusCategoryEntities) {
        this.commercializationStatusCategoryEntities = commercializationStatusCategoryEntities;
    }

    public boolean isCanSetOrganization() {
        return canSetOrganization;
    }

    public void setCanSetOrganization(boolean canSetOrganization) {
        this.canSetOrganization = canSetOrganization;
    }
}
