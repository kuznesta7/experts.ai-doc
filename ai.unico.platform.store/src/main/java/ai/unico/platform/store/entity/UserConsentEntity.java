package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
@Table(name = "user_consent")
public class UserConsentEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userConsentId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserProfileEntity userProfileEntity;

    @ManyToOne
    @JoinColumn(name = "consent_id")
    private ConsentEntity consentEntity;

    @Column
    private Boolean deleted;

    public Long getUserConsentId() {
        return userConsentId;
    }

    public void setUserConsentId(Long userConsentId) {
        this.userConsentId = userConsentId;
    }

    public UserProfileEntity getUserProfileEntity() {
        return userProfileEntity;
    }

    public void setUserProfileEntity(UserProfileEntity userProfileEntity) {
        this.userProfileEntity = userProfileEntity;
    }

    public ConsentEntity getConsentEntity() {
        return consentEntity;
    }

    public void setConsentEntity(ConsentEntity consentEntity) {
        this.consentEntity = consentEntity;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
