package ai.unico.platform.store.service;

import ai.unico.platform.store.api.enums.CommercializationOrganizationRelation;
import ai.unico.platform.store.api.service.CommercializationOrganizationService;
import ai.unico.platform.store.dao.CommercializationOrganizationDao;
import ai.unico.platform.store.dao.CommercializationProjectDao;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.entity.CommercializationProjectEntity;
import ai.unico.platform.store.entity.CommercializationProjectOrganizationEntity;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class CommercializationOrganizationServiceImpl implements CommercializationOrganizationService {

    @Autowired
    private CommercializationOrganizationDao commercializationOrganizationDao;

    @Autowired
    private CommercializationProjectDao commercializationProjectDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Override
    @Transactional
    public void addOrganization(Long commercializationId, Long organizationId, CommercializationOrganizationRelation relation, UserContext userContext) {
        final CommercializationProjectOrganizationEntity existingCommercializationOrganization = commercializationOrganizationDao.find(commercializationId, organizationId, relation);
        if (existingCommercializationOrganization != null) {
            throw new EntityAlreadyExistsException(CommercializationProjectOrganizationEntity.class);
        }
        final CommercializationProjectEntity commercializationProjectEntity = commercializationProjectDao.find(commercializationId);
        if (commercializationProjectEntity == null) {
            throw new NonexistentEntityException(CommercializationProjectEntity.class);
        }
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        if (organizationEntity == null) {
            throw new NonexistentEntityException(OrganizationEntity.class);
        }
        final CommercializationProjectOrganizationEntity entity = new CommercializationProjectOrganizationEntity();
        entity.setCommercializationProjectEntity(commercializationProjectEntity);
        entity.setOrganizationEntity(organizationEntity);
        entity.setRelation(relation);
        TrackableUtil.fillCreate(entity, userContext);
        commercializationOrganizationDao.save(entity);
    }

    @Override
    @Transactional
    public void removeOrganization(Long commercializationId, Long organizationId, CommercializationOrganizationRelation relation, UserContext userContext) {
        final CommercializationProjectOrganizationEntity entity = commercializationOrganizationDao.find(commercializationId, organizationId, relation);
        if (entity == null) throw new NonexistentEntityException(CommercializationProjectOrganizationEntity.class);
        entity.setDeleted(true);
        TrackableUtil.fillUpdate(entity, userContext);
    }
}
