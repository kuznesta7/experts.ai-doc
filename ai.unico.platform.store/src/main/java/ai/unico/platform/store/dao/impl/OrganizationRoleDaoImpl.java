package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.OrganizationRoleDao;
import ai.unico.platform.store.entity.OrganizationUserRoleEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.util.enums.OrganizationRole;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class OrganizationRoleDaoImpl implements OrganizationRoleDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<OrganizationUserRoleEntity> findOrganizationsRolesForUser(Long userId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationUserRoleEntity.class, "organizationRole");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("userProfileEntity.userId", userId));
        return (List<OrganizationUserRoleEntity>) criteria.list();
    }

    @Override
    public OrganizationUserRoleEntity find(Long userId, Long organizationId, OrganizationRole role) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationUserRoleEntity.class, "organizationRole");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("userProfileEntity.userId", userId));
        criteria.add(Restrictions.eq("organizationEntity.organizationId", organizationId));
        criteria.add(Restrictions.eq("role", role));
        return (OrganizationUserRoleEntity) criteria.uniqueResult();
    }

    @Override
    public List<OrganizationUserRoleEntity> findForOrganization(Long organizationId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationUserRoleEntity.class, "organizationRole");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("organizationEntity.organizationId", organizationId));
        return criteria.list();
    }

    @Override
    public List<OrganizationUserRoleEntity> findForOrganization(Long organizationId, OrganizationRole role) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationUserRoleEntity.class, "organizationRole");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("organizationEntity.organizationId", organizationId));
        criteria.add(Restrictions.eq("role", role));
        return criteria.list();
    }

    @Override
    public void save(OrganizationUserRoleEntity organizationUserRoleEntity) {
        entityManager.persist(organizationUserRoleEntity);
    }
}
