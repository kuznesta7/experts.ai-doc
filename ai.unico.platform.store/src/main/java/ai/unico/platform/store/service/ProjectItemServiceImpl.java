package ai.unico.platform.store.service;

import ai.unico.platform.store.api.service.ProjectItemService;
import ai.unico.platform.store.api.service.ProjectService;
import ai.unico.platform.store.dao.ItemDao;
import ai.unico.platform.store.dao.ProjectDao;
import ai.unico.platform.store.dao.ProjectItemDao;
import ai.unico.platform.store.entity.ItemEntity;
import ai.unico.platform.store.entity.ProjectEntity;
import ai.unico.platform.store.entity.ProjectItemEntity;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.enums.PlatformDataSource;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProjectItemServiceImpl implements ProjectItemService {

    @Autowired
    private ProjectItemDao projectItemDao;
    @Autowired
    private ProjectDao projectDao;
    @Autowired
    private ItemDao itemDao;
    @Autowired
    private ProjectService projectService;

    @Override
    @Transactional
    public void addItemToProject(Long projectId, Long itemId, PlatformDataSource itemDataSource, UserContext userContext) throws NonexistentEntityException {
        final ProjectItemEntity projectItem;
        if (PlatformDataSource.STORE.equals(itemDataSource)) {
            projectItem = projectItemDao.findProjectItem(itemId, projectId);
        } else {
            projectItem = projectItemDao.findProjectDwhItem(itemId, projectId);
        }
        if (projectItem != null) throw new EntityAlreadyExistsException(ProjectItemEntity.class);

        final ProjectEntity projectEntity = projectDao.find(projectId);
        if (projectEntity == null) {
            throw new NonexistentEntityException(ProjectEntity.class);
        }

        final ProjectItemEntity projectItemEntity = new ProjectItemEntity();
        projectItemEntity.setDeleted(false);
        projectItemEntity.setProjectEntity(projectEntity);
        TrackableUtil.fillCreate(projectItemEntity, userContext);
        if (PlatformDataSource.STORE.equals(itemDataSource)) {
            final ItemEntity item = itemDao.find(itemId);
            if (item == null) {
                throw new NonexistentEntityException(ItemEntity.class);
            }
            projectItemEntity.setItemEntity(item);
        } else {
            projectItemEntity.setOriginalItemId(itemId);
        }
        projectItemDao.saveProjectItem(projectItemEntity);
    }

    @Override
    @Transactional
    public void removeItemFromProject(Long projectId, Long itemId, PlatformDataSource itemDataSource, UserContext userContext) throws NonexistentEntityException {
        final ProjectItemEntity projectItem;
        if (PlatformDataSource.STORE.equals(itemDataSource)) {
            projectItem = projectItemDao.findProjectItem(itemId, projectId);
        } else {
            projectItem = projectItemDao.findProjectDwhItem(itemId, projectId);
        }
        if (projectItem == null) {
            throw new NonexistentEntityException(ProjectItemEntity.class);
        } else {
            projectItem.setDeleted(true);
            TrackableUtil.fillUpdate(projectItem, userContext);
        }
    }

    @Override
    @Transactional
    public void updateProjectItems(Set<Long> originalItemIds) {
        final List<ProjectItemEntity> projectItemEntities = projectItemDao.findAllWithOriginalItemIds(originalItemIds);
        final Set<Long> projectItemsToChangeIds = projectItemEntities.stream()
                .map(ProjectItemEntity::getOriginalItemId)
                .filter(originalItemIds::contains)
                .collect(Collectors.toSet());
        final List<ItemEntity> itemEntities = itemDao.findItemsByOriginalIds(projectItemsToChangeIds);
        final Map<Long, ItemEntity> originalItemIdEntityMap = itemEntities.stream().collect(Collectors.toMap(ItemEntity::getOrigItemId, Function.identity()));
        final Set<Long> projectToReindexIds = new HashSet<>();
        for (ProjectItemEntity projectItemEntity : projectItemEntities) {
            final Long originalItemId = projectItemEntity.getOriginalItemId();
            if (originalItemId == null || !originalItemIdEntityMap.containsKey(originalItemId) || HibernateUtil.getId(projectItemEntity, ProjectItemEntity::getItemEntity, ItemEntity::getItemId) != null) {
                continue;
            }
            projectItemEntity.setItemEntity(originalItemIdEntityMap.get(originalItemId));
            final Long projectId = projectItemEntity.getProjectEntity().getProjectId();
            projectToReindexIds.add(projectId);
        }
        projectService.loadAndIndexProjects(projectToReindexIds);
    }
}
