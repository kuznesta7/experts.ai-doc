package ai.unico.platform.store.service;

import ai.unico.platform.store.api.enums.WidgetType;
import ai.unico.platform.store.api.service.RecommendedItemService;
import ai.unico.platform.store.dao.RecommendedItemDao;
import ai.unico.platform.store.entity.RecommendedItemEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class RecommendedItemServiceImpl implements RecommendedItemService {

    @Autowired
    private RecommendedItemDao recommendedItemDao;

    @Override
    @Transactional
    public void saveRecommendation(String recommId, String recommendation, WidgetType widgetType, String user, int count) {
        RecommendedItemEntity recommendedItem = new RecommendedItemEntity();
        recommendedItem.setRecommendation(recommendation);
        recommendedItem.setId(recommId);
        recommendedItem.setWidgetType(widgetType);
        recommendedItem.setUserHash(user);
        recommendedItem.setNumOfRecomms(count);
        recommendedItemDao.save(recommendedItem);
    }

    @Override
    @Transactional
    public void extendRecommendation(String recommId, String[] recomm) {
        recommendedItemDao.extendRecommendation(recommId, recomm);
    }

    @Override
    @Transactional
    public String[] getRecommendations(String recommId) {
        return recommendedItemDao.getRecommendations(recommId);
    }

    @Override
    @Transactional
    public int getNumberOfRecomms(String recommId) {
        return recommendedItemDao.find(recommId).getNumOfRecomms();
    }
}
