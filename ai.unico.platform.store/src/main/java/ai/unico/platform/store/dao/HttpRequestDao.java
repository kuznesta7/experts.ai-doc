package ai.unico.platform.store.dao;


import ai.unico.platform.store.entity.HttpRequestEntity;

public interface HttpRequestDao {

    void save(HttpRequestEntity httpRequestEntity);

}
