package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.InternalMessageDto;
import ai.unico.platform.store.api.dto.MessageDto;
import ai.unico.platform.store.api.exception.MissingParameterException;
import ai.unico.platform.store.api.filter.MessageFilter;
import ai.unico.platform.store.api.service.MessageService;
import ai.unico.platform.store.api.service.UnicoNotifierService;
import ai.unico.platform.store.api.wrapper.MessageWrapper;
import ai.unico.platform.store.dao.*;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageDao messageDao;

    @Autowired
    private ExternalMessageDao externalMessageDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private SearchHistoryDao searchHistoryDao;

    @Autowired
    private UnicoNotifierService unicoNotifierService;

    @Override
    @Transactional
    public void sendInternalMessage(InternalMessageDto internalMessageDto, UserContext userContext) {
        final UserProfileEntity userFromEntity = userProfileDao.findUserProfile(userContext.getUserId());
        if (userFromEntity == null) throw new NonexistentEntityException(UserProfileEntity.class);

        ExternalMessageEntity userMessageEntity = new ExternalMessageEntity();

        userMessageEntity.setUserProfileEntity(userFromEntity);
        userMessageEntity.setMessage(internalMessageDto.getMessage());
        userMessageEntity.setSenderEmail(userFromEntity.getEmail());

        final OrganizationEntity organizationEntity = organizationDao.find(internalMessageDto.getOrganizationId());
        userMessageEntity.setOrganizationEntity(organizationEntity);

        externalMessageDao.saveMessage(userMessageEntity);


        internalMessageDto.setSenderEmail(userFromEntity.getEmail());

        String emails = organizationEntity.getOrganizationUserRoleEntities().stream()
                .filter(x -> !x.isDeleted() && x.getRole() == OrganizationRole.ORGANIZATION_ADMIN)
                .map(x -> x.getUserProfileEntity().getEmail())
                .collect(Collectors.joining(","));

        unicoNotifierService.sentRequestMail(internalMessageDto, userFromEntity.getName(), organizationEntity.getOrganizationName(), emails);
    }

    @Override
    @Transactional
    public void sendMessage(MessageDto messageDto, UserContext userContext, Long searchId) throws NonexistentEntityException {
        final UserMessageEntity userMessageEntity = new UserMessageEntity();

        final UserProfileEntity userFromEntity = userProfileDao.findUserProfile(userContext.getUserId());
        if (userFromEntity == null) throw new NonexistentEntityException(UserProfileEntity.class);

        final Long userToId = messageDto.getUserToId();
        final Long expertToId = messageDto.getExpertToId();
        if (userToId != null) {
            final UserProfileEntity userToEntity = userProfileDao.findUserProfile(userToId);
            if (userToEntity == null) throw new NonexistentEntityException(UserProfileEntity.class);
            userMessageEntity.setUserToEntity(userToEntity);
        } else {
            if (expertToId == null) throw new MissingParameterException("expertToId");
            userMessageEntity.setExpertToId(expertToId);
        }

        userMessageEntity.setUserFromEntity(userFromEntity);
        userMessageEntity.setContent(messageDto.getContent());
        userMessageEntity.setSubject(messageDto.getSubject());
        userMessageEntity.setDeleted(false);
        userMessageEntity.setRead(false);
        TrackableUtil.fillCreateTrackable(userMessageEntity, userContext.getUserId());

        if (searchId != null) {
            final SearchHistEntity searchHistory = searchHistoryDao.findSearchHistory(searchId);
            userMessageEntity.setSearchHistEntity(searchHistory);
        }
        messageDao.saveMessage(userMessageEntity);

        messageDto.setUserFromName(userFromEntity.getName());
        messageDto.setUserFromId(userFromEntity.getUserId());
        messageDto.setUserToName(userMessageEntity.getUserToEntity().getName());
        //todo fill user/expert to full name
        unicoNotifierService.sendUserContactRequest(messageDto, userFromEntity.getEmail());
    }



    @Override
    @Transactional
    public void sendMessageToOrganization(InternalMessageDto internalMessageDto, UserContext userContext) throws NonexistentEntityException {
        ExternalMessageEntity userMessageEntity = new ExternalMessageEntity();

        final UserProfileEntity userFromEntity = userProfileDao.findUserProfile(userContext.getUserId());
        if (userFromEntity == null) throw new NonexistentEntityException(UserProfileEntity.class);


        userMessageEntity.setUserProfileEntity(userFromEntity);
        userMessageEntity.setMessage(internalMessageDto.getMessage());
        userMessageEntity.setSenderEmail(userFromEntity.getEmail());

        final OrganizationEntity organizationEntity = organizationDao.find(internalMessageDto.getOrganizationId());
        userMessageEntity.setOrganizationEntity(organizationEntity);

        externalMessageDao.saveMessage(userMessageEntity);

        String emails = organizationEntity.getOrganizationUserRoleEntities().stream()
                .filter(x -> !x.isDeleted() && x.getRole() == OrganizationRole.ORGANIZATION_EDITOR)
                .map(x -> x.getUserProfileEntity().getEmail())
                .collect(Collectors.joining(","));

        MessageDto messageDto = new MessageDto();
        messageDto.setUserToId(internalMessageDto.getUserId());
        messageDto.setExpertToId(internalMessageDto.getExpertId());
        messageDto.setContent(internalMessageDto.getMessage());

        unicoNotifierService.sendInternalOrganizationContactRequest(messageDto, emails, userFromEntity.getEmail(), userFromEntity.getName(), organizationEntity.getOrganizationName());

    }


    @Override
    @Transactional
    public void markMessageAsRead(Long messageId, UserContext userContext) throws NonexistentEntityException, UserUnauthorizedException {
        final UserMessageEntity message = messageDao.findMessage(messageId);
        if (message == null) throw new NonexistentEntityException(UserMessageEntity.class);
        if (!canUserMarkMessageAsRead(message, userContext)) throw new UserUnauthorizedException();

        message.setRead(true);
        TrackableUtil.fillUpdateTrackable(message, userContext.getUserId());
    }

    @Override
    @Transactional
    public MessageWrapper getMessages(MessageFilter messageFilter, UserContext userContext) {
        final MessageWrapper messageWrapper = new MessageWrapper();
        final List<MessageDto> messageDtos = new ArrayList<>();
        final List<UserMessageEntity> messages = messageDao.findMessages(userContext.getUserId(), messageFilter);

        for (UserMessageEntity message : messages) {
            messageDtos.add(convert(message));
        }
        messageWrapper.setMessages(messageDtos);
        messageWrapper.setLimit(messageFilter.getLimit());
        messageWrapper.setPage(messageFilter.getPage());

        return messageWrapper;
    }

    @Override
    @Transactional
    public MessageDto findMessage(Long messageId, UserContext userContext) throws UserUnauthorizedException {
        final UserMessageEntity message = messageDao.findMessage(messageId);
        if (!canUserShowMessage(message, userContext)) throw new UserUnauthorizedException();
        final MessageDto messageDto = convert(message);

        message.setRead(true);
        TrackableUtil.fillUpdateTrackable(message, userContext.getUserId());

        return messageDto;
    }

    private Boolean canUserMarkMessageAsRead(UserMessageEntity messageEntity, UserContext userContext) {
        final Long userId = messageEntity.getUserToEntity().getUserId();
        return (userId.equals(userContext.getUserId()));
    }

    private Boolean canUserShowMessage(UserMessageEntity messageEntity, UserContext userContext) {
        final Long userToId = messageEntity.getUserToEntity().getUserId();
        final Long userFromId = messageEntity.getUserFromEntity().getUserId();
        final Long userId = userContext.getUserId();
        return (userToId.equals(userId) || userFromId.equals(userId));
    }

    private MessageDto convert(UserMessageEntity message) {
        final MessageDto messageDto = new MessageDto();
        final UserProfileEntity userFromEntity = message.getUserFromEntity();
        final UserProfileEntity userToEntity = message.getUserToEntity();
        final UserMessageEntity parentMessageEntity = message.getParentMessageEntity();

        messageDto.setMessageId(message.getUserMessageId());
        messageDto.setUserFromId(userFromEntity.getUserId());
        messageDto.setUserFromName(userFromEntity.getName());
        messageDto.setUserToId(userToEntity.getUserId());
        messageDto.setUserToName(userToEntity.getName());
        messageDto.setSubject(message.getSubject());
        messageDto.setContent(message.getContent());
        if (parentMessageEntity != null) {
            messageDto.setParentMessageId(parentMessageEntity.getUserMessageId());
        }
        return messageDto;
    }
}
