package ai.unico.platform.store.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "item_type_group")
public class ItemTypeGroupEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long typeGroupId;

    private String title;

    @OneToMany
    @JoinColumn(name = "type_group_id")
    private Set<ItemTypeEntity> itemTypeEntities;

    public Long getTypeGroupId() {
        return typeGroupId;
    }

    public void setTypeGroupId(Long typeGroupId) {
        this.typeGroupId = typeGroupId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<ItemTypeEntity> getItemTypeEntities() {
        return itemTypeEntities;
    }

    public void setItemTypeEntities(Set<ItemTypeEntity> itemTypeEntities) {
        this.itemTypeEntities = itemTypeEntities;
    }
}
