package ai.unico.platform.store.entity;

import ai.unico.platform.store.api.enums.ClaimRequestType;
import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class ClaimRequestEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long claimRequestId;

    @Enumerated(EnumType.STRING)
    private ClaimRequestType type;

    private String note;

    private Long concernedUserId;

    private Long concernedExpertId;

    public Long getClaimRequestId() {
        return claimRequestId;
    }

    public void setClaimRequestId(Long claimRequestId) {
        this.claimRequestId = claimRequestId;
    }

    public ClaimRequestType getType() {
        return type;
    }

    public void setType(ClaimRequestType type) {
        this.type = type;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getConcernedUserId() {
        return concernedUserId;
    }

    public void setConcernedUserId(Long concernedUserId) {
        this.concernedUserId = concernedUserId;
    }

    public Long getConcernedExpertId() {
        return concernedExpertId;
    }

    public void setConcernedExpertId(Long concernedExpertId) {
        this.concernedExpertId = concernedExpertId;
    }
}
