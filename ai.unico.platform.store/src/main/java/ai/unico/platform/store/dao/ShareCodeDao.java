package ai.unico.platform.store.dao;

import ai.unico.platform.store.api.enums.ShareCodeType;
import ai.unico.platform.store.entity.ShareCodeEntity;

public interface ShareCodeDao {

    void save(ShareCodeEntity shareCodeEntity);

    ShareCodeEntity find(String shareCode, ShareCodeType type);

}
