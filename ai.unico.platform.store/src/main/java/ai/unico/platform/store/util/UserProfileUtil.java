package ai.unico.platform.store.util;

import ai.unico.platform.store.api.dto.ItemPreviewDto;
import ai.unico.platform.store.api.dto.UserProfileDto;
import ai.unico.platform.store.api.dto.UserProfileIndexDto;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.util.KeywordsUtil;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class UserProfileUtil {

    public static UserProfileDto convert(UserProfileEntity userProfileEntity) {
        final Set<UserExpertItemEntity> itemUserEntities = userProfileEntity.getUserItemEntities();
        final Set<UserExpertEntity> userExpertEntities = userProfileEntity.getUserExpertEntities();

        final UserProfileDto userExpertDetailDto = new UserProfileDto();

        for (UserExpertEntity userExpertEntity : userExpertEntities) {
            if (userExpertEntity.getDeleted()) continue;
            userExpertDetailDto.getExpertIds().add(userExpertEntity.getExpertId());
        }

        for (UserExpertItemEntity userExpertItemEntity : itemUserEntities) {
            if (userExpertItemEntity.isDeleted() || userExpertItemEntity.isHidden()) continue;
            final ItemEntity itemEntity = userExpertItemEntity.getItemEntity();
            ItemPreviewDto itemPreviewDto = UserItemUtil.convertToPreview(itemEntity);
            userExpertDetailDto.getItemPreviewDtos().add(itemPreviewDto);
        }

        userExpertDetailDto.setUserId(userProfileEntity.getUserId());
        userExpertDetailDto.setName(userProfileEntity.getName());
        userExpertDetailDto.setUsername(userProfileEntity.getUsername());
//        userExpertDetailDto.setEmail(userProfileEntity.getEmail());
        userExpertDetailDto.setDescription(userProfileEntity.getDescription());
        userExpertDetailDto.setPositionDescription(userProfileEntity.getPositionDescription());

        final Set<UserOrganizationEntity> userOrganizationEntities = userProfileEntity.getUserOrganizationEntities();
        for (UserOrganizationEntity userOrganizationEntity : userOrganizationEntities) {
            if (userOrganizationEntity.isDeleted()) continue;
            final OrganizationEntity organizationEntity = userOrganizationEntity.getOrganizationEntity();
            userExpertDetailDto.getOrganizationDtos().add(OrganizationUtil.convertBase(organizationEntity));
        }
        return userExpertDetailDto;
    }

    public static UserProfileIndexDto convertToIndex(UserProfileEntity userProfileEntity) {
        final UserProfileIndexDto userProfileIndexDto = new UserProfileIndexDto();
        final Set<UserExpertEntity> userExpertEntities = userProfileEntity.getUserExpertEntities() != null ? userProfileEntity.getUserExpertEntities() : Collections.emptySet();
        final Set<UserOrganizationEntity> userOrganizationEntities = userProfileEntity.getUserOrganizationEntities();
        final Set<Long> expertIds = userExpertEntities.stream()
                .filter(userExpertEntity -> !userExpertEntity.getDeleted())
                .map(UserExpertEntity::getExpertId)
                .collect(Collectors.toSet());
        final Set<Long> organizationIds = createOrganizationIdSet(userOrganizationEntities, x -> true);
        final Set<Long> verifiedOrganizationIds = createOrganizationIdSet(userOrganizationEntities, UserOrganizationEntity::isVerified);
        final Set<Long> favoriteOrganizationIds = createOrganizationIdSet(userOrganizationEntities, UserOrganizationEntity::isFavorite);
        final Set<Long> retiredOrganizationIds = createOrganizationIdSet(userOrganizationEntities, UserOrganizationEntity::isRetired);
        final Set<Long> visibleOrganizationIds = createOrganizationIdSet(userOrganizationEntities, UserOrganizationEntity::isVisible);
        final Set<Long> activeOrganizationIds = createOrganizationIdSet(userOrganizationEntities, UserOrganizationEntity::isActive);

        final Set<Long> claimedExpertIds = userProfileEntity.getUserExpertEntities().stream()
                .map(UserExpertEntity::getExpertId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        final Set<Long> claimedUserIds = userProfileEntity.getUserExpertEntities().stream()
                .map(x -> HibernateUtil.getId(x, UserExpertEntity::getUserProfileEntity, UserProfileEntity::getUserId))
                .collect(Collectors.toSet());

        final List<String> keywords = userProfileEntity.getUserProfileKeywordEntities().stream()
                .filter(x -> !x.isDeleted())
                .map(UserProfileKeywordEntity::getTagEntity)
                .map(TagEntity::getValue)
                .collect(Collectors.toList());

        userProfileIndexDto.setUserId(userProfileEntity.getUserId());
        userProfileIndexDto.setName(userProfileEntity.getName());
        userProfileIndexDto.setUsername(userProfileEntity.getUsername());
        userProfileIndexDto.setEmail(userProfileEntity.getEmail());
        userProfileIndexDto.setDescription(userProfileEntity.getDescription());
        userProfileIndexDto.setPositionDescription(userProfileEntity.getPositionDescription());
        userProfileIndexDto.setOrganizationIds(organizationIds);
        userProfileIndexDto.setVerifiedOrganizationIds(verifiedOrganizationIds);
        userProfileIndexDto.setFavoriteOrganizationIds(favoriteOrganizationIds);
        userProfileIndexDto.setRetiredOrganizationIds(retiredOrganizationIds);
        userProfileIndexDto.setVisibleOrganizationIds(visibleOrganizationIds);
        userProfileIndexDto.setActiveOrganizationIds(activeOrganizationIds);
        userProfileIndexDto.setExpertIds(expertIds);
        userProfileIndexDto.setClaimable(userProfileEntity.isClaimable());
        userProfileIndexDto.setInvitation(userProfileEntity.isInvitation());
        userProfileIndexDto.setRetirementDate(userProfileEntity.getRetirementDate());
        userProfileIndexDto.setClaimedExpertIds(claimedExpertIds);
        userProfileIndexDto.setClaimedUserIds(claimedUserIds);
        userProfileIndexDto.setDescription(userProfileEntity.getDescription());
        userProfileIndexDto.setKeywords(KeywordsUtil.sortKeywordList(keywords));
        if (userProfileEntity.getClaimedBy() != null) {
            userProfileIndexDto.setClaimedById(userProfileEntity.getClaimedBy().getUserId());
        }
        return userProfileIndexDto;
    }

    private static Set<Long> createOrganizationIdSet(Set<UserOrganizationEntity> userOrganizationEntities, Predicate<UserOrganizationEntity> predicate) {
        return userOrganizationEntities.stream()
                .filter(x -> !x.isDeleted())
                .filter(predicate)
                .map(UserOrganizationEntity::getOrganizationEntity)
                .map(OrganizationUtil::findOrganizationIdAfterSubstitution)
                .collect(Collectors.toSet());
    }
}
