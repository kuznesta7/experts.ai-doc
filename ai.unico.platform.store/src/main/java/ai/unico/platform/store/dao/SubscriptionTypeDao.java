package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.SubscriptionTypeEntity;

import java.util.List;

public interface SubscriptionTypeDao {

    List<SubscriptionTypeEntity> getAllSubscriptionTypes();

    SubscriptionTypeEntity getSubscriptionType(Long id);

}
