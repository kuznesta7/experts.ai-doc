package ai.unico.platform.store.service;

import ai.unico.platform.search.api.service.ItemLookupService;
import ai.unico.platform.store.api.dto.ItemRegistrationDto;
import ai.unico.platform.store.api.service.ItemRegistrationService;
import ai.unico.platform.store.api.service.ProjectItemService;
import ai.unico.platform.store.dao.*;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ItemRegistrationServiceImpl implements ItemRegistrationService {

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private ItemTypeDao itemTypeDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private ItemOrganizationDao itemOrganizationDao;

    @Autowired
    private ProjectItemService projectItemService;

    @Autowired
    private UserExpertItemDao userExpertItemDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private TagDao tagDao;

    @Override
    @Transactional
    public Long registerItem(Long originalItemId, UserContext userContext) throws IOException, NonexistentEntityException {
        final ItemLookupService service = ServiceRegistryUtil.getService(ItemLookupService.class);
        final ItemRegistrationDto originalItem = service.findOriginalItem(originalItemId);
        return registerItem(originalItem, true, userContext);
    }

    @Override
    @Transactional
    public Long registerItem(ItemRegistrationDto itemRegistrationDto, boolean clean, UserContext userContext) {
        final ItemEntity itemEntity = new ItemEntity();
        itemEntity.setItemName(itemRegistrationDto.getItemName());
        itemEntity.setOrigItemId(itemRegistrationDto.getOriginalItemId());
        itemEntity.setItemDescription(itemRegistrationDto.getItemDescription());
        itemEntity.setOutdated(false);
        itemEntity.setYear(itemRegistrationDto.getYear());
        itemEntity.setConfidentiality(itemRegistrationDto.getConfidentiality());
        if (itemRegistrationDto.getItemTypeId() != null) {
            itemEntity.setItemTypeEntity(getItemType(itemRegistrationDto.getItemTypeId()));
        }

        TrackableUtil.fillCreate(itemEntity, userContext);
        itemDao.saveItem(itemEntity);

        final Set<Long> organizationIds = itemRegistrationDto.getOrganizationIds();
        for (Long organizationId : organizationIds) {
            final OrganizationEntity organizationEntity = getOrganization(organizationId);
            final ItemOrganizationEntity itemOrganizationEntity = new ItemOrganizationEntity();
            itemOrganizationEntity.setOrganizationEntity(organizationEntity);
            itemOrganizationEntity.setItemEntity(itemEntity);
            TrackableUtil.fillCreate(itemOrganizationEntity, userContext);
            itemOrganizationDao.saveItemOrganization(itemOrganizationEntity);
            itemEntity.getItemOrganizationEntities().add(itemOrganizationEntity);
        }

        for (String keyword : itemRegistrationDto.getKeywords()) {
            final ItemTagEntity itemTagEntity = new ItemTagEntity();
            final TagEntity tagEntity = getTag(keyword);
            itemTagEntity.setTagEntity(tagEntity);
            itemTagEntity.setItemEntity(itemEntity);
            itemDao.saveItemTag(itemTagEntity);
            itemEntity.getItemTagEntities().add(itemTagEntity);
        }

        final Set<Long> expertIds = itemRegistrationDto.getExpertIds();
        for (Long id : expertIds) {
            final UserExpertItemEntity expertItemEntity = new UserExpertItemEntity();
            expertItemEntity.setExpertId(id);
            expertItemEntity.setItemEntity(itemEntity);
            TrackableUtil.fillCreate(expertItemEntity, userContext);
            userExpertItemDao.saveUserItem(expertItemEntity);
            itemEntity.getUserItemEntities().add(expertItemEntity);
        }

        final Set<Long> userIds = itemRegistrationDto.getUserIds();
        for (Long id : userIds) {
            final UserExpertItemEntity expertItemEntity = new UserExpertItemEntity();
            final UserProfileEntity userProfile = userProfileDao.findUserProfile(id);
            expertItemEntity.setUserProfileEntity(userProfile);
            expertItemEntity.setItemEntity(itemEntity);
            TrackableUtil.fillCreate(expertItemEntity, userContext);
            userExpertItemDao.saveUserItem(expertItemEntity);
            itemEntity.getUserItemEntities().add(expertItemEntity);
        }

        if (clean) {
            projectItemService.updateProjectItems(Collections.singleton(itemRegistrationDto.getOriginalItemId()));
        }
        return itemEntity.getItemId();
    }

    private Map<Long, ItemTypeEntity> itemTypeCache = new HashMap<>();
    private Map<Long, OrganizationEntity> organizationCache = new HashMap<>();
    private Map<String, TagEntity> tagCache = new HashMap<>();

    private ItemTypeEntity getItemType(Long itemTypeId) {
        return itemTypeCache.computeIfAbsent(itemTypeId, itemTypeDao::find);
    }

    private OrganizationEntity getOrganization(Long organizationId) {
        return organizationCache.computeIfAbsent(organizationId, organizationDao::find);
    }

    private TagEntity getTag(String tagValue) {
        return tagCache.computeIfAbsent(tagValue, tagDao::findOrCreateTagByValues);
    }
}
