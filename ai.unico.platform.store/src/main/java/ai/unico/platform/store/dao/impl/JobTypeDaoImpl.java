package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.JobTypeDao;
import ai.unico.platform.store.entity.JobTypeEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class JobTypeDaoImpl implements JobTypeDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public List<JobTypeEntity> getAllJobTypes() {
        Criteria criteria = hibernateCriteriaCreator.createCriteria(JobTypeEntity.class, "jobType");
        criteria.add(Restrictions.eq("deleted", false));
        return criteria.list();
    }

    @Override
    public JobTypeEntity getJobTypeEntity(Long id) {
        return entityManager.find(JobTypeEntity.class, id);
    }

    @Override
    public void save(JobTypeEntity jobTypeEntity) {
        entityManager.persist(jobTypeEntity);
    }
}
