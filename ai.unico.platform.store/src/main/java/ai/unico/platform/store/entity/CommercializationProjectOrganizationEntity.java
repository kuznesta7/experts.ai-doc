package ai.unico.platform.store.entity;

import ai.unico.platform.store.api.enums.CommercializationOrganizationRelation;
import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class CommercializationProjectOrganizationEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commercializationProjectOrganizationId;

    @ManyToOne
    @JoinColumn(name = "commercializationId")
    private CommercializationProjectEntity commercializationProjectEntity;

    @ManyToOne
    @JoinColumn(name = "organizationId")
    private OrganizationEntity organizationEntity;

    @Enumerated(EnumType.STRING)
    private CommercializationOrganizationRelation relation;

    private boolean deleted;

    public Long getCommercializationProjectOrganizationId() {
        return commercializationProjectOrganizationId;
    }

    public void setCommercializationProjectOrganizationId(Long commercializationProjectOrganizationId) {
        this.commercializationProjectOrganizationId = commercializationProjectOrganizationId;
    }

    public CommercializationProjectEntity getCommercializationProjectEntity() {
        return commercializationProjectEntity;
    }

    public void setCommercializationProjectEntity(CommercializationProjectEntity commercializationProjectEntity) {
        this.commercializationProjectEntity = commercializationProjectEntity;
    }

    public OrganizationEntity getOrganizationEntity() {
        return organizationEntity;
    }

    public void setOrganizationEntity(OrganizationEntity organizationEntity) {
        this.organizationEntity = organizationEntity;
    }

    public CommercializationOrganizationRelation getRelation() {
        return relation;
    }

    public void setRelation(CommercializationOrganizationRelation relation) {
        this.relation = relation;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
