package ai.unico.platform.store.generator;

import ai.unico.platform.store.entity.EquipmentEntity;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import org.hibernate.type.LongType;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.Properties;

public class EquipmentSequenceGenerator extends SequenceStyleGenerator {
    public static final String EQUIPMENT_SEQ = "equipment_equipment_id_seq";
    @Override
    public Serializable generate(SessionImplementor session, Object object) throws HibernateException {
        if(object instanceof EquipmentEntity) {
            EquipmentEntity equipment = (EquipmentEntity) object;
            if (equipment.getEquipmentId() == null || equipment.getEquipmentId() == -1L) {
                equipment.setEquipmentId((Long) super.generate(session, EQUIPMENT_SEQ));
            }
            return equipment.getEquipmentId();
        }
        return super.generate(session, object);
    }
    @Override
    public void configure(Type type, Properties params, Dialect dialect) throws MappingException {
        super.configure(LongType.INSTANCE, params, dialect);
    }
}
