package ai.unico.platform.store.service;

import ai.unico.platform.search.api.service.ItemLookupService;
import ai.unico.platform.store.api.dto.ItemDetailDto;
import ai.unico.platform.store.api.dto.ProjectDto;
import ai.unico.platform.store.api.enums.WidgetType;
import ai.unico.platform.store.api.service.OrganizationService;
import ai.unico.platform.store.api.service.StoreSecurityHelperService;
import ai.unico.platform.store.api.service.WidgetService;
import ai.unico.platform.store.dao.CommercializationProjectDao;
import ai.unico.platform.store.dao.ItemDao;
import ai.unico.platform.store.dao.ProjectDao;
import ai.unico.platform.store.dao.SearchHistoryDao;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.enums.OrganizationRole;
import ai.unico.platform.util.enums.Role;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class StoreSecurityHelperServiceImpl implements StoreSecurityHelperService {

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private SearchHistoryDao searchHistoryDao;

    @Autowired
    private CommercializationProjectDao commercializationProjectDao;

    @Override
    @Transactional
    public Boolean isUserOwnerOfSearchHistoryItem(Long searchHistId, UserContext userContext) {
        final SearchHistEntity searchHistory = searchHistoryDao.findSearchHistory(searchHistId);
        return searchHistory.getUserInsert().equals(userContext.getUserId());
    }

    @Override
    @Transactional
    public Boolean canUserManageItemOrganization(Long itemId, Long organizationId, UserContext userContext) {
        final ItemEntity itemEntity = itemDao.find(itemId);
        return canEditItemEntity(itemEntity, false, userContext);
    }

    @Override
    @Transactional
    public Boolean canUserManageItemExperts(Long itemId, Long userId, UserContext userContext) {
        final ItemEntity itemEntity = itemDao.find(itemId);
        if (itemEntity == null) throw new NonexistentEntityException(ItemEntity.class);
        final Confidentiality confidentiality = itemEntity.getConfidentiality();
        if (confidentiality.equals(Confidentiality.PUBLIC) && userContext.getUserId().equals(userId)) return true;
        return canEditItemEntity(itemEntity, false, userContext);
    }

    @Override
    @Transactional
    public Boolean canUserSeeProject(Long projectId, UserContext userContext) {
        final ProjectEntity projectEntity = projectDao.find(projectId);
        if (projectEntity == null) throw new NonexistentEntityException(ProjectEntity.class);
        final Confidentiality confidentiality = projectEntity.getConfidentiality();
        final Long ownerOrganizationId = HibernateUtil.getId(projectEntity, ProjectEntity::getOwnerOrganization, OrganizationEntity::getOrganizationId);
        switch (confidentiality) {
            case PUBLIC:
                return true;
            case CONFIDENTIAL:
                final Set<ProjectOrganizationEntity> projectOrganizationEntities = projectEntity.getProjectOrganizationEntities();
                return projectOrganizationEntities.stream()
                        .filter(po -> !po.isDeleted())
                        .map(po -> HibernateUtil.getId(po, ProjectOrganizationEntity::getOrganizationEntity, OrganizationEntity::getOrganizationId))
                        .anyMatch(oid -> hasOrganizationRoles(oid, userContext, OrganizationRole.ORGANIZATION_VIEWER, OrganizationRole.ORGANIZATION_EDITOR));
            case SECRET:
            default:
                return hasOrganizationRoles(ownerOrganizationId, userContext, OrganizationRole.ORGANIZATION_EDITOR);
        }
    }

    @Override
    @Transactional
    public Boolean canUserEditProject(ProjectDto projectDto, UserContext userContext) {
        final Confidentiality newConfidentiality = projectDto.getConfidentiality();
        final ProjectEntity projectEntity = projectDao.find(projectDto.getProjectId());
        if (projectEntity == null) throw new NonexistentEntityException(ProjectEntity.class);
        final Confidentiality confidentiality = projectEntity.getConfidentiality();
        final boolean confidentialityChanged = !confidentiality.equals(newConfidentiality);
        return canUserEditProjectEntity(projectEntity, confidentialityChanged, userContext);
    }

    @Override
    @Transactional
    public Boolean canUserEditProject(Long projectId, UserContext userContext) {
        final ProjectEntity projectEntity = projectDao.find(projectId);
        return canUserEditProjectEntity(projectEntity, false, userContext);
    }

    @Override
    @Transactional
    public Boolean canUserSeeCommercializationProjectDetail(Long commercializationId, UserContext userContext) {
        final CommercializationProjectEntity commercializationProjectEntity = commercializationProjectDao.find(commercializationId);
        final Long ownerOrganizationId = HibernateUtil.getId(commercializationProjectEntity, CommercializationProjectEntity::getOwnerOrganizationEntity, OrganizationEntity::getOrganizationId);
        if (hasOrganizationRoles(ownerOrganizationId, userContext, OrganizationRole.ORGANIZATION_VIEWER, OrganizationRole.ORGANIZATION_EDITOR))
            return true;
        final List<CommercializationProjectStatusEntity> statusHistory = commercializationProjectDao.findStatusHistory(commercializationId);
        final CommercializationProjectStatusEntity lastProjectStatus = TrackableUtil.findLast(statusHistory);
        if (lastProjectStatus.getCommercializationStatusTypeEntity().isUseInSearch()) return true;
        return lastProjectStatus.getCommercializationStatusTypeEntity().isUseInInvest() && userContext.getRoles().contains(Role.COMMERCIALIZATION_INVESTOR);
    }

    @Override
    public Boolean canUserEditCommercializationProject(Long commercializationId, UserContext userContext) {
        final CommercializationProjectEntity commercializationProjectEntity = commercializationProjectDao.find(commercializationId);
        final Long ownerOrganizationId = HibernateUtil.getId(commercializationProjectEntity, CommercializationProjectEntity::getOwnerOrganizationEntity, OrganizationEntity::getOrganizationId);
        return (hasOrganizationRoles(ownerOrganizationId, userContext, OrganizationRole.ORGANIZATION_EDITOR));
    }

    @Override
    public Boolean doesUserOwnOriginalItem(Long originalItemId, UserContext userContext) throws IOException {
        final ItemLookupService service = ServiceRegistryUtil.getService(ItemLookupService.class);
        return service.findOriginalItem(originalItemId).getUserIds().contains(userContext.getUserId());
    }

    @Override
    public Boolean hasOrganizationPublicProfile(Long organizationId) {
        final OrganizationService service = ServiceRegistryUtil.getService(OrganizationService.class);
        return service.hasPublicProfile(organizationId);
    }

    @Override
    @Transactional
    public Boolean canUserSeeItem(Long itemId, UserContext userContext) {
        final ItemEntity itemEntity = itemDao.find(itemId);
        if (itemEntity == null) throw new NonexistentEntityException(ItemEntity.class);
        final Confidentiality confidentiality = itemEntity.getConfidentiality();
        final Long ownerOrganizationId = HibernateUtil.getId(itemEntity, ItemEntity::getOwnerOrganization, OrganizationEntity::getOrganizationId);
        switch (confidentiality) {
            case PUBLIC:
                return true;
            case CONFIDENTIAL:
                final Set<ItemOrganizationEntity> itemOrganizationEntities = itemEntity.getItemOrganizationEntities();
                return (itemOrganizationEntities.stream()
                        .filter(io -> !io.isDeleted())
                        .map(io -> HibernateUtil.getId(io, ItemOrganizationEntity::getOrganizationEntity, OrganizationEntity::getOrganizationId))
                        .anyMatch(orId -> hasOrganizationRoles(orId, userContext, OrganizationRole.ORGANIZATION_VIEWER, OrganizationRole.ORGANIZATION_EDITOR)));
            case SECRET:
            default:
                return hasOrganizationRoles(ownerOrganizationId, userContext, OrganizationRole.ORGANIZATION_EDITOR);
        }
    }

    @Override
    @Transactional
    public Boolean canUserEditItem(ItemDetailDto itemDetailDto, UserContext userContext) {
        final ItemEntity itemEntity = itemDao.find(itemDetailDto.getItemId());
        final Confidentiality confidentiality = itemEntity.getConfidentiality();
        final boolean confidentialityChanged = !confidentiality.equals(itemDetailDto.getConfidentiality());
        return canEditItemEntity(itemEntity, confidentialityChanged, userContext);
    }


    @Override
    public Boolean hasWidget(Long organizationId, WidgetType widgetType) {
        final WidgetService service = ServiceRegistryUtil.getService(WidgetService.class);
        return service.isAllowed(organizationId, widgetType);
    }

    private boolean canEditItemEntity(ItemEntity itemEntity, boolean confidentialityChanged, UserContext userContext) {
        if (itemEntity == null) throw new NonexistentEntityException(ItemEntity.class);
        final Confidentiality confidentiality = itemEntity.getConfidentiality();
        final Long ownerUserId = HibernateUtil.getId(itemEntity, ItemEntity::getOwnerUser, UserProfileEntity::getUserId);
        final Long ownerOrganizationId = HibernateUtil.getId(itemEntity, ItemEntity::getOwnerOrganization, OrganizationEntity::getOrganizationId);
        switch (confidentiality) {
            case PUBLIC:
                if (userContext.getUserId().equals(ownerUserId)) return true;
            case CONFIDENTIAL:
                final Set<ItemOrganizationEntity> itemOrganizationEntities = itemEntity.getItemOrganizationEntities();
                if (!confidentialityChanged) {
                    return (itemOrganizationEntities.stream()
                            .filter(io -> !io.isDeleted())
                            .map(io -> HibernateUtil.getId(io, ItemOrganizationEntity::getOrganizationEntity, OrganizationEntity::getOrganizationId))
                            .anyMatch(orId -> hasOrganizationRoles(orId, userContext, OrganizationRole.ORGANIZATION_EDITOR)));
                }
            case SECRET:
            default:
                return hasOrganizationRoles(ownerOrganizationId, userContext, OrganizationRole.ORGANIZATION_EDITOR);
        }
    }

    private boolean hasOrganizationRoles(Long organizationId, UserContext userContext, OrganizationRole... organizationRoles) {
        if (userContext == null) return false;
        return userContext.getOrganizationRoles().stream()
                .anyMatch(organization -> organizationId.equals(organization.getOrganizationId()) && organization.getRoles().stream()
                        .anyMatch(userRole -> userRole.isValid() && Arrays.stream(organizationRoles)
                                .anyMatch(availableRole -> availableRole.equals(userRole.getRole()))));
    }

    private boolean canUserEditProjectEntity(ProjectEntity projectEntity, boolean confidentialityChanged, UserContext userContext) {
        if (projectEntity == null) throw new NonexistentEntityException(ProjectEntity.class);
        final Confidentiality confidentiality = projectEntity.getConfidentiality();
        final Long ownerOrganizationId = HibernateUtil.getId(projectEntity, ProjectEntity::getOwnerOrganization, OrganizationEntity::getOrganizationId);
        switch (confidentiality) {
            case PUBLIC:
            case CONFIDENTIAL:
                final Set<ProjectOrganizationEntity> projectOrganizationEntities = projectEntity.getProjectOrganizationEntities();
                if (!confidentialityChanged)
                    return projectOrganizationEntities.stream()
                            .filter(po -> !po.isDeleted())
                            .map(po -> HibernateUtil.getId(po, ProjectOrganizationEntity::getOrganizationEntity, OrganizationEntity::getOrganizationId))
                            .anyMatch(oid -> hasOrganizationRoles(oid, userContext, OrganizationRole.ORGANIZATION_EDITOR));
            case SECRET:
            default:
                return hasOrganizationRoles(ownerOrganizationId, userContext, OrganizationRole.ORGANIZATION_EDITOR);
        }
    }
}
