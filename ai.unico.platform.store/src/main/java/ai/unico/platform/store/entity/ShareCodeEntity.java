package ai.unico.platform.store.entity;

import ai.unico.platform.store.api.enums.ShareCodeType;
import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class ShareCodeEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long shareCodeId;

    private String code;

    @Enumerated(EnumType.STRING)
    private ShareCodeType type;

    private String sharedEntityId;

    private boolean deleted;

    public Long getShareCodeId() {
        return shareCodeId;
    }

    public void setShareCodeId(Long shareCodeId) {
        this.shareCodeId = shareCodeId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ShareCodeType getType() {
        return type;
    }

    public void setType(ShareCodeType type) {
        this.type = type;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getSharedEntityId() {
        return sharedEntityId;
    }

    public void setSharedEntityId(String sharedEntityId) {
        this.sharedEntityId = sharedEntityId;
    }
}
