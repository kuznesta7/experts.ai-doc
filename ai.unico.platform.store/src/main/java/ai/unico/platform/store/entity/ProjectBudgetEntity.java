package ai.unico.platform.store.entity;

import ai.unico.platform.store.api.enums.Currency;
import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class ProjectBudgetEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long projectBudgetId;

    private Integer year;

    private Double totalAmount = 0D;

    private Double nationalSupportAmount = 0D;

    private Double privateAmount = 0D;

    private Double otherAmount = 0D;

    private boolean deleted;

    @Enumerated(EnumType.STRING)
    private Currency currency;

    @ManyToOne
    private OrganizationEntity organizationEntity;

    @ManyToOne
    private ProjectEntity projectEntity;

    public Long getProjectBudgetId() {
        return projectBudgetId;
    }

    public void setProjectBudgetId(Long projectBudgetId) {
        this.projectBudgetId = projectBudgetId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getNationalSupportAmount() {
        return nationalSupportAmount;
    }

    public void setNationalSupportAmount(Double nationalSupportAmount) {
        this.nationalSupportAmount = nationalSupportAmount;
    }

    public Double getPrivateAmount() {
        return privateAmount;
    }

    public void setPrivateAmount(Double privateAmount) {
        this.privateAmount = privateAmount;
    }

    public Double getOtherAmount() {
        return otherAmount;
    }

    public void setOtherAmount(Double otherAmount) {
        this.otherAmount = otherAmount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public ProjectEntity getProjectEntity() {
        return projectEntity;
    }

    public void setProjectEntity(ProjectEntity projectEntity) {
        this.projectEntity = projectEntity;
    }

    public OrganizationEntity getOrganizationEntity() {
        return organizationEntity;
    }

    public void setOrganizationEntity(OrganizationEntity organizationEntity) {
        this.organizationEntity = organizationEntity;
    }
}
