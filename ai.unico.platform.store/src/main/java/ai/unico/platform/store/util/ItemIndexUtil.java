package ai.unico.platform.store.util;

import ai.unico.platform.search.api.service.ItemIndexService;
import ai.unico.platform.store.api.dto.ItemIndexDto;
import ai.unico.platform.store.entity.ItemEntity;
import ai.unico.platform.util.ServiceRegistryUtil;

import java.io.IOException;

public class ItemIndexUtil {

    public static void indexItem(ItemEntity itemEntity) {
        final ItemIndexService service = ServiceRegistryUtil.getService(ItemIndexService.class);
        final ItemIndexDto itemIndexDto = UserItemUtil.convertToIndex(itemEntity);
        try {
            service.indexItem(itemIndexDto);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void indexItem(ItemEntity itemEntity, boolean flush) {
        indexItem(itemEntity, false);
    }
}
