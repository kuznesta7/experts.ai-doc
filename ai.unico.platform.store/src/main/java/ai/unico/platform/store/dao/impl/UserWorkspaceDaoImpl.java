package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.UserWorkspaceDao;
import ai.unico.platform.store.entity.FollowedProfileWorkspaceEntity;
import ai.unico.platform.store.entity.SearchHistWorkspaceEntity;
import ai.unico.platform.store.entity.UserWorkspaceEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class UserWorkspaceDaoImpl implements UserWorkspaceDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public UserWorkspaceEntity findUserWorkspace(Long userWorkspaceId) {
        return entityManager.find(UserWorkspaceEntity.class, userWorkspaceId);
    }

    @Override
    public UserWorkspaceEntity findUserWorkspace(Long userId, String workspaceName) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserWorkspaceEntity.class, "workspace");
        criteria.add(Restrictions.eq("workspaceName", workspaceName));
        criteria.add(Restrictions.eq("userProfileEntity.userId", userId));
        return (UserWorkspaceEntity) criteria.uniqueResult();
    }

    @Override
    public List<UserWorkspaceEntity> findWorkspacesForUser(Long userId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserWorkspaceEntity.class, "workspace");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("userProfileEntity.userId", userId));
        return criteria.list();
    }

    @Override
    public void save(FollowedProfileWorkspaceEntity followedProfileWorkspaceEntity) {
        entityManager.persist(followedProfileWorkspaceEntity);
    }

    @Override
    public void save(SearchHistWorkspaceEntity searchHistWorkspaceEntity) {
        entityManager.persist(searchHistWorkspaceEntity);
    }

    @Override
    public void save(UserWorkspaceEntity userWorkspaceEntity) {
        entityManager.persist(userWorkspaceEntity);
    }
}
