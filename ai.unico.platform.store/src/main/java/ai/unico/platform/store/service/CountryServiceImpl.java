package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.CountryDto;
import ai.unico.platform.store.api.service.CountryService;
import ai.unico.platform.store.dao.CountryDao;
import ai.unico.platform.store.entity.CountryEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryDao countryDao;

    @Override
    @Transactional
    public List<CountryDto> findAvailableCountries() {
        final List<CountryEntity> allCountries = countryDao.findAllCountries();
        final List<CountryDto> countryDtos = new ArrayList<>();
        for (CountryEntity countryEntity : allCountries) {
            if (!countryEntity.isPublished()) continue;
            final CountryDto countryDto = new CountryDto();
            countryDto.setCountryCode(countryEntity.getCountryCode());
            countryDto.setCountryAbbrev(countryEntity.getCountryAbbrev());
            countryDto.setCountryName(countryEntity.getCountryName());
            countryDtos.add(countryDto);
        }
        return countryDtos;
    }
}
