package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.InteractionIndividualDto;
import ai.unico.platform.store.api.dto.InteractionWidgetDto;
import ai.unico.platform.store.api.service.InteractionService;
import ai.unico.platform.store.dao.InteractionIndividualDao;
import ai.unico.platform.store.dao.InteractionWidgetDao;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.InteractionIndividualEntity;
import ai.unico.platform.store.entity.InteractionWidgetEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;


public class InteractionServiceImpl implements InteractionService {

    @Autowired
    private InteractionIndividualDao interactionIndividualDao;

    @Autowired
    private InteractionWidgetDao interactionWidgetDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Override
    @Async
    @Transactional
    public void saveInteraction(InteractionIndividualDto interactionIndividualDto) {
        interactionIndividualDao.save(convert(interactionIndividualDto));
    }

    @Override
    @Async
    @Transactional
    public void saveInteraction(InteractionWidgetDto interactionWidgetDto) {
        interactionWidgetDao.save(convert(interactionWidgetDto));
    }

    private InteractionIndividualEntity convert(InteractionIndividualDto interactionIndividualDto) {
        InteractionIndividualEntity entity = new InteractionIndividualEntity();
        entity.setSessionId(interactionIndividualDto.getSessionId());
        entity.setVisitedExpertCode(interactionIndividualDto.getVisitedExpertCode());
        entity.setVisitedOutcomeCode(interactionIndividualDto.getVisitedOutcomeCode());
        entity.setVisitedProjectCode(interactionIndividualDto.getVisitedProjectCode());
        entity.setVisitedCommercializationId(interactionIndividualDto.getVisitedCommercializationId());
        entity.setOrganizationId(interactionIndividualDto.getOrganization());
        entity.setVisitedLectureCode(interactionIndividualDto.getVisitedLectureCode());
        return entity;
    }

    private InteractionWidgetEntity convert(InteractionWidgetDto interactionWidgetDto) {
        InteractionWidgetEntity entity = new InteractionWidgetEntity();
        entity.setSessionId(interactionWidgetDto.getSessionId());
        entity.setInteractionType(interactionWidgetDto.getInteractionType());
        if (interactionWidgetDto.getUser() != null)
            entity.setUserProfileEntity(userProfileDao.findUserProfile(interactionWidgetDto.getUser()));
        if (interactionWidgetDto.getOrganization() != null)
            entity.setOrganization(organizationDao.find(interactionWidgetDto.getOrganization()));
        entity.setInteractionDetails(interactionWidgetDto.getInteractionDetails());
        return entity;
    }
}
