package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.CommercializationStatusDao;
import ai.unico.platform.store.entity.CommercializationStatusCategoryEntity;
import ai.unico.platform.store.entity.CommercializationStatusTypeEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.util.enums.CommercializationStatus;
import ai.unico.platform.util.enums.OrderDirection;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class CommercializationStatusDaoImpl implements CommercializationStatusDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public CommercializationStatusTypeEntity find(Long statusTypeId) {
        return entityManager.find(CommercializationStatusTypeEntity.class, statusTypeId);
    }

    @Override
    public CommercializationStatusTypeEntity find(CommercializationStatus commercializationStatus) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(CommercializationStatusTypeEntity.class, "status");
        criteria.add(Restrictions.eq("code", commercializationStatus.getStatusCode()));
        return (CommercializationStatusTypeEntity) criteria.uniqueResult();
    }

    @Override
    public List<CommercializationStatusTypeEntity> findAll() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(CommercializationStatusTypeEntity.class, "status");
        HibernateUtil.setOrder(criteria, "displayOrder", OrderDirection.ASC);
        return criteria.list();
    }

    @Override
    public void save(CommercializationStatusTypeEntity commercializationStatusTypeEntity) {
        entityManager.persist(commercializationStatusTypeEntity);
    }

    @Override
    public void save(CommercializationStatusCategoryEntity commercializationStatusCategoryEntity) {
        entityManager.persist(commercializationStatusCategoryEntity);
    }
}
