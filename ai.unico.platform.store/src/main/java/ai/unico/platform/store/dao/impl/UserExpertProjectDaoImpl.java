package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.api.service.ClaimProfileService;
import ai.unico.platform.store.dao.UserExpertProjectDao;
import ai.unico.platform.store.entity.UserExpertProjectEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class UserExpertProjectDaoImpl implements UserExpertProjectDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Autowired
    private ClaimProfileService claimProfileService;

    @Override
    public UserExpertProjectEntity findForUser(Long userId, Long projectId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserExpertProjectEntity.class, "projectUser");
        final Set<ClaimProfileService.Claim> claimsForUser = claimProfileService.findClaimsForUser(userId);
        final Set<Long> expertIds = claimsForUser.stream().map(ClaimProfileService.Claim::getExpertId).filter(Objects::nonNull).collect(Collectors.toSet());
        final Set<Long> userIds = claimsForUser.stream().map(ClaimProfileService.Claim::getUserId).filter(Objects::nonNull).collect(Collectors.toSet());
        criteria.add(Restrictions.eq("projectUser.deleted", false));
        criteria.add(Restrictions.eq("projectUser.projectEntity.projectId", projectId));
        if (!expertIds.isEmpty() || !userIds.isEmpty()) {
            final Disjunction disjunction = Restrictions.disjunction();
            if (!expertIds.isEmpty()) {
                disjunction.add(Restrictions.in("projectUser.expertId", expertIds));
            }
            if (!userIds.isEmpty()) {
                disjunction.add(Restrictions.in("projectUser.userProfileEntity.userId", userIds));
            }
            criteria.add(disjunction);
        }
        return (UserExpertProjectEntity) criteria.uniqueResult();
    }

    @Override
    public UserExpertProjectEntity findForExpert(Long expertId, Long projectId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserExpertProjectEntity.class, "projectUser");
        criteria.add(Restrictions.eq("projectUser.deleted", false));
        criteria.add(Restrictions.eq("projectUser.projectEntity.projectId", projectId));
        criteria.add(Restrictions.eq("projectUser.expertId", expertId));
        return (UserExpertProjectEntity) criteria.uniqueResult();
    }

    @Override
    public void saveProjectUserExpert(UserExpertProjectEntity userExpertProjectEntity) {
        entityManager.persist(userExpertProjectEntity);
    }
}
