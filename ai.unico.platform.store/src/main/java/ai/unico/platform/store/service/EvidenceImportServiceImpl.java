package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.EvidenceImportDto;
import ai.unico.platform.store.api.enums.EvidenceCategory;
import ai.unico.platform.store.api.service.EvidenceImportService;
import ai.unico.platform.store.api.service.FileService;
import ai.unico.platform.store.api.service.email.EmailService;
import ai.unico.platform.store.dao.EvidenceImportDao;
import ai.unico.platform.store.dao.FileDao;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.EvidenceImportEntity;
import ai.unico.platform.store.entity.FileEntity;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.ume.api.dto.UserDto;
import ai.unico.platform.ume.api.service.UserService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.enums.Role;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EvidenceImportServiceImpl implements EvidenceImportService {

    @Value("${env.host}")
    private String host;

    @Autowired
    private EvidenceImportDao evidenceImportDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private FileDao fileDao;

    @Autowired
    private FileService fileService;

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private EmailService emailService;

    @Override
    @Transactional
    public void uploadImportFile(Long organizationId, EvidenceCategory evidenceCategory, FileDto fileDto, String note, UserContext userContext) {
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        final FileEntity fileEntity = new FileEntity();
        fileEntity.setContent(fileDto.getContent());
        fileEntity.setName(fileDto.getName());
        TrackableUtil.fillCreate(fileEntity, userContext);
        fileDao.save(fileEntity);

        final EvidenceImportEntity evidenceImportEntity = new EvidenceImportEntity();
        evidenceImportEntity.setOrganizationEntity(organizationEntity);
        evidenceImportEntity.setEvidenceCategory(evidenceCategory);
        evidenceImportEntity.setNote(note);
        evidenceImportEntity.setFileEntity(fileEntity);
        TrackableUtil.fillCreate(evidenceImportEntity, userContext);
        evidenceImportDao.save(evidenceImportEntity);

        final UserService userService = ServiceRegistryUtil.getService(UserService.class);
        final List<UserDto> usersWithRole = userService.findUsersWithRole(Role.DATA_IMPORTER);
        final List<String> emails = usersWithRole.stream().map(UserDto::getEmail).collect(Collectors.toList());
        if (emails.isEmpty()) return;
        final Map<String, String> variables = new HashMap<>();
        variables.put("url", host + "evidence/organizations/" + organizationId + "/imports/" + evidenceImportEntity.getEvidenceImportId());
        variables.put("organizationName", organizationEntity.getOrganizationName());
        final String to = String.join(",", emails);
        emailService.sendEmailWithTemplate(to, "new-evidence-import-request", variables);
    }

    @Override
    @Transactional
    public EvidenceImportDto getImportDetail(Long evidenceImportId, Long organizationId, UserContext userContext) throws UserUnauthorizedException {
        final EvidenceImportEntity evidenceImportEntity = doGetEvidenceImportEntity(evidenceImportId, organizationId);
        final EvidenceImportDto evidenceImportDto = new EvidenceImportDto();
        evidenceImportDto.setEvidenceImportId(evidenceImportEntity.getEvidenceImportId());
        evidenceImportDto.setOrganizationId(organizationId);
        evidenceImportDto.setOrganizationName(evidenceImportEntity.getOrganizationEntity().getOrganizationName());
        evidenceImportDto.setEvidenceCategory(evidenceImportEntity.getEvidenceCategory());
        evidenceImportDto.setNote(evidenceImportEntity.getNote());
        evidenceImportDto.setFileUploadedDate(evidenceImportEntity.getDateInsert());
        final Long userId = evidenceImportEntity.getUserInsert();
        final UserProfileEntity userProfile = userProfileDao.findUserProfile(userId);
        if (userProfile != null) {
            evidenceImportDto.setUserImportedId(userId);
            evidenceImportDto.setUserImportedName(userProfile.getName());
            evidenceImportDto.setEmail(userProfile.getEmail());
        }
        final FileEntity fileEntity = evidenceImportEntity.getFileEntity();
        if (fileEntity != null) {
            evidenceImportDto.setFileName(fileEntity.getName());
            evidenceImportDto.setFileSize(fileEntity.getSize());
            evidenceImportDto.setDeleted(fileEntity.isDeleted());
        }
        return evidenceImportDto;
    }

    @Override
    @Transactional
    public FileDto getImportFile(Long evidenceImportId, Long organizationId, UserContext userContext) throws UserUnauthorizedException {
        final EvidenceImportEntity evidenceImportEntity = doGetEvidenceImportEntity(evidenceImportId, organizationId);
        final Long fileId = HibernateUtil.getId(evidenceImportEntity, EvidenceImportEntity::getFileEntity, FileEntity::getFileId);
        return fileService.getFile(fileId, userContext);
    }

    @Override
    @Transactional
    public void deleteImport(Long evidenceImportId, Long organizationId, UserContext userContext) throws UserUnauthorizedException {
        final EvidenceImportEntity evidenceImportEntity = doGetEvidenceImportEntity(evidenceImportId, organizationId);
        evidenceImportEntity.getFileEntity().setContent(null);
        evidenceImportEntity.getFileEntity().setDeleted(true);
        TrackableUtil.fillUpdate(evidenceImportEntity, userContext);
        TrackableUtil.fillUpdate(evidenceImportEntity.getFileEntity(), userContext);
    }

    private EvidenceImportEntity doGetEvidenceImportEntity(Long evidenceImportId, Long organizationId) throws UserUnauthorizedException {
        final EvidenceImportEntity evidenceImportEntity = evidenceImportDao.getEvidenceImportEntity(evidenceImportId);
        if (evidenceImportEntity == null) throw new NonexistentEntityException(EvidenceImportEntity.class);
        final Long importOrganizationId = HibernateUtil.getId(evidenceImportEntity, EvidenceImportEntity::getOrganizationEntity, OrganizationEntity::getOrganizationId);
        if (!importOrganizationId.equals(organizationId)) throw new UserUnauthorizedException();
        return evidenceImportEntity;
    }
}
