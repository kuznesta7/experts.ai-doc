package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.ProjectDto;
import ai.unico.platform.store.api.dto.ProjectExpertDto;
import ai.unico.platform.store.api.dto.ProjectUserDto;
import ai.unico.platform.store.api.service.ProjectExpertService;
import ai.unico.platform.store.api.service.ProjectUserService;
import ai.unico.platform.store.dao.ProjectDao;
import ai.unico.platform.store.dao.UserExpertProjectDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.ProjectEntity;
import ai.unico.platform.store.entity.UserExpertProjectEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.util.CollectionsUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class ProjectUserServiceImpl implements ProjectUserService {

    @Autowired
    private UserExpertProjectDao projectUserDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private ProjectUserService projectUserService;

    @Autowired
    private ProjectExpertService projectExpertService;

    @Override
    @Transactional
    public void saveUserToProject(ProjectUserDto projectUserDto, UserContext userContext) throws NonexistentEntityException {
        final Long userId = projectUserDto.getUserId();
        final Long projectId = projectUserDto.getProjectId();
        final String label = projectUserDto.getLabel();
        UserExpertProjectEntity userExpertProjectEntity = projectUserDao.findForUser(userId, projectId);
        if (userExpertProjectEntity == null) {
            userExpertProjectEntity = new UserExpertProjectEntity();
            final UserProfileEntity userProfile = userProfileDao.findUserProfile(userId);
            final ProjectEntity projectEntity = projectDao.find(projectId);
            if (userProfile == null) throw new NonexistentEntityException(UserProfileEntity.class);
            if (projectEntity == null) throw new NonexistentEntityException(ProjectEntity.class);
            userExpertProjectEntity.setUserProfileEntity(userProfile);
            userExpertProjectEntity.setProjectEntity(projectEntity);
            TrackableUtil.fillCreate(userExpertProjectEntity, userContext);
        } else {
            TrackableUtil.fillUpdate(userExpertProjectEntity, userContext);
        }
        userExpertProjectEntity.setLabel(label);
        userExpertProjectEntity.setDeleted(false);
        projectUserDao.saveProjectUserExpert(userExpertProjectEntity);
    }

    @Override
    @Transactional
    public void removeUserFromProject(ProjectUserDto projectUserDto, UserContext userContext) throws NonexistentEntityException {
        final Long userId = projectUserDto.getUserId();
        final Long projectId = projectUserDto.getProjectId();
        final UserExpertProjectEntity existingEntity = projectUserDao.findForUser(userId, projectId);
        if (existingEntity == null) throw new NonexistentEntityException(UserExpertProjectEntity.class);
        existingEntity.setDeleted(true);
        TrackableUtil.fillUpdate(existingEntity, userContext);
    }

    @Override
    @Transactional
    public void switchUsers(ProjectDto projectDto, UserContext userContext) throws NonexistentEntityException {
        ProjectEntity projectEntity = projectDao.find(projectDto.getProjectId());
        Set<Long> entityUsers = projectEntity.getProjectUserEntities().stream()
                .filter(x -> ! x.isDeleted())
                .map(UserExpertProjectEntity::getUserProfileEntity)
                .filter(Objects::nonNull)
                .map(UserProfileEntity::getUserId)
                .collect(Collectors.toSet());
        Set<Long> entityExperts = projectEntity.getProjectUserEntities().stream()
                .filter(x -> ! x.isDeleted())
                .map(UserExpertProjectEntity::getExpertId)
                .collect(Collectors.toSet());
        Set<Long> dtoExperts = projectDto.getExpertIds();
        Set<Long> dtoUsers = projectDto.getUserIds();

        Set<Long> usersToRemove = CollectionsUtil.setDifference(entityUsers, dtoUsers);
        Set<Long> usersToAdd = CollectionsUtil.setDifference(dtoUsers, entityUsers);
        Set<Long> expertsToRemove = CollectionsUtil.setDifference(entityExperts, dtoExperts);
        Set<Long> expertsToAdd = CollectionsUtil.setDifference(dtoExperts, entityExperts);

        for (Long id: usersToAdd) projectUserService.saveUserToProject(toProjectUserDto(id, projectDto.getProjectId()), userContext);
        for (Long id: usersToRemove) projectUserService.removeUserFromProject(toProjectUserDto(id, projectDto.getProjectId()), userContext);
        for (Long id: expertsToAdd) projectExpertService.saveExpertToProject(toProjectExpertDto(id, projectDto.getProjectId()), userContext);
        for (Long id: expertsToRemove) projectExpertService.removeExpertFromProject(toProjectExpertDto(id, projectDto.getProjectId()), userContext);
    }

    private ProjectUserDto toProjectUserDto(Long userId, Long projectId) {
        ProjectUserDto projectUserDto = new ProjectUserDto();
        projectUserDto.setProjectId(projectId);
        projectUserDto.setUserId(userId);
        return projectUserDto;
    }

    private ProjectExpertDto toProjectExpertDto(Long expertId, Long projectId) {
        ProjectExpertDto projectExpertDto = new ProjectExpertDto();
        projectExpertDto.setProjectId(projectId);
        projectExpertDto.setExpertId(expertId);
        return projectExpertDto;
    }
}
