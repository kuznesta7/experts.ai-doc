package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.UserPreClaimEntity;

import java.util.Collection;
import java.util.List;

public interface UserPreClaimDao {

    List<UserPreClaimEntity> findPreClaimsForUser(Long userId);

    void save(UserPreClaimEntity userPreClaimEntity);

    List<PreClaim> findPreClaims(Collection<Long> userIds, Collection<Long> expertIds);

    class PreClaim {
        private Long userId;
        private Long expertToClaimId;
        private Long userToClaimId;
        private String email;

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getExpertToClaimId() {
            return expertToClaimId;
        }

        public void setExpertToClaimId(Long expertToClaimId) {
            this.expertToClaimId = expertToClaimId;
        }

        public Long getUserToClaimId() {
            return userToClaimId;
        }

        public void setUserToClaimId(Long userToClaimId) {
            this.userToClaimId = userToClaimId;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}
