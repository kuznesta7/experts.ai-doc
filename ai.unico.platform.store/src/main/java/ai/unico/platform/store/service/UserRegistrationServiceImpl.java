package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.ConsentDto;
import ai.unico.platform.store.api.dto.UserProfileDto;
import ai.unico.platform.store.api.dto.UserRegistrationDto;
import ai.unico.platform.store.api.service.*;
import ai.unico.platform.ume.api.dto.UserDto;
import ai.unico.platform.ume.api.service.UserNotifierService;
import ai.unico.platform.ume.api.service.UserPasswordService;
import ai.unico.platform.ume.api.service.UserService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.InvalidParameterException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

public class UserRegistrationServiceImpl implements UserRegistrationService {

    @Autowired
    private UserProfileService userProfileService;

    @Autowired
    private UserOrganizationService userOrganizationService;

    @Autowired
    private CountryOfInterestService countryOfInterestService;

    @Autowired
    private UserConsentService userConsentService;

    @Autowired
    private UnicoNotifierService unicoNotifierService;

    @Override
    @Transactional
    public Long registerUser(UserRegistrationDto userRegistrationDto, UserContext userContext) throws EntityAlreadyExistsException, NonexistentEntityException {
        final UserService service = ServiceRegistryUtil.getService(UserService.class);
        final UserDto userDto = new UserDto();
        final String email = userRegistrationDto.getEmail();
        final String fullName = userRegistrationDto.getFullName();
        if (fullName == null || fullName.isEmpty() || email == null || email.isEmpty()) {
            throw new InvalidParameterException("MISSING_FORM_FIELDS", "");
        }

        if (!email.matches("^\\S+@\\S+$")) {
            throw new InvalidParameterException("INVALID_EMAIL", "Somebody tries to create user with email " + email);
        }

        final UserDto byEmail = service.findByEmail(email);
        if (byEmail != null)
            throw new EntityAlreadyExistsException("USER_WITH_THIS_EMAIL_ALREADY_EXIST", "Somebody trying to create duplicate user with email " + email);


        final UserProfileDto userProfileDto = new UserProfileDto();
        userProfileDto.setName(fullName);
        userProfileDto.setEmail(email);
        userProfileDto.setInvitation(userRegistrationDto.isInvitation());
        final Long userId = userProfileService.createProfile(userProfileDto, userContext);
        userOrganizationService.updateOrganizationsForUser(userId, userRegistrationDto.getOrganizationDtos(), null);
        countryOfInterestService.updateCountriesOfInterestForUser(userId, userRegistrationDto.getCountryOfInterestCodes(), null);


        userDto.setFirstName(fullName);
        userDto.setEmail(email.toLowerCase());
        userDto.setId(userId);
        userDto.setTermsOfUseAccepted(userRegistrationDto.isTermsOfUseAccepted());

        final String password = userRegistrationDto.getPassword();
        final UserPasswordService userPasswordService = ServiceRegistryUtil.getService(UserPasswordService.class);
        if (password != null) {
            userPasswordService.validateNewPassword(password);
        }
        service.createUmeUser(userDto, !userRegistrationDto.getSocial() && password == null, userContext);

        final List<ConsentDto> allConsents = userConsentService.findAllConsents();
        for (ConsentDto consentDto : allConsents) {
            userConsentService.addUserConsent(userId, consentDto.getConsentId(), null);
        }

        if (password != null) {
            userPasswordService.doSetPassword(email, password, userContext);
        }

        unicoNotifierService.sendRegisterNotificationToUnico(userDto.getEmail());

        return userId;
    }

    @Override
    @Transactional
    public UserRegistrationDto findUser(Long userId) throws NonexistentEntityException {
        final UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
        final UserService service = ServiceRegistryUtil.getService(UserService.class);
        final UserDto userDto = service.find(userId);
        final UserProfileDto userProfileDto = userProfileService.loadProfile(userId);
        final Set<String> countryOfInterestCodesForUser = countryOfInterestService.findCountryOfInterestCodesForUser(userId);
        userRegistrationDto.setId(userDto.getId());
        userRegistrationDto.setFullName(userDto.getFirstName());
        userRegistrationDto.setEmail(userDto.getEmail());
        userRegistrationDto.setUserRolePresets(userDto.getUserRolesPreset());
        userRegistrationDto.setOrganizationDtos(userProfileDto.getOrganizationDtos());
        userRegistrationDto.setUserRolePresets(userDto.getUserRolesPreset());
        userRegistrationDto.setTermsOfUseAccepted(userDto.isTermsOfUseAccepted());
        userRegistrationDto.setCountryOfInterestCodes(countryOfInterestCodesForUser);
        return userRegistrationDto;
    }

    @Override
    @Transactional
    public void updateUser(UserRegistrationDto userRegistrationDto, UserContext userContext) throws NonexistentEntityException {
        final UserService service = ServiceRegistryUtil.getService(UserService.class);
        final Long userId = userRegistrationDto.getId();
        final UserDto userDto = service.find(userId);
        userDto.setUserRolesPreset(userRegistrationDto.getUserRolePresets());
        userDto.setTermsOfUseAccepted(userRegistrationDto.isTermsOfUseAccepted());
        service.editUmeUser(userDto, userContext);
        userOrganizationService.updateOrganizationsForUser(userId, userRegistrationDto.getOrganizationDtos(), userContext);
        countryOfInterestService.updateCountriesOfInterestForUser(userId, userRegistrationDto.getCountryOfInterestCodes(), userContext);
    }

    @Override
    @Transactional
    public void reinviteUser(UserRegistrationDto userRegistrationDto, UserContext userContext) {
        UserService userService = ServiceRegistryUtil.getService(UserService.class);
        UserNotifierService notifierService = ServiceRegistryUtil.getService(UserNotifierService.class);
        UserPasswordService passwordService = ServiceRegistryUtil.getService(UserPasswordService.class);
        final UserDto userDto = userService.findByEmail(userRegistrationDto.getEmail());
        final String passwordSetToken = passwordService.createPasswordSetToken(userDto.getId());
        notifierService.sendInvitationSetPasswordLink(userRegistrationDto.getEmail(), passwordSetToken, userContext.getFullName());
    }
}
