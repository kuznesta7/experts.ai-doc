package ai.unico.platform.store.service;

import ai.unico.platform.extdata.service.DWHItemService;
import ai.unico.platform.store.api.dto.ItemDetailDto;
import ai.unico.platform.store.api.dto.ItemIndexDto;
import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.dto.UserExpertBaseDto;
import ai.unico.platform.store.api.service.ItemOrganizationService;
import ai.unico.platform.store.api.dto.ItemNotRegisteredExpertDto;
import ai.unico.platform.store.api.service.ItemService;
import ai.unico.platform.store.api.service.UserItemService;
import ai.unico.platform.store.dao.*;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.store.util.CollectionsUtil;
import ai.unico.platform.store.util.ItemIndexUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.store.util.UserItemUtil;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.exception.InvalidParameterException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemTypeDao itemTypeDao;

    @Autowired
    private TagDao tagDao;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private UserExpertItemDao userExpertItemDao;

    @Override
    @Transactional
    public Long createItem(ItemDetailDto itemDetailDto, UserContext userContext) throws IOException {
        final ItemEntity itemEntity = new ItemEntity();
        fillEntity(itemEntity, itemDetailDto, userContext);
        TrackableUtil.fillCreate(itemEntity, userContext);
        final Long ownerUserId = itemDetailDto.getOwnerUserId();
        final Long ownerOrganizationId = itemDetailDto.getOwnerOrganizationId();
        if (ownerUserId != null) {
            itemEntity.setOwnerUser(userProfileDao.findUserProfile(ownerUserId));
        }
        if (ownerOrganizationId != null) {
            itemEntity.setOwnerOrganization(organizationDao.find(ownerOrganizationId));
        }
        itemDao.saveItem(itemEntity);
        ItemIndexUtil.indexItem(itemEntity);

        return itemEntity.getItemId();
    }

    @Override
    @Transactional
    public void editItem(ItemDetailDto itemDetailDto, UserContext userContext) throws NonexistentEntityException, IOException {
        Long itemId = itemDetailDto.getItemId();
        final ItemEntity itemEntity = itemDao.find(itemId);
        if (itemEntity == null) throw new NonexistentEntityException(ItemEntity.class);
        fillEntity(itemEntity, itemDetailDto, userContext);
        TrackableUtil.fillUpdate(itemEntity, userContext);
        ItemIndexUtil.indexItem(itemEntity);
    }

    private void fillEntity(ItemEntity itemEntity, ItemDetailDto itemDetailDto, UserContext userContext) throws IOException {
        if (itemDetailDto.getItemTypeId() == null
                || itemDetailDto.getItemName() == null
                || itemDetailDto.getItemName().isEmpty()
                || itemDetailDto.getItemDescription() == null
                || itemDetailDto.getItemDescription().isEmpty()
                || itemDetailDto.getYear() == null)
            throw new InvalidParameterException("Missing fields", "Missing item fields while editing / creating item");
        itemEntity.setItemName(itemDetailDto.getItemName());
        itemEntity.setItemDescription(itemDetailDto.getItemDescription());
        itemEntity.setYear(itemDetailDto.getYear());

        final Confidentiality confidentiality = itemDetailDto.getConfidentiality();
        itemEntity.setConfidentiality(confidentiality != null ? confidentiality : Confidentiality.PUBLIC);
        if (itemDetailDto.getItemTypeId() != null) {
            final ItemTypeEntity itemTypeEntity = itemTypeDao.find(itemDetailDto.getItemTypeId());
            if (itemTypeEntity == null || (!itemTypeEntity.isAddable() && !itemTypeEntity.equals(itemEntity.getItemTypeEntity())))
                throw new NonexistentEntityException(ItemTypeEntity.class);
            itemEntity.setItemTypeEntity(itemTypeEntity);
        }
        itemDao.saveItem(itemEntity);

        fillNotRegisteredExperts(itemEntity, itemDetailDto.getItemNotRegisteredExperts());

        ItemOrganizationService itemOrganizationService = ServiceRegistryUtil.getService(ItemOrganizationService.class);
        itemOrganizationService.switchOrganizationItems(
                itemDetailDto.getItemOrganizations().stream().map(OrganizationBaseDto::getOrganizationId).collect(Collectors.toSet()),
                itemEntity.getItemOrganizationEntities().stream().map(x -> x.getOrganizationEntity().getOrganizationId()).collect(Collectors.toSet()),
                itemEntity.getItemId(),
                userContext);

        Map<Long, Boolean> newUsers = new HashMap<>();
        Map<Long, Boolean> newExperts = new HashMap<>();
        for(UserExpertItemEntity entity: itemEntity.getUserItemEntities()){
            // TODO: check if this is correct
            if (entity.getUserProfileEntity() != null)
                newUsers.put(entity.getUserProfileEntity().getUserId(), false);
            else
                newExperts.put(entity.getExpertId(), false);
        }
        for(UserExpertBaseDto dto: itemDetailDto.getItemExperts()){
            if (IdUtil.isIntId(dto.getExpertCode())) {
                newUsers.put(dto.getUserId(), true);
            } else {
                newExperts.put(dto.getExpertId(), true);
            }
        }

        UserItemService userItemService = ServiceRegistryUtil.getService(UserItemService.class);
        userItemService.switchUserItems(newUsers, itemEntity.getItemId(), userContext);
        userItemService.switchExpertItems(newExperts, itemEntity.getItemId(), userContext);

        final List<String> keywords = itemDetailDto.getKeywords();
        final List<TagEntity> tagEntities = tagDao.findOrCreateTagsByValues(keywords);
        final Set<ItemTagEntity> itemTagEntities = itemEntity.getItemTagEntities();
        for (ItemTagEntity itemTagEntity : itemTagEntities) {
            if (itemTagEntity.getDeleted()) continue;
            final TagEntity tagEntity = itemTagEntity.getTagEntity();
            if (!tagEntities.contains(tagEntity)) itemTagEntity.setDeleted(true);
            else tagEntities.remove(tagEntity);
        }
        for (TagEntity tagEntity : tagEntities) {
            final ItemTagEntity itemTagEntity = new ItemTagEntity();
            itemTagEntity.setItemEntity(itemEntity);
            itemTagEntity.setTagEntity(tagEntity);
            itemDao.saveItemTag(itemTagEntity);
            itemEntity.getItemTagEntities().add(itemTagEntity);
        }

        ItemIndexUtil.indexItem(itemEntity);
    }

    @Override
    @Transactional
    public List<ItemIndexDto> updateAndFindItems(Integer limit, Integer offset) {
        final List<ItemEntity> items = itemDao.findItems(limit, offset);
        final DWHItemService service = ServiceRegistryUtil.getService(DWHItemService.class);
        final Set<Long> originalItemIds = items.stream().map(ItemEntity::getOrigItemId).filter(Objects::nonNull).collect(Collectors.toSet());
        final Map<Long, Set<Long>> expertIdsForItems = service.findExpertIdsForItems(originalItemIds);
        for (ItemEntity item : items) {
            if (item.getOrigItemId() != null) {
                final Set<UserExpertItemEntity> expertItemEntities = item.getUserItemEntities();
                final Set<Long> addedExpertIds = expertItemEntities.stream().map(UserExpertItemEntity::getExpertId).collect(Collectors.toSet());
                if (item.getOrigItemId() != null && expertIdsForItems.containsKey(item.getOrigItemId())) {
                    final Set<Long> expertIdsForItem = expertIdsForItems.get(item.getOrigItemId());
                    expertIdsForItem.removeAll(addedExpertIds);
                    for (Long expertToAddId : expertIdsForItem) {
                        final UserExpertItemEntity expertItemEntity = new UserExpertItemEntity();
                        expertItemEntity.setItemEntity(item);
                        expertItemEntity.setExpertId(expertToAddId);
                        TrackableUtil.fillCreate(expertItemEntity, null);
                        userExpertItemDao.saveUserItem(expertItemEntity);
                        item.getUserItemEntities().add(expertItemEntity);
                    }
                }
            }
        }
        final List<ItemIndexDto> list = items.stream().map(UserItemUtil::convertToIndex).collect(Collectors.toList());
        return list;
    }

    @Override
    @Transactional
    public void loadAndIndexItem(Long itemId) {
        final ItemEntity itemEntity = itemDao.find(itemId);
        if (itemEntity == null) return;
        ItemIndexUtil.indexItem(itemEntity);
    }

    @Override
    @Transactional
    public void loadAndIndexItems(Set<Long> itemIds) {
        if (itemIds.isEmpty())
            return;
        final List<ItemEntity> itemEntity = itemDao.find(itemIds);
        for (ItemEntity entity : itemEntity) {
            ItemIndexUtil.indexItem(entity);
        }
    }

    @Override
    @Transactional
    public Map<Long, Long> findVerifiedItemIdsMap(Set<Long> originalItemIds) {
        if (originalItemIds.isEmpty()) return Collections.emptyMap();
        final List<ItemEntity> itemEntities = itemDao.findItemsByOriginalIds(originalItemIds);
        return itemEntities.stream().collect(Collectors.toMap(ItemEntity::getOrigItemId, ItemEntity::getItemId));
    }

    @Override
    @Transactional
    public List<ItemNotRegisteredExpertDto> findNotRegisteredExperts(Set<Long> expertIds) {
        List<ItemNotRegisteredExpertEntity> entities = userExpertItemDao.findNotRegisteredExperts(expertIds);
        return entities.stream().map(entity -> {
            ItemNotRegisteredExpertDto dto = new ItemNotRegisteredExpertDto();
            dto.setId(entity.getId());
            dto.setExpertName(entity.getExpertName());
            return dto;
        }).collect(Collectors.toList());
    }

    private void fillNotRegisteredExperts(ItemEntity itemEntity, List<ItemNotRegisteredExpertDto> receivedNotRegisteredExperts) {
        // Finding not registered experts to remove
        Set<Long> idsToRemove = CollectionsUtil.setDifference(itemEntity.getItemNotRegisteredExperts()
                        .stream().map(ItemNotRegisteredExpertEntity::getId)
                        .collect(Collectors.toSet()),
                receivedNotRegisteredExperts.stream().map(ItemNotRegisteredExpertDto::getId)
                        .collect(Collectors.toSet()));
        for (Long expertToRemove : idsToRemove) {
            userExpertItemDao.removeNotRegisteredExpert(expertToRemove);
        }

        itemEntity.setItemNotRegisteredExperts(receivedNotRegisteredExperts.stream()
                .map(dto -> {
                    // Avoiding creating duplicate not registered experts
                    ItemNotRegisteredExpertEntity entity = userExpertItemDao.findNotRegisteredExpertByItemIdAndName(itemEntity.getItemId(), dto.getExpertName());
                    if (entity != null) return entity;
                    // Creating new not registered expert which did not exist before
                    entity = new ItemNotRegisteredExpertEntity();
                    entity.setItem(itemEntity);
                    entity.setExpertName(dto.getExpertName());
                    userExpertItemDao.saveNotRegisteredExpert(entity);
                    return entity;
                })
                .collect(Collectors.toList()));
    }
}
