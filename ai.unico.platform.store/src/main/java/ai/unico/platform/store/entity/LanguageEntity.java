package ai.unico.platform.store.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "language")

public class LanguageEntity {
    @Id
    @Column(name = "language_code", nullable = false, length = 15)
    private String languageCode;
    @Column(name = "language_name", nullable = false, unique = true, length = 255)
    private String languageName;
    @Column(name = "language_name_en", nullable = false, unique = true, length = 255)
    private String languageNameEn;
    public String getLanguageCode() {
        return languageCode;
    }
    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }
    public String getLanguageName() {
        return languageName;
    }
    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }
    public String getLanguageNameEn() {
        return languageNameEn;
    }
    public void setLanguageNameEn(String languageNameEn) {
        this.languageNameEn = languageNameEn;
    }
}
