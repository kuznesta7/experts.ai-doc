package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.CommercializationDomainEntity;

import java.util.List;

public interface CommercializationDomainDao {

    CommercializationDomainEntity findDomain(Long domainId);

    void save(CommercializationDomainEntity commercializationDomainEntity);

    List<CommercializationDomainEntity> findAll();
}
