package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.ProjectDocumentDto;
import ai.unico.platform.store.api.dto.ProjectDocumentPreviewDto;
import ai.unico.platform.store.api.service.FileService;
import ai.unico.platform.store.api.service.ProjectDocumentService;
import ai.unico.platform.store.dao.FileDao;
import ai.unico.platform.store.dao.ProjectDao;
import ai.unico.platform.store.dao.ProjectDocumentDao;
import ai.unico.platform.store.entity.FileEntity;
import ai.unico.platform.store.entity.ProjectDocumentEntity;
import ai.unico.platform.store.entity.ProjectEntity;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

public class ProjectDocumentServiceImpl implements ProjectDocumentService {

    @Autowired
    private ProjectDocumentDao projectDocumentDao;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private FileDao fileDao;

    @Autowired
    private FileService fileService;

    @Override
    @Transactional
    public List<ProjectDocumentPreviewDto> findProjectDocuments(Long projectId) {
        final List<ProjectDocumentEntity> projectDocumentEntities = projectDocumentDao.findForProject(projectId);
        return projectDocumentEntities.stream().map(this::convert).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public FileDto getProjectDocument(Long projectId, Long fileId, UserContext userContext) {
        final ProjectDocumentEntity document = projectDocumentDao.findDocument(projectId, fileId);
        if (document == null) throw new NonexistentEntityException(ProjectDocumentEntity.class);
        return fileService.getFile(fileId, userContext);
    }

    @Override
    @Transactional
    public Long addProjectDocument(Long projectId, ProjectDocumentDto documentDto, UserContext userContext) {
        final ProjectEntity projectEntity = projectDao.find(projectId);
        if (projectEntity == null)
            throw new NonexistentEntityException(ProjectEntity.class);
        final FileEntity fileEntity = new FileEntity();
        fileEntity.setContent(documentDto.getContent());
        fileEntity.setName(documentDto.getName());
        TrackableUtil.fillCreate(fileEntity, userContext);
        fileDao.save(fileEntity);
        final ProjectDocumentEntity projectdocumentEntity = new ProjectDocumentEntity();
        projectdocumentEntity.setProjectEntity(projectEntity);
        projectdocumentEntity.setFileEntity(fileEntity);
        TrackableUtil.fillCreate(projectdocumentEntity, userContext);
        projectDocumentDao.save(projectdocumentEntity);
        return fileEntity.getFileId();
    }

    @Override
    @Transactional
    public void deleteProjectDocument(Long projectId, Long fileId, UserContext userContext) {
        final ProjectDocumentEntity document = projectDocumentDao.findDocument(projectId, fileId);
        if (document == null) throw new NonexistentEntityException(ProjectDocumentEntity.class);
        document.setDeleted(true);
        TrackableUtil.fillUpdate(document, userContext);
    }

    private ProjectDocumentPreviewDto convert(ProjectDocumentEntity entity) {
        ProjectDocumentPreviewDto dto = new ProjectDocumentPreviewDto();
        dto.setProjectId(HibernateUtil.getId(entity, ProjectDocumentEntity::getProjectEntity, ProjectEntity::getProjectId));
        final FileEntity fileEntity = entity.getFileEntity();
        dto.setDocumentId(fileEntity.getFileId());
        dto.setDocumentName(fileEntity.getName());
        dto.setSize(fileEntity.getSize());
        dto.setAuthorUserId(entity.getUserInsert());
        dto.setDateCreated(entity.getDateInsert());
        if (fileEntity.getName() != null && fileEntity.getName().contains(".")) {
            final int i = fileEntity.getName().lastIndexOf('.');
            dto.setDocumentType(fileEntity.getName().substring(i + 1).toUpperCase());
        }
        return dto;
    }
}
