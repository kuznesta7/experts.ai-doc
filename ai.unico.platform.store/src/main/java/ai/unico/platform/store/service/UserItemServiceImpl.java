package ai.unico.platform.store.service;

import ai.unico.platform.store.api.service.ExpertToUserService;
import ai.unico.platform.store.api.service.UnicoNotifierService;
import ai.unico.platform.store.api.service.UserItemService;
import ai.unico.platform.store.dao.ItemDao;
import ai.unico.platform.store.dao.UserExpertItemDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.ItemEntity;
import ai.unico.platform.store.entity.UserExpertItemEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.util.ItemIndexUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class UserItemServiceImpl implements UserItemService {

    @Autowired
    private UserExpertItemDao userExpertItemDao;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private UnicoNotifierService unicoNotifierService;

    @Autowired
    private ExpertToUserService expertToUserService;

    @Override
    @Transactional
    public void addUserItem(Long userId, Long itemId, UserContext userContext) throws NonexistentEntityException {
        final UserExpertItemEntity userExpertItemEntity = userExpertItemDao.findForUser(userId, itemId);
        if (userExpertItemEntity != null)
            throw new EntityAlreadyExistsException("USER_ALREADY_ADDED_TO_THE_ITEM", "UserItem already exists; userId:" + userId + " itemId:" + itemId + ", authorId: " + userContext.getUserId());
        final UserExpertItemEntity newUserExpertItemEntity = new UserExpertItemEntity();
        final UserProfileEntity userProfile = userProfileDao.findUserProfile(userId);
        final ItemEntity itemEntity = itemDao.find(itemId);
        if (itemEntity == null) throw new NonexistentEntityException(ItemEntity.class);
        if (userProfile == null) throw new NonexistentEntityException(UserProfileEntity.class);
        newUserExpertItemEntity.setUserProfileEntity(userProfile);
        newUserExpertItemEntity.setItemEntity(itemEntity);
        TrackableUtil.fillCreate(newUserExpertItemEntity, userContext);
        userExpertItemDao.saveUserItem(newUserExpertItemEntity);

        itemEntity.getUserItemEntities().add(newUserExpertItemEntity);
        ItemIndexUtil.indexItem(itemEntity);
    }

    @Override
    @Transactional
    public void addExpertItem(Long expertId, Long itemId, UserContext userContext) throws NonexistentEntityException {
        final UserExpertItemEntity expertItemEntity = userExpertItemDao.findForExpert(expertId, itemId);
        if (expertItemEntity != null)
            throw new EntityAlreadyExistsException("EXPERT_ALREADY_ADDED_TO_THE_ITEM", "ExpertItem already exists; userId:" + expertId + " itemId:" + itemId + ", authorId: " + userContext.getUserId());
        final UserExpertItemEntity newExpertItemEntity = new UserExpertItemEntity();
        final ItemEntity itemEntity = itemDao.find(itemId);
        if (itemEntity == null) throw new NonexistentEntityException(ItemEntity.class);
        newExpertItemEntity.setItemEntity(itemEntity);
        newExpertItemEntity.setExpertId(expertId);
        userExpertItemDao.saveUserItem(newExpertItemEntity);
        TrackableUtil.fillCreate(newExpertItemEntity, userContext);

        itemEntity.getUserItemEntities().add(newExpertItemEntity);
        ItemIndexUtil.indexItem(itemEntity);
    }

    @Override
    @Transactional
    public void removeUserItem(Long userId, Long itemId, String message, UserContext userContext) throws NonexistentEntityException {
        UserExpertItemEntity userExpertItemEntity = userExpertItemDao.findForUser(userId, itemId);
        if (userExpertItemEntity == null) throw new NonexistentEntityException(UserExpertItemEntity.class);

        userExpertItemEntity.setDeleted(true);
        TrackableUtil.fillUpdate(userExpertItemEntity, userContext);

        final ItemEntity itemEntity = userExpertItemEntity.getItemEntity();

        if (itemEntity.getOrigItemId() != null) {
            String fullName = userExpertItemEntity.getUserProfileEntity().getName();
            String itemName = itemEntity.getItemName();
            unicoNotifierService.sendItemUnclaimNotification(userId, fullName, itemId, itemName, message);
        }

        itemEntity.getUserItemEntities().remove(userExpertItemEntity);
        ItemIndexUtil.indexItem(itemEntity);
    }

    @Override
    @Transactional
    public void removeExpertItem(Long expertId, Long itemId, String message, UserContext userContext) throws NonexistentEntityException {
        final UserExpertItemEntity expertItemEntity = userExpertItemDao.findForExpert(expertId, itemId);
        if (expertItemEntity == null) throw new NonexistentEntityException(UserExpertItemEntity.class);
        expertItemEntity.setDeleted(true);
        TrackableUtil.fillUpdate(expertItemEntity, userContext);

        final ItemEntity itemEntity = expertItemEntity.getItemEntity();
        itemEntity.getUserItemEntities().remove(expertItemEntity);
        ItemIndexUtil.indexItem(itemEntity);
    }

    @Override
    @Transactional
    public void setHideUserItem(Long userId, Long itemId, UserContext userContext, boolean hide) throws NonexistentEntityException {
        final UserExpertItemEntity userExpertItemEntity = userExpertItemDao.findForUser(userId, itemId);
        if (userExpertItemEntity == null) throw new NonexistentEntityException(UserExpertItemEntity.class);
        userExpertItemEntity.setHidden(hide);
        TrackableUtil.fillUpdate(userExpertItemEntity, userContext);
    }

    @Override
    @Transactional
    public void setFavoriteUserItem(Long userId, Long itemId, UserContext userContext, boolean favorite) throws NonexistentEntityException {
        final UserExpertItemEntity userExpertItemEntity = userExpertItemDao.findForUser(userId, itemId);
        if (userExpertItemEntity == null) throw new NonexistentEntityException(UserExpertItemEntity.class);
        userExpertItemEntity.setFavorite(favorite);
        TrackableUtil.fillUpdate(userExpertItemEntity, userContext);
    }

    @Override
    public Set<Long> switchExpertItems(Map<Long, Boolean> newExperts, Long itemId, UserContext userContext) throws NonexistentEntityException {
        ItemEntity entity = itemDao.find(itemId);
        for (Map.Entry<Long, Boolean> entry: newExperts.entrySet()) {
            UserExpertItemEntity userExpertItemEntity = userExpertItemDao.findForExpert(entry.getKey(), itemId);
            if (userExpertItemEntity == null) {
                userExpertItemEntity = new UserExpertItemEntity();
                userExpertItemEntity.setExpertId(entry.getKey());
                userExpertItemEntity.setItemEntity(entity);
            }
            userExpertItemEntity.setDeleted(! entry.getValue());
            TrackableUtil.fillUpdate(userExpertItemEntity, userContext);
            userExpertItemDao.saveUserItem(userExpertItemEntity);
        }
        return newExperts.entrySet().stream()
                .filter(Map.Entry::getValue)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    @Override
    @Transactional
    public Set<Long> switchUserItems(Map<Long, Boolean> newUsers, Long itemId, UserContext userContext) throws NonexistentEntityException {
        ItemEntity entity = itemDao.find(itemId);
        for (Map.Entry<Long, Boolean> entry: newUsers.entrySet()) {
            UserExpertItemEntity userExpertItemEntity = userExpertItemDao.findForUser(entry.getKey(), itemId);
            if (userExpertItemEntity == null) {
                userExpertItemEntity = new UserExpertItemEntity();
                userExpertItemEntity.setUserProfileEntity(userProfileDao.findUserProfile(entry.getKey()));
                userExpertItemEntity.setItemEntity(entity);
            }
            userExpertItemEntity.setDeleted(! entry.getValue());
            TrackableUtil.fillUpdate(userExpertItemEntity, userContext);
            userExpertItemDao.saveUserItem(userExpertItemEntity);
        }
        itemDao.saveItem(entity);
        return newUsers.entrySet().stream()
                .filter(Map.Entry::getValue)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    @Override
    @Transactional
    public List<Long> getUserItems(Long userId, UserContext userContext) throws NonexistentEntityException {
        return userExpertItemDao.findUserItems(userId);
    }

    @Override
    @Transactional
    public List<Long> getExpertItems(Long expertId, UserContext userContext) throws NonexistentEntityException {
        return userExpertItemDao.findExpertItems(expertId);
    }
}
