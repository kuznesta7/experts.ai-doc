package ai.unico.platform.store.service;

import ai.unico.platform.store.api.service.ExpertService;
import ai.unico.platform.store.api.service.RetirementService;
import ai.unico.platform.store.dao.ExpertDao;
import ai.unico.platform.store.dao.UserOrganizationDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.ExpertEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Date;

public class RetirementServiceImpl implements RetirementService {

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private ExpertDao expertDao;

    @Autowired
    private UserOrganizationDao userOrganizationDao;

    @Autowired
    private ExpertService expertService;

    @Override
    @Transactional
    public void setExpertRetired(Long expertId, boolean retired, UserContext userContext) {
        final ExpertEntity expertEntity = expertDao.findOrCreate(expertId, userContext);
        final Date retirementDate = retired ? new Date() : null;
        expertEntity.setRetirementDate(retirementDate);
        TrackableUtil.fillUpdate(expertEntity, userContext);

        try {
            expertService.loadAndUpdateExpert(expertId);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void setUserRetired(Long userId, boolean retired, UserContext userContext) {
        final UserProfileEntity userProfile = userProfileDao.findUserProfile(userId);
        if (userProfile == null) throw new NonexistentEntityException(UserProfileEntity.class);
        final Date retirementDate = retired ? new Date() : null;
        userProfile.setRetirementDate(retirementDate);
        TrackableUtil.fillUpdate(userProfile, userContext);
        userProfileDao.save(userProfile);
    }
}
