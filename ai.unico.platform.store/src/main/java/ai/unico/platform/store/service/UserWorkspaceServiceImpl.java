package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.UserWorkspaceDto;
import ai.unico.platform.store.api.dto.UserWorkspacePreviewDto;
import ai.unico.platform.store.api.service.UserWorkspaceService;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.dao.UserWorkspaceDao;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.entity.UserWorkspaceEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.InvalidParameterException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public class UserWorkspaceServiceImpl implements UserWorkspaceService {

    @Autowired
    private UserWorkspaceDao userWorkspaceDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Override
    @Transactional
    public Long createUserWorkspace(UserWorkspaceDto userWorkspaceDto, UserContext userContext) {
        if (userWorkspaceDto.getWorkspaceName().isEmpty())
            throw new InvalidParameterException("WORKSPACE_NAME_NOT_FILLED", "missing workspace name while creating workspace");
        final UserProfileEntity userProfile = userProfileDao.findUserProfile(userWorkspaceDto.getUserId());
        if (userProfile == null) throw new NonexistentEntityException(UserProfileEntity.class);

        final UserWorkspaceEntity userWorkspaceEntity = new UserWorkspaceEntity();
        userWorkspaceEntity.setUserProfileEntity(userProfile);
        fillUserWorkspaceEntity(userWorkspaceEntity, userWorkspaceDto);
        TrackableUtil.fillCreate(userWorkspaceEntity, userContext);
        userWorkspaceDao.save(userWorkspaceEntity);
        return userWorkspaceEntity.getUserWorkspaceId();
    }

    @Override
    @Transactional
    public void updateUserWorkspace(Long userWorkspaceId, UserWorkspaceDto userWorkspaceDto, UserContext userContext) {
        final UserWorkspaceEntity userWorkspace = userWorkspaceDao.findUserWorkspace(userWorkspaceId);
        if (userWorkspace == null) throw new NonexistentEntityException(UserWorkspaceEntity.class);
        fillUserWorkspaceEntity(userWorkspace, userWorkspaceDto);
        TrackableUtil.fillUpdate(userWorkspace, userContext);
    }

    @Override
    @Transactional
    public void deleteUserWorkspace(Long userWorkspaceId, UserContext userContext) {
        final UserWorkspaceEntity userWorkspace = userWorkspaceDao.findUserWorkspace(userWorkspaceId);
        if (userWorkspace == null) throw new NonexistentEntityException(UserWorkspaceEntity.class);
        userWorkspace.setDeleted(true);
        TrackableUtil.fillUpdate(userWorkspace, userContext);
    }

    @Override
    @Transactional
    public List<UserWorkspacePreviewDto> findUserWorkspaces(Long userId) {
        final List<UserWorkspaceEntity> workspacesForUser = userWorkspaceDao.findWorkspacesForUser(userId);
        final List<UserWorkspacePreviewDto> userWorkspacePreviewDtos = new ArrayList<>();
        for (UserWorkspaceEntity userWorkspaceEntity : workspacesForUser) {
            final UserWorkspacePreviewDto workspacePreviewDto = new UserWorkspacePreviewDto();
            workspacePreviewDto.setUserId(userId);
            workspacePreviewDto.setWorkspaceId(userWorkspaceEntity.getUserWorkspaceId());
            workspacePreviewDto.setWorkspaceName(userWorkspaceEntity.getWorkspaceName());
            userWorkspacePreviewDtos.add(workspacePreviewDto);
        }
        return userWorkspacePreviewDtos;
    }

    private void fillUserWorkspaceEntity(UserWorkspaceEntity userWorkspaceEntity, UserWorkspaceDto userWorkspaceDto) {
        userWorkspaceEntity.setWorkspaceName(userWorkspaceDto.getWorkspaceName());
        userWorkspaceEntity.setWorkspaceNote(userWorkspaceDto.getWorkspaceNote());
    }
}
