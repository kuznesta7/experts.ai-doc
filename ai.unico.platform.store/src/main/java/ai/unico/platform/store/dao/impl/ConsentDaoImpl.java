package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ConsentDao;
import ai.unico.platform.store.entity.ConsentEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class ConsentDaoImpl implements ConsentDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public List<ConsentEntity> findAllConsents() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ConsentEntity.class, "consent");
        return (List<ConsentEntity>) criteria.list();
    }

    @Override
    public ConsentEntity findConsent(Long consentId) {
        return entityManager.find(ConsentEntity.class, consentId);
    }
}
