package ai.unico.platform.store.entity;

import javax.persistence.*;

@Entity
public class SearchHistResultEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long searchHistResultId;

    private Long resultUserId;

    private Long resultExpertId;

    private Integer position;

    @ManyToOne
    @JoinColumn(name = "search_hist_id")
    private SearchHistEntity searchHistEntity;

    public Long getSearchHistResultId() {
        return searchHistResultId;
    }

    public void setSearchHistResultId(Long searchHistResultId) {
        this.searchHistResultId = searchHistResultId;
    }

    public Long getResultUserId() {
        return resultUserId;
    }

    public void setResultUserId(Long resultUserId) {
        this.resultUserId = resultUserId;
    }

    public Long getResultExpertId() {
        return resultExpertId;
    }

    public void setResultExpertId(Long resultExpertId) {
        this.resultExpertId = resultExpertId;
    }

    public SearchHistEntity getSearchHistEntity() {
        return searchHistEntity;
    }

    public void setSearchHistEntity(SearchHistEntity searchHistEntity) {
        this.searchHistEntity = searchHistEntity;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }
}
