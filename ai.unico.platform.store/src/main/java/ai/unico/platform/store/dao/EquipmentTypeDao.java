package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.EquipmentTypeEntity;

import java.util.List;

public interface EquipmentTypeDao {
    void save(EquipmentTypeEntity equipmentType);
    EquipmentTypeEntity find(Long typeId);
    List<EquipmentTypeEntity> findAll();
}
