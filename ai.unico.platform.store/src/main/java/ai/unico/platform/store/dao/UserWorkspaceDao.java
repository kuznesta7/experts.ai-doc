package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.FollowedProfileWorkspaceEntity;
import ai.unico.platform.store.entity.SearchHistWorkspaceEntity;
import ai.unico.platform.store.entity.UserWorkspaceEntity;

import java.util.List;

public interface UserWorkspaceDao {

    UserWorkspaceEntity findUserWorkspace(Long userWorkspaceId);

    UserWorkspaceEntity findUserWorkspace(Long userId, String workspaceName);

    List<UserWorkspaceEntity> findWorkspacesForUser(Long userId);

    void save(FollowedProfileWorkspaceEntity followedProfileWorkspaceEntity);

    void save(SearchHistWorkspaceEntity searchHistWorkspaceEntity);

    void save(UserWorkspaceEntity userWorkspaceEntity);

}
