package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class ProjectItemEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long projectItemId;

    @ManyToOne
    private ProjectEntity projectEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    private ItemEntity itemEntity;

    private Long originalItemId;

    private boolean deleted;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Long getProjectItemId() {
        return projectItemId;
    }

    public void setProjectItemId(Long projectItemId) {
        this.projectItemId = projectItemId;
    }

    public ProjectEntity getProjectEntity() {
        return projectEntity;
    }

    public void setProjectEntity(ProjectEntity projectEntity) {
        this.projectEntity = projectEntity;
    }

    public ItemEntity getItemEntity() {
        return itemEntity;
    }

    public void setItemEntity(ItemEntity itemEntity) {
        this.itemEntity = itemEntity;
    }

    public Long getOriginalItemId() {
        return originalItemId;
    }

    public void setOriginalItemId(Long originalItemId) {
        this.originalItemId = originalItemId;
    }
}
