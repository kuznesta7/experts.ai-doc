package ai.unico.platform.store.dao;

import ai.unico.platform.store.api.dto.SearchStatisticsDto;
import ai.unico.platform.store.api.filter.SearchStatisticsFilter;
import ai.unico.platform.store.entity.SearchStatisticsEntity;

import java.util.List;

public interface SearchStatisticsDao {

    List<SearchStatisticsEntity> findHotTopics();

    List<SearchStatisticsEntity> findSearchedData(SearchStatisticsFilter filter);

    Long numberOfResults(SearchStatisticsFilter filter);

    void updateSearchedData(SearchStatisticsDto statisticsDto);

    Long createSearchData(SearchStatisticsDto statisticsDto);
}
