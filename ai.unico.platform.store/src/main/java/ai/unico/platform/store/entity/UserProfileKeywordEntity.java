package ai.unico.platform.store.entity;

import javax.persistence.*;

@Entity
public class UserProfileKeywordEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userProfileKeywordId;

    @ManyToOne
    @JoinColumn(name = "tag_id")
    private TagEntity tagEntity;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserProfileEntity userProfileEntity;

    private boolean deleted;

    public Long getUserProfileKeywordId() {
        return userProfileKeywordId;
    }

    public void setUserProfileKeywordId(Long userProfileKeywordId) {
        this.userProfileKeywordId = userProfileKeywordId;
    }

    public TagEntity getTagEntity() {
        return tagEntity;
    }

    public void setTagEntity(TagEntity tagEntity) {
        this.tagEntity = tagEntity;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public UserProfileEntity getUserProfileEntity() {
        return userProfileEntity;
    }

    public void setUserProfileEntity(UserProfileEntity userProfileEntity) {
        this.userProfileEntity = userProfileEntity;
    }
}
