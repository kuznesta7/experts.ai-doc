package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;
import ai.unico.platform.util.enums.ExpertSearchType;

import javax.persistence.*;
import java.util.Set;

@Entity
public class SearchHistEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long searchHistId;

    private String query;

    private Integer page;

    private Integer resultsLimit;

    private Boolean ontologyDisabled;

    private Integer yearFrom;

    private Integer yearTo;

    private boolean deleted;

    private boolean favorite;

    private String shareCode;

    private boolean hidden;

    private Integer queryLength;

    @OneToMany(mappedBy = "searchHistEntity")
    private Set<SearchHistSecondaryQueryEntity> secondaryQueries;

    @OneToMany(mappedBy = "searchHistEntity")
    private Set<SearchHistWorkspaceEntity> searchHistWorkspaceEntities;

    @ElementCollection
    @CollectionTable(name = "search_hist_disabled_secondary_query", joinColumns = @JoinColumn(name = "search_hist_id"))
    @Column(name = "query")
    private Set<String> disabledSecondaryQueries;

    @ElementCollection
    @CollectionTable(name = "search_hist_result_type_group", joinColumns = @JoinColumn(name = "search_hist_id"))
    @Column(name = "result_type_group_id")
    private Set<Long> resultTypeGroupIds;

    @ElementCollection
    @CollectionTable(name = "search_hist_organization", joinColumns = @JoinColumn(name = "search_hist_id"))
    @Column(name = "organization_id")
    private Set<Long> organizationIds;

    @ElementCollection
    @CollectionTable(name = "search_hist_country", joinColumns = @JoinColumn(name = "search_hist_id"))
    @Column(name = "country_code")
    private Set<String> countryCodes;

    @OneToMany(mappedBy = "searchHistEntity")
    private Set<SearchHistResultEntity> searchHistResultEntities;

    @Enumerated(EnumType.STRING)
    private ExpertSearchType searchType;

    private Long numberOfResultsFound;

    public Long getSearchHistId() {
        return searchHistId;
    }

    public void setSearchHistId(Long searchHistId) {
        this.searchHistId = searchHistId;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Boolean getOntologyDisabled() {
        return ontologyDisabled;
    }

    public void setOntologyDisabled(Boolean ontologyDisabled) {
        this.ontologyDisabled = ontologyDisabled;
    }

    public Integer getYearFrom() {
        return yearFrom;
    }

    public void setYearFrom(Integer yearFrom) {
        this.yearFrom = yearFrom;
    }

    public Integer getYearTo() {
        return yearTo;
    }

    public void setYearTo(Integer yearTo) {
        this.yearTo = yearTo;
    }

    public Set<String> getDisabledSecondaryQueries() {
        return disabledSecondaryQueries;
    }

    public void setDisabledSecondaryQueries(Set<String> disabledSecondaryQueries) {
        this.disabledSecondaryQueries = disabledSecondaryQueries;
    }

    public Set<Long> getResultTypeGroupIds() {
        return resultTypeGroupIds;
    }

    public void setResultTypeGroupIds(Set<Long> resultTypeGroupIds) {
        this.resultTypeGroupIds = resultTypeGroupIds;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<SearchHistSecondaryQueryEntity> getSecondaryQueries() {
        return secondaryQueries;
    }

    public void setSecondaryQueries(Set<SearchHistSecondaryQueryEntity> secondaryQueries) {
        this.secondaryQueries = secondaryQueries;
    }

    public Long getNumberOfResultsFound() {
        return numberOfResultsFound;
    }

    public void setNumberOfResultsFound(Long numberOfResultsFound) {
        this.numberOfResultsFound = numberOfResultsFound;
    }

    public Integer getResultsLimit() {
        return resultsLimit;
    }

    public void setResultsLimit(Integer resultsLimit) {
        this.resultsLimit = resultsLimit;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Set<SearchHistWorkspaceEntity> getSearchHistWorkspaceEntities() {
        return searchHistWorkspaceEntities;
    }

    public void setSearchHistWorkspaceEntities(Set<SearchHistWorkspaceEntity> searchHistWorkspaceEntities) {
        this.searchHistWorkspaceEntities = searchHistWorkspaceEntities;
    }

    public Set<SearchHistResultEntity> getSearchHistResultEntities() {
        return searchHistResultEntities;
    }

    public void setSearchHistResultEntities(Set<SearchHistResultEntity> searchHistResultEntities) {
        this.searchHistResultEntities = searchHistResultEntities;
    }

    public Set<String> getCountryCodes() {
        return countryCodes;
    }

    public void setCountryCodes(Set<String> countryCodes) {
        this.countryCodes = countryCodes;
    }

    public String getShareCode() {
        return shareCode;
    }

    public void setShareCode(String shareCode) {
        this.shareCode = shareCode;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public Integer getQueryLength() {
        return queryLength;
    }

    public ExpertSearchType getSearchType() {
        return searchType;
    }

    public void setSearchType(ExpertSearchType searchType) {
        this.searchType = searchType;
    }
}
