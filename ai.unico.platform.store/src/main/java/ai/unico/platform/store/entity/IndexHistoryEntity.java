package ai.unico.platform.store.entity;

import ai.unico.platform.store.api.enums.IndexHistoryStatus;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class IndexHistoryEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long indexHistoryId;

    @Enumerated(EnumType.STRING)
    private IndexTarget target;

    private boolean clean;

    private String errorLog;

    @Enumerated(EnumType.STRING)
    private IndexHistoryStatus status;

    public Long getIndexHistoryId() {
        return indexHistoryId;
    }

    public void setIndexHistoryId(Long indexHistoryId) {
        this.indexHistoryId = indexHistoryId;
    }

    public boolean isClean() {
        return clean;
    }

    public void setClean(boolean clean) {
        this.clean = clean;
    }

    public IndexTarget getTarget() {
        return target;
    }

    public void setTarget(IndexTarget target) {
        this.target = target;
    }

    public IndexHistoryStatus getStatus() {
        return status;
    }

    public void setStatus(IndexHistoryStatus status) {
        this.status = status;
    }

    public String getErrorLog() {
        return errorLog;
    }

    public void setErrorLog(String errorLog) {
        this.errorLog = errorLog;
    }
}
