package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ProjectOrganizationEntity;

import java.util.List;

public interface ProjectOrganizationDao {

    void saveProjectOrganization(ProjectOrganizationEntity projectOrganizationEntity);

    List<ProjectOrganizationEntity> findProjectOrganizations(Long projectId);

    ProjectOrganizationEntity findProjectOrganization(Long projectId, Long organizationId);

}
