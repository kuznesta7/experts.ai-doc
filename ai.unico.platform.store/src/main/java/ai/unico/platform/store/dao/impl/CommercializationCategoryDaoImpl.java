package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.CommercializationCategoryDao;
import ai.unico.platform.store.entity.CommercializationCategoryEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class CommercializationCategoryDaoImpl implements CommercializationCategoryDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public CommercializationCategoryEntity findCategory(Long categoryId) {
        return entityManager.find(CommercializationCategoryEntity.class, categoryId);
    }

    @Override
    public List<CommercializationCategoryEntity> findAll() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(CommercializationCategoryEntity.class, "category");
        return criteria.list();
    }

    @Override
    public void save(CommercializationCategoryEntity commercializationCategoryEntity) {
        entityManager.persist(commercializationCategoryEntity);
    }
}
