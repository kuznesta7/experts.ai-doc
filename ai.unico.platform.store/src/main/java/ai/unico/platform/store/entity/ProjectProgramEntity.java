package ai.unico.platform.store.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ProjectProgramEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long projectProgramId;

    private Long originalProjectProgramId;

    private String projectProgramName;

    private String projectProgramDescription;

    private boolean deleted;

    public Long getProjectProgramId() {
        return projectProgramId;
    }

    public void setProjectProgramId(Long projectProgramId) {
        this.projectProgramId = projectProgramId;
    }

    public Long getOriginalProjectProgramId() {
        return originalProjectProgramId;
    }

    public void setOriginalProjectProgramId(Long originalProjectProgramId) {
        this.originalProjectProgramId = originalProjectProgramId;
    }

    public String getProjectProgramName() {
        return projectProgramName;
    }

    public void setProjectProgramName(String projectProgramName) {
        this.projectProgramName = projectProgramName;
    }

    public String getProjectProgramDescription() {
        return projectProgramDescription;
    }

    public void setProjectProgramDescription(String projectProgramDescription) {
        this.projectProgramDescription = projectProgramDescription;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
