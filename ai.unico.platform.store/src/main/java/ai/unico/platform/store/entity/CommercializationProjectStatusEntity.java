package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class CommercializationProjectStatusEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commercializationProjectStatusId;

    private boolean outdated;

    @ManyToOne
    @JoinColumn(name = "commercializationProjectId")
    private CommercializationProjectEntity commercializationProjectEntity;

    @ManyToOne
    @JoinColumn(name = "commercializationStatusTypeId")
    private CommercializationStatusTypeEntity commercializationStatusTypeEntity;

    public Long getCommercializationProjectStatusId() {
        return commercializationProjectStatusId;
    }

    public void setCommercializationProjectStatusId(Long commercializationProjectStatusId) {
        this.commercializationProjectStatusId = commercializationProjectStatusId;
    }

    public CommercializationProjectEntity getCommercializationProjectEntity() {
        return commercializationProjectEntity;
    }

    public void setCommercializationProjectEntity(CommercializationProjectEntity commercializationProjectEntity) {
        this.commercializationProjectEntity = commercializationProjectEntity;
    }

    public CommercializationStatusTypeEntity getCommercializationStatusTypeEntity() {
        return commercializationStatusTypeEntity;
    }

    public void setCommercializationStatusTypeEntity(CommercializationStatusTypeEntity commercializationStatusTypeEntity) {
        this.commercializationStatusTypeEntity = commercializationStatusTypeEntity;
    }

    public boolean isOutdated() {
        return outdated;
    }

    public void setOutdated(boolean outdated) {
        this.outdated = outdated;
    }
}
