package ai.unico.platform.store.entity;

import ai.unico.platform.store.api.annotation.RequiredField;
import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class CommercializationProjectEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commercializationId;

    @RequiredField
    private String name;

    @RequiredField(conditions = {"allRequired"}, fieldName = "Executive summary")
    private String executiveSummary;

    @RequiredField(conditions = {"allRequired"}, fieldName = "Use case")
    private String useCase;

    @RequiredField(conditions = {"allRequired"}, fieldName = "Pain description")
    private String painDescription;

    @RequiredField(conditions = {"allRequired"}, fieldName = "Competitive advantage")
    private String competitiveAdvantage;

    @RequiredField(conditions = {"allRequired"}, fieldName = "Technical principles")
    private String technicalPrinciples;

    @RequiredField(conditions = {"allRequired"}, fieldName = "Technology readiness level")
    private Integer technologyReadinessLevel;

    @RequiredField(conditions = {"allRequired"}, fieldName = "Project start date")
    private Date startDate;

    @RequiredField(conditions = {"allRequired"}, fieldName = "Project end date")
    private Date endDate;

    private Date statusDeadline;

    private boolean deleted;

    private String link;

    @RequiredField(conditions = {"allRequired"}, fieldName = "Investment lower estimate")
    private Integer commercializationInvestmentFrom;

    @RequiredField(conditions = {"allRequired"}, fieldName = "Investment upper estimate")
    private Integer commercializationInvestmentTo;

    @RequiredField(conditions = {"allRequired"}, fieldName = "Idea score")
    private Integer ideaScore;

    @ManyToOne
    @JoinColumn(name = "ownerOrganizationId")
    private OrganizationEntity ownerOrganizationEntity;

    @ManyToOne
    @JoinColumn(name = "commercializationPriorityId")
    @RequiredField(conditions = {"allRequired"}, fieldName = "Priority")
    private CommercializationPriorityEntity commercializationPriorityEntity;

    @ManyToOne
    @JoinColumn(name = "commercializationDomainId")
    @RequiredField(conditions = {"allRequired"}, fieldName = "Domain")
    private CommercializationDomainEntity commercializationDomainEntity;

    @ManyToOne
    @JoinColumn(name = "img_file_id")
    private FileEntity imgFile;

    @OneToMany(mappedBy = "commercializationProjectEntity")
    private Set<CommercializationProjectStatusEntity> commercializationProjectStatusEntities = new HashSet<>();

    @OneToMany(mappedBy = "commercializationProjectEntity")
    private Set<CommercializationTeamMemberEntity> commercializationTeamMemberEntities = new HashSet<>();

    @OneToMany(mappedBy = "commercializationProjectEntity")
    @RequiredField(conditions = {"allRequired"}, fieldName = "Keywords")
    private Set<CommercializationProjectKeywordEntity> commercializationProjectKeywordEntities = new HashSet<>();

    @OneToMany(mappedBy = "commercializationProjectEntity")
    private Set<CommercializationProjectOrganizationEntity> commercializationProjectOrganizationEntities = new HashSet<>();

    @OneToMany(mappedBy = "commercializationProjectEntity")
    @RequiredField(conditions = {"allRequired"}, fieldName = "Category")
    private Set<CommercializationProjectCategoryEntity> commercializationProjectCategoryEntities = new HashSet<>();

    public Long getCommercializationId() {
        return commercializationId;
    }

    public void setCommercializationId(Long commercializationId) {
        this.commercializationId = commercializationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExecutiveSummary() {
        return executiveSummary;
    }

    public void setExecutiveSummary(String executiveSummary) {
        this.executiveSummary = executiveSummary;
    }

    public String getUseCase() {
        return useCase;
    }

    public void setUseCase(String useCase) {
        this.useCase = useCase;
    }

    public String getPainDescription() {
        return painDescription;
    }

    public void setPainDescription(String painDescription) {
        this.painDescription = painDescription;
    }

    public String getCompetitiveAdvantage() {
        return competitiveAdvantage;
    }

    public void setCompetitiveAdvantage(String competitiveAdvantage) {
        this.competitiveAdvantage = competitiveAdvantage;
    }

    public String getTechnicalPrinciples() {
        return technicalPrinciples;
    }

    public void setTechnicalPrinciples(String technicalPrinciples) {
        this.technicalPrinciples = technicalPrinciples;
    }

    public Integer getTechnologyReadinessLevel() {
        return technologyReadinessLevel;
    }

    public void setTechnologyReadinessLevel(Integer technologyReadinessLevel) {
        this.technologyReadinessLevel = technologyReadinessLevel;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStatusDeadline() {
        return statusDeadline;
    }

    public void setStatusDeadline(Date statusDeadline) {
        this.statusDeadline = statusDeadline;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }


    public FileEntity getImgFile() {
        return imgFile;
    }

    public void setImgFile(FileEntity imgFile) {
        this.imgFile = imgFile;
    }

    public Integer getCommercializationInvestmentFrom() {
        return commercializationInvestmentFrom;
    }

    public void setCommercializationInvestmentFrom(Integer commercializationInvestmentFrom) {
        this.commercializationInvestmentFrom = commercializationInvestmentFrom;
    }

    public Integer getCommercializationInvestmentTo() {
        return commercializationInvestmentTo;
    }

    public void setCommercializationInvestmentTo(Integer commercializationInvestmentTo) {
        this.commercializationInvestmentTo = commercializationInvestmentTo;
    }

    public Set<CommercializationProjectStatusEntity> getCommercializationProjectStatusEntities() {
        return commercializationProjectStatusEntities;
    }

    public void setCommercializationProjectStatusEntities(Set<CommercializationProjectStatusEntity> commercializationProjectStatusEntities) {
        this.commercializationProjectStatusEntities = commercializationProjectStatusEntities;
    }

    public Set<CommercializationTeamMemberEntity> getCommercializationTeamMemberEntities() {
        return commercializationTeamMemberEntities;
    }

    public void setCommercializationTeamMemberEntities(Set<CommercializationTeamMemberEntity> commercializationTeamMemberEntities) {
        this.commercializationTeamMemberEntities = commercializationTeamMemberEntities;
    }

    public CommercializationPriorityEntity getCommercializationPriorityEntity() {
        return commercializationPriorityEntity;
    }

    public void setCommercializationPriorityEntity(CommercializationPriorityEntity commercializationPriorityEntity) {
        this.commercializationPriorityEntity = commercializationPriorityEntity;
    }

    public CommercializationDomainEntity getCommercializationDomainEntity() {
        return commercializationDomainEntity;
    }

    public void setCommercializationDomainEntity(CommercializationDomainEntity commercializationDomainEntity) {
        this.commercializationDomainEntity = commercializationDomainEntity;
    }

    public Set<CommercializationProjectKeywordEntity> getCommercializationProjectKeywordEntities() {
        return commercializationProjectKeywordEntities;
    }

    public void setCommercializationProjectKeywordEntities(Set<CommercializationProjectKeywordEntity> commercializationProjectKeywordEntities) {
        this.commercializationProjectKeywordEntities = commercializationProjectKeywordEntities;
    }

    public Integer getIdeaScore() {
        return ideaScore;
    }

    public void setIdeaScore(Integer ideaScore) {
        this.ideaScore = ideaScore;
    }

    public Set<CommercializationProjectOrganizationEntity> getCommercializationProjectOrganizationEntities() {
        return commercializationProjectOrganizationEntities;
    }

    public void setCommercializationProjectOrganizationEntities(Set<CommercializationProjectOrganizationEntity> commercializationProjectOrganizationEntities) {
        this.commercializationProjectOrganizationEntities = commercializationProjectOrganizationEntities;
    }

    public OrganizationEntity getOwnerOrganizationEntity() {
        return ownerOrganizationEntity;
    }

    public void setOwnerOrganizationEntity(OrganizationEntity ownerOrganizationEntity) {
        this.ownerOrganizationEntity = ownerOrganizationEntity;
    }

    public Set<CommercializationProjectCategoryEntity> getCommercializationProjectCategoryEntities() {
        return commercializationProjectCategoryEntities;
    }

    public void setCommercializationProjectCategoryEntities(Set<CommercializationProjectCategoryEntity> commercializationProjectCategoryEntities) {
        this.commercializationProjectCategoryEntities = commercializationProjectCategoryEntities;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
