package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.api.filter.MessageFilter;
import ai.unico.platform.store.dao.MessageDao;
import ai.unico.platform.store.entity.UserMessageEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.util.enums.OrderDirection;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class MessageDaoImpl implements MessageDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void saveMessage(UserMessageEntity userMessageEntity) {
        entityManager.persist(userMessageEntity);
    }

    @Override
    public UserMessageEntity findMessage(Long messageId) {
        return entityManager.find(UserMessageEntity.class, messageId);
    }

    @Override
    public List<UserMessageEntity> findMessages(Long userId, MessageFilter messageFilter) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserMessageEntity.class, "messageEntity");

        criteria.createAlias("messageEntity.userToEntity", "userTo");
        criteria.createAlias("messageEntity.userFromEntity", "userFrom");

        final String userTo = messageFilter.getUserFrom();
        final String userFrom = messageFilter.getUserFrom();

        if (userTo != null) {
            Restrictions.ilike("userTo.name", userTo, MatchMode.ANYWHERE);
        }

        if (userFrom != null) {
            Restrictions.ilike("userFrom.name", userFrom, MatchMode.ANYWHERE);
        }

        criteria.add(Restrictions.or(
                Restrictions.eq("userTo.userId", userId),
                Restrictions.eq("userFrom.userId", userId))
        );


        criteria.add(Restrictions.or(
                Restrictions.ilike("subject", messageFilter.getQuery()),
                Restrictions.ilike("content", messageFilter.getQuery())
        ));

        criteria.add(Restrictions.eq("deleted", false));

        HibernateUtil.setPagination(criteria, messageFilter.getPage(), messageFilter.getLimit());

        HibernateUtil.setOrder(criteria, "dateInsert", OrderDirection.DESC);
        HibernateUtil.setOrder(criteria, "read", OrderDirection.DESC);

        return (List<UserMessageEntity>) criteria.list();
    }
}
