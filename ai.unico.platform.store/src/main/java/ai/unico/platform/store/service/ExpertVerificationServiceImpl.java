package ai.unico.platform.store.service;

import ai.unico.platform.search.api.service.ExpertLookupService;
import ai.unico.platform.store.api.dto.ExpertClaimDto;
import ai.unico.platform.store.api.service.ClaimProfileService;
import ai.unico.platform.store.api.service.ExpertVerificationService;
import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.HashSet;

public class ExpertVerificationServiceImpl implements ExpertVerificationService {

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private ClaimProfileService claimProfileService;

    @Autowired
    private OrganizationStructureService organizationStructureService;

    @Override
    @Transactional
    public Long createUserFromExpertProfile(Long expertId, UserContext userContext) throws IOException {
        final ExpertLookupService service = ServiceRegistryUtil.getService(ExpertLookupService.class);
        final ExpertClaimDto expertToClaim = service.findExpertToClaim(expertId);
        final UserProfileEntity userProfileEntity = new UserProfileEntity();
        userProfileEntity.setName(expertToClaim.getFullName());
        userProfileEntity.setClaimable(true);
        userProfileEntity.setClaimedBy(null);
        userProfileEntity.setUserOrganizationEntities(new HashSet<>());
        userProfileEntity.setUserItemEntities(new HashSet<>());
        userProfileEntity.setUserExpertEntities(new HashSet<>());
        userProfileDao.createUserProfile(userProfileEntity);

        final Long userId = userProfileEntity.getUserId();
        claimProfileService.claimExpertProfile(expertId, userId, userContext);
        return userId;
    }
}
