package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.CommercializationCategoryDto;
import ai.unico.platform.store.api.dto.CommercializationCategoryPreviewDto;
import ai.unico.platform.store.api.service.CommercializationCategoryService;
import ai.unico.platform.store.dao.CommercializationCategoryDao;
import ai.unico.platform.store.entity.CommercializationCategoryEntity;
import ai.unico.platform.store.util.ReferenceDataUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

public class CommercializationCategoryServiceImpl implements CommercializationCategoryService {

    @Autowired
    private CommercializationCategoryDao commercializationCategoryDao;

    @Override
    @Transactional
    public List<CommercializationCategoryDto> findAvailableCommercializationCategories(boolean includeInactive) {
        final List<CommercializationCategoryEntity> all = commercializationCategoryDao.findAll();
        if (includeInactive) return all.stream().map(this::convert).collect(Collectors.toList());
        return ReferenceDataUtil.filterAndConvertReferenceData(all, this::convert);
    }

    @Override
    @Transactional
    public List<CommercializationCategoryPreviewDto> findAvailableCommercializationCategoriesPreview(boolean includeInactive) {
        final List<CommercializationCategoryEntity> all = commercializationCategoryDao.findAll();
        if (includeInactive) return all.stream().map(this::convertPreview).collect(Collectors.toList());
        return ReferenceDataUtil.filterAndConvertReferenceData(all, this::convertPreview);
    }


    @Override
    @Transactional
    public Long createCommercializationCategory(CommercializationCategoryDto commercializationCategoryDto, UserContext userContext) {
        final CommercializationCategoryEntity commercializationCategoryEntity = new CommercializationCategoryEntity();
        commercializationCategoryEntity.setCode(commercializationCategoryDto.getCommercializationCategoryCode());
        fillEntity(commercializationCategoryEntity, commercializationCategoryDto);
        TrackableUtil.fillCreate(commercializationCategoryEntity, userContext);
        commercializationCategoryDao.save(commercializationCategoryEntity);
        return commercializationCategoryEntity.getCommercializationCategoryId();
    }

    @Override
    @Transactional
    public void updateCommercializationCategory(Long categoryId, CommercializationCategoryDto commercializationCategoryDto, UserContext userContext) {
        final CommercializationCategoryEntity commercializationCategoryEntity = commercializationCategoryDao.findCategory(categoryId);
        if (commercializationCategoryEntity == null)
            throw new NonexistentEntityException(CommercializationCategoryEntity.class);
        fillEntity(commercializationCategoryEntity, commercializationCategoryDto);
        TrackableUtil.fillUpdate(commercializationCategoryEntity, userContext);
    }

    private CommercializationCategoryDto convert(CommercializationCategoryEntity entity) {
        final CommercializationCategoryDto dto = new CommercializationCategoryDto();
        dto.setCommercializationCategoryId(entity.getCommercializationCategoryId());
        dto.setCommercializationCategoryName(entity.getName());
        dto.setCommercializationCategoryCode(entity.getCode());
        dto.setCommercializationCategoryDescription(entity.getDescription());
        dto.setDeleted(entity.isDeleted());
        return dto;
    }

    private CommercializationCategoryPreviewDto convertPreview(CommercializationCategoryEntity entity) {
        final CommercializationCategoryPreviewDto dto = new CommercializationCategoryPreviewDto();
        dto.setCommercializationCategoryId(entity.getCommercializationCategoryId());
        dto.setCommercializationCategoryName(entity.getName());
        return dto;
    }

    private void fillEntity(CommercializationCategoryEntity commercializationCategoryEntity, CommercializationCategoryDto commercializationCategoryDto) {
        commercializationCategoryEntity.setName(commercializationCategoryDto.getCommercializationCategoryName());
        commercializationCategoryEntity.setDescription(commercializationCategoryDto.getCommercializationCategoryDescription());
        commercializationCategoryEntity.setDeleted(commercializationCategoryDto.isDeleted());
    }
}
