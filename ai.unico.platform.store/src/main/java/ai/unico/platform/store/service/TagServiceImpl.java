package ai.unico.platform.store.service;

import ai.unico.platform.store.api.service.TagService;
import ai.unico.platform.store.dao.TagDao;
import ai.unico.platform.store.entity.TagEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

public class TagServiceImpl implements TagService {

    @Autowired
    private TagDao tagDao;

    @Override
    @Transactional
    public List<String> findRelevantTags(String query) {
        final List<TagEntity> tagEntities = tagDao.findRelevantTags(query);
        return tagEntities.stream().map(TagEntity::getValue).collect(Collectors.toList());
    }
}
