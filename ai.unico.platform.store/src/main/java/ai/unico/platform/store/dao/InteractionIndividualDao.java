package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.InteractionIndividualEntity;

public interface InteractionIndividualDao {

    void save(InteractionIndividualEntity interactionIndividualEntity);
}
