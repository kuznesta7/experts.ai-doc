package ai.unico.platform.store.service;

import ai.unico.platform.store.api.service.CountryOfInterestService;
import ai.unico.platform.store.dao.CountryDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.CountryEntity;
import ai.unico.platform.store.entity.UserCountryOfInterestEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CountryOfInterestServiceImpl implements CountryOfInterestService {

    @Autowired
    private CountryDao countryDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Override
    @Transactional
    public Set<String> findCountryOfInterestCodesForUser(Long userId) {
        final List<UserCountryOfInterestEntity> userCountryOfInterestEntities = countryDao.findCountriesOfInterestForUser(userId);
        return userCountryOfInterestEntities.stream().map(UserCountryOfInterestEntity::getCountryEntity).map(CountryEntity::getCountryCode).collect(Collectors.toSet());
    }

    @Override
    @Transactional
    public void updateCountriesOfInterestForUser(Long userId, Set<String> countryCodes, UserContext userContext) {
        final UserProfileEntity userProfile = userProfileDao.findUserProfile(userId);
        if (userProfile.getUserCountryOfInterestEntities() == null)
            userProfile.setUserCountryOfInterestEntities(new HashSet<>());
        for (UserCountryOfInterestEntity userCountryOfInterestEntity : userProfile.getUserCountryOfInterestEntities()) {
            if (userCountryOfInterestEntity.isDeleted()) continue;
            final String countryCode = userCountryOfInterestEntity.getCountryEntity().getCountryCode();
            if (countryCodes.contains(countryCode)) {
                countryCodes.remove(countryCode);
            } else {
                userCountryOfInterestEntity.setDeleted(true);
            }
        }
        for (String countryCode : countryCodes) {
            final CountryEntity country = countryDao.findCountry(countryCode);
            final UserCountryOfInterestEntity userCountryOfInterestEntity = new UserCountryOfInterestEntity();
            userCountryOfInterestEntity.setCountryEntity(country);
            userCountryOfInterestEntity.setUserProfileEntity(userProfile);
            TrackableUtil.fillCreate(userCountryOfInterestEntity, userContext);
            countryDao.save(userCountryOfInterestEntity);
        }
    }
}
