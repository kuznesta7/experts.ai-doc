package ai.unico.platform.store.service;

import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.store.api.service.ProjectOrganizationService;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.dao.ProjectDao;
import ai.unico.platform.store.dao.ProjectOrganizationDao;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.entity.ProjectEntity;
import ai.unico.platform.store.entity.ProjectOrganizationEntity;
import ai.unico.platform.store.util.CollectionsUtil;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.enums.ProjectOrganizationRole;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ProjectOrganizationServiceImpl implements ProjectOrganizationService {

    @Autowired
    private ProjectOrganizationDao projectOrganizationDao;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private OrganizationStructureService organizationStructureService;

    @Override
    @Transactional
    public void addProjectOrganization(Long projectId, Long organizationId, boolean verify, ProjectOrganizationRole role, UserContext userContext) throws NonexistentEntityException {
        final ProjectEntity projectEntity = projectDao.find(projectId);
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        if (projectEntity == null) throw new NonexistentEntityException(ProjectEntity.class);
        if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
        if (projectOrganizationDao.findProjectOrganization(projectId, organizationId) != null)
            throw new EntityAlreadyExistsException("Organization already added");
        final ProjectOrganizationEntity projectOrganizationEntity = new ProjectOrganizationEntity();
        projectOrganizationEntity.setOrganizationEntity(organizationEntity);
        projectOrganizationEntity.setProjectEntity(projectEntity);
        projectOrganizationEntity.setProjectOrganizationRole(role);
        projectOrganizationEntity.setVerified(verify);
        TrackableUtil.fillCreate(projectOrganizationEntity, userContext);
        projectOrganizationDao.saveProjectOrganization(projectOrganizationEntity);
    }

    @Override
    @Transactional
    public void removeProjectOrganization(Long projectId, Long organizationId, UserContext userContext) throws NonexistentEntityException {
        final ProjectEntity projectEntity = projectDao.find(projectId);
        final Set<ProjectOrganizationEntity> projectOrganizationEntities = projectEntity.getProjectOrganizationEntities();
        final Set<Long> childOrganizationIds = organizationStructureService.findChildOrganizationIds(organizationId, false);
        for (ProjectOrganizationEntity projectOrganizationEntity : projectOrganizationEntities) {
            if (projectOrganizationEntity.isDeleted()) continue;
            final OrganizationEntity organizationEntity = projectOrganizationEntity.getOrganizationEntity();
            if (childOrganizationIds.contains(organizationEntity.getOrganizationId())) {
                projectOrganizationEntity.setDeleted(true);
                TrackableUtil.fillUpdate(projectOrganizationEntity, userContext);
            }
        }
    }

    @Override
    @Transactional
    public void verifyOrganizationProjects(Set<Long> projectIds, Long organizationId, boolean verify, UserContext userContext) {
        final List<ProjectEntity> projectEntities = projectDao.findJoinedByIds(projectIds);
        final Set<Long> childOrganizationIds = organizationStructureService.findChildOrganizationIds(organizationId, false);
        for (ProjectEntity projectEntity : projectEntities) {
            projectEntity.getProjectOrganizationEntities().stream()
                    .filter(x -> !x.isDeleted())
                    .filter(x -> childOrganizationIds.contains(HibernateUtil.getId(x, ProjectOrganizationEntity::getOrganizationEntity, OrganizationEntity::getOrganizationId)))
                    .forEach(x -> x.setVerified(verify));
        }
    }

    @Override
    @Transactional
    public void removeOrganizationProjects(Set<Long> projectIds, Long organizationId, UserContext userContext) {
        final List<ProjectEntity> projectEntities = projectDao.findJoinedByIds(projectIds);
        final Set<Long> childOrganizationIds = organizationStructureService.findChildOrganizationIds(organizationId, false);
        projectEntities.forEach(projectEntity -> doRemoveProjectOrganization(projectEntity, childOrganizationIds, userContext));
    }

    private void doRemoveProjectOrganization(ProjectEntity projectEntity, Set<Long> organizationIds, UserContext userContext) {
        for (ProjectOrganizationEntity projectOrganizationEntity : projectEntity.getProjectOrganizationEntities()) {
            if (projectOrganizationEntity.isDeleted()) continue;
            final OrganizationEntity organizationEntity = projectOrganizationEntity.getOrganizationEntity();
            if (organizationIds.contains(organizationEntity.getOrganizationId())) {
                projectOrganizationEntity.setDeleted(true);
                TrackableUtil.fillUpdate(projectOrganizationEntity, userContext);
            }
        }
    }

    @Override
    @Transactional
    public void switchOrganizations(Long projectId, Set<Long> organizationIds, UserContext userContext) {
        ProjectEntity projectEntity = projectDao.find(projectId);
        Set<Long> entityOrganizations = projectEntity.getProjectOrganizationEntities().stream()
                .filter(x -> ! x.isDeleted())
                .map(x -> x.getOrganizationEntity().getOrganizationId())
                .collect(Collectors.toSet());

        Set<Long> organizationToAdd = CollectionsUtil.setDifference(organizationIds, entityOrganizations);
        Set<Long> organizationToRemove = CollectionsUtil.setDifference(entityOrganizations, organizationIds);

        for (Long id: organizationToAdd) addProjectOrganization(projectId, id, true, ProjectOrganizationRole.PARTICIPANT, userContext);
        for (Long id: organizationToRemove) removeProjectOrganization(projectId, id, userContext);
    }
}
