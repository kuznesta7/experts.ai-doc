package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ItemNotRegisteredExpertEntity;
import ai.unico.platform.store.entity.UserExpertItemEntity;

import java.util.List;
import java.util.Set;

public interface UserExpertItemDao {

    void saveUserItem(UserExpertItemEntity userExpertItemEntity);

    void saveNotRegisteredExpert(ItemNotRegisteredExpertEntity notRegisteredExpert);

    ItemNotRegisteredExpertEntity findNotRegisteredExpert(Long expertId);

    ItemNotRegisteredExpertEntity findNotRegisteredExpertByItemIdAndName(Long itemId, String expertName);

    void removeNotRegisteredExpert(Long expertId);

    List<ItemNotRegisteredExpertEntity> findNotRegisteredExperts(Set<Long> expertIds);

    UserExpertItemEntity findForUser(Long userId, Long itemId);

    UserExpertItemEntity findForExpert(Long expertId, Long itemId);

    List<Long> findUserItems(Long userId);

    List<Long> findExpertItems(Long expertId);
}
