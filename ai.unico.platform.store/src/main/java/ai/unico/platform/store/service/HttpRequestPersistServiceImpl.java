package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.HttpRequestDto;
import ai.unico.platform.store.api.service.HttpRequestPersistService;
import ai.unico.platform.store.dao.HttpRequestDao;
import ai.unico.platform.store.entity.HttpRequestEntity;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HttpRequestPersistServiceImpl implements HttpRequestPersistService {

    @Autowired
    private HttpRequestDao httpRequestDao;

    private ExecutorService executorService;

    public void onInit() {
        executorService = Executors.newFixedThreadPool(2);
    }

    public void onDestroy() {
        executorService.shutdown();
    }

    @Override
    public void save(HttpRequestDto httpRequestDto) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                final HttpRequestEntity httpRequestEntity = new HttpRequestEntity();
                httpRequestEntity.setAccepted(httpRequestDto.getAccepted());
                httpRequestEntity.setDuration(httpRequestDto.getDuration());
                httpRequestEntity.setMethod(httpRequestDto.getMethod());
                httpRequestEntity.setPath(httpRequestDto.getPath());
                httpRequestEntity.setQuery(httpRequestDto.getQuery());
                httpRequestEntity.setResult(httpRequestDto.getResult());
                httpRequestEntity.setRemoteUser(httpRequestDto.getRemoteUser());
                httpRequestEntity.setSystem(httpRequestDto.getSystem());
                httpRequestDao.save(httpRequestEntity);
            }
        });
    }
}
