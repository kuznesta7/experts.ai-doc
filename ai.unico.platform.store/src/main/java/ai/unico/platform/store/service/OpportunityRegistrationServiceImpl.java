package ai.unico.platform.store.service;

import ai.unico.platform.search.api.service.OpportunityIndexService;
import ai.unico.platform.search.api.service.OpportunitySearchService;
import ai.unico.platform.store.api.dto.OpportunityRegisterDto;
import ai.unico.platform.store.api.service.OpportunityRegistrationService;
import ai.unico.platform.store.api.service.OpportunityService;
import ai.unico.platform.store.dao.*;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class OpportunityRegistrationServiceImpl implements OpportunityRegistrationService {

    private final OpportunityIndexService indexService = ServiceRegistryProxy.createProxy(OpportunityIndexService.class);
    private final Map<Long, OrganizationEntity> organizationCache = new HashMap<>();
    private final Map<String, TagEntity> tagCache = new HashMap<>();
    @Autowired
    private OpportunityDao opportunityDao;
    @Autowired
    private OpportunityService opportunityService;
    @Autowired
    private TagDao tagDao;
    @Autowired
    private OrganizationDao organizationDao;
    @Autowired
    private OpportunityTypeDao opportunityTypeDao;
    @Autowired
    private JobTypeDao jobTypeDao;
    @Autowired
    private UserProfileDao userProfileDao;

    @Override
    @Transactional
    public Long registerOpportunity(Long originalId, boolean clean, UserContext userContext) throws IOException {
        return registerOpportunity(IdUtil.convertToExtDataCode(originalId), clean, userContext);
    }

    @Override
    @Transactional
    public Long registerOpportunity(String originalCode, boolean clean, UserContext userContext) throws IOException {
        OpportunitySearchService opportunitySearchService = ServiceRegistryUtil.getService(OpportunitySearchService.class);
        OpportunityRegisterDto opportunity = opportunitySearchService.getOpportunity(originalCode);
        OpportunityEntity entity = convert(opportunity);
        opportunityDao.save(entity);
        TrackableUtil.fillCreate(entity, userContext);
        for (String kw : opportunity.getOpportunityKw()) {
            final OpportunityTagEntity opportunityTag = new OpportunityTagEntity();
            final TagEntity tagEntity = getTag(kw);
            opportunityTag.setTag(tagEntity);
            opportunityTag.setOpportunity(entity);
            opportunityDao.saveOpportunityTag(opportunityTag);
        }
        entity.setOpportunityTypeEntity(opportunityTypeDao.getOpportunityTypeEntity(opportunity.getOpportunityType()));
        Map<Long, JobTypeEntity> jobTypeEntityMap = jobTypeDao.getAllJobTypes().stream().collect(Collectors.toMap(JobTypeEntity::getId, x -> x));
        for (Long typeId : opportunity.getJobTypes()) {
            final OpportunityJobTypeEntity opportunityJobType = new OpportunityJobTypeEntity();
            final JobTypeEntity jobType = jobTypeEntityMap.get(typeId);
            opportunityJobType.setOpportunity(entity);
            opportunityJobType.setJobType(jobType);
            opportunityDao.saveOpportunityJob(opportunityJobType);
        }
        if (!opportunity.getOrganizationIds().isEmpty()) {
            OrganizationEntity organization = getOrganization(opportunity.getOrganizationIds().stream().findAny().get());
            entity.setOrganization(organization);
        }
        if (!opportunity.getExpertIds().isEmpty()) {
            String expertCode = opportunity.getExpertIds().stream().findAny().get();
            if (IdUtil.isIntId(expertCode)) {
                entity.setUserProfile(userProfileDao.findUserProfile(IdUtil.getIntDataId(expertCode)));
            } else {
                entity.setExpertId(IdUtil.getExtDataId(expertCode));
            }
        }
        //opportunityDao.save(entity);
        Long id = entity.getOpportunityId();
        OpportunityRegisterDto indexDto = opportunityService.getOpportunity(id);
        indexService.reindexOpportunity(indexDto);
        indexService.deleteOpportunity(IdUtil.convertToExtDataCode(indexDto.getOriginalId()));
        return id;
    }

    OpportunityEntity convert(OpportunityRegisterDto dto) {
        OpportunityEntity entity = new OpportunityEntity();
        entity.setOriginalOpportunityId(dto.getOriginalId());
        entity.setOpportunityName(dto.getOpportunityName());
        entity.setOpportunityDescription(dto.getOpportunityDescription());
        entity.setOpportunitySignupDate(dto.getOpportunitySignupDate());
        entity.setOpportunityLocation(dto.getOpportunityLocation());
        entity.setOpportunityWage(dto.getOpportunityWage());
        entity.setOpportunityTechReq(dto.getOpportunityTechReq());
        entity.setOpportunityFormReq(dto.getOpportunityFormReq());
        entity.setOpportunityOtherReq(dto.getOpportunityOtherReq());
        entity.setOpportunityBenefit(dto.getOpportunityBenefit());
        entity.setOpportunityJobStartDate(dto.getOpportunityJobStartDate());
        entity.setOpportunityExtLink(dto.getOpportunityExtLink());
        entity.setOpportunityHomeOffice(dto.getOpportunityHomeOffice());
        return entity;
    }

    private OrganizationEntity getOrganization(Long organizationId) {
        return organizationCache.computeIfAbsent(organizationId, organizationDao::find);
    }

    private TagEntity getTag(String tagValue) {
        return tagDao.findOrCreateTagByValues(tagValue);
        //return tagCache.computeIfAbsent(tagValue, tagDao::findOrCreateTagByValues);
    }
}
