package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.CommercializationProjectCategoryDao;
import ai.unico.platform.store.entity.CommercializationCategoryEntity;
import ai.unico.platform.store.entity.CommercializationProjectCategoryEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class CommercializationProjectCategoryDaoImpl implements CommercializationProjectCategoryDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void save(CommercializationProjectCategoryEntity commercializationProjectCategorytEntity) {
        entityManager.persist(commercializationProjectCategorytEntity);
    }

    @Override
    public CommercializationCategoryEntity findCategoryById(Long categoryId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(CommercializationCategoryEntity.class, "commercializationCategory");
        criteria.add(Restrictions.eq("commercializationCategory.commercializationCategoryId", categoryId));
        return (CommercializationCategoryEntity) criteria.list().get(0);
    }
}
