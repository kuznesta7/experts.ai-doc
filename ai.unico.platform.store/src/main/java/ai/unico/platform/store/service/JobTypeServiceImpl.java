package ai.unico.platform.store.service;

import ai.unico.platform.extdata.dto.CategoryDwhDto;
import ai.unico.platform.extdata.service.OpportunityCategoryDwhService;
import ai.unico.platform.store.api.dto.CategoryDto;
import ai.unico.platform.store.api.service.JobTypeService;
import ai.unico.platform.store.dao.JobTypeDao;
import ai.unico.platform.store.entity.JobTypeEntity;
import ai.unico.platform.util.ServiceRegistryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class JobTypeServiceImpl implements JobTypeService {

    List<CategoryDto> jobTypes;
    @Autowired
    private JobTypeDao jobTypeDao;

    @Override
    @Transactional
    public void updateJobTypesFromDwh() {
        final OpportunityCategoryDwhService service = ServiceRegistryUtil.getService(OpportunityCategoryDwhService.class);
        List<CategoryDwhDto> opportunityTypes = service.getJobTypes();
        Set<Long> types = jobTypeDao.getAllJobTypes().stream().map(JobTypeEntity::getId).filter(Objects::nonNull).collect(Collectors.toSet());
        List<CategoryDwhDto> newCategories = opportunityTypes.stream().filter(x -> !types.contains(x.getId())).collect(Collectors.toList());
        for (CategoryDwhDto c : newCategories) {
            JobTypeEntity entity = new JobTypeEntity();
            entity.setOriginalId(c.getId());
            entity.setName(c.getName());
            jobTypeDao.save(entity);
        }
    }

    @Override
    @Transactional
    public List<CategoryDto> getJobTypes() {
        if (jobTypes == null || jobTypes.isEmpty()) {
            jobTypes = jobTypeDao.getAllJobTypes().stream().map(this::convert).collect(Collectors.toList());
        }
        return jobTypes;
    }

    @Override
    @Transactional
    public Map<Long, Long> translateJobTypes() {
        return jobTypeDao.getAllJobTypes().stream()
                .filter(x -> x.getOriginalId() != null)
                .collect(Collectors.toMap(JobTypeEntity::getOriginalId, JobTypeEntity::getId));
    }

    @Override
    @Transactional
    public CategoryDto getJobType(Long id) {
        if (jobTypes == null)
            getJobTypes();
        return jobTypes.stream().filter(x -> Objects.equals(x.getId(), id)).findFirst().get();
    }


    private CategoryDto convert(JobTypeEntity entity) {
        CategoryDto dto = new CategoryDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        return dto;
    }
}
