package ai.unico.platform.store.service;

import ai.unico.platform.store.api.service.OrganizationLogoService;
import ai.unico.platform.store.dao.FileDao;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.entity.FileEntity;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.CacheKey;
import ai.unico.platform.util.ImgUtil;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.cache2k.Cache;
import org.cache2k.Cache2kBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.TimeUnit;

public class OrganizationLogoServiceImpl implements OrganizationLogoService {

    @Autowired
    private FileDao fileDao;

    @Autowired
    private OrganizationDao organizationDao;

    private final Cache<CacheKey, FileDto> cache = new Cache2kBuilder<CacheKey, FileDto>() {
    }
            .expireAfterWrite(30, TimeUnit.MINUTES)     // expire/refresh after 30 minutes
            .resilienceDuration(30, TimeUnit.SECONDS)   // cope with at most 30 seconds
            .permitNullValues(true)
            .refreshAhead(true)                         // keep fresh when expiring
            .loader(this::load)                         // auto populating function
            .build();


    @Override
    @Transactional
    public void saveImg(UserContext userContext, Long organizationId, ImgType type, FileDto fileDto) {
        ImgUtil.validateImageIsResizable(fileDto.getContent());
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
        final FileEntity imgFile;
        if (ImgType.COVER.equals(type)) {
            imgFile = organizationEntity.getOrganizationCoverEntity();
        } else {
            imgFile = organizationEntity.getOrganizationLogoEntity();
        }

        final FileEntity fileEntity;
        if (imgFile == null) {
            fileEntity = new FileEntity();
            TrackableUtil.fillCreate(fileEntity, userContext);
        } else {
            fileEntity = imgFile;
            TrackableUtil.fillUpdate(fileEntity, userContext);
        }

        fileEntity.setName(fileDto.getName());
        fileEntity.setContent(fileDto.getContent());
        fileDao.save(fileEntity);
        if (ImgType.COVER.equals(type)) {
            organizationEntity.setOrganizationCoverEntity(fileEntity);
        } else {
            organizationEntity.setOrganizationLogoEntity(fileEntity);
        }

        for (CacheKey key : cache.keys()) {
            if (key.getEntityId().equals(organizationId)) {
                cache.remove(key);
            }
        }
    }

    @Override
    @Transactional
    public FileDto getImg(Long userId, ImgType type, String size) {
        final String typeDefault = type != null ? type.toString() : ImgType.LOGO.toString();
        final CacheKey cacheKey = new CacheKey(userId, null, typeDefault, size);
        return cache.get(cacheKey);
    }

    private FileDto load(CacheKey cacheKey) {
        final Long organizationId = cacheKey.getEntityId();

        final FileEntity fileEntity;
        if (ImgType.COVER.toString().equals(cacheKey.getType())) {
            fileEntity = organizationDao.findOrganizationCoverImgFile(organizationId);
        } else {
            fileEntity = organizationDao.findOrganizationLogoFile(organizationId);
        }
        if (fileEntity == null) return null;

        final byte[] logoContent = fileEntity.getContent();

        final byte[] content;
        final String sizeCode = cacheKey.getSize();
        if (sizeCode != null) {
            final int size = ImgUtil.codeToSize(sizeCode);
            if (ImgType.COVER.toString().equals(cacheKey.getType())) {
                content = ImgUtil.resize(logoContent, size, null);
            } else {
                content = ImgUtil.resize(logoContent, size);
            }
        } else {
            content = logoContent;
        }
        return new FileDto(content, fileEntity.getName());
    }
}
