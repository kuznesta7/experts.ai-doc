package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ProfilePersonalNoteDao;
import ai.unico.platform.store.entity.ProfilePersonalNoteEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ProfilePersonalNoteDaoImpl implements ProfilePersonalNoteDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public ProfilePersonalNoteEntity findExpertNote(Long userFromId, Long expertId) {
        final Criteria criteria = prepareCriteria(userFromId);
        criteria.add(Restrictions.eq("expertId", expertId));
        criteria.add(Restrictions.eq("deleted", false));
        return (ProfilePersonalNoteEntity) criteria.uniqueResult();
    }

    @Override
    public ProfilePersonalNoteEntity findUserNote(Long userFromId, Long userToId) {
        final Criteria criteria = prepareCriteria(userFromId);
        criteria.add(Restrictions.eq("toUserEntity.userId", userToId));
        criteria.add(Restrictions.eq("deleted", false));
        return (ProfilePersonalNoteEntity) criteria.uniqueResult();
    }

    private Criteria prepareCriteria(Long userFromId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProfilePersonalNoteEntity.class, "personalNote");
        criteria.add(Restrictions.eq("fromUserEntity.userId", userFromId));
        return criteria;
    }

    @Override
    public void save(ProfilePersonalNoteEntity profilePersonalNoteEntity) {
        entityManager.persist(profilePersonalNoteEntity);
    }
}
