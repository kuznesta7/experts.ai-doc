package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class CommercializationProjectDocumentEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commercializationProjectDocumentId;

    @OneToOne
    @JoinColumn(name = "fileId")
    private FileEntity fileEntity;

    @ManyToOne
    @JoinColumn(name = "commercializationId")
    private CommercializationProjectEntity commercializationProjectEntity;

    private boolean deleted;

    public Long getCommercializationProjectDocumentId() {
        return commercializationProjectDocumentId;
    }

    public void setCommercializationProjectDocumentId(Long commercializationProjectDocumentId) {
        this.commercializationProjectDocumentId = commercializationProjectDocumentId;
    }

    public FileEntity getFileEntity() {
        return fileEntity;
    }

    public void setFileEntity(FileEntity fileEntity) {
        this.fileEntity = fileEntity;
    }

    public CommercializationProjectEntity getCommercializationProjectEntity() {
        return commercializationProjectEntity;
    }

    public void setCommercializationProjectEntity(CommercializationProjectEntity commercializationProjectEntity) {
        this.commercializationProjectEntity = commercializationProjectEntity;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
