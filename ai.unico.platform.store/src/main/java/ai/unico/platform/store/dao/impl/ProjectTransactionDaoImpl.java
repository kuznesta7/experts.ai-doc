package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ProjectTransactionDao;
import ai.unico.platform.store.entity.ProjectTransactionEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class ProjectTransactionDaoImpl implements ProjectTransactionDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public ProjectTransactionEntity findTransaction(Long transactionId) {
        return entityManager.find(ProjectTransactionEntity.class, transactionId);
    }

    @Override
    public void saveTransaction(ProjectTransactionEntity projectTransactionEntity) {
        entityManager.persist(projectTransactionEntity);
    }

    @Override
    public List<ProjectTransactionEntity> findProjectTransactions(Long projectId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProjectTransactionEntity.class, "projectTransaction");
        criteria.add(Restrictions.eq("projectTransaction.projectEntity.projectId", projectId));
        criteria.add(Restrictions.eq("deleted", false));
        return criteria.list();
    }
}
