package ai.unico.platform.store.entity;

import ai.unico.platform.store.util.TrackableUtil;

import javax.persistence.*;

@Entity
public class ProjectTagEntity extends TrackableUtil {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long projectTagId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "tag_id")
    private TagEntity tagEntity;

    @ManyToOne
    private ProjectEntity projectEntity;

    private boolean deleted;

    public Long getProjectTagId() {
        return projectTagId;
    }

    public void setProjectTagId(Long projectTagId) {
        this.projectTagId = projectTagId;
    }

    public TagEntity getTagEntity() {
        return tagEntity;
    }

    public void setTagEntity(TagEntity tagEntity) {
        this.tagEntity = tagEntity;
    }

    public ProjectEntity getProjectEntity() {
        return projectEntity;
    }

    public void setProjectEntity(ProjectEntity projectEntity) {
        this.projectEntity = projectEntity;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
