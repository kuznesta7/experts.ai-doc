package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ProjectProgramDao;
import ai.unico.platform.store.entity.ProjectProgramEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class ProjectProgramDaoImpl implements ProjectProgramDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public List<ProjectProgramEntity> findAll() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProjectProgramEntity.class, "program");
        criteria.add(Restrictions.eq("deleted", false));
        return criteria.list();
    }

    @Override
    public ProjectProgramEntity find(Long projectProgramId) {
        return entityManager.find(ProjectProgramEntity.class, projectProgramId);
    }

    @Override
    public List<ProjectProgramEntity> find(Set<Long> projectProgramIds) {
        if (projectProgramIds.isEmpty()) return Collections.emptyList();
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProjectProgramEntity.class, "program");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.in("projectProgramId", projectProgramIds));
        return criteria.list();
    }

    @Override
    public void save(ProjectProgramEntity projectProgramEntity) {
        entityManager.persist(projectProgramEntity);
    }

    @Override
    public List<ProjectProgramEntity> autocomplete(String query) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProjectProgramEntity.class, "program");
        criteria.add(Restrictions.ilike("projectProgramName", query, MatchMode.ANYWHERE));
        return criteria.list();
    }
}
