package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.OrganizationStructureDto;
import ai.unico.platform.store.api.enums.OrganizationRelationType;
import ai.unico.platform.store.api.exception.InvalidOrganizationStructureException;
import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.dao.OrganizationStructureDao;
import ai.unico.platform.store.entity.CountryEntity;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.entity.OrganizationStructureEntity;
import ai.unico.platform.store.entity.OrganizationStructureViewEntity;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.store.util.OrganizationUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.dto.OrganizationDto;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.cache2k.Cache;
import org.cache2k.Cache2kBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

public class OrganizationStructureServiceImpl implements OrganizationStructureService {

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private OrganizationStructureDao organizationStructureDao;

    private final Cache<Long, List<OrganizationStructure>> cache = new Cache2kBuilder<Long, List<OrganizationStructure>>() {
    }
            .expireAfterWrite(300, TimeUnit.MINUTES)     // expire/refresh after 30 minutes
            .resilienceDuration(30, TimeUnit.SECONDS)   // cope with at most 30 seconds
            .permitNullValues(false)
            .refreshAhead(false)                         // keep fresh when expiring
            .loader(this::load)                         // auto populating function
            .build();

    private List<OrganizationStructure> load(Long organizationId) {
        final List<OrganizationStructureViewEntity> structure = organizationStructureDao.findOrganizationStructure(organizationId);
        return structure.stream().map(this::convert).collect(Collectors.toList());
    }

    private OrganizationStructure convert(OrganizationStructureViewEntity entity) {
        final Long upperOrganizationId = entity.getUpperOrganizationId();
        final Long lowerOrganizationId = entity.getLowerOrganizationId();
        final OrganizationRelationType relationType = entity.getRelationType();
        return new OrganizationStructure(upperOrganizationId, lowerOrganizationId, relationType);
    }

    @Override
    @Transactional
    public List<OrganizationStructureDto> findOrganizationStructure(Set<Long> organizationIds, boolean members) {
        final Set<Long> rootOrganizationIds = organizationIds.stream().map(this::findRootOrganizationId).collect(Collectors.toSet());
        final Set<Long> organizationToLoadIds = findChildOrganizationIds(rootOrganizationIds, members);
        final List<OrganizationEntity> organizationEntities = organizationDao.findByIds(organizationToLoadIds);
        final Map<Long, OrganizationEntity> organizationEntityMap = organizationEntities.stream()
                .collect(Collectors.toMap(OrganizationEntity::getOrganizationId, Function.identity()));
        final List<OrganizationStructureDto> rootDtos = rootOrganizationIds.stream()
                .map(id -> findChildrenOrganizationStructure(id, organizationEntityMap, members))
                .collect(Collectors.toList());
        return rootDtos;
    }

    @Override
    @Transactional
    public OrganizationStructureDto findLowerOrganizationStructure(Long organizationId, boolean members) {
        final Set<Long> organizationToLoadIds = findChildOrganizationIds(Collections.singleton(organizationId), members);
        final List<OrganizationEntity> organizationEntities = organizationDao.findByIds(organizationToLoadIds);
        final Map<Long, OrganizationEntity> organizationEntityMap = organizationEntities.stream()
                .collect(Collectors.toMap(OrganizationEntity::getOrganizationId, Function.identity()));
        return findChildrenOrganizationStructure(organizationId, organizationEntityMap, members);
    }


    private OrganizationStructureDto findChildrenOrganizationStructure(Long organizationId, Map<Long, OrganizationEntity> organizationEntityMap, boolean members) {
        final OrganizationStructureDto organizationStructureDto = new OrganizationStructureDto();
        final OrganizationEntity organizationEntity = organizationEntityMap.get(organizationId);
        organizationEntityMap.remove(organizationId);
        if (organizationEntity == null) return null;
        final String countryCode = HibernateUtil.getId(organizationEntity, OrganizationEntity::getCountryEntity, CountryEntity::getCountryCode);
        organizationStructureDto.setOrganizationId(organizationEntity.getOrganizationId());
        organizationStructureDto.setOrganizationName(organizationEntity.getOrganizationName());
        organizationStructureDto.setOrganizationAbbrev(organizationEntity.getOrganizationAbbrev());
        organizationStructureDto.setCountryCode(countryCode);
        organizationStructureDto.setMemberOrganization(organizationEntity.getAllowedMembers());

        final List<OrganizationStructure> lowerStructure = cache.get(organizationId).stream()
                .filter(s -> s.upperOrganizationId.equals(organizationId))
                .filter(s -> members || !s.relationType.equals(OrganizationRelationType.MEMBER))
                .collect(Collectors.toList());

        for (OrganizationStructure organizationStructure : lowerStructure) {
            final OrganizationStructureDto childrenOrganizationStructure = findChildrenOrganizationStructure(organizationStructure.lowerOrganizationId, organizationEntityMap, members);
            if (childrenOrganizationStructure == null) continue;
            organizationStructureDto.getOrganizationUnits().add(childrenOrganizationStructure);
        }

        return organizationStructureDto;
    }

    @Override
    @Transactional
    public Set<Long> findChildOrganizationIds(Long organizationId, boolean members) {
        return findChildOrganizationIds(Collections.singleton(organizationId), members);
    }

    @Override
    @Transactional
    public Set<Long> findChildOrganizationIds(Set<Long> organizationIds, boolean members) {
        return doFindLowerStructureIds(organizationIds, new HashSet<>(), members);
    }

    private Set<Long> doFindLowerStructureIds(Set<Long> levelOrganizationIds, Set<Long> checkedOrganizationIds, boolean members) {
        if (levelOrganizationIds.isEmpty()) return Collections.emptySet();
        final Set<Long> checkedIdsCopy = new HashSet<>(checkedOrganizationIds);
        final Set<Long> result = new HashSet<>();
        checkedIdsCopy.addAll(levelOrganizationIds);
        result.addAll(levelOrganizationIds);

        // cache time boost
        Set<Long> notCached = levelOrganizationIds.stream().filter(k -> !cache.containsKey(k)).collect(Collectors.toSet());
        List<OrganizationStructureViewEntity> organizationStructureViewEntities = organizationStructureDao.findOrganizationStructure(notCached);
        notCached.forEach(organizationId -> {
            cache.put(organizationId, organizationStructureViewEntities.stream()
                    .filter(e -> Objects.equals(e.getLowerOrganizationId(), organizationId) || Objects.equals(e.getUpperOrganizationId(), organizationId))
                    .map(this::convert)
                    .collect(Collectors.toList()));
        });

        levelOrganizationIds.forEach(organizationId -> {
            final List<OrganizationStructure> organizationStructures = cache.get(organizationId);
            final Set<Long> lowerOrganizationIds = organizationStructures.stream()
                    .filter(s -> s.upperOrganizationId.equals(organizationId))
                    .filter(s -> members || !s.relationType.equals(OrganizationRelationType.MEMBER))
                    .map(s -> s.lowerOrganizationId).collect(Collectors.toSet());
            final Set<Long> filteredLowerOrganizationIds = lowerOrganizationIds.stream()
                    .filter(id -> !checkedIdsCopy.contains(id)).collect(Collectors.toSet());
            result.addAll(doFindLowerStructureIds(filteredLowerOrganizationIds, checkedIdsCopy, members));
        });
        return result;
    }

    @Override
    @Transactional
    public Long findRootOrganizationId(Long organizationId) {
        final Set<Long> checkedOrganizationIds = new HashSet<>();
        Long pointer = organizationId;
        do {
            final Long currentId = pointer;
            final Optional<OrganizationStructure> optionalOrganizationStructure = cache.get(pointer).stream()
                    .filter(s -> s.lowerOrganizationId.equals(currentId) && OrganizationRelationType.PARENT.equals(s.relationType))
                    .findFirst();
            if (checkedOrganizationIds.contains(pointer)) break;
            checkedOrganizationIds.add(pointer);
            if (optionalOrganizationStructure.isPresent()) {
                pointer = optionalOrganizationStructure.get().upperOrganizationId;
            }
        } while (pointer != null);
        return pointer;
    }

    @Override
    @Transactional
    public List<OrganizationDto> findRootOrganizationDtos(Set<Long> organizationIds) {
        final Set<Long> rootOrganizationIds = organizationIds.stream().map(this::findRootOrganizationId).collect(Collectors.toSet());
        final List<OrganizationEntity> organizationEntities = organizationDao.findByIds(rootOrganizationIds);
        return organizationEntities.stream().map(OrganizationUtil::convert).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<Long> findMemberOrganizations(Long organizationId) {
        return cache.get(organizationId).stream()
                .filter(x -> Objects.equals(x.upperOrganizationId, organizationId))
                .filter(x -> x.relationType == OrganizationRelationType.MEMBER)
                .map(x -> x.lowerOrganizationId)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Set<Long> findMyMemberships(Long organizationId) {
        return cache.get(organizationId).stream()
                .filter(x -> x.relationType == OrganizationRelationType.MEMBER)
                .filter(x -> Objects.equals(x.lowerOrganizationId, organizationId))
                .map(x -> x.upperOrganizationId)
                .collect(Collectors.toSet());
    }

    @Override
    @Transactional
    public Set<Long> findParents(Long organizationId) {
        return cache.get(organizationId).stream()
                .filter(x -> Objects.equals(x.lowerOrganizationId, organizationId))
                .filter(x -> x.relationType == OrganizationRelationType.PARENT)
                .filter(x -> !Objects.equals(x.upperOrganizationId, organizationId))
                .map(x -> x.upperOrganizationId)
                .collect(Collectors.toSet());
    }

    @Override
    @Transactional
    public void deleteOrganizationStructure(Long organizationId, Long childOrganizationId, UserContext userContext) {
        OrganizationStructureEntity organizationStructureEntity = organizationStructureDao.find(organizationId, childOrganizationId);

        if (organizationStructureEntity == null) {
            List<OrganizationEntity> sub = organizationDao.findBySubstituteId(childOrganizationId);
            if (sub.size() == 0)
                throw new NonexistentEntityException(OrganizationStructureEntity.class);
            for (OrganizationEntity oe : sub) {
                OrganizationStructureEntity organizationStructure = organizationStructureDao.find(organizationId, oe.getOrganizationId());
                if (organizationStructure != null) {
                    organizationStructure.setDeleted(true);
                    TrackableUtil.fillUpdate(organizationStructure, userContext);
                    cache.remove(childOrganizationId);
                }
            }
            cache.remove(organizationId);
        } else {
            organizationStructureEntity.setDeleted(true);
            TrackableUtil.fillUpdate(organizationStructureEntity, userContext);
            cache.remove(organizationId);
            cache.remove(childOrganizationId);
        }
    }

    @Override
    @Transactional
    public void addOrganizationStructure(Long organizationId, Long childOrganizationId, OrganizationRelationType relationType, UserContext userContext) {
        if (organizationId.equals(childOrganizationId))
            throw new InvalidOrganizationStructureException("Parent and child organization cannot be the same");
        final Long rootOrganizationId = findRootOrganizationId(organizationId);
        final Set<Long> treeOrganizationIds = findChildOrganizationIds(rootOrganizationId, false);
        if (treeOrganizationIds.contains(childOrganizationId))
            throw new InvalidOrganizationStructureException("Organization structure cannot contain loops");

        final OrganizationStructureEntity organizationStructureEntity = organizationStructureDao.find(organizationId, childOrganizationId);
        if (organizationStructureEntity != null)
            throw new EntityAlreadyExistsException(OrganizationStructureEntity.class);
        final OrganizationEntity parentOrganizationEntity = organizationDao.find(organizationId);
        final OrganizationEntity childOrganizationEntity = organizationDao.find(childOrganizationId);
        if (relationType.equals(OrganizationRelationType.MEMBER)) {
            if (!parentOrganizationEntity.getAllowedMembers())
                throw new InvalidOrganizationStructureException("Can not be member of nonmember organization");
            if (childOrganizationEntity.getAllowedMembers())
                throw new InvalidOrganizationStructureException("Can not nest member organizations");
        }
        if (parentOrganizationEntity == null || childOrganizationEntity == null)
            throw new NonexistentEntityException(OrganizationEntity.class);
        final OrganizationStructureEntity structureEntity = new OrganizationStructureEntity();
        structureEntity.setUpperOrganizationEntity(parentOrganizationEntity);
        structureEntity.setLowerOrganizationEntity(childOrganizationEntity);
        structureEntity.setRelationType(relationType);
        TrackableUtil.fillCreate(structureEntity, userContext);

        organizationStructureDao.save(structureEntity);
        cache.remove(organizationId);
        cache.remove(childOrganizationId);
    }

    @Override
    public void deleteCache() {
        cache.removeAll();
    }

    private static class OrganizationStructure {
        private Long upperOrganizationId;
        private Long lowerOrganizationId;
        private OrganizationRelationType relationType;

        public OrganizationStructure(Long upperOrganizationId, Long lowerOrganizationId, OrganizationRelationType relationType) {
            this.upperOrganizationId = upperOrganizationId;
            this.lowerOrganizationId = lowerOrganizationId;
            this.relationType = relationType;
        }
    }
}
