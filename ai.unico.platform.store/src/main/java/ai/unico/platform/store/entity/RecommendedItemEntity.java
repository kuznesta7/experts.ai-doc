package ai.unico.platform.store.entity;

import ai.unico.platform.store.api.enums.WidgetType;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "recommended_items")
public class RecommendedItemEntity {
    @Id
    @Column(name = "recomm_id", nullable = false, length = 256)
    private String id;

    @Column(name = "widget_type", length = 64)
    @Enumerated(EnumType.STRING)
    private WidgetType widgetType;

    @Column(name = "user_hash", length = 256)
    private String userHash;

    @Lob
    @Column(name = "recommendation")
    private String recommendation;

    @Column(name = "num_of_recomms")
    private Integer numOfRecomms;

    @Column(name = "recomm_time")
    private Date recommTime;

    public Date getRecommTime() {
        return recommTime;
    }

    public void setRecommTime(Date recommTime) {
        this.recommTime = recommTime;
    }

    public Integer getNumOfRecomms() {
        return numOfRecomms;
    }

    public void setNumOfRecomms(Integer numOfRecomms) {
        this.numOfRecomms = numOfRecomms;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

    public String getUserHash() {
        return userHash;
    }

    public void setUserHash(String userHash) {
        this.userHash = userHash;
    }

    public WidgetType getWidgetType() {
        return widgetType;
    }

    public void setWidgetType(WidgetType widgetType) {
        this.widgetType = widgetType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
