package ai.unico.platform.store.dao;

import java.util.Map;

public interface ExpertsStatisticsAdministrationDao {

    Map<String, Long> getCountriesUserClaims();

    Long getAllCountriesUserClaims();

}
