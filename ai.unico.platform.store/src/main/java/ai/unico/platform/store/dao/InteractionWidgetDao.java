package ai.unico.platform.store.dao;

import ai.unico.platform.store.api.enums.WidgetTab;
import ai.unico.platform.store.entity.InteractionWidgetEntity;

import java.util.List;
import java.util.Map;

public interface InteractionWidgetDao {

    void save(InteractionWidgetEntity interactionWidgetEntity);

    List<InteractionWidgetEntity> findOrganizationInteractions(Long organizationId);

    Map<String, Long> findOrganizationInteractionsCount(Long organizationId, WidgetTab widgetTab);
}
