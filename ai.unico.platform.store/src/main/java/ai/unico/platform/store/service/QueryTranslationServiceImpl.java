package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.QueryTranslationDto;
import ai.unico.platform.store.api.exception.MissingParameterException;
import ai.unico.platform.store.api.service.QueryTranslationService;
import ai.unico.platform.store.dao.QueryTranslationDao;
import ai.unico.platform.store.entity.QueryTranslationEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

public class QueryTranslationServiceImpl implements QueryTranslationService {

    @Autowired
    QueryTranslationDao queryTranslationDao;

    @Override
    @Transactional
    public QueryTranslationDto translate(String query) {
        QueryTranslationEntity translationEntity = queryTranslationDao.translate(query);
        if (translationEntity != null) {
            return convert(translationEntity);
        }
        return null;
    }

    @Override
    @Transactional
    public Long createTranslation(QueryTranslationDto translationDto) {
        QueryTranslationEntity translationEntity = new QueryTranslationEntity();
        fill(translationEntity, translationDto);
        queryTranslationDao.saveTranslations(translationEntity);
        return translationEntity.getTranslationId();
    }

    @Override
    @Transactional
    public void updateTranslation(QueryTranslationDto translationDto) {
        QueryTranslationEntity translationEntity = queryTranslationDao.find(translationDto.getTranslationId());
        fill(translationEntity, translationDto);
    }

    @Override
    @Transactional
    public List<QueryTranslationDto> findAllTranslations() {
        List<QueryTranslationEntity> translationEntities = queryTranslationDao.findAllTranslations();
        List<QueryTranslationDto> list = translationEntities.stream().map(this::convert).collect(Collectors.toList());
        return list;
    }

    @Override
    @Transactional
    public void deleteTranslation(Long translationId) {
        QueryTranslationEntity translationEntity = queryTranslationDao.find(translationId);
        translationEntity.setDeleted(true);
    }


    private QueryTranslationDto convert(QueryTranslationEntity translationEntity) {
        QueryTranslationDto translationDto = new QueryTranslationDto();
        translationDto.setTranslationId(translationEntity.getTranslationId());
        translationDto.setTranslationPhrase(translationEntity.getTranslationPhrase());
        translationDto.setQuery(translationEntity.getQuery());
        translationDto.setYearFrom(translationEntity.getYearFrom());
        translationDto.setYearTo(translationEntity.getYearTo());
        translationDto.setFullNameQuery(translationEntity.getFullNameQuery());
        translationDto.setCountryCodes(translationEntity.getCountryCodes());
        translationDto.setExcludeInvestProjects(translationEntity.isExcludeInvestProjects());
        translationDto.setExcludeOutcomes(translationEntity.isExcludeOutcomes());
        translationDto.setOrganizationIds(translationEntity.getOrganizationIds());
        translationDto.setTypeIds(translationEntity.getTypeIds());
        translationDto.setTypeGroupIds(translationEntity.getTypeGroupIds());
        return translationDto;
    }

    private void fill(QueryTranslationEntity translationEntity, QueryTranslationDto translationDto) {
        if (translationDto.getTranslationPhrase() == null || translationDto.getTranslationPhrase().isEmpty()) {
            throw new MissingParameterException("Translation phrase");
        }
        translationEntity.setTranslationPhrase(translationDto.getTranslationPhrase().toLowerCase());
        translationEntity.setQuery(translationDto.getQuery());
        translationEntity.setYearFrom(translationDto.getYearFrom());
        translationEntity.setYearTo(translationDto.getYearTo());
        translationEntity.setFullNameQuery(translationDto.getFullNameQuery());
        translationEntity.setCountryCodes(translationDto.getCountryCodes());
        translationEntity.setExcludeInvestProjects(translationEntity.isExcludeInvestProjects());
        translationEntity.setExcludeOutcomes(translationEntity.isExcludeOutcomes());
        translationEntity.setOrganizationIds(translationDto.getOrganizationIds());
        translationEntity.setTypeIds(translationDto.getTypeIds());
        translationEntity.setTypeGroupIds(translationDto.getTypeGroupIds());
    }

}
