package ai.unico.platform.store.service;

import ai.unico.platform.store.api.service.PlatformParamService;
import ai.unico.platform.store.dao.PlatformParamDao;
import ai.unico.platform.store.entity.PlatformParamEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PlatformParamServiceImpl implements PlatformParamService {

    @Autowired
    private PlatformParamDao platformParamDao;

    private Map<String, String> cache;

    @Override
    @Transactional
    public Map<String, String> findPlatformParams() {
//        if (cache != null) return cache;
        final List<PlatformParamEntity> platformParams = platformParamDao.findPlatformParams();
        cache = platformParams.stream().collect(Collectors.toMap(PlatformParamEntity::getParamKey, PlatformParamEntity::getParamValue));
        return cache;
    }

    @Override
    @Transactional
    public String findPlatformParamValue(String paramKey) {
        final Map<String, String> platformParams = findPlatformParams();
        if (platformParams.containsKey(paramKey)) return platformParams.get(paramKey);
        return null;
    }
}
