package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class ProfilePersonalNoteEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long profileNoteId;

    private String note;

    private Long expertId;

    private boolean deleted;

    @ManyToOne
    @JoinColumn(name = "from_user_id")
    private UserProfileEntity fromUserEntity;

    @ManyToOne
    @JoinColumn(name = "to_user_id")
    private UserProfileEntity toUserEntity;

    public Long getProfileNoteId() {
        return profileNoteId;
    }

    public void setProfileNoteId(Long profileNoteId) {
        this.profileNoteId = profileNoteId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public UserProfileEntity getFromUserEntity() {
        return fromUserEntity;
    }

    public void setFromUserEntity(UserProfileEntity fromUserEntity) {
        this.fromUserEntity = fromUserEntity;
    }

    public UserProfileEntity getToUserEntity() {
        return toUserEntity;
    }

    public void setToUserEntity(UserProfileEntity toUserEntity) {
        this.toUserEntity = toUserEntity;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
