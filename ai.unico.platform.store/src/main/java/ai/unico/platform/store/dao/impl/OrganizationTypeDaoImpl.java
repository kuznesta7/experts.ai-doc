package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.OrganizationTypeDao;
import ai.unico.platform.store.entity.OrganizationTypeEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class OrganizationTypeDaoImpl implements OrganizationTypeDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public OrganizationTypeEntity find(Long organizationTypeId) {
        return entityManager.find(OrganizationTypeEntity.class, organizationTypeId);
    }

    @Override
    public List<OrganizationTypeEntity> find() {
        Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationTypeEntity.class, "organizationType");
        return criteria.list();
    }
}
