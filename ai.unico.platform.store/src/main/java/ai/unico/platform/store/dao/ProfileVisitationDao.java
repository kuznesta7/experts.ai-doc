package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ProfileVisitationEntity;

import java.util.List;
import java.util.Set;

public interface ProfileVisitationDao {

    void saveVisitation(ProfileVisitationEntity visitationEntity);

    Long findNumberOfVisitations();

    List<ProfileVisitationEntity> findExpertProfileVisitations(Long expertId);

    List<ProfileVisitationEntity> findUserProfileVisitations(Long userId);

    Long countProfileVisitationForExperts(Set<Long> userIds, Set<Long> expertIds);

    List<ProfileVisitationStatistics> findMostVisitedProfilesForExperts(Set<Long> userIds, Set<Long> expertIds, Integer page, Integer limit);

    public static class ProfileVisitationStatistics {
        private Long expertId;

        private Long userId;

        private String name;

        private Long visitationsCount;

        public Long getExpertId() {
            return expertId;
        }

        public void setExpertId(Long expertId) {
            this.expertId = expertId;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getVisitationsCount() {
            return visitationsCount;
        }

        public void setVisitationsCount(Long visitationsCount) {
            this.visitationsCount = visitationsCount;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}
