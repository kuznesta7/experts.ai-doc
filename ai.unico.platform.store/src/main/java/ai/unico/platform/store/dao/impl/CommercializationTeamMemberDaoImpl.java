package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.api.service.ClaimProfileService;
import ai.unico.platform.store.dao.CommercializationTeamMemberDao;
import ai.unico.platform.store.entity.CommercializationTeamMemberEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class CommercializationTeamMemberDaoImpl implements CommercializationTeamMemberDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ClaimProfileService claimProfileService;

    @Override
    public void save(CommercializationTeamMemberEntity commercializationTeamMemberEntity) {
        entityManager.persist(commercializationTeamMemberEntity);
    }

    @Override
    public CommercializationTeamMemberEntity findForUser(Long userId, Long commercializationProjectId) {
        final Set<ClaimProfileService.Claim> claimsForUser = claimProfileService.findClaimsForUser(userId);
        final Set<Long> expertIds = claimsForUser.stream().map(ClaimProfileService.Claim::getExpertId).filter(Objects::nonNull).collect(Collectors.toSet());
        final Set<Long> userIds = claimsForUser.stream().map(ClaimProfileService.Claim::getUserId).filter(Objects::nonNull).collect(Collectors.toSet());

        final Criteria criteria = prepareCriteria(commercializationProjectId);
        final Disjunction disjunction = Restrictions.disjunction();
        if (!userIds.isEmpty()) {
            disjunction.add(Restrictions.in("userProfileEntity.userId", userIds));
        }
        if (!expertIds.isEmpty()) {
            Restrictions.in("expertId", expertIds);
        }
        criteria.add(disjunction);
        return (CommercializationTeamMemberEntity) criteria.uniqueResult();
    }

    @Override
    public CommercializationTeamMemberEntity findForExpert(Long expertId, Long commercializationProjectId) {
        final Criteria criteria = prepareCriteria(commercializationProjectId);
        criteria.add(Restrictions.eq("expertId", expertId));
        return (CommercializationTeamMemberEntity) criteria.uniqueResult();
    }

    private Criteria prepareCriteria(Long projectId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(CommercializationTeamMemberEntity.class, "member");
        criteria.add(Restrictions.eq("commercializationProjectEntity.commercializationId", projectId));
        criteria.add(Restrictions.eq("deleted", false));
        return criteria;
    }
}
