package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.api.dto.SearchStatisticsDto;
import ai.unico.platform.store.api.filter.SearchStatisticsFilter;
import ai.unico.platform.store.dao.SearchStatisticsDao;
import ai.unico.platform.store.entity.SearchStatisticsEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.store.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;

public class SearchStatisticsDaoImpl implements SearchStatisticsDao {

    private static final int numberOfHotTopic = 4;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public List<SearchStatisticsEntity> findHotTopics() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(SearchStatisticsEntity.class, "search_statistics");
        criteria.add(Restrictions.eq("hotTopic", true));
        List<SearchStatisticsEntity> statisticsEntities = criteria.list();
        Collections.shuffle(statisticsEntities);
        int number = Math.min(numberOfHotTopic, statisticsEntities.size());
        return statisticsEntities.subList(0, number);
    }

    @Override
    public List<SearchStatisticsEntity> findSearchedData(SearchStatisticsFilter filter) {
        final Criteria criteria = fillCriteria(filter);
        criteria.addOrder(Order.desc("uniqueSearches"));
        criteria.addOrder(Order.desc("searches"));
        criteria.addOrder(Order.desc("hotTopic"));
        HibernateUtil.setPagination(criteria, filter.getPage(), filter.getLimit());
        return criteria.list();
    }

    @Override
    public Long numberOfResults(SearchStatisticsFilter filter) {
        final Criteria criteria = fillCriteria(filter);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.list().get(0);
    }

    @Override
    public void updateSearchedData(SearchStatisticsDto statisticsDto) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(SearchStatisticsEntity.class, "search_statistics");
        criteria.add(Restrictions.eq("statisticId", statisticsDto.getStatisticId()));
        final SearchStatisticsEntity searchStatisticsEntity = (SearchStatisticsEntity) criteria.list().get(0);
        searchStatisticsEntity.setHidden(statisticsDto.isHidden());
        searchStatisticsEntity.setHotTopic(statisticsDto.isHotTopic());
    }

    @Override
    public Long createSearchData(SearchStatisticsDto statisticsDto) {
        SearchStatisticsEntity searchStatisticsEntity = new SearchStatisticsEntity();
        searchStatisticsEntity.setValue(statisticsDto.getValue());
        searchStatisticsEntity.setHidden(statisticsDto.isHidden());
        searchStatisticsEntity.setHotTopic(statisticsDto.isHotTopic());
        searchStatisticsEntity.setSearches(0);
        searchStatisticsEntity.setUniqueSearches(0);
        entityManager.persist(searchStatisticsEntity);
        return searchStatisticsEntity.getStatisticId();
    }

    private Criteria fillCriteria(SearchStatisticsFilter filter) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(SearchStatisticsEntity.class, "search_statistics");
        if (filter.isHidden()) {
            criteria.add(Restrictions.eq("hidden", true));
        } else {
            criteria.add(Restrictions.eq("hidden", false));
        }
        if (filter.isHotTopic()) {
            criteria.add(Restrictions.eq("hotTopic", true));
        }
        if (filter.getValue() != null) {
            criteria.add(Restrictions.ilike("value", filter.getValue(), MatchMode.ANYWHERE));
        }
        return criteria;
    }
}
