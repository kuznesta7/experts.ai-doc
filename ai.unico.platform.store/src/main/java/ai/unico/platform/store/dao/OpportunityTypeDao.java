package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.OpportunityTypeEntity;

import java.util.List;

public interface OpportunityTypeDao {
    List<OpportunityTypeEntity> getAllOpportunityTypes();

    OpportunityTypeEntity getOpportunityTypeEntity(Long id);

    void save(OpportunityTypeEntity opportunityTypeEntity);
}
