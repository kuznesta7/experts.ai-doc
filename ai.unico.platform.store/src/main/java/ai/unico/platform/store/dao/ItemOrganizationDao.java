package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ItemOrganizationEntity;

import java.util.List;
import java.util.Set;

public interface ItemOrganizationDao {

    void saveItemOrganization(ItemOrganizationEntity itemOrganizationEntity);

    List<ItemOrganizationEntity> findItemOrganizations(Long itemId);

    ItemOrganizationEntity findItemOrganization(Long organizationId, Long itemId);

    List<ItemOrganizationEntity> findItemOrganizations(Long organizationId, Set<Long> itemIds);

    List<ItemOrganizationEntity> findExternalItemOrganization(Set<Long> itemIds);

}
