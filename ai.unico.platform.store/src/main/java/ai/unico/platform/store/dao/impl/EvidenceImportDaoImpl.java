package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.EvidenceImportDao;
import ai.unico.platform.store.entity.EvidenceImportEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class EvidenceImportDaoImpl implements EvidenceImportDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(EvidenceImportEntity evidenceImportEntity) {
        entityManager.persist(evidenceImportEntity);
    }

    @Override
    public EvidenceImportEntity getEvidenceImportEntity(Long evidenceImportId) {
        return entityManager.find(EvidenceImportEntity.class, evidenceImportId);
    }
}
