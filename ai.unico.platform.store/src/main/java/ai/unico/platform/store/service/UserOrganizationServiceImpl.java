package ai.unico.platform.store.service;

import ai.unico.platform.search.api.dto.EvidenceItemPreviewWithExpertsDto;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.service.EvidenceItemService;
import ai.unico.platform.search.api.service.ItemAffiliationService;
import ai.unico.platform.search.api.service.ItemIndexService;
import ai.unico.platform.search.api.wrapper.EvidenceItemWrapper;
import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.service.*;
import ai.unico.platform.store.dao.CountryDao;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.dao.UserOrganizationDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.entity.UserOrganizationEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.store.util.OrganizationIndexUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.store.util.UserProfileIndexUtil;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class UserOrganizationServiceImpl implements UserOrganizationService {

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private UserOrganizationDao userOrganizationDao;

    @Autowired
    private OrganizationStructureService organizationStructureService;

    @Autowired
    private OrganizationService organizationService;

    @Autowired
    private CountryDao countryDao;

    @Autowired
    private UserItemService userItemService;

    @Autowired
    private ItemOrganizationService itemOrganizationService;


    private ItemAffiliationService itemAffiliationService = ServiceRegistryProxy.createProxy(ItemAffiliationService.class);

    @Override
    @Transactional
    public void updateOrganizationsForUser(Long userId, List<OrganizationBaseDto> organizationDtos, UserContext userContext) throws NonexistentEntityException {
        final UserProfileEntity userProfileEntity = updateUserOrganizations(userId, organizationDtos, userContext);
        UserProfileIndexUtil.indexUserProfile(userProfileEntity);
        try {
            itemAffiliationService.updateItemsForUser(Collections.singleton(userId), organizationDtos.stream().map(OrganizationBaseDto::getOrganizationId).collect(Collectors.toSet()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void addOrganization(Long userId, Long organizationId, boolean verify, UserContext userContext) {
        final UserProfileEntity userProfile = userProfileDao.findUserProfile(userId);
        if (userProfile == null) throw new NonexistentEntityException(UserProfileEntity.class);
        final UserOrganizationEntity userOrganizationEntity = userOrganizationDao.findNotDeleted(organizationId, userId);
        if (userOrganizationEntity == null) {
            final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
            if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
            final UserOrganizationEntity newUserOrganizationEntity = new UserOrganizationEntity();
            newUserOrganizationEntity.setUserProfileEntity(userProfile);
            newUserOrganizationEntity.setOrganizationEntity(organizationEntity);
            TrackableUtil.fillCreate(newUserOrganizationEntity, userContext);
            userProfile.getUserOrganizationEntities().add(newUserOrganizationEntity);
            newUserOrganizationEntity.setVerified(verify);
            userOrganizationDao.saveUserOrganization(newUserOrganizationEntity);
        } else {
            userOrganizationEntity.setVerified(verify);
            TrackableUtil.fillUpdate(userOrganizationEntity, userContext);
        }

        UserProfileIndexUtil.indexUserProfile(userProfileDao.findUserProfile(userId));
        try {
            itemAffiliationService.updateItemsForUser(Collections.singleton(userId), organizationId, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void removeOrganization(Set<Long> userIds, Long organizationId, UserContext userContext) {
        final List<UserProfileEntity> userProfiles = userProfileDao.findByIds(userIds);
        for (UserProfileEntity userProfile : userProfiles) {
            final Set<UserOrganizationEntity> userOrganizationEntities = userProfile.getUserOrganizationEntities();
            final Set<Long> allOrganizationUnitIds = organizationStructureService.findChildOrganizationIds(organizationId, false);
            for (UserOrganizationEntity userOrganizationEntity : userOrganizationEntities) {
                if (userOrganizationEntity.isDeleted()) continue;
                final OrganizationEntity organizationEntity = userOrganizationEntity.getOrganizationEntity();
                final Long substituteId = HibernateUtil.getId(organizationEntity, OrganizationEntity::getSubstituteOrganizationEntity, OrganizationEntity::getOrganizationId);
                if (allOrganizationUnitIds.contains(organizationEntity.getOrganizationId()) || (substituteId != null && allOrganizationUnitIds.contains(substituteId))) {
                    userOrganizationEntity.setDeleted(true);
                    TrackableUtil.fillUpdate(userOrganizationEntity, userContext);
                }
            }
        }
        try {
            itemAffiliationService.updateItemsForUser(userIds, organizationId, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void setFavoriteUserUserOrganization(Long userId, Long organizationId, boolean favorite, UserContext userContext) throws NonexistentEntityException {
        final UserOrganizationEntity userOrganizationEntity = userOrganizationDao.findNotDeleted(organizationId, userId);
        if (userOrganizationEntity == null) throw new NonexistentEntityException(UserOrganizationEntity.class);
        userOrganizationEntity.setFavorite(favorite);
        TrackableUtil.fillUpdate(userOrganizationEntity, userContext);
        final UserProfileEntity userProfileEntity = userOrganizationEntity.getUserProfileEntity();
        UserProfileIndexUtil.indexUserProfile(userProfileEntity);
    }

    private UserProfileEntity updateUserOrganizations(Long userId, List<OrganizationBaseDto> organizationDtos, UserContext userContext) throws NonexistentEntityException {
        final UserProfileEntity userProfileEntity = userProfileDao.findUserProfile(userId);
        if (userProfileEntity == null) throw new NonexistentEntityException(UserProfileEntity.class);
        fillUserOrganizationEntities(userProfileEntity, organizationDtos, userContext);
        return userProfileEntity;
    }

    private void fillUserOrganizationEntity(OrganizationBaseDto dto, UserOrganizationEntity userOrganizationEntity, UserContext userContext) {
        userOrganizationEntity.setVisible(dto.isVisible());
        userOrganizationEntity.setActive(dto.isActive());
        TrackableUtil.fillUpdate(userOrganizationEntity, userContext);
    }

    private void fillUserOrganizationEntities(UserProfileEntity userProfileEntity, List<OrganizationBaseDto> organizationDtos, UserContext userContext) {
        Map<Long, OrganizationBaseDto> organizationDtoMap = organizationDtos.stream()
                .collect(Collectors.toMap(OrganizationBaseDto::getOrganizationId, Function.identity()));
        Set<Long> toDelete = new HashSet<>();

        // update existing
        for (UserOrganizationEntity userOrganizationEntity : userProfileEntity.getUserOrganizationEntities()) {
            Long id = userOrganizationEntity.getOrganizationEntity().getOrganizationId();
            if (organizationDtoMap.containsKey(id)) {  // update
                final OrganizationBaseDto organizationDto = organizationDtoMap.get(id);
                userOrganizationEntity.setDeleted(false);
                fillUserOrganizationEntity(organizationDto, userOrganizationEntity, userContext);
                TrackableUtil.fillUpdate(userOrganizationEntity, userContext);
                toDelete.add(id);
            } else {  // delete
                userOrganizationEntity.setDeleted(true);
                TrackableUtil.fillUpdate(userOrganizationEntity, userContext);
            }
        }
        organizationDtoMap.keySet().removeAll(toDelete);

        // add new
        for (OrganizationBaseDto organizationDto : organizationDtoMap.values()) {
            final Long organizationId = organizationDto.getOrganizationId();
            final OrganizationEntity organizationEntity;
            if (organizationId == null) {
                organizationEntity = new OrganizationEntity();
                organizationEntity.setOrganizationName(organizationDto.getOrganizationName());
                organizationEntity.setOrganizationAbbrev(organizationDto.getOrganizationAbbrev());
                TrackableUtil.fillCreate(organizationEntity, userContext);
                organizationDao.saveOrganization(organizationEntity);
                OrganizationIndexUtil.indexOrganization(organizationEntity);
            } else {
                organizationEntity = organizationDao.find(organizationId);
            }

            final UserOrganizationEntity userOrganizationEntity = new UserOrganizationEntity();
            userOrganizationEntity.setUserProfileEntity(userProfileEntity);
            userOrganizationEntity.setOrganizationEntity(organizationEntity);
            userOrganizationEntity.setDeleted(false);
            fillUserOrganizationEntity(organizationDto, userOrganizationEntity, userContext);
            TrackableUtil.fillCreate(userOrganizationEntity, userContext);
            userProfileEntity.getUserOrganizationEntities().add(userOrganizationEntity);
        }
    }

    @Override
    @Transactional
    public void verifyUsers(Set<Long> userIds, Long organizationId, boolean verify, UserContext userContext) {
        final List<UserProfileEntity> userProfileEntities = userProfileDao.findByIds(userIds);
        final Set<Long> allOrganizationUnitIds = organizationStructureService.findChildOrganizationIds(organizationId, false);
        for (UserProfileEntity userProfile : userProfileEntities) {
            for (UserOrganizationEntity userOrganizationEntity : userProfile.getUserOrganizationEntities()) {
                if (userOrganizationEntity.isDeleted()) continue;
                final OrganizationEntity organizationEntity = userOrganizationEntity.getOrganizationEntity();
                final Long substituteId = HibernateUtil.getId(organizationEntity, OrganizationEntity::getSubstituteOrganizationEntity, OrganizationEntity::getOrganizationId);
                if (allOrganizationUnitIds.contains(organizationEntity.getOrganizationId()) || (substituteId != null && allOrganizationUnitIds.contains(substituteId))) {
                    userOrganizationEntity.setVerified(verify);
                    TrackableUtil.fillUpdate(userOrganizationEntity, userContext);
                }
            }
        }
    }

    @Override
    @Transactional
    public List<Long> addUserItemsToOrganization(Long userId, Long organizationId, boolean verify, UserContext userContext) throws IOException {
        final EvidenceFilter evidenceFilter = new EvidenceFilter();
        evidenceFilter.setLimit(2000);
        final EvidenceItemService service = ServiceRegistryUtil.getService(EvidenceItemService.class);
        assert service != null;
        evidenceFilter.setExpertCodes(Collections.singleton("ST" + userId));
        EvidenceItemWrapper evidenceItemWrapper = service.findAnalyticsItems(evidenceFilter);
        List<EvidenceItemPreviewWithExpertsDto> evidenceItemPreviewWithExpertsDtos = evidenceItemWrapper.getItemPreviewDtos();
        List<Long> itemIds = evidenceItemPreviewWithExpertsDtos
                .stream()
                .map(EvidenceItemPreviewWithExpertsDto::getItemId)
                .collect(Collectors.toList());
        Set<Long> externalIds = evidenceItemPreviewWithExpertsDtos
                .stream()
                .filter(x -> IdUtil.isExtId(x.getItemCode()))
                .map(x -> IdUtil.getExtDataId(x.getItemCode()))
                .collect(Collectors.toSet());
        itemOrganizationService.addExternalItemOrganization(organizationId, externalIds, verify, userContext);
        itemIds.removeAll(Collections.singleton(null));
        if (! itemIds.isEmpty())
            itemOrganizationService.addItemOrganization(organizationId, new HashSet<>(itemIds), verify, userContext);
        ItemIndexService itemIndexService = ServiceRegistryUtil.getService(ItemIndexService.class);
        assert itemIndexService != null;
        for(Long id : externalIds) {
            itemIndexService.appendOrganizations(IdUtil.DATA_WAREHOUSE_PREFIX + id);
        }
        return itemIds;
    }

    @Override
    @Transactional
    public void retireUserOrganization(Long userId, Long organizationId, boolean retired, UserContext userContext) {
        UserOrganizationEntity userOrganizationEntity = userOrganizationDao.find(organizationId, userId);
        if (userOrganizationEntity == null) {
            userOrganizationEntity = new UserOrganizationEntity();
            userOrganizationEntity.setOrganizationEntity(organizationDao.find(organizationId));
            userOrganizationEntity.setUserProfileEntity(userProfileDao.findUserProfile(userId));
        } else {
            // in case the user has the user-organization entity deleted, but he has an active entity with a sub-organization
            if (userOrganizationEntity.isDeleted()) {
                userOrganizationEntity.setDeleted(false);
                userOrganizationEntity.setVisible(true);
            }
        }
        userOrganizationEntity.setRetired(retired);
        TrackableUtil.fillUpdate(userOrganizationEntity, userContext);
        userOrganizationDao.saveUserOrganization(userOrganizationEntity);
    }

    @Override
    @Transactional
    public void changeExpertOrganizationVisibility(Long userId, Long organizationId, boolean visible, UserContext userContext) {
        UserOrganizationEntity userOrganizationEntity = userOrganizationDao.find(organizationId, userId);
        if(userOrganizationEntity == null) {
            userOrganizationEntity = new UserOrganizationEntity();
            userOrganizationEntity.setOrganizationEntity(organizationDao.find(organizationId));
            userOrganizationEntity.setUserProfileEntity(userProfileDao.findUserProfile(userId));
        }
        userOrganizationEntity.setVisible(visible);
        TrackableUtil.fillUpdate(userOrganizationEntity, userContext);
        userOrganizationDao.saveUserOrganization(userOrganizationEntity);
    }

    @Override
    @Transactional
    public void changeExpertOrganizationActivity(Long userId, Long organizationId, boolean active, UserContext userContext) {
        UserOrganizationEntity userOrganizationEntity = userOrganizationDao.find(organizationId, userId);
        if(userOrganizationEntity == null) {
            userOrganizationEntity = new UserOrganizationEntity();
            userOrganizationEntity.setOrganizationEntity(organizationDao.find(organizationId));
            userOrganizationEntity.setUserProfileEntity(userProfileDao.findUserProfile(userId));
        }
        userOrganizationEntity.setActive(active);
        TrackableUtil.fillUpdate(userOrganizationEntity, userContext);
        userOrganizationDao.saveUserOrganization(userOrganizationEntity);
    }
}
