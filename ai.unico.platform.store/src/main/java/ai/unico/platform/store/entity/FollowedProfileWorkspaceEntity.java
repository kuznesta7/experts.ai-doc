package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class FollowedProfileWorkspaceEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long followedProfileWorkspaceId;

    @ManyToOne
    @JoinColumn(name = "user_workspace_id")
    private UserWorkspaceEntity userWorkspaceEntity;

    @ManyToOne
    @JoinColumn(name = "followed_profile_id")
    private FollowedProfileEntity followedProfileEntity;

    private boolean deleted;

    public Long getFollowedProfileWorkspaceId() {
        return followedProfileWorkspaceId;
    }

    public void setFollowedProfileWorkspaceId(Long followedProfileWorkspaceId) {
        this.followedProfileWorkspaceId = followedProfileWorkspaceId;
    }

    public UserWorkspaceEntity getUserWorkspaceEntity() {
        return userWorkspaceEntity;
    }

    public void setUserWorkspaceEntity(UserWorkspaceEntity userWorkspaceEntity) {
        this.userWorkspaceEntity = userWorkspaceEntity;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public FollowedProfileEntity getFollowedProfileEntity() {
        return followedProfileEntity;
    }

    public void setFollowedProfileEntity(FollowedProfileEntity followedProfileEntity) {
        this.followedProfileEntity = followedProfileEntity;
    }
}
