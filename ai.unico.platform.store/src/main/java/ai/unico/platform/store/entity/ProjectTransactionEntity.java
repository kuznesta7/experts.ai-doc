package ai.unico.platform.store.entity;

import ai.unico.platform.store.api.enums.Currency;
import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;
import java.util.Date;

@Entity
public class ProjectTransactionEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long projectTransactionId;

    private Date transactionDate;

    private Double amount;

    @Enumerated(EnumType.STRING)
    private Currency currency;

    @ManyToOne
    @JoinColumn(name = "organization_from_id")
    private OrganizationEntity organizationFrom;

    @ManyToOne
    @JoinColumn(name = "organization_to_id")
    private OrganizationEntity organizationTo;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private ProjectEntity projectEntity;

    private String transactionNote;

    private boolean deleted;

    public Long getProjectTransactionId() {
        return projectTransactionId;
    }

    public void setProjectTransactionId(Long projectTransactionId) {
        this.projectTransactionId = projectTransactionId;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public OrganizationEntity getOrganizationFrom() {
        return organizationFrom;
    }

    public void setOrganizationFrom(OrganizationEntity organizationFrom) {
        this.organizationFrom = organizationFrom;
    }

    public OrganizationEntity getOrganizationTo() {
        return organizationTo;
    }

    public void setOrganizationTo(OrganizationEntity organizationTo) {
        this.organizationTo = organizationTo;
    }

    public ProjectEntity getProjectEntity() {
        return projectEntity;
    }

    public void setProjectEntity(ProjectEntity projectEntity) {
        this.projectEntity = projectEntity;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getTransactionNote() {
        return transactionNote;
    }

    public void setTransactionNote(String transactionNote) {
        this.transactionNote = transactionNote;
    }
}
