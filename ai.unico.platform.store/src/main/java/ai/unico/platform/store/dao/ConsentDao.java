package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ConsentEntity;

import java.util.List;

public interface ConsentDao {

    List<ConsentEntity> findAllConsents();

    ConsentEntity findConsent(Long consentId);
}
