package ai.unico.platform.store.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "equipment_type")
public class EquipmentTypeEntity {
    @Id
    @Column(name = "equipment_type_id", nullable = false)
    private Long id;

    @Column(name = "type_name_en", nullable = false, length = 256)
    private String typeNameEn;

    public String getTypeNameEn() {
        return typeNameEn;
    }

    public void setTypeNameEn(String typeNameEn) {
        this.typeNameEn = typeNameEn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
