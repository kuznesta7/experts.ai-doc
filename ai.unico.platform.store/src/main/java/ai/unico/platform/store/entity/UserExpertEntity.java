package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class UserExpertEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userExpertId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserProfileEntity userProfileEntity;

    private Long expertId;

    private Boolean deleted = false;

    public Long getUserExpertId() {
        return userExpertId;
    }

    public void setUserExpertId(Long userExpertId) {
        this.userExpertId = userExpertId;
    }

    public UserProfileEntity getUserProfileEntity() {
        return userProfileEntity;
    }

    public void setUserProfileEntity(UserProfileEntity userProfileEntity) {
        this.userProfileEntity = userProfileEntity;
    }

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
