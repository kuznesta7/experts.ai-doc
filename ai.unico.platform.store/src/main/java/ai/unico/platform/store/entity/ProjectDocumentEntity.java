package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class ProjectDocumentEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long projectDocumentId;

    @OneToOne
    @JoinColumn(name = "fileId")
    private FileEntity fileEntity;

    @ManyToOne
    @JoinColumn(name = "projectId")
    private ProjectEntity projectEntity;

    private boolean deleted;

    public Long getProjectDocumentId() {
        return projectDocumentId;
    }

    public void setProjectDocumentId(Long projectDocumentId) {
        this.projectDocumentId = projectDocumentId;
    }

    public FileEntity getFileEntity() {
        return fileEntity;
    }

    public void setFileEntity(FileEntity fileEntity) {
        this.fileEntity = fileEntity;
    }

    public ProjectEntity getProjectEntity() {
        return projectEntity;
    }

    public void setProjectEntity(ProjectEntity projectEntity) {
        this.projectEntity = projectEntity;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

}
