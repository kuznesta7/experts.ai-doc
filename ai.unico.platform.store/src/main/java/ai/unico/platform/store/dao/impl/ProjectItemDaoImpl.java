package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ProjectItemDao;
import ai.unico.platform.store.entity.ProjectItemEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class ProjectItemDaoImpl implements ProjectItemDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public ProjectItemEntity findProjectItem(Long projectId, Long itemId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProjectItemEntity.class, "projectItem");
        criteria.add(Restrictions.eq("projectItem.deleted", false));
        criteria.add(Restrictions.eq("projectItem.projectEntity.projectId", projectId));
        criteria.add(Restrictions.eq("projectItem.itemEntity.itemId", itemId));
        return (ProjectItemEntity) criteria.uniqueResult();
    }

    @Override
    public void saveProjectItem(ProjectItemEntity projectItemEntity) {
        entityManager.persist(projectItemEntity);
    }

    @Override
    public List<ProjectItemEntity> findAllWithOriginalItemIds(Set<Long> originalItemIds) {
        if (originalItemIds.isEmpty()) return Collections.emptyList();
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProjectItemEntity.class, "projectItem");
        criteria.add(Restrictions.eq("projectItem.deleted", false));
        criteria.add(Restrictions.isNull("projectItem.itemEntity"));
        criteria.add(Restrictions.in("projectItem.originalItemId", originalItemIds));
        return criteria.list();
    }

    @Override
    public ProjectItemEntity findProjectDwhItem(Long projectId, Long originalItemId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProjectItemEntity.class, "projectItem");
        criteria.add(Restrictions.eq("projectItem.deleted", false));
        criteria.add(Restrictions.eq("projectItem.projectEntity.projectId", projectId));
        criteria.add(Restrictions.eq("projectItem.originalItemId", originalItemId));
        return (ProjectItemEntity) criteria.uniqueResult();
    }
}
