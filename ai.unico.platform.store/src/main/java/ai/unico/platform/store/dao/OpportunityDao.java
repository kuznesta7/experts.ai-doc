package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.OpportunityEntity;
import ai.unico.platform.store.entity.OpportunityJobTypeEntity;
import ai.unico.platform.store.entity.OpportunityTagEntity;

import java.util.List;

public interface OpportunityDao {

    List<OpportunityEntity> getOpportunities(Integer limit, Integer offset);

    List<OpportunityEntity> getOpportunities(Integer limit, Integer offset, boolean ignoreDeleted);

    OpportunityEntity getOpportunity(Long id);

    OpportunityEntity getOpportunityByOriginalId(Long id);

    void saveOpportunityTag(OpportunityTagEntity opportunityTag);

    void saveOpportunityJob(OpportunityJobTypeEntity opportunityJobType);

    void save(OpportunityEntity opportunity);
}
