package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.AskForPermissionsEntity;

public interface AskForPermissionsDao {

    void save(AskForPermissionsEntity askForPermissionsEntity);

}
