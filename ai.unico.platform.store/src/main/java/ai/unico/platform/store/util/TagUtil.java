package ai.unico.platform.store.util;

import ai.unico.platform.store.api.dto.TagDto;
import ai.unico.platform.store.entity.TagEntity;

public class TagUtil {

    public static TagDto convert(TagEntity tagEntity) {
        final TagDto tagDto = new TagDto();
        tagDto.setTagId(tagEntity.getTagId());
        tagDto.setValue(tagEntity.getValue());
        return tagDto;
    }

    public static TagEntity convert(TagDto tagDto) {
        final TagEntity tagEntity = new TagEntity();
        tagEntity.setTagId(tagDto.getTagId());
        tagEntity.setValue(tagDto.getValue());
        return tagEntity;
    }

}
