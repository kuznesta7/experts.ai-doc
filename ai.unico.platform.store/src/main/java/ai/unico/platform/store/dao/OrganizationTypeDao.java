package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.OrganizationTypeEntity;

import java.util.List;

public interface OrganizationTypeDao {

    OrganizationTypeEntity find(Long organizationTypeId);

    List<OrganizationTypeEntity> find();
}
