package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.OpportunityTypeDao;
import ai.unico.platform.store.entity.OpportunityTypeEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class OpportunityTypeDaoImpl implements OpportunityTypeDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public List<OpportunityTypeEntity> getAllOpportunityTypes() {
        Criteria criteria = hibernateCriteriaCreator.createCriteria(OpportunityTypeEntity.class, "opportunityType");
        criteria.add(Restrictions.eq("deleted", false));
        return criteria.list();
    }

    @Override
    public OpportunityTypeEntity getOpportunityTypeEntity(Long id) {
        return entityManager.find(OpportunityTypeEntity.class, id);
    }

    @Override
    public void save(OpportunityTypeEntity opportunityTypeEntity) {
        entityManager.persist(opportunityTypeEntity);
    }
}
