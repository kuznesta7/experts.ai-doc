package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.EquipmentOrganizationEntity;

import java.util.List;

public interface EquipmentOrganizationDao {

    void save(EquipmentOrganizationEntity equipmentOrganizationEntity);

    List<EquipmentOrganizationEntity> findOrganizationsForEquipment(Long equipmentId);

    EquipmentOrganizationEntity find(Long equipmentId, Long organizationId);
    
}
