package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.OpportunityDao;
import ai.unico.platform.store.entity.OpportunityEntity;
import ai.unico.platform.store.entity.OpportunityJobTypeEntity;
import ai.unico.platform.store.entity.OpportunityTagEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

public class OpportunityDaoImpl implements OpportunityDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public List<OpportunityEntity> getOpportunities(Integer limit, Integer offset, boolean ignoreDeleted) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OpportunityEntity.class, "opportunity");
        if(ignoreDeleted)
            criteria.add(Restrictions.eq("deleted", false));
        criteria.addOrder(Order.asc("opportunityId"));
        criteria.setMaxResults(limit);
        criteria.setFirstResult(offset);
        return criteria.list();
    }

    @Override
    public List<OpportunityEntity> getOpportunities(Integer limit, Integer offset) {
        return getOpportunities(limit, offset, true);
    }

    @Override
    public OpportunityEntity getOpportunity(Long id) {
        final OpportunityEntity entity = entityManager.find(OpportunityEntity.class, id);
        if (entity.isDeleted()) {
            throw new EntityNotFoundException();
        }
        return entity;
    }

    @Override
    public OpportunityEntity getOpportunityByOriginalId(Long id) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OpportunityEntity.class, "opportunity");
        criteria.add(Restrictions.eq("originalOpportunityId", id));
        return (OpportunityEntity) criteria.uniqueResult();
    }

    @Override
    public void saveOpportunityTag(OpportunityTagEntity opportunityTag) {
        entityManager.persist(opportunityTag);
    }

    @Override
    public void saveOpportunityJob(OpportunityJobTypeEntity opportunityJobType) {
        entityManager.persist(opportunityJobType);
    }

    @Override
    public void save(OpportunityEntity opportunity) {
        entityManager.persist(opportunity);
    }
}
