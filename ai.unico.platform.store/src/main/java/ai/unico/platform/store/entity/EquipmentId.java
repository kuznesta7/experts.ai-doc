package ai.unico.platform.store.entity;

//import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

//@Embeddable
public class EquipmentId implements Serializable {
    private Long equipmentId;
    private String languageCode;

    public EquipmentId() {
    }

    public EquipmentId(Long equipmentId, String languageCode) {
        this.equipmentId = equipmentId;
        this.languageCode = languageCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentId)) return false;
        EquipmentId that = (EquipmentId) o;
        return Objects.equals(equipmentId, that.equipmentId) && Objects.equals(languageCode, that.languageCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(equipmentId, languageCode);
    }

    public Long getEquipmentId() {
        return equipmentId;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setEquipmentId(Long equipmentId) {
        this.equipmentId = equipmentId;
    }
}
