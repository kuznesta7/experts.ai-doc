package ai.unico.platform.store.util;

import ai.unico.platform.search.api.service.CommercializationProjectIndexService;
import ai.unico.platform.store.api.dto.CommercializationProjectIndexDto;
import ai.unico.platform.store.api.enums.CommercializationOrganizationRelation;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.util.ServiceRegistryUtil;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class CommercializationProjectIndexUtil {
    public static CommercializationProjectIndexDto convertIndex(CommercializationProjectEntity commercializationProjectEntity) {
        CommercializationProjectIndexDto commercializationProjectIndexDto = new CommercializationProjectIndexDto();
        commercializationProjectIndexDto.setCommercializationProjectId(commercializationProjectEntity.getCommercializationId());
        commercializationProjectIndexDto.setName(commercializationProjectEntity.getName());
        commercializationProjectIndexDto.setExecutiveSummary(commercializationProjectEntity.getExecutiveSummary());
        commercializationProjectIndexDto.setUseCase(commercializationProjectEntity.getUseCase());
        commercializationProjectIndexDto.setPainDescription(commercializationProjectEntity.getPainDescription());
        commercializationProjectIndexDto.setCompetitiveAdvantage(commercializationProjectEntity.getCompetitiveAdvantage());
        commercializationProjectIndexDto.setTechnicalPrinciples(commercializationProjectEntity.getTechnicalPrinciples());
        commercializationProjectIndexDto.setTechnologyReadinessLevel(commercializationProjectEntity.getTechnologyReadinessLevel());
        commercializationProjectIndexDto.setStartDate(commercializationProjectEntity.getStartDate());
        commercializationProjectIndexDto.setEndDate(commercializationProjectEntity.getEndDate());
        commercializationProjectIndexDto.setStatusDeadline(commercializationProjectEntity.getStatusDeadline());
        commercializationProjectIndexDto.setDeleted(commercializationProjectEntity.isDeleted());
        commercializationProjectIndexDto.setCommercializationInvestmentFrom(commercializationProjectEntity.getCommercializationInvestmentFrom());
        commercializationProjectIndexDto.setCommercializationInvestmentTo(commercializationProjectEntity.getCommercializationInvestmentTo());
        commercializationProjectIndexDto.setCommercializationPriorityId(HibernateUtil.getId(commercializationProjectEntity, CommercializationProjectEntity::getCommercializationPriorityEntity, CommercializationPriorityEntity::getCommercializationPriorityId));
        commercializationProjectIndexDto.setCommercializationDomainId(HibernateUtil.getId(commercializationProjectEntity, CommercializationProjectEntity::getCommercializationDomainEntity, CommercializationDomainEntity::getCommercializationDomainId));
        commercializationProjectIndexDto.setOwnerOrganizationId(HibernateUtil.getId(commercializationProjectEntity, CommercializationProjectEntity::getOwnerOrganizationEntity, OrganizationEntity::getOrganizationId));
        commercializationProjectIndexDto.setIdeaScore(commercializationProjectEntity.getIdeaScore());
        commercializationProjectIndexDto.setLink(commercializationProjectEntity.getLink());

        final Set<Long> filteredCommercializationCategories = commercializationProjectEntity.getCommercializationProjectCategoryEntities().stream().filter(ctm -> !ctm.isDeleted()).map(CommercializationProjectCategoryEntity::getCommercializationCategoryEntity).map(CommercializationCategoryEntity::getCommercializationCategoryId).collect(Collectors.toSet());

        final Set<CommercializationTeamMemberEntity> filteredCommercializationTeamMembers = commercializationProjectEntity.getCommercializationTeamMemberEntities().stream()
                .filter(ctm -> !ctm.isDeleted())
                .collect(Collectors.toSet());
        final Set<Long> teamMemberUserIds = filteredCommercializationTeamMembers.stream()
                .map(CommercializationTeamMemberEntity::getUserProfileEntity)
                .filter(Objects::nonNull)
                .map(CommercializationProjectIndexUtil::getUserId)
                .collect(Collectors.toSet());
        final Set<Long> teamLeaderUserIds = filteredCommercializationTeamMembers.stream()
                .filter(CommercializationTeamMemberEntity::isTeamLeader)
                .map(CommercializationTeamMemberEntity::getUserProfileEntity)
                .filter(Objects::nonNull)
                .map(CommercializationProjectIndexUtil::getUserId)
                .collect(Collectors.toSet());
        final Set<Long> teamMemberExpertIds = filteredCommercializationTeamMembers.stream()
                .filter(x -> x.getUserProfileEntity() == null)
                .map(CommercializationTeamMemberEntity::getExpertId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        final Set<Long> teamLeaderExpertIds = filteredCommercializationTeamMembers.stream()
                .filter(x -> x.getUserProfileEntity() == null)
                .filter(CommercializationTeamMemberEntity::isTeamLeader)
                .map(CommercializationTeamMemberEntity::getExpertId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());

        final Set<CommercializationProjectKeywordEntity> commercializationProjectKeywordEntities = commercializationProjectEntity.getCommercializationProjectKeywordEntities();
        final List<String> commercializationProjectKeywords = commercializationProjectKeywordEntities.stream().filter(x -> !x.isDeleted())
                .map(CommercializationProjectKeywordEntity::getTagEntity)
                .map(TagEntity::getValue)
                .collect(Collectors.toList());

        final Set<CommercializationProjectOrganizationEntity> commercializationProjectOrganizationEntities = commercializationProjectEntity.getCommercializationProjectOrganizationEntities();
        final Map<CommercializationOrganizationRelation, List<CommercializationProjectOrganizationEntity>> relationOrganizationEntitiesMap = commercializationProjectOrganizationEntities.stream()
                .filter(cpo -> !cpo.isDeleted())
                .collect(Collectors.groupingBy(CommercializationProjectOrganizationEntity::getRelation));
        for (Map.Entry<CommercializationOrganizationRelation, List<CommercializationProjectOrganizationEntity>> entry : relationOrganizationEntitiesMap.entrySet()) {
            commercializationProjectIndexDto.getCommercializationOrganizationRelationMap().put(entry.getKey(), entry.getValue().stream()
                    .map(cpo -> HibernateUtil.getId(cpo, CommercializationProjectOrganizationEntity::getOrganizationEntity, OrganizationEntity::getOrganizationId))
                    .collect(Collectors.toSet()));
        }
        commercializationProjectIndexDto.setCommercializationCategoryIds(filteredCommercializationCategories);
        commercializationProjectIndexDto.setTeamMemberUserIds(teamMemberUserIds);
        commercializationProjectIndexDto.setTeamLeaderUserIds(teamLeaderUserIds);
        commercializationProjectIndexDto.setTeamMemberExpertIds(teamMemberExpertIds);
        commercializationProjectIndexDto.setTeamLeaderExpertIds(teamLeaderExpertIds);
        commercializationProjectIndexDto.setKeywords(commercializationProjectKeywords);
        commercializationProjectEntity.getCommercializationProjectStatusEntities().stream()
                .filter(x -> !x.isOutdated()).findFirst()
                .ifPresent(x -> commercializationProjectIndexDto.setStatusId(HibernateUtil.getId(x, CommercializationProjectStatusEntity::getCommercializationStatusTypeEntity, CommercializationStatusTypeEntity::getCommercializationStatusId)));

        if (commercializationProjectEntity.getImgFile() != null)
            commercializationProjectIndexDto.setImgChecksum(commercializationProjectEntity.getImgFile().getHash());
        return commercializationProjectIndexDto;
    }

    public static void index(CommercializationProjectEntity commercializationProjectEntity) {
        final CommercializationProjectIndexService service = ServiceRegistryUtil.getService(CommercializationProjectIndexService.class);
        try {
            service.indexCommercializationProject(convertIndex(commercializationProjectEntity));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Long getUserId(UserProfileEntity userProfileEntity) {
        if (userProfileEntity.getClaimedBy() == null) return userProfileEntity.getUserId();
        return userProfileEntity.getClaimedBy().getUserId();
    }
}
