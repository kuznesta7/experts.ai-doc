package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.EquipmentEntity;
import ai.unico.platform.store.entity.EquipmentTagEntity;

import java.util.List;
import java.util.Set;

public interface EquipmentDao {

    void save(EquipmentEntity equipment);

    void saveTag(EquipmentTagEntity equipmentTag);

    EquipmentEntity find(Long equipmentId, String languageCode);
    List<EquipmentEntity> find(Long equipmentId);

    List<EquipmentEntity> findAll(Set<Long> ids);

    List<EquipmentEntity> findAll(Integer limit, Integer offset);

    List<EquipmentEntity> findByOriginalIds(List<String> originalIds);

    List<Long> getAllOriginalIds();
}
