package ai.unico.platform.store.entity;

import ai.unico.platform.store.api.enums.OrganizationRelationType;
import jdk.nashorn.internal.ir.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Immutable
public class OrganizationStructureViewEntity implements Serializable {

    @Id
    private Long lowerOrganizationId;

    @Id
    private Long upperOrganizationId;

    @Enumerated(EnumType.STRING)
    private OrganizationRelationType relationType;

    public Long getLowerOrganizationId() {
        return lowerOrganizationId;
    }

    public void setLowerOrganizationId(Long lowerOrganizationId) {
        this.lowerOrganizationId = lowerOrganizationId;
    }

    public Long getUpperOrganizationId() {
        return upperOrganizationId;
    }

    public void setUpperOrganizationId(Long upperOrganizationId) {
        this.upperOrganizationId = upperOrganizationId;
    }

    public OrganizationRelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(OrganizationRelationType relationType) {
        this.relationType = relationType;
    }
}
