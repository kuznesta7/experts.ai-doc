package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.ReferenceData;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CommercializationDomainEntity extends ReferenceData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commercializationDomainId;

    public Long getCommercializationDomainId() {
        return commercializationDomainId;
    }

    public void setCommercializationDomainId(Long commercializationDomainId) {
        this.commercializationDomainId = commercializationDomainId;
    }
}
