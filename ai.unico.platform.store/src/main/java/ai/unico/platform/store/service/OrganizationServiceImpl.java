package ai.unico.platform.store.service;


import ai.unico.platform.extdata.dto.DWHOrganizationDto;
import ai.unico.platform.extdata.dto.DWHOrganizationStructureDto;
import ai.unico.platform.extdata.service.DWHOrganizationService;
import ai.unico.platform.search.api.service.ElasticsearchService;
import ai.unico.platform.search.api.service.OrganizationIndexService;
import ai.unico.platform.store.api.dto.*;
import ai.unico.platform.store.api.enums.OrganizationRelationType;
import ai.unico.platform.store.api.service.OrganizationService;
import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.store.api.wrapper.OrganizationTypeWrapper;
import ai.unico.platform.store.dao.*;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.store.util.OrganizationIndexUtil;
import ai.unico.platform.store.util.OrganizationUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.dto.OrganizationDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.RuntimeExceptionWithMessage;
import ai.unico.platform.util.model.UserContext;
import org.cache2k.Cache;
import org.cache2k.Cache2kBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

public class OrganizationServiceImpl implements OrganizationService {

    @Autowired
    private OrganizationDao organizationDao;
    private final Cache<Long, OrganizationEntity> organizationEntityCache = new Cache2kBuilder<Long, OrganizationEntity>() {
    }
            .expireAfterWrite(2, TimeUnit.MINUTES)
            .permitNullValues(true)
            .loader(this::doFind)
            .build();
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private CountryDao countryDao;

    @Autowired
    private OrganizationStructureDao organizationStructureDao;

    @Autowired
    private OrganizationStructureService organizationStructureService;

    @Autowired
    private OrganizationLicenceDao organizationLicenceDao;

    @Autowired
    private OrganizationTypeDao organizationTypeDao;

    private OrganizationEntity doFind(Long organizationId) {
        return organizationDao.find(organizationId);
    }

    @Override
    @Transactional
    public List<OrganizationIndexDto> findAllOrganizations() {
        final List<OrganizationEntity> organizationEntities = organizationDao.find();
        final Map<Long, Long> substitutionMap = createSubstitutionMap(organizationEntities);
        final List<OrganizationDao.OrganizationRankHelper> organizationsRank = organizationDao.findAndComputeOrganizationsRank();
        final List<OrganizationStructureEntity> organizationStructureEntities = organizationStructureDao.findAll();
        final List<OrganizationLicenceEntity> organizationLicences = organizationLicenceDao.getOrganizationLicences();

        final Map<Long, List<OrganizationStructureEntity>> structureMap = organizationStructureEntities.stream().collect(Collectors.groupingBy(x -> HibernateUtil.getId(x, OrganizationStructureEntity::getUpperOrganizationEntity, OrganizationEntity::getOrganizationId)));
        final Map<Long, List<OrganizationLicenceEntity>> licenceMap = organizationLicences.stream().collect(Collectors.groupingBy(x -> HibernateUtil.getId(x, OrganizationLicenceEntity::getOrganizationEntity, OrganizationEntity::getOrganizationId)));

        final Map<Long, OrganizationDao.OrganizationRankHelper> organizationRankMap = organizationsRank.stream()
                .collect(Collectors.toMap(OrganizationDao.OrganizationRankHelper::getOrganizationId, Function.identity()));
        final List<OrganizationIndexDto> result = new ArrayList<>();

        for (OrganizationEntity organizationEntity : organizationEntities) {
            final Long originalOrganizationId = organizationEntity.getOrganizationId();
            final Long substituteOrganizationId = HibernateUtil.getId(organizationEntity, OrganizationEntity::getSubstituteOrganizationEntity, OrganizationEntity::getOrganizationId);
            if (substituteOrganizationId != null) {
                final OrganizationDao.OrganizationRankHelper originalOrganizationRank = organizationRankMap.get(originalOrganizationId);
                final OrganizationDao.OrganizationRankHelper substituteOrganizationRank = organizationRankMap.computeIfAbsent(substituteOrganizationId, x -> new OrganizationDao.OrganizationRankHelper());
                substituteOrganizationRank.setItemCount(substituteOrganizationRank.getItemCount() + originalOrganizationRank.getItemCount());
                substituteOrganizationRank.setUserCount(substituteOrganizationRank.getUserCount() + originalOrganizationRank.getUserCount());
                final Long substituteRank = substituteOrganizationRank.getRank();
                final Long originalRank = originalOrganizationRank.getRank();
                substituteOrganizationRank.setRank((substituteRank != null ? substituteRank : 0) + (originalRank != null ? originalRank : 0));
                originalOrganizationRank.setItemCount(0L);
                originalOrganizationRank.setUserCount(0L);
                originalOrganizationRank.setRank(0L);
            }
        }
        for (OrganizationEntity organizationEntity : organizationEntities) {
            final Long organizationId = organizationEntity.getOrganizationId();
            final List<OrganizationLicenceEntity> licences = licenceMap.getOrDefault(organizationId, Collections.emptyList());
            final List<OrganizationStructureEntity> structure = structureMap.getOrDefault(organizationId, Collections.emptyList());
            final OrganizationIndexDto organizationIndexDto = OrganizationUtil.convertIndex(organizationEntity, substitutionMap, structure, licences);
            if (organizationRankMap.containsKey(organizationId)) {
                final OrganizationDao.OrganizationRankHelper rankContainer = organizationRankMap.get(organizationId);
                organizationIndexDto.setRank((rankContainer.getRank() != null ? rankContainer.getRank() : 0) + rankContainer.getUserCount() + rankContainer.getItemCount());
            }
            result.add(organizationIndexDto);
        }
        return result;
    }

    private Map<Long, Long> createSubstitutionMap(List<OrganizationEntity> organizationEntities) {
        return organizationEntities.stream().collect(Collectors.toMap(OrganizationEntity::getOrganizationId, x -> {
            final Long substituteId = HibernateUtil.getId(x, OrganizationEntity::getSubstituteOrganizationEntity, OrganizationEntity::getOrganizationId);
            return substituteId != null ? substituteId : x.getOrganizationId();
        }));
    }

    @Override
    @Transactional
    public List<OrganizationDto> findOrganizationsByIds(Set<Long> organizationIds) {
        final List<OrganizationEntity> organizationEntities = organizationDao.findByIds(organizationIds);
        return organizationEntities.stream().map(OrganizationUtil::convert).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public OrganizationDetailDto findOrganizationDetail(Long organizationId) {
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
        final OrganizationDetailDto organizationDetailDto = OrganizationUtil.convertToDetail(organizationEntity);
        final Set<Long> childOrganizationIds = organizationStructureService.findChildOrganizationIds(organizationId, false);
        final List<OrganizationEntity> childrenOrganizations = organizationDao.findByIds(childOrganizationIds);

        final Optional<Long> reduce = childrenOrganizations.stream()
                .map(OrganizationEntity::getRank)
                .filter(Objects::nonNull)
                .reduce(Long::sum);
        final Double rank = Double.valueOf(reduce.orElse(organizationEntity.getRank() != null ? organizationEntity.getRank() : 0));
        organizationDetailDto.setOrganizationRank((-Math.pow(2, -Math.sqrt(rank / 1000)) + 1) * 100);

        organizationDetailDto.setOrganizationUnitIds(childOrganizationIds);

        return organizationDetailDto;
    }

    @Override
    @Transactional
    public void updateOrganizationsFromDWH() {
        final DWHOrganizationService service = ServiceRegistryUtil.getService(DWHOrganizationService.class);
        final List<DWHOrganizationDto> organizations = service.findOrganizations();
        final Map<Long, DWHOrganizationDto> dwhOrganizationMap = organizations.stream()
                .collect(Collectors.toMap(DWHOrganizationDto::getOriginalOrganizationId, Function.identity()));
        final List<CountryEntity> allCountries = countryDao.findAllCountries();
        final Map<String, CountryEntity> countryMap = allCountries.stream()
                .collect(Collectors.toMap(CountryEntity::getCountryCode, Function.identity()));

        final List<OrganizationEntity> organizationEntities = organizationDao.find();

        final Map<Long, OrganizationEntity> originalOrganizationIdEntityMap = organizationEntities.stream()
                .filter(x -> x.getOriginalOrganizationId() != null)
                .collect(Collectors.toMap(OrganizationEntity::getOriginalOrganizationId, Function.identity()));

        for (DWHOrganizationDto organization : organizations) {
            final OrganizationEntity organizationEntity = originalOrganizationIdEntityMap.computeIfAbsent(organization.getOriginalOrganizationId(), id -> doCreateOrganizationFromDWH(organization, dwhOrganizationMap, originalOrganizationIdEntityMap));
            if (!organizationEntity.isLocked()) {
                final String countryCode = organization.getCountryCode();
                if (countryCode != null) organizationEntity.setCountryEntity(countryMap.get(countryCode.toLowerCase()));
            }
        }
        final Set<Long> processedOriginalOrganizationIds = new HashSet<>();
        for (DWHOrganizationDto organization : organizations) {
            final Long originalOrganizationId = organization.getOriginalOrganizationId();
            final Long substituteOrganizationId = organization.getSubstituteOrganizationId();
            if (originalOrganizationId.equals(substituteOrganizationId) || processedOriginalOrganizationIds.contains(originalOrganizationId))
                continue;
            originalOrganizationIdEntityMap.get(originalOrganizationId).setRank(organization.getRank());
            processedOriginalOrganizationIds.add(originalOrganizationId);
            if (originalOrganizationIdEntityMap.containsKey(originalOrganizationId)) {
                final OrganizationEntity organizationEntity = originalOrganizationIdEntityMap.get(originalOrganizationId);
                if (substituteOrganizationId != null && originalOrganizationIdEntityMap.containsKey(substituteOrganizationId)) {
                    final OrganizationEntity substituteOrganizationEntity = originalOrganizationIdEntityMap.get(substituteOrganizationId);
                    if (!organizationEntity.isLocked()) {
                        organizationEntity.setSubstituteOrganizationEntity(substituteOrganizationEntity);
                    } else if (!substituteOrganizationEntity.isLocked()) {
                        substituteOrganizationEntity.setSubstituteOrganizationEntity(organizationEntity);
                        processedOriginalOrganizationIds.add(substituteOrganizationId);
                    }
                }
            }
        }
        final List<DWHOrganizationStructureDto> organizationStructure = service.findOrganizationStructure();
        organizationStructure.addAll(service.findOrganizationMembers());
        final List<Set<Long>> relatedOrganizationsList = new ArrayList<>();
        final List<OrganizationStructureEntity> organizationStructureEntities = organizationStructureDao.findAll();
        for (OrganizationStructureEntity structureEntity : organizationStructureEntities) {
            final Long upperOrganizationId = HibernateUtil.getId(structureEntity, OrganizationStructureEntity::getUpperOrganizationEntity, OrganizationEntity::getOrganizationId);
            final Long lowerOrganizationId = HibernateUtil.getId(structureEntity, OrganizationStructureEntity::getLowerOrganizationEntity, OrganizationEntity::getOrganizationId);
            if (upperOrganizationId == null || lowerOrganizationId == null) continue;
            final Set<Long> relatedSet = relatedOrganizationsList.stream().filter(ros -> ros.contains(upperOrganizationId) || ros.contains(lowerOrganizationId)).findFirst().orElseGet(() -> {
                final Set<Long> set = new HashSet<>();
                relatedOrganizationsList.add(set);
                return set;
            });
            relatedSet.add(upperOrganizationId);
            relatedSet.add(lowerOrganizationId);
        }
        for (DWHOrganizationStructureDto structureDto : organizationStructure) {
            final Long childOriginalOrganizationId = structureDto.getChildOrganizationId();
            final Long parentOriginalOrganizationId = structureDto.getParentOrganizationId();

            final OrganizationEntity childOrganizationEntity = originalOrganizationIdEntityMap.get(childOriginalOrganizationId);
            final OrganizationEntity parentOrganizationEntity = originalOrganizationIdEntityMap.get(parentOriginalOrganizationId);

            final Long childOrganizationId = childOrganizationEntity.getOrganizationId();
            final Long parentOrganizationId = parentOrganizationEntity.getOrganizationId();

            final List<Set<Long>> relevantSets = relatedOrganizationsList.stream()
                    .filter(ros -> ros.contains(childOrganizationId) || ros.contains(parentOrganizationId)).collect(Collectors.toList());
            final Set<Long> relatedOrganizations = new HashSet<>();
            relatedOrganizationsList.removeAll(relevantSets);
            relevantSets.forEach(relatedOrganizations::addAll);
            relatedOrganizationsList.add(relatedOrganizations);

            if (relatedOrganizations.contains(childOrganizationId) && relatedOrganizations.contains(parentOrganizationId)) {
                continue;
            }
            final OrganizationStructureEntity structureEntity = new OrganizationStructureEntity();
            structureEntity.setUpperOrganizationEntity(parentOrganizationEntity);
            structureEntity.setLowerOrganizationEntity(childOrganizationEntity);
            if (Objects.equals(structureDto.getRelationType(), "MEMBER")) {
                structureEntity.setRelationType(OrganizationRelationType.MEMBER);
                parentOrganizationEntity.setAllowedMembers(true);
            } else
                structureEntity.setRelationType(OrganizationRelationType.PARENT);
            organizationStructureDao.save(structureEntity);
        }
        entityManager.flush();
        organizationStructureService.deleteCache();

        System.out.println("Organizations loaded from DWH; Starting substitution fix");
        final List<OrganizationEntity> updatedOrganizationEntities = organizationDao.find();
        fixOrganizationSubstitutions(updatedOrganizationEntities);
        System.out.println("Organization substitution fix done");
    }

    @Override
    @Transactional(noRollbackForClassName = "ElasticsearchException")
    public void updateOrganization(AdminOrganizationUpdateDto organizationDto, UserContext userContext) throws IOException {
        final OrganizationIndexService organizationIndexService = ServiceRegistryUtil.getService(OrganizationIndexService.class);
        final Long organizationId = organizationDto.getOrganizationId();
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
        if (organizationDto.getOrganizationName() != null && !organizationDto.getOrganizationName().isEmpty()) {
            organizationEntity.setOrganizationName(organizationDto.getOrganizationName());
            organizationEntity.setLocked(true);
            TrackableUtil.fillUpdate(organizationEntity, userContext);
        }
        if (organizationDto.getCountryCode() != null) {
            final CountryEntity country = countryDao.findCountry(organizationDto.getCountryCode());
            if (country != null) {
                organizationEntity.setCountryEntity(country);
                organizationEntity.setLocked(true);
                TrackableUtil.fillUpdate(organizationEntity, userContext);
            }
        }
        if (organizationDto.getOrganizationAbbrev() != null && !organizationDto.getOrganizationAbbrev().isEmpty()) {
            organizationEntity.setOrganizationAbbrev(organizationDto.getOrganizationAbbrev());
            TrackableUtil.fillUpdate(organizationEntity, userContext);
        }

        try {
            OrganizationIndexUtil.indexOrganization(organizationEntity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void restoreOrganization(Long organizationId, UserContext userContext) {
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
        organizationEntity.setSubstituteOrganizationEntity(null);
        TrackableUtil.fillUpdate(organizationEntity, userContext);
        OrganizationIndexUtil.indexOrganization(organizationEntity);
    }

    @Override
    @Transactional
    public Long createOrganization(AdminOrganizationUpdateDto organizationDto, UserContext userContext) {
        final OrganizationEntity organizationEntity = new OrganizationEntity();
        organizationEntity.setOrganizationName(organizationDto.getOrganizationName());
        organizationEntity.setOrganizationAbbrev(organizationDto.getOrganizationAbbrev());
        if (organizationDto.getCountryCode() != null) {
            final CountryEntity country = countryDao.findCountry(organizationDto.getCountryCode());
            organizationEntity.setCountryEntity(country);
        }
        organizationEntity.setLocked(true);
        TrackableUtil.fillCreate(organizationEntity, userContext);
        organizationDao.saveOrganization(organizationEntity);
        final Long organizationId = organizationEntity.getOrganizationId();
        final Long parentOrganizationId = organizationDto.getParentOrganizationId();
        if (parentOrganizationId != null) {
            organizationStructureService.addOrganizationStructure(parentOrganizationId, organizationId, OrganizationRelationType.PARENT, userContext);
        }
        OrganizationIndexUtil.indexOrganization(organizationEntity);
        return organizationId;
    }

    @Override
    @Transactional
    public Map<Long, Long> findOrganizationIdsMap(Set<Long> originalOrganizationIds) {
        if (originalOrganizationIds.isEmpty()) return Collections.emptyMap();
        final List<OrganizationEntity> organizationEntities = organizationDao.findByOriginalIds(originalOrganizationIds);
        final Map<Long, Long> result = new HashMap<>();
        for (OrganizationEntity organizationEntity : organizationEntities) {
            final Long originalOrganizationId = organizationEntity.getOriginalOrganizationId();
            final Long substituteOrganizationId = HibernateUtil.getId(organizationEntity, OrganizationEntity::getSubstituteOrganizationEntity, OrganizationEntity::getOrganizationId);
            if (substituteOrganizationId != null) {
                result.put(originalOrganizationId, substituteOrganizationId);
            } else {
                result.put(originalOrganizationId, organizationEntity.getOrganizationId());
            }
        }
        return result;
    }

    @Override
    @Transactional(noRollbackForClassName = "ElasticsearchException")
    public void replaceOrganization(Long organizationId, Long substituteOrganizationId, UserContext userContext) throws IOException {
        final List<OrganizationEntity> organizationEntities = organizationDao.findByIds(Arrays.asList(organizationId, substituteOrganizationId));
        final Map<Long, OrganizationEntity> entityMap = organizationEntities.stream().collect(Collectors.toMap(OrganizationEntity::getOrganizationId, Function.identity()));
        final OrganizationEntity organizationToReplace = entityMap.get(organizationId);
        final OrganizationEntity substituteOrganization = entityMap.get(substituteOrganizationId);
        if (!canOrganizationBeReplaced(organizationToReplace))
            throw new RuntimeExceptionWithMessage("Organization \"" + organizationToReplace.getOrganizationName() + "\" cannot be replaced because it is already in use", "organization cannot be replaced");
        organizationToReplace.setSubstituteOrganizationEntity(substituteOrganization);
        TrackableUtil.fillUpdate(organizationToReplace, userContext);
        final OrganizationIndexService organizationIndexService = ServiceRegistryUtil.getService(OrganizationIndexService.class);
        organizationIndexService.replaceOrganization(organizationToReplace.getOrganizationId(), substituteOrganization.getOrganizationId());
        try {
            OrganizationIndexUtil.indexOrganization(substituteOrganization);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void loadAndIndexOrganization(Long organizationId) {
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        OrganizationIndexUtil.indexOrganization(organizationEntity);
        final ElasticsearchService service = ServiceRegistryUtil.getService(ElasticsearchService.class);
        service.flushSyncedAll();
    }

    @Override
    @Transactional
    public void toggleMembers(Long organizationId, boolean allow) {
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        organizationEntity.setAllowedMembers(allow);
        try {
            OrganizationIndexUtil.indexOrganization(organizationEntity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void changeOrganizationType(Long organizationId, Long organizationTypeId) {
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        organizationEntity.setOrganizationTypeEntity(organizationTypeDao.find(organizationTypeId));
        try {
            OrganizationIndexUtil.indexOrganization(organizationEntity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void toggleSearchable(Long organizationId, boolean allow) {
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        organizationEntity.setSearchable(allow);
        try {
            OrganizationIndexUtil.indexOrganization(organizationEntity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public Boolean hasPublicProfile(Long organizationId) {
        final OrganizationEntity organizationEntity = organizationEntityCache.get(organizationId);
        return organizationEntity != null
                && organizationEntity.getOrganizationProfileVisibilityEntity() != null
                && organizationEntity.getOrganizationProfileVisibilityEntity().isVisible();
    }

    @Override
    @Transactional
    public Boolean hasGraphVisible(Long organizationId) {
        final OrganizationEntity organizationEntity = organizationEntityCache.get(organizationId);
        return organizationEntity != null
                && organizationEntity.getOrganizationProfileVisibilityEntity() != null
                && organizationEntity.getOrganizationProfileVisibilityEntity().isVisible()
                && organizationEntity.getOrganizationProfileVisibilityEntity().isGraphVisible();
    }

    @Override
    @Transactional
    public Boolean hasExpertsVisible(Long organizationId) {
        final OrganizationEntity organizationEntity = organizationEntityCache.get(organizationId);
        return organizationEntity != null
                && organizationEntity.getOrganizationProfileVisibilityEntity() != null
                && organizationEntity.getOrganizationProfileVisibilityEntity().isVisible()
                && organizationEntity.getOrganizationProfileVisibilityEntity().isExpertsVisible();
    }

    @Override
    @Transactional
    public Boolean hasOutcomesVisible(Long organizationId) {
        final OrganizationEntity organizationEntity = organizationEntityCache.get(organizationId);
        return organizationEntity != null
                && organizationEntity.getOrganizationProfileVisibilityEntity() != null
                && organizationEntity.getOrganizationProfileVisibilityEntity().isVisible()
                && organizationEntity.getOrganizationProfileVisibilityEntity().isOutcomesVisible();
    }

    @Override
    @Transactional
    public Boolean hasProjectsVisible(Long organizationId) {
        final OrganizationEntity organizationEntity = organizationEntityCache.get(organizationId);
        return organizationEntity != null
                && organizationEntity.getOrganizationProfileVisibilityEntity() != null
                && organizationEntity.getOrganizationProfileVisibilityEntity().isVisible()
                && organizationEntity.getOrganizationProfileVisibilityEntity().isProjectsVisible();
    }

    @Override
    @Transactional
    public Boolean hasKeywordsVisible(Long organizationId) {
        final OrganizationEntity organizationEntity = organizationEntityCache.get(organizationId);
        return organizationEntity != null
                && organizationEntity.getOrganizationProfileVisibilityEntity() != null
                && organizationEntity.getOrganizationProfileVisibilityEntity().isVisible()
                && organizationEntity.getOrganizationProfileVisibilityEntity().isKeywordsVisible();
    }

    @Override
    public Boolean hasGdpr(Long organizationId) {
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        return organizationEntity.getSearchable();
    }

    @Override
    @Transactional
    public PublicProfileVisibilityDto getProfileVisibility(Long organizationId) {
        OrganizationProfileVisibilityEntity organizationProfileVisibilityEntity = organizationEntityCache.get(organizationId).getOrganizationProfileVisibilityEntity();
        if (organizationProfileVisibilityEntity == null) {
            return new PublicProfileVisibilityDto(organizationId);
        }
        return convert(organizationProfileVisibilityEntity);
    }

    @Override
    @Transactional
    public void setProfileVisibility(PublicProfileVisibilityDto profileVisibilityDto) {
        final OrganizationEntity organizationEntity = organizationDao.find(profileVisibilityDto.getOrganizationId());
        if (organizationEntity.getOrganizationProfileVisibilityEntity() == null) {
            OrganizationProfileVisibilityEntity organizationProfileVisibilityEntity = new OrganizationProfileVisibilityEntity();
            organizationEntity.setOrganizationProfileVisibilityEntity(organizationProfileVisibilityEntity);
            organizationProfileVisibilityEntity.setOrganizationEntity(organizationEntity);
            organizationProfileVisibilityEntity.setOrganizationId(organizationEntity.getOrganizationId());
            entityManager.persist(organizationProfileVisibilityEntity);
        }
        fill(organizationEntity.getOrganizationProfileVisibilityEntity(), profileVisibilityDto);
        organizationEntityCache.remove(profileVisibilityDto.getOrganizationId());
    }

    @Override
    @Transactional
    public void saveProfile(Long organizationId, OrganizationDetailDto profileDto, UserContext userContext) {
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        organizationEntity.setOrganizationDescription(profileDto.getOrganizationDescription());
        organizationEntity.setOrganizationName(profileDto.getOrganizationName());
        organizationEntity.setOrganizationAbbrev(profileDto.getOrganizationAbbrev());
        organizationEntity.setCity(profileDto.getCity());
        organizationEntity.setStreet(profileDto.getStreet());
        organizationEntity.setEmail(profileDto.getEmail());
        organizationEntity.setPhone(profileDto.getPhone());
        organizationEntity.setWeb(profileDto.getWeb());
        TrackableUtil.fillUpdate(organizationEntity, userContext);
    }

    private PublicProfileVisibilityDto convert(OrganizationProfileVisibilityEntity organizationProfileVisibilityEntity) {
        PublicProfileVisibilityDto publicProfileVisibilityDto = new PublicProfileVisibilityDto();
        publicProfileVisibilityDto.setOrganizationId(organizationProfileVisibilityEntity.getOrganizationId());
        publicProfileVisibilityDto.setVisible(organizationProfileVisibilityEntity.isVisible());
        publicProfileVisibilityDto.setExpertsVisible(organizationProfileVisibilityEntity.isExpertsVisible());
        publicProfileVisibilityDto.setGraphVisible(organizationProfileVisibilityEntity.isGraphVisible());
        publicProfileVisibilityDto.setOutcomesVisible(organizationProfileVisibilityEntity.isOutcomesVisible());
        publicProfileVisibilityDto.setKeywordsVisible(organizationProfileVisibilityEntity.isKeywordsVisible());
        publicProfileVisibilityDto.setProjectsVisible(organizationProfileVisibilityEntity.isProjectsVisible());
        return publicProfileVisibilityDto;
    }

    private void fill(OrganizationProfileVisibilityEntity organizationProfileVisibilityEntity, PublicProfileVisibilityDto publicProfileVisibilityDto) {
        organizationProfileVisibilityEntity.setVisible(publicProfileVisibilityDto.isVisible());
        organizationProfileVisibilityEntity.setExpertsVisible(publicProfileVisibilityDto.isExpertsVisible());
        organizationProfileVisibilityEntity.setGraphVisible(publicProfileVisibilityDto.isGraphVisible());
        organizationProfileVisibilityEntity.setKeywordsVisible(publicProfileVisibilityDto.isKeywordsVisible());
        organizationProfileVisibilityEntity.setOrganizationId(publicProfileVisibilityDto.getOrganizationId());
        organizationProfileVisibilityEntity.setOutcomesVisible(publicProfileVisibilityDto.isOutcomesVisible());
        organizationProfileVisibilityEntity.setProjectsVisible(publicProfileVisibilityDto.isProjectsVisible());
    }

    private OrganizationEntity doCreateOrganizationFromDWH(DWHOrganizationDto dto, Map<Long, DWHOrganizationDto> dtoMap, Map<Long, OrganizationEntity> entityMap) {
        if (dto == null) return null;
        final OrganizationEntity organizationEntity = new OrganizationEntity();
        organizationEntity.setOriginalOrganizationId(dto.getOriginalOrganizationId());
        organizationEntity.setOrganizationName(dto.getOrganizationName());
        organizationEntity.setRank(dto.getRank());
        organizationDao.saveOrganization(organizationEntity);
        return organizationEntity;
    }

    private boolean canOrganizationBeReplaced(OrganizationEntity organizationEntity) {
        return organizationEntity.getOrganizationUserRoleEntities() == null ||
                organizationEntity.getOrganizationUserRoleEntities().isEmpty() ||
                organizationEntity.getOrganizationUserRoleEntities().stream().allMatch(OrganizationUserRoleEntity::isDeleted);
    }

    private void fixOrganizationSubstitutions(List<OrganizationEntity> organizationEntities) {
        final Map<Long, Long> substitutionMap = createSubstitutionMap(organizationEntities);
        final Map<Long, Long> originalSubstitutionMap = new HashMap<>(substitutionMap);
        final Map<Long, OrganizationEntity> organizationMap = organizationEntities.stream().collect(Collectors.toMap(OrganizationEntity::getOrganizationId, Function.identity()));
        for (OrganizationEntity organizationEntity : organizationEntities) {
            final Long organizationId = organizationEntity.getOrganizationId();
            final Set<Long> organizationIds = new HashSet<>();
            Long substituteOrganizationId = organizationId;
            while (!organizationIds.contains(substituteOrganizationId) && substitutionMap.containsKey(substituteOrganizationId)) {
                organizationIds.add(substituteOrganizationId);
                substituteOrganizationId = substitutionMap.get(substituteOrganizationId);
            }

            substitutionMap.put(organizationId, substituteOrganizationId);

            if (originalSubstitutionMap.get(organizationId).equals(substituteOrganizationId)) continue;

            organizationEntity.setSubstituteOrganizationEntity(organizationMap.get(substituteOrganizationId));
        }
    }

    @Override
    @Transactional
    public OrganizationTypeWrapper getOrganizationTypes(Long organizationId) {
        final OrganizationTypeWrapper organizationTypeWrapper = new OrganizationTypeWrapper();
        final List<OrganizationTypeEntity> organizationTypeEntities = organizationTypeDao.find();
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        final OrganizationTypeEntity organizationTypeEntity = organizationEntity.getOrganizationTypeEntity();
        if (organizationTypeEntity != null)
            organizationTypeWrapper.setSelected(organizationTypeEntityToDto(organizationTypeEntity));
        for (OrganizationTypeEntity entity : organizationTypeEntities) {
            organizationTypeWrapper.getOrganizationTypeDtos().add(organizationTypeEntityToDto(entity));
        }
        return organizationTypeWrapper;
    }

    @Override
    @Transactional
    public RecombeeConnectionDto getRecombeeConnection(Long organizationId) {
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        return OrganizationUtil.convertToConnectionDto(organizationEntity);
    }

    @Override
    @Transactional
    public List<RecombeeConnectionDto> getRecombeeConnections(Set<Long> organizationIds) {
        return organizationDao.findByIds(organizationIds).stream()
                .map(OrganizationUtil::convertToConnectionDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<OrganizationDetailDto> getAllRecombeeConnections() {
        return organizationDao.getRecombeeOrganizations().stream()
                .map(OrganizationUtil::convertToDetail)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<OrganizationTypeDto> getOrganizationsTypes() {
        final List<OrganizationTypeEntity> organizationTypeEntities = organizationTypeDao.find();
        final List<OrganizationTypeDto> organizationTypeDtos = new ArrayList<>();
        for (OrganizationTypeEntity entity : organizationTypeEntities) {
            organizationTypeDtos.add(organizationTypeEntityToDto(entity));
        }
        return organizationTypeDtos;
    }

    private OrganizationTypeDto organizationTypeEntityToDto(OrganizationTypeEntity entity){
        OrganizationTypeDto organizationTypeDto = new OrganizationTypeDto();
        organizationTypeDto.setId(entity.getId());
        organizationTypeDto.setName(entity.getName());
        organizationTypeDto.setDescription(entity.getDescription());
        return organizationTypeDto;
    }
}
