package ai.unico.platform.store.entity;


import javax.persistence.*;

@Entity
public class SearchHistSecondaryQueryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String query;

    private Float weight;

    @ManyToOne
    @JoinColumn(name = "search_hist_id")
    private SearchHistEntity searchHistEntity;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SearchHistEntity getSearchHistEntity() {
        return searchHistEntity;
    }

    public void setSearchHistEntity(SearchHistEntity searchHistEntity) {
        this.searchHistEntity = searchHistEntity;
    }
}
