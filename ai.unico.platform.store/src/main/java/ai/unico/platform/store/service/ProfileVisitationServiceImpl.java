package ai.unico.platform.store.service;

import ai.unico.platform.store.api.service.ProfileVisitationService;
import ai.unico.platform.store.dao.ProfileVisitationDao;
import ai.unico.platform.store.dao.SearchHistoryDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.ProfileVisitationEntity;
import ai.unico.platform.store.entity.SearchHistEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class ProfileVisitationServiceImpl implements ProfileVisitationService {

    @Autowired
    private ProfileVisitationDao userProfileVisitationDao;

    @Autowired
    private SearchHistoryDao searchHistoryDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Override
    @Transactional
    public void saveUserProfileVisitation(UserContext userContext, Long userId, Long searchId) {
        final ProfileVisitationEntity visitationEntity = new ProfileVisitationEntity();
        final UserProfileEntity userProfile = userProfileDao.findUserProfile(userId);
        visitationEntity.setUserProfileEntity(userProfile);
        fillAndSaveVisitation(visitationEntity, userContext, searchId);
    }

    @Override
    @Transactional
    public void saveExpertProfileVisitation(UserContext userContext, Long expertId, Long searchId) {
        final ProfileVisitationEntity visitationEntity = new ProfileVisitationEntity();
        visitationEntity.setExpertId(expertId);
        fillAndSaveVisitation(visitationEntity, userContext, searchId);
    }

    private void fillAndSaveVisitation(ProfileVisitationEntity visitationEntity, UserContext userContext, Long searchId) {
        if (searchId != null) {
            final SearchHistEntity searchHistory = searchHistoryDao.findSearchHistory(searchId);
            visitationEntity.setSearchHistEntity(searchHistory);
        }
        final Long visitorUserId;
        if (userContext != null) {
            visitorUserId = userContext.getUserId();
        } else {
            visitorUserId = null;
        }
        TrackableUtil.fillCreateTrackable(visitationEntity, visitorUserId);
        userProfileVisitationDao.saveVisitation(visitationEntity);
    }
}
