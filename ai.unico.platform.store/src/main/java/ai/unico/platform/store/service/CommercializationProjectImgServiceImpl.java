package ai.unico.platform.store.service;

import ai.unico.platform.store.api.exception.ChecksumMismatchException;
import ai.unico.platform.store.api.service.CommercializationProjectImgService;
import ai.unico.platform.store.dao.CommercializationProjectDao;
import ai.unico.platform.store.dao.FileDao;
import ai.unico.platform.store.entity.CommercializationProjectEntity;
import ai.unico.platform.store.entity.FileEntity;
import ai.unico.platform.store.util.CommercializationProjectIndexUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.CacheKey;
import ai.unico.platform.util.ImgUtil;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.cache2k.Cache;
import org.cache2k.Cache2kBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.concurrent.TimeUnit;

public class CommercializationProjectImgServiceImpl implements CommercializationProjectImgService {

    @Autowired
    private CommercializationProjectDao commercializationProjectDao;

    @Autowired
    private FileDao fileDao;

    @PersistenceContext
    private EntityManager entityManager;

    private final Cache<CacheKey, FileDto> cache = new Cache2kBuilder<CacheKey, FileDto>() {
    }
            .expireAfterWrite(30, TimeUnit.MINUTES)     // expire/refresh after 30 minutes
            .resilienceDuration(30, TimeUnit.SECONDS)   // cope with at most 30 seconds
            .permitNullValues(true)
            .refreshAhead(true)                         // keep fresh when expiring
            .loader(this::load)                         // auto populating function
            .build();

    @Override
    @Transactional
    public String saveImg(UserContext userContext, Long commercializationProjectId, FileDto fileDto) {
        ImgUtil.validateImageIsResizable(fileDto.getContent());
        final CommercializationProjectEntity commercializationProjectEntity = commercializationProjectDao.find(commercializationProjectId);
        if (commercializationProjectEntity == null)
            throw new NonexistentEntityException(CommercializationProjectEntity.class);
        final FileEntity imgFile = commercializationProjectEntity.getImgFile();
        final FileEntity fileEntity;

        if (imgFile == null) {
            fileEntity = new FileEntity();
            TrackableUtil.fillCreate(fileEntity, userContext);
        } else {
            fileEntity = imgFile;
            TrackableUtil.fillUpdate(fileEntity, userContext);
        }

        fileEntity.setName(fileDto.getName());
        fileEntity.setContent(fileDto.getContent());
        fileDao.save(fileEntity);
        commercializationProjectEntity.setImgFile(fileEntity);

        for (CacheKey key : cache.keys()) {
            if (key.getEntityId().equals(commercializationProjectId)) {
                cache.remove(key);
                break;
            }
        }
        entityManager.flush();
        CommercializationProjectIndexUtil.index(commercializationProjectEntity);
        return fileEntity.getHash();
    }

    @Override
    @Transactional
    public FileDto getImg(Long commercializationProjectId, String checkSum, String size) {
        final CacheKey cacheKey = new CacheKey(commercializationProjectId, checkSum, size);
        return cache.get(cacheKey);
    }

    private FileDto load(CacheKey cacheKey) {
        final Long userId = cacheKey.getEntityId();

        final CommercializationProjectEntity commercializationProjectEntity = commercializationProjectDao.find(userId);
        if (commercializationProjectEntity == null) return null;
        final FileEntity userImgFile = commercializationProjectEntity.getImgFile();
        if (userImgFile == null) return null;
        if (!userImgFile.getHash().equals(cacheKey.getCheckSum())) throw new ChecksumMismatchException();

        final byte[] logoContent = userImgFile.getContent();

        final byte[] content;
        final String sizeCode = cacheKey.getSize();
        if (sizeCode != null) {
            final int size = ImgUtil.codeToSize(sizeCode);
            content = ImgUtil.resize(logoContent, size);
        } else {
            content = logoContent;
        }
        return new FileDto(content, userImgFile.getName());
    }
}
