package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;
import ai.unico.platform.store.generator.EquipmentSequenceGenerator;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.id.enhanced.SequenceStyleGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "equipment")
@IdClass(EquipmentId.class)
public class EquipmentEntity extends Trackable {

    @Id
    @GenericGenerator(name = "equipment_generator", strategy = "ai.unico.platform.store.generator.EquipmentSequenceGenerator",
            parameters = {
                    @Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1"),
                    @Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = EquipmentSequenceGenerator.EQUIPMENT_SEQ)
            })
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "equipment_generator")
    @Column(name = "equipment_id", nullable = false, updatable = false)
    private Long equipmentId = -1L;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "language_code", referencedColumnName = "language_code")
    private LanguageEntity languageCode;
    @Column(name = "name", length = 256)
    private String name;

    @Column(name = "original_id")
    private Long originalId;

    @Column(name = "marking")
    private String marking;

    @Column(name = "description")
    private String description;

    @Column(name = "year")
    private Integer year;

    @Column(name = "service_life")
    private Integer serviceLife;

    @Column(name = "portable_device")
    private Boolean portableDevice;

    @Column(name = "hidden")
    private Boolean hidden;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "good_for_description", columnDefinition = "text")
    private String goodForDescription;
    @Column(name = "target_group", columnDefinition = "text")
    private String targetGroup;
    @Column(name = "preparation_time", columnDefinition = "text")
    private String preparationTime;
    @Column(name = "equipment_cost", columnDefinition = "text")
    private String equipmentCost;
    @Column(name = "infrastructure_description", columnDefinition = "text")
    private String infrastructureDescription;
    @Column(name = "duration_time", columnDefinition = "text")
    private String durationTime;
    @Column(name = "trl", columnDefinition = "text")
    private String trl;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type_id")
    private EquipmentTypeEntity type;

    @OneToMany(mappedBy = "equipmentEntity")
    private Set<UserEquipmentEntity> userEquipmentEntities = new HashSet<>();

    @OneToMany(mappedBy = "equipmentEntity")
    private Set<EquipmentOrganizationEntity> equipmentOrganizationEntities = new HashSet<>();

    @OneToMany(mappedBy = "equipment")
    private Set<EquipmentTagEntity> specialization = new HashSet<>();

    public EquipmentTypeEntity getType() {
        return type;
    }

    public void setType(EquipmentTypeEntity type) {
        this.type = type;
    }

    public Boolean getPortableDevice() {
        return portableDevice;
    }

    public void setPortableDevice(Boolean portableDevice) {
        this.portableDevice = portableDevice;
    }

    public Integer getServiceLife() {
        return serviceLife;
    }

    public void setServiceLife(Integer serviceLife) {
        this.serviceLife = serviceLife;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMarking() {
        return marking;
    }

    public void setMarking(String marking) {
        this.marking = marking;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(Long equipmentId) {
        this.equipmentId = equipmentId;
    }

    public Set<UserEquipmentEntity> getUserEquipmentEntities() {
        return userEquipmentEntities;
    }

    public void setUserEquipmentEntities(Set<UserEquipmentEntity> userEquipmentEntity) {
        this.userEquipmentEntities = userEquipmentEntity;
    }

    public Set<EquipmentOrganizationEntity> getEquipmentOrganizationEntities() {
        return equipmentOrganizationEntities;
    }

    public void setEquipmentOrganizationEntities(Set<EquipmentOrganizationEntity> equipmentOrganizationEntities) {
        this.equipmentOrganizationEntities = equipmentOrganizationEntities;
    }

    public Boolean isHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public Set<EquipmentTagEntity> getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Set<EquipmentTagEntity> specialization) {
        this.specialization = specialization;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getOriginalId() {
        return originalId;
    }

    public void setOriginalId(Long originalId) {
        this.originalId = originalId;
    }

    public LanguageEntity getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(LanguageEntity languageCode) {
        this.languageCode = languageCode;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public String getGoodForDescription() {
        return goodForDescription;
    }

    public void setGoodForDescription(String goodForDescription) {
        this.goodForDescription = goodForDescription;
    }

    public String getTargetGroup() {
        return targetGroup;
    }

    public void setTargetGroup(String targetGroup) {
        this.targetGroup = targetGroup;
    }

    public String getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(String preparationTime) {
        this.preparationTime = preparationTime;
    }

    public String getEquipmentCost() {
        return equipmentCost;
    }

    public void setEquipmentCost(String equipmentCost) {
        this.equipmentCost = equipmentCost;
    }

    public String getInfrastructureDescription() {
        return infrastructureDescription;
    }

    public void setInfrastructureDescription(String infrastructureDescription) {
        this.infrastructureDescription = infrastructureDescription;
    }

    public String getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(String durationTime) {
        this.durationTime = durationTime;
    }

    public String getTrl() {
        return trl;
    }

    public void setTrl(String trl) {
        this.trl = trl;
    }
}


