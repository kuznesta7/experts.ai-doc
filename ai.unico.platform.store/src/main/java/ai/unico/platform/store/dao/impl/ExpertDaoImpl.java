package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ExpertDao;
import ai.unico.platform.store.entity.ExpertEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.model.UserContext;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class ExpertDaoImpl implements ExpertDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public ExpertEntity findOrCreate(Long expertId, UserContext userContext) {
        final ExpertEntity expertEntity = find(expertId);
        if (expertEntity != null) return expertEntity;
        final ExpertEntity newExpertEntity = new ExpertEntity();
        newExpertEntity.setExpertId(expertId);
        TrackableUtil.fillCreate(newExpertEntity, userContext);
        save(newExpertEntity);
        return newExpertEntity;
    }

    @Override
    public ExpertEntity find(Long expertId) {
        return entityManager.find(ExpertEntity.class, expertId);
    }

    @Override
    public void save(ExpertEntity expertEntity) {
        entityManager.persist(expertEntity);
    }

    @Override
    public List<ExpertEntity> findAll() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ExpertEntity.class, "expert");
        return criteria.list();
    }
}
