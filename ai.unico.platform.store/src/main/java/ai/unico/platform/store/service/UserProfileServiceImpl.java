package ai.unico.platform.store.service;

import ai.unico.platform.search.api.service.ElasticsearchService;
import ai.unico.platform.search.api.service.ExpertIndexService;
import ai.unico.platform.store.api.dto.ItemPreviewDto;
import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.dto.UserProfileDto;
import ai.unico.platform.store.api.dto.UserProfileIndexDto;
import ai.unico.platform.store.api.service.UserOrganizationService;
import ai.unico.platform.store.api.service.UserProfileService;
import ai.unico.platform.store.dao.ItemTypeDao;
import ai.unico.platform.store.dao.TagDao;
import ai.unico.platform.store.dao.UserKeywordDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.store.util.UserProfileIndexUtil;
import ai.unico.platform.store.util.UserProfileUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class UserProfileServiceImpl implements UserProfileService {

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private ItemTypeDao itemTypeDao;

    @Autowired
    private TagDao tagDao;

    @Autowired
    private UserOrganizationService userOrganizationService;

    @Autowired
    private UserKeywordDao userKeywordDao;

    @Override
    @Transactional
    public UserProfileDto loadProfile(Long userId) throws NonexistentEntityException {
        final UserProfileEntity userProfileEntity = userProfileDao.findUserProfile(userId);
        if (userProfileEntity == null || userProfileEntity.isDeleted()) {
            throw new NonexistentEntityException(UserProfileEntity.class);
        }
        final UserProfileDto userProfileDto = UserProfileUtil.convert(userProfileEntity);
        final List<ItemTypeGroupEntity> groups = itemTypeDao.findGroups();
        for (ItemTypeGroupEntity itemTypeGroupEntity : groups) {
            final UserProfileDto.ItemTypeGroupWithCountsDto groupDto = new UserProfileDto.ItemTypeGroupWithCountsDto();
            groupDto.setItemTypeGroupId(itemTypeGroupEntity.getTypeGroupId());
            groupDto.setItemTypeGroupTitle(itemTypeGroupEntity.getTitle());
            groupDto.setItemTypeIds(itemTypeGroupEntity.getItemTypeEntities().stream().map((ItemTypeEntity::getTypeId)).collect(Collectors.toSet()));
            for (ItemPreviewDto itemPreviewDto : userProfileDto.getItemPreviewDtos()) {
                if (groupDto.getItemTypeIds().contains(itemPreviewDto.getItemTypeId())) {
                    groupDto.setCount(groupDto.getCount() + 1);
                }
            }
            userProfileDto.getItemTypeGroupsWithCount().add(groupDto);
        }
        return userProfileDto;
    }

    @Override
    @Transactional
    public Long createProfile(UserProfileDto userProfileDto, UserContext userContext) {
        final UserProfileEntity userProfileEntity = convert(userProfileDto);
        userProfileEntity.setEmail(userProfileDto.getEmail());
        userProfileEntity.setClaimable(userProfileDto.isClaimable());
        userProfileEntity.setInvitation(userProfileDto.isInvitation());
        TrackableUtil.fillCreate(userProfileEntity, userContext);
        userProfileDao.createUserProfile(userProfileEntity);
        return userProfileEntity.getUserId();
    }

    @Override
    @Transactional
    public void updateProfile(UserProfileDto userProfileDto, UserContext userContext) throws NonexistentEntityException {
        final Long userId = userProfileDto.getUserId();
        final UserProfileEntity userProfileEntity = userProfileDao.findUserProfile(userId);
        if (!userProfileDto.getName().equals(userProfileEntity.getName())) {
            userContext.setFullName(userProfileDto.getName());
        }
        for(OrganizationBaseDto organization: userProfileDto.getOrganizationDtos()){
            userOrganizationService.changeExpertOrganizationVisibility(userId, organization.getOrganizationId(), organization.isVisible(), userContext);
        }
        for(OrganizationBaseDto organization: userProfileDto.getOrganizationDtos()){
            userOrganizationService.changeExpertOrganizationActivity(userId, organization.getOrganizationId(), organization.isActive(), userContext);
        }
        userProfileEntity.setInvitation(false);
        fillEntity(userProfileEntity, userProfileDto, userContext.getUserId());
        userOrganizationService.updateOrganizationsForUser(userId, userProfileDto.getOrganizationDtos(), userContext);
    }

    @Override
    @Transactional
    public void deleteProfile(Long userId, UserContext userContext) throws NonexistentEntityException {
        final UserProfileEntity userProfileEntity = userProfileDao.findUserProfile(userId);
        userProfileEntity.setDeleted(true);
        TrackableUtil.fillUpdate(userProfileEntity, userContext);
    }

    @Override
    @Transactional
    public void loadAndIndexUserProfile(Long userId) {
        final UserProfileEntity userProfile = userProfileDao.findUserProfile(userId);
        UserProfileIndexUtil.indexUserProfile(userProfile);
        final ExpertIndexService indexService = ServiceRegistryUtil.getService(ExpertIndexService.class);
        try {
            indexService.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void loadAndIndexUserProfiles(Collection<Long> userIds) {
        final List<UserProfileEntity> userProfileEntities = userProfileDao.findByIds(userIds);
        for (UserProfileEntity userProfileEntity : userProfileEntities) {
            UserProfileIndexUtil.indexUserProfile(userProfileEntity);
        }
        final ElasticsearchService elasticsearchService = ServiceRegistryUtil.getService(ElasticsearchService.class);
        elasticsearchService.flushSyncedAll();
    }

    @Override
    @Transactional
    public List<UserProfileIndexDto> findUserProfiles(Integer limit, Integer offset) {
        final List<UserProfileEntity> userProfiles = userProfileDao.findUserProfiles(limit, offset);
        final List<UserProfileIndexDto> userProfileDtos = userProfiles.stream()
                .map(UserProfileUtil::convertToIndex)
                .collect(Collectors.toList());
        return userProfileDtos;
    }

    private UserProfileEntity convert(UserProfileDto userProfileDto) {
        final UserProfileEntity userProfileEntity = new UserProfileEntity();
        userProfileEntity.setUserOrganizationEntities(new HashSet<>());
        userProfileEntity.setName(userProfileDto.getName());
        userProfileEntity.setEmail(userProfileDto.getEmail());
        userProfileEntity.setDeleted(false);

        return userProfileEntity;
    }

    private void fillEntity(UserProfileEntity userProfileEntity, UserProfileDto userProfileDto, Long authorId) {
        userProfileEntity.setUserId(userProfileDto.getUserId());
        userProfileEntity.setName(userProfileDto.getName());
        userProfileEntity.setDescription(userProfileDto.getDescription());
        userProfileEntity.setPositionDescription(userProfileDto.getPositionDescription());

        final Set<UserProfileKeywordEntity> userProfileKeywordEntities = userProfileEntity.getUserProfileKeywordEntities();
        final Set<String> currentTags = userProfileKeywordEntities.stream().filter(x -> !x.isDeleted())
                .map(UserProfileKeywordEntity::getTagEntity)
                .map(TagEntity::getValue)
                .collect(Collectors.toSet());
        final Set<String> keywordsToAdd = userProfileDto.getKeywords().stream()
                .filter(keyword -> !currentTags.contains(keyword))
                .collect(Collectors.toSet());
        final Set<String> keywordsToRemove = currentTags.stream()
                .filter(keyword -> !userProfileDto.getKeywords()
                        .contains(keyword)).collect(Collectors.toSet());
        for (UserProfileKeywordEntity userProfileKeywordEntity : userProfileKeywordEntities) {
            final String value = userProfileKeywordEntity.getTagEntity().getValue();
            if (keywordsToRemove.contains(value)) userProfileKeywordEntity.setDeleted(true);
        }
        final List<TagEntity> tagEntities = tagDao.findOrCreateTagsByValues(keywordsToAdd);
        for (TagEntity tagEntity : tagEntities) {
            final UserProfileKeywordEntity userProfileKeywordEntity = new UserProfileKeywordEntity();
            userProfileKeywordEntity.setTagEntity(tagEntity);
            userProfileKeywordEntity.setUserProfileEntity(userProfileEntity);
            userKeywordDao.save(userProfileKeywordEntity);
            userProfileEntity.getUserProfileKeywordEntities().add(userProfileKeywordEntity);
        }
    }
}
