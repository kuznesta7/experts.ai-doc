package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ProjectBudgetEntity;

public interface ProjectBudgetDao {

    void save(ProjectBudgetEntity projectBudgetEntity);

    ProjectBudgetEntity find(Long projectId, Long organizationId, Integer year);

}
