package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.CommercializationProjectDao;
import ai.unico.platform.store.entity.CommercializationProjectEntity;
import ai.unico.platform.store.entity.CommercializationProjectKeywordEntity;
import ai.unico.platform.store.entity.CommercializationProjectStatusEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;

public class CommercializationProjectDaoImpl implements CommercializationProjectDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public CommercializationProjectEntity find(Long commercializationProjectId) {
        return entityManager.find(CommercializationProjectEntity.class, commercializationProjectId);
    }

    @Override
    public void save(CommercializationProjectEntity commercializationProjectEntity) {
        entityManager.persist(commercializationProjectEntity);
    }

    @Override
    public void save(CommercializationProjectStatusEntity commercializationProjectStatusEntity) {
        entityManager.persist(commercializationProjectStatusEntity);
    }

    @Override
    public void save(CommercializationProjectKeywordEntity projectKeywordEntity) {
        entityManager.persist(projectKeywordEntity);
    }

    @Override
    public List<CommercializationProjectEntity> findAll(Integer limit, Integer offset) {
        final Criteria idCriteria = hibernateCriteriaCreator.createCriteria(CommercializationProjectEntity.class, "project");
        idCriteria.setMaxResults(limit);
        idCriteria.setFirstResult(offset);
        idCriteria.addOrder(Order.asc("commercializationId"));
        idCriteria.setProjection(Projections.property("commercializationId"));
        idCriteria.add(Restrictions.eq("deleted", false));
        final List<Long> ids = idCriteria.list();
        if (ids.isEmpty()) return Collections.emptyList();

        final Criteria criteria = hibernateCriteriaCreator.createCriteria(CommercializationProjectEntity.class, "project");
        criteria.add(Restrictions.in("commercializationId", ids));
        criteria.createAlias("commercializationProjectStatusEntities", "status", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("commercializationTeamMemberEntities", "teamMember", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("commercializationProjectKeywordEntities", "keyword", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("commercializationProjectOrganizationEntities", "organization", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("commercializationProjectCategoryEntities", "category", JoinType.LEFT_OUTER_JOIN);
        criteria.setFetchMode("status", FetchMode.JOIN);
        criteria.setFetchMode("teamMember", FetchMode.JOIN);
        criteria.setFetchMode("keyword", FetchMode.JOIN);
        criteria.setFetchMode("organization", FetchMode.JOIN);
        criteria.setFetchMode("category", FetchMode.JOIN);
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @Override
    public List<CommercializationProjectStatusEntity> findStatusHistory(Long commercializationProjectId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(CommercializationProjectStatusEntity.class, "projectStatus");
        criteria.add(Restrictions.eq("commercializationProjectEntity.commercializationId", commercializationProjectId));
        criteria.addOrder(Order.asc("dateInsert"));
        return criteria.list();
    }

    @Override
    public List<CommercializationProjectEntity> findForUser(Long userId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(CommercializationProjectEntity.class, "project");
        criteria.createAlias("commercializationTeamMemberEntities", "teamMember", JoinType.LEFT_OUTER_JOIN);
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("teamMember.userProfileEntity.userId", userId));
        return criteria.list();
    }

    @Override
    public List<CommercializationProjectEntity> findForExpert(Long expertId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(CommercializationProjectEntity.class, "project");
        criteria.createAlias("commercializationTeamMemberEntities", "teamMember", JoinType.LEFT_OUTER_JOIN);
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("teamMember.expertId", expertId));
        return criteria.list();
    }
}
