package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class FollowedProfileEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long followedProfileId;

    @ManyToOne
    @JoinColumn(name = "follower_user_id")
    private UserProfileEntity followerUser;

    @ManyToOne
    @JoinColumn(name = "followed_user_id")
    private UserProfileEntity followedUser;

    @OneToMany(mappedBy = "followedProfileEntity")
    private Set<FollowedProfileWorkspaceEntity> followedProfileWorkspaceEntities = new HashSet<>();

    private Long followedExpertId;

    private boolean deleted;

    public Long getFollowedProfileId() {
        return followedProfileId;
    }

    public void setFollowedProfileId(Long followedProfileId) {
        this.followedProfileId = followedProfileId;
    }

    public UserProfileEntity getFollowerUser() {
        return followerUser;
    }

    public void setFollowerUser(UserProfileEntity followerUser) {
        this.followerUser = followerUser;
    }

    public UserProfileEntity getFollowedUser() {
        return followedUser;
    }

    public void setFollowedUser(UserProfileEntity followedUser) {
        this.followedUser = followedUser;
    }

    public Set<FollowedProfileWorkspaceEntity> getFollowedProfileWorkspaceEntities() {
        return followedProfileWorkspaceEntities;
    }

    public void setFollowedProfileWorkspaceEntities(Set<FollowedProfileWorkspaceEntity> followedProfileWorkspaceEntities) {
        this.followedProfileWorkspaceEntities = followedProfileWorkspaceEntities;
    }

    public Long getFollowedExpertId() {
        return followedExpertId;
    }

    public void setFollowedExpertId(Long followedExpertId) {
        this.followedExpertId = followedExpertId;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

}
