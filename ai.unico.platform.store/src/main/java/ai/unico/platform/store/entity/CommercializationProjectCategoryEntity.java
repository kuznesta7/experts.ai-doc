package ai.unico.platform.store.entity;

import javax.persistence.*;

@Entity
public class CommercializationProjectCategoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commercializationProjectCategoryId;

    @ManyToOne
    @JoinColumn(name = "commercializationProjectId")
    private CommercializationProjectEntity commercializationProjectEntity;

    @ManyToOne
    @JoinColumn(name = "commercializationCategoryId")
    private CommercializationCategoryEntity commercializationCategoryEntity;

    private boolean deleted;

    public CommercializationProjectEntity getCommercializationProjectEntity() {
        return commercializationProjectEntity;
    }

    public void setCommercializationProjectEntity(CommercializationProjectEntity commercializationProjectEntity) {
        this.commercializationProjectEntity = commercializationProjectEntity;
    }

    public CommercializationCategoryEntity getCommercializationCategoryEntity() {
        return commercializationCategoryEntity;
    }

    public void setCommercializationCategoryEntity(CommercializationCategoryEntity commercializationCategoryEntity) {
        this.commercializationCategoryEntity = commercializationCategoryEntity;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
