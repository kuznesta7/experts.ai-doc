package ai.unico.platform.store.util;

import ai.unico.platform.store.entity.SearchHistEntity;
import ai.unico.platform.util.model.UserContext;

import java.util.Objects;

public class SecurityUtil {

    public static boolean canUserSeeSearchHistory(SearchHistEntity searchHistEntity, String shareCode, UserContext userContext) {
        if (searchHistEntity.getShareCode() == null) {
            return Objects.equals(searchHistEntity.getUserInsert(), userContext.getUserId());
        }
        return searchHistEntity.getShareCode().equals(shareCode);
    }

}
