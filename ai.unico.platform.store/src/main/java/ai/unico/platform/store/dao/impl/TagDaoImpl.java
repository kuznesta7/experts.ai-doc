package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.TagDao;
import ai.unico.platform.store.entity.TagEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;
import java.util.stream.Collectors;

public class TagDaoImpl implements TagDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public List<TagEntity> findRelevantTags(String query) {
        if (query == null) {
            query = "";
        }
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(TagEntity.class, "tag");
        criteria.add(Restrictions.ilike("value", query, MatchMode.ANYWHERE));
        criteria.setMaxResults(10);
//        criteria.add(Restrictions.eq("autocomplete", true));
        return (List<TagEntity>) criteria.list();
    }

    @Override
    public List<TagEntity> findOrCreateTagsByValues(Collection<String> values) {
        if (values.isEmpty()) return Collections.emptyList();
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(TagEntity.class, "tag");
        final Set<String> tagsNorm = values.stream().map(x -> x.trim().toLowerCase()).collect(Collectors.toSet());
        criteria.add(Restrictions.in("value", tagsNorm));
        final List<TagEntity> list = criteria.list();
        list.forEach(x -> tagsNorm.remove(x.getValue()));
        for (String tag : tagsNorm) {
            final TagEntity tagEntity = saveTag(tag);
            list.add(tagEntity);
        }
        return list;
    }

    @Override
    public TagEntity saveTag(String tag) {
        final TagEntity tagEntity = new TagEntity();
        tagEntity.setValue(tag.trim().toLowerCase());
        tagEntity.setAutocomplete(false);
        entityManager.persist(tagEntity);
        return tagEntity;
    }

    @Override
    public TagEntity findOrCreateTagByValues(String value) {
        final List<TagEntity> tagEntities = findOrCreateTagsByValues(Collections.singleton(value));
        final Optional<TagEntity> first = tagEntities.stream().findFirst();
        return first.orElse(null);
    }
}
