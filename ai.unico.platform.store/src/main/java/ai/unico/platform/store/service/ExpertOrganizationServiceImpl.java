package ai.unico.platform.store.service;

import ai.unico.platform.search.api.dto.EvidenceItemPreviewWithExpertsDto;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.service.EvidenceItemService;
import ai.unico.platform.search.api.service.ExpertIndexService;
import ai.unico.platform.search.api.service.ExpertLookupService;
import ai.unico.platform.search.api.wrapper.EvidenceItemWrapper;
import ai.unico.platform.store.api.dto.ExpertOrganizationDto;
import ai.unico.platform.store.api.dto.UserExpertBaseDto;
import ai.unico.platform.store.api.service.ExpertOrganizationService;
import ai.unico.platform.store.api.service.ItemOrganizationService;
import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.store.api.service.UserItemService;
import ai.unico.platform.store.dao.ExpertOrganizationDao;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.entity.ExpertOrganizationEntity;
import ai.unico.platform.store.entity.OrganizationEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ExpertOrganizationServiceImpl implements ExpertOrganizationService {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Autowired
    private ExpertOrganizationDao expertOrganizationDao;

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired
    private OrganizationStructureService organizationStructureService;

    @Autowired
    private UserItemService userItemService;

    @Autowired
    private ItemOrganizationService itemOrganizationService;

    private final ExpertIndexService expertIndexService = ServiceRegistryProxy.createProxy(ExpertIndexService.class);

    @Override
    @Transactional(noRollbackForClassName = "IOException")
    public void addExpertOrganization(Long expertId, Long organizationId, boolean verify, UserContext userContext) {
        final ExpertOrganizationEntity expertOrganizationEntity = findOrCreate(expertId, organizationId, userContext);
        expertOrganizationEntity.setVerified(verify);
        TrackableUtil.fillUpdate(expertOrganizationEntity, userContext);
        try {
            expertIndexService.updateExpertOrganization(expertId, organizationId, false, verify);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional(noRollbackForClassName = "IOException")
    public void verifyExpertOrganization(Long expertId, Long organizationId, boolean verify, UserContext userContext) throws IOException {
        final ExpertLookupService expertLookupService = ServiceRegistryUtil.getService(ExpertLookupService.class);
        final UserExpertBaseDto userExpertBase = expertLookupService.findUserExpertBase(expertId);
        final Set<Long> expertOrganizationIds = userExpertBase.getOrganizationIds();
        for (OrganizationEntity organizationEntity : findOrganizationWithUnitEntities(organizationId)) {
            if (expertOrganizationIds.contains(organizationEntity.getOrganizationId())) {
                final ExpertOrganizationEntity expertOrganizationEntity = findOrCreate(expertId, organizationEntity.getOrganizationId(), userContext);
                expertOrganizationEntity.setVerified(verify);
                TrackableUtil.fillUpdate(expertOrganizationEntity, userContext);
                try {
                    expertIndexService.updateExpertOrganization(expertId, organizationEntity.getOrganizationId(), false, verify);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    @Transactional
    public void retireExpertOrganization(Long expertId, Long organizationId, boolean retired, UserContext userContext) throws IOException {
        final ExpertOrganizationEntity expertOrganizationEntity = findOrCreate(expertId, organizationId, userContext);
        expertOrganizationEntity.setRetired(retired);
        TrackableUtil.fillUpdate(expertOrganizationEntity, userContext);
        try {
            expertIndexService.retireExpertOrganization(expertId, organizationId, retired);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void changeExpertVisibilityInOrganization(Long expertId, Long organizationId, boolean visible, UserContext userContext) throws IOException {
        final ExpertOrganizationEntity expertOrganization = findOrCreate(expertId, organizationId, userContext);
        expertOrganization.setVisible(visible);
        TrackableUtil.fillUpdate(expertOrganization, userContext);
        try {
            expertIndexService.changeVisibilityInOrganization(expertId, organizationId, visible);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void changeExpertActivityInOrganization(Long expertId, Long organizationId, boolean active, UserContext userContext) throws IOException {
        final ExpertOrganizationEntity expertOrganization = findOrCreate(expertId, organizationId, userContext);
        expertOrganization.setActive(active);
        TrackableUtil.fillUpdate(expertOrganization, userContext);
        try {
            expertIndexService.changeActivityInOrganization(expertId, organizationId, active);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional(noRollbackForClassName = "IOException")
    public void removeOrganizationExperts(Set<Long> expertIds, Long organizationId, UserContext userContext) throws IOException {
        final ExpertLookupService expertLookupService = ServiceRegistryUtil.getService(ExpertLookupService.class);
        for (Long expertId : expertIds) {
            final UserExpertBaseDto userExpertBase = expertLookupService.findUserExpertBase(expertId);
            final Set<Long> expertOrganizationIds = userExpertBase.getOrganizationIds();
            for (OrganizationEntity organizationEntity : findOrganizationWithUnitEntities(organizationId)) {
                if (expertOrganizationIds.contains(organizationEntity.getOrganizationId())) {
                    final ExpertOrganizationEntity expertOrganizationEntity = findOrCreate(expertId, organizationEntity.getOrganizationId(), userContext);
                    expertOrganizationEntity.setDeleted(true);
                    TrackableUtil.fillUpdate(expertOrganizationEntity, userContext);
                    try {
                        expertIndexService.updateExpertOrganization(expertId, organizationEntity.getOrganizationId(), true, false);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    @Transactional
    public List<ExpertOrganizationDto> findExpertOrganizations() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ExpertOrganizationEntity.class, "expertOrganization");
        criteria.setProjection(Projections.projectionList()
                .add(Projections.property("expertId").as("expertId"))
                .add(Projections.property("organizationEntity.organizationId").as("organizationId"))
                .add(Projections.property("deleted").as("deleted"))
                .add(Projections.property("verified").as("verified"))
                .add(Projections.property("retired").as("retired"))
                .add(Projections.property("visible").as("visible"))
                .add(Projections.property("active").as("active"))
        );
        criteria.setResultTransformer(new AliasToBeanResultTransformer(ExpertOrganizationDto.class));
        return criteria.list();
    }

    private ExpertOrganizationEntity findOrCreate(Long expertId, Long organizationId, UserContext userContext) {
        final ExpertOrganizationEntity expertOrganizationEntity = expertOrganizationDao.findNotDeleted(expertId, organizationId);
        if (expertOrganizationEntity != null) {
            return expertOrganizationEntity;
        }
        final ExpertOrganizationEntity newExpertOrganizationEntity = new ExpertOrganizationEntity();
        final OrganizationEntity organizationEntity = organizationDao.find(organizationId);
        if (organizationEntity == null) throw new NonexistentEntityException(OrganizationEntity.class);
        newExpertOrganizationEntity.setOrganizationEntity(organizationEntity);
        newExpertOrganizationEntity.setExpertId(expertId);
        TrackableUtil.fillCreate(newExpertOrganizationEntity, userContext);
        expertOrganizationDao.save(newExpertOrganizationEntity);
        return newExpertOrganizationEntity;
    }

    private List<OrganizationEntity> findOrganizationWithUnitEntities(Long organizationId) {
        final Set<Long> childOrganizationIds = organizationStructureService.findChildOrganizationIds(organizationId, false);
        return organizationDao.findByIds(childOrganizationIds);
    }

    @Override
    public List<Long> addExpertItemsToOrganization(Long expertId, Long organizationId, boolean verify, UserContext userContext) throws IOException {
        final EvidenceFilter evidenceFilter = new EvidenceFilter();
        final EvidenceItemService service = ServiceRegistryUtil.getService(EvidenceItemService.class);
        assert service != null;
        evidenceFilter.setExpertCodes(Collections.singleton("DWH" + expertId));
        EvidenceItemWrapper evidenceItemWrapper = service.findAnalyticsItems(evidenceFilter);
        List<Long> itemIds = evidenceItemWrapper.getItemPreviewDtos()
                .stream()
                .map(EvidenceItemPreviewWithExpertsDto::getItemId)
                .collect(Collectors.toList());
        itemIds.removeAll(Collections.singleton(null));
        if (! itemIds.isEmpty())
            itemOrganizationService.addItemOrganization(organizationId, new HashSet<>(itemIds), verify, userContext);
        return itemIds;
    }
}
