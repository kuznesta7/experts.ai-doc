package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.EquipmentTypeDao;
import ai.unico.platform.store.entity.EquipmentEntity;
import ai.unico.platform.store.entity.EquipmentTypeEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class EquipmentTypeDaoImpl implements EquipmentTypeDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void save(EquipmentTypeEntity equipmentType) {
        entityManager.persist(equipmentType);
    }

    @Override
    public EquipmentTypeEntity find(Long typeId) {
        return entityManager.find(EquipmentTypeEntity.class, typeId);
    }

    @Override
    public List<EquipmentTypeEntity> findAll() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(EquipmentEntity.class, "equipment");
        return criteria.list();
    }
}
