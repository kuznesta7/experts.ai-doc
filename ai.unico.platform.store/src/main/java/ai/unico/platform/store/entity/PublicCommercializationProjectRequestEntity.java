package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class PublicCommercializationProjectRequestEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long publicCommercializationProjectRequestId;

    private String email;

    private String content;

    private String firstName;

    private String lastName;

    private String organizationName;

    @ManyToOne
    private CommercializationProjectEntity commercializationProjectEntity;

    public Long getPublicCommercializationProjectRequestId() {
        return publicCommercializationProjectRequestId;
    }

    public void setPublicCommercializationProjectRequestId(Long publicCommercializationProjectRequestId) {
        this.publicCommercializationProjectRequestId = publicCommercializationProjectRequestId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public CommercializationProjectEntity getCommercializationProjectEntity() {
        return commercializationProjectEntity;
    }

    public void setCommercializationProjectEntity(CommercializationProjectEntity commercializationProjectEntity) {
        this.commercializationProjectEntity = commercializationProjectEntity;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }
}
