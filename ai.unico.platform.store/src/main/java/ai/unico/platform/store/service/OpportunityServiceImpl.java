package ai.unico.platform.store.service;

import ai.unico.platform.search.api.service.OpportunityIndexService;
import ai.unico.platform.store.api.dto.OpportunityRegisterDto;
import ai.unico.platform.store.api.service.OpportunityRegistrationService;
import ai.unico.platform.store.api.service.OpportunityService;
import ai.unico.platform.store.dao.*;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class OpportunityServiceImpl implements OpportunityService {

    @Autowired
    private OpportunityDao opportunityDao;

    private final Map<Long, OrganizationEntity> organizationCache = new HashMap<>();
    private final Map<String, TagEntity> tagCache = new HashMap<>();
    @Autowired
    private OpportunityRegistrationService registrationService;
    @Autowired
    private TagDao tagDao;
    @Autowired
    private OrganizationDao organizationDao;
    @Autowired
    private OpportunityTypeDao opportunityTypeDao;
    @Autowired
    private JobTypeDao jobTypeDao;
    @Autowired
    private UserProfileDao userProfileDao;
    private OpportunityIndexService indexService = ServiceRegistryProxy.createProxy(OpportunityIndexService.class);

    public static void fill(OpportunityEntity entity, OpportunityRegisterDto dto) {
        //entity.setOriginalOpportunityId(dto.getOriginalId());
        entity.setOpportunityName(dto.getOpportunityName());
        entity.setOpportunityDescription(dto.getOpportunityDescription());
        entity.setOpportunitySignupDate(dto.getOpportunitySignupDate());
        entity.setOpportunityLocation(dto.getOpportunityLocation());
        entity.setOpportunityWage(dto.getOpportunityWage());
        entity.setOpportunityTechReq(dto.getOpportunityTechReq());
        entity.setOpportunityFormReq(dto.getOpportunityFormReq());
        entity.setOpportunityOtherReq(dto.getOpportunityOtherReq());
        entity.setOpportunityBenefit(dto.getOpportunityBenefit());
        entity.setOpportunityJobStartDate(dto.getOpportunityJobStartDate());
        entity.setOpportunityExtLink(dto.getOpportunityExtLink());
        entity.setOpportunityHomeOffice(dto.getOpportunityHomeOffice());
    }

    @Override
    @Transactional
    public List<OpportunityRegisterDto> findOpportunities(Integer limit, Integer offset, boolean ignoreDeleted) {
        return opportunityDao.getOpportunities(limit, offset, ignoreDeleted).stream().map(this::convert).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public OpportunityRegisterDto getOpportunity(Long opportunityId) {
        return convert(opportunityDao.getOpportunity(opportunityId));
    }

    @Override
    @Transactional
    public Long createOpportunity(OpportunityRegisterDto registerDto, UserContext userContext) throws IOException {
        OpportunityEntity opportunity = new OpportunityEntity();
        if (! registerDto.getMemberOrganizationIds().isEmpty()){
            registerDto.setMemberOrganizationIds(registerDto.getMemberOrganizationIds());
        }
        opportunityDao.save(opportunity);
        fillEntity(registerDto, opportunity);
        TrackableUtil.fillCreate(opportunity, userContext);
        indexService.reindexOpportunity(getOpportunity(opportunity.getOpportunityId()));
        return opportunity.getOpportunityId();
    }

    @Override
    @Transactional
    public void updateOpportunity(OpportunityRegisterDto registerDto, String opportunityCode, UserContext userContext) throws IOException {
        Long opportunityId;
        if (IdUtil.isExtId(opportunityCode)) {
            opportunityId = registrationService.registerOpportunity(opportunityCode, true, userContext);
        } else {
            opportunityId = IdUtil.getIntDataId(opportunityCode);
        }
        OpportunityEntity opportunity = opportunityDao.getOpportunity(opportunityId);
        fillEntity(registerDto, opportunity);
        TrackableUtil.fillUpdate(opportunity, userContext);
        indexService.reindexOpportunity(getOpportunity(opportunityId));

    }

    @Override
    @Transactional
    public void deleteOpportunity(String opportunityCode, UserContext userContext) throws IOException {
        final Long opportunityId;
        if (IdUtil.isExtId(opportunityCode)) {
            opportunityId = registrationService.registerOpportunity(opportunityCode, true, userContext);
        } else {
            opportunityId = IdUtil.getIntDataId(opportunityCode);
        }
        OpportunityEntity opportunity = opportunityDao.getOpportunity(opportunityId);
        opportunity.setDeleted(true);
        TrackableUtil.fillUpdate(opportunity, userContext);
        indexService.deleteOpportunity(IdUtil.convertToIntDataCode(opportunityId));
    }

    @Override
    @Transactional
    public void toggleHiddenOpportunity(String opportunityCode, Boolean hide, UserContext userContext) throws IOException {
        final Long opportunityId;
        if (IdUtil.isExtId(opportunityCode)) {
            opportunityId = registrationService.registerOpportunity(opportunityCode, true, userContext);
        } else {
            opportunityId = IdUtil.getIntDataId(opportunityCode);
        }
        OpportunityEntity opportunity = opportunityDao.getOpportunity(opportunityId);
        opportunity.setHidden(hide);
        TrackableUtil.fillUpdate(opportunity, userContext);
        indexService.reindexOpportunity(getOpportunity(opportunityId));
    }

    private void fillEntity(OpportunityRegisterDto opportunity, OpportunityEntity entity) {
        fill(entity, opportunity);
        entity.getKeywords()
                .forEach(x -> x.setDeleted(true)); //delete all
        entity.getKeywords().stream()
                .filter(x -> opportunity.getOpportunityKw().contains(x.getTag().getValue()))
                .forEach(x -> x.setDeleted(false)); //undelete already connected
        opportunity.getOpportunityKw()
                .removeAll(entity.getKeywords().stream().map(x -> x.getTag().getValue()).collect(Collectors.toSet())); //remove already added
        for (String kw : opportunity.getOpportunityKw()) {
            final OpportunityTagEntity opportunityTag = new OpportunityTagEntity();
            final TagEntity tagEntity = getTag(kw);
            opportunityTag.setTag(tagEntity);
            opportunityTag.setOpportunity(entity);
            opportunityDao.saveOpportunityTag(opportunityTag);
            entity.getKeywords().add(opportunityTag);
        }
        entity.setOpportunityTypeEntity(opportunityTypeDao.getOpportunityTypeEntity(opportunity.getOpportunityType()));
        Map<Long, JobTypeEntity> jobTypeEntityMap = jobTypeDao.getAllJobTypes().stream().collect(Collectors.toMap(JobTypeEntity::getId, x -> x));
        entity.getJobTypes().forEach(x -> x.setDeleted(true));
        entity.getJobTypes().stream()
                .filter(x -> opportunity.getJobTypes().contains(x.getJobType().getId()))
                .forEach(x -> x.setDeleted(false));
        opportunity.getJobTypes()
                .removeAll(entity.getJobTypes().stream().map(x -> x.getJobType().getId()).collect(Collectors.toSet()));
        for (Long typeId : opportunity.getJobTypes()) {
            final OpportunityJobTypeEntity opportunityJobType = new OpportunityJobTypeEntity();
            final JobTypeEntity jobType = jobTypeEntityMap.get(typeId);
            opportunityJobType.setOpportunity(entity);
            opportunityJobType.setJobType(jobType);
            opportunityDao.saveOpportunityJob(opportunityJobType);
            entity.getJobTypes().add(opportunityJobType);
        }

        if (opportunity.getOrganizationIds().isEmpty()) {
            entity.setOrganization(null);
        }

        if (!opportunity.getOrganizationIds().isEmpty() && (entity.getOrganization() == null || !opportunity.getOrganizationIds().contains(entity.getOrganization().getOrganizationId()))) {
            OrganizationEntity organization = getOrganization(opportunity.getOrganizationIds().stream().findAny().get());
            entity.setOrganization(organization);
        }

        if (! opportunity.getMemberOrganizationIds().isEmpty()){
            OrganizationEntity organization = getOrganization(opportunity.getMemberOrganizationIds().stream().findAny().get());
            entity.setOrganization(organization);
        }

        String prevCode = entity.getUserProfile() != null ?
                IdUtil.convertToIntDataCode(entity.getUserProfile().getUserId()) :
                IdUtil.convertToExtDataCode(entity.getExpertId());
        if (prevCode != null && opportunity.getExpertIds().isEmpty()) { //delete user expert
            entity.setExpertId(null);
            entity.setUserProfile(null);
        }
        if (!opportunity.getExpertIds().isEmpty() && !opportunity.getExpertIds().contains(prevCode)) { //replace user expert
            String expertCode = opportunity.getExpertIds().stream().findAny().get();
            if (IdUtil.isIntId(expertCode)) {
                entity.setUserProfile(userProfileDao.findUserProfile(IdUtil.getIntDataId(expertCode)));
                entity.setExpertId(null);
            } else {
                entity.setExpertId(IdUtil.getExtDataId(expertCode));
                entity.setUserProfile(null);
            }
        }
    }

    private OrganizationEntity getOrganization(Long organizationId) {
        return organizationCache.computeIfAbsent(organizationId, organizationDao::find);
    }

    private TagEntity getTag(String tagValue) {
        return tagDao.findOrCreateTagByValues(tagValue);
        //return tagCache.computeIfAbsent(tagValue, tagDao::findOrCreateTagByValues);
    }


    private OpportunityRegisterDto convert(OpportunityEntity entity) {
        OpportunityRegisterDto registerDto = new OpportunityRegisterDto();
        registerDto.setOpportunityCode(IdUtil.convertToIntDataCode(entity.getOpportunityId()));
        registerDto.setOriginalId(entity.getOriginalOpportunityId());
        registerDto.setOpportunityName(entity.getOpportunityName());
        registerDto.setOpportunityDescription(entity.getOpportunityDescription());
        registerDto.setOpportunitySignupDate(entity.getOpportunitySignupDate());
        registerDto.setOpportunityLocation(entity.getOpportunityLocation());
        registerDto.setOpportunityWage(entity.getOpportunityWage());
        registerDto.setOpportunityTechReq(entity.getOpportunityTechReq());
        registerDto.setOpportunityFormReq(entity.getOpportunityFormReq());
        registerDto.setOpportunityOtherReq(entity.getOpportunityOtherReq());
        registerDto.setOpportunityBenefit(entity.getOpportunityBenefit());
        registerDto.setOpportunityJobStartDate(entity.getOpportunityJobStartDate());
        registerDto.setOpportunityExtLink(entity.getOpportunityExtLink());
        registerDto.setOpportunityHomeOffice(entity.getOpportunityHomeOffice());
        registerDto.setOpportunityKw(entity.getKeywords().stream().filter(x -> !x.getDeleted()).map(OpportunityTagEntity::getTag).map(TagEntity::getValue).collect(Collectors.toSet()));
        registerDto.setOpportunityType(entity.getOpportunityTypeEntity().getId());
        registerDto.setJobTypes(entity.getJobTypes().stream().filter(x -> !x.getDeleted()).map(OpportunityJobTypeEntity::getJobType).map(JobTypeEntity::getId).collect(Collectors.toSet()));
        registerDto.setOrganizationIds(Collections.singleton(entity.getOrganization().getOrganizationId()));
        registerDto.setHidden(entity.isHidden());
        if (entity.getUserProfile() != null) {
            registerDto.setExpertIds(Collections.singleton(IdUtil.convertToIntDataCode(entity.getUserProfile().getUserId())));
        } else {
            registerDto.setExpertIds(Collections.singleton(IdUtil.convertToExtDataCode(entity.getExpertId())));
        }
        return registerDto;

    }
}
