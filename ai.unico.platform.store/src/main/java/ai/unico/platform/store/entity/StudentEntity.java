package ai.unico.platform.store.entity;

import javax.persistence.*;

@Entity
public class StudentEntity {
    @Id
    private String studentHash;

    private String username;

    private String recommendation;

    private String testGroup;

    public String getStudentHash() {
        return studentHash;
    }

    public void setStudentHash(String student_hash) {
        this.studentHash = student_hash;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

    public String getTestGroup() {
        return testGroup;
    }

    public void setTestGroup(String testGroup) {
        this.testGroup = testGroup;
    }
}
