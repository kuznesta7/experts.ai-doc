package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ProjectOrganizationDao;
import ai.unico.platform.store.entity.ProjectOrganizationEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class ProjectOrganizationDaoImpl implements ProjectOrganizationDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void saveProjectOrganization(ProjectOrganizationEntity projectOrganizationEntity) {
        entityManager.persist(projectOrganizationEntity);
    }

    @Override
    public List<ProjectOrganizationEntity> findProjectOrganizations(Long projectId) {
        final Criteria criteria = prepareCriteria(projectId);
        return criteria.list();
    }

    @Override
    public ProjectOrganizationEntity findProjectOrganization(Long projectId, Long organizationId) {
        final Criteria criteria = prepareCriteria(projectId);
        criteria.add(Restrictions.eq("projectOrganizationEntity.organizationEntity.organizationId", organizationId));
        final ProjectOrganizationEntity projectOrganizationEntity = (ProjectOrganizationEntity) criteria.uniqueResult();
        return projectOrganizationEntity;
    }

    private Criteria prepareCriteria(Long projectId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProjectOrganizationEntity.class, "projectOrganizationEntity");
        criteria.add(Restrictions.eq("projectOrganizationEntity.projectEntity.projectId", projectId));
        criteria.add(Restrictions.eq("projectOrganizationEntity.deleted", false));
        return criteria;
    }
}
