package ai.unico.platform.store.service;

import ai.unico.platform.search.api.service.EquipmentIndexService;
import ai.unico.platform.search.api.service.EquipmentSearchService;
import ai.unico.platform.store.api.dto.EquipmentRegisterDto;
import ai.unico.platform.store.api.dto.MultilingualDto;
import ai.unico.platform.store.api.service.EquipmentService;
import ai.unico.platform.store.api.service.ExpertToUserService;
import ai.unico.platform.store.dao.*;
import ai.unico.platform.store.entity.*;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class EquipmentServiceImpl implements EquipmentService {

    @Autowired
    EquipmentDao equipmentDao;

    @Autowired
    EquipmentOrganizationDao equipmentOrganizationDao;

    @Autowired
    OrganizationDao organizationDao;

    @Autowired
    UserEquipmentDao userEquipmentDao;

    @Autowired
    UserProfileDao userProfileDao;

    @Autowired
    ExpertToUserService expertToUserService;

    @Autowired
    EquipmentTypeDao equipmentTypeDao;

    @Autowired
    TagDao tagDao;

    @Autowired
    LanguageDao languageDao;

    final private EquipmentIndexService equipmentIndexService = ServiceRegistryProxy.createProxy(EquipmentIndexService.class);

    @Override
    @Transactional
    public List<EquipmentRegisterDto> findAllEquipment(Integer limit, Integer offset, boolean includeDeleted) {
        if (includeDeleted)
            return equipmentDao.findAll(limit, offset).stream().map(this::convert).collect(Collectors.toList());
        return equipmentDao.findAll(limit, offset).stream().filter(e -> e.isDeleted()!=null && !e.isDeleted()).map(this::convert).collect(Collectors.toList());
    }


    @Override
    @Transactional
    public List<MultilingualDto<EquipmentRegisterDto>> findAllEquipmentMultilingual(Integer limit, Integer offset, boolean includeDeleted) {
        List<EquipmentEntity> equipmentEntities = equipmentDao.findAll(limit, offset);
        if (!includeDeleted)
            equipmentEntities = equipmentEntities.stream().filter(e -> e.isDeleted()!=null && !e.isDeleted()).collect(Collectors.toList());
        List<MultilingualDto<EquipmentRegisterDto>> dtos = new ArrayList<>();
        for (EquipmentEntity equipmentEntity : equipmentEntities) {
            if (! includeDeleted && equipmentEntity.isDeleted())
                continue;
            MultilingualDto<EquipmentRegisterDto> dto = find(equipmentEntity.getEquipmentId(), dtos);
            if (dto == null) {
                dto = new MultilingualDto<>();
                dto.setId(equipmentEntity.getEquipmentId());
                dto.setCode(IdUtil.convertToIntDataCode(equipmentEntity.getEquipmentId()));
                dto.setTranslations(new ArrayList<>(Collections.singletonList(convert(equipmentEntity))));
                dto.setPreview(convert(equipmentEntity));
                dtos.add(dto);
            } else {
                dto.getTranslations().add(convert(equipmentEntity));
            }
        }
        return dtos;
    }

    @Override
    @Transactional
    public MultilingualDto<EquipmentRegisterDto> find(Long id) {
        List<EquipmentEntity> equipmentList = equipmentDao.find(id);
        return convert(equipmentList, id);
    }

    private MultilingualDto<EquipmentRegisterDto> convert(List<EquipmentEntity> equipmentList, Long id) {
        MultilingualDto<EquipmentRegisterDto> multilingual = new MultilingualDto<>();
        multilingual.setId(id);
        multilingual.setCode(IdUtil.convertToIntDataCode(id));
        multilingual.setTranslations(equipmentList.stream().map(this::convert).collect(Collectors.toList()));
        if (equipmentList.size() > 0)
            multilingual.setPreview(convert(equipmentList.get(0)));
        return multilingual;
    }

    private MultilingualDto<EquipmentRegisterDto> find(Long id, List<MultilingualDto<EquipmentRegisterDto>> dtos) {
        for (MultilingualDto<EquipmentRegisterDto> dto : dtos) {
            if (dto.getId().equals(id))
                return dto;
        }
        return null;
    }

    @Override
    @Transactional
    public List<EquipmentRegisterDto> findByOriginalIds(List<String> originalIds) {
        return equipmentDao.findByOriginalIds(originalIds).stream().map(this::convert).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<Long> getAllOriginalIds() {
        return equipmentDao.getAllOriginalIds();
    }

    @Override
    @Transactional
    public void deleteEquipment(String equipmentCode, UserContext userContext) throws NonexistentEntityException, IOException {
        final List<EquipmentEntity> equipmentList = getOrRegisterEquipment(equipmentCode, userContext);
        for (EquipmentEntity equipment : equipmentList) {
            equipment.setDeleted(true);
            if (IdUtil.isExtId(equipmentCode))
                equipment.setOriginalId(IdUtil.getExtDataId(equipmentCode));
            TrackableUtil.fillUpdate(equipment, userContext);
            equipmentDao.save(equipment);
        }
        if (!equipmentList.isEmpty())
            equipmentIndexService.deleteEquipment(IdUtil.convertToIntDataCode(equipmentList.get(0).getEquipmentId()));  // delete ST equipment
    }

    private Long saveEquipment(EquipmentRegisterDto equipmentRegisterDto, UserContext userContext) throws IOException {
        return saveEquipment(equipmentRegisterDto, userContext, null);
    }

    private Long saveEquipment(EquipmentRegisterDto equipmentRegisterDto, UserContext userContext, String originalCode) throws IOException {
        EquipmentEntity equipment = new EquipmentEntity();
        TrackableUtil.fillCreate(equipment, userContext);
        if (equipmentRegisterDto.getId() != null) {
            equipment.setEquipmentId(equipmentRegisterDto.getId());
        }
        equipment.setLanguageCode(languageDao.find(equipmentRegisterDto.getLanguageCode()));
        equipmentDao.save(equipment);
        fillEntity(equipmentRegisterDto, equipment, userContext);
        if (originalCode != null) {
            equipment.setOriginalId(IdUtil.getExtDataId(originalCode));
        }
        equipment.setDeleted(false);
        equipmentDao.save(equipment);
        equipmentRegisterDto.setId(equipment.getEquipmentId());
//        equipmentIndexService.reindexEquipment(equipmentRegisterDto);
        return equipment.getEquipmentId();
    }

    @Override
    @Transactional
    public Long saveEquipment(MultilingualDto<EquipmentRegisterDto> multilingualDto, UserContext userContext) throws IOException {
        Long id = multilingualDto.getId();
        if (id == null) {
            Iterator<EquipmentRegisterDto> iterator = multilingualDto.getTranslations().iterator();
            EquipmentRegisterDto equipmentRegisterDto = iterator.next();
            id = saveEquipment(equipmentRegisterDto, userContext, multilingualDto.getCode());
            multilingualDto.setId(id);
            multilingualDto.setCode(IdUtil.convertToIntDataCode(id));
            while (iterator.hasNext()) {
                equipmentRegisterDto = iterator.next();
                equipmentRegisterDto.setId(id);
                equipmentRegisterDto.setEquipmentCode(IdUtil.convertToIntDataCode(id));
                updateEquipment(equipmentRegisterDto, userContext);
            }
        } else {
            for (EquipmentRegisterDto equipmentRegisterDto : multilingualDto.getTranslations()) {
                equipmentRegisterDto.setId(id);
                equipmentRegisterDto.setEquipmentCode(IdUtil.convertToIntDataCode(id));
                updateEquipment(equipmentRegisterDto, userContext);
            }
        }
        equipmentIndexService.reindexEquipment(multilingualDto);
        return id;
    }

    @Override
    @Transactional
    public void updateEquipment(MultilingualDto<EquipmentRegisterDto> multilingualDto, UserContext userContext) throws IOException {
        saveEquipment(multilingualDto, userContext);
    }

    private void updateEquipment(EquipmentRegisterDto equipmentRegisterDto, UserContext userContext) throws IOException {
        if (IdUtil.isIntId(equipmentRegisterDto.getEquipmentCode())) {
            equipmentRegisterDto.setId(IdUtil.getIntDataId(equipmentRegisterDto.getEquipmentCode()));
            EquipmentEntity equipment = equipmentDao.find(equipmentRegisterDto.getId(), equipmentRegisterDto.getLanguageCode());
            if (equipment == null) {
                Long id = saveEquipment(equipmentRegisterDto, userContext, equipmentRegisterDto.getEquipmentCode());
                equipmentRegisterDto.setId(id);
                equipmentRegisterDto.setEquipmentCode(IdUtil.convertToIntDataCode(id));
            } else {
                TrackableUtil.fillUpdate(equipment, userContext);
                fillEntity(equipmentRegisterDto, equipment, userContext);
                equipmentDao.save(equipment);
            }
        } else {
            String originalCode = equipmentRegisterDto.getEquipmentCode();
            Long id = saveEquipment(equipmentRegisterDto, userContext, equipmentRegisterDto.getEquipmentCode());
            equipmentRegisterDto.setId(id);
            equipmentRegisterDto.setEquipmentCode(IdUtil.convertToIntDataCode(id));
            equipmentIndexService.deleteEquipment(originalCode);
        }
//        equipmentIndexService.reindexEquipment(equipmentRegisterDto);
    }

    @Override
    @Transactional
    public void toggleHidden(String equipmentCode, boolean hidden, UserContext userContext) throws IOException {
        final List<EquipmentEntity> equipmentList = getOrRegisterEquipment(equipmentCode, userContext);
        for (EquipmentEntity equipment : equipmentList) {
            equipment.setHidden(hidden);
            if (IdUtil.isExtId(equipmentCode)){
                System.out.println("isExt");
                equipment.setOriginalId(IdUtil.getExtDataId(equipmentCode));
            }
            TrackableUtil.fillUpdate(equipment, userContext);
            equipmentDao.save(equipment);
        }
        if (IdUtil.isIntId(equipmentCode)) {
            System.out.println("isInt");
            equipmentIndexService.reindexEquipment(convert(equipmentList, IdUtil.getIntDataId(equipmentCode)));
        }
    }

    private List<EquipmentEntity> getOrRegisterEquipment(String equipmentCode, UserContext userContext) throws IOException {
        Long equipmentId;
        if (IdUtil.isIntId(equipmentCode)) {
            equipmentId = IdUtil.getIntDataId(equipmentCode);
        } else {
            equipmentId = registerEquipment(equipmentCode, userContext);  // create new ST equipment
            equipmentIndexService.deleteEquipment(equipmentCode);  // delete DWH equipment
        }
        final List<EquipmentEntity> equipment = equipmentDao.find(equipmentId);
        if (equipment == null) throw new NonexistentEntityException(EquipmentEntity.class);
        return equipment;
    }

    @Override
    public Long registerEquipment(String originalCode, UserContext userContext) throws IOException {
        final EquipmentSearchService equipmentSearchService = ServiceRegistryProxy.createProxy(EquipmentSearchService.class);
        EquipmentRegisterDto equipmentRegisterDto = equipmentSearchService.getEquipmentForRegister(originalCode);
        return saveEquipment(equipmentRegisterDto, userContext);
    }

    private void resolveOrganizations(EquipmentEntity entity, EquipmentRegisterDto dto) {
        Map<Long, Boolean> newOrganizations = new HashMap<>();
        entity.getEquipmentOrganizationEntities().stream()
                .map(EquipmentOrganizationEntity::getOrganizationEntity)
                .map(OrganizationEntity::getOrganizationId)
                .collect(Collectors.toSet())
                .forEach(orgId -> newOrganizations.put(orgId, false));
        dto.getOrganizationIds().forEach(orgId -> newOrganizations.put(orgId, true));
        for (Map.Entry<Long, Boolean> entry: newOrganizations.entrySet()) {
            EquipmentOrganizationEntity equipmentOrganization = equipmentOrganizationDao.find(entity.getEquipmentId(), entry.getKey());
            if (equipmentOrganization == null) {
                equipmentOrganization = new EquipmentOrganizationEntity();
                equipmentOrganization.setEquipmentEntity(entity);
                equipmentOrganization.setOrganizationEntity(organizationDao.find(entry.getKey()));
            }
            equipmentOrganization.setDeleted(! entry.getValue());
            equipmentOrganizationDao.save(equipmentOrganization);
        }
    }

    private Set<Long> resolveUsers(EquipmentEntity entity, EquipmentRegisterDto dto, UserContext userContext) {
        Map<Long, Boolean> newUsers = new HashMap<>();
        entity.getUserEquipmentEntities().stream()
                .map(UserEquipmentEntity::getUserProfile)
                .map(UserProfileEntity::getUserId)
                .collect(Collectors.toSet())
                .forEach(userId -> newUsers.put(userId, false));
        for (String expertCode: dto.getExpertCodes()) {
            if (IdUtil.isIntId(expertCode)) {
                newUsers.put(IdUtil.getIntDataId(expertCode), true);
            } else {
                try {
                    newUsers.put(expertToUserService.expertToUser(expertCode, userContext), true);
                } catch (IOException e) {
                    System.out.println("Expert with code " + expertCode + " failed the conversion to internal profile.");
                }
            }
        }

        for (Map.Entry<Long, Boolean> entry: newUsers.entrySet()) {
            UserEquipmentEntity userEquipment = userEquipmentDao.find(entity.getEquipmentId(), entry.getKey());
            if (userEquipment == null) {
                userEquipment = new UserEquipmentEntity();
                userEquipment.setEquipmentEntity(entity);
                userEquipment.setUserProfile(userProfileDao.findUserProfile(entry.getKey()));
            }
            userEquipment.setDeleted(! entry.getValue());
            userEquipmentDao.save(userEquipment);
        }
        return newUsers.entrySet().stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).collect(Collectors.toSet());
    }

    private void fillEntity(EquipmentRegisterDto dto, EquipmentEntity entity, UserContext userContext) {
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        entity.setEquipmentCost(dto.getEquipmentCost());
        entity.setPreparationTime(dto.getPreparationTime());
        entity.setDurationTime(dto.getDurationTime());
        entity.setInfrastructureDescription(dto.getInfrastructureDescription());
        entity.setGoodForDescription(dto.getGoodForDescription());
        entity.setTargetGroup(dto.getTargetGroup());
        entity.setTrl(dto.getTrl());
        entity.setHidden(dto.isHidden());
        entity.setType(equipmentTypeDao.find(dto.getType()));

        Set<String> toAdd = new HashSet<>(dto.getSpecialization());
        for (EquipmentTagEntity tag: entity.getSpecialization()) {
            if (dto.getSpecialization().contains(tag.getTag().getValue())) {
                tag.setDeleted(false);
                toAdd.remove(tag.getTag().getValue());
            } else {
                tag.setDeleted(true);
            }
        }
        for (String kw : toAdd) {
            final EquipmentTagEntity equipmentTag = new EquipmentTagEntity();
            final TagEntity tagEntity = tagDao.findOrCreateTagByValues(kw);
            equipmentTag.setTag(tagEntity);
            equipmentTag.setEquipment(entity);
            equipmentTag.setDeleted(false);
            equipmentDao.saveTag(equipmentTag);
            entity.getSpecialization().add(equipmentTag);
        }
        Set<Long> userIds = resolveUsers(entity, dto, userContext);
        dto.setExpertCodes(userIds.stream().map(IdUtil::convertToIntDataCode).collect(Collectors.toSet()));
        resolveOrganizations(entity, dto);
    }

    private EquipmentRegisterDto convert(EquipmentEntity entity){
        final EquipmentRegisterDto dto = new EquipmentRegisterDto();

        dto.setId(entity.getEquipmentId());
        dto.setName(entity.getName());
        dto.setMarking(entity.getMarking());
        dto.setDescription(entity.getDescription());
        dto.setSpecialization(entity.getSpecialization().stream()
                .filter(x -> !x.isDeleted())
                .map(x -> x.getTag().getValue())
                .collect(Collectors.toSet()));
        dto.setYear(entity.getYear());
        dto.setServiceLife(entity.getServiceLife());
        dto.setPortableDevice(entity.getPortableDevice());
        dto.setExpertCodes(entity.getUserEquipmentEntities().stream()
                .filter(x -> !x.isDeleted())
                .map(x -> x.getUserProfile().getUserId())
                .map(IdUtil::convertToIntDataCode)
                .collect(Collectors.toSet()));
        dto.setOrganizationIds(entity.getEquipmentOrganizationEntities().stream()
                .filter(x -> !x.isDeleted())
                .map(x -> x.getOrganizationEntity().getOrganizationId())
                .collect(Collectors.toSet()));
        dto.setType(entity.getType().getId());
        dto.setHidden(entity.isHidden());
        dto.setLanguageCode(entity.getLanguageCode().getLanguageCode());
        dto.setGoodForDescription(entity.getGoodForDescription());
        dto.setTargetGroup(entity.getTargetGroup());
        dto.setPreparationTime(entity.getPreparationTime());
        dto.setEquipmentCost(entity.getEquipmentCost());
        dto.setInfrastructureDescription(entity.getInfrastructureDescription());
        dto.setDurationTime(entity.getDurationTime());
        dto.setTrl(entity.getTrl());
        return dto;
    }

}
