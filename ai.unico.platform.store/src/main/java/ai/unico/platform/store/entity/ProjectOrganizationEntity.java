package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;
import ai.unico.platform.util.enums.ProjectOrganizationRole;

import javax.persistence.*;
import java.util.Set;

@Entity
public class ProjectOrganizationEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long projectOrganizationId;

    @Enumerated(EnumType.STRING)
    private ProjectOrganizationRole projectOrganizationRole;

    @ManyToOne
    private ProjectEntity projectEntity;

    @ManyToOne
    private OrganizationEntity organizationEntity;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_project_organization")
    private ProjectOrganizationEntity parentProjectOrganizationEntity;

    @OneToMany(mappedBy = "parentProjectOrganizationEntity")
    private Set<ProjectOrganizationEntity> subProjectEntities;

    private boolean hidden;

    private boolean deleted;

    private boolean verified;

    public Long getProjectOrganizationId() {
        return projectOrganizationId;
    }

    public void setProjectOrganizationId(Long projectOrganizationId) {
        this.projectOrganizationId = projectOrganizationId;
    }

    public ProjectEntity getProjectEntity() {
        return projectEntity;
    }

    public void setProjectEntity(ProjectEntity projectEntity) {
        this.projectEntity = projectEntity;
    }

    public OrganizationEntity getOrganizationEntity() {
        return organizationEntity;
    }

    public void setOrganizationEntity(OrganizationEntity organizationEntity) {
        this.organizationEntity = organizationEntity;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public ProjectOrganizationEntity getParentProjectOrganizationEntity() {
        return parentProjectOrganizationEntity;
    }

    public void setParentProjectOrganizationEntity(ProjectOrganizationEntity parentProjectOrganizationEntity) {
        this.parentProjectOrganizationEntity = parentProjectOrganizationEntity;
    }

    public Set<ProjectOrganizationEntity> getSubProjectEntities() {
        return subProjectEntities;
    }

    public void setSubProjectEntities(Set<ProjectOrganizationEntity> subProjectEntities) {
        this.subProjectEntities = subProjectEntities;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public ProjectOrganizationRole getProjectOrganizationRole() {
        return projectOrganizationRole;
    }

    public void setProjectOrganizationRole(ProjectOrganizationRole projectOrganizationRole) {
        this.projectOrganizationRole = projectOrganizationRole;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
}
