package ai.unico.platform.store.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class FileAccessEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long fileAccessId;

    private Long accessUserId;

    private Date accessDate;

    @ManyToOne
    @JoinColumn(name = "fileId")
    private FileEntity fileEntity;

    public Long getFileAccessId() {
        return fileAccessId;
    }

    public void setFileAccessId(Long fileAccessId) {
        this.fileAccessId = fileAccessId;
    }

    public Long getAccessUserId() {
        return accessUserId;
    }

    public void setAccessUserId(Long accessUserId) {
        this.accessUserId = accessUserId;
    }

    public Date getAccessDate() {
        return accessDate;
    }

    public void setAccessDate(Date accessDate) {
        this.accessDate = accessDate;
    }

    public FileEntity getFileEntity() {
        return fileEntity;
    }

    public void setFileEntity(FileEntity fileEntity) {
        this.fileEntity = fileEntity;
    }
}
