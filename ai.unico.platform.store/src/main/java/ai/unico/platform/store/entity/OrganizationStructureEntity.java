package ai.unico.platform.store.entity;

import ai.unico.platform.store.api.enums.OrganizationRelationType;
import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class OrganizationStructureEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long organizationStructureId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "upperOrganizationId")
    private OrganizationEntity upperOrganizationEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lowerOrganizationId")
    private OrganizationEntity lowerOrganizationEntity;

    @Enumerated(EnumType.STRING)
    private OrganizationRelationType relationType;

    private boolean deleted;

    public Long getOrganizationStructureId() {
        return organizationStructureId;
    }

    public void setOrganizationStructureId(Long organizationStructureId) {
        this.organizationStructureId = organizationStructureId;
    }

    public OrganizationEntity getUpperOrganizationEntity() {
        return upperOrganizationEntity;
    }

    public void setUpperOrganizationEntity(OrganizationEntity upperOrganizationEntity) {
        this.upperOrganizationEntity = upperOrganizationEntity;
    }

    public OrganizationEntity getLowerOrganizationEntity() {
        return lowerOrganizationEntity;
    }

    public void setLowerOrganizationEntity(OrganizationEntity lowerOrganizationEntity) {
        this.lowerOrganizationEntity = lowerOrganizationEntity;
    }

    public OrganizationRelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(OrganizationRelationType relationType) {
        this.relationType = relationType;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
