package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.api.enums.WidgetTab;
import ai.unico.platform.store.dao.InteractionWidgetDao;
import ai.unico.platform.store.entity.InteractionWidgetEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InteractionWidgetDaoImpl implements InteractionWidgetDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void save(InteractionWidgetEntity interactionWidgetEntity) {
        entityManager.persist(interactionWidgetEntity);
    }

    @Override
    public List<InteractionWidgetEntity> findOrganizationInteractions(Long organizationId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(InteractionWidgetEntity.class, "interaction_widget");
        criteria.createCriteria("organization").add(Restrictions.eq("organizationId", organizationId));
        criteria.add(Restrictions.isNotNull("interactionDetails"));
        return criteria.list();
    }

    @Override
    public Map<String, Long> findOrganizationInteractionsCount(Long organizationId, WidgetTab widgetTab) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(InteractionWidgetEntity.class, "interaction_widget");
        criteria.createCriteria("organization").add(Restrictions.eq("organizationId", organizationId));
        // takes only entries 60 days old max
        Date from = new Date(new Date().getTime() - 60L * (1000L * 60L * 60L * 24L));
        criteria.add(Restrictions.ge("date", from));
        criteria.add(Restrictions.isNotNull("interactionDetails"));
        criteria.add(Restrictions.eq("interactionType", widgetTab.asWidgetType()));
        criteria.setProjection(Projections.projectionList()
                .add(Projections.groupProperty("interactionDetails"))
                .add(Projections.rowCount()));
        Map<String, Long> results = new HashMap();
        List<Object[]> o = criteria.list();
        for (Object[] obj: o) {
            results.put((String)obj[0], (Long)obj[1]);
        }
        return results;
    }
}
