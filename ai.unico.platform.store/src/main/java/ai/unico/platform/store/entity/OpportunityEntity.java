package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class OpportunityEntity extends Trackable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long opportunityId;

    private Long originalOpportunityId;
    private String opportunityName;
    private String opportunityDescription;
    private Date opportunitySignupDate;
    private String opportunityLocation;
    private String opportunityWage;
    private String opportunityTechReq;
    private String opportunityFormReq;
    private String opportunityOtherReq;
    private String opportunityBenefit;
    private Date opportunityJobStartDate;
    private String opportunityExtLink;
    private String opportunityHomeOffice;

    private Long expertId;

    private boolean deleted = false;

    private boolean hidden = false;

    @OneToMany(mappedBy = "opportunity")
    private Set<OpportunityTagEntity> keywords = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "opportunityTypeId")
    private OpportunityTypeEntity opportunityTypeEntity;

    @OneToMany(mappedBy = "opportunity")
    private Set<OpportunityJobTypeEntity> jobTypes = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "organizationId")
    private OrganizationEntity organization;

    @ManyToOne
    @JoinColumn(name = "userProfileId")
    private UserProfileEntity userProfile;

    public Long getOpportunityId() {
        return opportunityId;
    }

    public void setOpportunityId(Long opportunityId) {
        this.opportunityId = opportunityId;
    }

    public String getOpportunityName() {
        return opportunityName;
    }

    public void setOpportunityName(String opportunityName) {
        this.opportunityName = opportunityName;
    }

    public String getOpportunityDescription() {
        return opportunityDescription;
    }

    public void setOpportunityDescription(String opportunityDescription) {
        this.opportunityDescription = opportunityDescription;
    }


    public String getOpportunityLocation() {
        return opportunityLocation;
    }

    public void setOpportunityLocation(String opportunityLocation) {
        this.opportunityLocation = opportunityLocation;
    }

    public String getOpportunityWage() {
        return opportunityWage;
    }

    public void setOpportunityWage(String opportunityWage) {
        this.opportunityWage = opportunityWage;
    }

    public String getOpportunityTechReq() {
        return opportunityTechReq;
    }

    public void setOpportunityTechReq(String opportunityTechReq) {
        this.opportunityTechReq = opportunityTechReq;
    }

    public String getOpportunityFormReq() {
        return opportunityFormReq;
    }

    public void setOpportunityFormReq(String opportunityFormReq) {
        this.opportunityFormReq = opportunityFormReq;
    }

    public String getOpportunityOtherReq() {
        return opportunityOtherReq;
    }

    public void setOpportunityOtherReq(String opportunityOtherReq) {
        this.opportunityOtherReq = opportunityOtherReq;
    }

    public String getOpportunityBenefit() {
        return opportunityBenefit;
    }

    public void setOpportunityBenefit(String opportunityBenefit) {
        this.opportunityBenefit = opportunityBenefit;
    }

    public Date getOpportunitySignupDate() {
        return opportunitySignupDate;
    }

    public void setOpportunitySignupDate(Date opportunitySignupDate) {
        this.opportunitySignupDate = opportunitySignupDate;
    }

    public Date getOpportunityJobStartDate() {
        return opportunityJobStartDate;
    }

    public void setOpportunityJobStartDate(Date opportunityJobStartDate) {
        this.opportunityJobStartDate = opportunityJobStartDate;
    }

    public String getOpportunityExtLink() {
        return opportunityExtLink;
    }

    public void setOpportunityExtLink(String opportunityExtLink) {
        this.opportunityExtLink = opportunityExtLink;
    }

    public String getOpportunityHomeOffice() {
        return opportunityHomeOffice;
    }

    public void setOpportunityHomeOffice(String opportunityHomeOffice) {
        this.opportunityHomeOffice = opportunityHomeOffice;
    }

    public Set<OpportunityTagEntity> getKeywords() {
        return keywords;
    }

    public void setKeywords(Set<OpportunityTagEntity> keywords) {
        this.keywords = keywords;
    }

    public OpportunityTypeEntity getOpportunityTypeEntity() {
        return opportunityTypeEntity;
    }

    public void setOpportunityTypeEntity(OpportunityTypeEntity opportunityTypeEntity) {
        this.opportunityTypeEntity = opportunityTypeEntity;
    }

    public Set<OpportunityJobTypeEntity> getJobTypes() {
        return jobTypes;
    }

    public void setJobTypes(Set<OpportunityJobTypeEntity> jobTypes) {
        this.jobTypes = jobTypes;
    }

    public OrganizationEntity getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationEntity organization) {
        this.organization = organization;
    }

    public UserProfileEntity getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfileEntity expert) {
        this.userProfile = expert;
    }

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public Long getOriginalOpportunityId() {
        return originalOpportunityId;
    }

    public void setOriginalOpportunityId(Long originalOpportunityId) {
        this.originalOpportunityId = originalOpportunityId;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hide) {
        this.hidden = hide;
    }
}
