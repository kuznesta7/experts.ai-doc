package ai.unico.platform.store.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PlatformParamEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long platformParamId;

    private String paramKey;

    private String paramValue;

    public Long getPlatformParamId() {
        return platformParamId;
    }

    public void setPlatformParamId(Long platformParamId) {
        this.platformParamId = platformParamId;
    }

    public String getParamKey() {
        return paramKey;
    }

    public void setParamKey(String paramKey) {
        this.paramKey = paramKey;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }
}
