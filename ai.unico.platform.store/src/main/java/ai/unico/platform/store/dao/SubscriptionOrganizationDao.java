package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.SubscriptionOrganizationEntity;

public interface SubscriptionOrganizationDao {

    void save(SubscriptionOrganizationEntity subscriptionOrganizationEntity);

    SubscriptionOrganizationEntity find(Long organizationId, Long widgetEmailSubId);

    SubscriptionOrganizationEntity find(Long organizationId, Long widgetEmailSubId, String userToken);

}
