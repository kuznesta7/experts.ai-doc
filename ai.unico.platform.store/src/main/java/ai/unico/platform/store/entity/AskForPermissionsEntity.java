package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class AskForPermissionsEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long askForPermissionsId;

    @ManyToOne
    private OrganizationEntity organizationEntity;

    private String message;

    private String sourceUrl;

    public Long getAskForPermissionsId() {
        return askForPermissionsId;
    }

    public void setAskForPermissionsId(Long askForPermissionsId) {
        this.askForPermissionsId = askForPermissionsId;
    }

    public OrganizationEntity getOrganizationEntity() {
        return organizationEntity;
    }

    public void setOrganizationEntity(OrganizationEntity organizationEntity) {
        this.organizationEntity = organizationEntity;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }
}
