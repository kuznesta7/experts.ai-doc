package ai.unico.platform.store.entity;

import javax.persistence.*;

@Entity
@Table(name = "item_type")
public class ItemTypeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long typeId;

    private String title;

    private String code;

    private String description;

    private Float weight;

    private boolean addable;

    private boolean impacted;

    private boolean utilityModel;

    @ManyToOne
    @JoinColumn(name = "type_group_id")
    private ItemTypeGroupEntity itemTypeGroupEntity;

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ItemTypeGroupEntity getItemTypeGroupEntity() {
        return itemTypeGroupEntity;
    }

    public void setItemTypeGroupEntity(ItemTypeGroupEntity itemTypeGroupEntity) {
        this.itemTypeGroupEntity = itemTypeGroupEntity;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public boolean isAddable() {
        return addable;
    }

    public void setAddable(boolean addable) {
        this.addable = addable;
    }

    public boolean isImpacted() {
        return impacted;
    }

    public void setImpacted(boolean impacted) {
        this.impacted = impacted;
    }

    public boolean isUtilityModel() {
        return utilityModel;
    }

    public void setUtilityModel(boolean utilityModel) {
        this.utilityModel = utilityModel;
    }
}
