package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.SearchSuggestionDto;
import ai.unico.platform.store.api.service.SearchAutocompleteService;
import ai.unico.platform.store.entity.SearchStatisticsEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.util.model.UserContext;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SearchAutocompleteServiceImpl implements SearchAutocompleteService {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    private static SearchSuggestionDto convert(SearchStatisticsEntity statisticsEntity) {
        SearchSuggestionDto suggestionDto = new SearchSuggestionDto();
        suggestionDto.setSearchExpression(statisticsEntity.getValue());
        suggestionDto.setAppearance(statisticsEntity.getUniqueSearches());
        suggestionDto.setTotalAppearance(statisticsEntity.getSearches());
        return suggestionDto;
    }

    @Override
    @Transactional
    public List<SearchSuggestionDto> findRelevantSuggestions(String query, Integer limit, boolean randomize, UserContext userContext) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(SearchStatisticsEntity.class, "search_statistics");
        criteria.add(Restrictions.ilike("value", query, MatchMode.ANYWHERE));
        criteria.add(Restrictions.ne("value", ""));
        criteria.add(Restrictions.eq("hidden", false));
        criteria.addOrder(Order.desc("uniqueSearches"));
        criteria.addOrder(Order.desc("searches"));

        if (limit == null || limit > 20) {
            limit = 20;
        }
        if (randomize) {
            criteria.setMaxResults(limit * 2);
        } else {
            criteria.setMaxResults(limit);
        }
        List<SearchStatisticsEntity> result = criteria.list();
        List<SearchSuggestionDto> list = new ArrayList<>();
        for (SearchStatisticsEntity searchStatisticsEntity :
                result) {
            list.add(convert(searchStatisticsEntity));
        }
        if (randomize) Collections.shuffle(list);
        final List<SearchSuggestionDto> subList = list.subList(0, Math.min(limit, list.size()));
        subList.sort(Comparator.comparing(SearchSuggestionDto::getAppearance).reversed());
        return subList;
    }
}
