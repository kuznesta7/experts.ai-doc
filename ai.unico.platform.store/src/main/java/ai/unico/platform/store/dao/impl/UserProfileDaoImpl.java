package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.api.filter.UserProfilePreviewFilter;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class UserProfileDaoImpl implements UserProfileDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public UserProfileEntity findUserProfile(Long userId) {
        final UserProfileEntity userProfileEntity = entityManager.find(UserProfileEntity.class, userId);
        if (userProfileEntity == null || userProfileEntity.isDeleted()) {
            return null;
        }
        return userProfileEntity;
    }

    @Override
    public List<UserProfileEntity> findClaimedUserProfiles(Set<Long> userIds) {
        if (userIds.isEmpty()) return Collections.emptyList();
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserProfileEntity.class, "userProfile");
        criteria.add(Restrictions.in("userId", userIds));
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.isNotNull("claimedBy"));
        return criteria.list();
    }

    @Override
    public List<UserProfileEntity> findClaimedUserProfiles() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserProfileEntity.class, "userProfile");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.isNotNull("claimedBy"));
        return criteria.list();
    }

    @Override
    public UserProfileEntity createUserProfile(UserProfileEntity userProfileEntity) {
        userProfileEntity.setDeleted(false);
        entityManager.persist(userProfileEntity);
        return userProfileEntity;
    }

    @Override
    public void save(UserProfileEntity userProfileEntity) {
        entityManager.persist(userProfileEntity);
    }

    @Override
    public List<UserProfileEntity> findUserProfiles(Integer limit, Integer offset) {
        final Criteria idCriteria = hibernateCriteriaCreator.createCriteria(UserProfileEntity.class, "userProfile");
        idCriteria.add(Restrictions.eq("deleted", false));
        idCriteria.setMaxResults(limit);
        idCriteria.setFirstResult(offset);
        idCriteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        final List<UserProfileEntity> userProfileEntities = idCriteria.list();
        if (userProfileEntities.isEmpty()) return Collections.emptyList();

        final Set<Long> userIds = userProfileEntities.stream().map(UserProfileEntity::getUserId).collect(Collectors.toSet());
        return getByIds(userIds);
    }

    @Override
    public List<UserProfileEntity> findClaimableVerifiedProfiles() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserProfileEntity.class, "userProfile");
        criteria.add(Restrictions.eq("deleted", true));
        criteria.add(Restrictions.eq("claimable", true));
        return criteria.list();
    }

    @Override
    public UserProfileEntity findUserProfileWithPreloadedClaims(Long userId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserProfileEntity.class, "userProfile");
        criteria.add(Restrictions.eq("userId", userId));
        criteria.createAlias("claimedUserProfiles", "claimedUserProfile", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("claimedUserProfile.userExpertEntities", "claimedUserExpert", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("userExpertEntities", "claimedExpert", JoinType.LEFT_OUTER_JOIN);
        criteria.setFetchMode("claimedUserProfile", FetchMode.JOIN);
        criteria.setFetchMode("claimedUserExpert", FetchMode.JOIN);
        criteria.setFetchMode("claimedExpert", FetchMode.JOIN);
        return (UserProfileEntity) criteria.uniqueResult();
    }

    @Override
    public List<UserProfileEntity> findByIds(Collection<Long> userIds) {
        return getByIds(userIds);
    }

    private Criteria prepareLoadUserProfilesCriteria(UserProfilePreviewFilter filter) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserProfileEntity.class, "userProfile");

        criteria.add(Restrictions.eq("deleted", false));

        if (filter.getQuery() != null && !filter.getQuery().isEmpty()) {
            criteria.add(Restrictions.ilike("name", filter.getQuery(), MatchMode.ANYWHERE));
        }

        if (filter.getTags() != null && !filter.getTags().isEmpty()) {
            criteria.createAlias("userProfile.userProfileTagEntities", "userProfileTagEntity");
            criteria.createAlias("userProfileTagEntity.tagEntity", "tagEntity");
            criteria.add(Restrictions.eq("userProfileTagEntity.outdated", false));
            criteria.add(Restrictions.in("tagEntity.value", filter.getTags()));
        }
        if (filter.getLocations() != null && !filter.getLocations().isEmpty()) {
            criteria.add(Restrictions.or(
                    Restrictions.in("city", filter.getLocations()),
                    Restrictions.in("country", filter.getLocations())
            ));
        }
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria;
    }

    private List<UserProfileEntity> getByIds(Collection<Long> userIds) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserProfileEntity.class, "userProfile");
        criteria.add(Restrictions.in("userProfile.userId", userIds));

        criteria.createAlias("userProfile.userProfileKeywordEntities", "keywords", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("userProfile.userExpertEntities", "userExpertEntities", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("userProfile.userOrganizationEntities", "userOrganizationEntity", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("userOrganizationEntity.organizationEntity", "organizationEntity", JoinType.LEFT_OUTER_JOIN);
        criteria.setFetchMode("keywords", FetchMode.JOIN);
        criteria.setFetchMode("userExpertEntities", FetchMode.JOIN);
        criteria.setFetchMode("userOrganizationEntity", FetchMode.JOIN);
        criteria.setFetchMode("organizationEntity", FetchMode.JOIN);

//        criteria.createAlias("userProfile.userItemEntities", "itemUserEntity", JoinType.LEFT_OUTER_JOIN);
//        criteria.createAlias("itemUserEntity.itemEntity", "itemEntity", JoinType.LEFT_OUTER_JOIN);
//        criteria.setFetchMode("itemUserEntity", FetchMode.JOIN);
//        criteria.setFetchMode("itemEntity", FetchMode.JOIN);

        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }
}
