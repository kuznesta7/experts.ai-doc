package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ItemTypeDao;
import ai.unico.platform.store.entity.ItemTypeEntity;
import ai.unico.platform.store.entity.ItemTypeGroupEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class ItemTypeDaoImpl implements ItemTypeDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public ItemTypeEntity find(Long itemTypeId) {
        return entityManager.find(ItemTypeEntity.class, itemTypeId);
    }

    @Override
    public List<ItemTypeEntity> find() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ItemTypeEntity.class, "itemType");
        return (List<ItemTypeEntity>) criteria.list();
    }

    @Override
    public List<ItemTypeGroupEntity> findGroups() {
        Criteria criteria = hibernateCriteriaCreator.createCriteria(ItemTypeGroupEntity.class, "itemTypeGroup");
        return criteria.list();
    }
}
