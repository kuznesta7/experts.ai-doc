package ai.unico.platform.store.service;

import ai.unico.platform.store.api.exception.RecaptchaValidationException;
import ai.unico.platform.store.api.service.RecaptchaValidationService;
import org.springframework.beans.factory.annotation.Value;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RecaptchaValidationServiceImpl implements RecaptchaValidationService {

    @Value("${app.recaptcha.key.private}")
    private String recaptchaPrivateKey;

    @Override
    public void validateToken(String recaptchaToken) throws IOException {
        if (recaptchaPrivateKey == null || recaptchaPrivateKey.isEmpty()) return;
        final URL url = new URL("https://www.google.com/recaptcha/api/siteverify?secret=" + recaptchaPrivateKey + "&response=" + recaptchaToken);
        final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setFixedLengthStreamingMode(0);
        connection.setDoOutput(true);
        connection.disconnect();
        final BufferedReader inputStream = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        final StringBuilder content = new StringBuilder();
        String inputLine;
        while ((inputLine = inputStream.readLine()) != null) {
            content.append(inputLine);
        }
        if (!content.toString().contains("\"success\": true")) throw new RecaptchaValidationException();

    }
}
