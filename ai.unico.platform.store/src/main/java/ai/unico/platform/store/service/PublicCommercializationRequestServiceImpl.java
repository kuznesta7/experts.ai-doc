package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.PublicCommercializationRequestDto;
import ai.unico.platform.store.api.service.PublicCommercializationRequestService;
import ai.unico.platform.store.api.service.RecaptchaValidationService;
import ai.unico.platform.store.api.service.email.EmailService;
import ai.unico.platform.store.dao.CommercializationProjectDao;
import ai.unico.platform.store.dao.CommercializationReferentDao;
import ai.unico.platform.store.dao.PublicCommercializationRequestDao;
import ai.unico.platform.store.entity.CommercializationProjectEntity;
import ai.unico.platform.store.entity.CommercializationProjectReferentEntity;
import ai.unico.platform.store.entity.PublicCommercializationProjectRequestEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PublicCommercializationRequestServiceImpl implements PublicCommercializationRequestService {

    @Value("${env.host}")
    private String host;

    @Autowired
    private CommercializationReferentDao commercializationReferentDao;

    @Autowired
    private PublicCommercializationRequestDao publicCommercializationRequestDao;

    @Autowired
    private CommercializationProjectDao commercializationProjectDao;

    @Autowired
    private EmailService emailService;

    @Autowired
    private RecaptchaValidationService recaptchaValidationService;

    @Override
    @Transactional(noRollbackFor = Exception.class)
    public void createPublicRequest(Long commercializationId, PublicCommercializationRequestDto requestDto, String recaptchaToken) throws IOException {
        recaptchaValidationService.validateToken(recaptchaToken);
        final CommercializationProjectEntity commercializationProjectEntity = commercializationProjectDao.find(commercializationId);
        if (commercializationProjectEntity == null)
            throw new NonexistentEntityException(CommercializationProjectEntity.class);
        final PublicCommercializationProjectRequestEntity requestEntity = new PublicCommercializationProjectRequestEntity();
        requestEntity.setCommercializationProjectEntity(commercializationProjectEntity);
        requestEntity.setContent(requestDto.getContent());
        requestEntity.setEmail(requestDto.getEmail());
        requestEntity.setFirstName(requestDto.getFirstName());
        requestEntity.setLastName(requestDto.getLastName());
        requestEntity.setOrganizationName(requestDto.getOrganizationName());
        TrackableUtil.fillCreate(requestEntity, null);
        publicCommercializationRequestDao.save(requestEntity);
        final List<CommercializationProjectReferentEntity> referentEntities = commercializationReferentDao.findForCommercializationProject(commercializationId);
        if (referentEntities.isEmpty()) return;

        final List<String> referentEmails = referentEntities.stream()
                .map(CommercializationProjectReferentEntity::getUserProfileEntity)
                .map(UserProfileEntity::getEmail)
                .collect(Collectors.toList());
        final String to = String.join(",", referentEmails);
        final Map<String, String> variables = new HashMap<>();
        variables.put("commercializationName", commercializationProjectEntity.getName());
        variables.put("email", requestDto.getEmail());
        variables.put("content", requestDto.getContent());
        variables.put("firstName", requestDto.getFirstName());
        variables.put("lastName", requestDto.getLastName());
        variables.put("organizationName", requestDto.getOrganizationName());
        variables.put("url", host + "commercialization/" + commercializationId);
        emailService.sendEmailWithTemplate(to, "public-commercialization-project-request", variables);
    }
}
