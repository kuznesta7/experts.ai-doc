package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.SearchHistoryStatisticsDao;
import ai.unico.platform.store.entity.SearchHistResultEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.store.util.CollectionsUtil;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.util.enums.OrderDirection;
import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.stream.Collectors;

public class SearchHistoryStatisticsDaoImpl implements SearchHistoryStatisticsDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public Long countShownProfilesForExperts(Set<Long> userIds, Set<Long> expertIds) {
        final Criteria criteria = prepareStatisticsCriteria(userIds, expertIds);
        criteria.setProjection(Projections.projectionList()
                .add(Projections.countDistinct("resultUserId"), "c1")
                .add(Projections.countDistinct("resultExpertId"), "c2"));
        criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        final Map<String, Long> o = (Map<String, Long>) criteria.uniqueResult();
        return o.get("c1") + o.get("c2");
    }

    @Override
    public Long countSearchedKeywordsForExperts(Set<Long> userIds, Set<Long> expertIds) {
        final Criteria criteria = prepareKeywordsCriteria(userIds, expertIds);
        criteria.setProjection(Projections.countDistinct("search.query"));
        return (Long) criteria.uniqueResult();
    }

    @Override
    public List<ShownProfileStatistics> findMostShownProfilesForExperts(Set<Long> userIds, Set<Long> expertIds, Integer page, Integer limit) {
        final Criteria criteria = prepareStatisticsCriteria(userIds, expertIds);
        criteria.setProjection(Projections.projectionList()
                .add(Projections.groupProperty("resultUserId").as("userId"))
                .add(Projections.groupProperty("resultExpertId").as("expertId"))
                .add(Projections.countDistinct("searchHistResultId").as("shownCount"))
                .add(Projections.avg("position").as("avgPosition"))
                .add(Projections.min("position").as("bestPosition"))
        );
        HibernateUtil.setOrder(criteria, "shownCount", OrderDirection.DESC);
        HibernateUtil.setPagination(criteria, page, limit);
        criteria.setResultTransformer(new AliasToBeanResultTransformer(ShownProfileStatistics.class));
        final List<ShownProfileStatistics> list = criteria.list();
        final Set<Long> userProfilesToLoadIds = list.stream().map(ShownProfileStatistics::getUserId).filter(Objects::nonNull).collect(Collectors.toSet());
        if (userProfilesToLoadIds.size() > 0) {
            final Criteria userProfileCriteria = hibernateCriteriaCreator.createCriteria(UserProfileEntity.class, "userProfile");
            userProfileCriteria.add(Restrictions.in("userId", userProfilesToLoadIds));
            final List<UserProfileEntity> userProfiles = userProfileCriteria.list();
            for (ShownProfileStatistics profile : list) {
                if (profile.getUserId() != null) {
                    userProfiles.stream()
                            .filter(userProfileEntity -> profile.getUserId().equals(userProfileEntity.getUserId()))
                            .findFirst()
                            .ifPresent(userProfileEntity -> profile.setName(userProfileEntity.getName()));
                }
            }
        }
        return list;
    }

    @Override
    public List<SearchedKeyword> findSearchedKeywordsStatisticsForExperts(Set<Long> userIds, Set<Long> expertIds, Integer page, Integer limit) {
        final Criteria criteria = prepareKeywordsCriteria(userIds, expertIds);
        HibernateUtil.setOrder(criteria, "numberOfExpertsShown", OrderDirection.DESC);
        HibernateUtil.setPagination(criteria, page, limit);
        criteria.setResultTransformer(new AliasToBeanResultTransformer(SearchedKeyword.class));
        return criteria.list();
    }

    private Criteria prepareStatisticsCriteria(Set<Long> userIds, Set<Long> expertIds) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(SearchHistResultEntity.class, "searchHistResult");
        final Set<Long> confirmedUserIds = getConfirmedUserIds(userIds);
        final Set<Long> confirmedExpertIds = getConfirmedUserIds(expertIds);
        if (confirmedExpertIds.isEmpty() && confirmedUserIds.isEmpty())
            return criteria.add(Restrictions.sqlRestriction("(1=0)")); //empty result
        final Disjunction disjunction = Restrictions.or();
        if (!confirmedUserIds.isEmpty())
            disjunction.add(Restrictions.in("resultUserId", confirmedUserIds));
        if (!confirmedExpertIds.isEmpty())
            disjunction.add(Restrictions.in("resultExpertId", confirmedExpertIds));
        criteria.add(disjunction);
        return criteria;
    }

    private Criteria prepareKeywordsCriteria(Set<Long> userIds, Set<Long> expertIds) {
        final Criteria criteria = prepareStatisticsCriteria(userIds, expertIds);
        criteria.createAlias("searchHistEntity", "search", JoinType.INNER_JOIN);
        criteria.add(Restrictions.isNotNull("search.query"));
        criteria.add(Restrictions.ne("search.query", ""));
        criteria.setProjection(Projections.projectionList()
                .add(Projections.groupProperty("search.query").as("query"))
                .add(Projections.countDistinct("search.searchHistId").as("searchCount"))
                .add(Projections.countDistinct("searchHistResultId").as("numberOfExpertsShown"))
                .add(Projections.avg("position").as("avgPosition"))
                .add(Projections.min("position").as("bestPosition"))
        );
        return criteria;
    }

    private Set<Long> getConfirmedUserIds(Set<Long> userIds) {
        final Set<Long> confirmedUserIds = new HashSet<>();
        if (userIds.size() > CollectionsUtil.HIBERNATE_LIMIT) {
            final List<List<Long>> lists = CollectionsUtil.toSubCollectionForHibernate(userIds);
            for (List<Long> list : lists) {
                final Criteria criteria = hibernateCriteriaCreator.createCriteria(SearchHistResultEntity.class, "searchHistResult");
                criteria.add(Restrictions.in("resultUserId", list));
                criteria.setProjection(Projections.groupProperty("resultUserId").as("userId"));
                criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
                final List<Map<String, Long>> result = criteria.list();
                final Set<Long> resultUserIds = result.stream().map(x -> x.get("userId")).collect(Collectors.toSet());
                confirmedUserIds.addAll(resultUserIds);
            }
            return confirmedUserIds;
        }
        return userIds;
    }

    private Set<Long> getConfirmedExpertIds(Set<Long> expertIds) {
        final Set<Long> confirmedExpertIds = new HashSet<>();
        if (expertIds.size() > CollectionsUtil.HIBERNATE_LIMIT) {
            final List<List<Long>> lists = CollectionsUtil.toSubCollectionForHibernate(expertIds);
            for (List<Long> list : lists) {
                final Criteria criteria = hibernateCriteriaCreator.createCriteria(SearchHistResultEntity.class, "searchHistResult");
                criteria.add(Restrictions.in("resultExpertId", list));
                criteria.setProjection(Projections.groupProperty("resultExpertId").as("expertId"));
                criteria.setProjection(Projections.groupProperty("resultExpertId").as("expertId"));
                criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
                final List<Map<String, Long>> result = criteria.list();
                final Set<Long> resultExpertIds = result.stream().map(x -> x.get("expertId")).collect(Collectors.toSet());
                confirmedExpertIds.addAll(resultExpertIds);
            }
            return confirmedExpertIds;
        }
        return expertIds;
    }
}
