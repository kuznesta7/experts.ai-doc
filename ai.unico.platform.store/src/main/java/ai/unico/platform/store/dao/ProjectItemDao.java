package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.ProjectItemEntity;

import java.util.List;
import java.util.Set;

public interface ProjectItemDao {

    ProjectItemEntity findProjectItem(Long projectId, Long itemId);

    ProjectItemEntity findProjectDwhItem(Long projectId, Long originalItemId);

    void saveProjectItem(ProjectItemEntity projectItemEntity);

    List<ProjectItemEntity> findAllWithOriginalItemIds(Set<Long> originalItemIds);
}
