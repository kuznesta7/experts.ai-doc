package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.ExpertSearchHistoryDto;
import ai.unico.platform.store.api.enums.ShareCodeType;
import ai.unico.platform.store.api.service.ShareCodeService;
import ai.unico.platform.store.dao.SearchHistoryDao;
import ai.unico.platform.store.dao.ShareCodeDao;
import ai.unico.platform.store.entity.SearchHistEntity;
import ai.unico.platform.store.entity.ShareCodeEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

public class ShareCodeServiceImpl implements ShareCodeService {

    @Autowired
    private ShareCodeDao shareCodeDao;

    @Autowired
    private SearchHistoryDao searchHistoryDao;

    @Override
    @Transactional
    public String generateShareCode(ShareCodeType shareCodeType, String sharedEntityId, UserContext userContext) {
        final String shareCode = UUID.randomUUID().toString();
        final ShareCodeEntity shareCodeEntity = new ShareCodeEntity();
        shareCodeEntity.setCode(shareCode);
        shareCodeEntity.setType(shareCodeType);
        shareCodeEntity.setSharedEntityId(sharedEntityId);
        TrackableUtil.fillCreate(shareCodeEntity, userContext);
        shareCodeDao.save(shareCodeEntity);
        return shareCode;
    }

    @Override
    @Transactional
    public boolean verifySearchCode(String shareCode, ExpertSearchHistoryDto expertSearchHistoryDto) {
        final ShareCodeEntity shareCodeEntity = shareCodeDao.find(shareCode, ShareCodeType.SEARCH);
        if (shareCodeEntity == null) return false;
        final Long searchHistId = Long.parseLong(shareCodeEntity.getSharedEntityId());
        final SearchHistEntity search = searchHistoryDao.findSameSearchAs(searchHistId, expertSearchHistoryDto);
        return search != null;
    }

    @Override
    @Transactional
    public boolean verifyExpertProfileCode(String shareCode, String expertCode) {
        final ShareCodeEntity shareCodeEntity = shareCodeDao.find(shareCode, ShareCodeType.EXPERT_PROFILE);
        if (shareCodeEntity == null) return false;
        return shareCodeEntity.getSharedEntityId().equals(expertCode);
    }
}
