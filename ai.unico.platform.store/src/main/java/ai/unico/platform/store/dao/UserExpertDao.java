package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.UserExpertEntity;

import java.util.List;
import java.util.Set;

public interface UserExpertDao {

    List<UserExpertEntity> findForExperts(Set<Long> expertIds);

    List<UserExpertEntity> findForUser(Long userId);

    void save(UserExpertEntity userExpertEntity);

    List<UserExpertEntity> findAll();
}
