package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.CommercializationProjectDocumentDao;
import ai.unico.platform.store.entity.CommercializationProjectDocumentEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class CommercializationProjectDocumentDaoImpl implements CommercializationProjectDocumentDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void save(CommercializationProjectDocumentEntity commercializationProjectDocumentEntity) {
        entityManager.persist(commercializationProjectDocumentEntity);
    }

    @Override
    public List<CommercializationProjectDocumentEntity> findForCommercializationProject(Long commercializationId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(CommercializationProjectDocumentEntity.class, "commercializationDocument");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("commercializationProjectEntity.commercializationId", commercializationId));
        return criteria.list();
    }

    @Override
    public CommercializationProjectDocumentEntity findDocument(Long commercializationId, Long fileId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(CommercializationProjectDocumentEntity.class, "commercializationDocument");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("commercializationProjectEntity.commercializationId", commercializationId));
        criteria.add(Restrictions.eq("fileEntity.fileId", fileId));
        return (CommercializationProjectDocumentEntity) criteria.uniqueResult();
    }
}
