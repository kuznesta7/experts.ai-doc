package ai.unico.platform.store.dao;

import ai.unico.platform.store.api.enums.CommercializationOrganizationRelation;
import ai.unico.platform.store.entity.CommercializationProjectOrganizationEntity;

public interface CommercializationOrganizationDao {

    void save(CommercializationProjectOrganizationEntity entity);

    CommercializationProjectOrganizationEntity find(Long commercializationId, Long organizationId, CommercializationOrganizationRelation relation);

}
