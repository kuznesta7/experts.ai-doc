package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
@Table(name = "user_message")
public class UserMessageEntity extends Trackable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userMessageId;

    @ManyToOne
    @JoinColumn(name = "user_to_id")
    private UserProfileEntity userToEntity;

    private Long expertToId;

    @ManyToOne
    @JoinColumn(name = "user_from_id")
    private UserProfileEntity userFromEntity;

    @ManyToOne
    @JoinColumn(name = "search_hist_id")
    private SearchHistEntity searchHistEntity;

    @ManyToOne
    @JoinColumn(name = "parent_message")
    private UserMessageEntity parentMessageEntity;

    private String subject;

    private String content;

    private Boolean read;

    private Boolean deleted;

    public Long getUserMessageId() {
        return userMessageId;
    }

    public void setUserMessageId(Long userMessageId) {
        this.userMessageId = userMessageId;
    }

    public UserProfileEntity getUserToEntity() {
        return userToEntity;
    }

    public void setUserToEntity(UserProfileEntity userToEntity) {
        this.userToEntity = userToEntity;
    }

    public UserProfileEntity getUserFromEntity() {
        return userFromEntity;
    }

    public void setUserFromEntity(UserProfileEntity userFromEntity) {
        this.userFromEntity = userFromEntity;
    }

    public UserMessageEntity getParentMessageEntity() {
        return parentMessageEntity;
    }

    public void setParentMessageEntity(UserMessageEntity parentMessageEntity) {
        this.parentMessageEntity = parentMessageEntity;
    }

    public SearchHistEntity getSearchHistEntity() {
        return searchHistEntity;
    }

    public void setSearchHistEntity(SearchHistEntity searchHistEntity) {
        this.searchHistEntity = searchHistEntity;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getExpertToId() {
        return expertToId;
    }

    public void setExpertToId(Long expertToId) {
        this.expertToId = expertToId;
    }
}
