package ai.unico.platform.store.util;

import ai.unico.platform.util.enums.OrderDirection;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;

import java.util.function.Function;

public class HibernateUtil {

    public static void setOrder(Criteria criteria, String columnToSort, OrderDirection direction) {
        if (direction != null) {
            switch (direction) {
                case ASC:
                    criteria.addOrder(Order.asc(columnToSort));
                    break;
                case DESC:
                    criteria.addOrder(Order.desc(columnToSort));
                    break;
            }
        } else {
            criteria.addOrder(Order.asc(columnToSort));
        }
    }

    public static void setPagination(Criteria criteria, Integer page, Integer limit) {
        if (limit == null) {
            return;
        }
        if (page == null) {
            page = 1;
        }
        criteria.setMaxResults(limit);
        criteria.setFirstResult(limit * (page - 1));
    }

    public static <T, Id> Id getId(T o, Function<T, Id> function) {
        if (o instanceof HibernateProxy) {
            final LazyInitializer lazyInitializer = ((HibernateProxy) o).getHibernateLazyInitializer();
            if (lazyInitializer.isUninitialized()) {
                return (Id) lazyInitializer.getIdentifier();
            }
        }
        return function.apply(o);
    }

    public static <T, E, Id> Id getId(T o, Function<T, E> function, Function<E, Id> functionId) {
        final E e = function.apply(o);
        if (e == null) return null;
        if (e instanceof HibernateProxy) {
            final LazyInitializer lazyInitializer = ((HibernateProxy) e).getHibernateLazyInitializer();
            if (lazyInitializer.isUninitialized()) {
                return (Id) lazyInitializer.getIdentifier();
            }
        }
        return functionId.apply(e);
    }

    public static <T> Function<T, Long> getId(Function<T, Long> function) {
        return new Function<T, Long>() {
            @Override
            public Long apply(T t) {
                return getId(t, function);
            }
        };
    }

    public static <T, E> Function<T, Long> getId(Function<T, E> function, Function<E, Long> functionId) {
        return new Function<T, Long>() {
            @Override
            public Long apply(T t) {
                return getId(t, function, functionId);
            }
        };
    }
}
