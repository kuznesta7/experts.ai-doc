package ai.unico.platform.store.service;

import ai.unico.platform.search.api.dto.CountryStatisticsDto;
import ai.unico.platform.search.api.service.CountryStatisticsService;
import ai.unico.platform.store.api.dto.CountryDto;
import ai.unico.platform.store.api.dto.ExpertsStatisticsDto;
import ai.unico.platform.store.api.service.CountryService;
import ai.unico.platform.store.api.service.ExpertsStatisticsAdministrationService;
import ai.unico.platform.store.dao.ExpertsStatisticsAdministrationDao;
import ai.unico.platform.util.ServiceRegistryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class ExpertsStatisticsAdministrationServiceImpl implements ExpertsStatisticsAdministrationService {

    @Autowired
    ExpertsStatisticsAdministrationDao expertsStatisticsAdministrationDao;

    @Autowired
    CountryService countryService;

    @Override
    @Transactional
    public List<ExpertsStatisticsDto> getCountryExpertsStatistics() throws IOException {
        CountryStatisticsService countryStatisticsService = ServiceRegistryUtil.getService(CountryStatisticsService.class);
        List<CountryDto> countryDtos = countryService.findAvailableCountries();
        List<ExpertsStatisticsDto> expertsStatisticsDtos = new ArrayList<>();
        Map<String, Long> claimsMap = expertsStatisticsAdministrationDao.getCountriesUserClaims();

        for (CountryDto countryDto :
                countryDtos) {
            CountryStatisticsDto countryStatisticsDto = countryStatisticsService.getCountryStatistics(countryDto.getCountryCode());
            ExpertsStatisticsDto expertsStatisticsDto = convert(countryStatisticsDto, countryDto);
            expertsStatisticsDto.setClaimed(claimsMap.get(countryDto.getCountryCode()));
            expertsStatisticsDtos.add(expertsStatisticsDto);
        }
        return expertsStatisticsDtos;
    }

    @Override
    @Transactional
    public ExpertsStatisticsDto getAllExpertsStatistics() throws IOException {
        CountryStatisticsService countryStatisticsService = ServiceRegistryUtil.getService(CountryStatisticsService.class);
        CountryStatisticsDto allStatisticsDto = countryStatisticsService.getCountryStatistics(null);
        ExpertsStatisticsDto expertsStatisticsDto = convert(allStatisticsDto, null);
        expertsStatisticsDto.setClaimed(expertsStatisticsAdministrationDao.getAllCountriesUserClaims());
        return expertsStatisticsDto;
    }

    private static ExpertsStatisticsDto convert(CountryStatisticsDto countryStatisticsDto, CountryDto countryDto) {
        ExpertsStatisticsDto expertsStatisticsDto = new ExpertsStatisticsDto();
        expertsStatisticsDto.setCountry(countryDto);
        expertsStatisticsDto.setExperts(countryStatisticsDto.getAllExperts());
        expertsStatisticsDto.setActive(countryStatisticsDto.getActiveExperts());
        expertsStatisticsDto.setRoleCount(countryStatisticsDto.getRoleCount());
        expertsStatisticsDto.setRetired(countryStatisticsDto.getRetiredExperts());
        expertsStatisticsDto.setVerified(countryStatisticsDto.getVerifiedExperts());
        return expertsStatisticsDto;
    }
}
