package ai.unico.platform.store.dao;

import ai.unico.platform.store.entity.UserProfileKeywordEntity;

public interface UserKeywordDao {

    void save(UserProfileKeywordEntity userProfileKeywordEntity);

}
