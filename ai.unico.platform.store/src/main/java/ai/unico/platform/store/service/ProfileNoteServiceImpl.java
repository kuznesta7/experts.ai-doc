package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.ProfilePersonalNoteDto;
import ai.unico.platform.store.api.service.ProfileNoteService;
import ai.unico.platform.store.dao.ProfilePersonalNoteDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.ProfilePersonalNoteEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class ProfileNoteServiceImpl implements ProfileNoteService {

    @Autowired
    private ProfilePersonalNoteDao profilePersonalNoteDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Override
    @Transactional
    public ProfilePersonalNoteDto findUserProfileNote(Long userFromId, Long userToId) {
        final ProfilePersonalNoteEntity userNote = profilePersonalNoteDao.findUserNote(userFromId, userToId);
        return convert(userNote);
    }

    @Override
    @Transactional
    public ProfilePersonalNoteDto findExpertProfileNote(Long userFromId, Long expertId) {
        final ProfilePersonalNoteEntity expertNote = profilePersonalNoteDao.findExpertNote(userFromId, expertId);
        return convert(expertNote);
    }

    @Override
    @Transactional
    public void saveProfileNote(Long userFromId, ProfilePersonalNoteDto profilePersonalNoteDto, UserContext userContext) {
        final ProfilePersonalNoteEntity profilePersonalNoteEntity = new ProfilePersonalNoteEntity();
        profilePersonalNoteEntity.setExpertId(profilePersonalNoteDto.getExpertId());
        if (profilePersonalNoteDto.getUserId() != null) {
            final ProfilePersonalNoteEntity userNote = profilePersonalNoteDao.findUserNote(userFromId, profilePersonalNoteDto.getUserId());
            if (userNote != null) {
                userNote.setDeleted(true);
                TrackableUtil.fillUpdate(userNote, userContext);
            }
        }
        if (profilePersonalNoteDto.getExpertId() != null) {
            final ProfilePersonalNoteEntity userNote = profilePersonalNoteDao.findExpertNote(userFromId, profilePersonalNoteDto.getExpertId());
            if (userNote != null) {
                userNote.setDeleted(true);
                TrackableUtil.fillUpdate(userNote, userContext);
            }
        }

        final UserProfileEntity userFromEntity = userProfileDao.findUserProfile(userFromId);
        profilePersonalNoteEntity.setFromUserEntity(userFromEntity);
        profilePersonalNoteEntity.setNote(profilePersonalNoteDto.getNote());
        final Long userId = profilePersonalNoteDto.getUserId();
        if (userId != null) {
            final UserProfileEntity userProfile = userProfileDao.findUserProfile(userId);
            if (userProfile == null) throw new NonexistentEntityException(UserProfileEntity.class);
            profilePersonalNoteEntity.setToUserEntity(userProfile);
        } else {
            profilePersonalNoteEntity.setExpertId(profilePersonalNoteDto.getExpertId());
        }
        TrackableUtil.fillCreate(profilePersonalNoteEntity, userContext);
        profilePersonalNoteDao.save(profilePersonalNoteEntity);
    }

    private ProfilePersonalNoteDto convert(ProfilePersonalNoteEntity userNote) {
        if (userNote == null) return null;
        final ProfilePersonalNoteDto profilePersonalNoteDto = new ProfilePersonalNoteDto();
        final UserProfileEntity toUserEntity = userNote.getToUserEntity();
        if (toUserEntity != null) profilePersonalNoteDto.setUserId(toUserEntity.getUserId());
        profilePersonalNoteDto.setExpertId(userNote.getExpertId());
        profilePersonalNoteDto.setNote(userNote.getNote());
        return profilePersonalNoteDto;
    }
}
