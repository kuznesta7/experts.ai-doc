package ai.unico.platform.store.entity;

import ai.unico.platform.store.api.enums.WidgetType;
import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class OrganizationWidgetEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long organizationWidgetId;

    private Boolean allowed;

    @Enumerated(EnumType.STRING)
    private WidgetType widgetType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organization_id")
    private OrganizationEntity organizationEntity;

    public Long getOrganizationWidgetId() {
        return organizationWidgetId;
    }

    public void setOrganizationWidgetId(Long organizationWidgetId) {
        this.organizationWidgetId = organizationWidgetId;
    }

    public Boolean getAllowed() {
        return allowed;
    }

    public void setAllowed(Boolean allowed) {
        this.allowed = allowed;
    }

    public WidgetType getWidgetType() {
        return widgetType;
    }

    public void setWidgetType(WidgetType widgetType) {
        this.widgetType = widgetType;
    }

    public OrganizationEntity getOrganizationEntity() {
        return organizationEntity;
    }

    public void setOrganizationEntity(OrganizationEntity organizationEntity) {
        this.organizationEntity = organizationEntity;
    }
}
