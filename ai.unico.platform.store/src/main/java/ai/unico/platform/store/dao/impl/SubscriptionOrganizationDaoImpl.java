package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.SubscriptionOrganizationDao;
import ai.unico.platform.store.entity.SubscriptionOrganizationEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Arrays;

public class SubscriptionOrganizationDaoImpl implements SubscriptionOrganizationDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void save(ai.unico.platform.store.entity.SubscriptionOrganizationEntity subscriptionOrganizationEntity) {
        entityManager.persist(subscriptionOrganizationEntity);
    }

    @Override
    public SubscriptionOrganizationEntity find(Long organizationId, Long widgetEmailSubId) {
        Criteria criteria = hibernateCriteriaCreator.createCriteria(SubscriptionOrganizationEntity.class, "subscriptionOrganization");
        criteria.add(Restrictions.eq("subscriptionOrganization.organizationEntity.id", organizationId));
        criteria.add(Restrictions.eq("subscriptionOrganization.widgetEmailSubEntity.subId", widgetEmailSubId));
        try {
            return (SubscriptionOrganizationEntity) criteria.uniqueResult();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println(Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    @Override
    public SubscriptionOrganizationEntity find(Long organizationId, Long widgetEmailSubId, String userToken) {
        Criteria criteria = hibernateCriteriaCreator.createCriteria(SubscriptionOrganizationEntity.class, "subscriptionOrganization");
        criteria.add(Restrictions.eq("subscriptionOrganization.organizationEntity.id", organizationId));
        criteria.add(Restrictions.eq("subscriptionOrganization.widgetEmailSubEntity.subId", widgetEmailSubId));
        criteria.add(Restrictions.eq("subscriptionOrganization.userToken", userToken));
        try {
            return (SubscriptionOrganizationEntity) criteria.uniqueResult();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println(Arrays.toString(e.getStackTrace()));
            return null;
        }    }
}
