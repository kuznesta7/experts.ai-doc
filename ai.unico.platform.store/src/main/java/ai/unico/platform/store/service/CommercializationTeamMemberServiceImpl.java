package ai.unico.platform.store.service;

import ai.unico.platform.store.api.service.CommercializationTeamMemberService;
import ai.unico.platform.store.dao.CommercializationProjectDao;
import ai.unico.platform.store.dao.CommercializationTeamMemberDao;
import ai.unico.platform.store.dao.UserProfileDao;
import ai.unico.platform.store.entity.CommercializationProjectEntity;
import ai.unico.platform.store.entity.CommercializationTeamMemberEntity;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.store.util.CommercializationProjectIndexUtil;
import ai.unico.platform.store.util.TrackableUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class CommercializationTeamMemberServiceImpl implements CommercializationTeamMemberService {

    @Autowired
    private CommercializationTeamMemberDao commercializationTeamMemberDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private CommercializationProjectDao commercializationProjectDao;

    @Override
    @Transactional
    public void addTeamMemberUser(Long commercializationProjectId, Long userId, boolean teamLeader, UserContext userContext) {
        final CommercializationTeamMemberEntity existingEntity = commercializationTeamMemberDao.findForUser(userId, commercializationProjectId);
        if (existingEntity != null) {
            existingEntity.setDeleted(true);
            TrackableUtil.fillUpdate(existingEntity, userContext);
        }
        final UserProfileEntity userProfile = userProfileDao.findUserProfile(userId);
        if (userProfile == null) throw new NonexistentEntityException(UserProfileEntity.class);
        final CommercializationProjectEntity commercializationProjectEntity = commercializationProjectDao.find(commercializationProjectId);
        if (commercializationProjectEntity == null)
            throw new NonexistentEntityException(CommercializationProjectEntity.class);

        final CommercializationTeamMemberEntity commercializationTeamMemberEntity = new CommercializationTeamMemberEntity();
        commercializationTeamMemberEntity.setCommercializationProjectEntity(commercializationProjectEntity);
        commercializationTeamMemberEntity.setUserProfileEntity(userProfile);
        commercializationTeamMemberEntity.setTeamLeader(teamLeader);
        TrackableUtil.fillCreate(commercializationTeamMemberEntity, userContext);
        commercializationTeamMemberDao.save(commercializationTeamMemberEntity);

        CommercializationProjectIndexUtil.index(commercializationProjectEntity);
    }

    @Override
    @Transactional
    public void addTeamMemberExpert(Long commercializationProjectId, Long expertId, boolean teamLeader, UserContext userContext) {
        final CommercializationTeamMemberEntity existingEntity = commercializationTeamMemberDao.findForExpert(expertId, commercializationProjectId);
        if (existingEntity != null) {
            existingEntity.setDeleted(true);
            TrackableUtil.fillUpdate(existingEntity, userContext);
        }

        final CommercializationProjectEntity commercializationProjectEntity = commercializationProjectDao.find(commercializationProjectId);
        if (commercializationProjectEntity == null)
            throw new NonexistentEntityException(CommercializationProjectEntity.class);

        final CommercializationTeamMemberEntity commercializationTeamMemberEntity = new CommercializationTeamMemberEntity();
        commercializationTeamMemberEntity.setCommercializationProjectEntity(commercializationProjectEntity);
        commercializationTeamMemberEntity.setExpertId(expertId);
        commercializationTeamMemberEntity.setTeamLeader(teamLeader);
        TrackableUtil.fillCreate(commercializationTeamMemberEntity, userContext);
        commercializationTeamMemberDao.save(commercializationTeamMemberEntity);

        CommercializationProjectIndexUtil.index(commercializationProjectEntity);
    }

    @Override
    @Transactional
    public void removeTeamMemberUser(Long commercializationProjectId, Long userId, UserContext userContext) {
        final CommercializationTeamMemberEntity commercializationTeamMemberEntity = commercializationTeamMemberDao.findForUser(userId, commercializationProjectId);
        if (commercializationTeamMemberEntity == null)
            throw new NonexistentEntityException(CommercializationTeamMemberEntity.class);
        commercializationTeamMemberEntity.setDeleted(true);
        TrackableUtil.fillUpdate(commercializationTeamMemberEntity, userContext);
    }

    @Override
    @Transactional
    public void removeTeamMemberExpert(Long commercializationProjectId, Long expertId, UserContext userContext) {
        final CommercializationTeamMemberEntity commercializationTeamMemberEntity = commercializationTeamMemberDao.findForExpert(expertId, commercializationProjectId);
        if (commercializationTeamMemberEntity == null)
            throw new NonexistentEntityException(CommercializationTeamMemberEntity.class);
        commercializationTeamMemberEntity.setDeleted(true);
        TrackableUtil.fillUpdate(commercializationTeamMemberEntity, userContext);
    }
}
