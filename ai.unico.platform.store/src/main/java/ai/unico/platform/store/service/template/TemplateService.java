package ai.unico.platform.store.service.template;

import java.util.Map;

public interface TemplateService {

    String getTemplate(String id);

    String getReplacedTemplate(String id, Map<String, String> variables);

}
