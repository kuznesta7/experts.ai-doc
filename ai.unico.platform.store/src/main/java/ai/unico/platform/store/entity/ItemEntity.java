package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;
import ai.unico.platform.util.enums.Confidentiality;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "item")
public class ItemEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long itemId;

    private Long origItemId;

    private String itemName;

    private String itemDescription;

    private Integer year;

    @ManyToOne
    @JoinColumn(name = "item_type_id")
    private ItemTypeEntity itemTypeEntity;

    @OneToMany
    @JoinColumn(name = "item_id")
    private Set<ItemTagEntity> itemTagEntities = new HashSet<>();

    @OneToMany
    @JoinColumn(name = "item_id")
    private List<ItemNotRegisteredExpertEntity> notRegisteredExperts = new ArrayList<>();

    @OneToMany
    @JoinColumn(name = "item_id")
    private Set<UserExpertItemEntity> userItemEntities = new HashSet<>();

    @OneToMany
    @JoinColumn(name = "item_id")
    private Set<ItemOrganizationEntity> itemOrganizationEntities = new HashSet<>();

    private Boolean outdated = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_user")
    private UserProfileEntity ownerUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_organization")
    private OrganizationEntity ownerOrganization;

    @Enumerated(EnumType.STRING)
    private Confidentiality confidentiality;

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getOrigItemId() {
        return origItemId;
    }

    public void setOrigItemId(Long origItemId) {
        this.origItemId = origItemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Boolean getOutdated() {
        return outdated;
    }

    public void setOutdated(Boolean outdated) {
        this.outdated = outdated;
    }

    public ItemTypeEntity getItemTypeEntity() {
        return itemTypeEntity;
    }

    public void setItemTypeEntity(ItemTypeEntity itemTypeEntity) {
        this.itemTypeEntity = itemTypeEntity;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Set<ItemTagEntity> getItemTagEntities() {
        return itemTagEntities;
    }

    public void setItemTagEntities(Set<ItemTagEntity> itemTagEntities) {
        this.itemTagEntities = itemTagEntities;
    }

    public List<ItemNotRegisteredExpertEntity> getItemNotRegisteredExperts() {
        return notRegisteredExperts;
    }

    public void setItemNotRegisteredExperts(List<ItemNotRegisteredExpertEntity> notRegisteredExperts) {
        this.notRegisteredExperts = notRegisteredExperts;
    }

    public Set<UserExpertItemEntity> getUserItemEntities() {
        return userItemEntities;
    }

    public void setUserItemEntities(Set<UserExpertItemEntity> userItemEntities) {
        this.userItemEntities = userItemEntities;
    }

    public Set<ItemOrganizationEntity> getItemOrganizationEntities() {
        return itemOrganizationEntities;
    }

    public void setItemOrganizationEntities(Set<ItemOrganizationEntity> itemOrganizationEntities) {
        this.itemOrganizationEntities = itemOrganizationEntities;
    }

    public UserProfileEntity getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(UserProfileEntity ownerUser) {
        this.ownerUser = ownerUser;
    }

    public Confidentiality getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Confidentiality confidentiality) {
        this.confidentiality = confidentiality;
    }

    public OrganizationEntity getOwnerOrganization() {
        return ownerOrganization;
    }

    public void setOwnerOrganization(OrganizationEntity ownerOrganization) {
        this.ownerOrganization = ownerOrganization;
    }
}
