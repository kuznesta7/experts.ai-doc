package ai.unico.platform.store.util;

import ai.unico.platform.search.api.service.ExpertIndexService;
import ai.unico.platform.store.api.dto.UserProfileIndexDto;
import ai.unico.platform.store.entity.UserProfileEntity;
import ai.unico.platform.util.ServiceRegistryUtil;

import java.io.IOException;

public class UserProfileIndexUtil {

    public static void indexUserProfile(UserProfileEntity userProfileEntity) {
        UserProfileIndexDto userProfileIndexDto = UserProfileUtil.convertToIndex(userProfileEntity);
        try {
            final ExpertIndexService expertIndexService = ServiceRegistryUtil.getService(ExpertIndexService.class);
            expertIndexService.updateUser(userProfileIndexDto);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
