package ai.unico.platform.store.entity;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "queryTranslation")
public class QueryTranslationEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long translationId;

    private String translationPhrase;

    private String query;

    private String fullNameQuery;

    private boolean excludeInvestProjects;

    private boolean excludeOutcomes;

    private Integer yearFrom;

    private Integer yearTo;

    private boolean deleted;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "organization_query_translation", joinColumns = @JoinColumn(name = "translation_id"))
    @Column(name = "organization_id")
    private Set<Long> organizationIds;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "item_type_group_query_translation", joinColumns = @JoinColumn(name = "translation_id"))
    @Column(name = "type_group_id")
    private Set<Long> typeGroupIds;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "item_type_query_translation", joinColumns = @JoinColumn(name = "translation_id"))
    @Column(name = "type_id")
    private Set<Long> typeIds;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "country_query_translation", joinColumns = @JoinColumn(name = "translation_id"))
    @Column(name = "country_code")
    private Set<String> countryCodes;

    public Long getTranslationId() {
        return translationId;
    }

    public void setTranslationId(Long translationId) {
        this.translationId = translationId;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getFullNameQuery() {
        return fullNameQuery;
    }

    public void setFullNameQuery(String fullNameQuery) {
        this.fullNameQuery = fullNameQuery;
    }

    public String getTranslationPhrase() {
        return translationPhrase;
    }

    public void setTranslationPhrase(String translationPhrase) {
        this.translationPhrase = translationPhrase;
    }

    public boolean isExcludeInvestProjects() {
        return excludeInvestProjects;
    }

    public void setExcludeInvestProjects(boolean excludeInvestProjects) {
        this.excludeInvestProjects = excludeInvestProjects;
    }

    public boolean isExcludeOutcomes() {
        return excludeOutcomes;
    }

    public void setExcludeOutcomes(boolean excludeOutcomes) {
        this.excludeOutcomes = excludeOutcomes;
    }

    public Integer getYearFrom() {
        return yearFrom;
    }

    public void setYearFrom(Integer yearFrom) {
        this.yearFrom = yearFrom;
    }

    public Integer getYearTo() {
        return yearTo;
    }

    public void setYearTo(Integer yearTo) {
        this.yearTo = yearTo;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<Long> getTypeGroupIds() {
        return typeGroupIds;
    }

    public void setTypeGroupIds(Set<Long> typeGroupIds) {
        this.typeGroupIds = typeGroupIds;
    }

    public Set<Long> getTypeIds() {
        return typeIds;
    }

    public void setTypeIds(Set<Long> typeIds) {
        this.typeIds = typeIds;
    }

    public Set<String> getCountryCodes() {
        return countryCodes;
    }

    public void setCountryCodes(Set<String> countryCodes) {
        this.countryCodes = countryCodes;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
