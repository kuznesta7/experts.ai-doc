package ai.unico.platform.store.entity;


import javax.persistence.*;

@Entity
public class OpportunityJobTypeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long opportunityJobTypeId;

    @ManyToOne
    @JoinColumn(name = "opportunityId")
    private OpportunityEntity opportunity;

    @ManyToOne
    @JoinColumn(name = "jobTypeId")
    private JobTypeEntity jobType;

    private Boolean deleted = false;

    public Long getOpportunityJobTypeId() {
        return opportunityJobTypeId;
    }

    public void setOpportunityJobTypeId(Long opportunityJobTypeId) {
        this.opportunityJobTypeId = opportunityJobTypeId;
    }

    public OpportunityEntity getOpportunity() {
        return opportunity;
    }

    public void setOpportunity(OpportunityEntity opportunity) {
        this.opportunity = opportunity;
    }

    public JobTypeEntity getJobType() {
        return jobType;
    }

    public void setJobType(JobTypeEntity jobType) {
        this.jobType = jobType;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
