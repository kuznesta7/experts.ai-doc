package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ProfileVisitationDao;
import ai.unico.platform.store.entity.ProfileVisitationEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.store.util.HibernateUtil;
import ai.unico.platform.util.enums.OrderDirection;
import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class UserProfileVisitationDaoImpl implements ProfileVisitationDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void saveVisitation(ProfileVisitationEntity visitationEntity) {
        entityManager.persist(visitationEntity);
    }

    @Override
    public Long findNumberOfVisitations() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProfileVisitationEntity.class, "profleVisitation");
        criteria.setProjection(Projections.countDistinct("visitationId"));
        return (Long) criteria.uniqueResult();
    }

    @Override
    public List<ProfileVisitationEntity> findExpertProfileVisitations(Long expertId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProfileVisitationEntity.class, "profileVisitation");
        criteria.add(Restrictions.eq("expertId", expertId));
        return criteria.list();
    }

    @Override
    public List<ProfileVisitationEntity> findUserProfileVisitations(Long userId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProfileVisitationEntity.class, "profileVisitation");
        criteria.add(Restrictions.eq("userProfileEntity.userId", userId));
        return criteria.list();
    }

    @Override
    public Long countProfileVisitationForExperts(Set<Long> userIds, Set<Long> expertIds) {
        final Criteria criteria = prepareCriteriaForVisitationStats(userIds, expertIds);
        criteria.setProjection(Projections.projectionList()
                .add(Projections.countDistinct("userProfileEntity.userId"), "c1")
                .add(Projections.countDistinct("expertId"), "c2"));
        criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        final Map<String, Long> o = (Map<String, Long>) criteria.uniqueResult();
        return o.get("c1") + o.get("c2");
    }

    @Override
    public List<ProfileVisitationStatistics> findMostVisitedProfilesForExperts(Set<Long> userIds, Set<Long> expertIds, Integer page, Integer limit) {
        final Criteria criteria = prepareCriteriaForVisitationStats(userIds, expertIds);
        criteria.createAlias("userProfileEntity", "userProfile", JoinType.LEFT_OUTER_JOIN);
        criteria.setProjection(Projections.projectionList()
                .add(Projections.groupProperty("userProfile.userId").as("userId"))
                .add(Projections.groupProperty("userProfile.name").as("name"))
                .add(Projections.groupProperty("expertId").as("expertId"))
                .add(Projections.countDistinct("visitationId").as("visitationsCount"))
        );
        HibernateUtil.setOrder(criteria, "visitationsCount", OrderDirection.DESC);
        HibernateUtil.setPagination(criteria, page, limit);
        criteria.setResultTransformer(new AliasToBeanResultTransformer(ProfileVisitationStatistics.class));
        return criteria.list();
    }

    private Criteria prepareCriteriaForVisitationStats(Set<Long> userIds, Set<Long> expertIds) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProfileVisitationEntity.class, "profileVisitation");
        final Disjunction disjunction = Restrictions.or();
        if (!userIds.isEmpty())
            disjunction.add(Restrictions.in("userProfileEntity.userId", userIds));
        if (!expertIds.isEmpty())
            disjunction.add(Restrictions.in("expertId", expertIds));
        criteria.add(disjunction);
        return criteria;
    }

}
