package ai.unico.platform.store.entity;

import ai.unico.platform.store.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class UserPreClaimEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userPreClaimId;

    @ManyToOne
    @JoinColumn(name = "userProfileId")
    private UserProfileEntity userProfileEntity;

    @ManyToOne
    @JoinColumn(name = "userToClaimId")
    private UserProfileEntity userToClaimEntity;

    private Long expertToClaimId;

    private boolean resolved;

    private boolean deleted;

    public Long getUserPreClaimId() {
        return userPreClaimId;
    }

    public void setUserPreClaimId(Long userPreClaimId) {
        this.userPreClaimId = userPreClaimId;
    }

    public UserProfileEntity getUserProfileEntity() {
        return userProfileEntity;
    }

    public void setUserProfileEntity(UserProfileEntity userProfileEntity) {
        this.userProfileEntity = userProfileEntity;
    }

    public UserProfileEntity getUserToClaimEntity() {
        return userToClaimEntity;
    }

    public void setUserToClaimEntity(UserProfileEntity userToClaimEntity) {
        this.userToClaimEntity = userToClaimEntity;
    }

    public Long getExpertToClaimId() {
        return expertToClaimId;
    }

    public void setExpertToClaimId(Long expertToClaimId) {
        this.expertToClaimId = expertToClaimId;
    }

    public boolean isResolved() {
        return resolved;
    }

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
