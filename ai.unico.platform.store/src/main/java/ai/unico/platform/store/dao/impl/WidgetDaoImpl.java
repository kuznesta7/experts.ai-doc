package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.api.enums.WidgetType;
import ai.unico.platform.store.dao.OrganizationDao;
import ai.unico.platform.store.dao.WidgetDao;
import ai.unico.platform.store.entity.OrganizationWidgetEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class WidgetDaoImpl implements WidgetDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public boolean isAllowed(Long organizationId, WidgetType widgetType) {
        Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationWidgetEntity.class, "widget");
        criteria.add(Restrictions.eq("organizationEntity.organizationId", organizationId));
        criteria.add(Restrictions.eq("widgetType", widgetType));
        OrganizationWidgetEntity entity = (OrganizationWidgetEntity) criteria.uniqueResult();
        if (entity != null)
            return entity.getAllowed();
        return false;
    }

    @Override
    public List<OrganizationWidgetEntity> findForOrganization(Long organizationId) {
        Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationWidgetEntity.class, "widget");
        criteria.add(Restrictions.eq("organizationEntity.organizationId", organizationId));
        criteria.add(Restrictions.eq("allowed", true));
        return criteria.list();
    }

    @Override
    public void save(OrganizationWidgetEntity organizationWidgetEntity) {
        entityManager.persist(organizationWidgetEntity);
    }
}
