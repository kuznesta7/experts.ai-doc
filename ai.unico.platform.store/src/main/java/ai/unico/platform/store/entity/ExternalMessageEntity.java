package ai.unico.platform.store.entity;

import ai.unico.platform.store.api.enums.MessageType;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
public class ExternalMessageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long messageId;

    private String senderEmail;
    private String message;
    @ManyToOne
    private OrganizationEntity organizationEntity;
    @ManyToOne
    private UserProfileEntity userProfileEntity;

    private Long expertId;

    private String lectureCode;

    @Enumerated(EnumType.STRING)
    private MessageType messageType;

    private Timestamp timeSend = new Timestamp(new Date().getTime());

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OrganizationEntity getOrganizationEntity() {
        return organizationEntity;
    }

    public void setOrganizationEntity(OrganizationEntity organizationEntity) {
        this.organizationEntity = organizationEntity;
    }

    public UserProfileEntity getUserProfileEntity() {
        return userProfileEntity;
    }

    public void setUserProfileEntity(UserProfileEntity userProfileEntity) {
        this.userProfileEntity = userProfileEntity;
    }

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public Timestamp getTimeSend() {
        return timeSend;
    }

    public void setTimeSend(Timestamp timeSend) {
        this.timeSend = timeSend;
    }

    public String getLectureCode() {
        return lectureCode;
    }

    public void setLectureCode(String lecture) {
        this.lectureCode = lecture;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }
}
