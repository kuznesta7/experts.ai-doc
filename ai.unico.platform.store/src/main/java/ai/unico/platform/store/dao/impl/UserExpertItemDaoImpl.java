package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.api.service.ClaimProfileService;
import ai.unico.platform.store.dao.UserExpertItemDao;
import ai.unico.platform.store.entity.ItemEntity;
import ai.unico.platform.store.entity.ItemNotRegisteredExpertEntity;
import ai.unico.platform.store.entity.UserExpertItemEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class UserExpertItemDaoImpl implements UserExpertItemDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void saveUserItem(UserExpertItemEntity userExpertItemEntity) {
        entityManager.persist(userExpertItemEntity);
    }

    @Override
    public void saveNotRegisteredExpert(ItemNotRegisteredExpertEntity notRegisteredExpert) {
        entityManager.persist(notRegisteredExpert);
    }

    @Override
    public ItemNotRegisteredExpertEntity findNotRegisteredExpert(Long expertId) {
        return entityManager.find(ItemNotRegisteredExpertEntity.class, expertId);
    }

    @Override
    public ItemNotRegisteredExpertEntity findNotRegisteredExpertByItemIdAndName(Long itemId, String expertName) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ItemNotRegisteredExpertEntity> criteriaQuery = builder.createQuery(ItemNotRegisteredExpertEntity.class);
        Root<ItemNotRegisteredExpertEntity> root = criteriaQuery.from(ItemNotRegisteredExpertEntity.class);
        criteriaQuery.select(root).where(builder.equal(root.get("item"), itemId), builder.equal(root.get("expertName"), expertName));
        TypedQuery<ItemNotRegisteredExpertEntity> query = entityManager.createQuery(criteriaQuery);
        return query.getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeNotRegisteredExpert(Long expertId) {
        ItemNotRegisteredExpertEntity notRegisteredExpert = findNotRegisteredExpert(expertId);
        if (notRegisteredExpert != null) entityManager.remove(notRegisteredExpert);
    }

    @Override
    public List<ItemNotRegisteredExpertEntity> findNotRegisteredExperts(Set<Long> expertIds) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ItemNotRegisteredExpertEntity> criteriaQuery = builder.createQuery(ItemNotRegisteredExpertEntity.class);
        Root<ItemNotRegisteredExpertEntity> root = criteriaQuery.from(ItemNotRegisteredExpertEntity.class);
        criteriaQuery.select(root).where(root.get("id").in(expertIds));
        TypedQuery<ItemNotRegisteredExpertEntity> query = entityManager.createQuery(criteriaQuery);
        return query.getResultList();
    }

    @Autowired
    private ClaimProfileService claimProfileService;


    @Override
    public UserExpertItemEntity findForUser(Long userId, Long itemId) {
        final Set<ClaimProfileService.Claim> claimsForUser = claimProfileService.findClaimsForUser(userId);
        final Set<Long> expertIds = claimsForUser.stream().map(ClaimProfileService.Claim::getExpertId).filter(Objects::nonNull).collect(Collectors.toSet());
        final Set<Long> userIds = claimsForUser.stream().map(ClaimProfileService.Claim::getUserId).filter(Objects::nonNull).collect(Collectors.toSet());
        Criteria criteria = hibernateCriteriaCreator.createCriteria(UserExpertItemEntity.class, "userItem");
        criteria.createAlias("userProfileEntity", "user");
        final Disjunction disjunction = Restrictions.disjunction();
        if (!userIds.isEmpty()) disjunction.add(Restrictions.in("user.userId", userIds));
        if (!expertIds.isEmpty()) disjunction.add(Restrictions.in("expertId", expertIds));
        if (!userIds.isEmpty() || !expertIds.isEmpty()) criteria.add(disjunction);
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("itemEntity.itemId", itemId));
        return (UserExpertItemEntity) criteria.uniqueResult();
    }

    @Override
    public UserExpertItemEntity findForExpert(Long expertId, Long itemId) {
        Criteria criteria = hibernateCriteriaCreator.createCriteria(UserExpertItemEntity.class, "userItem");
        criteria.add(Restrictions.eq("expertId", expertId));
        criteria.add(Restrictions.eq("itemEntity.itemId", itemId));
        criteria.add(Restrictions.eq("deleted", false));
        return (UserExpertItemEntity) criteria.uniqueResult();
    }

    @Override
    public List<Long> findUserItems(Long userId) {
        final Set<ClaimProfileService.Claim> claimsForUser = claimProfileService.findClaimsForUser(userId);
        final Set<Long> expertIds = claimsForUser.stream().map(ClaimProfileService.Claim::getExpertId).filter(Objects::nonNull).collect(Collectors.toSet());
        final Set<Long> userIds = claimsForUser.stream().map(ClaimProfileService.Claim::getUserId).filter(Objects::nonNull).collect(Collectors.toSet());
        Criteria criteria = hibernateCriteriaCreator.createCriteria(UserExpertItemEntity.class, "userItem");
        criteria.createAlias("userProfileEntity", "user");
        final Disjunction disjunction = Restrictions.disjunction();
        if (!userIds.isEmpty()) disjunction.add(Restrictions.in("user.userId", userIds));
        if (!expertIds.isEmpty()) disjunction.add(Restrictions.in("expertId", expertIds));
        if (!userIds.isEmpty() || !expertIds.isEmpty()) criteria.add(disjunction);
        criteria.add(Restrictions.eq("deleted", false));
        List<UserExpertItemEntity> list = (List<UserExpertItemEntity>) criteria.list();
        return list.stream().map(UserExpertItemEntity::getItemEntity).map(ItemEntity::getItemId).collect(Collectors.toList());
    }

    @Override
    public List<Long> findExpertItems(Long expertId) {
        Criteria criteria = hibernateCriteriaCreator.createCriteria(UserExpertItemEntity.class, "userItem");
        criteria.add(Restrictions.eq("expertId", expertId));
        criteria.add(Restrictions.eq("deleted", false));
        List<UserExpertItemEntity> list = (List<UserExpertItemEntity>) criteria.list();
        return list.stream().map(UserExpertItemEntity::getItemEntity).map(ItemEntity::getItemId).collect(Collectors.toList());
    }
}
