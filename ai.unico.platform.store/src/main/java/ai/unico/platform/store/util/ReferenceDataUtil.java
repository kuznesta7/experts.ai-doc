package ai.unico.platform.store.util;

import ai.unico.platform.store.entity.superclass.ReferenceData;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ReferenceDataUtil {

    public static boolean validateReferenceData(ReferenceData referenceData) {
        final Date now = new Date();
        return referenceData.getActiveFrom().before(now) &&
                (referenceData.getActiveUntil() == null || (referenceData.getActiveUntil().after(now))) &&
                !referenceData.isDeleted();
    }

    public static <T extends ReferenceData> List<T> filterReferenceData(Collection<T> referenceData) {
        return referenceData.stream()
                .filter(ReferenceDataUtil::validateReferenceData)
                .sorted(Comparator.comparing(ReferenceData::getDisplayOrder, Comparator.nullsLast(Comparator.naturalOrder())))
                .collect(Collectors.toList());
    }

    public static <R extends ReferenceData, T> List<T> filterAndConvertReferenceData(Collection<R> referenceData, Function<R, T> convertFunction) {
        return filterReferenceData(referenceData).stream().map(convertFunction).collect(Collectors.toList());
    }
}
