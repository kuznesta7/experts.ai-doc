package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.ProjectBudgetDao;
import ai.unico.platform.store.entity.ProjectBudgetEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ProjectBudgetDaoImpl implements ProjectBudgetDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void save(ProjectBudgetEntity projectBudgetEntity) {
        entityManager.persist(projectBudgetEntity);
    }

    @Override
    public ProjectBudgetEntity find(Long projectId, Long organizationId, Integer year) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProjectBudgetEntity.class, "projectBudget");
        criteria.add(Restrictions.eq("projectBudget.projectEntity.projectId", projectId));
        criteria.add(Restrictions.eq("projectBudget.organizationEntity.organizationId", projectId));
        criteria.add(Restrictions.eq("projectBudget.year", year));
        criteria.add(Restrictions.eq("projectBudget.deleted", false));
        return (ProjectBudgetEntity) criteria.uniqueResult();
    }
}
