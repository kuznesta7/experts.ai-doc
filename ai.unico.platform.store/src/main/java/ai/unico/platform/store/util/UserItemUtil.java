package ai.unico.platform.store.util;

import ai.unico.platform.store.api.dto.ItemIndexDto;
import ai.unico.platform.store.api.dto.ItemPreviewDto;
import ai.unico.platform.store.entity.*;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class UserItemUtil {

    public static ItemPreviewDto convertToPreview(ItemEntity itemEntity) {
        final ItemPreviewDto itemPreviewDto = new ItemPreviewDto();
        fillItemPreviewDto(itemPreviewDto, itemEntity);
        return itemPreviewDto;
    }

    public static ItemIndexDto convertToIndex(ItemEntity itemEntity) {
        final ItemIndexDto itemIndexDto = new ItemIndexDto();
        final Set<Long> organizationIds = itemEntity.getItemOrganizationEntities().stream()
                .filter(x -> !x.isDeleted())
                .map(ItemOrganizationEntity::getOrganizationEntity)
                .map(OrganizationUtil::findOrganizationIdAfterSubstitution)
                .collect(Collectors.toSet());
        itemIndexDto.setOrganizationIds(organizationIds);

        final Set<Long> verifiedOrganizationIds = itemEntity.getItemOrganizationEntities().stream()
                .filter(x -> !x.isDeleted() && x.isVerified())
                .map(ItemOrganizationEntity::getOrganizationEntity)
                .map(OrganizationUtil::findOrganizationIdAfterSubstitution)
                .collect(Collectors.toSet());
        itemIndexDto.setVerifiedOrganizationIds(verifiedOrganizationIds);

        final Set<Long> activeOrganizationIds = itemEntity.getItemOrganizationEntities().stream()
                .filter(x -> !x.isDeleted() && x.isActive())
                .map(ItemOrganizationEntity::getOrganizationEntity)
                .map(OrganizationUtil::findOrganizationIdAfterSubstitution)
                .collect(Collectors.toSet());
        itemIndexDto.setActiveOrganizationIds(activeOrganizationIds);

        itemIndexDto.setActiveUserIds(itemEntity.getUserItemEntities().stream()
                .filter(x -> !x.isDeleted() && !x.isHidden())
                .map(UserExpertItemEntity::getUserProfileEntity)
                .filter(x -> x != null && x.getClaimedBy() == null)
                .map(UserProfileEntity::getUserId)
                .collect(Collectors.toSet()));

        itemIndexDto.setAllUserIds(itemEntity.getUserItemEntities().stream()
                .filter(x -> !x.isDeleted())
                .map(x -> HibernateUtil.getId(x, UserExpertItemEntity::getUserProfileEntity, UserProfileEntity::getUserId))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet()));

        itemIndexDto.setFavoriteUserIds(itemEntity.getUserItemEntities().stream()
                .filter(x -> !x.isDeleted() && x.isFavorite())
                .map(x -> HibernateUtil.getId(x, UserExpertItemEntity::getUserProfileEntity, UserProfileEntity::getUserId))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet()));

        itemIndexDto.setAllExpertIds(itemEntity.getUserItemEntities().stream()
                .filter(x -> !x.isDeleted())
                .map(UserExpertItemEntity::getExpertId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet()));

        itemIndexDto.setActiveExpertIds(itemEntity.getUserItemEntities().stream()
                .filter(x -> !x.isDeleted())
                .map(UserExpertItemEntity::getExpertId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet()));

        itemIndexDto.setNotRegisteredExpertIds(itemEntity.getItemNotRegisteredExperts().stream()
                .map(ItemNotRegisteredExpertEntity::getId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet()));

        fillItemPreviewDto(itemIndexDto, itemEntity);
        itemIndexDto.setItemDescription(itemEntity.getItemDescription());
        return itemIndexDto;
    }

    private static void fillItemPreviewDto(ItemPreviewDto itemPreviewDto, ItemEntity itemEntity) {
        final Long ownerOrganizationId = HibernateUtil.getId(itemEntity, ItemEntity::getOwnerOrganization, OrganizationEntity::getOrganizationId);
        final Long ownerUserId = HibernateUtil.getId(itemEntity, ItemEntity::getOwnerUser, UserProfileEntity::getUserId);

        itemPreviewDto.setItemId(itemEntity.getItemId());
        itemPreviewDto.setOriginalItemId(itemEntity.getOrigItemId());
        itemPreviewDto.setItemName(itemEntity.getItemName());
        itemPreviewDto.setItemDescription(itemEntity.getItemDescription());
        itemPreviewDto.setYear(itemEntity.getYear());
        itemPreviewDto.setOwnerUserId(ownerUserId);
        itemPreviewDto.setOwnerOrganizationId(ownerOrganizationId);
        itemPreviewDto.setConfidentiality(itemEntity.getConfidentiality());

        final ItemTypeEntity itemTypeEntity = itemEntity.getItemTypeEntity();
        if (itemTypeEntity != null) {
            itemPreviewDto.setItemTypeId(itemTypeEntity.getTypeId());
        }

        final Set<ItemTagEntity> itemTagEntities = itemEntity.getItemTagEntities();
        for (ItemTagEntity itemTagEntity : itemTagEntities) {
            if (itemTagEntity.getDeleted()) continue;
            final TagEntity tagEntity = itemTagEntity.getTagEntity();
            itemPreviewDto.getKeywords().add(tagEntity.getValue());
        }
    }
}
