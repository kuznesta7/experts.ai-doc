package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.api.enums.CommercializationOrganizationRelation;
import ai.unico.platform.store.dao.CommercializationOrganizationDao;
import ai.unico.platform.store.entity.CommercializationProjectOrganizationEntity;
import ai.unico.platform.store.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class CommercializationOrganizationDaoImpl implements CommercializationOrganizationDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void save(CommercializationProjectOrganizationEntity entity) {
        entityManager.persist(entity);
    }

    @Override
    public CommercializationProjectOrganizationEntity find(Long commercializationId, Long organizationId, CommercializationOrganizationRelation relation) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(CommercializationProjectOrganizationEntity.class, "commercializationOrganization");
        criteria.add(Restrictions.eq("commercializationProjectEntity.commercializationId", commercializationId));
        criteria.add(Restrictions.eq("organizationEntity.organizationId", organizationId));
        criteria.add(Restrictions.eq("relation", relation));
        criteria.add(Restrictions.eq("deleted", false));
        return (CommercializationProjectOrganizationEntity) criteria.uniqueResult();
    }
}
