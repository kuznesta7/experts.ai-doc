package ai.unico.platform.store.service;

import ai.unico.platform.store.api.dto.ItemTypeDto;
import ai.unico.platform.store.api.dto.ItemTypeGroupDto;
import ai.unico.platform.store.api.dto.ItemTypeSearchDto;
import ai.unico.platform.store.api.service.ItemTypeService;
import ai.unico.platform.store.dao.ItemTypeDao;
import ai.unico.platform.store.entity.ItemTypeEntity;
import ai.unico.platform.store.entity.ItemTypeGroupEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ItemTypeServiceImpl implements ItemTypeService {

    @Autowired
    private ItemTypeDao itemTypeDao;

    private List<ItemTypeSearchDto> itemTypeSearchDtos;

    private List<ItemTypeDto> itemTypeDtos;

    private List<ItemTypeGroupDto> itemTypeGroupDtos;

    @Override
    @Transactional
    public List<ItemTypeDto> findItemTypes() {
        return findItemTypes(true);
    }

    @Override
    @Transactional
    public List<ItemTypeDto> findItemTypes(boolean all) {
        if (this.itemTypeDtos != null) {
            if (all)
                return this.itemTypeDtos;
            else
                return this.itemTypeDtos.stream().filter(ItemTypeDto::isAddable).collect(Collectors.toList());
        }

        final List<ItemTypeDto> itemTypeDtos = new ArrayList<>();
        final List<ItemTypeEntity> itemTypeEntities = itemTypeDao.find();
        for (ItemTypeEntity itemTypeEntity : itemTypeEntities) {
            final ItemTypeDto itemTypeDto = new ItemTypeDto();
            itemTypeDto.setTypeId(itemTypeEntity.getTypeId());
            itemTypeDto.setTypeTitle(itemTypeEntity.getTitle());
            itemTypeDto.setTypeCode(itemTypeEntity.getCode());
            itemTypeDto.setAddable(itemTypeEntity.isAddable());
            itemTypeDto.setImpacted(itemTypeEntity.isImpacted());
            itemTypeDto.setUtilityModel(itemTypeEntity.isUtilityModel());
            itemTypeDtos.add(itemTypeDto);
        }
        this.itemTypeDtos = itemTypeDtos;
        return findItemTypes(all);
    }

    @Override
    @Transactional
    public List<ItemTypeGroupDto> findItemTypeGroups() {
        if (this.itemTypeGroupDtos != null) return this.itemTypeGroupDtos;

        List<ItemTypeGroupDto> itemTypeGroupDtos = new ArrayList<>();
        List<ItemTypeGroupEntity> groups = itemTypeDao.findGroups();
        for (ItemTypeGroupEntity group : groups) {
            ItemTypeGroupDto itemTypeGroupDto = new ItemTypeGroupDto();
            itemTypeGroupDto.setItemTypeGroupId(group.getTypeGroupId());
            itemTypeGroupDto.setItemTypeGroupTitle(group.getTitle());
            for (ItemTypeEntity itemTypeEntity : group.getItemTypeEntities()) {
                itemTypeGroupDto.getItemTypeIds().add(itemTypeEntity.getTypeId());
            }
            itemTypeGroupDtos.add(itemTypeGroupDto);
        }
        this.itemTypeGroupDtos = itemTypeGroupDtos;
        return itemTypeGroupDtos;
    }

    @Override
    @Transactional
    public List<ItemTypeSearchDto> findItemTypesSearch() {
        if (this.itemTypeSearchDtos != null) return this.itemTypeSearchDtos;
        final List<ItemTypeEntity> itemTypeEntities = itemTypeDao.find();
        final List<ItemTypeSearchDto> itemTypeSearchDtos = new ArrayList<>();

        for (ItemTypeEntity itemTypeEntity : itemTypeEntities) {
            final ItemTypeSearchDto itemTypeSearchDto = new ItemTypeSearchDto();
            itemTypeSearchDto.setTypeId(itemTypeEntity.getTypeId());
            itemTypeSearchDto.setTypeCode(itemTypeEntity.getCode());
            itemTypeSearchDto.setTypeTitle(itemTypeEntity.getTitle());
            itemTypeSearchDto.setWeight(itemTypeEntity.getWeight());
            final ItemTypeGroupEntity itemTypeGroupEntity = itemTypeEntity.getItemTypeGroupEntity();
            if (itemTypeGroupEntity != null) {
                itemTypeSearchDto.setTypeGroupId(itemTypeGroupEntity.getTypeGroupId());
            }
            itemTypeSearchDtos.add(itemTypeSearchDto);
        }
        this.itemTypeSearchDtos = itemTypeSearchDtos;
        return itemTypeSearchDtos;
    }

    @Override
    @Transactional
    public Set<Long> findItemTypeIds(Set<Long> groupIds) {
        Set<Long> itemTypeIds = new HashSet<>();
        findItemTypeGroups().stream()
                .filter(group -> groupIds.contains(group.getItemTypeGroupId()))
                .map(ItemTypeGroupDto::getItemTypeIds)
                .forEach(itemTypeIds::addAll);
        return itemTypeIds;
    }
}
