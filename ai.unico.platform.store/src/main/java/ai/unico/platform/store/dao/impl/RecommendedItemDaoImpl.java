package ai.unico.platform.store.dao.impl;

import ai.unico.platform.store.dao.RecommendedItemDao;
import ai.unico.platform.store.entity.RecommendedItemEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Arrays;

public class RecommendedItemDaoImpl implements RecommendedItemDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(RecommendedItemEntity item) {
        entityManager.persist(item);
    }

    @Override
    public RecommendedItemEntity find(String id) {
        return entityManager.find(RecommendedItemEntity.class, id);
    }

    @Override
    public String[] getRecommendations(String id) {
        RecommendedItemEntity item = find(id);
        String recomms = item.getRecommendation();
        recomms = recomms.replaceAll("\\s+","");
        return recomms.split(",");
    }

    @Override
    public void extendRecommendation(String id, String[] recomms) {
        RecommendedItemEntity item = find(id);
        String[] oldRecomms = getRecommendations(id);
        String[] mergedRecomms = Arrays.copyOf(oldRecomms, recomms.length + oldRecomms.length);
        System.arraycopy(recomms, 0, mergedRecomms, oldRecomms.length, recomms.length);
        item.setRecommendation(Arrays.stream(mergedRecomms).reduce("", (partial, element) -> partial + "," + element).substring(1));
        item.setNumOfRecomms(mergedRecomms.length);
        save(item);
    }
}
