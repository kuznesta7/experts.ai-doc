package ai.unico.platform.search.service;

import ai.unico.platform.search.api.dto.AdminOrganizationDto;
import ai.unico.platform.search.api.dto.OrganizationAutocompleteDto;
import ai.unico.platform.search.api.dto.SearchResultBucketDto;
import ai.unico.platform.search.api.filter.*;
import ai.unico.platform.search.api.service.*;
import ai.unico.platform.search.api.wrapper.AdminOrganizationWrapper;
import ai.unico.platform.search.api.wrapper.EvidenceItemWrapper;
import ai.unico.platform.search.api.wrapper.OrganizationWrapper;
import ai.unico.platform.search.api.wrapper.ProjectWrapper;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Organization;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.dto.OrganizationLicencePreviewDto;
import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.dto.OrganizationDto;
import ai.unico.platform.util.enums.ExpertSearchType;
import ai.unico.platform.util.enums.OrganizationRole;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.common.lucene.search.function.FunctionScoreQuery;
import org.elasticsearch.index.query.*;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScriptScoreFunctionBuilder;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class OrganizationSearchServiceImpl implements OrganizationSearchService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private ItemSearchService itemSearchService;

    @Autowired
    private OntologySearchService ontologySearchService;

    @Autowired
    private CommercializationProjectSearchService commercializationProjectSearchService;

    @Autowired
    private EvidenceProjectService evidenceProjectService;

    @Autowired
    private EvidenceItemService evidenceItemService;

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;

    @Override
    public OrganizationWrapper findOrganizations(OrganizationSearchFilter organizationSearchFilter) throws IOException {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();

        if (organizationSearchFilter.getQuery() != null && !organizationSearchFilter.getQuery().equals("")) {
            final BoolQueryBuilder queryBuilder = Objects.equals(organizationSearchFilter.getExpertSearchType(), ExpertSearchType.EXPERTISE) ? new BoolQueryBuilder() : prepareBoolQuery(organizationSearchFilter.getQueryForSearch(), organizationSearchFilter.isExactMatch());

            if (organizationSearchFilter.getExpertSearchType() == null || organizationSearchFilter.getExpertSearchType().equals(ExpertSearchType.EXPERTISE)) {
                addSearchByExpertiseCriteria(organizationSearchFilter.getQueryForSearch(), queryBuilder);
                searchSourceBuilder.minScore(1.1F);
            }
            queryBuilder.minimumShouldMatch(1);
            boolQueryBuilder.must(queryBuilder);
        }
        if (!organizationSearchFilter.getCountryCodes().isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("countryCodes", organizationSearchFilter.getCountryCodes()));
        }
        if (!organizationSearchFilter.getOrganizationIds().isEmpty()) {
            boolQueryBuilder.must(new TermsQueryBuilder("id", organizationSearchFilter.getOrganizationIds()));
        }
        if (!organizationSearchFilter.isUseSubstituteOrganization()) {
            boolQueryBuilder.mustNot(new ExistsQueryBuilder("substituteOrganizationId"));
        }
        if (organizationSearchFilter.getOrganizationTypesIds().size() > 0) {
            boolQueryBuilder.filter(new TermsQueryBuilder("organizationTypeId", organizationSearchFilter.getOrganizationTypesIds()));
        }

        searchSourceBuilder.query(boolQueryBuilder)
                .size(organizationSearchFilter.getLimit())
                .from(organizationSearchFilter.getLimit() * (organizationSearchFilter.getPage() - 1));
        final SearchResponseEntityWrapper<Organization> hits = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Organization.class);
        OrganizationWrapper organizationWrapper = new OrganizationWrapper();

        organizationWrapper.setOrganizationBaseDtos(hits.getSearchResponseEntities().stream().map(SearchResponseEntity::getSource).map(this::convertToBase).collect(Collectors.toList()));
        organizationWrapper.setLimit(organizationSearchFilter.getLimit());
        organizationWrapper.setPage(organizationSearchFilter.getPage());
        organizationWrapper.setNumberOfAllItems(hits.getResultCount());
        return organizationWrapper;
    }

    @Override
    public List<OrganizationAutocompleteDto> autocompleteOrganizations(OrganizationAutocompleteFilter filter) throws IOException {
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder boolQueryBuilder = prepareBoolQuery(filter.getQuery(), false);
        boolQueryBuilder.mustNot(new ExistsQueryBuilder("substituteOrganizationId"));
        final ScriptScoreFunctionBuilder scoreFunctionBuilder = new ScriptScoreFunctionBuilder(new Script("_score * Math.log(doc['rank'].value + 10)"));
        final FunctionScoreQueryBuilder scoreQueryBuilder = new FunctionScoreQueryBuilder(boolQueryBuilder, scoreFunctionBuilder).scoreMode(FunctionScoreQuery.ScoreMode.MULTIPLY);
        if (filter.getEnableOrganizationIds().size() > 0){
            boolQueryBuilder.must(new TermsQueryBuilder("organizationId", filter.getEnableOrganizationIds()));
        }
        if (filter.getCountryCodes() != null && !filter.getCountryCodes().isEmpty()) {
            boolQueryBuilder.filter(new BoolQueryBuilder()
                    .minimumShouldMatch(0)
                    .should(new TermsQueryBuilder("countryCode", filter.getCountryCodes()))
                    .should(new BoolQueryBuilder()
                            .mustNot(new ExistsQueryBuilder("countryCode"))));
        }
        if (filter.getDisabledOrganizationIds() != null && !filter.getDisabledOrganizationIds().isEmpty()) {
            boolQueryBuilder.mustNot(new TermsQueryBuilder("organizationId", filter.getDisabledOrganizationIds()));
        }
        sourceBuilder.query(new BoolQueryBuilder()
                .must(scoreQueryBuilder));
        final SearchResponseEntityWrapper<Organization> wrapper = elasticSearchDao.doSearchAndGetHits(OrganizationIndexService.indexName, sourceBuilder, Organization.class);
        return wrapper.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource)
                .map(this::convertToAutocomplete)
                .collect(Collectors.toList());
    }

    @Override
    public List<OrganizationAutocompleteDto> autocompleteOrganizationsForResearchProjects(OrganizationAutocompleteFilter filter, Long organizationId) throws IOException {
        ProjectFilter projectFilter = new ProjectFilter();
        projectFilter.setOrganizationId(organizationId);
        projectFilter.setLimit(1000);
        projectFilter.setOrganizationIds(new HashSet<>(Collections.singletonList(organizationId)));
        ProjectWrapper projectWrapper = evidenceProjectService.findProjects(projectFilter);

        Set<Long> ids = projectWrapper.getProjectPreviewDtos()
                .stream().flatMap(e->e.getOrganizationBaseDtos().stream())
                .map(OrganizationBaseDto::getOrganizationId)
                .collect(Collectors.toSet());
        filter.setEnableOrganizationIds(ids);
        return autocompleteOrganizations(filter);
    }

    @Override
    public List<OrganizationAutocompleteDto> autocompleteOrganizationsForResearchOutcomes(OrganizationAutocompleteFilter filter, Long organizationId) throws IOException{
        EvidenceFilter evidenceFilter = new EvidenceFilter();
        evidenceFilter.setOrganizationId(organizationId);
        evidenceFilter.setLimit(10000);
        evidenceFilter.setOrganizationIds(new HashSet<>(Collections.singletonList(organizationId)));
        EvidenceItemWrapper evidenceItemWrapper = evidenceItemService.findAnalyticsItems(evidenceFilter);

        Set<Long> ids = evidenceItemWrapper.getItemPreviewDtos()
                .stream().flatMap(e->e.getOrganizationBaseDtos().stream())
                .map(OrganizationBaseDto::getOrganizationId)
                .collect(Collectors.toSet());
        filter.setEnableOrganizationIds(ids);
        return autocompleteOrganizations(filter);
    }



    @Override
    public List<OrganizationAutocompleteDto> findAutocompleteOrganizations(Set<Long> organizationIds) throws IOException {
        final Set<String> organizationIdsString = organizationIds.stream().map(Object::toString).collect(Collectors.toSet());
        final List<Organization> documents = elasticSearchDao.getDocuments(OrganizationIndexService.indexName, organizationIdsString, Organization.class);
        return documents.stream()
                .filter(Objects::nonNull)
                .map(this::convertToAutocomplete)
                .collect(Collectors.toList());
    }

    @Override
    public List<OrganizationBaseDto> findBaseOrganizations(Set<Long> organizationIds) throws IOException {
        final Set<String> organizationIdsString = organizationIds.stream().map(Objects::toString).collect(Collectors.toSet());
        final List<Organization> documents = elasticSearchDao.getDocuments(indexName, organizationIdsString, Organization.class);
        return documents.stream()
                .map(this::convertToBase)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public AdminOrganizationWrapper findOrganizationsForAdmin(AdminOrganizationFilter filter) throws IOException {
        final AdminOrganizationWrapper adminOrganizationWrapper = new AdminOrganizationWrapper();
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final String query = filter.getQuery();
        final BoolQueryBuilder boolQueryBuilder = prepareBoolQuery(query, filter.isExactMatch());
        boolQueryBuilder.filter(new ExistsQueryBuilder("organizationName"));
        if (filter.isActiveLicencesOnly() || filter.isInvalidLicencesOnly()) {
            final BoolQueryBuilder licenceBoolQueryBuilder = new BoolQueryBuilder();
            licenceBoolQueryBuilder.filter(new MatchAllQueryBuilder());
            if (filter.isActiveLicencesOnly()) {
                licenceBoolQueryBuilder.filter(new BoolQueryBuilder()
                        .should(new RangeQueryBuilder("organizationLicences.validUntil").gt("now"))
                        .should(new BoolQueryBuilder()
                                .mustNot(new ExistsQueryBuilder("organizationLicences.validUntil")))
                        .minimumShouldMatch(1));
            }
            if (filter.isInvalidLicencesOnly()) {
                licenceBoolQueryBuilder.filter(new BoolQueryBuilder()
                        .must(new RangeQueryBuilder("organizationLicences.validUntil").lt("now"))
                        .must(new ExistsQueryBuilder("organizationLicences.validUntil")));
            }
            boolQueryBuilder.filter(new NestedQueryBuilder("organizationLicences", licenceBoolQueryBuilder, ScoreMode.Max));
        }
        final AdminOrganizationFilter.ReplacementState replacementState = filter.getReplacementState();
        if (AdminOrganizationFilter.ReplacementState.REPLACED.equals(replacementState)) {
            boolQueryBuilder.filter(new ExistsQueryBuilder("substituteOrganizationId"));
        } else if (AdminOrganizationFilter.ReplacementState.VALID.equals(replacementState)) {
            boolQueryBuilder.mustNot(new ExistsQueryBuilder("substituteOrganizationId"));
        }
        if (filter.isOnlyWithoutCountry()) {
            boolQueryBuilder.mustNot(new ExistsQueryBuilder("countryCode"));
        }
        if (!filter.getCountryCodes().isEmpty()) {
            boolQueryBuilder.must(new TermsQueryBuilder("countryCode", filter.getCountryCodes()));
        }
        searchSourceBuilder.query(boolQueryBuilder)
                .trackScores(true)
                .size(filter.getLimit())
                .from(filter.getLimit() * (filter.getPage() - 1))
                .sort("_score", SortOrder.DESC)
                .sort("rank", SortOrder.DESC);
        final SearchResponseEntityWrapper<Organization> organizationSearchResponseEntityWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Organization.class);
        adminOrganizationWrapper.setLimit(filter.getLimit());
        adminOrganizationWrapper.setPage(filter.getPage());
        adminOrganizationWrapper.setNumberOfAllItems(organizationSearchResponseEntityWrapper.getResultCount());
        final List<SearchResponseEntity<Organization>> searchResponseEntities = organizationSearchResponseEntityWrapper.getSearchResponseEntities();

        final Set<Long> organizationsToLoadIds = new HashSet<>();
        final Map<Long, Organization> shownOrganizationsMap = new HashMap<>();

        final Date now = new Date();
        for (SearchResponseEntity<Organization> searchResponseEntity : searchResponseEntities) {
            final Organization organization = searchResponseEntity.getSource();
            final AdminOrganizationDto adminOrganizationDto = new AdminOrganizationDto();
            final Long substituteOrganizationId = organization.getSubstituteOrganizationId();
            organizationsToLoadIds.addAll(organization.getChildrenOrganizationIds());
            organizationsToLoadIds.addAll(organization.getAffiliatedOrganizationIds());
            if (substituteOrganizationId != null) {
                organizationsToLoadIds.add(substituteOrganizationId);
            }
            shownOrganizationsMap.put(organization.getOrganizationId(), organization);
            adminOrganizationDto.setOrganizationId(organization.getOrganizationId());
            adminOrganizationDto.setOrganizationName(organization.getOrganizationName());
            adminOrganizationDto.setOrganizationAbbrev(organization.getOrganizationAbbrev());
            adminOrganizationDto.setSubstituteOrganizationId(substituteOrganizationId);
            adminOrganizationDto.setCountryCode(organization.getCountryCode());
            adminOrganizationDto.setHasAllowedSearch(organization.getAllowedSearch());
            adminOrganizationDto.setMembersAllowed(organization.getAllowedMembers());
            adminOrganizationDto.setRank(organization.getRank() != null ? Float.valueOf(organization.getRank()) : null);
            adminOrganizationDto.setOrganizationLicencePreviewDtos(organization.getOrganizationLicences().stream()
                    .map(this::convertOrganizationLicence)
                    .collect(Collectors.toList()));
            adminOrganizationDto.setHasValidLicence(adminOrganizationDto.getOrganizationLicencePreviewDtos().stream().anyMatch(l -> l.getValidUntil() == null || l.getValidUntil().after(now)));
            adminOrganizationWrapper.getAdminOrganizationDtos().add(adminOrganizationDto);
        }

        final OrganizationStructureService organizationStructureService = ServiceRegistryUtil.getService(OrganizationStructureService.class);
        final List<OrganizationDto> rootOrganizationDtos = organizationStructureService.findRootOrganizationDtos(shownOrganizationsMap.keySet());
        final Map<Long, List<OrganizationDto>> rootOrganizationMap = new HashMap<>();
        for (OrganizationDto rootDto : rootOrganizationDtos) {
            for (OrganizationDto unitDto : rootDto.getOrganizationUnits()) {
                final List<OrganizationDto> roots = rootOrganizationMap.computeIfAbsent(unitDto.getOrganizationId(), x -> new ArrayList<>());
                roots.add(rootDto);
            }
        }

        final BoolQueryBuilder additiveOrganizationsQueryBuilder = new BoolQueryBuilder();
        additiveOrganizationsQueryBuilder
                .should(new TermsQueryBuilder("organizationId", organizationsToLoadIds))
                .should(new TermsQueryBuilder("substituteOrganizationId", shownOrganizationsMap.keySet()))
                .should(new TermsQueryBuilder("childrenOrganizationIds", shownOrganizationsMap.keySet()))
                .should(new TermsQueryBuilder("affiliatedOrganizationIds", shownOrganizationsMap.keySet()))
                .minimumShouldMatch(1);
        final SearchSourceBuilder additiveOrganizationsSearchBuilder = new SearchSourceBuilder().query(additiveOrganizationsQueryBuilder).size(maxResultWindow);
        final SearchResponseEntityWrapper<Organization> additiveOrganizations = elasticSearchDao.doSearchAndGetHits(indexName, additiveOrganizationsSearchBuilder, Organization.class);
        final List<SearchResponseEntity<Organization>> additiveOrganizationSearchResponse = additiveOrganizations.getSearchResponseEntities();
        final Map<Long, List<Organization>> replacedOrganizationsMap = additiveOrganizationSearchResponse.stream()
                .map(SearchResponseEntity::getSource)
                .filter(x -> x.getSubstituteOrganizationId() != null)
                .collect(Collectors.groupingBy(Organization::getSubstituteOrganizationId));
        final Map<Long, OrganizationBaseDto> organizationMap = additiveOrganizationSearchResponse.stream()
                .map(SearchResponseEntity::getSource)
                .map(this::convertToBase)
                .collect(Collectors.toMap(OrganizationBaseDto::getOrganizationId, Function.identity()));
        final Map<Long, List<OrganizationBaseDto>> upperOrganizationsMap = new HashMap<>();
        for (SearchResponseEntity<Organization> searchResponseEntity : additiveOrganizationSearchResponse) {
            final Organization organization = searchResponseEntity.getSource();
            final OrganizationBaseDto organizationBaseDto = organizationMap.get(organization.getOrganizationId());
            for (Long childrenOrganizationId : organization.getChildrenOrganizationIds()) {
                upperOrganizationsMap.computeIfAbsent(childrenOrganizationId, id -> new ArrayList<>()).add(organizationBaseDto);
            }
        }

        for (AdminOrganizationDto adminOrganizationDto : adminOrganizationWrapper.getAdminOrganizationDtos()) {
            final Long organizationId = adminOrganizationDto.getOrganizationId();
            if (replacedOrganizationsMap.containsKey(organizationId)) {
                final List<Organization> substituteOrganizations = replacedOrganizationsMap.get(organizationId);
                adminOrganizationDto.getReplacedOrganizations().addAll(substituteOrganizations.stream()
                        .map(this::convertToBase)
                        .collect(Collectors.toList()));
            }
            final Long substituteOrganizationId = adminOrganizationDto.getSubstituteOrganizationId();
            if (substituteOrganizationId != null && organizationMap.containsKey(substituteOrganizationId)) {
                final OrganizationBaseDto substituteOrganization = organizationMap.get(substituteOrganizationId);
                adminOrganizationDto.setSubstituteOrganizationName(substituteOrganization.getOrganizationName());
                adminOrganizationDto.setSubstituteOrganizationAbbrev(substituteOrganization.getOrganizationAbbrev());
            }
            if (upperOrganizationsMap.containsKey(organizationId)) {
                final List<OrganizationBaseDto> upperOrganizationDtos = upperOrganizationsMap.get(organizationId);
                adminOrganizationDto.getUpperOrganizations().addAll(upperOrganizationDtos);
            }
            final Organization organization = shownOrganizationsMap.get(organizationId);
            for (Long childrenOrganizationId : organization.getChildrenOrganizationIds()) {
                adminOrganizationDto.getLowerOrganizations().add(organizationMap.get(childrenOrganizationId));
            }
            for (Long affiliatedOrganizationId : organization.getAffiliatedOrganizationIds()) {
                adminOrganizationDto.getAffiliatedOrganizations().add(organizationMap.get(affiliatedOrganizationId));
            }

            if (rootOrganizationMap.containsKey(organizationId)) {
                for (OrganizationDto rootOrganizationDto : rootOrganizationMap.get(organizationId)) {
                    if (adminOrganizationDto.getUpperOrganizations().stream().map(OrganizationBaseDto::getOrganizationId).anyMatch(x -> x.equals(rootOrganizationDto.getOrganizationId())))
                        continue;
                    final OrganizationBaseDto rootBaseDto = new OrganizationBaseDto();
                    rootBaseDto.setOrganizationId(rootOrganizationDto.getOrganizationId());
                    rootBaseDto.setOrganizationName(rootOrganizationDto.getOrganizationName());
                    rootBaseDto.setOrganizationAbbrev(rootOrganizationDto.getOrganizationAbbrev());
                    rootBaseDto.setGdpr(rootOrganizationDto.getGdpr());
                    adminOrganizationDto.getRootOrganizations().add(rootBaseDto);
                }
            }
        }
        return adminOrganizationWrapper;
    }

    @Override
    public Map<Long, OrganizationBaseDto> findBaseOrganizationsMap(Set<Long> organizationToLoadIds) throws IOException {
        final List<OrganizationBaseDto> baseOrganizations = findBaseOrganizations(organizationToLoadIds);
        return baseOrganizations.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(OrganizationBaseDto::getOrganizationId, Function.identity()));
    }

    private Map<Long, SearchResultBucketDto> addSearchByExpertiseCriteria(String expertise, BoolQueryBuilder boolQueryBuilder) throws IOException {
        final Map<Long, SearchResultBucketDto> searchResultBucketMap = new HashMap<>();
        final ExpertSearchFilter expertSearchFilter = new ExpertSearchFilter();
        expertSearchFilter.setQuery(expertise);
        ontologySearchService.fillItemSearchOntology(expertSearchFilter);
        if (!expertSearchFilter.isExcludeOutcomes()) {
            final List<SearchResultBucketDto> expertsForItemSearch = itemSearchService.findOrganizationsForItemSearch(expertSearchFilter);
            final List<SearchResultBucketDto> expertsForItemSearchCut;
            if (expertsForItemSearch.size() > 200) {
                final Optional<SearchResultBucketDto> best = expertsForItemSearch.stream().max(Comparator.comparing(SearchResultBucketDto::getRank));
                expertsForItemSearchCut = expertsForItemSearch.stream().filter(r -> r.getRank() > best.get().getRank() / 50).collect(Collectors.toList());
            } else {
                expertsForItemSearchCut = expertsForItemSearch;
            }
            expertsForItemSearchCut.forEach(x -> searchResultBucketMap.put(x.getOrganizationId(), x));
        }
        if (!expertSearchFilter.isExcludeInvestProjects()) {
            final List<SearchResultBucketDto> expertsForProjectSearchBuckets = commercializationProjectSearchService.findOrganizationsForProjectSearch(expertSearchFilter);
            for (SearchResultBucketDto expertsForProjectSearchBucket : expertsForProjectSearchBuckets) {
                final Long expertCode = expertsForProjectSearchBucket.getOrganizationId();
                if (searchResultBucketMap.containsKey(expertCode)) {
                    final SearchResultBucketDto searchResultBucketDto = searchResultBucketMap.get(expertCode);
                    searchResultBucketDto.setInvestmentProjectAppearance(searchResultBucketDto.getInvestmentProjectAppearance() + expertsForProjectSearchBucket.getInvestmentProjectAppearance());
                    searchResultBucketDto.setRank(searchResultBucketDto.getRank() + expertsForProjectSearchBucket.getRank());
                } else {
                    searchResultBucketMap.put(expertCode, expertsForProjectSearchBucket);
                }
            }
        }
        final HashMap<String, Object> params = new HashMap<>();
        params.put("resultBuckets", searchResultBucketMap.values().stream().collect(Collectors.toMap(x -> x.getOrganizationId().toString(), SearchResultBucketDto::getRank)));
        final Script script = new Script(ScriptType.INLINE, "painless", "if (params.resultBuckets.containsKey(doc['_id'].value)) {return params.resultBuckets[doc['_id'].value];} return 0;", params);
        final List<FunctionScoreQueryBuilder.FilterFunctionBuilder> filterFunctionBuilders = new ArrayList<>();
        filterFunctionBuilders.add(new FunctionScoreQueryBuilder.FilterFunctionBuilder(new ScriptScoreFunctionBuilder(script)));
        final FunctionScoreQueryBuilder.FilterFunctionBuilder[] filterFunctionBuildersArray = filterFunctionBuilders.toArray(new FunctionScoreQueryBuilder.FilterFunctionBuilder[0]);
        boolQueryBuilder.should(new FunctionScoreQueryBuilder(new MatchAllQueryBuilder(), filterFunctionBuildersArray));
        return searchResultBucketMap;
    }

    private OrganizationLicencePreviewDto convertOrganizationLicence(Organization.OrganizationLicence organizationLicence) {
        final OrganizationLicencePreviewDto organizationLicencePreviewDto = new OrganizationLicencePreviewDto();
        organizationLicencePreviewDto.setLicenceRole(OrganizationRole.valueOf(organizationLicence.getLicenceRole()));
        organizationLicencePreviewDto.setOrganizationLicenceId(organizationLicence.getOrganizationLicenceId());
        organizationLicencePreviewDto.setValidUntil(organizationLicence.getValidUntil());
        return organizationLicencePreviewDto;
    }

    private OrganizationBaseDto convertToBase(Organization organization) {
        if (organization == null) return null;
        final OrganizationBaseDto organizationDto = new OrganizationBaseDto();
        organizationDto.setOrganizationName(organization.getOrganizationName());
        organizationDto.setOrganizationId(organization.getOrganizationId());
        organizationDto.setOrganizationAbbrev(organization.getOrganizationAbbrev());
        organizationDto.setGdpr(organization.getSearchable());
        return organizationDto;
    }

    private OrganizationAutocompleteDto convertToAutocomplete(Organization organization) {
        final OrganizationAutocompleteDto organizationAutocompleteDto = new OrganizationAutocompleteDto();
        organizationAutocompleteDto.setOrganizationName(organization.getOrganizationName());
        organizationAutocompleteDto.setOrganizationAbbrev(organization.getOrganizationAbbrev());
        organizationAutocompleteDto.setOrganizationId(organization.getOrganizationId());
        organizationAutocompleteDto.setCountryCode(organization.getCountryCode());
        return organizationAutocompleteDto;
    }

    private BoolQueryBuilder prepareBoolQuery(String query, boolean exactMatch) {
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        if (query != null && !query.isEmpty()) {
            boolQueryBuilder.should(new MatchQueryBuilder("organizationName.folded", query).boost(6.0F).operator(exactMatch ? Operator.AND : Operator.OR))
                    .should(new MatchQueryBuilder("organizationAbbrev.folded", query).boost(50.0F).operator(exactMatch ? Operator.AND : Operator.OR))
                    .should(new MatchQueryBuilder("organizationName.autocomplete", query).boost(0.7F).operator(exactMatch ? Operator.AND : Operator.OR))
                    .should(new MatchQueryBuilder("organizationAbbrev.autocomplete", query).boost(10.0F).operator(exactMatch ? Operator.AND : Operator.OR))
                    .minimumShouldMatch(1);
        }
        return boolQueryBuilder;
    }
}
