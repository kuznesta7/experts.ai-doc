package ai.unico.platform.search.util;

import ai.unico.platform.extdata.dto.DWHEquipmentDto;
import ai.unico.platform.search.model.Equipment;
import ai.unico.platform.search.model.EquipmentMultiLanguage;
import ai.unico.platform.store.api.dto.EquipmentRegisterDto;
import ai.unico.platform.store.api.dto.MultilingualDto;
import ai.unico.platform.util.IdUtil;

import java.util.HashSet;
import java.util.stream.Collectors;


public class EquipmentUtil {

    @Deprecated
    public static Equipment convert(DWHEquipmentDto dwhEquipmentDto){
        Equipment equipment = new Equipment();
        equipment.setId(IdUtil.convertToExtDataCode(dwhEquipmentDto.getEquipmentId()));
        equipment.setEquipmentId(dwhEquipmentDto.getEquipmentId());
        equipment.setEquipmentName(dwhEquipmentDto.getEquipmentName());
        equipment.setEquipmentDescription(dwhEquipmentDto.getEquipmentDesc());
        equipment.setEquipmentMarking(dwhEquipmentDto.getEquipmentMarking());
        equipment.setEquipmentSpecialization(new HashSet<>(dwhEquipmentDto.getEquipmentSpecialization()));
        equipment.setYear(dwhEquipmentDto.getYear());
        equipment.setEquipmentServiceLife(dwhEquipmentDto.getEquipmentServiceLife());
        equipment.setPortableDevice(dwhEquipmentDto.getPortableDevice());
        equipment.setEquipmentDomainId(dwhEquipmentDto.getEquipmentDomainId());
        equipment.setEquipmentTypeId(dwhEquipmentDto.getEquipmentTypeId());
        equipment.setTranslationUnavailable(dwhEquipmentDto.isTranslationUnavailable());
        equipment.setHidden(false);
        return equipment;
    }

    public static Equipment convert(EquipmentRegisterDto dto) {
        Equipment equipment = new Equipment();
        equipment.setId(IdUtil.convertToIntDataCode(dto.getId()));
        equipment.setEquipmentId(dto.getId());
        equipment.setEquipmentName(dto.getName());
        equipment.setEquipmentDescription(dto.getDescription());
        equipment.setEquipmentMarking(dto.getMarking());
        equipment.setExpertCodes(dto.getExpertCodes());
        equipment.setEquipmentSpecialization(dto.getSpecialization());
        equipment.setYear(dto.getYear());
        equipment.setEquipmentServiceLife(dto.getServiceLife());
        equipment.setPortableDevice(dto.getPortableDevice());
        equipment.setEquipmentTypeId(dto.getType());
        equipment.setOrganizationIds(dto.getOrganizationIds());
        equipment.setHidden(dto.isHidden());
        equipment.setGoodForDescription(dto.getGoodForDescription());
        equipment.setTargetGroup(dto.getTargetGroup());
        equipment.setPreparationTime(dto.getPreparationTime());
        equipment.setEquipmentCost(dto.getEquipmentCost());
        equipment.setInfrastructureDescription(dto.getInfrastructureDescription());
        equipment.setDurationTime(dto.getDurationTime());
        equipment.setLanguageCode(dto.getLanguageCode());
        equipment.setTrl(dto.getTrl());
        return equipment;
    }

    public static EquipmentRegisterDto convert(Equipment equipment) {
        EquipmentRegisterDto dto = new EquipmentRegisterDto();
        dto.setId(equipment.getEquipmentId());
        dto.setName(equipment.getEquipmentName());
        dto.setDescription(equipment.getEquipmentDescription());
        dto.setMarking(equipment.getEquipmentMarking());
        dto.setExpertCodes(equipment.getExpertCodes());
        dto.setSpecialization(equipment.getEquipmentSpecialization());
        dto.setYear(equipment.getYear());
        dto.setServiceLife(equipment.getEquipmentServiceLife());
        dto.setPortableDevice(equipment.getPortableDevice());
        dto.setType(equipment.getEquipmentTypeId());
        dto.setOrganizationIds(equipment.getOrganizationIds());
        dto.setHidden(equipment.isHidden());
        dto.setGoodForDescription(equipment.getGoodForDescription());
        dto.setTargetGroup(equipment.getTargetGroup());
        dto.setPreparationTime(equipment.getPreparationTime());
        dto.setEquipmentCost(equipment.getEquipmentCost());
        dto.setInfrastructureDescription(equipment.getInfrastructureDescription());
        dto.setDurationTime(equipment.getDurationTime());
        dto.setLanguageCode(equipment.getLanguageCode());
        dto.setTrl(equipment.getTrl());
        return dto;
    }

    public static EquipmentMultiLanguage convert(MultilingualDto<EquipmentRegisterDto> dto) {
        final EquipmentMultiLanguage equipmentMultiLanguage = new EquipmentMultiLanguage();
        equipmentMultiLanguage.setEquipmentId(dto.getId());
        equipmentMultiLanguage.setId(dto.getCode());
        equipmentMultiLanguage.setTranslations(
                dto.getTranslations().stream()
                        .map(EquipmentUtil::convert)
                        .collect(Collectors.toList())
        );
        return equipmentMultiLanguage;
    }
}
