package ai.unico.platform.search.util;

import ai.unico.platform.extdata.dto.ExpertLectureDto;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.model.Lecture;
import ai.unico.platform.store.api.dto.ItemTypeSearchDto;
import ai.unico.platform.store.api.service.ItemTypeService;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.KeywordsUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.ScoreSortBuilder;
import org.elasticsearch.search.sort.ScriptSortBuilder;
import org.elasticsearch.search.sort.SortOrder;

import java.util.*;
import java.util.stream.Collectors;

public class LectureUtil {

    public static Lecture convert(ExpertLectureDto expertItemPreviewDto) {
        final Lecture lecture = new Lecture();
        lecture.setId(ItemUtil.convertToDWHItemCode(expertItemPreviewDto.getItemId()));
        lecture.setOriginalLectureId(expertItemPreviewDto.getItemId());
        lecture.setLectureBk(expertItemPreviewDto.getItemBk());
        lecture.setItemTypeId(expertItemPreviewDto.getItemTypeId());
        lecture.setLectureName(expertItemPreviewDto.getItemName());
        lecture.setLectureDescription(expertItemPreviewDto.getItemDescription());
        lecture.setYear(expertItemPreviewDto.getYear());
        lecture.setTranslationUnavailable(expertItemPreviewDto.isTranslationUnavailable());
        lecture.setKeywords(KeywordsUtil.sortKeywordList(expertItemPreviewDto.getKeywords().stream().distinct().collect(Collectors.toList())));
        lecture.setConfidentiality(expertItemPreviewDto.isConfidential() ? Confidentiality.CONFIDENTIAL : Confidentiality.PUBLIC);
        lecture.setItemDomain(expertItemPreviewDto.getItemDomain());
        lecture.setItemSpecification(expertItemPreviewDto.getItemSpecification());
        lecture.setItemScienceSpecification(expertItemPreviewDto.getItemScienceSpecification());
        final Set<String> expertCodes = expertItemPreviewDto.getExpertIds().stream()
                .map(IdUtil::convertToExtDataCode)
                .collect(Collectors.toSet());
        lecture.getExpertCodes().addAll(expertCodes);
        lecture.getAllExpertCodes().addAll(expertCodes);
        lecture.getOriginalExpertCodes().addAll(expertCodes);
        return lecture;
    }

    public static void addLectureSortFunction(SearchSourceBuilder searchSourceBuilder, ExpertSearchResultFilter.Order orderBy, String expertCode) {
        searchSourceBuilder.trackScores(true);
        if (orderBy.equals(ExpertSearchResultFilter.Order.YEAR_DESC)) {
            searchSourceBuilder.sort("year", SortOrder.DESC);
        } else if (orderBy.equals(ExpertSearchResultFilter.Order.YEAR_ASC)) {
            searchSourceBuilder.sort("year", SortOrder.ASC);
        }
        searchSourceBuilder.sort(new ScoreSortBuilder().order(SortOrder.DESC));
        final ItemTypeService itemTypeService = ServiceRegistryUtil.getService(ItemTypeService.class);
        final List<ItemTypeSearchDto> itemTypesSearch = itemTypeService.findItemTypesSearch();
        final Map<String, Object> sortScriptParams = new HashMap<>();
        final Map<String, Float> scoresMap = new HashMap<>();
        sortScriptParams.put("scores", scoresMap);
        sortScriptParams.put("expertCode", expertCode);
        sortScriptParams.put("currentYear", Calendar.getInstance().get(Calendar.YEAR));
        for (ItemTypeSearchDto itemType : itemTypesSearch) {
            scoresMap.put(itemType.getTypeId().toString(), itemType.getWeight());
        }

        if (orderBy.equals(ExpertSearchResultFilter.Order.YEAR_DESC) || orderBy.equals(ExpertSearchResultFilter.Order.YEAR_ASC)) {
            final Script sortScript = new Script(ScriptType.INLINE, "painless", "if(params.scores.containsKey(doc['itemTypeId'].value.toString())) { return params.scores[doc['itemTypeId'].value.toString()];} return 0;", sortScriptParams);
            searchSourceBuilder.sort(new ScriptSortBuilder(sortScript, ScriptSortBuilder.ScriptSortType.NUMBER).order(SortOrder.DESC));
        } else {
            final Script sortScript = new Script(ScriptType.INLINE, "painless", "if(params.scores.containsKey(doc['itemTypeId'].value.toString())) { return params.scores[doc['itemTypeId'].value.toString()] * Math.pow(1.05,doc['year'].value - params.currentYear);} return 0;", sortScriptParams);
            searchSourceBuilder.sort(new ScriptSortBuilder(sortScript, ScriptSortBuilder.ScriptSortType.NUMBER).order(SortOrder.DESC));
        }
    }


}
