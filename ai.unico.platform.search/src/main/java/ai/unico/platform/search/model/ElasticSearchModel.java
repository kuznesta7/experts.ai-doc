package ai.unico.platform.search.model;

public abstract class ElasticSearchModel {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
