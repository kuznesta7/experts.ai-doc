package ai.unico.platform.search.util;

import org.apache.lucene.queryparser.flexible.standard.QueryParserUtil;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ElasticSearchUtil {

    public static String getHighlightedText(SearchHit searchHit, String key) {
        final Map<String, HighlightField> highlightMap = searchHit.getHighlightFields();
        final Map<String, Object> baseMap = searchHit.getSourceAsMap();
        if (highlightMap.containsKey(key)) {
            final HighlightField highlightField = highlightMap.get(key);
            final String s = highlightField.fragments()[0].toString();
            if (s != null) return s;
        }
        return (String) baseMap.get(key);
    }

    public static List<String> getHighlightedTexts(SearchHit searchHit, String key) {
        final Map<String, HighlightField> highlightMap = searchHit.getHighlightFields();
        final Map<String, Object> baseMap = searchHit.getSourceAsMap();
        final List<String> texts = new ArrayList<>();
        if (highlightMap.containsKey(key)) {
            final HighlightField highlightField = highlightMap.get(key);
            for (Text fragment : highlightField.fragments()) {
                final String highlightedText = fragment.toString();
                texts.add(highlightedText);
            }
        }
        final List<String> notHighlightedTexts = (List<String>) baseMap.get(key);
        for (String text : texts) {
            final String added = text.replaceAll("<.*?>", "");
            notHighlightedTexts.remove(added);
        }
        texts.addAll(notHighlightedTexts);
        return texts;
    }

    public static String prepareQueryStringQuery(String query) {
        if (query == null || query.isEmpty()) return "";
        return QueryParserUtil.escape(query);
//        return query.replaceAll("/", "");
    }

}
