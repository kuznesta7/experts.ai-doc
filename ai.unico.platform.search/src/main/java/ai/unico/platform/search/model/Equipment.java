package ai.unico.platform.search.model;

import java.util.HashSet;
import java.util.Set;

public class Equipment extends ElasticSearchModel {

    private Long equipmentId;
    private String languageCode;
    private String equipmentName;
    private String equipmentMarking;
    private String equipmentDescription;
    private Set<String> equipmentSpecialization = new HashSet<>();
    private Integer year;
    private Integer equipmentServiceLife;
    private Boolean portableDevice;
    private Long equipmentDomainId;
    private Long equipmentTypeId;
    private boolean translationUnavailable;
    private Set<Long> organizationIds = new HashSet<>();
    private Set<String> expertCodes = new HashSet<>();
    private Boolean hidden;
    private String goodForDescription;
    private String targetGroup;
    private String preparationTime;
    private String equipmentCost;
    private String infrastructureDescription;
    private String durationTime;
    private String trl;

    public Long getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(Long equipmentId) {
        this.equipmentId = equipmentId;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getEquipmentMarking() {
        return equipmentMarking;
    }

    public void setEquipmentMarking(String equipmentMarking) {
        this.equipmentMarking = equipmentMarking;
    }

    public String getEquipmentDescription() {
        return equipmentDescription;
    }

    public void setEquipmentDescription(String equipmentDescription) {
        this.equipmentDescription = equipmentDescription;
    }

    public Set<String> getEquipmentSpecialization() {
        return equipmentSpecialization;
    }

    public void setEquipmentSpecialization(Set<String> equipmentSpecialization) {
        this.equipmentSpecialization = equipmentSpecialization;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getEquipmentServiceLife() {
        return equipmentServiceLife;
    }

    public void setEquipmentServiceLife(Integer equipmentServiceLife) {
        this.equipmentServiceLife = equipmentServiceLife;
    }

    public Boolean getPortableDevice() {
        return portableDevice;
    }

    public void setPortableDevice(Boolean portableDevice) {
        this.portableDevice = portableDevice;
    }

    public Long getEquipmentDomainId() {
        return equipmentDomainId;
    }

    public void setEquipmentDomainId(Long equipmentDomainId) {
        this.equipmentDomainId = equipmentDomainId;
    }

    public Long getEquipmentTypeId() {
        return equipmentTypeId;
    }

    public void setEquipmentTypeId(Long equipmentTypeId) {
        this.equipmentTypeId = equipmentTypeId;
    }

    public boolean isTranslationUnavailable() {
        return translationUnavailable;
    }

    public void setTranslationUnavailable(boolean translationUnavailable) {
        this.translationUnavailable = translationUnavailable;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<String> getExpertCodes() {
        return expertCodes;
    }

    public void setExpertCodes(Set<String> expertCodes) {
        this.expertCodes = expertCodes;
    }

    public Boolean isHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public String getGoodForDescription() {
        return goodForDescription;
    }

    public void setGoodForDescription(String goodForDescription) {
        this.goodForDescription = goodForDescription;
    }

    public String getTargetGroup() {
        return targetGroup;
    }

    public void setTargetGroup(String targetGroup) {
        this.targetGroup = targetGroup;
    }

    public String getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(String preparationTime) {
        this.preparationTime = preparationTime;
    }

    public String getEquipmentCost() {
        return equipmentCost;
    }

    public void setEquipmentCost(String equipmentCost) {
        this.equipmentCost = equipmentCost;
    }

    public String getInfrastructureDescription() {
        return infrastructureDescription;
    }

    public void setInfrastructureDescription(String infrastructureDescription) {
        this.infrastructureDescription = infrastructureDescription;
    }

    public String getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(String durationTime) {
        this.durationTime = durationTime;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getTrl() {
        return trl;
    }

    public void setTrl(String trl) {
        this.trl = trl;
    }
}
