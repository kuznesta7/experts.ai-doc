package ai.unico.platform.search.service;

import ai.unico.platform.recommendation.api.interfaces.RecombeeSearchEntity;
import ai.unico.platform.search.api.service.*;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.*;
import ai.unico.platform.store.api.enums.IndexTarget;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

public class RecommendationIndexServiceImpl implements RecommendationIndexService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Override
    public Set<RecombeeSearchEntity> findEntriesForOrganizations(Set<Long> organizationIds, IndexTarget indexName, int limit, int from) throws IOException {
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(new TermsQueryBuilder("organizationIds", organizationIds));
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(boolQueryBuilder)
                .size(limit)
                .from(from);

        switch (indexName) {
            case EXPERTS:
                return elasticSearchDao.doSearchAndGetHits(ExpertIndexService.indexName, searchSourceBuilder, Expert.class)
                        .getSearchResponseEntities()
                        .stream()
                        .map(SearchResponseEntity::getSource)
                        .collect(Collectors.toSet());
            case ITEMS:
                return elasticSearchDao.doSearchAndGetHits(ItemIndexService.indexName, searchSourceBuilder, Item.class)
                        .getSearchResponseEntities()
                        .stream()
                        .map(SearchResponseEntity::getSource)
                        .collect(Collectors.toSet());
            case PROJECTS:
                return elasticSearchDao.doSearchAndGetHits(ProjectIndexService.indexName, searchSourceBuilder, Project.class)
                        .getSearchResponseEntities()
                        .stream()
                        .map(SearchResponseEntity::getSource)
                        .collect(Collectors.toSet());
            case OPPORTUNITY:
                return elasticSearchDao.doSearchAndGetHits(OpportunityIndexService.indexName, searchSourceBuilder, Opportunity.class)
                        .getSearchResponseEntities()
                        .stream()
                        .map(SearchResponseEntity::getSource)
                        .collect(Collectors.toSet());
        }

        return null;
    }
}
