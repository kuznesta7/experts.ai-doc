package ai.unico.platform.search.service;

import ai.unico.platform.search.api.service.SearchMappingsLoaderService;
import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class SearchMappingsLoaderServiceImpl implements SearchMappingsLoaderService {

    private final String basePath = "json/";

    @Override
    public String loadMappings(String key) {
        try (final InputStream inputStream = SearchMappingsLoaderServiceImpl.class.getClassLoader().getResourceAsStream(basePath + key + ".json")) {
            return IOUtils.toString(inputStream, StandardCharsets.UTF_8);
        } catch (Exception e) {
            return null;
        }
    }
}
