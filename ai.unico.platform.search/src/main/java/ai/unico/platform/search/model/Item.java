package ai.unico.platform.search.model;

import ai.unico.platform.recommendation.api.enums.RecombeeColumn;
import ai.unico.platform.recommendation.api.interfaces.RecombeeSearchEntity;
import ai.unico.platform.search.api.service.EvidenceExpertService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;

import java.util.*;

public class Item extends ElasticSearchModel implements RecombeeSearchEntity {

    private Long itemId;

    private Long originalItemId;

    private String itemBk;

    private String itemName;

    private String itemDescription;

    private Long itemTypeId;

    private Integer year;

    private Confidentiality confidentiality;

    private Long ownerUserId;

    private Long ownerOrganizationId;

    private Boolean translationUnavailable;

    private String doi;

    private String itemLink;

    private List<String> keywords = new ArrayList<>();

    private Set<Long> organizationIds = new HashSet<>();

    private Set<Long> verifiedOrganizationIds = new HashSet<>();

    private Set<Long> evidenceOrganizationIds = new HashSet<>();

    private Set<Long> affiliatedOrganizationIds = new HashSet<>();

    private Set<Long> activeOrganizationIds = new HashSet<>();

    private Set<Long> notRegisteredExpertsIds = new HashSet<>();

    private Set<String> expertCodes = new HashSet<>();

    private Set<String> allExpertCodes = new HashSet<>();

    private Set<String> favoriteExpertCodes = new HashSet<>();

    private Set<String> originalExpertCodes = new HashSet<>();

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getOriginalItemId() {
        return originalItemId;
    }

    public void setOriginalItemId(Long originalItemId) {
        this.originalItemId = originalItemId;
    }

    public String getItemBk() {
        return itemBk;
    }

    public void setItemBk(String itemBk) {
        this.itemBk = itemBk;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Long getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(Long itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<Long> getNotRegisteredExpertsIds() {
        return notRegisteredExpertsIds;
    }

    public void setNotRegisteredExpertsIds(Set<Long> notRegisteredExperts) {
        this.notRegisteredExpertsIds = notRegisteredExperts;
    }

    public Set<String> getExpertCodes() {
        return expertCodes;
    }

    public void setExpertCodes(Set<String> expertCodes) {
        this.expertCodes = expertCodes;
    }

    public Set<String> getAllExpertCodes() {
        return allExpertCodes;
    }

    public void setAllExpertCodes(Set<String> allExpertCodes) {
        this.allExpertCodes = allExpertCodes;
    }

    public Set<String> getFavoriteExpertCodes() {
        return favoriteExpertCodes;
    }

    public void setFavoriteExpertCodes(Set<String> favoriteExpertCodes) {
        this.favoriteExpertCodes = favoriteExpertCodes;
    }

    public Set<Long> getVerifiedOrganizationIds() {
        return verifiedOrganizationIds;
    }

    public void setVerifiedOrganizationIds(Set<Long> verifiedOrganizationIds) {
        this.verifiedOrganizationIds = verifiedOrganizationIds;
    }

    public Long getOwnerUserId() {
        return ownerUserId;
    }

    public void setOwnerUserId(Long ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    public Long getOwnerOrganizationId() {
        return ownerOrganizationId;
    }

    public void setOwnerOrganizationId(Long ownerOrganizationId) {
        this.ownerOrganizationId = ownerOrganizationId;
    }

    public Confidentiality getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Confidentiality confidentiality) {
        this.confidentiality = confidentiality;
    }

    public Set<String> getOriginalExpertCodes() {
        return originalExpertCodes;
    }

    public void setOriginalExpertCodes(Set<String> originalExpertCodes) {
        this.originalExpertCodes = originalExpertCodes;
    }

    public Set<Long> getEvidenceOrganizationIds() {
        return evidenceOrganizationIds;
    }

    public void setEvidenceOrganizationIds(Set<Long> evidenceOrganizationIds) {
        this.evidenceOrganizationIds = evidenceOrganizationIds;
    }

    public Set<Long> getAffiliatedOrganizationIds() {
        return affiliatedOrganizationIds;
    }

    public void setAffiliatedOrganizationIds(Set<Long> affiliatedOrganizationIds) {
        this.affiliatedOrganizationIds = affiliatedOrganizationIds;
    }

    public Boolean getTranslationUnavailable() {
        return translationUnavailable;
    }

    public void setTranslationUnavailable(Boolean translationUnavailable) {
        this.translationUnavailable = translationUnavailable;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getItemLink() {
        return itemLink;
    }

    public void setItemLink(String itemLink) {
        this.itemLink = itemLink;
    }

    public Set<Long> getActiveOrganizationIds() {
        return activeOrganizationIds;
    }

    public void setActiveOrganizationIds(Set<Long> activeOrganizationIds) {
        this.activeOrganizationIds = activeOrganizationIds;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put(RecombeeColumn.IS_ITEM.getColumnName(), true);
        map.put(RecombeeColumn.ID.getColumnName(), itemId);
        map.put(RecombeeColumn.ORIGINAL_ID.getColumnName(), originalItemId);
        map.put(RecombeeColumn.NAME.getColumnName(), itemName);
        map.put(RecombeeColumn.DESCRIPTION.getColumnName(), itemDescription);
        map.put(RecombeeColumn.TYPE_ID.getColumnName(), itemTypeId);
        map.put(RecombeeColumn.YEAR.getColumnName(), year);
        map.put(RecombeeColumn.KEYWORDS.getColumnName(), keywords);
        map.put(RecombeeColumn.ORGANIZATION_IDS.getColumnName(), organizationIds);
        map.put(RecombeeColumn.EXPERT_CODES.getColumnName(), expertCodes);
        map.put(RecombeeColumn.VERIFIED_ORGANIZATION_IDS.getColumnName(), verifiedOrganizationIds);
        map.put(RecombeeColumn.RETIRED_ORGANIZATION_IDS.getColumnName(), activeOrganizationIds);
        map.put(RecombeeColumn.OWNER_USER_ID.getColumnName(), ownerUserId);
        map.put(RecombeeColumn.OWNER_ORGANIZATION_ID.getColumnName(), ownerOrganizationId);
        map.put(RecombeeColumn.CONFIDENTIALITY.getColumnName(), confidentiality);
        map.put(RecombeeColumn.LINK.getColumnName(), itemLink);
        map.put(RecombeeColumn.AFFILIATED_ORGANIZATION_IDS.getColumnName(), affiliatedOrganizationIds);

        final EvidenceExpertService evidenceExpertService = ServiceRegistryUtil.getService(EvidenceExpertService.class);
        try {
            assert evidenceExpertService != null;
            Set<String> names = evidenceExpertService.findExpertNamesByCodes(getExpertCodes());
            map.put(RecombeeColumn.EXPERT_NAMES.getColumnName(), names);
        } catch (Exception e) {
            System.out.printf("Error when uploading merged Item's expert names. Continuing... Error message: " + e.getMessage());
            e.printStackTrace();
        }
        return map;
    }
}
