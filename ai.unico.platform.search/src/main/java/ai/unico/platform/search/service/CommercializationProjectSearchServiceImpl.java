package ai.unico.platform.search.service;

import ai.unico.platform.search.api.dto.CommercializationProjectDetailDto;
import ai.unico.platform.search.api.dto.CommercializationTeamMemberDto;
import ai.unico.platform.search.api.dto.InvestmentProjectPublicPreviewDto;
import ai.unico.platform.search.api.dto.SearchResultBucketDto;
import ai.unico.platform.search.api.filter.ExpertSearchFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.service.CommercializationProjectSearchService;
import ai.unico.platform.search.api.service.ExpertLookupService;
import ai.unico.platform.search.api.service.ItemSearchService;
import ai.unico.platform.search.api.service.OrganizationSearchService;
import ai.unico.platform.search.api.wrapper.ExpertInvestmentProjectWrapper;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.CommercializationProject;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.CommercializationEnumUtil;
import ai.unico.platform.search.util.ElasticSearchUtil;
import ai.unico.platform.search.util.SearchRankUtil;
import ai.unico.platform.store.api.dto.*;
import ai.unico.platform.store.api.service.CommercializationStatusService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.*;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.BucketOrder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import org.elasticsearch.search.aggregations.metrics.sum.SumAggregationBuilder;
import org.elasticsearch.search.aggregations.support.ValueType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CommercializationProjectSearchServiceImpl implements CommercializationProjectSearchService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private ExpertLookupService expertLookupService;

    @Value("${elasticsearch.settings.maxBucketSize}")
    private Integer maxBucketSize;

    @Override
    public List<SearchResultBucketDto> findExpertsForProjectSearch(ExpertSearchFilter expertSearchFilter) throws IOException {
        final List<? extends Terms.Bucket> buckets = findForProjectSearch(expertSearchFilter, ValueType.STRING, "experts", "expertCodes");
        final List<SearchResultBucketDto> resultBuckets = new ArrayList<>();
        for (Terms.Bucket bucket : buckets) {
            final SearchResultBucketDto resultBucket = new SearchResultBucketDto();
            final Sum score = bucket.getAggregations().get("score");
            resultBucket.setExpertCode((String) bucket.getKey());
            resultBucket.setInvestmentProjectAppearance(bucket.getDocCount());
            resultBucket.setRank(score.getValue());
            resultBuckets.add(resultBucket);
        }
        return resultBuckets;
    }

    @Override
    public List<SearchResultBucketDto> findOrganizationsForProjectSearch(ExpertSearchFilter expertSearchFilter) throws IOException {
        final List<? extends Terms.Bucket> buckets = findForProjectSearch(expertSearchFilter, ValueType.LONG, "organizations", "organizationIds");
        final List<SearchResultBucketDto> resultBuckets = new ArrayList<>();
        for (Terms.Bucket bucket : buckets) {
            final SearchResultBucketDto resultBucket = new SearchResultBucketDto();
            final Sum score = bucket.getAggregations().get("score");
            resultBucket.setOrganizationId((Long) bucket.getKey());
            resultBucket.setInvestmentProjectAppearance(bucket.getDocCount());
            resultBucket.setRank(score.getValue());
            resultBuckets.add(resultBucket);
        }
        return resultBuckets;
    }

    @Override
    public ExpertInvestmentProjectWrapper findExpertInvestmentProjects(String expertCode, ExpertSearchResultFilter expertSearchResultFilter) throws IOException {
        final String query = expertSearchResultFilter.getQuery();
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final Integer limit = expertSearchResultFilter.getInvestmentProjectsLimit();
        final Integer page = expertSearchResultFilter.getInvestmentProjectsPage();
        final Map<String, Object> params = new HashMap<>();
        params.put("minScore", ItemSearchService.minScore);
        final Script script = new Script(ScriptType.INLINE, "painless", "_score > params.minScore ? _score : 0;", params);
        final BoolQueryBuilder boolQuery = new BoolQueryBuilder();
        if (query != null && !query.isEmpty()) {
            final HighlightBuilder highlightBuilder = new HighlightBuilder().numOfFragments(0).highlightQuery(
                    prepareQuery(expertSearchResultFilter))
                    .field("name")
                    .field("executiveSummary")
                    .field("domainNames");
            searchSourceBuilder.highlighter(highlightBuilder);
            boolQuery.must(prepareQuery(expertSearchResultFilter));
        }
        boolQuery.filter(new TermQueryBuilder("expertCodes", expertCode))
                .filter(prepareTermsQueryForSearchProjects());
        searchSourceBuilder.query(boolQuery)
                .size(limit)
                .from((page - 1) * limit)
                .fetchSource(null, "image")
                .aggregation(new SumAggregationBuilder("scoreSum").script(script));

        final SearchResponse searchResponse = elasticSearchDao.doSearch(indexName, searchSourceBuilder);
        final SearchHits searchHits = searchResponse.getHits();
        final Sum scoreSum = searchResponse.getAggregations().get("scoreSum");
        final ExpertInvestmentProjectWrapper expertInvestmentProjectWrapper = new ExpertInvestmentProjectWrapper();
        expertInvestmentProjectWrapper.setPage(page);
        expertInvestmentProjectWrapper.setLimit(limit);
        expertInvestmentProjectWrapper.setNumberOfRelevantInvestmentProjects(searchHits.totalHits);
        expertInvestmentProjectWrapper.setScoreSum(scoreSum.getValue());

        final Map<Long, CommercializationCategoryPreviewDto> categoryMap = CommercializationEnumUtil.findCategoryMap();
        final Map<Long, CommercializationDomainDto> domainMap = CommercializationEnumUtil.findDomainMap();
        final Map<Long, CommercializationStatusTypeDto> statusMap = CommercializationEnumUtil.findStatusMap();

        for (SearchHit searchHit : searchHits) {
            final Map<String, Object> sourceAsMap = searchHit.getSourceAsMap();
            final ObjectMapper objectMapper = new ObjectMapper();
            final CommercializationProject commercializationProject = objectMapper.convertValue(sourceAsMap, CommercializationProject.class);
            final float score = searchHit.getScore();
            final Double rank = SearchRankUtil.recalculateRank((double) score);
            final InvestmentProjectPublicPreviewDto investmentProjectPublicPreviewDto = new InvestmentProjectPublicPreviewDto();
            final Set<Long> categoryIds = commercializationProject.getCategoryIds();
            final Long domainId = commercializationProject.getDomainId();
            final Long statusId = commercializationProject.getStatusId();
            investmentProjectPublicPreviewDto.setId(searchHit.getId());
            investmentProjectPublicPreviewDto.setRelevancy(rank);
            investmentProjectPublicPreviewDto.setName(ElasticSearchUtil.getHighlightedText(searchHit, "name"));
            investmentProjectPublicPreviewDto.setExecutiveSummary(ElasticSearchUtil.getHighlightedText(searchHit, "executiveSummary"));
            investmentProjectPublicPreviewDto.setKeywords(commercializationProject.getKeywords());
            investmentProjectPublicPreviewDto.setInvestmentFrom(commercializationProject.getInvestmentRangeFrom());
            investmentProjectPublicPreviewDto.setInvestmentTo(commercializationProject.getInvestmentRangeTo());
            investmentProjectPublicPreviewDto.setImgCheckSum(commercializationProject.getCommercializationProjectImgCheckSum());

            if (categoryMap.containsKey(domainId)) {
                investmentProjectPublicPreviewDto.setDomainId(domainId);
                investmentProjectPublicPreviewDto.setDomainName(domainMap.get(domainId).getDomainName());
            }
            if (statusMap.containsKey(statusId)) {
                investmentProjectPublicPreviewDto.setStatusId(statusId);
                investmentProjectPublicPreviewDto.setStatusName(statusMap.get(statusId).getStatusName());
            }
            for (Long categoryId : categoryIds) {
                if (categoryMap.containsKey(categoryId)) {
                    investmentProjectPublicPreviewDto.getCategories().add(categoryMap.get(categoryId));
                }
            }
            expertInvestmentProjectWrapper.getInvestmentProjectPublicPreviewDtos().add(investmentProjectPublicPreviewDto);
        }
        return expertInvestmentProjectWrapper;
    }

    @Override
    public ExpertInvestmentProjectStatistics findExpertInvestmentProjectStatistics(String expertCode, Set<Long> organizationIds) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        BoolQueryBuilder boolQueryBuilder1 = new BoolQueryBuilder();
        if (!organizationIds.isEmpty()) {
            boolQueryBuilder1.filter(new TermsQueryBuilder("commercializationProjectOrganizations.organizationId", organizationIds));
            boolQueryBuilder.filter(new NestedQueryBuilder("commercializationProjectOrganizations", boolQueryBuilder1, ScoreMode.Max));
        }
        searchSourceBuilder.query(boolQueryBuilder
                .filter(new TermQueryBuilder("expertCodes", expertCode))
                .filter(prepareTermsQueryForSearchProjects()))
                .size(0);
        final SearchResponseEntityWrapper<CommercializationProject> resultWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, CommercializationProject.class);
        final ExpertInvestmentProjectStatistics expertInvestmentProjectStatistics = new ExpertInvestmentProjectStatistics();
        expertInvestmentProjectStatistics.setExpertCode(expertCode);
        expertInvestmentProjectStatistics.setNumberOfInvestmentProjects(resultWrapper.getResultCount());
        return expertInvestmentProjectStatistics;
    }

    @Override
    public CommercializationProjectDetailDto findCommercializationProjectDetail(Long commercializationProjectId) throws IOException {
        final OrganizationSearchService organizationSearchService = ServiceRegistryUtil.getService(OrganizationSearchService.class);
        final Map<Long, CommercializationCategoryPreviewDto> categoryMap = CommercializationEnumUtil.findCategoryMap();
        final Map<Long, CommercializationDomainDto> domainMap = CommercializationEnumUtil.findDomainMap();
        final Map<Long, CommercializationPriorityTypeDto> priorityMap = CommercializationEnumUtil.findPriorityMap();
        final Map<Long, CommercializationStatusTypeDto> statusMap = CommercializationEnumUtil.findStatusMap();
        final CommercializationProject document = elasticSearchDao.getDocument(indexName, String.valueOf(commercializationProjectId), CommercializationProject.class);
        if (document == null) throw new NonexistentEntityException(CommercializationProject.class);
        final Set<Long> organizationIds = document.getCommercializationProjectOrganizations().stream().map(CommercializationProject.CommercializationProjectOrganization::getOrganizationId).collect(Collectors.toSet());
        final Long ownerOrganizationId = document.getOwnerOrganizationId();
        if (ownerOrganizationId != null) organizationIds.add(ownerOrganizationId);

        final List<OrganizationBaseDto> baseOrganizations = organizationSearchService.findBaseOrganizations(organizationIds);
        final Map<Long, OrganizationBaseDto> organizationMap = baseOrganizations.stream().collect(Collectors.toMap(OrganizationBaseDto::getOrganizationId, Function.identity()));

        final CommercializationProjectDetailDto commercializationProjectDetailDto = new CommercializationProjectDetailDto();
        commercializationProjectDetailDto.setName(document.getName());
        commercializationProjectDetailDto.setCommercializationProjectId(document.getCommercializationId());
        commercializationProjectDetailDto.setExecutiveSummary(document.getExecutiveSummary());
        commercializationProjectDetailDto.setCommercializationInvestmentFrom(document.getInvestmentRangeFrom());
        commercializationProjectDetailDto.setCommercializationInvestmentTo(document.getInvestmentRangeTo());
        commercializationProjectDetailDto.setIdeaScore(document.getIdeaScore());
        commercializationProjectDetailDto.setTechnologyReadinessLevel(document.getTechnologyReadinessLevel());
        commercializationProjectDetailDto.setKeywords(document.getKeywords());
        commercializationProjectDetailDto.setUseCase(document.getUseCaseDescription());
        commercializationProjectDetailDto.setPainDescription(document.getPainDescription());
        commercializationProjectDetailDto.setCompetitiveAdvantage(document.getCompetitiveAdvantageDescription());
        commercializationProjectDetailDto.setTechnicalPrinciples(document.getTechnicalPrinciplesDescription());
        commercializationProjectDetailDto.setTechnologyReadinessLevel(document.getTechnologyReadinessLevel());
        commercializationProjectDetailDto.setStartDate(document.getStartDate());
        commercializationProjectDetailDto.setEndDate(document.getEndDate());
        commercializationProjectDetailDto.setImgCheckSum(document.getCommercializationProjectImgCheckSum());
        commercializationProjectDetailDto.setCommercializationCategoryIds(document.getCategoryIds());
        commercializationProjectDetailDto.setLink(document.getLink());

        if (ownerOrganizationId != null) {
            commercializationProjectDetailDto.setOwnerOrganization(organizationMap.get(ownerOrganizationId));
        }

        document.getCommercializationProjectOrganizations().stream()
                .map(o -> new CommercializationProjectDetailDto.OrganizationWithRelationBaseDto(organizationMap.get(o.getOrganizationId()), o.getRelation()))
                .forEach(commercializationProjectDetailDto.getProjectOrganizations()::add);

        final Long domainId = document.getDomainId();
        if (domainMap.containsKey(domainId)) {
            commercializationProjectDetailDto.setCommercializationDomainId(domainId);
            commercializationProjectDetailDto.setCommercializationDomainName(domainMap.get(domainId).getDomainName());
        }
        final Long statusId = document.getStatusId();
        if (statusMap.containsKey(statusId)) {
            commercializationProjectDetailDto.setCommercializationStatusId(statusId);
            commercializationProjectDetailDto.setCommercializationStatusName(statusMap.get(statusId).getStatusName());
        }
        final Set<Long> categoryIds = document.getCategoryIds();
        for (Long categoryId :
                categoryIds) {
            if (categoryMap.containsKey(categoryId)) {
                commercializationProjectDetailDto.getCommercializationCategoryPreview().add(categoryMap.get(categoryId));
            }
        }
        final Long priorityId = document.getPriorityId();
        if (priorityMap.containsKey(priorityId)) {
            commercializationProjectDetailDto.setCommercializationPriorityId(priorityId);
            commercializationProjectDetailDto.setCommercializationPriorityName(priorityMap.get(priorityId).getPriorityTypeName());
        }

        final List<UserExpertBaseDto> userExpertBases = expertLookupService.findUserExpertBases(document.getExpertCodes());
        for (UserExpertBaseDto userExpertBase : userExpertBases) {
            final CommercializationTeamMemberDto teamMemberDto = new CommercializationTeamMemberDto(userExpertBase);
            if (document.getTeamLeaderExpertCodes().contains(userExpertBase.getExpertCode())) {
                teamMemberDto.setTeamLeader(true);
            }
            commercializationProjectDetailDto.getTeamMemberDtos().add(teamMemberDto);
        }
        commercializationProjectDetailDto.getTeamMemberDtos().sort(Comparator.comparing(CommercializationTeamMemberDto::isTeamLeader).reversed());
        return commercializationProjectDetailDto;
    }

    private List<? extends Terms.Bucket> findForProjectSearch(ExpertSearchFilter expertSearchFilter, ValueType valueType, String aggregationName, String fieldName) throws IOException {
        final BoolQueryBuilder query = new BoolQueryBuilder();
        if (expertSearchFilter.getQuery() != null && !expertSearchFilter.getQuery().isEmpty()) {
            query.must(prepareQuery(expertSearchFilter))
                    .filter(prepareTermsQueryForSearchProjects());
        }
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder()
                .query(query)
                .aggregation(
                        new TermsAggregationBuilder(aggregationName, valueType)
                                .field(fieldName)
                                .order(BucketOrder.aggregation("score", false))
                                .size(maxBucketSize)
                                .subAggregation(new SumAggregationBuilder("score").script(new Script("_score"))));
        final SearchResponse searchResponse = elasticSearchDao.doSearch(indexName, sourceBuilder);
        final Terms terms = searchResponse.getAggregations().get(aggregationName);
        return terms.getBuckets();
    }

    private QueryStringQueryBuilder prepareQuery(ExpertSearchFilter expertSearchFilter) {
        final Map<String, Float> fields = new HashMap<>();
        fields.put("name", 6.0F);
        fields.put("executiveSummary", 3.0F);
        fields.put("useCaseDescription", 3.0F);
        fields.put("painDescription", 3.0F);
        fields.put("competitiveAdvantageDescription", 3.0F);
        fields.put("technicalPrinciplesDescription", 3.0F);
        fields.put("keywords", 10.0F);
        final String query = expertSearchFilter.getQuery();
        return new QueryStringQueryBuilder(ElasticSearchUtil.prepareQueryStringQuery(query))
                .defaultOperator(Operator.AND).fields(fields);
    }

    private TermsQueryBuilder prepareTermsQueryForSearchProjects() {
        final CommercializationStatusService commercializationStatusService = ServiceRegistryUtil.getService(CommercializationStatusService.class);
        final Set<Long> statusIdsForSearch = commercializationStatusService.findStatusIdsForSearch();
        return new TermsQueryBuilder("statusId", statusIdsForSearch);
    }
}
