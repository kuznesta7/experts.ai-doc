package ai.unico.platform.search.service;

import ai.unico.platform.search.api.service.*;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Expert;
import ai.unico.platform.search.model.Organization;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.store.api.dto.OrganizationIndexDto;
import ai.unico.platform.store.api.dto.OrganizationLicencePreviewDto;
import ai.unico.platform.store.api.enums.IndexHistoryStatus;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.api.service.IndexHistoryService;
import ai.unico.platform.store.api.service.OrganizationService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.model.UserContext;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.index.query.NestedQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.reindex.UpdateByQueryRequest;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class OrganizationIndexServiceImpl implements OrganizationIndexService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private SearchMappingsLoaderService searchMappingsLoaderService;

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;

    private boolean indexingInProgress = false;

    @Override
    public void reindexOrganizations(Integer limit, boolean clean, UserContext userContext) throws IOException {

        final String content = searchMappingsLoaderService.loadMappings("organizations");
        if (content == null) throw new FileNotFoundException();

        if (indexingInProgress) return;
        indexingInProgress = true;
        final IndexHistoryService indexHistoryService = ServiceRegistryUtil.getService(IndexHistoryService.class);
        final Long indexHistoryId = indexHistoryService.saveIndexHistory(IndexTarget.ORGANIZATIONS, true, userContext);
        try {
            final OrganizationService userOrganizationService = ServiceRegistryUtil.getService(OrganizationService.class);
            System.out.println("starting organization reindex");
            if (clean) {
                System.out.println("updating organizations");
                userOrganizationService.updateOrganizationsFromDWH();
                System.out.println("organizations updated");
                try {
                    elasticSearchDao.deleteIndex(indexName);
                } catch (Exception ignore) {
                }
                elasticSearchDao.initIndex(indexName, content);
            }
            System.out.println("loading organizations");
            Integer offset = 0;
            final List<OrganizationIndexDto> allOrganizations = userOrganizationService.findAllOrganizations();
            System.out.println("organizations loaded");
            List<OrganizationIndexDto> organizations;
            do {
                organizations = allOrganizations.subList(offset, Math.min(offset + limit, allOrganizations.size()));

                final List<Organization> organizationEntities = new ArrayList<>();

                for (OrganizationIndexDto registeredOrganization : organizations) {
                    organizationEntities.add(convert(registeredOrganization));
                }
                System.out.println("indexing organizations " + offset);
                elasticSearchDao.doIndex(indexName, organizationEntities);
                offset += limit;
            } while (offset < allOrganizations.size());
            System.out.println("organization index done");
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FINISHED, userContext);
        } catch (Exception e) {
            e.printStackTrace();
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FAILED, e.getCause().toString(), userContext);
        } finally {
            indexingInProgress = false;
        }
    }

    @Override
    public void indexOrganization(OrganizationIndexDto organizationDto) throws IOException {
        final Organization organization = convert(organizationDto);
        elasticSearchDao.doIndex(indexName, Collections.singletonList(organization));
        updateOrganizationExpertsComputedFields(organizationDto.getOrganizationId());
    }

    @Override
    public void replaceOrganization(Long currentOrganizationId, Long substituteOrganizationId) throws IOException {
        final HashMap<String, Object> valuesMap = new HashMap<>();
        valuesMap.put(currentOrganizationId.toString(), substituteOrganizationId);
        final Map<String, Object> params = new HashMap<>();
        params.put("currentOrganizationId", currentOrganizationId);
        params.put("substituteOrganizationId", substituteOrganizationId);
        final Script script = new Script(ScriptType.INLINE, "painless", "ctx._source.organizationIds.removeAll([params.currentOrganizationId]); if (!ctx._source.organizationIds.contains(params.substituteOrganizationId)) {ctx._source.organizationIds.add(params.substituteOrganizationId);}", params);
        final Script evidenceScript = new Script(ScriptType.INLINE, "painless", "ctx._source.evidenceOrganizationIds.removeAll([params.currentOrganizationId]); if (!ctx._source.evidenceOrganizationIds.contains(params.substituteOrganizationId)) {ctx._source.evidenceOrganizationIds.add(params.substituteOrganizationId);}", params);
        final Script financesScript = new Script(ScriptType.INLINE, "painless", "if (ctx._source.finances != null) {for (f in ctx._source.finances) {if (f.organizationId == params.currentOrganizationId) {f.organizationId = params.substituteOrganizationId;}}}", params);
        final TermQueryBuilder queryBuilder = new TermQueryBuilder("organizationIds", currentOrganizationId);
        final TermQueryBuilder evidenceQueryBuilder = new TermQueryBuilder("evidenceOrganizationIds", currentOrganizationId);

        elasticSearchDao.doUpdate(indexName, "substituteOrganizationId", valuesMap);
        elasticSearchDao.doUpdate(CommercializationProjectIndexService.indexName, "ownerOrganizationId", valuesMap);
        elasticSearchDao.doUpdate(ItemIndexService.indexName, "ownerOrganizationId", valuesMap);
        elasticSearchDao.doUpdate(ProjectIndexService.indexName, "ownerOrganizationId", valuesMap);

        elasticSearchDao.doUpdate(new UpdateByQueryRequest(ItemIndexService.indexName).setQuery(queryBuilder).setScript(script));
        elasticSearchDao.flush(ItemIndexService.indexName);
        elasticSearchDao.doUpdate(new UpdateByQueryRequest(ItemIndexService.indexName).setQuery(evidenceQueryBuilder).setScript(evidenceScript));
        elasticSearchDao.flush(ItemIndexService.indexName);

        elasticSearchDao.doUpdate(new UpdateByQueryRequest(ExpertIndexService.indexName).setQuery(queryBuilder).setScript(script));

        elasticSearchDao.doUpdate(new UpdateByQueryRequest(ProjectIndexService.indexName).setQuery(queryBuilder).setScript(script));
        elasticSearchDao.flush(ProjectIndexService.indexName);
        elasticSearchDao.doUpdate(new UpdateByQueryRequest(ProjectIndexService.indexName).setQuery(evidenceQueryBuilder).setScript(financesScript));
        elasticSearchDao.flush(ProjectIndexService.indexName);
        elasticSearchDao.doUpdate(new UpdateByQueryRequest(ProjectIndexService.indexName).setQuery(evidenceQueryBuilder).setScript(evidenceScript));
        elasticSearchDao.flush(ProjectIndexService.indexName);

        final NestedQueryBuilder commercializationQuery = new NestedQueryBuilder("commercializationProjectOrganizations", new TermQueryBuilder("commercializationProjectOrganizations.organizationId", currentOrganizationId), ScoreMode.Max);
        final Script commercializationScript = new Script(ScriptType.INLINE, "painless", "ctx._source.commercializationProjectOrganizations.organizationIds.removeAll([params.currentOrganizationId]); if (!ctx._source.commercializationProjectOrganizations.organizationIds.contains(params.substituteOrganizationId)) {ctx._source.commercializationProjectOrganizations.organizationIds.add(params.substituteOrganizationId);}", params);
        elasticSearchDao.doUpdate(new UpdateByQueryRequest(CommercializationProjectIndexService.indexName).setQuery(commercializationQuery).setScript(commercializationScript));
        updateOrganizationExpertsComputedFields(substituteOrganizationId);
    }

    private void updateOrganizationExpertsComputedFields(Long organizationId) throws IOException {
        final SearchSourceBuilder organizationExpertCodeSearchBuilder = new SearchSourceBuilder()
                .query(new TermQueryBuilder("organizationIds", organizationId))
                .fetchSource(new String[]{"expertCode"}, new String[]{})
                .size(maxResultWindow);
        final SearchResponseEntityWrapper<Expert> expertSearchResponseEntityWrapper = elasticSearchDao.doSearchAndGetHits(ExpertIndexService.indexName, organizationExpertCodeSearchBuilder, Expert.class);
        final ExpertEvaluationService expertEvaluationService = ServiceRegistryUtil.getService(ExpertEvaluationService.class);
        final Set<String> expertCodes = expertSearchResponseEntityWrapper.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource)
                .map(Expert::getExpertCode)
                .collect(Collectors.toSet());
        expertEvaluationService.recalculateComputedFieldsForExperts(expertCodes);
    }

    private Organization convert(OrganizationIndexDto organizationIndexDto) {
        final Organization organization = new Organization();
        final Long organizationId = organizationIndexDto.getOrganizationId();
        organization.setId(organizationId.toString());
        organization.setOrganizationId(organizationId);
        organization.setOrganizationName(organizationIndexDto.getOrganizationName());
        organization.setOrganizationAbbrev(organizationIndexDto.getOrganizationAbbrev());
        organization.setOriginalOrganizationId(organizationIndexDto.getOriginalOrganizationId());
        organization.setChildrenOrganizationIds(organizationIndexDto.getChildrenOrganizationIds());
        organization.setAffiliatedOrganizationIds(organizationIndexDto.getAffiliatedOrganizationIds());
        organization.setMemberOrganizationIds(organizationIndexDto.getMemberOrganizationIds());
        organization.setRank(organizationIndexDto.getRank());
        organization.setCountryCode(organizationIndexDto.getCountryCode());
        organization.setAllowedMembers(organizationIndexDto.getAllowedMembers());
        organization.setAllowedSearch(organizationIndexDto.getAllowedSearch());
        organization.setOrganizationTypeId(organizationIndexDto.getOrganizationTypeId());
        organization.setSearchable(organizationIndexDto.getSearchable());
        organization.setSubstituteOrganizationId(organizationIndexDto.getSubstituteOrganizationId());
        for (OrganizationLicencePreviewDto organizationLicencePreviewDto : organizationIndexDto.getOrganizationLicencePreviewDtos()) {
            final Organization.OrganizationLicence organizationLicence = new Organization.OrganizationLicence();
            organizationLicence.setOrganizationLicenceId(organizationLicencePreviewDto.getOrganizationLicenceId());
            organizationLicence.setLicenceRole(organizationLicencePreviewDto.getLicenceRole().name());
            organizationLicence.setValidUntil(organizationLicencePreviewDto.getValidUntil());
            organization.getOrganizationLicences().add(organizationLicence);
        }
        return organization;
    }
}
