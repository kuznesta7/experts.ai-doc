package ai.unico.platform.search.util;

import ai.unico.platform.store.api.dto.CommercializationCategoryPreviewDto;
import ai.unico.platform.store.api.dto.CommercializationDomainDto;
import ai.unico.platform.store.api.dto.CommercializationPriorityTypeDto;
import ai.unico.platform.store.api.dto.CommercializationStatusTypeDto;
import ai.unico.platform.store.api.service.CommercializationCategoryService;
import ai.unico.platform.store.api.service.CommercializationDomainService;
import ai.unico.platform.store.api.service.CommercializationPriorityService;
import ai.unico.platform.store.api.service.CommercializationStatusService;
import ai.unico.platform.util.ServiceRegistryUtil;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CommercializationEnumUtil {

    public static Map<Long, CommercializationStatusTypeDto> findStatusMap() {
        final CommercializationStatusService statusService = ServiceRegistryUtil.getService(CommercializationStatusService.class);
        final List<CommercializationStatusTypeDto> statuses = statusService.findAvailableStatuses(true);
        return statuses.stream().collect(Collectors.toMap(CommercializationStatusTypeDto::getStatusId, Function.identity()));
    }

    public static Map<Long, CommercializationDomainDto> findDomainMap() {
        final CommercializationDomainService domainService = ServiceRegistryUtil.getService(CommercializationDomainService.class);
        final List<CommercializationDomainDto> domains = domainService.findAvailableCommercializationDomains(true);
        return domains.stream().collect(Collectors.toMap(CommercializationDomainDto::getDomainId, Function.identity()));
    }

    public static Map<Long, CommercializationPriorityTypeDto> findPriorityMap() {
        final CommercializationPriorityService priorityService = ServiceRegistryUtil.getService(CommercializationPriorityService.class);
        final List<CommercializationPriorityTypeDto> priorities = priorityService.findAvailableCommercializationPriorityTypes(true);
        return priorities.stream().collect(Collectors.toMap(CommercializationPriorityTypeDto::getPriorityTypeId, Function.identity()));
    }

    public static Map<Long, CommercializationCategoryPreviewDto> findCategoryMap() {
        final CommercializationCategoryService categoryService = ServiceRegistryUtil.getService(CommercializationCategoryService.class);
        final List<CommercializationCategoryPreviewDto> categories = categoryService.findAvailableCommercializationCategoriesPreview(true);
        return categories.stream().collect(Collectors.toMap(CommercializationCategoryPreviewDto::getCommercializationCategoryId, Function.identity()));
    }

}
