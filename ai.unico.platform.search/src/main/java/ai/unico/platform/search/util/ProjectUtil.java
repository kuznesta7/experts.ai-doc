package ai.unico.platform.search.util;

import ai.unico.platform.extdata.dto.DWHProjectDto;
import ai.unico.platform.search.model.Project;
import ai.unico.platform.store.api.dto.ProjectIndexDto;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.KeywordsUtil;
import ai.unico.platform.util.enums.Confidentiality;

import java.util.Map;

public class ProjectUtil {

    public static Project convert(DWHProjectDto projectDto) {
        final Project project = new Project();
        project.setId(toOriginalProjectCode(projectDto.getOriginalProjectId()));
        project.setOriginalProjectId(projectDto.getOriginalProjectId());
        project.setProjectNumber(projectDto.getProjectNumber());
        project.setStartDate(projectDto.getStartDate());
        project.setEndDate(projectDto.getEndDate());
        project.setName(projectDto.getProjectName());
        project.setDescription(projectDto.getProjectDescription());
        project.setCountryCode(projectDto.getCountryCode());
        project.setKeywords(KeywordsUtil.sortKeywordList(projectDto.getKeywords()));
        project.setProjectLink(projectDto.getProjectLink());
        project.setConfidentiality(projectDto.isConfidential() ? Confidentiality.CONFIDENTIAL : Confidentiality.PUBLIC);
        return project;
    }
    public static Project convert(ProjectIndexDto projectIndexDto) {
        final Project project = new Project();
        project.setId(toConfirmedProjectCode(projectIndexDto.getProjectId()));
        project.setProjectNumber(projectIndexDto.getProjectNumber());
        project.setProjectId(projectIndexDto.getProjectId());
        project.setOriginalProjectId(projectIndexDto.getOriginalProjectId());
        project.setName(projectIndexDto.getName());
        project.setDescription(projectIndexDto.getDescription());
        project.setKeywords(KeywordsUtil.sortKeywordList(projectIndexDto.getKeywords()));
        project.setEndDate(projectIndexDto.getEndDate());
        project.setStartDate(projectIndexDto.getStartDate());
        project.setOrganizationIds(projectIndexDto.getOrganizationIds());
        project.setVerifiedOrganizationIds(projectIndexDto.getVerifiedOrganizationIds());
        project.setProjectProgramId(projectIndexDto.getProjectProgramId());
        project.setConfidentiality(projectIndexDto.getConfidentiality());
        project.setOwnerOrganizationId(projectIndexDto.getOwnerOrganizationId());

        projectIndexDto.getItemIds().stream()
                .map(ItemUtil::convertToItemCode)
                .forEach(project.getItemCodes()::add);
        projectIndexDto.getOriginalItemIds().stream()
                .map(ItemUtil::convertToDWHItemCode)
                .forEach(project.getItemCodes()::add);

        projectIndexDto.getExpertIds().stream()
                .map(IdUtil::convertToExtDataCode)
                .forEach(project.getExpertCodes()::add);

        projectIndexDto.getUserIds().stream()
                .map(IdUtil::convertToIntDataCode)
                .forEach(project.getExpertCodes()::add);

        copyOriginalFields(project);

        for (Map.Entry<Long, Long> entry : projectIndexDto.getOrganizationParentProjectIdMap().entrySet()) {
            final Project.ProjectOrganizationParentProject projectOrganizationParentProject = new Project.ProjectOrganizationParentProject();
            projectOrganizationParentProject.setOrganizationId(entry.getKey());
            projectOrganizationParentProject.setProjectCode(toConfirmedProjectCode(entry.getValue()));
            project.getParentProjects().add(projectOrganizationParentProject);
        }

        for (ProjectIndexDto.ProjectOrganizationBudget budgetDto : projectIndexDto.getProjectOrganizationBudgetList()) {
            final Project.ProjectOrganizationBudget budgetEntity = new Project.ProjectOrganizationBudget();
            budgetEntity.setOrganizationId(budgetDto.getOrganizationId());
            budgetEntity.setYear(budgetDto.getYear());
            budgetEntity.setTotal(budgetDto.getTotal());
            budgetEntity.setNationalSupport(budgetDto.getNationalSupport());
            budgetEntity.setPrivateFinances(budgetDto.getPrivateFinances());
            budgetEntity.setOtherFinances(budgetDto.getOtherFinances());
            project.getFinances().add(budgetEntity);
        }

        if (Confidentiality.SECRET.equals(projectIndexDto.getConfidentiality())) {
            project.getEvidenceOrganizationIds().add(projectIndexDto.getOwnerOrganizationId());
        } else {
            project.getEvidenceOrganizationIds().addAll(projectIndexDto.getOrganizationIds());
        }
        return project;
    }

    public static String toConfirmedProjectCode(Long projectId) {
        return IdUtil.convertToIntDataCode(projectId);
    }

    public static String toOriginalProjectCode(Long originalProjectId) {
        return IdUtil.convertToExtDataCode(originalProjectId);
    }

    private static void copyOriginalFields(Project project) {
        project.getOriginalExpertCodes().addAll(project.getExpertCodes());
    }

}
