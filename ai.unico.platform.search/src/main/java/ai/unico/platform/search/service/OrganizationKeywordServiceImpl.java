package ai.unico.platform.search.service;

import ai.unico.platform.search.api.service.OrganizationKeywordService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.store.api.enums.WidgetTab;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.support.ValueType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class OrganizationKeywordServiceImpl implements OrganizationKeywordService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Override
    public Map<String, Long> findOrganizationKeywords(Set<Long> organizationIds) throws IOException {
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(new TermsQueryBuilder("organizationIds", organizationIds));
        boolQueryBuilder.filter(new TermQueryBuilder("confidentiality", "PUBLIC"));
        Map<String, Long> itemKeywords = findItemKeywords(boolQueryBuilder);
        Map<String, Long> projectKeywords = findProjectKeywords(boolQueryBuilder);
        itemKeywords.forEach((k, v) -> projectKeywords.merge(k, v, Long::sum));
        return projectKeywords;
    }

    private Map<String, Long> findItemKeywords(BoolQueryBuilder boolQueryBuilder) throws IOException {
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.size(0);
        final TermsAggregationBuilder termsAgg = new TermsAggregationBuilder("keywords", ValueType.STRING)
                .field("keywords.raw")
                .size(20);
        sourceBuilder.aggregation(termsAgg);
        final SearchResponse searchResponse = elasticSearchDao.doSearch(itemIndexName, sourceBuilder);
        return getAggregations(searchResponse, "keywords");
    }

    private Map<String, Long> findProjectKeywords(BoolQueryBuilder boolQueryBuilder) throws IOException {
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.size(0);
        final TermsAggregationBuilder termsAgg = new TermsAggregationBuilder("keywords", ValueType.STRING)
                .field("keywords.raw")
                .size(20);
        sourceBuilder.aggregation(termsAgg);
        final SearchResponse searchResponse = elasticSearchDao.doSearch(projectIndexName, sourceBuilder);
        return getAggregations(searchResponse, "keywords");
    }

    private Map<String, Long> findOpportunityKeywords(BoolQueryBuilder boolQueryBuilder) throws IOException {
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.size(0);
        final TermsAggregationBuilder termsAgg = new TermsAggregationBuilder("opportunityKw", ValueType.STRING)
                .field("opportunityKw.raw")
                .size(20);
        sourceBuilder.aggregation(termsAgg);
        final SearchResponse searchResponse = elasticSearchDao.doSearch(opportunityIndexName, sourceBuilder);
        return getAggregations(searchResponse, "opportunityKw");
    }

    private Map<String, Long> getAggregations(SearchResponse searchResponse, String aggregationColumn){
        final Terms keywordsAggregation = searchResponse.getAggregations().get(aggregationColumn);
        final List<? extends Terms.Bucket> buckets = keywordsAggregation.getBuckets();
        final HashMap<String, Long> result = new HashMap<>();
        for (Terms.Bucket bucket : buckets) {
            result.put(bucket.getKeyAsString(), bucket.getDocCount());
        }
        return result;
    }

    @Override
    public Map<String, Long> findOrganizationProjectKeywords(Set<Long> organizationIds) throws IOException {
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(new TermsQueryBuilder("organizationIds", organizationIds));
        boolQueryBuilder.filter(new TermQueryBuilder("confidentiality", "PUBLIC"));
        return findProjectKeywords(boolQueryBuilder);
    }

    @Override
    public Map<String, Long> findOrganizationItemKeywords(Set<Long> organizationIds) throws IOException {
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(new TermsQueryBuilder("organizationIds", organizationIds));
        boolQueryBuilder.filter(new TermQueryBuilder("confidentiality", "PUBLIC"));
        return findItemKeywords(boolQueryBuilder);
    }

    @Override
    public Map<String, Long> findOrganizationOpportunityKeywords(Set<Long> organizationIds) throws IOException {
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(new TermsQueryBuilder("organizationIds", organizationIds));
        return findOpportunityKeywords(boolQueryBuilder);
    }

    @Override
    public Map<String, Long> suggestKeywords(Set<Long> organizationIds, Map<String, Long> searchHistory,
                                             Map<String, Long> widgetHistory, WidgetTab widgetTab) throws IOException {
        switch (widgetTab){
            case EXPERT:
                Map<String, Long> projectItemMap = findOrganizationKeywords(organizationIds);
                searchHistory.forEach((k, v) -> projectItemMap.merge(k, v, Long::sum));
                projectItemMap.entrySet().removeIf(t -> t.getKey().split(" ").length > 2);
                return projectItemMap;
            case LECTURE:
                break;
            case EQUIPMENT:
                break;
            case OPPORTUNITY:
                Map<String, Long> opportunityMap = findOrganizationOpportunityKeywords(organizationIds);
                widgetHistory.forEach((k, v) -> opportunityMap.merge(k, v, Long::sum));
                opportunityMap.entrySet().removeIf(t -> t.getKey().split(" ").length > 2);
                return opportunityMap;
            case RESEARCH_OUTCOME:
                Map<String, Long> itemMap = findOrganizationItemKeywords(organizationIds);
                widgetHistory.forEach((k, v) -> itemMap.merge(k, v, Long::sum));
                itemMap.entrySet().removeIf(t -> t.getKey().split(" ").length > 2);
                return itemMap;
            case ORGANIZATION_CHILDREN:
                break;
            case RESEARCH_PROJECT:
                Map<String, Long> projectMap = findOrganizationProjectKeywords(organizationIds);
                widgetHistory.forEach((k, v) -> projectMap.merge(k, v, Long::sum));
                projectMap.entrySet().removeIf(t -> t.getKey().split(" ").length > 2);
                return projectMap;
            case COMMERCIALIZATION_PROJECT:
                break;
        }
        return new HashMap<>();
    }
}
