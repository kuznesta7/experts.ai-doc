package ai.unico.platform.search.service;

import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.filter.ProjectFilter;
import ai.unico.platform.search.api.service.EvidenceExpertService;
import ai.unico.platform.search.api.service.EvidenceItemService;
import ai.unico.platform.search.api.service.EvidenceProjectService;
import ai.unico.platform.search.api.service.EvidenceSearchService;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class EvidenceSearchServiceImpl implements EvidenceSearchService {

    @Autowired
    private EvidenceExpertService evidenceExpertService;

    @Autowired
    private EvidenceItemService evidenceItemService;

    @Autowired
    private EvidenceProjectService evidenceProjectService;

    @Override
    public Map<String, Object> search(Set<Long> organizationIds, String query, UserContext userContext) throws IllegalAccessException, IOException, InstantiationException {
        final Map<String, Object> evidenceGlobalSearchWrapper = new HashMap<>();
        final EvidenceFilter evidenceFilter = new EvidenceFilter();
        evidenceFilter.setQuery(query);
        evidenceFilter.setOrganizationIds(organizationIds);
        evidenceFilter.setLimit(3);
        evidenceGlobalSearchWrapper.put("EVIDENCE_EXPERTS", evidenceExpertService.findAnalyticsExperts(evidenceFilter));
        evidenceGlobalSearchWrapper.put("EVIDENCE_ITEMS", evidenceItemService.findAnalyticsItems(evidenceFilter));

        final ProjectFilter projectFilter = new ProjectFilter();
        projectFilter.setOrganizationIds(organizationIds);
        projectFilter.setQuery(query);
        projectFilter.setLimit(3);
        evidenceGlobalSearchWrapper.put("EVIDENCE_PROJECTS", evidenceProjectService.findProjects(projectFilter));

        return evidenceGlobalSearchWrapper;
    }
}
