package ai.unico.platform.search.dao.impl;


import ai.unico.platform.recommendation.api.service.RecombeeManipulationService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.ElasticSearchModel;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.util.ServiceRegistryProxy;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.flush.FlushRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.*;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.*;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.reindex.UpdateByQueryRequest;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.*;

public class ElasticSearchDaoImpl implements ElasticSearchDao {

    @Value("${elasticsearch.host}")
    private String host;

    private RestHighLevelClient client;

    final RecombeeManipulationService recombeeManipulationService = ServiceRegistryProxy.createProxy(RecombeeManipulationService.class);

    public void init() {
        final HttpHost http = new HttpHost(host, 9200, "http");
        final RestClientBuilder builder = RestClient.builder(http);
        client = new RestHighLevelClient(builder);
    }

    public void destroy() throws IOException {
        client.close();
    }

    @Override
    public void initIndex(String indexName, String optionsJson) throws IOException {
        final Request request = new Request("PUT", indexName);
        request.setEntity(new NStringEntity(optionsJson, ContentType.APPLICATION_JSON));
        client.getLowLevelClient().performRequest(request);
    }


    @Override
    public void doIndex(String indexName, List<? extends ElasticSearchModel> items) throws IOException {
        final ObjectMapper objectMapper = new ObjectMapper();
        final BulkRequest bulkRequest = new BulkRequest();
        for (ElasticSearchModel item : items) {
            final String source = objectMapper.writeValueAsString(item);
            bulkRequest.add(new IndexRequest(indexName, baseDocumentTypeName, item.getId())
                    .source(source, XContentType.JSON));
        }
        client.bulk(bulkRequest, RequestOptions.DEFAULT);
    }

    @Override
    public <T extends ElasticSearchModel> void doIndex(String indexName, T item) throws IOException {
        final ObjectMapper objectMapper = new ObjectMapper();
        final String source = objectMapper.writeValueAsString(item);
        client.index(new IndexRequest(indexName, baseDocumentTypeName, item.getId()).source(source, XContentType.JSON), RequestOptions.DEFAULT);
    }

    @Override
    public <T> void doUpdate(String indexName, String fieldName, Map<String, T> valuesMap) throws IOException {
        if (valuesMap.isEmpty()) return;
        final BulkRequest bulkRequest = new BulkRequest();
        final Set<Map.Entry<String, T>> entries = valuesMap.entrySet();
        for (Map.Entry<String, T> entry : entries) {
            final Map<String, T> valueMap = new HashMap<>();
            valueMap.put(fieldName, entry.getValue());
            final UpdateRequest request = new UpdateRequest(indexName, baseDocumentTypeName, entry.getKey()).doc(valueMap);
            bulkRequest.add(request);
        }
        client.bulk(bulkRequest, RequestOptions.DEFAULT);
    }

    @Override
    public void doUpdate(UpdateByQueryRequest updateByQueryRequest) throws IOException {
        updateByQueryRequest.setConflicts("proceed");
        updateByQueryRequest.setDocTypes(baseDocumentTypeName);
        client.updateByQuery(updateByQueryRequest, RequestOptions.DEFAULT);
    }

    @Override
    public void doIndexObjects(String indexName, List items) throws IOException {
        final ObjectMapper objectMapper = new ObjectMapper();
        final BulkRequest bulkRequest = new BulkRequest();
        for (Object item : items) {
            bulkRequest.add(new IndexRequest(indexName, baseDocumentTypeName)
                    .source(objectMapper.writeValueAsString(item), XContentType.JSON));
        }
        client.bulk(bulkRequest, RequestOptions.DEFAULT);
    }

    @Override
    public void deleteDocument(String indexName, String documentId) throws IOException {
        client.delete(new DeleteRequest(indexName, baseDocumentTypeName, documentId), RequestOptions.DEFAULT);
    }

    @Override
    public void deleteDocuments(String indexName, Collection<String> documentIds) throws IOException {
        final BulkRequest bulkRequest = new BulkRequest();
        for (String documentId : documentIds) {
            bulkRequest.add(new DeleteRequest(indexName, baseDocumentTypeName, documentId));
        }
        client.bulk(bulkRequest, RequestOptions.DEFAULT);
    }

    @Override
    public void deleteIndex(String indexName) throws IOException {
        client.indices().delete(new DeleteIndexRequest(indexName), RequestOptions.DEFAULT);
    }

    @Override
    public SearchHits doSearchAndGetHits(String indexName, SearchSourceBuilder sourceBuilder) throws IOException {
        final SearchResponse searchResponse = doSearch(indexName, sourceBuilder);
        return searchResponse.getHits();
    }

    @Override
    public SearchResponse doSearch(String indexName, SearchSourceBuilder sourceBuilder) throws IOException {
        final SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(indexName);
        searchRequest.types(baseDocumentTypeName);
        searchRequest.source(sourceBuilder);
        return client.search(searchRequest, RequestOptions.DEFAULT);
    }

    @Override
    public <T> SearchResponseEntityWrapper<T> doSearchAndGetHits(String indexName, SearchSourceBuilder searchSourceBuilder, Class<T> resultClass) throws IOException {
        final SearchHits searchHits = doSearchAndGetHits(indexName, searchSourceBuilder);
        final SearchResponseEntityWrapper<T> searchResponseEntityWrapper = new SearchResponseEntityWrapper<>();

        searchResponseEntityWrapper.setResultCount(searchHits.getTotalHits());
        searchResponseEntityWrapper.setMaxScore(searchHits.getMaxScore());

        final ObjectMapper objectMapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        for (SearchHit searchHit : searchHits) {
            final String sourceAsString = searchHit.getSourceAsString();
            final T source = objectMapper.readValue(sourceAsString, resultClass);
            final SearchResponseEntity<T> searchResponseEntity = new SearchResponseEntity<>();
            searchResponseEntity.setSource(source);
            searchResponseEntity.setScore(searchHit.getScore());
            searchResponseEntityWrapper.getSearchResponseEntities().add(searchResponseEntity);
        }
        return searchResponseEntityWrapper;
    }

    @Override
    public GetResponse getDocument(String indexName, String id) throws IOException {
        final GetRequest getRequest = new GetRequest();
        getRequest.id(id).index(indexName);
        return client.get(getRequest, RequestOptions.DEFAULT);
    }

    @Override
    public <T> T getDocument(String indexName, String id, Class<T> resultClass) throws IOException {
        final GetResponse document = getDocument(indexName, id);
        return convertToEntity(document, resultClass);
    }

    @Override
    public MultiGetItemResponse[] getDocuments(String indexName, Set<String> ids) throws IOException {
        final MultiGetRequest multiGetRequest = new MultiGetRequest();
        for (String id : ids) {
            multiGetRequest.add(indexName, baseDocumentTypeName, id);
        }
        final MultiGetResponse mget = client.mget(multiGetRequest, RequestOptions.DEFAULT);
        return mget.getResponses();
    }

    @Override
    public <T> List<T> getDocuments(String indexName, Collection<String> ids, Class<T> resultClass) throws IOException {
        final List<T> results = new ArrayList<>();
        if (ids.isEmpty()) return results;
        final MultiGetItemResponse[] documents = getDocuments(indexName, new HashSet<>(ids));
        for (MultiGetItemResponse document : documents) {
            results.add(convertToEntity(document.getResponse(), resultClass));
        }
        return results;
    }

    @Override
    public void renameIndex(String currentName, String newName) throws IOException {
        final Request request = new Request("POST", "/_reindex");
        final String s = "{\"source\": {\"index\": \"" + currentName + "\"},\"dest\": {\"index\": \"" + newName + "\"}}";
        request.setEntity(new NStringEntity(s));
        client.getLowLevelClient().performRequest(request);
        deleteIndex(currentName);
    }

    @Override
    public void flush(String indexName) throws IOException {
        final RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();
        builder.addHeader("wait_if_ongoing", "true");
        builder.addHeader("ignore_unavailable", "true");
        client.indices().flush(indexName == null ? new FlushRequest() : new FlushRequest(indexName), builder.build());
    }

    private <T> T convertToEntity(GetResponse getResponse, Class<T> resultClass) throws IOException {
        if (!getResponse.isExists()) return null;
        final String sourceAsString = getResponse.getSourceAsString();
        final ObjectMapper objectMapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper.readValue(sourceAsString, resultClass);
    }
}
