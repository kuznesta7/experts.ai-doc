package ai.unico.platform.search.service;

import ai.unico.platform.search.api.service.ExpertIndexService;
import ai.unico.platform.search.api.service.ItemIndexService;
import ai.unico.platform.search.api.service.ProjectIndexService;
import ai.unico.platform.search.api.service.PublicOrganizationStatisticsService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Expert;
import ai.unico.platform.search.model.Item;
import ai.unico.platform.search.model.Project;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.util.enums.Confidentiality;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Collection;

public class PublicOrganizationStatisticsServiceImpl implements PublicOrganizationStatisticsService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Override
    public PublicOrganizationStatisticsDto getPublicOrganizationStatistics(Collection<Long> organizationIds) throws IOException {
        final SearchSourceBuilder expertsSourceBuilder = prepareExpertsSourceBuilder(organizationIds);
        final SearchResponseEntityWrapper<Expert> expertsWrapper = elasticSearchDao.doSearchAndGetHits(ExpertIndexService.indexName, expertsSourceBuilder, Expert.class);
        final SearchSourceBuilder outcomesSourceBuilder = prepareOutcomesSourceBuilder(organizationIds);
        final SearchResponseEntityWrapper<Item> outcomeWrapper = elasticSearchDao.doSearchAndGetHits(ItemIndexService.indexName, outcomesSourceBuilder, Item.class);
        final SearchSourceBuilder projectsSourceBuilder = prepareProjectsSourceBuilder(organizationIds);
        final SearchResponseEntityWrapper<Project> projectWrapper = elasticSearchDao.doSearchAndGetHits(ProjectIndexService.indexName, projectsSourceBuilder, Project.class);
        final PublicOrganizationStatisticsDto result = new PublicOrganizationStatisticsDto();
        result.setNumberOfExperts(expertsWrapper.getResultCount());
        result.setNumberOfOutcomes(outcomeWrapper.getResultCount());
        result.setNumberOfProjects(projectWrapper.getResultCount());
        return result;
    }

    private SearchSourceBuilder prepareExpertsSourceBuilder(Collection<Long> organizationIds) {
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder query = new BoolQueryBuilder();
        query.filter(new TermsQueryBuilder("organizationIds", organizationIds));
        sourceBuilder.query(query);
        sourceBuilder.size(0);
        return sourceBuilder;
    }

    private SearchSourceBuilder prepareOutcomesSourceBuilder(Collection<Long> organizationIds) {
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder query = new BoolQueryBuilder();
        query.filter(new TermsQueryBuilder("organizationIds", organizationIds));
        query.filter(new TermQueryBuilder("confidentiality", Confidentiality.PUBLIC));
        sourceBuilder.query(query);
        sourceBuilder.size(0);
        return sourceBuilder;
    }

    private SearchSourceBuilder prepareProjectsSourceBuilder(Collection<Long> organizationIds) {
        return prepareOutcomesSourceBuilder(organizationIds); //the same for now
    }
}
