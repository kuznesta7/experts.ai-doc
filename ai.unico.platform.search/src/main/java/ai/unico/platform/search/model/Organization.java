package ai.unico.platform.search.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class Organization extends ElasticSearchModel {

    private Long organizationId;

    private Long originalOrganizationId;

    private Long substituteOrganizationId;

    private String organizationName;

    private String organizationAbbrev;

    private Set<Long> childrenOrganizationIds;

    private Set<Long> affiliatedOrganizationIds;

    private Set<Long> memberOrganizationIds;

    private Long rank;

    private String countryCode;

    private Boolean allowedSearch;

    private Boolean allowedMembers;

    private Long organizationTypeId;

    private Boolean isSearchable;

    private List<OrganizationLicence> organizationLicences = new ArrayList<>();

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Long getOriginalOrganizationId() {
        return originalOrganizationId;
    }

    public void setOriginalOrganizationId(Long originalOrganizationId) {
        this.originalOrganizationId = originalOrganizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationAbbrev() {
        return organizationAbbrev;
    }

    public void setOrganizationAbbrev(String organizationAbbrev) {
        this.organizationAbbrev = organizationAbbrev;
    }

    public Long getRank() {
        return rank;
    }

    public void setRank(Long rank) {
        this.rank = rank;
    }

    public Long getSubstituteOrganizationId() {
        return substituteOrganizationId;
    }

    public void setSubstituteOrganizationId(Long substituteOrganizationId) {
        this.substituteOrganizationId = substituteOrganizationId;
    }

    public List<OrganizationLicence> getOrganizationLicences() {
        return organizationLicences;
    }

    public void setOrganizationLicences(List<OrganizationLicence> organizationLicences) {
        this.organizationLicences = organizationLicences;
    }

    public Set<Long> getMemberOrganizationIds() {
        return memberOrganizationIds;
    }

    public void setMemberOrganizationIds(Set<Long> memberOrganizationIds) {
        this.memberOrganizationIds = memberOrganizationIds;
    }

    public Boolean getAllowedMembers() {
        return allowedMembers;
    }

    public void setAllowedMembers(Boolean allowedMembers) {
        this.allowedMembers = allowedMembers;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Set<Long> getChildrenOrganizationIds() {
        return childrenOrganizationIds;
    }

    public void setChildrenOrganizationIds(Set<Long> childrenOrganizationIds) {
        this.childrenOrganizationIds = childrenOrganizationIds;
    }

    public Set<Long> getAffiliatedOrganizationIds() {
        return affiliatedOrganizationIds;
    }

    public void setAffiliatedOrganizationIds(Set<Long> affiliatedOrganizationIds) {
        this.affiliatedOrganizationIds = affiliatedOrganizationIds;
    }

    public Boolean getAllowedSearch() {
        return allowedSearch;
    }

    public void setAllowedSearch(Boolean allowedSearch) {
        this.allowedSearch = allowedSearch;
    }

    public Long getOrganizationTypeId() {
        return organizationTypeId;
    }

    public void setOrganizationTypeId(Long organizationTypeId) {
        this.organizationTypeId = organizationTypeId;
    }

    public Boolean getSearchable() {
        return isSearchable;
    }

    public void setSearchable(Boolean searchable) {
        isSearchable = searchable;
    }

    public static class OrganizationLicence {

        private Long organizationLicenceId;

        private String licenceRole;

        private Date validUntil;

        public Long getOrganizationLicenceId() {
            return organizationLicenceId;
        }

        public void setOrganizationLicenceId(Long organizationLicenceId) {
            this.organizationLicenceId = organizationLicenceId;
        }

        public String getLicenceRole() {
            return licenceRole;
        }

        public void setLicenceRole(String licenceRole) {
            this.licenceRole = licenceRole;
        }

        public Date getValidUntil() {
            return validUntil;
        }

        public void setValidUntil(Date validUntil) {
            this.validUntil = validUntil;
        }
    }
}
