package ai.unico.platform.search.service;

import ai.unico.platform.extdata.dto.ExpertThesisPreviewDto;
import ai.unico.platform.extdata.service.DWHThesisService;
import ai.unico.platform.search.api.service.SearchMappingsLoaderService;
import ai.unico.platform.search.api.service.ThesisIndexService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Thesis;
import ai.unico.platform.search.util.ExpertUtil;
import ai.unico.platform.search.util.ThesisUtil;
import ai.unico.platform.store.api.dto.ItemIndexDto;
import ai.unico.platform.store.api.enums.IndexHistoryStatus;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.api.service.IndexHistoryService;
import ai.unico.platform.store.api.service.OrganizationService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class ThesisIndexServiceImpl implements ThesisIndexService {

    private boolean indexingInProgress = false;

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private SearchMappingsLoaderService searchMappingsLoaderService;

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;

    @Override
    public void reindexThesis(Integer limit, boolean clean, String target, UserContext userContext) throws IOException {
        final String mappingJson = searchMappingsLoaderService.loadMappings("thesis");
        if (mappingJson == null) throw new FileNotFoundException();

        if (indexingInProgress) return;

        if (clean) {
            try {
                elasticSearchDao.deleteIndex(indexName);
            } catch (Exception ignore) {
            }
            elasticSearchDao.initIndex(indexName, mappingJson);
            System.out.println("creating index " + indexName + " with mappings:");
        }
        indexingInProgress = true;
        final IndexHistoryService indexHistoryService = ServiceRegistryUtil.getService(IndexHistoryService.class);
        final Long indexHistoryId = indexHistoryService.saveIndexHistory(IndexTarget.THESIS, true, userContext);
        try {

            if (target == null || "ALL".equals(target) || "DWH".equals(target)) {
                final DWHThesisService dwhThesisService = ServiceRegistryUtil.getService(DWHThesisService.class);
                List<ExpertThesisPreviewDto> itemDtos;
                Integer offset = 0;
                System.out.println("Starting DWH thesis index");

                final OrganizationService organizationService = ServiceRegistryUtil.getService(OrganizationService.class);
                do {
                    itemDtos = dwhThesisService.findThesisPreviews(limit, offset);
                    int size = itemDtos.size();
                    if (size == 0) {
                        break;
                    }
                    if (size % 10 !=0)
                        size += 10; //index last items when size%10 != 0
                    final Set<String> expertCodes = new HashSet<>();
                    itemDtos.stream()
                            .map(ExpertThesisPreviewDto::getExpertIds)
                            .forEach(x -> x.values().stream().map(ExpertUtil::convertToExpertCode).forEach(expertCodes::add));
                    final Map<String, String> substituteExpertCodes = ExpertUtil.findSubstituteExpertCodes(expertCodes);
                    for (int i = 0; i < 10; i++) {
                        final List<Thesis> items = new ArrayList<>();
                        final int start = i * (size / 10);
                        final int end = start + size / 10;
                        final Set<Long> originalOrganizationIds = new HashSet<>();
                        itemDtos.stream().map(ExpertThesisPreviewDto::getOriginalOrganizationIds).forEach(originalOrganizationIds::addAll);
                        final Map<Long, Long> organizationMap = organizationService.findOrganizationIdsMap(originalOrganizationIds);
                        for (ExpertThesisPreviewDto expertThesisPreviewDto : itemDtos.subList(start, Math.min(end,itemDtos.size()))) {

                            final Thesis item = ThesisUtil.convert(expertThesisPreviewDto);
                            for (Long originalOrganizationId : expertThesisPreviewDto.getOriginalOrganizationIds()) {
                                final Long organizationId = organizationMap.get(originalOrganizationId);
                                item.setUniversityId(organizationId);
                                if (organizationId != null) item.getOrganizationIds().add(organizationId);
                            }
                            item.getEvidenceOrganizationIds().addAll(item.getOrganizationIds());
                            item.setAssignee(substituteExpertCodes.getOrDefault(item.getAssignee(), item.getAssignee()));
                            item.setReviewer(substituteExpertCodes.getOrDefault(item.getReviewer(), item.getReviewer()));
                            item.setSupervisor(substituteExpertCodes.getOrDefault(item.getSupervisor(), item.getSupervisor()));
                            items.add(item);
                        }
                        elasticSearchDao.doIndex(indexName, items);
                    }
                    offset += limit;
                    System.out.println(offset);
                } while (itemDtos.size() > 0);

                System.out.println("DWH thesis done.");
            }

            //todo store index

            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FINISHED, userContext);
        } catch (Exception e) {
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FAILED, e.getCause().toString(), userContext);
            e.printStackTrace();
        }
        indexingInProgress = false;
    }

    @Override
    public void indexThesis(ItemIndexDto itemIndexDto) throws IOException {
        throw new NotImplementedException();
    }

    @Override
    public void replaceExperts(Map<String, String> replacementMap, boolean originalOnly) throws IOException {
        throw new NotImplementedException();
    }

    @Override
    public void flush() throws IOException {
        elasticSearchDao.flush(null);
    }
}
