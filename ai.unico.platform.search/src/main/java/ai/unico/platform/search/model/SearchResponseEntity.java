package ai.unico.platform.search.model;

public class SearchResponseEntity<T> {

    private Float score;

    private T source;

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }

    public T getSource() {
        return source;
    }

    public void setSource(T source) {
        this.source = source;
    }
}
