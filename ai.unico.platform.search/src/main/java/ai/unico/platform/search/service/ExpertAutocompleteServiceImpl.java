package ai.unico.platform.search.service;

import ai.unico.platform.search.api.dto.UserExpertPreviewDto;
import ai.unico.platform.search.api.filter.ExpertLookupFilter;
import ai.unico.platform.search.api.service.ExpertAutocompleteService;
import ai.unico.platform.search.api.service.OrganizationIndexService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Expert;
import ai.unico.platform.search.model.Organization;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.util.dto.OrganizationDto;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.*;

public class ExpertAutocompleteServiceImpl implements ExpertAutocompleteService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Override
    public List<UserExpertPreviewDto> findUserExpertPreviews(ExpertLookupFilter filter) throws IOException {
        final Set<Long> organizationIds = filter.getOrganizationIds();
        final Set<Long> affiliatedOrganizationIds = filter.getAffiliatedOrganizationIds();

        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder query = new BoolQueryBuilder();
        if (filter.getQuery() != null && !filter.getQuery().isEmpty()) {
            query.must(new MultiMatchQueryBuilder(filter.getQuery(), "fullName", "email", "username"));
        }
        if (!organizationIds.isEmpty()) {
            query.filter(new TermsQueryBuilder("organizationIds", organizationIds));
        }
        if (!affiliatedOrganizationIds.isEmpty()) {
            final BoolQueryBuilder organizationQuery = new BoolQueryBuilder();
            organizationQuery.should(new TermsQueryBuilder("organizationIds", affiliatedOrganizationIds));
            organizationQuery.should(new TermsQueryBuilder("affiliatedOrganizationIds", affiliatedOrganizationIds));
            organizationQuery.minimumShouldMatch(1);
            query.filter(organizationQuery);
        }
        if (!filter.getDisallowedOrganizationIds().isEmpty()) {
            query.mustNot(new TermsQueryBuilder("organizationIds", filter.getDisallowedOrganizationIds()));
        }
        if (!filter.getUserIds().isEmpty()) {
            query.filter(new TermsQueryBuilder("userId", filter.getUserIds()));
        }
        if (!filter.getDisallowedUserIds().isEmpty()) {
            query.mustNot(new TermsQueryBuilder("userId", filter.getDisallowedUserIds()));
        }
        if (!filter.getExpertIds().isEmpty()) {
            query.filter(new TermsQueryBuilder("expertIds", filter.getExpertIds()));
        }
        if (!filter.getDisallowedExpertIds().isEmpty()) {
            query.mustNot(new TermsQueryBuilder("expertIds", filter.getDisallowedExpertIds()));
        }
        if (filter.isUsersOnly()) {
            query.filter(new ExistsQueryBuilder("userId"));
            query.mustNot(new TermQueryBuilder("claimable", true));
            query.minimumShouldMatch(0);
        }
        if (!filter.getExpertCodes().isEmpty()) {
            query.filter(new TermsQueryBuilder("expertCode", filter.getExpertCodes()));
        }
        if (!filter.getDisallowedExpertCodes().isEmpty()) {
            query.mustNot(new TermsQueryBuilder("expertCode", filter.getExpertCodes()));
        }
        query.should(new ExistsQueryBuilder("organizationIds").boost(5));
        query.mustNot(new ExistsQueryBuilder("claimedById"));
        query.should(new ExistsQueryBuilder("email").boost(5));
        searchSourceBuilder.query(query);
        final SearchResponseEntityWrapper<Expert> responseWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Expert.class);
        final List<SearchResponseEntity<Expert>> searchResponseEntities = responseWrapper.getSearchResponseEntities();
        final List<UserExpertPreviewDto> userExpertPreviewDtos = new ArrayList<>();
        final Set<String> organizationsToLoadIds = new HashSet<>();
        for (SearchResponseEntity<Expert> searchResponseEntity : searchResponseEntities) {
            final Expert source = searchResponseEntity.getSource();
            source.getOrganizationIds().stream().filter(Objects::nonNull).forEach(x -> organizationsToLoadIds.add(x.toString()));
        }
        final Map<Long, Organization> organizationMap = new HashMap<>();
        if (!organizationsToLoadIds.isEmpty()) {
            final List<Organization> organizations = elasticSearchDao.getDocuments(OrganizationIndexService.indexName, organizationsToLoadIds, Organization.class);
            organizations.stream().filter(Objects::nonNull).forEach(x -> organizationMap.put(x.getOrganizationId(), x));
        }
        for (SearchResponseEntity<Expert> searchResponseEntity : searchResponseEntities) {
            final Expert source = searchResponseEntity.getSource();
            final UserExpertPreviewDto userExpertPreviewDto = new UserExpertPreviewDto();

            userExpertPreviewDto.setName(source.getFullName());
            userExpertPreviewDto.setExpertCode(source.getExpertCode());
            userExpertPreviewDto.setUsername(source.getUsername());
            userExpertPreviewDto.setEmail(source.getEmail());
            userExpertPreviewDto.setClaimable(source.isClaimable());

            if (source.getUserId() != null) {
                userExpertPreviewDto.setUserId(source.getUserId());
            } else if (source.getExpertIds() != null && source.getExpertIds().size() == 1) {
                final Long expertId = source.getExpertIds().iterator().next();
                userExpertPreviewDto.setExpertId(expertId);
            }

            for (Long organizationId : source.getOrganizationIds()) {
                final Organization organization = organizationMap.get(organizationId);
                if (organization != null) {
                    userExpertPreviewDto.getOrganizationDtos().add(convertOrganization(organization));
                }
            }
            userExpertPreviewDtos.add(userExpertPreviewDto);
        }
        return userExpertPreviewDtos;
    }

    private OrganizationDto convertOrganization(Organization organization) {
        final OrganizationDto organizationDto = new OrganizationDto();
        if (organization == null) return organizationDto;
        organizationDto.setOrganizationId(organization.getOrganizationId());
        organizationDto.setOrganizationName(organization.getOrganizationName());
        organizationDto.setGdpr(organization.getSearchable());
        return organizationDto;
    }
}
