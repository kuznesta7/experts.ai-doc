package ai.unico.platform.search.util;

import ai.unico.platform.extdata.dto.ExpertItemPreviewDto;
import ai.unico.platform.search.api.dto.EvidenceItemPreviewWithExpertsDto;
import ai.unico.platform.search.api.dto.ItemLookupPreviewDto;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.model.Item;
import ai.unico.platform.store.api.dto.ItemIndexDto;
import ai.unico.platform.store.api.dto.ItemTypeSearchDto;
import ai.unico.platform.store.api.service.ItemTypeService;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.KeywordsUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.ScoreSortBuilder;
import org.elasticsearch.search.sort.ScriptSortBuilder;
import org.elasticsearch.search.sort.SortOrder;

import java.util.*;
import java.util.stream.Collectors;

public class ItemUtil {

    public static Item convert(ExpertItemPreviewDto expertItemPreviewDto) {
        final Item item = new Item();
        item.setId(convertToDWHItemCode(expertItemPreviewDto.getItemId()));
        item.setOriginalItemId(expertItemPreviewDto.getItemId());
        item.setItemBk(expertItemPreviewDto.getItemBk());
        item.setItemTypeId(expertItemPreviewDto.getItemTypeId());
        item.setItemName(expertItemPreviewDto.getItemName());
        item.setItemDescription(expertItemPreviewDto.getItemDescription());
        item.setYear(expertItemPreviewDto.getYear());
        item.setTranslationUnavailable(expertItemPreviewDto.isTranslationUnavailable());
        item.setKeywords(KeywordsUtil.sortKeywordList(expertItemPreviewDto.getKeywords().stream().distinct().collect(Collectors.toList())));
        item.setConfidentiality(expertItemPreviewDto.isConfidential() ? Confidentiality.CONFIDENTIAL : Confidentiality.PUBLIC);
        item.setDoi(expertItemPreviewDto.getDoi());
        final Set<String> expertCodes = expertItemPreviewDto.getExpertIds().stream()
                .map(IdUtil::convertToExtDataCode)
                .collect(Collectors.toSet());
        item.getExpertCodes().addAll(expertCodes);
        item.getAllExpertCodes().addAll(expertCodes);
        item.getOriginalExpertCodes().addAll(expertCodes);
        return item;
    }

    public static Item convert(ItemIndexDto itemIndexDto) {
        final Item item = new Item();
        item.setId(convertToItemCode(itemIndexDto.getItemId()));
        item.setOriginalItemId(itemIndexDto.getItemId());
        item.setItemId(itemIndexDto.getItemId());
        item.setItemTypeId(itemIndexDto.getItemTypeId());
        item.setItemName(itemIndexDto.getItemName());
        item.setItemDescription(itemIndexDto.getItemDescription());
        item.setYear(itemIndexDto.getYear());
        item.setKeywords(KeywordsUtil.sortKeywordList(itemIndexDto.getKeywords().stream().distinct().collect(Collectors.toList())));
        item.setOrganizationIds(itemIndexDto.getOrganizationIds());
        item.setVerifiedOrganizationIds(itemIndexDto.getVerifiedOrganizationIds());
        item.setActiveOrganizationIds(itemIndexDto.getActiveOrganizationIds());
        item.setConfidentiality(itemIndexDto.getConfidentiality());
        item.setOwnerUserId(itemIndexDto.getOwnerUserId());
        item.setOwnerOrganizationId(itemIndexDto.getOwnerOrganizationId());
        item.setNotRegisteredExpertsIds(itemIndexDto.getNotRegisteredExpertIds());

        item.getExpertCodes().addAll(itemIndexDto.getActiveExpertIds().stream()
                .map(IdUtil::convertToExtDataCode)
                .collect(Collectors.toSet()));
        item.getExpertCodes().addAll(itemIndexDto.getActiveUserIds().stream()
                .map(IdUtil::convertToIntDataCode)
                .collect(Collectors.toSet()));

        item.getAllExpertCodes().addAll(itemIndexDto.getAllExpertIds().stream()
                .map(IdUtil::convertToExtDataCode)
                .collect(Collectors.toSet()));
        item.getAllExpertCodes().addAll(itemIndexDto.getAllUserIds().stream()
                .map(IdUtil::convertToIntDataCode)
                .collect(Collectors.toSet()));

        item.getFavoriteExpertCodes().addAll(itemIndexDto.getFavoriteUserIds().stream()
                .map(IdUtil::convertToIntDataCode)
                .collect(Collectors.toSet()));
        item.getOriginalExpertCodes().addAll(item.getAllExpertCodes());

        if (Confidentiality.SECRET.equals(itemIndexDto.getConfidentiality())) {
            item.getEvidenceOrganizationIds().add(itemIndexDto.getOwnerOrganizationId());
        } else {
            item.getEvidenceOrganizationIds().addAll(itemIndexDto.getOrganizationIds());
        }
        return item;
    }

    public static String convertToDWHItemCode(Long originalItemId) {
        return IdUtil.convertToExtDataCode(originalItemId);
    }

    public static String convertToItemCode(Long itemId) {
        return IdUtil.convertToIntDataCode(itemId);
    }

    public static void addItemSortFunction(SearchSourceBuilder searchSourceBuilder, ExpertSearchResultFilter.Order orderBy, String expertCode) {
        searchSourceBuilder.trackScores(true);
        if (orderBy.equals(ExpertSearchResultFilter.Order.YEAR_DESC)) {
            searchSourceBuilder.sort("year", SortOrder.DESC);
        } else if (orderBy.equals(ExpertSearchResultFilter.Order.YEAR_ASC)) {
            searchSourceBuilder.sort("year", SortOrder.ASC);
        }
        searchSourceBuilder.sort(new ScoreSortBuilder().order(SortOrder.DESC));
        final ItemTypeService itemTypeService = ServiceRegistryUtil.getService(ItemTypeService.class);
        final List<ItemTypeSearchDto> itemTypesSearch = itemTypeService.findItemTypesSearch();
        final Map<String, Object> sortScriptParams = new HashMap<>();
        final Map<String, Float> scoresMap = new HashMap<>();
        sortScriptParams.put("scores", scoresMap);
        sortScriptParams.put("expertCode", expertCode);
        sortScriptParams.put("currentYear", Calendar.getInstance().get(Calendar.YEAR));
        for (ItemTypeSearchDto itemType : itemTypesSearch) {
            scoresMap.put(itemType.getTypeId().toString(), itemType.getWeight());
        }

        if (expertCode != null) {
            final Script favoriteSortScript = new Script(ScriptType.INLINE, "painless", "if (doc['favoriteExpertCodes'].contains(params.expertCode)) {return 1;} return 0;", sortScriptParams);
            searchSourceBuilder.sort(new ScriptSortBuilder(favoriteSortScript, ScriptSortBuilder.ScriptSortType.NUMBER).order(SortOrder.DESC));
        }

        if (orderBy.equals(ExpertSearchResultFilter.Order.YEAR_DESC) || orderBy.equals(ExpertSearchResultFilter.Order.YEAR_ASC)) {
            final Script sortScript = new Script(ScriptType.INLINE, "painless", "if(params.scores.containsKey(doc['itemTypeId'].value.toString())) { return params.scores[doc['itemTypeId'].value.toString()];} return 0;", sortScriptParams);
            searchSourceBuilder.sort(new ScriptSortBuilder(sortScript, ScriptSortBuilder.ScriptSortType.NUMBER).order(SortOrder.DESC));
        } else {
            final Script sortScript = new Script(ScriptType.INLINE, "painless", "if(params.scores.containsKey(doc['itemTypeId'].value.toString())) { return params.scores[doc['itemTypeId'].value.toString()] * Math.pow(1.05,doc['year'].value - params.currentYear);} return 0;", sortScriptParams);
            searchSourceBuilder.sort(new ScriptSortBuilder(sortScript, ScriptSortBuilder.ScriptSortType.NUMBER).order(SortOrder.DESC));
        }
    }

    public static ItemLookupPreviewDto convertToLookupPreview(Item document) {
        final ItemLookupPreviewDto itemLookupPreviewDto = new ItemLookupPreviewDto();
        itemLookupPreviewDto.setItemId(document.getItemId());
        itemLookupPreviewDto.setOriginalItemId(document.getOriginalItemId());
        itemLookupPreviewDto.setItemName(document.getItemName());
        itemLookupPreviewDto.setYear(document.getYear());
        itemLookupPreviewDto.setItemTypeId(document.getItemTypeId());
        itemLookupPreviewDto.setItemCode(document.getId());
        return itemLookupPreviewDto;
    }

    public static EvidenceItemPreviewWithExpertsDto convertToEvidencePreview(Item source) {
        final EvidenceItemPreviewWithExpertsDto itemDto = new EvidenceItemPreviewWithExpertsDto();
        itemDto.setItemCode(source.getId());
        itemDto.setItemName(source.getItemName());
        itemDto.setItemId(source.getItemId());
        itemDto.setItemLink(source.getItemLink());
        itemDto.setDoi(source.getDoi());
        itemDto.setOriginalItemId(source.getOriginalItemId());
        itemDto.setItemBk(source.getItemBk());
        itemDto.setItemTypeId(source.getItemTypeId());
        itemDto.setKeywords(source.getKeywords());
        itemDto.setItemDescription(source.getItemDescription());
        itemDto.setYear(source.getYear());
        return itemDto;
    }
}
