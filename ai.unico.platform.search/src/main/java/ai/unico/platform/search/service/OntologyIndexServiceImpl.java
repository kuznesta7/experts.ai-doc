package ai.unico.platform.search.service;

import ai.unico.platform.extdata.dto.OntologyItemDto;
import ai.unico.platform.extdata.service.OntologyService;
import ai.unico.platform.search.api.service.OntologyIndexService;
import ai.unico.platform.search.api.service.SearchMappingsLoaderService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.store.api.enums.IndexHistoryStatus;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.api.service.IndexHistoryService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

public class OntologyIndexServiceImpl implements OntologyIndexService {

    @Autowired
    private SearchMappingsLoaderService searchMappingsLoaderService;

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    private boolean indexingInProgress = false;

    @Override
    public void reindexOntology(UserContext userContext) throws IOException {
        if (indexingInProgress) return;
        final String content = searchMappingsLoaderService.loadMappings("ontology");
        if (content == null) throw new FileNotFoundException();
        indexingInProgress = true;
        final IndexHistoryService indexHistoryService = ServiceRegistryUtil.getService(IndexHistoryService.class);
        final Long indexHistoryId = indexHistoryService.saveIndexHistory(IndexTarget.ONTOLOGY, true, userContext);
        try {
            try {
                elasticSearchDao.deleteIndex(indexName);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            elasticSearchDao.initIndex(indexName, content);

            final OntologyService ontologyService = ServiceRegistryUtil.getService(OntologyService.class);
            int i = 0;
            final int limit = 100000;
            while (true) {
                final Set<OntologyItemDto> ontologyItems = ontologyService.getOntologyItems(i, limit);
                if (ontologyItems.isEmpty()) {
                    break;
                }
                elasticSearchDao.doIndexObjects(indexName, new ArrayList<>(ontologyItems));
                i += limit;
                System.out.println(i);
            }
            System.out.println("ontology index done");
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FINISHED, userContext);
        } catch (Exception e) {
            e.printStackTrace();
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FAILED, e.getCause().toString(), userContext);
        }
        indexingInProgress = false;
    }

}
