package ai.unico.platform.search.service;

import ai.unico.platform.recommendation.api.dto.RecommendationWrapperDto;
import ai.unico.platform.recommendation.api.enums.RecombeeScenario;
import ai.unico.platform.recommendation.api.service.RecombeeRecommendationService;
import ai.unico.platform.search.api.dto.OpportunityPreviewDto;
import ai.unico.platform.search.api.filter.OpportunityFilter;
import ai.unico.platform.search.api.service.ExpertLookupService;
import ai.unico.platform.search.api.service.OpportunitySearchService;
import ai.unico.platform.search.api.service.OrganizationSearchService;
import ai.unico.platform.search.api.wrapper.OpportunityWrapper;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Opportunity;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.RecombeeFilterConverterUtil;
import ai.unico.platform.store.api.dto.*;
import ai.unico.platform.util.ServiceRegistryProxy;
import org.apache.commons.lang.ArrayUtils;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OpportunitySearchServiceImpl implements OpportunitySearchService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private ExpertLookupService expertLookupService;

    @Autowired
    private OrganizationSearchService organizationSearchService;

    private final RecombeeRecommendationService recombeeRecommendationService = ServiceRegistryProxy.createProxy(RecombeeRecommendationService.class);

    private BoolQueryBuilder setupFilters(OpportunityFilter filter) {
        final BoolQueryBuilder query = new BoolQueryBuilder();
        if (filter.getJobType() != null)
            query.filter(new TermQueryBuilder("jobTypes", filter.getJobType()));
        if (filter.getOpportunityType() != null)
            query.filter(new TermQueryBuilder("opportunityType", filter.getOpportunityType()));
        if (!filter.getOrganizationIds().isEmpty())
            query.filter(new TermsQueryBuilder("organizationIds", filter.getOrganizationIds()));
        if (!filter.getIncludeHidden()) {
            query.filter(new TermQueryBuilder("hidden", false));
        }
        return query;
    }

    @Override
    public OpportunityWrapper findOpportunitiesWithRecommendation(OpportunityFilter filter, OrganizationDetailDto organizationDetailDto) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder query = setupFilters(filter);
        OpportunityWrapper wrapper = new OpportunityWrapper();
        RecommendationWrapperDto recommendationWrapperDto = wrapper.getRecommendationWrapperDto();

        String[] recombeeIds;
        try {
            recombeeIds = recombeeRecommendationService.recommend(organizationDetailDto, RecombeeFilterConverterUtil.convert(filter), RecombeeScenario.OPPORTUNITY, recommendationWrapperDto);
        } catch (RuntimeException e ){
            System.out.println("Alternative no-recommendation");
            return findOpportunities(filter);
        }
        if (recombeeIds.length == 0)
            return findOpportunities(filter);
        // temporary check due to inability to delete some rows from Recombee
        List<String> tmpIds = new ArrayList<>();
        for(String id: recombeeIds) {
            if (id.startsWith("DWH") || id.startsWith("ST"))
                tmpIds.add(id);
        }
        Long docCount = (long) recombeeRecommendationService.numberOfRecomms(recommendationWrapperDto.getRecommId());
        recombeeIds = tmpIds.toArray(new String[0]);
        query.filter(new TermsQueryBuilder("id",recombeeIds));
        // no paging here - is considered in recommendation
        searchSourceBuilder.query(query).size(filter.getLimit());
        sourceBuilderToOpportunityWrapper(wrapper, searchSourceBuilder, filter);
        List<OpportunityPreviewDto> sorted = RecombeeFilterConverterUtil.sortByRecombee(wrapper.getOpportunityPreviewDtos(),
                Arrays.stream(recombeeIds).collect(Collectors.toList()),
                wrapper.getOpportunityPreviewDtos().stream().map(OpportunityPreviewDto::getOpportunityId).collect(Collectors.toList()));
        wrapper.setOpportunityPreviewDtos(sorted);
        wrapper.setNumberOfAllItems(docCount);
        return wrapper;
    }

    @Override
    public OpportunityWrapper findOpportunitiesWithRecommendation(OpportunityFilter filter, OrganizationDetailDto organizationDetailDto, List<String> recommendations) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder query = setupFilters(filter);
        if (filter.getQuery() != null && !filter.getQuery().equals("")) {
            final Map<String, Float> fields = new HashMap<>();
            fields.put("opportunityName", 2.0F);
            fields.put("opportunityDescription", 1.0F);
            fields.put("opportunityKw", 3.0F);
            fields.put("opportunityTechReq",1.5F);
            final QueryStringQueryBuilder queryStringQueryBuilder = new QueryStringQueryBuilder(filter.getQueryForSearch())
                    .defaultOperator(Operator.OR).fields(fields);
            query.should(queryStringQueryBuilder);
            query.minimumShouldMatch(1);
        }
        List<String> queryRecommendations = getRecommendationsToQuery(filter, recommendations);
        query.filter(new TermsQueryBuilder("opportunityId", queryRecommendations));
        searchSourceBuilder.query(query)
                .size(filter.getLimit());

        OpportunityWrapper wrapper = new OpportunityWrapper();
        sourceBuilderToOpportunityWrapper(wrapper, searchSourceBuilder, filter);

        OpportunityWrapper fillerWrapper = this.findOpportunitiesWithExclusion(filter, queryRecommendations);
        fillerWrapper.prependOpportunityPreviewDtos(wrapper.getOpportunityPreviewDtos(), queryRecommendations, filter.getLimit());
        return fillerWrapper;
    }
    @Override
    public OpportunityWrapper findOpportunitiesWithCombinedRecommendation(OpportunityFilter filter, OrganizationDetailDto organizationDetailDto, List<String> recommendations) throws IOException{
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder query = new BoolQueryBuilder();
        OpportunityWrapper wrapper = new OpportunityWrapper();
        RecommendationWrapperDto recommendationWrapperDto = wrapper.getRecommendationWrapperDto();

        String[] recombeeIds;
        try {
            recombeeIds = recombeeRecommendationService.recommend(organizationDetailDto, RecombeeFilterConverterUtil.convert(filter), RecombeeScenario.OPPORTUNITY, recommendationWrapperDto);
        } catch (RuntimeException e ){
            System.out.println("Alternative no-recommendation");
            return findOpportunities(filter);
        }
        Map<String, Integer> topRecommendations = new HashMap<>();
        List<String> notFound = new ArrayList<>();
        for (int i = 0; i < recombeeIds.length; i++){
            String item = recombeeIds[i];
            int index = ArrayUtils.indexOf(recommendations.toArray(), item);
            if(index != -1){
                topRecommendations.put(item, i + index);
            } else {
                notFound.add(item);
            }
        }
        Stream<Map.Entry<String, Integer>> sorted = topRecommendations.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue()));
        topRecommendations = sorted.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        List<String> finalRecommendationList = new ArrayList<>(topRecommendations.keySet());
        finalRecommendationList.addAll(notFound);
        List<String> queryRecommendations = getRecommendationsToQuery(filter, finalRecommendationList);
        query.filter(new TermsQueryBuilder("id", queryRecommendations));

        // page is considered in queryRecommendations
        searchSourceBuilder.query(query)
                .size(filter.getLimit());

        sourceBuilderToOpportunityWrapper(wrapper, searchSourceBuilder, filter);
        OpportunityWrapper fillerWrapper = this.findOpportunitiesWithExclusion(filter, queryRecommendations);
        fillerWrapper.prependOpportunityPreviewDtos(wrapper.getOpportunityPreviewDtos(), queryRecommendations, filter.getLimit());
        fillerWrapper.setRecommendationWrapperDto(recommendationWrapperDto);
        return fillerWrapper;
    }

    private List<String> getRecommendationsToQuery(OpportunityFilter opportunityFilter, List<String> recommendations){
        int len = recommendations.size();
        Integer limit = opportunityFilter.getLimit();
        Integer page = opportunityFilter.getPage();
        if(len > limit * page){
            return recommendations.subList((page - 1) * limit, (page) * limit);
        } else if (len > limit * (page - 1)){
            return recommendations.subList((page - 1) * limit, len);
        }
        return new ArrayList<>();
    }

    @Override
    public OpportunityWrapper findOpportunities(OpportunityFilter filter) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder query = setupFilters(filter);
        if (filter.getQuery() != null && !filter.getQuery().equals("")) {
            final Map<String, Float> fields = new HashMap<>();
            fields.put("opportunityName", 2.0F);
            fields.put("opportunityDescription", 1.0F);
            fields.put("opportunityKw", 3.0F);
            fields.put("opportunityTechReq", 1.5F);
            final QueryStringQueryBuilder queryStringQueryBuilder = new QueryStringQueryBuilder(filter.getQueryForSearch())
                    .defaultOperator(Operator.OR).fields(fields);
            query.should(queryStringQueryBuilder);
            query.minimumShouldMatch(1);
        }
        searchSourceBuilder.query(query)
                .size(filter.getLimit())
                .from((filter.getPage() - 1) * filter.getLimit());

        OpportunityWrapper wrapper = new OpportunityWrapper();
        sourceBuilderToOpportunityWrapper(wrapper, searchSourceBuilder, filter);

        wrapper.shuffleOpportunityPreviewDtos();
        return wrapper;
    }

    private OpportunityWrapper findOpportunitiesWithExclusion(OpportunityFilter filter, List<String> toExclude) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder query = setupFilters(filter);
        if (filter.getQuery() != null && !filter.getQuery().equals("")) {
            final Map<String, Float> fields = new HashMap<>();
            fields.put("opportunityName", 2.0F);
            fields.put("opportunityDescription", 1.0F);
            fields.put("opportunityKw", 3.0F);
            fields.put("opportunityTechReq",1.5F);
            final QueryStringQueryBuilder queryStringQueryBuilder = new QueryStringQueryBuilder(filter.getQueryForSearch())
                    .defaultOperator(Operator.AND).fields(fields);
            query.should(queryStringQueryBuilder);
            query.minimumShouldMatch(1);
        }
        query.mustNot(new TermsQueryBuilder("id", toExclude));
        searchSourceBuilder.query(query)
                .size(filter.getLimit())
                .from((filter.getPage() - 1) * filter.getLimit());

        OpportunityWrapper wrapper = new OpportunityWrapper();
        sourceBuilderToOpportunityWrapper(wrapper, searchSourceBuilder, filter);

        wrapper.shuffleOpportunityPreviewDtos();
        return wrapper;
    }

    private void sourceBuilderToOpportunityWrapper(OpportunityWrapper wrapper, SearchSourceBuilder searchSourceBuilder, OpportunityFilter filter) throws IOException {
        final SearchResponseEntityWrapper<Opportunity> result = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Opportunity.class);
        wrapper.setLimit(filter.getLimit());
        wrapper.setPage(filter.getPage());
        wrapper.setNumberOfAllItems(result.getResultCount());

        Set<String> expertIds = result.getSearchResponseEntities()
                .stream().map(SearchResponseEntity::getSource)
                .map(Opportunity::getExpertIds)
                .flatMap(Set::stream).filter(Objects::nonNull).collect(Collectors.toSet());
        Set<Long> organizationIds = result.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource).map(Opportunity::getOrganizationIds)
                .flatMap(Set::stream).filter(Objects::nonNull).collect(Collectors.toSet());
        final Map<String, UserExpertBaseDto> expertBaseDtoMap = expertLookupService.findUserExpertBases(expertIds).stream().collect(Collectors.toMap(UserExpertBaseDto::getExpertCode, Function.identity()));
        final Map<Long, OrganizationBaseDto> organizationBaseMap = organizationSearchService.findBaseOrganizationsMap(organizationIds);
        for (SearchResponseEntity<Opportunity> entity : result.getSearchResponseEntities()) {
            final Opportunity opportunity = entity.getSource();
            final OpportunityPreviewDto opportunityPreviewDto = convert(opportunity);
            opportunityPreviewDto.setOrganizationBaseDtos(opportunity.getOrganizationIds().stream().map(organizationBaseMap::get).filter(Objects::nonNull).collect(Collectors.toList()));
            opportunityPreviewDto.setExpertPreviews(opportunity.getExpertIds().stream().map(expertBaseDtoMap::get).filter(Objects::nonNull).collect(Collectors.toList()));
            wrapper.getOpportunityPreviewDtos().add(opportunityPreviewDto);
        }
    }

    @Override
    public OpportunityPreviewDto findOpportunity(String opportunityId) throws IOException {
        Opportunity opportunity = elasticSearchDao.getDocument(indexName, opportunityId, Opportunity.class);
        if (opportunity == null)
            return null;
        OpportunityPreviewDto dto = convert(opportunity);
        List<UserExpertBaseDto> userExpertBases = expertLookupService.findUserExpertBases(opportunity.getExpertIds().stream().filter(Objects::nonNull).collect(Collectors.toSet()));
        List<OrganizationBaseDto> baseOrganizations = organizationSearchService.findBaseOrganizations(opportunity.getOrganizationIds().stream().filter(Objects::nonNull).collect(Collectors.toSet()));
        dto.setExpertPreviews(userExpertBases);
        dto.setOrganizationBaseDtos(baseOrganizations);
        return dto;
    }

    @Override
    public OpportunityRegisterDto getOpportunity(String opportunityId) throws IOException {
        Opportunity opportunity = elasticSearchDao.getDocument(indexName, opportunityId, Opportunity.class);
        return convertToRegister(opportunity);
    }

    @Override
    public OpportunityStatisticsDto findOpportunityStatistics(Set<Long> organizationIds) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        OpportunityStatisticsDto opportunityStatisticsDto = new OpportunityStatisticsDto();
        final BoolQueryBuilder query = new BoolQueryBuilder()
                .filter(new TermsQueryBuilder("organizationIds", organizationIds));
        searchSourceBuilder.query(query).size(0);
        final SearchResponseEntityWrapper<Opportunity> responseWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Opportunity.class);
        opportunityStatisticsDto.setNumberOfAllOpportunities(responseWrapper.getResultCount());
        return opportunityStatisticsDto;
    }

    private OpportunityRegisterDto convertToRegister(Opportunity opportunity) {
        if (opportunity == null)
            return null;
        OpportunityRegisterDto dto = new OpportunityRegisterDto();
        dto.setOriginalId(opportunity.getOriginalId());
        dto.setOpportunityName(opportunity.getOpportunityName());
        dto.setOpportunityDescription(opportunity.getOpportunityDescription());
        dto.setOpportunityKw(opportunity.getOpportunityKw());
        dto.setOpportunitySignupDate(opportunity.getOpportunitySignupDate());
        dto.setOpportunityLocation(opportunity.getOpportunityLocation());
        dto.setOpportunityWage(opportunity.getOpportunityWage());
        dto.setOpportunityTechReq(opportunity.getOpportunityTechReq());
        dto.setOpportunityFormReq(opportunity.getOpportunityFormReq());
        dto.setOpportunityOtherReq(opportunity.getOpportunityOtherReq());
        dto.setOpportunityBenefit(opportunity.getOpportunityBenefit());
        dto.setOpportunityJobStartDate(opportunity.getOpportunityJobStartDate());
        dto.setOpportunityExtLink(opportunity.getOpportunityExtLink());
        dto.setOpportunityHomeOffice(opportunity.getOpportunityHomeOffice());
        dto.setOpportunityType(opportunity.getOpportunityType());
        dto.setJobTypes(opportunity.getJobTypes());
        dto.setOrganizationIds(opportunity.getOrganizationIds());
        dto.setExpertIds(opportunity.getExpertIds());
        return dto;
    }

    private OpportunityPreviewDto convert(Opportunity opportunity) {
        if (opportunity == null)
            return null;
        OpportunityPreviewDto dto = new OpportunityPreviewDto();
        dto.setOpportunityId(opportunity.getOpportunityId());
        dto.setOpportunityName(opportunity.getOpportunityName());
        dto.setOpportunityDescription(opportunity.getOpportunityDescription());
        dto.setOpportunityKw(opportunity.getOpportunityKw());
        dto.setOpportunitySignupDate(opportunity.getOpportunitySignupDate());
        dto.setOpportunityLocation(opportunity.getOpportunityLocation());
        dto.setOpportunityWage(opportunity.getOpportunityWage());
        dto.setOpportunityTechReq(opportunity.getOpportunityTechReq());
        dto.setOpportunityFormReq(opportunity.getOpportunityFormReq());
        dto.setOpportunityOtherReq(opportunity.getOpportunityOtherReq());
        dto.setOpportunityBenefit(opportunity.getOpportunityBenefit());
        dto.setOpportunityJobStartDate(opportunity.getOpportunityJobStartDate());
        dto.setOpportunityExtLink(opportunity.getOpportunityExtLink());
        dto.setOpportunityHomeOffice(opportunity.getOpportunityHomeOffice());
        dto.setOpportunityType(opportunity.getOpportunityType());
        dto.setJobTypes(opportunity.getJobTypes());
        dto.setHidden(opportunity.getHidden());
        return dto;
    }
}
