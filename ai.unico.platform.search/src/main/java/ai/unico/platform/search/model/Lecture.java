package ai.unico.platform.search.model;

import ai.unico.platform.util.enums.Confidentiality;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Lecture extends ElasticSearchModel {

    private Long lectureId;

    private Long originalLectureId;

    private String lectureBk;

    private String lectureName;

    private Long itemTypeId;

    private String lectureDescription;

    private Integer year;

    private Confidentiality confidentiality;

    private Long ownerOrganizationId; //todo check if not null in data

    private Boolean translationUnavailable;

    private List<String> keywords = new ArrayList<>();

    private Set<Long> organizationIds = new HashSet<>();

    private Set<Long> verifiedOrganizationIds = new HashSet<>();

    private Set<Long> evidenceOrganizationIds = new HashSet<>();

    private Set<String> expertCodes = new HashSet<>();

    private Set<String> allExpertCodes = new HashSet<>();

    private Set<String> originalExpertCodes = new HashSet<>();

    private String itemSpecification;

    private String itemDomain;

    private String itemScienceSpecification;

    public Long getLectureId() {
        return lectureId;
    }

    public void setLectureId(Long lectureId) {
        this.lectureId = lectureId;
    }

    public Long getOriginalLectureId() {
        return originalLectureId;
    }

    public void setOriginalLectureId(Long originalLectureId) {
        this.originalLectureId = originalLectureId;
    }

    public String getLectureBk() {
        return lectureBk;
    }

    public void setLectureBk(String lectureBk) {
        this.lectureBk = lectureBk;
    }

    public String getLectureName() {
        return lectureName;
    }

    public void setLectureName(String lectureName) {
        this.lectureName = lectureName;
    }

    public Long getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(Long itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public String getLectureDescription() {
        return lectureDescription;
    }

    public void setLectureDescription(String lectureDescription) {
        this.lectureDescription = lectureDescription;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Confidentiality getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Confidentiality confidentiality) {
        this.confidentiality = confidentiality;
    }

    public Long getOwnerOrganizationId() {
        return ownerOrganizationId;
    }

    public void setOwnerOrganizationId(Long ownerOrganizationId) {
        this.ownerOrganizationId = ownerOrganizationId;
    }

    public Boolean getTranslationUnavailable() {
        return translationUnavailable;
    }

    public void setTranslationUnavailable(Boolean translationUnavailable) {
        this.translationUnavailable = translationUnavailable;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<Long> getVerifiedOrganizationIds() {
        return verifiedOrganizationIds;
    }

    public void setVerifiedOrganizationIds(Set<Long> verifiedOrganizationIds) {
        this.verifiedOrganizationIds = verifiedOrganizationIds;
    }

    public Set<Long> getEvidenceOrganizationIds() {
        return evidenceOrganizationIds;
    }

    public void setEvidenceOrganizationIds(Set<Long> evidenceOrganizationIds) {
        this.evidenceOrganizationIds = evidenceOrganizationIds;
    }

    public Set<String> getExpertCodes() {
        return expertCodes;
    }

    public void setExpertCodes(Set<String> expertCodes) {
        this.expertCodes = expertCodes;
    }

    public Set<String> getAllExpertCodes() {
        return allExpertCodes;
    }

    public void setAllExpertCodes(Set<String> allExpertCodes) {
        this.allExpertCodes = allExpertCodes;
    }

    public Set<String> getOriginalExpertCodes() {
        return originalExpertCodes;
    }

    public void setOriginalExpertCodes(Set<String> originalExpertCodes) {
        this.originalExpertCodes = originalExpertCodes;
    }

    public String getItemSpecification() {
        return itemSpecification;
    }

    public void setItemSpecification(String itemSpecification) {
        this.itemSpecification = itemSpecification;
    }

    public String getItemDomain() {
        return itemDomain;
    }

    public void setItemDomain(String itemDomain) {
        this.itemDomain = itemDomain;
    }

    public String getItemScienceSpecification() {
        return itemScienceSpecification;
    }

    public void setItemScienceSpecification(String itemScienceSpecification) {
        this.itemScienceSpecification = itemScienceSpecification;
    }
}
