package ai.unico.platform.search.util;

public class SearchRankUtil {
    public static Double recalculateRank(Double initialRank) {
        if (initialRank.equals(0D)) return 0D;
        return (-Math.pow(2, -Math.sqrt(initialRank / 50)) + 1) * 100;
    }
}
