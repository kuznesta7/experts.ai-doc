package ai.unico.platform.search.util;

import ai.unico.platform.search.api.filter.ExpertSearchFilter;
import ai.unico.platform.store.api.dto.QueryTranslationDto;
import ai.unico.platform.store.api.service.QueryTranslationService;
import ai.unico.platform.util.ServiceRegistryUtil;

public class SearchQueryTranslationUtil {

    public static void updateFilter(ExpertSearchFilter searchFilter) {
        final QueryTranslationService queryTranslationService = ServiceRegistryUtil.getService(QueryTranslationService.class);
        final QueryTranslationDto queryTranslationDto = queryTranslationService.translate(searchFilter.getQuery());
        if (queryTranslationDto == null) return;
        searchFilter.setQuery(queryTranslationDto.getQuery());
        searchFilter.setFullNameQuery(queryTranslationDto.getFullNameQuery());
        searchFilter.setExcludeInvestProjects(queryTranslationDto.isExcludeInvestProjects());
        searchFilter.setExcludeOutcomes(queryTranslationDto.isExcludeOutcomes());
        searchFilter.setYearFrom(queryTranslationDto.getYearFrom());
        searchFilter.setYearTo(queryTranslationDto.getYearTo());
        searchFilter.setOrganizationIds(queryTranslationDto.getOrganizationIds());
        searchFilter.setResultTypeGroupIds(queryTranslationDto.getTypeGroupIds());
        searchFilter.setResultTypeIds(queryTranslationDto.getTypeIds());
        searchFilter.setCountryCodes(queryTranslationDto.getCountryCodes());
    }

}
