package ai.unico.platform.search.service;

import ai.unico.platform.search.api.service.ExpertIndexService;
import ai.unico.platform.search.api.service.ItemAffiliationService;
import ai.unico.platform.search.api.service.ItemIndexService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Expert;
import ai.unico.platform.search.model.Item;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.ExpertUtil;
import ai.unico.platform.store.api.enums.IndexHistoryStatus;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.api.service.IndexHistoryService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.model.UserContext;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.ExistsQueryBuilder;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ItemAffiliationServiceImpl implements ItemAffiliationService {

    private final Integer maxBucketSize = 50000;
    @Autowired
    private ElasticSearchDao elasticSearchDao;
    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;
    private boolean indexingInProgress = false;

    @Override
    public void recalculateItems(Integer limit, UserContext userContext) throws IOException {
        if (indexingInProgress) return;
        final IndexHistoryService indexHistoryService = ServiceRegistryUtil.getService(IndexHistoryService.class);
        final Long indexHistoryId = indexHistoryService.saveIndexHistory(IndexTarget.ITEM_ASSOCIATION, false, userContext);
        try {
            indexingInProgress = true;
            Integer lastId = 0;
            System.out.println("Starting item association");
            while (true) {
                final BoolQueryBuilder query = new BoolQueryBuilder()
                        .filter(new ExistsQueryBuilder("originalItemId"))
                        .filter(new RangeQueryBuilder("originalItemId").gt(lastId));
                final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                        .query(query)
                        .size(limit)
                        .sort("originalItemId", SortOrder.ASC);
                final SearchHits searchHits = elasticSearchDao.doSearchAndGetHits(ItemIndexService.indexName, searchSourceBuilder);
                final Set<String> itemIds = new HashSet<>();
                for (SearchHit searchHit : searchHits) {
                    final String itemId = searchHit.getId();
                    itemIds.add(itemId);
                    lastId = (Integer) searchHit.getSortValues()[0];
                }
                recalculateComputedFieldsForItems(itemIds);
                System.out.println(lastId);
                if (searchHits.totalHits == 0)
                    break;
            }


            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FINISHED, userContext);
            System.out.println("Item association finished");
        } catch (Exception e) {
            e.printStackTrace();
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FAILED, userContext);
        }
        indexingInProgress = false;
    }


    @Override
    public void recalculateComputedFieldsForItems(Set<String> itemsIds) throws IOException {
        try {
            elasticSearchDao.flush(ItemIndexService.indexName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (itemsIds.isEmpty()) return;
        elasticSearchDao.doUpdate(ItemIndexService.indexName, "affiliatedOrganizationIds", getItemAffiliatedOrganizationIdsMap(itemsIds));
    }

    @Override
    public void updateItemsForExpert(Set<Long> expertIds, Long organizationId, boolean add) throws IOException {
        Set<String> ids = expertIds.stream().map(ExpertUtil::convertToExpertCode).collect(Collectors.toSet());
        updateItemsForUserExpert(ids, organizationId, add);
    }

    @Override
    public void updateItemsForUser(Set<Long> userIds, Long organizationId, boolean add) throws IOException {
        Set<String> ids = userIds.stream().map(ExpertUtil::convertToUserCode).collect(Collectors.toSet());
        updateItemsForUserExpert(ids, organizationId, add);
    }

    @Override
    public void updateItemsForUser(Set<Long> userIds, Set<Long> organizationId) throws IOException {
        Set<String> ids = userIds.stream().map(ExpertUtil::convertToUserCode).collect(Collectors.toSet());
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(new TermsQueryBuilder("allExpertCodes", ids))
                .size(10000);
        SearchResponseEntityWrapper<Item> searchHits = elasticSearchDao.doSearchAndGetHits(ItemIndexService.indexName, searchSourceBuilder, Item.class);
        final List<Item> items = new ArrayList<>();
        for (SearchResponseEntity<Item> i : searchHits.getSearchResponseEntities()) {
            i.getSource().setAffiliatedOrganizationIds(organizationId);
            items.add(i.getSource());
        }
        elasticSearchDao.doIndex(ItemIndexService.indexName, items);
    }


    private void updateItemsForUserExpert(Set<String> ids, Long organizationId, boolean add) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(new TermsQueryBuilder("allExpertCodes", ids))
                .size(10000);
        SearchResponseEntityWrapper<Item> searchHits = elasticSearchDao.doSearchAndGetHits(ItemIndexService.indexName, searchSourceBuilder, Item.class);
        final List<Item> items = new ArrayList<>();
        for (SearchResponseEntity<Item> i : searchHits.getSearchResponseEntities()) {
            if (add)
                i.getSource().getAffiliatedOrganizationIds().add(organizationId);
            else
                i.getSource().getAffiliatedOrganizationIds().remove(organizationId);
            items.add(i.getSource());
        }
        if (!items.isEmpty())
            elasticSearchDao.doIndex(ItemIndexService.indexName, items);
    }

    private Map<String, Set<Long>> getItemAffiliatedOrganizationIdsMap(Set<String> itemsIds) throws IOException {
        final Map<String, Set<String>> itemExpert = new HashMap<>();
        final Map<String, Set<Long>> expertOrganization = new HashMap<>();
        final Map<String, Set<Long>> itemOrganization = new HashMap<>();
        final Set<String> expertIds = new HashSet<>();
        itemsIds.remove(null);
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(new TermsQueryBuilder("id", itemsIds))
                .size(itemsIds.size());
        final SearchResponseEntityWrapper<Item> searchHits = elasticSearchDao.doSearchAndGetHits(ItemIndexService.indexName, searchSourceBuilder, Item.class);
        for (SearchResponseEntity<Item> i :
                searchHits.getSearchResponseEntities()) {
            itemExpert.put(i.getSource().getId(), i.getSource().getAllExpertCodes());
            expertIds.addAll(i.getSource().getAllExpertCodes());
        }
        if (expertIds.isEmpty()) return itemOrganization;
        expertIds.remove(null);
        final SearchSourceBuilder searchSourceBuilder2 = new SearchSourceBuilder()
                .query(new TermsQueryBuilder("id", expertIds))
                .size(expertIds.size());
        final SearchResponseEntityWrapper<Expert> searchResponseEntity2 = elasticSearchDao.doSearchAndGetHits(ExpertIndexService.indexName, searchSourceBuilder2, Expert.class);
        for (SearchResponseEntity<Expert> e :
                searchResponseEntity2.getSearchResponseEntities()) {
            expertOrganization.put(e.getSource().getId(), e.getSource().getOrganizationIds());
        }
        for (String itemId : itemsIds) {
            Set<Long> organizationIds = new HashSet<>();
            Set<String> itemExpert2 = itemExpert.get(itemId);
            if (itemExpert2 == null) {
                continue;
            }
            for (String expertId : itemExpert2) {
                Set<Long> orgIds = expertOrganization.get(expertId);
                if (orgIds == null) {
                    continue;
                }
                organizationIds.addAll(orgIds);
            }
            itemOrganization.put(itemId, organizationIds);
        }
        return itemOrganization;
    }
}
