package ai.unico.platform.search.util;

import ai.unico.platform.extdata.dto.ExpertProfilePreviewDto;
import ai.unico.platform.search.model.Expert;
import ai.unico.platform.store.api.dto.UserProfileIndexDto;
import ai.unico.platform.store.api.service.ClaimProfileService;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.KeywordsUtil;
import ai.unico.platform.util.ServiceRegistryUtil;

import java.util.*;
import java.util.stream.Collectors;

public class ExpertUtil {

    public static Expert convertToEntity(ExpertProfilePreviewDto expertDto) {
        final Expert expert = new Expert();
        expert.setId(convertToExpertCode(expertDto.getExpertId()));
        expert.getExpertIds().add(expertDto.getExpertId());
        expert.setFullName(expertDto.getExpertFullName());
        expert.setExpertCode(expert.getId());
        expert.setClaimable(true);
        expert.setRetiredOrganizationIds(expertDto.getRetiredOrganizationIds());
        expert.setVisibleOrganizationIds(expertDto.getVisibleOrganizationIds());
        expert.setActiveOrganizationIds(expertDto.getActiveOrganizationIds());
        return expert;
    }

    public static Expert convertToEntity(UserProfileIndexDto userProfileDto) {
        final Expert expert = new Expert();
        expert.setId(convertToUserCode(userProfileDto.getUserId()));
        expert.setUserId(userProfileDto.getUserId());
        expert.setExpertIds(userProfileDto.getExpertIds());
        expert.setFullName(userProfileDto.getName());
        expert.setExpertCode(expert.getId());
        expert.setEmail(userProfileDto.getEmail());
        expert.setOrganizationIds(userProfileDto.getOrganizationIds());
        expert.setVerifiedOrganizationIds(userProfileDto.getVerifiedOrganizationIds());
        expert.setFavoriteOrganizationIds(userProfileDto.getFavoriteOrganizationIds());
        expert.setRetiredOrganizationIds(userProfileDto.getRetiredOrganizationIds());
        expert.setVisibleOrganizationIds(userProfileDto.getVisibleOrganizationIds());
        expert.setActiveOrganizationIds(userProfileDto.getActiveOrganizationIds());
        expert.setUsername(userProfileDto.getUsername());
        expert.setClaimable(userProfileDto.isClaimable());
        expert.setClaimedById(userProfileDto.getClaimedById());
        expert.setRetirementDate(userProfileDto.getRetirementDate());
        expert.setDescription(userProfileDto.getDescription());
        expert.setKeywords(KeywordsUtil.sortKeywordList(userProfileDto.getKeywords()));
        expert.setRoles(userProfileDto.getRoles().stream().map(Enum::toString).collect(Collectors.toSet()));
        return expert;
    }

    public static String convertToExpertCode(Long expertId) {
        if (expertId == null) return null;
        return IdUtil.convertToExtDataCode(expertId);
    }

    public static String convertToUserCode(Long userId) {
        if (userId == null) return null;
        return IdUtil.convertToIntDataCode(userId);
    }

    public static Long getUserId(String expertCode) {
        return IdUtil.getIntDataId(expertCode);
    }

    public static Long getExpertId(String expertCode) {
        return IdUtil.getExtDataId(expertCode);
    }

    public static boolean isExpertVerified(Expert expert) {
        final Set<Long> verifiedOrganizationIds = expert.getVerifiedOrganizationIds();
        return expert.getUserId() != null || (verifiedOrganizationIds != null && !verifiedOrganizationIds.isEmpty());
    }

    public static Map<String, String> findSubstituteExpertCodes() {
        final ClaimProfileService claimProfileService = ServiceRegistryUtil.getService(ClaimProfileService.class);
        final Map<Long, Long> expertUserMap = claimProfileService.findClaimedExpertIdUserIdMap();
        final Map<Long, Long> claimedUserIdUserIdMap = claimProfileService.findClaimedUserIdUserIdMap();
        return createReplacementMap(claimedUserIdUserIdMap, expertUserMap);
    }

    public static Map<String, String> findSubstituteExpertCodes(Collection<String> expertCodes) {
        final Set<Long> expertIds = expertCodes.stream()
                .map(ExpertUtil::getExpertId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        final Set<Long> userIds = expertCodes.stream()
                .map(ExpertUtil::getUserId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        final ClaimProfileService claimProfileService = ServiceRegistryUtil.getService(ClaimProfileService.class);
        final Map<Long, Long> userUserMap;
        final Map<Long, Long> expertUserMap;
        if (!expertIds.isEmpty()) {
            expertUserMap = claimProfileService.findClaimedExpertIdUserIdMap(expertIds);
        } else {
            expertUserMap = Collections.emptyMap();
        }
        if (!userIds.isEmpty()) {
            userUserMap = claimProfileService.findClaimedUserIdUserIdMap(expertIds);
        } else {
            userUserMap = Collections.emptyMap();
        }
        return createReplacementMap(userUserMap, expertUserMap);
    }

    public static void replaceExpertCodes(Collection<String> expertCodes, Map<String, String> replacementMap) {
        final Set<String> expertCodesToAdd = expertCodes.stream()
                .filter(replacementMap::containsKey)
                .map(replacementMap::get)
                .collect(Collectors.toSet());
        expertCodes.removeAll(replacementMap.keySet());
        expertCodes.addAll(expertCodesToAdd);
    }

    private static Map<String, String> createReplacementMap(Map<Long, Long> userUserMap, Map<Long, Long> expertUserMap) {
        final Map<String, String> resultMap = new HashMap<>();
        for (Map.Entry<Long, Long> expertUserEntry : expertUserMap.entrySet()) {
            final String replacedExpertCode = convertToExpertCode(expertUserEntry.getKey());
            final String newUserCode = convertToUserCode(expertUserEntry.getValue());
            resultMap.put(replacedExpertCode, newUserCode);
        }
        for (Map.Entry<Long, Long> userIdUserIdEntrySet : userUserMap.entrySet()) {
            final String replacedUserCode = convertToUserCode(userIdUserIdEntrySet.getKey());
            final String newUserCode = convertToUserCode(userIdUserIdEntrySet.getValue());
            if (resultMap.containsValue(replacedUserCode)) {
                for (Map.Entry<String, String> resultEntry : resultMap.entrySet()) {
                    if (resultEntry.getValue().equals(replacedUserCode)) {
                        resultEntry.setValue(newUserCode);
                    }
                }
            }
            resultMap.put(replacedUserCode, newUserCode);
        }
        return resultMap;
    }
}
