package ai.unico.platform.search.util;

import ai.unico.platform.search.api.filter.ExpertSearchFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.store.api.dto.ItemTypeSearchDto;
import ai.unico.platform.store.api.service.ItemTypeService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;
import org.elasticsearch.common.lucene.search.function.FunctionScoreQuery;
import org.elasticsearch.index.query.*;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.index.query.functionscore.ScriptScoreFunctionBuilder;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;

import java.util.*;

public class ItemSearchUtil {

    public static FunctionScoreQueryBuilder prepareFindByExpertiseScoreQueryBuilder(ExpertSearchFilter expertSearchFilter, boolean getAllItems, String expertCode) {
        return prepareFindByExpertiseScoreQueryBuilder(expertSearchFilter, getAllItems, 0.0F, null);
    }

    public static FunctionScoreQueryBuilder prepareFindByExpertiseScoreQueryBuilder(ExpertSearchFilter expertSearchFilter, boolean getAllItems, Float baseBoost, String expertCode) {
        final ItemTypeService itemTypeService = ServiceRegistryUtil.getService(ItemTypeService.class);
        final List<ItemTypeSearchDto> itemTypesSearch = itemTypeService.findItemTypesSearch();
        final List<FunctionScoreQueryBuilder.FilterFunctionBuilder> filterFunctionBuilders = new ArrayList<>();

        final Set<Long> resultTypeGroupToFilterIds = expertSearchFilter.getResultTypeGroupIds();

        for (ItemTypeSearchDto typesSearch : itemTypesSearch) {
            filterFunctionBuilders.add(new FunctionScoreQueryBuilder.FilterFunctionBuilder(
                    new TermQueryBuilder("itemTypeId", typesSearch.getTypeId()),
                    ScoreFunctionBuilders.weightFactorFunction(typesSearch.getWeight())
            ));
            if (resultTypeGroupToFilterIds.contains(typesSearch.getTypeGroupId())) {
                expertSearchFilter.getResultTypeIds().add(typesSearch.getTypeId());
            }
        }

        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.filter(new TermQueryBuilder("confidentiality", Confidentiality.PUBLIC));

        if (getAllItems) {
            boolQueryBuilder.must(new MatchAllQueryBuilder().boost(baseBoost));
        }

        if (expertSearchFilter.getAvailableExpertCodes() != null) {
            boolQueryBuilder.filter(new TermsQueryBuilder("expertCodes", expertSearchFilter.getAvailableExpertCodes()));
        }

        if (expertSearchFilter instanceof ExpertSearchResultFilter && expertCode != null) {
            if (((ExpertSearchResultFilter) expertSearchFilter).isShowHidden()) {
                boolQueryBuilder.filter(new TermQueryBuilder("allExpertCodes", expertCode));
            } else {
                boolQueryBuilder.filter(new TermQueryBuilder("expertCodes", expertCode));
            }
        }
        if (expertSearchFilter.getQuery() != null) {
            final QueryBuilder matchQuery = prepareMultiMatchQuery(expertSearchFilter.getQueryForSearch());
            boolQueryBuilder.should(matchQuery).minimumShouldMatch(getAllItems ? 0 : 1);

            final List<String> secondarySearchQueries = expertSearchFilter.getSecondaryQueries();
            for (int i = 0; i < secondarySearchQueries.size(); i++) {
                final List<Float> secondarySearchWeights = expertSearchFilter.getSecondaryWeights();
                if (secondarySearchWeights.size() <= i) secondarySearchWeights.add(0.5F);

                boolQueryBuilder.should(
                        prepareMultiMatchQuery(secondarySearchQueries.get(i))
                                .boost(secondarySearchWeights.get(i)));
            }
        }

        if (expertSearchFilter.getResultTypeIds().size() > 0) {
            final TermsQueryBuilder itemIdsTermQueryBuilder = new TermsQueryBuilder("itemTypeId", expertSearchFilter.getResultTypeIds());
            filterFunctionBuilders.add(new FunctionScoreQueryBuilder.FilterFunctionBuilder(
                    new BoolQueryBuilder().mustNot(itemIdsTermQueryBuilder),
                    ScoreFunctionBuilders.weightFactorFunction(0)));

            if (expertSearchFilter instanceof ExpertSearchResultFilter) {
                if (((ExpertSearchResultFilter) expertSearchFilter).isForceFilterItemTypes()) {
                    if (expertSearchFilter.getResultTypeGroupIds().contains(4L)) {
                        boolQueryBuilder.filter(new BoolQueryBuilder().should(itemIdsTermQueryBuilder).should(new BoolQueryBuilder().mustNot(new ExistsQueryBuilder("itemTypeId"))));
                    } else {
                        boolQueryBuilder.filter(itemIdsTermQueryBuilder);
                    }
                }
            }
            if (!getAllItems) boolQueryBuilder.filter(itemIdsTermQueryBuilder);
        }

        if (expertSearchFilter.getYearFrom() != null || expertSearchFilter.getYearTo() != null) {
            final RangeQueryBuilder rangeQueryBuilder = new RangeQueryBuilder("year");
            if (expertSearchFilter.getYearFrom() != null) rangeQueryBuilder.from(expertSearchFilter.getYearFrom());
            if (expertSearchFilter.getYearTo() != null) rangeQueryBuilder.to(expertSearchFilter.getYearTo());
            filterFunctionBuilders.add(new FunctionScoreQueryBuilder.FilterFunctionBuilder(
                    new BoolQueryBuilder().mustNot(rangeQueryBuilder),
                    ScoreFunctionBuilders.weightFactorFunction(0)));
            if (!getAllItems) boolQueryBuilder.filter(rangeQueryBuilder);
        }

        final Map<String, Object> yearParams = new HashMap<>();
        yearParams.put("currentYear", Calendar.getInstance().get(Calendar.YEAR));
        final Script yearScript = new Script(ScriptType.INLINE, "painless", "doc['year'].value != null ? Math.pow(1.05, doc['year'].value - params.currentYear) : 1", yearParams);
        filterFunctionBuilders.add(new FunctionScoreQueryBuilder.FilterFunctionBuilder(new ScriptScoreFunctionBuilder(yearScript)));
        final FunctionScoreQueryBuilder.FilterFunctionBuilder[] filterFunctionBuildersArray = filterFunctionBuilders.toArray(new FunctionScoreQueryBuilder.FilterFunctionBuilder[filterFunctionBuilders.size()]);
        return new FunctionScoreQueryBuilder(boolQueryBuilder, filterFunctionBuildersArray)
                .scoreMode(FunctionScoreQuery.ScoreMode.MULTIPLY);
    }

    private static QueryBuilder prepareMultiMatchQuery(String query) {
        final Map<String, Float> fields = new HashMap<>();
        fields.put("itemName", 2.0F);
        fields.put("itemDescription", 1.0F);
        fields.put("keywords", 3.0F);
        return new QueryStringQueryBuilder(ElasticSearchUtil.prepareQueryStringQuery(query)).defaultOperator(Operator.AND).fields(fields);
    }
}
