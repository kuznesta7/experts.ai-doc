package ai.unico.platform.search.model;

import java.util.ArrayList;
import java.util.List;

public class SearchResponseEntityWrapper<T> {

    private Long resultCount;

    private Float maxScore;

    private List<SearchResponseEntity<T>> searchResponseEntities = new ArrayList<>();

    public Long getResultCount() {
        return resultCount;
    }

    public void setResultCount(Long resultCount) {
        this.resultCount = resultCount;
    }

    public List<SearchResponseEntity<T>> getSearchResponseEntities() {
        return searchResponseEntities;
    }

    public void setSearchResponseEntities(List<SearchResponseEntity<T>> searchResponseEntities) {
        this.searchResponseEntities = searchResponseEntities;
    }

    public Float getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(Float maxScore) {
        this.maxScore = maxScore;
    }
}
