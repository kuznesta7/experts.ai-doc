package ai.unico.platform.search.service;

import ai.unico.platform.search.api.dto.EvidenceItemPreviewWithExpertsDto;
import ai.unico.platform.search.api.dto.ExpertClaimPreviewDto;
import ai.unico.platform.search.api.service.ExpertClaimSearchService;
import ai.unico.platform.search.api.service.ItemIndexService;
import ai.unico.platform.search.api.service.OrganizationSearchService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Expert;
import ai.unico.platform.search.model.Item;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.ItemUtil;
import ai.unico.platform.store.api.dto.ItemTypeSearchDto;
import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.service.ItemTypeService;
import ai.unico.platform.ume.api.dto.UserDto;
import ai.unico.platform.ume.api.service.UserService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;
import org.elasticsearch.index.query.*;
import org.elasticsearch.index.query.functionscore.FieldValueFactorFunctionBuilder;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ExpertClaimSearchServiceImpl implements ExpertClaimSearchService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private OrganizationSearchService organizationSearchService;

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;

    @Override
    public List<ExpertClaimPreviewDto> findExpertsToClaim(Long userId) throws IOException {
        final UserService userService = ServiceRegistryUtil.getService(UserService.class);
        final UserDto userDto = userService.find(userId);
        final String firstName = userDto.getFirstName();
        final String lastName = userDto.getLastName();
        final String fullName = firstName + " " + lastName;
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder()
                .should(new MatchQueryBuilder("fullName.folded", fullName).boost(10))
                .should(new TermQueryBuilder("claimedById", userId));
        boolQueryBuilder.minimumShouldMatch(1);

        if (lastName != null && !lastName.isEmpty()) {
            final String shorten = firstName.substring(0, 1) + " " + lastName;
            final String xShorten = shorten.substring(0, 3);
            boolQueryBuilder.should(new MatchQueryBuilder("fullName.folded", shorten).boost(0.5F));
            boolQueryBuilder.should(new MatchQueryBuilder("fullName.folded", xShorten).boost(0.25F));
        }
//        boolQueryBuilder.filter(new ExistsQueryBuilder("organizationIds"));
        return doSearch(boolQueryBuilder, maxResultWindow, userId);
    }

    @Override
    public List<ExpertClaimPreviewDto> findExpertsToClaim(String query) throws IOException {
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder()
                .filter(new MatchQueryBuilder("fullName.folded", query));
        return doSearch(boolQueryBuilder, 30, null);
    }

    private void fillItemsInfo(List<ExpertClaimPreviewDto> expertClaimPreviewDtos) throws IOException {
        final Set<String> expertCodes = expertClaimPreviewDtos.stream()
                .map(ExpertClaimPreviewDto::getExpertCode)
                .collect(Collectors.toSet());
        final String itemIndexName = ItemIndexService.indexName;
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.filter(new TermsQueryBuilder("originalExpertCodes", expertCodes))
                .filter(new TermQueryBuilder("confidentiality", Confidentiality.PUBLIC));
        searchSourceBuilder.query(boolQueryBuilder).size(maxResultWindow);
        final ItemTypeService itemTypeService = ServiceRegistryUtil.getService(ItemTypeService.class);
        final List<ItemTypeSearchDto> itemTypesSearch = itemTypeService.findItemTypesSearch();
        final Map<Long, Float> itemTypeIdWeightMap = itemTypesSearch.stream().collect(Collectors.toMap(ItemTypeSearchDto::getTypeId, ItemTypeSearchDto::getWeight));
        final SearchResponseEntityWrapper<Item> itemSearchResponseEntityWrapper = elasticSearchDao.doSearchAndGetHits(itemIndexName, searchSourceBuilder, Item.class);
        final Map<String, List<Item>> expertCodeToItemMap = new HashMap<>();
        for (SearchResponseEntity<Item> searchResponseEntity : itemSearchResponseEntityWrapper.getSearchResponseEntities()) {
            final Item item = searchResponseEntity.getSource();
            for (String expertCode : item.getOriginalExpertCodes()) {
                final List<Item> expertItems = expertCodeToItemMap.computeIfAbsent(expertCode, x -> new ArrayList<>());
                expertItems.add(item);
            }
        }
        for (ExpertClaimPreviewDto expertClaimPreviewDto : expertClaimPreviewDtos) {
            final String expertCode = expertClaimPreviewDto.getExpertCode();
            final List<Item> expertItems = expertCodeToItemMap.get(expertCode);
            if (expertItems == null) continue;
            expertItems.sort(Comparator.comparing(x -> itemTypeIdWeightMap.getOrDefault(x.getItemId(), 0F)));
            final List<EvidenceItemPreviewWithExpertsDto> expertItemDtos = expertItems.stream()
                    .map(ItemUtil::convertToEvidencePreview)
                    .collect(Collectors.toList())
                    .subList(0, Math.min(10, expertItems.size()));
            expertClaimPreviewDto.setItemPreviewDtos(expertItemDtos);
        }
    }

    private List<ExpertClaimPreviewDto> doSearch(BoolQueryBuilder boolQueryBuilder, Integer limit, Long userId) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        boolQueryBuilder.must(new MatchAllQueryBuilder());
        boolQueryBuilder.filter(new RangeQueryBuilder("expertRank").gte(0));
        boolQueryBuilder.filter(new TermQueryBuilder("claimable", true));

        searchSourceBuilder.query(new FunctionScoreQueryBuilder(boolQueryBuilder, new FieldValueFactorFunctionBuilder("expertRank")));

        searchSourceBuilder.size(limit);
        final SearchResponseEntityWrapper<Expert> expertSearchResponseEntityWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Expert.class);

        final List<ExpertClaimPreviewDto> userExpertPreviewDtos = new ArrayList<>();
        final List<SearchResponseEntity<Expert>> searchResponseEntities = expertSearchResponseEntityWrapper.getSearchResponseEntities();

        final Set<Long> organizationIds = searchResponseEntities.stream()
                .map(SearchResponseEntity::getSource)
                .map(Expert::getOrganizationIds)
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
        final List<OrganizationBaseDto> baseOrganizations = organizationIds.isEmpty() ? Collections.emptyList() : organizationSearchService.findBaseOrganizations(organizationIds);

        for (SearchResponseEntity<Expert> searchResponseEntity : searchResponseEntities) {
            final Expert source = searchResponseEntity.getSource();
//            if (source.getExpertRank() < 0.1) continue;  // this sometimes makes problems with profile claims
            final ExpertClaimPreviewDto userExpertPreviewDto = new ExpertClaimPreviewDto();
            userExpertPreviewDto.setExpertCode(source.getExpertCode());
            userExpertPreviewDto.setUserId(source.getUserId());
            userExpertPreviewDto.setFullName(source.getFullName());
            userExpertPreviewDto.setClaimed(source.getClaimedById() != null);
            userExpertPreviewDto.setClaimedByAnotherUser(userExpertPreviewDto.isClaimed() && userId != null && !userId.equals(source.getClaimedById()));

            final Set<Long> expertIds = source.getExpertIds();
            if (expertIds != null && expertIds.size() == 1) {
                userExpertPreviewDto.setExpertId(expertIds.stream().findFirst().get());
            }

            final Set<Long> expertOrganizationIds = source.getOrganizationIds();
            userExpertPreviewDto.setOrganizationBaseDtos(baseOrganizations.stream()
                    .filter(organizationBaseDto -> organizationBaseDto != null && expertOrganizationIds.contains(organizationBaseDto.getOrganizationId()))
                    .collect(Collectors.toSet()));
            userExpertPreviewDtos.add(userExpertPreviewDto);
        }
        fillItemsInfo(userExpertPreviewDtos);
        return userExpertPreviewDtos;
    }
}
