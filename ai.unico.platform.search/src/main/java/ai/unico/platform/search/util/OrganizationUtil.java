package ai.unico.platform.search.util;

import ai.unico.platform.store.api.dto.OrganizationBaseDto;

import java.util.*;
import java.util.stream.Collectors;

public class OrganizationUtil {

    public static List<OrganizationBaseDto> findAndSortOrganizationBases(Map<Long, OrganizationBaseDto> baseOrganizationsMap, Collection<Long> organizationIdsToFilter, Long preferredOrganizationId, Collection<Long> otherOrganizationIds) {
        return organizationIdsToFilter.stream()
                .map(baseOrganizationsMap::get)
                .filter(Objects::nonNull)
                .sorted(Comparator.comparingInt(o -> o.getOrganizationId().equals(preferredOrganizationId) ? 0 : (otherOrganizationIds.contains(o.getOrganizationId()) ? 1 : 2)))
                .collect(Collectors.toList());
    }

}
