package ai.unico.platform.search.service;

import ai.unico.platform.extdata.dto.ExpertItemPreviewDto;
import ai.unico.platform.extdata.service.DWHItemService;
import ai.unico.platform.recommendation.api.enums.RecombeeScenario;
import ai.unico.platform.recommendation.api.service.RecombeeManipulationService;
import ai.unico.platform.search.api.service.ExpertEvaluationService;
import ai.unico.platform.search.api.service.ItemIndexService;
import ai.unico.platform.search.api.service.SearchMappingsLoaderService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Item;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.ExpertUtil;
import ai.unico.platform.search.util.ItemUtil;
import ai.unico.platform.store.api.dto.ItemIndexDto;
import ai.unico.platform.store.api.enums.IndexHistoryStatus;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.api.service.IndexHistoryService;
import ai.unico.platform.store.api.service.ItemOrganizationService;
import ai.unico.platform.store.api.service.ItemService;
import ai.unico.platform.store.api.service.OrganizationService;
import ai.unico.platform.util.CollectionsUtil;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.model.UserContext;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ItemIndexServiceImpl implements ItemIndexService {

    private boolean indexingInProgress = false;

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private SearchMappingsLoaderService searchMappingsLoaderService;

    private final RecombeeManipulationService recombeeManipulationService = ServiceRegistryProxy.createProxy(RecombeeManipulationService.class);

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;

    @Override
    public void reindexItems(Integer limit, boolean clean, String target, UserContext userContext) throws IOException {
        final String mappingJson = searchMappingsLoaderService.loadMappings("items");
        if (mappingJson == null) throw new FileNotFoundException();

        if (indexingInProgress) return;

        if (clean) {
            try {
                elasticSearchDao.deleteIndex(indexName);
            } catch (Exception ignore) {
            }
            elasticSearchDao.initIndex(indexName, mappingJson);
            System.out.println("creating index " + indexName + " with mappings:");
            System.out.println(mappingJson);
        }

        indexingInProgress = true;
        final IndexHistoryService indexHistoryService = ServiceRegistryUtil.getService(IndexHistoryService.class);
        final ItemOrganizationService itemOrganizationService = ServiceRegistryUtil.getService(ItemOrganizationService.class);
        final Long indexHistoryId = indexHistoryService.saveIndexHistory(IndexTarget.ITEMS, true, userContext);

        try {
            if (target == null || "ALL".equals(target) || "DWH".equals(target)) {
                final DWHItemService DWHItemService = ServiceRegistryUtil.getService(DWHItemService.class);
                List<ExpertItemPreviewDto> itemDtos;
                Integer offset = 0;
                System.out.println("Starting DWH items index");

                final OrganizationService organizationService = ServiceRegistryUtil.getService(OrganizationService.class);
                do {
                    itemDtos = DWHItemService.findItemPreviews(limit, offset);
                    int size = itemDtos.size();
                    if (size == 0) {
                        break;
                    }
                    if (size % 10 !=0)
                        size += 10; //index last items when size%10 != 0
                    final Set<String> expertCodes = new HashSet<>();
                    itemDtos.stream()
                            .map(ExpertItemPreviewDto::getExpertIds)
                            .forEach(x -> x.stream().map(ExpertUtil::convertToExpertCode).forEach(expertCodes::add));
                    final Map<String, String> substituteExpertCodes = ExpertUtil.findSubstituteExpertCodes(expertCodes);
                    for (int i = 0; i < 10; i++) {
                        final List<Item> items = new ArrayList<>();
                        final int start = i * (size / 10);
                        final int end = start + size / 10;
                        final Set<Long> originalOrganizationIds = new HashSet<>();
                        itemDtos.stream().map(ExpertItemPreviewDto::getOriginalOrganizationIds).forEach(originalOrganizationIds::addAll);
                        final Map<Long, Long> organizationMap = organizationService.findOrganizationIdsMap(originalOrganizationIds);
                        final Map<Long, Set<Long>> externalItemOrganizations = itemOrganizationService.getExternalItemOrganizations(itemDtos.stream().map(ExpertItemPreviewDto::getItemId).collect(Collectors.toSet()));
                        for (ExpertItemPreviewDto itemPreviewDto : itemDtos.subList(start, Math.min(end,itemDtos.size()))) {

                            final Item item = ItemUtil.convert(itemPreviewDto);
                            for (Long originalOrganizationId : itemPreviewDto.getOriginalOrganizationIds()) {
                                final Long organizationId = organizationMap.get(originalOrganizationId);
                                if (organizationId != null) item.getOrganizationIds().add(organizationId);
                            }
                            if (externalItemOrganizations.containsKey(item.getItemId())) {
                                item.getOrganizationIds().addAll(externalItemOrganizations.get(item.getItemId()));
                            }

                            item.getEvidenceOrganizationIds().addAll(item.getOrganizationIds());
                            doReplaceExpertCodes(item, substituteExpertCodes);
                            items.add(item);
                        }
                        elasticSearchDao.doIndex(indexName, items);
                    }
                    offset += limit;
                    System.out.println(offset);
                } while (itemDtos.size() > 0);
                System.out.println("DWH index done.");
            }
            if (target == null || "ALL".equals(target) || "ST".equals(target)) {
                final Integer stLimit = Math.toIntExact(limit / 10);
                System.out.println("Starting ST items index.");
                final ItemService userItemService = ServiceRegistryUtil.getService(ItemService.class);
                Integer offset2 = 0;
                List<ItemIndexDto> userItemDtos;

                do {
                    userItemDtos = userItemService.updateAndFindItems(stLimit, offset2);
                    final Set<String> expertCodes = new HashSet<>();
                    userItemDtos.stream().map(this::getAllExpertCodes).forEach(expertCodes::addAll);
                    final Map<String, String> substituteExpertCodes = ExpertUtil.findSubstituteExpertCodes(expertCodes);

                    if (userItemDtos.size() == 0) break;
                    final List<Item> items = userItemDtos.stream()
                            .map(x -> prepareItem(x, substituteExpertCodes))
                            .collect(Collectors.toList());
                    elasticSearchDao.doIndex(indexName, items);
                    offset2 += stLimit;
                    System.out.println(offset2);
                } while (userItemDtos.size() > 0);

                System.out.println("Indexing done");
            }
            replaceExperts(ExpertUtil.findSubstituteExpertCodes(), false);
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FINISHED, userContext);
        } catch (Exception e) {
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FAILED, e.getCause().toString(), userContext);
            e.printStackTrace();
        }
        indexingInProgress = false;
    }

    @Override
    public void indexItem(ItemIndexDto itemIndexDto) throws IOException {
        final Item currentItem = elasticSearchDao.getDocument(indexName, ItemUtil.convertToItemCode(itemIndexDto.getItemId()), Item.class);
        final Map<String, String> substituteExpertCodes = ExpertUtil.findSubstituteExpertCodes(getAllExpertCodes(itemIndexDto));
        ItemOrganizationService itemOrganizationService = ServiceRegistryUtil.getService(ItemOrganizationService.class);
        itemIndexDto.getOrganizationIds().addAll(itemOrganizationService.getExternalItemOrganization(itemIndexDto.getItemId()));
        final Item item = prepareItem(itemIndexDto, substituteExpertCodes);
        System.out.println("Indexing item " + item.getItemId());
        elasticSearchDao.doIndex(indexName, item);
        recombeeManipulationService.uploadEntry(item, RecombeeScenario.ITEM.getPrefix());


        try {
            elasticSearchDao.flush(indexName);
            final ExpertEvaluationService expertEvaluationService = ServiceRegistryUtil.getService(ExpertEvaluationService.class);
            final Set<String> expertCodes;
            if (currentItem != null)
                expertCodes = CollectionsUtil.mergeCollections(item.getAllExpertCodes(), currentItem.getAllExpertCodes());
            else expertCodes = item.getAllExpertCodes();
            expertEvaluationService.recalculateComputedFieldsForExperts(expertCodes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void replaceExperts(Map<String, String> replacementMap, boolean originalOnly) throws IOException {
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder()
                .filter(new TermsQueryBuilder("allExpertCodes", replacementMap.keySet()));
        if (originalOnly) {
            boolQueryBuilder.filter(new TermsQueryBuilder("originalExpertCodes", replacementMap.values()));
        }
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder()
                .query(boolQueryBuilder)
                .size(maxResultWindow);

        final SearchResponseEntityWrapper<Item> searchResponseEntityWrapper = elasticSearchDao.doSearchAndGetHits(ItemIndexService.indexName, sourceBuilder, Item.class);
        if (searchResponseEntityWrapper.getResultCount() == 0) return;
        final List<Item> items = searchResponseEntityWrapper.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource)
                .collect(Collectors.toList());
        for (Item item : items) {
            doReplaceExpertCodes(item, replacementMap);
        }
        elasticSearchDao.doIndex(indexName, items);
    }

    @Override
    public void appendOrganizations(String itemCode) throws IOException {
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder().filter(new TermQueryBuilder("_id", itemCode));
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(boolQueryBuilder)
                .size(1);
        final SearchResponseEntityWrapper<Item> searchResponseEntityWrapper = elasticSearchDao.doSearchAndGetHits(ItemIndexService.indexName, searchSourceBuilder, Item.class);
        if (searchResponseEntityWrapper.getResultCount() == 0) return;
        Item item = searchResponseEntityWrapper.getSearchResponseEntities().get(0).getSource();
        final ItemOrganizationService itemOrganizationService = ServiceRegistryUtil.getService(ItemOrganizationService.class);
        assert itemOrganizationService != null;
        Set<Long> organizationIds = itemOrganizationService.getExternalItemOrganization(IdUtil.getExtDataId(itemCode));
        item.getOrganizationIds().addAll(organizationIds);
        elasticSearchDao.doIndex(indexName, item);
    }

    @Override
    public void flush() throws IOException {
        elasticSearchDao.flush(null);
    }

    private void doReplaceExpertCodes(Item item, Map<String, String> replacementMap) {
        ExpertUtil.replaceExpertCodes(item.getExpertCodes(), replacementMap);
        ExpertUtil.replaceExpertCodes(item.getFavoriteExpertCodes(), replacementMap);
        ExpertUtil.replaceExpertCodes(item.getAllExpertCodes(), replacementMap);
    }

    private Set<String> getAllExpertCodes(ItemIndexDto itemIndexDto) {
        final Set<String> expertCodes = new HashSet<>();
        itemIndexDto.getAllExpertIds().stream().map(ExpertUtil::convertToExpertCode).forEach(expertCodes::add);
        itemIndexDto.getAllUserIds().stream().map(ExpertUtil::convertToUserCode).forEach(expertCodes::add);
        return expertCodes;
    }

    private Item prepareItem(ItemIndexDto itemIndexDto, Map<String, String> expertCodeReplacementMap) {
        final Long originalItemId = itemIndexDto.getOriginalItemId();
        if (originalItemId != null) {
            try {
                elasticSearchDao.deleteDocument(indexName, ItemUtil.convertToDWHItemCode(originalItemId));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        final Item item = ItemUtil.convert(itemIndexDto);
        doReplaceExpertCodes(item, expertCodeReplacementMap);
        return item;
    }
}
