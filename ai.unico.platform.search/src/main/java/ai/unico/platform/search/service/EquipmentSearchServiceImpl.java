package ai.unico.platform.search.service;

import ai.unico.platform.search.api.dto.EquipmentPreviewDto;
import ai.unico.platform.search.api.filter.EquipmentFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.service.EquipmentSearchService;
import ai.unico.platform.search.api.service.ExpertLookupService;
import ai.unico.platform.search.api.service.OrganizationSearchService;
import ai.unico.platform.search.api.wrapper.EquipmentWrapper;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Equipment;
import ai.unico.platform.search.model.EquipmentMultiLanguage;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.EquipmentUtil;
import ai.unico.platform.store.api.dto.*;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.ScoreSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class EquipmentSearchServiceImpl implements EquipmentSearchService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;


    @Autowired
    private OrganizationSearchService organizationSearchService;

    @Autowired
    private ExpertLookupService expertLookupService;

    private BoolQueryBuilder setupFilters(EquipmentFilter equipmentFilter) {
        final BoolQueryBuilder outer = new BoolQueryBuilder();
        final BoolQueryBuilder query = new BoolQueryBuilder();
        if (equipmentFilter.getOrganizationId() != null)
            equipmentFilter.getOrganizationIds().add(equipmentFilter.getOrganizationId());
        if (!equipmentFilter.getOrganizationIds().isEmpty())
            query.must(new TermsQueryBuilder("translations.organizationIds", equipmentFilter.getOrganizationIds()));
        if (equipmentFilter.getEquipmentDomainId() != null)
            query.filter(new TermQueryBuilder("translations.equipmentDomainId", equipmentFilter.getEquipmentDomainId()));
        if (equipmentFilter.getEquipmentTypeId() != null)
            query.filter(new TermQueryBuilder("translations.equipmentTypeId", equipmentFilter.getEquipmentTypeId()));
        if (equipmentFilter.getPortableDevice() != null)
            query.filter(new TermQueryBuilder("translations.portableDevice", equipmentFilter.getPortableDevice()));
        if (equipmentFilter.getYearFrom() != null || equipmentFilter.getYearTo() != null)
            query.filter(new RangeQueryBuilder("translations.year").from(equipmentFilter.getYearFrom()).to(equipmentFilter.getYearTo()));
        if (!equipmentFilter.getIncludeHidden())
            query.filter(new TermQueryBuilder("translations.hidden", false));
        // Create a NestedQueryBuilder to search inside nested documents
        NestedQueryBuilder nested = new NestedQueryBuilder("translations", query, ScoreMode.None);
        outer.must(nested);
        return outer;
    }

    @Override
    public EquipmentWrapper findEquipment(EquipmentFilter equipmentFilter, ExpertSearchResultFilter.Order order) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder query = setupFilters(equipmentFilter);
        if (equipmentFilter.getQuery() != null && !equipmentFilter.getQuery().equals("")) {
            final Map<String, Float> fields = new HashMap<>();
            fields.put("translations.equipmentName", 2.0F);
            fields.put("translations.equipmentDescription", 1.0F);
            fields.put("translations.equipmentSpecialization", 3.0F);
            final QueryStringQueryBuilder queryStringQueryBuilder = new QueryStringQueryBuilder(equipmentFilter.getQueryForSearch())
                    .defaultOperator(Operator.AND).fields(fields);
            NestedQueryBuilder nested = new NestedQueryBuilder("translations", queryStringQueryBuilder, ScoreMode.Avg);
            query.should(nested);
            query.minimumShouldMatch(1);
        }
        searchSourceBuilder.query(query);
        searchSourceBuilder.size(equipmentFilter.getLimit()).from((equipmentFilter.getPage() - 1) * equipmentFilter.getLimit());
        searchSourceBuilder.trackScores(true);
        searchSourceBuilder.sort(new ScoreSortBuilder().order(SortOrder.DESC));

        SearchResponseEntityWrapper<EquipmentMultiLanguage> responseEntityWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, EquipmentMultiLanguage.class);
        EquipmentWrapper equipmentWrapper = new EquipmentWrapper();
        equipmentWrapper.setLimit(equipmentFilter.getLimit());
        equipmentWrapper.setPage(equipmentFilter.getPage());
        equipmentWrapper.setNumberOfAllItems(responseEntityWrapper.getResultCount());
        equipmentWrapper.setOrganizationId(equipmentFilter.getOrganizationId());
        equipmentWrapper.setOrganizationUnitIds(equipmentFilter.getOrganizationIds());
        final Set<Long> organizationIds = responseEntityWrapper.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource)
                .map(x -> x.getTranslations().get(0).getOrganizationIds())
                .flatMap(Collection::stream)
//                .map(Equipment::getOrganizationIds).flatMap(Collection::stream)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        final Map<Long, OrganizationBaseDto> organizationsMap = organizationSearchService.findBaseOrganizationsMap(organizationIds);

        final Set<String> expertCodes = responseEntityWrapper.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource)
                .map(x -> x.getTranslations().get(0).getExpertCodes())
//                .map(Equipment::getExpertCodes)
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
        final Map<String, UserExpertBaseDto> expertBaseDtoMap = new HashMap<>();
        if (expertCodes.size() > 0) {
            final List<UserExpertBaseDto> userExpertBases = expertLookupService.findUserExpertBases(expertCodes);
            userExpertBases.forEach(x -> expertBaseDtoMap.put(x.getExpertCode(), x));
        }

        equipmentWrapper.setMultilingualPreviews(
                responseEntityWrapper.getSearchResponseEntities().stream()
                        .map(SearchResponseEntity::getSource)
                        .map(x -> convert(x, organizationsMap, expertBaseDtoMap))
                        .collect(Collectors.toList()));

        return equipmentWrapper;
    }

    @Override
    public MultilingualDto<EquipmentPreviewDto> getEquipment(String equipmentCode) throws IOException {
        EquipmentMultiLanguage equipment = elasticSearchDao.getDocument(indexName, equipmentCode, EquipmentMultiLanguage.class);
        MultilingualDto<EquipmentPreviewDto> dto = convert(equipment);
        List<OrganizationBaseDto> organizations = new ArrayList<>();
        List<UserExpertBaseDto> experts = new ArrayList<>();
        if (equipment.getTranslations() != null && equipment.getTranslations().size() > 0) {
            Equipment entity = equipment.getTranslations().get(0);
            organizations = organizationSearchService.findBaseOrganizations(entity.getOrganizationIds());
            experts = expertLookupService.findUserExpertBases(entity.getExpertCodes());
            dto.getPreview().setOrganizations(organizations);
            dto.getPreview().setExpertPreviews(experts);
        }
        for (EquipmentPreviewDto previewDto : dto.getTranslations()) {
            previewDto.setOrganizations(organizations);
            previewDto.setExpertPreviews(experts);
        }
        return dto;
    }

    @Override
    public EquipmentRegisterDto getEquipmentForRegister(String equipmentCode) throws IOException {
        Equipment equipment = elasticSearchDao.getDocument(indexName, equipmentCode, Equipment.class);
        return EquipmentUtil.convert(equipment);
    }

    @Override
    public BasicStatisticsDto findEquipmentStatistics(Set<Long> organizationIds) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BasicStatisticsDto basicStatisticsDto = new BasicStatisticsDto();
        final BoolQueryBuilder query = new BoolQueryBuilder()
                .filter(new TermsQueryBuilder("translations.organizationIds", organizationIds));
        final NestedQueryBuilder nested = new NestedQueryBuilder("translations", query, ScoreMode.None);
        final BoolQueryBuilder outer = new BoolQueryBuilder().must(nested);
        searchSourceBuilder.query(outer).size(0);
        final SearchResponseEntityWrapper<Equipment> responseWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Equipment.class);
        basicStatisticsDto.setNumOfAllItems(responseWrapper.getResultCount());
        return basicStatisticsDto;
    }

    private static EquipmentPreviewDto convert(Equipment equipment, Map<Long, OrganizationBaseDto> organizationsMap, Map<String, UserExpertBaseDto> expertBaseDtoMap) {
        EquipmentPreviewDto dto = convert(equipment);
        dto.setOrganizations(equipment.getOrganizationIds().stream().map(organizationsMap::get).collect(Collectors.toList()));
        dto.setExpertPreviews(equipment.getExpertCodes().stream().map(expertBaseDtoMap::get).collect(Collectors.toList()));
        return dto;
    }

    private static MultilingualDto<EquipmentPreviewDto> convert(EquipmentMultiLanguage entity, Map<Long, OrganizationBaseDto> organizationsMap, Map<String, UserExpertBaseDto> expertBaseDtoMap) {
        MultilingualDto<EquipmentPreviewDto> dto = new MultilingualDto<>();
        dto.setId(entity.getEquipmentId());
        dto.setCode(entity.getId());
        if (entity.getTranslations() != null) {
            dto.setPreview(convert(entity.getTranslations().get(0), organizationsMap, expertBaseDtoMap));
        }
        for (Equipment equipment : entity.getTranslations()) {
            dto.getTranslations().add(convert(equipment, organizationsMap, expertBaseDtoMap));
        }
        return dto;

    }

    private static MultilingualDto<EquipmentPreviewDto> convert(EquipmentMultiLanguage entity) {
        MultilingualDto<EquipmentPreviewDto> dto = new MultilingualDto<>();
        dto.setId(entity.getEquipmentId());
        dto.setCode(entity.getId());
        if (entity.getTranslations() != null) {
            dto.setPreview(convert(entity.getTranslations().get(0)));
        }
        for (Equipment equipment : entity.getTranslations()) {
            dto.getTranslations().add(convert(equipment));
        }
        return dto;
    }

    private static EquipmentPreviewDto convert(Equipment equipment) {
        EquipmentPreviewDto equipmentPreviewDto = new EquipmentPreviewDto();
        equipmentPreviewDto.setEquipmentId(equipment.getEquipmentId());
        equipmentPreviewDto.setEquipmentCode(equipment.getId());
        equipmentPreviewDto.setLanguageCode(equipment.getLanguageCode());
        equipmentPreviewDto.setName(equipment.getEquipmentName());
        equipmentPreviewDto.setMarking(equipment.getEquipmentMarking());
        equipmentPreviewDto.setDescription(equipment.getEquipmentDescription());
        if (equipment.getEquipmentSpecialization() != null)
            equipmentPreviewDto.setSpecialization(new ArrayList<>(equipment.getEquipmentSpecialization()));
        equipmentPreviewDto.setYear(equipment.getYear());
        equipmentPreviewDto.setServiceLife(equipment.getEquipmentServiceLife());
        equipmentPreviewDto.setPortableDevice(equipment.getPortableDevice());
        equipmentPreviewDto.setDomainId(equipment.getEquipmentDomainId());
        equipmentPreviewDto.setType(equipment.getEquipmentTypeId());
        equipmentPreviewDto.setHidden(equipment.isHidden());
        equipmentPreviewDto.setGoodForDescription(equipment.getGoodForDescription());
        equipmentPreviewDto.setTargetGroup(equipment.getTargetGroup());
        equipmentPreviewDto.setPreparationTime(equipment.getPreparationTime());
        equipmentPreviewDto.setEquipmentCost(equipment.getEquipmentCost());
        equipmentPreviewDto.setInfrastructureDescription(equipment.getInfrastructureDescription());
        equipmentPreviewDto.setDurationTime(equipment.getDurationTime());
        equipmentPreviewDto.setTrl(equipment.getTrl());
        return equipmentPreviewDto;
    }
}
