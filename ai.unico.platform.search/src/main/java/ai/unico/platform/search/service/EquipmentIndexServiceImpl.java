package ai.unico.platform.search.service;

import ai.unico.platform.search.api.service.EquipmentIndexService;
import ai.unico.platform.search.api.service.SearchMappingsLoaderService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.EquipmentMultiLanguage;
import ai.unico.platform.search.util.EquipmentUtil;
import ai.unico.platform.store.api.dto.EquipmentRegisterDto;
import ai.unico.platform.store.api.dto.MultilingualDto;
import ai.unico.platform.store.api.enums.IndexHistoryStatus;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.api.service.EquipmentService;
import ai.unico.platform.store.api.service.IndexHistoryService;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class EquipmentIndexServiceImpl implements EquipmentIndexService {

    private boolean indexingInProgress = false;

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private SearchMappingsLoaderService searchMappingsLoaderService;

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;

    @Override
    public void reindexEquipment(Integer limit, boolean clean, String target, UserContext userContext) throws IOException {
        final String mappingJson = searchMappingsLoaderService.loadMappings("equipment");
        if (mappingJson == null) throw new FileNotFoundException();

        if (indexingInProgress) return;

        if (clean) {
            try {
                elasticSearchDao.deleteIndex(indexName);
            } catch (Exception ignore) {
            }
            elasticSearchDao.initIndex(indexName, mappingJson);
            System.out.println("creating index " + indexName + " with mappings:");
            System.out.println(mappingJson);
        }

        indexingInProgress = true;
        final IndexHistoryService indexHistoryService = ServiceRegistryUtil.getService(IndexHistoryService.class);
        final Long indexHistoryId = indexHistoryService.saveIndexHistory(IndexTarget.EQUIPMENT, true, userContext);

        try {
//            final OrganizationService organizationService = ServiceRegistryUtil.getService(OrganizationService.class);
//            if (target == null || "ALL".equals(target) || "DWH".equals(target)) {
//                final DWHEquipmentService DWHEquipmentService = ServiceRegistryUtil.getService(DWHEquipmentService.class);
//                List<DWHEquipmentDto> equipmentDtos;
//                Integer offset = 0;
//                System.out.println("Starting DWH equipment index");
//
//                do {
//                    equipmentDtos = DWHEquipmentService.findEquipment(offset, limit);
//                    int size = equipmentDtos.size();
//                    if (size == 0) {
//                        break;
//                    }
//                    if (size % 10 != 0)
//                        size += 10; // index last items when size%10 != 0
//                    for (int i = 0; i < 10; i++) {
//                        final List<Equipment> equipments = new ArrayList<>();
//                        final int start = i * (size / 10);
//                        final int end = start + size / 10;
//                        final Set<Long> originalOrganizationIds = new HashSet<>();
//                        equipmentDtos.stream().map(DWHEquipmentDto::getOrganizationIds).forEach(originalOrganizationIds::addAll);
//                        final Map<Long, Long> organizationMap = organizationService.findOrganizationIdsMap(originalOrganizationIds);
//                        for (DWHEquipmentDto dwhEquipmentDto : equipmentDtos.subList(start, Math.min(end, equipmentDtos.size()))) {
//
//                            final Equipment equipment = EquipmentUtil.convert(dwhEquipmentDto);
//                            for (Long originalOrganizationId : dwhEquipmentDto.getOrganizationIds()) {
//                                final Long organizationId = organizationMap.get(originalOrganizationId);
//                                if (organizationId != null) equipment.getOrganizationIds().add(organizationId);
//                            }
//                            equipments.add(equipment);
//                        }
//                        elasticSearchDao.doIndex(indexName, equipments);
//                    }
//                    offset += limit;
//                    System.out.println(offset);
//                } while (equipmentDtos.size() > 0);
//
//                System.out.println("DWH index done.");
//            }
            if (target == null || "ALL".equals(target) || "ST".equals(target)) {
                System.out.println("Starting ST equipment index.");
                final EquipmentService equipmentService = ServiceRegistryUtil.getService(EquipmentService.class);
                Integer offset2 = 0;
                while (true) {
                    List<MultilingualDto<EquipmentRegisterDto>> equipmentRegisterDtos = equipmentService.findAllEquipmentMultilingual(limit, offset2, false);
                    if (equipmentRegisterDtos.size() == 0) break;
                    List<EquipmentMultiLanguage> equipments = equipmentRegisterDtos.stream()
                            .map(EquipmentUtil::convert)
                            .collect(Collectors.toList());

                    elasticSearchDao.doIndex(indexName, equipments);
                    offset2 += limit;
                }
                // delete all documents that are in both ST and DWH
                Set<String> toDelete = equipmentService.getAllOriginalIds().stream()
                        .filter(Objects::nonNull)
                        .map(IdUtil::convertToExtDataCode)
                        .collect(Collectors.toSet());
                if (! toDelete.isEmpty())
                    elasticSearchDao.deleteDocuments(indexName, toDelete);
            }
                indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FINISHED, userContext);
        } catch (Exception e) {
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FAILED, e.getCause().toString(), userContext);
            e.printStackTrace();
        }
        indexingInProgress = false;
    }

    @Override
    public void reindexEquipment(MultilingualDto<EquipmentRegisterDto> equipmentRegisterDto) throws IOException {
        EquipmentMultiLanguage equipment = EquipmentUtil.convert(equipmentRegisterDto);
        elasticSearchDao.doIndex(indexName, equipment);
    }

    @Override
    public void deleteEquipment(String equipmentCode) throws IOException {
        elasticSearchDao.deleteDocument(indexName, equipmentCode);
    }
}
