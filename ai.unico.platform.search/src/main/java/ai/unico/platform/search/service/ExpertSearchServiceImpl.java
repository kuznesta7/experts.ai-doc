package ai.unico.platform.search.service;

import ai.unico.platform.recommendation.api.interfaces.RecombeeSearchEntity;
import ai.unico.platform.search.api.dto.*;
import ai.unico.platform.search.api.filter.ExpertSearchFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.service.*;
import ai.unico.platform.search.api.wrapper.ExpertInvestmentProjectWrapper;
import ai.unico.platform.search.api.wrapper.ExpertItemWrapper;
import ai.unico.platform.search.api.wrapper.SearchResultWrapper;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Expert;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.ElasticSearchUtil;
import ai.unico.platform.search.util.ExpertUtil;
import ai.unico.platform.search.util.SearchQueryTranslationUtil;
import ai.unico.platform.search.util.SearchRankUtil;
import ai.unico.platform.store.api.dto.FollowedProfilePreviewDto;
import ai.unico.platform.store.api.dto.ItemTypeGroupDto;
import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.filter.FollowedProfilesFilter;
import ai.unico.platform.store.api.service.FollowProfileService;
import ai.unico.platform.store.api.service.ItemTypeService;
import ai.unico.platform.store.api.service.OrganizationStructureService;
import ai.unico.platform.store.api.wrapper.FollowedProfilesWrapper;
import ai.unico.platform.util.CSVUtil;
import ai.unico.platform.util.KeywordsUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.dto.OrganizationDto;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.*;
import org.elasticsearch.index.query.functionscore.FieldValueFactorFunctionBuilder;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScriptScoreFunctionBuilder;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ExpertSearchServiceImpl implements ExpertSearchService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private ItemSearchService itemSearchService;

    @Autowired
    private OrganizationSearchService organizationSearchService;

    @Autowired
    private CommercializationProjectSearchService commercializationProjectSearchService;

    @Autowired
    private OntologySearchService ontologySearchService;

    @Autowired
    private ProjectSearchService projectSearchService;

    @Autowired
    private ThesisSearchService thesisSearchService;

    @Autowired
    private LectureSearchService lectureSearchService;

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;

    @Override
    public SearchResultWrapper searchExperts(ExpertSearchFilter expertSearchFilter, UserContext userContext) throws IOException {
        final OrganizationStructureService organizationStructureService = ServiceRegistryUtil.getService(OrganizationStructureService.class);
        final FollowProfileService followProfileService = ServiceRegistryUtil.getService(FollowProfileService.class);

        final Map<Long, FollowedProfilePreviewDto> followedUserMap = new HashMap<>();
        final Map<Long, FollowedProfilePreviewDto> followedExpertMap = new HashMap<>();

        SearchQueryTranslationUtil.updateFilter(expertSearchFilter);

        if (userContext != null) {
            final FollowedProfilesFilter followedProfilesFilter = new FollowedProfilesFilter();
            followedProfilesFilter.setLimit(null);
            followedProfilesFilter.setPage(null);
            final FollowedProfilesWrapper followedProfilesForUser = followProfileService.findFollowedProfilesForUser(userContext.getUserId(), followedProfilesFilter);
            for (FollowedProfilePreviewDto followedProfilePreviewDto : followedProfilesForUser.getFollowedProfilePreviewDtos()) {
                if (followedProfilePreviewDto.getUserId() == null) {
                    followedExpertMap.put(followedProfilePreviewDto.getExpertId(), followedProfilePreviewDto);
                } else {
                    followedUserMap.put(followedProfilePreviewDto.getUserId(), followedProfilePreviewDto);
                }
            }
        }
        final boolean searchByItemsMode = expertSearchFilter.getQuery() != null && !expertSearchFilter.getQuery().isEmpty();
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.mustNot(new ExistsQueryBuilder("claimedById"));

        final Set<Long> organizationIds = expertSearchFilter.getOrganizationIds();
        final SearchResultWrapper searchResultWrapper = new SearchResultWrapper();
        searchResultWrapper.setPage(expertSearchFilter.getPage());
        searchResultWrapper.setLimit(expertSearchFilter.getLimit());

        final Map<String, SearchResultBucketDto> searchResultBucketMap = new HashMap<>();
        if (searchByItemsMode) {
            ontologySearchService.fillItemSearchOntology(expertSearchFilter);
            if (!expertSearchFilter.isExcludeOutcomes()) {
                final List<SearchResultBucketDto> expertsForItemSearch = itemSearchService.findExpertsForItemSearch(expertSearchFilter);
                final List<SearchResultBucketDto> expertsForItemSearchCut;
                if (expertsForItemSearch.size() > 200) {
                    final Optional<SearchResultBucketDto> best = expertsForItemSearch.stream().max(Comparator.comparing(SearchResultBucketDto::getRank));
                    expertsForItemSearchCut = expertsForItemSearch.stream().filter(r -> r.getRank() > best.get().getRank() / 50).collect(Collectors.toList());
                } else {
                    expertsForItemSearchCut = expertsForItemSearch;
                }
                expertsForItemSearchCut.forEach(x -> searchResultBucketMap.put(x.getExpertCode(), x));
            }
            if (!expertSearchFilter.isExcludeInvestProjects()) {
                final List<SearchResultBucketDto> expertsForProjectSearchBuckets = commercializationProjectSearchService.findExpertsForProjectSearch(expertSearchFilter);
                for (SearchResultBucketDto expertsForProjectSearchBucket : expertsForProjectSearchBuckets) {
                    final String expertCode = expertsForProjectSearchBucket.getExpertCode();
                    if (searchResultBucketMap.containsKey(expertCode)) {
                        final SearchResultBucketDto searchResultBucketDto = searchResultBucketMap.get(expertCode);
                        searchResultBucketDto.setInvestmentProjectAppearance(searchResultBucketDto.getInvestmentProjectAppearance() + expertsForProjectSearchBucket.getInvestmentProjectAppearance());
                        searchResultBucketDto.setRank(searchResultBucketDto.getRank() + expertsForProjectSearchBucket.getRank());
                    } else {
                        searchResultBucketMap.put(expertCode, expertsForProjectSearchBucket);
                    }
                }
            }
            final HashMap<String, Object> params = new HashMap<>();
            params.put("resultBuckets", searchResultBucketMap.values().stream().collect(Collectors.toMap(SearchResultBucketDto::getExpertCode, SearchResultBucketDto::getRank)));
            final Script script = new Script(ScriptType.INLINE, "painless", "if (params.resultBuckets.containsKey(doc['id'].value)) {return params.resultBuckets[doc['id'].value];} return 0;", params);
            final List<FunctionScoreQueryBuilder.FilterFunctionBuilder> filterFunctionBuilders = new ArrayList<>();
            filterFunctionBuilders.add(new FunctionScoreQueryBuilder.FilterFunctionBuilder(new ScriptScoreFunctionBuilder(script)));
            final FunctionScoreQueryBuilder.FilterFunctionBuilder[] filterFunctionBuildersArray = filterFunctionBuilders.toArray(new FunctionScoreQueryBuilder.FilterFunctionBuilder[0]);
            boolQueryBuilder.should(new FunctionScoreQueryBuilder(new MatchAllQueryBuilder(), filterFunctionBuildersArray));
            addDescriptionAndKeywordsSearchFunction(boolQueryBuilder, expertSearchFilter);
            searchResultWrapper.setOntologyItemSearchDtos(convertSecondaryQueries(expertSearchFilter));
        }
        if (!expertSearchFilter.getCountryCodes().isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("countryCodes", expertSearchFilter.getCountryCodes()));
        }
        if (!organizationIds.isEmpty()) {
            final Set<Long> organizationUnitIds = organizationStructureService.findChildOrganizationIds(organizationIds, true);
            organizationIds.addAll(organizationUnitIds);
            boolQueryBuilder.filter(new BoolQueryBuilder().minimumShouldMatch(1)
                    .should(new TermsQueryBuilder("favoriteOrganizationIds", organizationIds))
                    .should(new BoolQueryBuilder()
                            .mustNot(new ExistsQueryBuilder("favoriteOrganizationIds"))
                            .must(new TermsQueryBuilder("organizationIds", organizationIds))));
            sourceBuilder.from((expertSearchFilter.getPage() - 1) * expertSearchFilter.getLimit())
                    .size(expertSearchFilter.getLimit());
        }
        if (expertSearchFilter.getFullNameQuery() != null && !expertSearchFilter.getFullNameQuery().isEmpty()) {
            final FunctionScoreQueryBuilder.FilterFunctionBuilder[] filterFunctionBuilders = {
                    new FunctionScoreQueryBuilder.FilterFunctionBuilder(
                            new FieldValueFactorFunctionBuilder("expertRank")
                                    .factor(1.1F))
            };
            final MatchQueryBuilder fullName = new MatchQueryBuilder("fullName", expertSearchFilter.getFullNameQuery())
                    .operator(Operator.AND);
            boolQueryBuilder.must(new FunctionScoreQueryBuilder(fullName, filterFunctionBuilders).maxBoost(5));
        }

        boolQueryBuilder.must(new MatchAllQueryBuilder().boost(0));
        sourceBuilder.query(boolQueryBuilder)
                .trackScores(true)
                .size(expertSearchFilter.getLimit())
                .from(expertSearchFilter.getLimit() * (expertSearchFilter.getPage() - 1))
                .minScore(0.1F);
        final SearchResponseEntityWrapper<Expert> resultWrapper = elasticSearchDao.doSearchAndGetHits(indexName, sourceBuilder, Expert.class);
        final Map<String, Set<Long>> expertOrganizationIdsMap = new HashMap<>();
        searchResultWrapper.setNumberOfResults(resultWrapper.getResultCount());
        for (SearchResponseEntity<Expert> searchResponseEntity : resultWrapper.getSearchResponseEntities()) {
            final Expert source = searchResponseEntity.getSource();
            final String id = source.getId();
            final UserProfileSearchPreviewDto profilePreviewDto = new UserProfileSearchPreviewDto();
            profilePreviewDto.setCode(id);
            profilePreviewDto.setName(source.getFullName());
            profilePreviewDto.setUserId(source.getUserId());
            profilePreviewDto.setVerified(ExpertUtil.isExpertVerified(source));
            profilePreviewDto.setRetired(source.getRetirementDate() != null);
            profilePreviewDto.setClaimable(source.isClaimable());
            if (source.getUserId() == null) {
                source.getExpertIds().stream().findFirst().ifPresent(profilePreviewDto::setExpertId);
                final FollowedProfilePreviewDto followedProfilePreviewDto = followedExpertMap.get(profilePreviewDto.getExpertId());
                if (followedProfilePreviewDto != null) {
                    profilePreviewDto.setProfileFollow(new UserProfileSearchPreviewDto.ProfileFollow(followedProfilePreviewDto.getWorkspaces()));
                }
            } else {
                final FollowedProfilePreviewDto followedProfilePreviewDto = followedUserMap.get(profilePreviewDto.getUserId());
                if (followedProfilePreviewDto != null) {
                    profilePreviewDto.setProfileFollow(new UserProfileSearchPreviewDto.ProfileFollow(followedProfilePreviewDto.getWorkspaces()));
                }
            }
            final Set<Long> expertOrganizationIds = source.getOrganizationIds();
            final Set<Long> favoriteOrganizationIds = source.getFavoriteOrganizationIds();
            if (favoriteOrganizationIds != null && !favoriteOrganizationIds.isEmpty()) {
                expertOrganizationIdsMap.put(id, favoriteOrganizationIds);
            } else if (expertOrganizationIds != null) {
                expertOrganizationIdsMap.put(id, expertOrganizationIds);
            } else {
                expertOrganizationIdsMap.put(id, Collections.emptySet());
            }
            if (searchResultBucketMap.containsKey(id)) {
                final SearchResultBucketDto searchResultBucketDto = searchResultBucketMap.get(id);
                profilePreviewDto.setRank(SearchRankUtil.recalculateRank(Double.valueOf(searchResponseEntity.getScore())));
                profilePreviewDto.setAppearance(searchResultBucketDto.getAppearance());
                profilePreviewDto.setInvestmentProjectAppearance(searchResultBucketDto.getInvestmentProjectAppearance());
            } else if (searchByItemsMode) {
                profilePreviewDto.setRank(SearchRankUtil.recalculateRank(Double.valueOf(searchResponseEntity.getScore())));
            }
            searchResultWrapper.getUserProfilePreviews().add(profilePreviewDto);
        }

        final Set<Long> allExpertsOrganizationIds = new HashSet<>();
        expertOrganizationIdsMap.values().forEach(allExpertsOrganizationIds::addAll);
        if (!allExpertsOrganizationIds.isEmpty()) {
            final List<OrganizationDto> organizationDtos = organizationStructureService.findRootOrganizationDtos(allExpertsOrganizationIds);
            for (UserProfileSearchPreviewDto userProfilePreview : searchResultWrapper.getUserProfilePreviews()) {
                final Set<Long> expertOrganizationIds = expertOrganizationIdsMap.get(userProfilePreview.getCode());
                if (expertOrganizationIds == null || expertOrganizationIds.isEmpty()) continue;
                for (OrganizationDto organizationDto : organizationDtos) {
                    if (expertOrganizationIds.contains(organizationDto.getOrganizationId())
                            || organizationDto.getOrganizationUnits().stream()
                            .map(OrganizationDto::getOrganizationId)
                            .anyMatch(expertOrganizationIds::contains)) {
                        final OrganizationDto organizationCopyDto = new OrganizationDto();
                        organizationCopyDto.setOrganizationId(organizationDto.getOrganizationId());
                        organizationCopyDto.setOrganizationName(organizationDto.getOrganizationName());
                        organizationCopyDto.setGdpr(organizationDto.getGdpr());
                        organizationCopyDto.getOrganizationUnits().addAll(organizationDto.getOrganizationUnits().stream()
                                .filter(x -> expertOrganizationIds.contains(x.getOrganizationId()))
                                .collect(Collectors.toList()));
                        userProfilePreview.getOrganizationDtos().add(organizationCopyDto);
                    }
                }
            }
        }

        if (searchByItemsMode) {
            searchResultWrapper.getUserProfilePreviews().sort(Comparator.comparing(UserProfileSearchPreviewDto::getRank).reversed());
        }
        return searchResultWrapper;
    }

    @Override
    public UserExpertProfileSearchDetailDto findSearchByExpertiseDetail(String expertCode, ExpertSearchResultFilter searchResultFilter) throws IOException, NonexistentEntityException, IllegalArgumentException {
        final UserExpertProfileSearchDetailDto userExpertProfileSearchDetailDto = new UserExpertProfileSearchDetailDto();

        searchResultFilter.setForceFilterItemTypes(false);

        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        SearchQueryTranslationUtil.updateFilter(searchResultFilter);
        final TermQueryBuilder termQueryBuilder = new TermQueryBuilder("id", expertCode);
        boolQueryBuilder.mustNot(new ExistsQueryBuilder("claimedById"));
        boolQueryBuilder.filter(termQueryBuilder);
        boolQueryBuilder.minimumShouldMatch(0);
        addDescriptionAndKeywordsSearchFunction(boolQueryBuilder, searchResultFilter);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder()
                .query(boolQueryBuilder)
                .from(0)
                .size(1)
                .fetchSource(FetchSourceContext.FETCH_SOURCE);
        final SearchResponseEntityWrapper<Expert> expertSearchResponseEntityWrapper = elasticSearchDao.doSearchAndGetHits(indexName, sourceBuilder, Expert.class);
        if (expertSearchResponseEntityWrapper.getResultCount() == 0) throw new NonexistentEntityException(Expert.class);

        final SearchResponseEntity<Expert> expertSearchResponseEntity = expertSearchResponseEntityWrapper.getSearchResponseEntities().get(0);
        final Expert source = expertSearchResponseEntity.getSource();
        final Set<Long> organizationIds = source.getOrganizationIds();
        userExpertProfileSearchDetailDto.setExpertCode(source.getExpertCode());
        userExpertProfileSearchDetailDto.setName(source.getFullName());
        userExpertProfileSearchDetailDto.setUserId(source.getUserId());
        userExpertProfileSearchDetailDto.setVerified(ExpertUtil.isExpertVerified(source));
        userExpertProfileSearchDetailDto.setRetired(source.getRetirementDate() != null);
        userExpertProfileSearchDetailDto.setKeywords(KeywordsUtil.sortKeywordList(source.getKeywords()));
        userExpertProfileSearchDetailDto.setDescription(source.getDescription());
        userExpertProfileSearchDetailDto.setClaimable(source.isClaimable());
        if (source.getUserId() == null) {
            source.getExpertIds().stream().findFirst().ifPresent(userExpertProfileSearchDetailDto::setExpertId);
        }
        final List<OrganizationBaseDto> baseOrganizations = organizationSearchService.findBaseOrganizations(organizationIds);
        baseOrganizations.removeAll(Collections.singleton(null));
        for (OrganizationBaseDto baseOrganization : baseOrganizations) {
            final UserOrganizationDto userOrganizationDto = new UserOrganizationDto();
            final Long organizationId = baseOrganization.getOrganizationId();
            userOrganizationDto.setOrganizationId(organizationId);
            userOrganizationDto.setOrganizationAbbrev(baseOrganization.getOrganizationAbbrev());
            userOrganizationDto.setOrganizationName(baseOrganization.getOrganizationName());
            userOrganizationDto.setFavorite(source.getFavoriteOrganizationIds().contains(organizationId));
            userOrganizationDto.setVisible(source.getVisibleOrganizationIds().contains(organizationId));
            userOrganizationDto.setActive(source.getActiveOrganizationIds().contains(organizationId));
            if(baseOrganization.getGdpr() != null)
                userOrganizationDto.setGdpr(baseOrganization.getGdpr());
            userExpertProfileSearchDetailDto.getOrganizationDtos().add(userOrganizationDto);
        }
        userExpertProfileSearchDetailDto.getOrganizationDtos().sort(Comparator.comparing(UserOrganizationDto::isFavorite).reversed());
        final ItemTypeService itemTypeService = ServiceRegistryUtil.getService(ItemTypeService.class);
        final List<ItemTypeGroupDto> itemTypeGroups = itemTypeService.findItemTypeGroups();

        final ItemSearchService.ExpertItemStatistics itemStatisticsForExpert = itemSearchService.findItemStatisticsForExpert(expertCode);
        final Map<Long, Long> itemTypeGroupsWithCounts = itemStatisticsForExpert.getItemTypeGroupsWithCounts();
        for (ItemTypeGroupDto itemTypeGroup : itemTypeGroups) {
            final UserExpertProfileSearchDetailDto.ItemTypeGroupWithCountsDto itemTypeGroupWithCountDto = new UserExpertProfileSearchDetailDto.ItemTypeGroupWithCountsDto();
            itemTypeGroupWithCountDto.setCount(itemTypeGroupsWithCounts.get(itemTypeGroup.getItemTypeGroupId()));
            itemTypeGroupWithCountDto.setItemTypeGroupTitle(itemTypeGroup.getItemTypeGroupTitle());
            itemTypeGroupWithCountDto.setItemTypeGroupId(itemTypeGroup.getItemTypeGroupId());
            itemTypeGroupWithCountDto.setItemTypeIds(itemTypeGroup.getItemTypeIds());
            userExpertProfileSearchDetailDto.getItemTypeGroupsWithCount().add(itemTypeGroupWithCountDto);
        }
        userExpertProfileSearchDetailDto.setNumberOfAllItems(itemStatisticsForExpert.getNumberOfItems());

        final CommercializationProjectSearchService.ExpertInvestmentProjectStatistics expertInvestmentProjectStatistics = commercializationProjectSearchService.findExpertInvestmentProjectStatistics(expertCode, Collections.emptySet());
        userExpertProfileSearchDetailDto.setNumberOfInvestmentProjects(expertInvestmentProjectStatistics.getNumberOfInvestmentProjects());

        final ProjectSearchService.ExpertProjectStatistics expertProjectStatistics = projectSearchService.findExpertProjectStatistics(expertCode, Collections.emptySet(), false);
        userExpertProfileSearchDetailDto.setNumberOfProjects(expertProjectStatistics.getNumberOfInvestmentProjects());

        final ThesisSearchService.ExpertThesisStatistics expertThesisStatistics = thesisSearchService.findExpertThesisStatistics(expertCode, Collections.emptySet(), false);
        userExpertProfileSearchDetailDto.setNumberOfAllThesis(expertThesisStatistics.getNumberOfThesis());

        final LectureSearchService.ExpertLectureStatistics expertLectureStatistics = lectureSearchService.findExpertLectureStatistics(expertCode, Collections.emptySet(), false);
        userExpertProfileSearchDetailDto.setNumberOfAllLectures(expertLectureStatistics.getNumberOfLecture());

        ontologySearchService.fillItemSearchOntology(searchResultFilter);
        searchResultFilter.setItemsLimit(0);
        searchResultFilter.setInvestmentProjectsLimit(0);
        final Double itemScore;
        final Double investmentProjectScore;
        final ExpertItemWrapper expertItems = itemSearchService.findExpertItems(expertCode, searchResultFilter);
        userExpertProfileSearchDetailDto.setItemAppearance(expertItems.getNumberOfRelevantItems());
        if (!searchResultFilter.isExcludeOutcomes()) {
            itemScore = expertItems.getScoreSum();
        } else {
            itemScore = 0D;
        }
        final ExpertInvestmentProjectWrapper expertInvestmentProjects = commercializationProjectSearchService.findExpertInvestmentProjects(expertCode, searchResultFilter);
        userExpertProfileSearchDetailDto.setInvestmentProjectAppearance(expertInvestmentProjects.getNumberOfRelevantInvestmentProjects());
        if (!searchResultFilter.isExcludeInvestProjects()) {
            investmentProjectScore = expertInvestmentProjects.getScoreSum();
        } else {
            investmentProjectScore = 0D;
        }
        userExpertProfileSearchDetailDto.setRank(SearchRankUtil.recalculateRank(itemScore + investmentProjectScore + expertSearchResponseEntity.getScore()));
        return userExpertProfileSearchDetailDto;
    }

    @Override
    public FileDto exportSearch(ExpertSearchFilter expertSearchFilter) throws IOException {
        final SearchResultWrapper searchResultWrapper = searchExperts(expertSearchFilter, null);
        final List<UserProfileSearchPreviewDto> userProfilePreviews = searchResultWrapper.getUserProfilePreviews();

        final List<List<String>> rows = new ArrayList<>();
        final ArrayList<String> header = new ArrayList<>();
        header.add("unico code");
        header.add("name");
        header.add("organizations");
        header.add("relevancy[%]");
        header.add("appearance");
        rows.add(header);
        for (UserProfileSearchPreviewDto userProfilePreview : userProfilePreviews) {
            final List<String> row = new ArrayList<>();
            final List<String> organizationNames = userProfilePreview.getOrganizationDtos().stream()
                    .map(OrganizationDto::getOrganizationName)
                    .collect(Collectors.toList());
            final String organizationsString = StringUtils.join(organizationNames, ", ");
            row.add(userProfilePreview.getCode());
            row.add(userProfilePreview.getName());
            row.add(organizationsString);
            row.add(String.format("%1$,.2f", userProfilePreview.getRank()));
            row.add(userProfilePreview.getAppearance().toString());
            rows.add(row);
        }
        return CSVUtil.createFile(rows, "experts-ai_search_" + expertSearchFilter.getQuery(), false);
    }

    private List<OntologyItemSearchDto> convertSecondaryQueries(ExpertSearchFilter expertSearchFilter) {
        final List<OntologyItemSearchDto> ontologyItemSearchDtos = new ArrayList<>();
        final List<String> secondarySearchQueries = expertSearchFilter.getSecondaryQueries();
        final List<Float> secondarySearchWeights = expertSearchFilter.getSecondaryWeights();

        for (int i = 0; i < secondarySearchQueries.size(); i++) {
            final OntologyItemSearchDto ontologyItemSearchDto = new OntologyItemSearchDto();
            ontologyItemSearchDto.setValue(secondarySearchQueries.get(i));
            ontologyItemSearchDto.setRelevancy(secondarySearchWeights.get(i));
            ontologyItemSearchDto.setDisabled(false);
            ontologyItemSearchDtos.add(ontologyItemSearchDto);
        }
        for (int i = 0; i < expertSearchFilter.getDisabledSecondaryQueries().size(); i++) {
            final OntologyItemSearchDto ontologyItemSearchDto = new OntologyItemSearchDto();
            ontologyItemSearchDto.setRelevancy(expertSearchFilter.getDisabledSecondaryWeights().get(i));
            ontologyItemSearchDto.setValue(expertSearchFilter.getDisabledSecondaryQueries().get(i));
            ontologyItemSearchDto.setDisabled(true);
            ontologyItemSearchDtos.add(ontologyItemSearchDto);
        }
        return ontologyItemSearchDtos;
    }

    private void addDescriptionAndKeywordsSearchFunction(BoolQueryBuilder boolQueryBuilder, ExpertSearchFilter expertSearchFilter) {
        if (!expertSearchFilter.getResultTypeGroupIds().isEmpty()) return;
        final Map<String, Float> fields = new HashMap<>();
        fields.put("description", 2.0F);
        fields.put("keywords", 3.0F);
        final QueryStringQueryBuilder qsb = new QueryStringQueryBuilder(expertSearchFilter.getQueryForSearch())
                .defaultOperator(Operator.AND)
                .fields(fields);
        boolQueryBuilder.should(qsb);
        final List<String> secondarySearchQueries = expertSearchFilter.getSecondaryQueries();
        for (int i = 0; i < secondarySearchQueries.size(); i++) {
            final List<Float> secondarySearchWeights = expertSearchFilter.getSecondaryWeights();
            if (secondarySearchWeights.size() <= i) secondarySearchWeights.add(0.5F);
            final String secondaryQuery = secondarySearchQueries.get(i);
            final QueryStringQueryBuilder secondaryQsb = new QueryStringQueryBuilder(ElasticSearchUtil.prepareQueryStringQuery(secondaryQuery))
                    .defaultOperator(Operator.AND)
                    .fields(fields)
                    .boost(secondarySearchWeights.get(i));
            boolQueryBuilder.should(secondaryQsb);
        }
    }

    public Set<RecombeeSearchEntity> findExpertsForOrganization(Set<Long> organizationIds, int limit, int from) throws IOException {
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(new TermsQueryBuilder("organizationIds", organizationIds));
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(boolQueryBuilder)
                .size(limit)
                .from(from);
        final SearchResponseEntityWrapper<Expert> hits = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Expert.class);
        return hits.getSearchResponseEntities().stream().map(SearchResponseEntity::getSource).collect(Collectors.toSet());
    }

}
