package ai.unico.platform.search.service;

import ai.unico.platform.search.api.dto.OntologyItemSearchDto;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.filter.ExpertSearchFilter;
import ai.unico.platform.search.api.service.OntologyIndexService;
import ai.unico.platform.search.api.service.OntologySearchService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.util.TypeConversionUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import org.elasticsearch.index.query.MatchPhraseQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class OntologySearchServiceImpl implements OntologySearchService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Override
    @Deprecated
    public void fillItemSearchOntology(ExpertSearchFilter expertSearchFilter) {
        if (expertSearchFilter.getQuery() == null || expertSearchFilter.getQuery().isEmpty()) {
            return;
        }
        if (expertSearchFilter.getQuery().contains("\"")) {
            expertSearchFilter.setOntologyDisabled(true);
        }
        try {
            final OntologySearchService ontologySearchService = ServiceRegistryUtil.getService(OntologySearchService.class);
            final List<OntologyItemSearchDto> ontologyItemSearchDtos = ontologySearchService.searchOntology(expertSearchFilter.getQuery(), 0.2D, 7);
            if (expertSearchFilter.getSecondaryQueries().size() == 0 && !expertSearchFilter.isOntologyDisabled()) {
                for (OntologyItemSearchDto ontologyItemSearchDto : ontologyItemSearchDtos) {
                    final String value = ontologyItemSearchDto.getValue();

                    if (expertSearchFilter.getDisabledSecondaryQueries().stream()
                            .map(String::toLowerCase)
                            .collect(Collectors.toList()).contains(value.toLowerCase())) continue;

                    expertSearchFilter.getSecondaryQueries().add(value);
                    expertSearchFilter.getSecondaryWeights().add(ontologyItemSearchDto.getRelevancy());
                }
            }
            for (OntologyItemSearchDto ontologyItemSearchDto : ontologyItemSearchDtos) {
                final String value = ontologyItemSearchDto.getValue();
                if (!expertSearchFilter.getSecondaryQueries().stream().map(String::toLowerCase).collect(Collectors.toList())
                        .contains(value.toLowerCase())) {
                    expertSearchFilter.getDisabledSecondaryQueries().add(value);
                    expertSearchFilter.getDisabledSecondaryWeights().add(ontologyItemSearchDto.getRelevancy() * 0.8F);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fillItemSearchOntology(EvidenceFilter evidenceFilter) {
        if (evidenceFilter.getQuery() == null || evidenceFilter.getQuery().isEmpty()) {
            return;
        }
        if (evidenceFilter.getQuery().contains("\"")) {
            evidenceFilter.setOntologyDisabled(true);
        }
        try {
            final OntologySearchService ontologySearchService = ServiceRegistryUtil.getService(OntologySearchService.class);
            final List<OntologyItemSearchDto> ontologyItemSearchDtos = ontologySearchService.searchOntology(evidenceFilter.getQuery(), 0.2D, 7);
            if (evidenceFilter.getAlternateQueries().size() == 0 && !evidenceFilter.isOntologyDisabled()) {
                for (OntologyItemSearchDto ontologyItemSearchDto : ontologyItemSearchDtos) {
                    final String value = ontologyItemSearchDto.getValue();
                    final Float relevancy = ontologyItemSearchDto.getRelevancy();
                    evidenceFilter.getAlternateQueries().put(value, relevancy);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<OntologyItemSearchDto> searchOntology(String query, Double relevancyMin, Integer resultMax) throws IOException {
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(new MatchPhraseQueryBuilder("value", query));

        final SearchHits searchHits = elasticSearchDao.doSearchAndGetHits(OntologyIndexService.indexName, sourceBuilder);

        final Map<String, OntologyItemSearchDto> ontologyItemMap = new HashMap<>();

        for (SearchHit searchHit : searchHits) {
            final Map<String, Object> source = searchHit.getSourceAsMap();
            final List<Map<String, Object>> relatedItems = (List<Map<String, Object>>) source.get("relatedItems");
            for (Map relatedItem : relatedItems) {
                final Float relevancy = TypeConversionUtil.getFloatValue(relatedItem.get("relevancy"));
                if (relevancy == null || relevancy < relevancyMin) continue;

                final String value = (String) relatedItem.get("value");
                if (value.equals(query)) continue;

                final OntologyItemSearchDto existingOntologyItemDto = ontologyItemMap.get(value);
                if (existingOntologyItemDto != null && existingOntologyItemDto.getRelevancy() > relevancy) continue;

                final OntologyItemSearchDto ontologyItemSearchDto = new OntologyItemSearchDto();
                ontologyItemSearchDto.setValue(value);
                ontologyItemSearchDto.setRelevancy(relevancy * 0.9F); // constant to play with
                ontologyItemMap.put(value, ontologyItemSearchDto);
            }
        }
        final List<OntologyItemSearchDto> ontologyItemSearchDtos = new ArrayList<>(ontologyItemMap.values());
        ontologyItemSearchDtos.sort(Comparator.comparing(OntologyItemSearchDto::getRelevancy).reversed());
        return ontologyItemSearchDtos.subList(0, Math.min(ontologyItemSearchDtos.size(), resultMax));
    }
}
