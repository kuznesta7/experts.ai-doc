package ai.unico.platform.search.service;

import ai.unico.platform.recommendation.api.dto.RecommendationWrapperDto;
import ai.unico.platform.recommendation.api.enums.RecombeeScenario;
import ai.unico.platform.recommendation.api.service.RecombeeRecommendationService;
import ai.unico.platform.search.api.dto.*;
import ai.unico.platform.search.api.filter.ProjectFilter;
import ai.unico.platform.search.api.service.*;
import ai.unico.platform.search.api.wrapper.ProjectWrapper;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Project;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.OrganizationUtil;
import ai.unico.platform.search.util.RecombeeFilterConverterUtil;
import ai.unico.platform.search.util.TypeConversionUtil;
import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.dto.OrganizationDetailDto;
import ai.unico.platform.store.api.dto.ProjectProgramPreviewDto;
import ai.unico.platform.store.api.dto.UserExpertBaseDto;
import ai.unico.platform.store.api.service.ProjectProgramService;
import ai.unico.platform.util.KeywordsUtil;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import ai.unico.platform.util.security.SecurityUtil;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filter.FilterAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.histogram.HistogramAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.nested.NestedAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import org.elasticsearch.search.aggregations.metrics.sum.SumAggregationBuilder;
import org.elasticsearch.search.aggregations.support.ValueType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class EvidenceProjectServiceImpl implements EvidenceProjectService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private ExpertLookupService expertLookupService;

    @Autowired
    private OrganizationSearchService organizationSearchService;

    @Autowired
    private EvidenceItemService evidenceItemService;

    @Autowired
    private ItemLookupService itemLookupService;

    private final RecombeeRecommendationService recombeeRecommendationService = ServiceRegistryProxy.createProxy(RecombeeRecommendationService.class);

    @Value("${elasticsearch.settings.maxBucketSize}")
    private Integer maxBucketSize;

    @Override
    public ProjectWrapper findProjects(ProjectFilter projectFilter) throws IOException {
        ProjectWrapper projectWrapper = new ProjectWrapper();
        final Set<Confidentiality> confidentiality = projectFilter.getConfidentiality();
        if (confidentiality.isEmpty()) {
            confidentiality.add(Confidentiality.PUBLIC);
        }
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder boolQueryBuilder = setupFilters(projectFilter);

        if (projectFilter.getQuery() != null && !projectFilter.getQuery().isEmpty()) {
            boolQueryBuilder.must(new MultiMatchQueryBuilder(projectFilter.getQueryForSearch(), "name", "description"));
        }

        searchSourceBuilder.size(projectFilter.getLimit());
        searchSourceBuilder.from(projectFilter.getLimit() * (projectFilter.getPage() - 1));
        searchSourceBuilder.query(boolQueryBuilder);
        searchSourceBuilder.trackScores(true);
        sourceBuilderToEvidenceWrapper(searchSourceBuilder, projectWrapper, projectFilter);
        return projectWrapper;
    }

    private void sourceBuilderToEvidenceWrapper(SearchSourceBuilder searchSourceBuilder, ProjectWrapper projectWrapper, ProjectFilter projectFilter) throws IOException {
        final Long organizationId = projectFilter.getOrganizationId();
        final Set<Long> organizationIds = projectFilter.getOrganizationIds();
        final SearchResponseEntityWrapper<Project> projectSearchResponseEntityWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Project.class);
        final List<SearchResponseEntity<Project>> searchResponseEntities = projectSearchResponseEntityWrapper.getSearchResponseEntities();
        final List<ProjectPreviewDto> projectPreviewDtos = new ArrayList<>();
        final Set<String> expertCodes = searchResponseEntities.stream()
                .map(SearchResponseEntity::getSource)
                .map(Project::getExpertCodes)
                .flatMap(Set::stream)
                .collect(Collectors.toSet());

        final Map<String, UserExpertBaseDto> expertBaseDtoMap = new HashMap<>();
        if (expertCodes.size() > 0) {
            final List<UserExpertBaseDto> userExpertBases = expertLookupService.findUserExpertBases(expertCodes);
            userExpertBases.forEach(x -> expertBaseDtoMap.put(x.getExpertCode(), x));
        }
        final Set<Long> organizationToLoadIds = new HashSet<>();
        searchResponseEntities.stream()
                .map(SearchResponseEntity::getSource)
                .filter(x -> x.getProvidingOrganizationIds() != null)
                .map(Project::getProvidingOrganizationIds)
                .forEach(organizationToLoadIds::addAll);
        searchResponseEntities.stream()
                .map(SearchResponseEntity::getSource)
                .map(Project::getOrganizationIds)
                .forEach(organizationToLoadIds::addAll);
        final Set<Long> projectProgramToLoadIds = searchResponseEntities.stream()
                .map(SearchResponseEntity::getSource)
                .map(Project::getProjectProgramId)
                .collect(Collectors.toSet());
        final ProjectProgramService projectProgramService = ServiceRegistryUtil.getService(ProjectProgramService.class);
        final List<ProjectProgramPreviewDto> projectProgramPreviews = projectProgramService.findPreviews(projectProgramToLoadIds);
        final Map<Long, ProjectProgramPreviewDto> projectProgramMap = projectProgramPreviews.stream()
                .collect(Collectors.toMap(ProjectProgramPreviewDto::getProjectProgramId, Function.identity()));
        final List<OrganizationBaseDto> baseOrganizations = organizationSearchService.findBaseOrganizations(organizationToLoadIds);
        final Map<Long, OrganizationBaseDto> organizationsMap = baseOrganizations.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(OrganizationBaseDto::getOrganizationId, Function.identity()));
        for (SearchResponseEntity<Project> searchResponseEntity : searchResponseEntities) {
            final Project source = searchResponseEntity.getSource();
            final ProjectPreviewDto projectPreviewDto = convertPreview(source);
            final Long projectProgramId = source.getProjectProgramId();
            if (projectProgramId != null && projectProgramMap.containsKey(projectProgramId)) {
                final ProjectProgramPreviewDto projectProgramPreviewDto = projectProgramMap.get(projectProgramId);

                projectPreviewDto.setProjectProgramId(projectProgramPreviewDto.getProjectProgramId());
                projectPreviewDto.setProjectProgramName(projectProgramPreviewDto.getProjectProgramName());
            }
            if (source.getFinances() != null) {
                source.getFinances().stream()
                        .filter(x -> organizationIds.contains(x.getOrganizationId()))
                        .filter(x -> source.getOrganizationIds().contains(x.getOrganizationId()))
                        .map(Project.ProjectOrganizationBudget::getTotal)
                        .filter(Objects::nonNull)
                        .reduce(Double::sum)
                        .ifPresent(projectPreviewDto::setOrganizationBudgetTotal);
            }
            final List<OrganizationBaseDto> organizations = OrganizationUtil.findAndSortOrganizationBases(organizationsMap, source.getOrganizationIds(), organizationId, organizationIds);
            final List<OrganizationBaseDto> providingOrganizations = OrganizationUtil.findAndSortOrganizationBases(organizationsMap, source.getProvidingOrganizationIds(), organizationId, organizationIds);
            projectPreviewDto.getOrganizationBaseDtos().addAll(organizations);
            projectPreviewDto.getOrganizationBaseDtos().addAll(providingOrganizations);

            projectPreviewDto.getProjectExperts().addAll(source.getExpertCodes().stream()
                    .map(expertBaseDtoMap::get)
                    .collect(Collectors.toList()));
            projectPreviewDto.setVerified(source.getVerifiedOrganizationIds().contains(organizationId));
            projectPreviewDto.setUnitVerified(source.getVerifiedOrganizationIds().stream().anyMatch(projectFilter.getOrganizationIds()::contains));
            projectPreviewDtos.add(projectPreviewDto);
        }
        projectWrapper.setNumberOfAllItems(projectSearchResponseEntityWrapper.getResultCount());
        projectWrapper.setLimit(projectFilter.getLimit());
        projectWrapper.setPage(projectFilter.getPage());
        projectWrapper.setProjectPreviewDtos(projectPreviewDtos);
        if (organizationId != null) {
            projectWrapper.setOrganizationId(organizationId);
            projectWrapper.setOrganizationUnitIds(organizationIds.stream()
                    .filter(id -> !organizationId.equals(id))
                    .collect(Collectors.toSet()));
        }
    }

    @Override
    public ProjectWrapper findProjectsWithRecommendation(ProjectFilter projectFilter, OrganizationDetailDto organizationDetailDto) throws IOException {
        final ProjectWrapper projectWrapper = new ProjectWrapper();
        final Set<Confidentiality> confidentiality = projectFilter.getConfidentiality();
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder boolQueryBuilder = setupFilters(projectFilter);
        final RecommendationWrapperDto recommendationWrapper = projectWrapper.getRecommendationWrapperDto();
        if (confidentiality.isEmpty()) {
            confidentiality.add(Confidentiality.PUBLIC);
        }

        String[] recombeeIds;
        try {
            recombeeIds = recombeeRecommendationService.recommend(organizationDetailDto, RecombeeFilterConverterUtil.convert(projectFilter), RecombeeScenario.PROJECT, recommendationWrapper);
        } catch (RuntimeException e ){
            System.out.println("Recommendation with Recombee failed - using Elastic instead");
            return findProjects(projectFilter);
        }

        Long docCount = (long) recombeeRecommendationService.numberOfRecomms(recommendationWrapper.getRecommId());
        boolQueryBuilder.filter(new TermsQueryBuilder("id", recombeeIds));
        searchSourceBuilder.query(boolQueryBuilder).size(projectFilter.getLimit());
        sourceBuilderToEvidenceWrapper(searchSourceBuilder, projectWrapper, projectFilter);
        List<ProjectPreviewDto> sorted = RecombeeFilterConverterUtil.sortByRecombee(projectWrapper.getProjectPreviewDtos(),
                Arrays.stream(recombeeIds).collect(Collectors.toList()),
                projectWrapper.getProjectPreviewDtos().stream().map(ProjectPreviewDto::getProjectCode).collect(Collectors.toList()));
        projectWrapper.setProjectPreviewDtos(sorted);
        projectWrapper.setNumberOfAllItems(docCount);
        return projectWrapper;
    }

    @Override
    public ProjectDetailDto findProject(UserContext userContext, String projectCode) throws IOException {
        final Project document = elasticSearchDao.getDocument(indexName, projectCode, Project.class);
        final Confidentiality confidentiality = document.getConfidentiality();
        final Long ownerOrganizationId = document.getOwnerOrganizationId();
        if (!SecurityUtil.canDetailBeLoaded(userContext, document.getEvidenceOrganizationIds(), ownerOrganizationId, confidentiality)) {
            throw new NonexistentEntityException(Project.class);
        }

        final Set<Long> projectOrganizationIds = document.getOrganizationIds().stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        final Set<Long> providingOrganizationIds = document.getProvidingOrganizationIds().stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());

        final Set<Long> organizationToLoadIds = new HashSet<>();

        if (ownerOrganizationId != null) organizationToLoadIds.add(ownerOrganizationId);
        organizationToLoadIds.addAll(providingOrganizationIds);
        organizationToLoadIds.addAll(projectOrganizationIds);

        final List<OrganizationBaseDto> baseOrganizations = organizationSearchService.findBaseOrganizations(organizationToLoadIds);
        //To avoid null pointer exceptions
        baseOrganizations.removeAll(Collections.singleton(null));
        final Map<Long, OrganizationBaseDto> organizationsMap = baseOrganizations.stream().collect(Collectors.toMap(OrganizationBaseDto::getOrganizationId, Function.identity()));
        final ProjectDetailDto projectDetailDto = convertDetail(document);

        projectDetailDto.setOrganizationBaseDtos(projectOrganizationIds.stream()
                .map(organizationsMap::get)
                .collect(Collectors.toList()));
        projectDetailDto.setProvidingOrganizationBaseDtos(providingOrganizationIds.stream()
                .map(organizationsMap::get)
                .collect(Collectors.toList()));

        if (ownerOrganizationId != null && organizationsMap.containsKey(ownerOrganizationId)) {
            projectDetailDto.setOwnerOrganizationName(organizationsMap.get(ownerOrganizationId).getOrganizationName());
        }
        projectDetailDto.setItemPreviewDtos(evidenceItemService.findAnalyticsItems(document.getItemCodes()));
        return projectDetailDto;
    }

    private BoolQueryBuilder setupFilters(ProjectFilter projectFilter) {
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder().must(new MatchAllQueryBuilder());
        final Set<Confidentiality> confidentiality = projectFilter.getConfidentiality();
        if (confidentiality.isEmpty()) {
            confidentiality.add(Confidentiality.PUBLIC);
        }
        final Set<Long> participatingOrganizationIds = projectFilter.getParticipatingOrganizationIds();
        final Set<String> participatingExpertCodes = projectFilter.getExpertCodes();
        final Set<Long> projectProgramIds = projectFilter.getProjectProgramIds();

        final Long organizationId = projectFilter.getOrganizationId();
        final Set<Long> organizationIds = projectFilter.getOrganizationIds();
        if (organizationId != null && projectFilter.isRestrictOrganization()) {
            boolQueryBuilder.filter(new TermQueryBuilder("evidenceOrganizationIds", organizationId));
        } else if (organizationIds.size() > 0) {
            boolQueryBuilder.filter(new TermsQueryBuilder("evidenceOrganizationIds", organizationIds));
        }
        if (projectFilter.getYearFrom() != null) {
            boolQueryBuilder.filter(new RangeQueryBuilder("endDate").format("yyyy").gte(projectFilter.getYearFrom()));
        }
        if (projectFilter.getYearTo() != null) {
            boolQueryBuilder.filter(new RangeQueryBuilder("startDate").format("yyyy").lte(projectFilter.getYearTo()));
        }
        if (projectFilter.isVerifiedOnly()) {
            if (organizationIds.isEmpty()) {
                boolQueryBuilder.filter(new ExistsQueryBuilder("verifiedOrganizationIds"));
            } else {
                boolQueryBuilder.filter(new TermsQueryBuilder("verifiedOrganizationIds", organizationIds));
            }
        }
        if (projectFilter.isNotVerifiedOnly()) {
            if (organizationIds.isEmpty()) {
                boolQueryBuilder.mustNot(new ExistsQueryBuilder("verifiedOrganizationIds"));
            } else {
                boolQueryBuilder.mustNot(new TermsQueryBuilder("verifiedOrganizationIds", organizationIds));
            }
        }

        if (!organizationIds.isEmpty()) {
            boolQueryBuilder.should(new TermsQueryBuilder("verifiedOrganizationIds", organizationIds));
            if (organizationId != null) {
                boolQueryBuilder.should(new TermQueryBuilder("verifiedOrganizationIds", organizationId));
            }
            boolQueryBuilder.minimumShouldMatch(0);

            final BoolQueryBuilder confidentialityBoolQueryBuilder = new BoolQueryBuilder();
            boolQueryBuilder.filter(confidentialityBoolQueryBuilder
                    .minimumShouldMatch(1)
                    .should(new TermQueryBuilder("confidentiality", Confidentiality.PUBLIC))
                    .should(new TermQueryBuilder("confidentiality", Confidentiality.CONFIDENTIAL)));
            if (confidentiality.contains(Confidentiality.SECRET)) {
                confidentialityBoolQueryBuilder.should(new BoolQueryBuilder()
                        .must(new TermQueryBuilder("confidentiality", Confidentiality.SECRET))
                        .must(new TermsQueryBuilder("ownerOrganizationId", organizationIds)));
            }
        }

        if (!participatingOrganizationIds.isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("organizationIds", participatingOrganizationIds));
        }

        if (!participatingExpertCodes.isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("expertCodes", participatingExpertCodes));
        }
        if (!projectProgramIds.isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("projectProgramId", projectProgramIds));
        }

        boolQueryBuilder.filter(new TermsQueryBuilder("confidentiality", confidentiality));

        return boolQueryBuilder;
    }

    @Override
    public ProjectStatisticsDto findProjectStatistics(Set<Long> organizationIds) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final ProjectStatisticsDto projectStatisticsDto = new ProjectStatisticsDto();
        final int LOWER_FINANCES_YEAR_RANGE = 1980;
        final int UPPER_FINANCES_YEAR_RANGE = new LocalDate().getYear();
        searchSourceBuilder.query(new BoolQueryBuilder()
                .filter(new TermsQueryBuilder("evidenceOrganizationIds", organizationIds))
                .filter(new BoolQueryBuilder().minimumShouldMatch(1)
                        .should(new TermQueryBuilder("confidentiality", Confidentiality.PUBLIC))
                        .should(new TermQueryBuilder("confidentiality", Confidentiality.CONFIDENTIAL))
                        .should(new BoolQueryBuilder()
                                .must(new TermQueryBuilder("confidentiality", Confidentiality.SECRET))
                                .must(new TermsQueryBuilder("ownerOrganizationId", organizationIds)))))
                .size(0)
                .aggregation(new NestedAggregationBuilder("financesAgg", "finances")
                        .subAggregation(new FilterAggregationBuilder("organizationFinancesAgg", new BoolQueryBuilder()
                                .filter(new TermsQueryBuilder("finances.organizationId", organizationIds))
                                .filter(new RangeQueryBuilder("finances.year")
                                        .from(LOWER_FINANCES_YEAR_RANGE)
                                        .to(UPPER_FINANCES_YEAR_RANGE)))
                                .subAggregation(new HistogramAggregationBuilder("yearHistogram")
                                        .field("finances.year")
                                        .interval(1)
                                        .subAggregation(new SumAggregationBuilder("yearSum")
                                                .field("finances.total")))
                                .subAggregation(new SumAggregationBuilder("totalSum")
                                        .field("finances.total"))));
        final SearchResponse searchResponse = elasticSearchDao.doSearch(indexName, searchSourceBuilder);
        projectStatisticsDto.setNumberOfAllProjects(searchResponse.getHits().getTotalHits());
        final Nested aggregation = searchResponse.getAggregations().get("financesAgg");
        final Filter organizationFinances = aggregation.getAggregations().get("organizationFinancesAgg");
        final Sum totalSum = organizationFinances.getAggregations().get("totalSum");
        final Histogram yearFinanceHistogram = organizationFinances.getAggregations().get("yearHistogram");
        final List<? extends Histogram.Bucket> buckets = yearFinanceHistogram.getBuckets();
        for (Histogram.Bucket bucket : buckets) {
            final Sum yearSum = bucket.getAggregations().get("yearSum");
            final Integer year = TypeConversionUtil.getIntegerValue(bucket.getKey());
            final double value = yearSum.getValue();
            projectStatisticsDto.getYearProjectFinancesSumHistogram().put(year, value);
        }
        projectStatisticsDto.setAllProjectsFinancesSum(totalSum.getValue());
        return projectStatisticsDto;
    }

    @Override
    public Map<Long, Double> findProjectStatisticsPerCompany(Set<Long> organizationIds, Integer year) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final Map<Long, Double> idMoney = new HashMap<>();
        searchSourceBuilder.query(new BoolQueryBuilder()
                .must(new TermsQueryBuilder("evidenceOrganizationIds", organizationIds)))
                .size(0)
                .aggregation(new NestedAggregationBuilder("financesAgg", "finances")
                        .subAggregation(new FilterAggregationBuilder("organizationYearFinancesAgg", new BoolQueryBuilder()
                                .filter(new TermQueryBuilder("finances.year", year))
                                .must(new TermsQueryBuilder("finances.organizationId", organizationIds)))
                                .subAggregation(new HistogramAggregationBuilder("organizationHistogram")
                                        .field("finances.organizationId")
                                        .interval(1)
                                        .minDocCount(1)
                                        .subAggregation(new SumAggregationBuilder("organizationSum")
                                                .field("finances.total")))));
        final SearchResponse response = elasticSearchDao.doSearch(indexName, searchSourceBuilder);
        final Nested aggregation = response.getAggregations().get("financesAgg");
        final Filter filter = aggregation.getAggregations().get("organizationYearFinancesAgg");
        final Histogram histogram = filter.getAggregations().get("organizationHistogram");
        final List<? extends Histogram.Bucket> buckets = histogram.getBuckets();
        for (Histogram.Bucket bucket : buckets) {
            final Sum organizationSum = bucket.getAggregations().get("organizationSum");
            final Long organizationId = TypeConversionUtil.getLongValue(bucket.getKey());
            final double value = organizationSum.getValue();
            idMoney.put(organizationId, value);
        }
        return idMoney;
    }

    @Override
    public List<OrganizationCostWrapperDto> findTopPartners(Set<Long> organizationIds, Integer yearFrom, Integer yearTo) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder
                .query(new TermsQueryBuilder("evidenceOrganizationIds", organizationIds))
                .size(0)
                .aggregation(new TermsAggregationBuilder("orgProj", ValueType.LONG)
                        .field("evidenceOrganizationIds")
                        .size(maxBucketSize)
                        .subAggregation(new NestedAggregationBuilder("financesAgg", "finances")
                                .subAggregation(new FilterAggregationBuilder("financesFilter",
                                        new BoolQueryBuilder()
                                                .filter(new TermsQueryBuilder("finances.organizationId", organizationIds))
                                                .filter(new RangeQueryBuilder("finances.year").lte(yearTo).gte(yearFrom)))
                                        .subAggregation(new TermsAggregationBuilder("year", ValueType.NUMBER)
                                                .field("finances.year")
                                                .size(100)
                                                .subAggregation(new SumAggregationBuilder("orgSum")
                                                        .field("finances.total"))))));
        final SearchResponse response = elasticSearchDao.doSearch(indexName, searchSourceBuilder);
        final Map<Integer, List<OrganizationCostDto>> results = new HashMap<>();
        final Set<Long> allOrganizationIds = new HashSet<>();
        final Terms organizations = response.getAggregations().get("orgProj");
        final Map<Integer, Map<Long, Double>> yearToBestOrganizationsMap = new HashMap<>();

        //iterate all organization
        for (Terms.Bucket bucket : organizations.getBuckets()) {
            final Long organizationId = TypeConversionUtil.getLongValue(bucket.getKey());
            if (organizationIds.contains(organizationId)) continue;
            allOrganizationIds.add(organizationId);
            final Nested nested = bucket.getAggregations().get("financesAgg");
            final Filter filter = nested.getAggregations().get("financesFilter");
            final Terms terms = filter.getAggregations().get("year");

            //iterate years
            for (Terms.Bucket yearBucket : terms.getBuckets()) {
                final Integer year = TypeConversionUtil.getIntegerValue(yearBucket.getKey());
                final Sum sum = yearBucket.getAggregations().get("orgSum");
                final Double cost = sum.getValue();

                final Map<Long, Double> organizationToCostsMap = yearToBestOrganizationsMap.computeIfAbsent(year, x -> new HashMap<>());
                organizationToCostsMap.put(organizationId, cost);
            }
        }

        final List<OrganizationCostWrapperDto> wrapperDtos = new ArrayList<>();
        for (Map.Entry<Integer, Map<Long, Double>> yearOrganizationCostsEntry : yearToBestOrganizationsMap.entrySet()) {
            final Integer year = yearOrganizationCostsEntry.getKey();
            final Map<Long, Double> organizationIdToCostsMap = yearOrganizationCostsEntry.getValue();
            final List<Map.Entry<Long, Double>> sorted = organizationIdToCostsMap.entrySet().stream()
                    .sorted((v1, v2) -> Double.compare(v2.getValue(), v1.getValue()))
                    .limit(3)
                    .collect(Collectors.toList());
            final OrganizationCostWrapperDto wrapperDto = new OrganizationCostWrapperDto();
            wrapperDto.setYear(year);
            for (Map.Entry<Long, Double> longDoubleEntry : sorted) {
                final Double cost = longDoubleEntry.getValue();
                final Long organizationId = longDoubleEntry.getKey();
                final OrganizationCostDto organizationCostDto = new OrganizationCostDto();
                organizationCostDto.setOrganizationId(organizationId);
                organizationCostDto.setCost(cost);
                wrapperDto.getValues().add(organizationCostDto);
            }
            wrapperDtos.add(wrapperDto);
        }

        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder().minimumShouldMatch(1);
        for (OrganizationCostWrapperDto wrapperDto : wrapperDtos) {
            final Set<Long> bestOrganizationIds = wrapperDto.getValues().stream().map(OrganizationCostDto::getOrganizationId).collect(Collectors.toSet());
            final Set<Long> otherOrganizationIdsForYear = allOrganizationIds.stream().filter(x -> !bestOrganizationIds.contains(x)).collect(Collectors.toSet());
            final Integer year = wrapperDto.getYear();
            boolQueryBuilder.should(new BoolQueryBuilder()
                    .must(new NestedQueryBuilder("finances", new TermQueryBuilder("finances.year", year), ScoreMode.Total))
                    .must(new TermsQueryBuilder("finances.organizationId", otherOrganizationIdsForYear)));
        }
        final SearchSourceBuilder otherOrganizationSearchSourceBuilder = new SearchSourceBuilder()
                .size(0)
                .query(new TermsQueryBuilder("evidenceOrganizationIds", organizationIds))
                .aggregation(new NestedAggregationBuilder("finances", "finances")
                        .subAggregation(new TermsAggregationBuilder("year", ValueType.NUMBER).field("finances.year")
                                .subAggregation(new FilterAggregationBuilder("organizations", boolQueryBuilder)
                                        .subAggregation(new SumAggregationBuilder("sum").field("finances.total")))));
        final SearchResponse searchResponse = elasticSearchDao.doSearch(indexName, otherOrganizationSearchSourceBuilder);
        final Nested aggregation = searchResponse.getAggregations().get("finances");
        final Terms yearBuckets = aggregation.getAggregations().get("year");
        for (Terms.Bucket yearBucket : yearBuckets.getBuckets()) {
            final Integer year = TypeConversionUtil.getIntegerValue(yearBucket.getKey());
            final Filter organizationsAgg = yearBucket.getAggregations().get("organizations");
            final Sum sumAgg = organizationsAgg.getAggregations().get("sum");
            wrapperDtos.stream()
                    .filter(x -> x.getYear().equals(year))
                    .findFirst()
                    .ifPresent(w -> w.setOthers(sumAgg.getValue()));
        }

        return wrapperDtos;
    }


    private ProjectPreviewDto convertPreview(Project project) {
        final ProjectPreviewDto projectPreviewDto = new ProjectPreviewDto();
        projectPreviewDto.setProjectCode(project.getId());
        projectPreviewDto.setProjectNumber(project.getProjectNumber());
        projectPreviewDto.setProjectId(project.getProjectId());
        projectPreviewDto.setName(project.getName());
        projectPreviewDto.setDescription(project.getDescription());
        projectPreviewDto.setStartDate(project.getStartDate());
        projectPreviewDto.setEndDate(project.getEndDate());
        projectPreviewDto.setOriginalProjectId(project.getOriginalProjectId());
        projectPreviewDto.setKeywords(KeywordsUtil.sortKeywordList(project.getKeywords()));
        projectPreviewDto.setConfidentiality(project.getConfidentiality());
        projectPreviewDto.setProjectLink(project.getProjectLink());
        if (project.getFinances() != null) {
            project.getFinances().stream()
                    .filter(f -> project.getOrganizationIds().contains(f.getOrganizationId()))
                    .map(Project.ProjectOrganizationBudget::getTotal)
                    .filter(Objects::nonNull)
                    .reduce(Double::sum)
                    .ifPresent(projectPreviewDto::setBudgetTotal);
        }
        final List<Project.ProjectOrganizationParentProject> parentProjects = project.getParentProjects();
        for (Project.ProjectOrganizationParentProject parentProject : parentProjects) {
            projectPreviewDto.getParentProjectIds().put(parentProject.getOrganizationId(), parentProject.getProjectCode());
        }
        return projectPreviewDto;
    }

    private ProjectDetailDto convertDetail(Project project) throws IOException {
        final ProjectDetailDto projectDetailDto = new ProjectDetailDto();
        projectDetailDto.setProjectCode(project.getId());
        projectDetailDto.setProjectNumber(project.getProjectNumber());
        projectDetailDto.setProjectId(project.getProjectId());
        projectDetailDto.setName(project.getName());
        projectDetailDto.setDescription(project.getDescription());
        projectDetailDto.setStartDate(project.getStartDate());
        projectDetailDto.setEndDate(project.getEndDate());
        projectDetailDto.setOriginalProjectId(project.getOriginalProjectId());
        projectDetailDto.setKeywords(KeywordsUtil.sortKeywordList(project.getKeywords()));
        projectDetailDto.setConfidentiality(project.getConfidentiality());
        projectDetailDto.setOwnerOrganizationId(project.getOwnerOrganizationId());
        projectDetailDto.setProjectLink(project.getProjectLink());
        if (project.getFinances() != null) {
            project.getFinances().stream()
                    .filter(f -> project.getOrganizationIds().contains(f.getOrganizationId()))
                    .map(Project.ProjectOrganizationBudget::getTotal)
                    .filter(Objects::nonNull)
                    .reduce(Double::sum)
                    .ifPresent(projectDetailDto::setBudgetTotal);
            for (Project.ProjectOrganizationBudget finance : project.getFinances()) {
                if (finance.getOrganizationId() == null) continue;
                final Map<Integer, Double> integerDoubleMap = projectDetailDto.getOrganizationFinanceMap().computeIfAbsent(finance.getOrganizationId(), x -> new HashMap<>());
                integerDoubleMap.put(finance.getYear(), finance.getTotal());
            }
        }
        final List<Project.ProjectOrganizationParentProject> parentProjects = project.getParentProjects();
        for (Project.ProjectOrganizationParentProject parentProject : parentProjects) {
            projectDetailDto.getParentProjectIds().put(parentProject.getOrganizationId(), parentProject.getProjectCode());
        }

        final ProjectProgramService projectProgramService = ServiceRegistryUtil.getService(ProjectProgramService.class);
        final Long projectProgramId = project.getProjectProgramId();
        if (projectProgramId != null) {
            final ProjectProgramPreviewDto preview = projectProgramService.findPreview(projectProgramId);
            projectDetailDto.setProjectProgram(preview);
        }

        projectDetailDto.setProjectExperts(expertLookupService.findUserExpertBases(project.getExpertCodes()));

        return projectDetailDto;
    }
}
