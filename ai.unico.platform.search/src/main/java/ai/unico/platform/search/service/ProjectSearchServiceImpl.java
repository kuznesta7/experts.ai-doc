package ai.unico.platform.search.service;

import ai.unico.platform.search.api.dto.ProjectPreviewDto;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.service.ProjectSearchService;
import ai.unico.platform.search.api.wrapper.ExpertProjectWrapper;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Project;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.store.api.dto.ProjectProgramPreviewDto;
import ai.unico.platform.store.api.service.ProjectProgramService;
import ai.unico.platform.util.KeywordsUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ProjectSearchServiceImpl implements ProjectSearchService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Override
    public ExpertProjectWrapper findExpertProjects(String expertCode, ExpertSearchResultFilter expertSearchResultFilter) throws IOException {
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.filter(new TermQueryBuilder("expertCodes", expertCode));
        boolQueryBuilder.filter(new TermQueryBuilder("confidentiality", Confidentiality.PUBLIC));
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.size(expertSearchResultFilter.getProjectLimit()).from((expertSearchResultFilter.getProjectPage() - 1) * expertSearchResultFilter.getProjectLimit());
        final SearchResponseEntityWrapper<Project> projectSearchResponseEntityWrapper = elasticSearchDao.doSearchAndGetHits(indexName, sourceBuilder, Project.class);
        ExpertProjectWrapper expertProjectWrapper = new ExpertProjectWrapper();
        expertProjectWrapper.setPage(expertSearchResultFilter.getProjectPage());
        expertProjectWrapper.setLimit(expertSearchResultFilter.getLimit());
        expertProjectWrapper.setNumberOfRelevantItems(projectSearchResponseEntityWrapper.getResultCount());

        final List<SearchResponseEntity<Project>> searchResponseEntities = projectSearchResponseEntityWrapper.getSearchResponseEntities();
        final Set<Long> projectProgramIds = searchResponseEntities.stream()
                .map(SearchResponseEntity::getSource)
                .map(Project::getProjectProgramId)
                .collect(Collectors.toSet());
        final Map<Long, ProjectProgramPreviewDto> projectProgramMap = new HashMap<>();

        if (!projectProgramIds.isEmpty()) {
            final ProjectProgramService projectProgramService = ServiceRegistryUtil.getService(ProjectProgramService.class);
            final List<ProjectProgramPreviewDto> projectProgramPreviews = projectProgramService.findPreviews(projectProgramIds);
            projectProgramPreviews.forEach(x -> projectProgramMap.put(x.getProjectProgramId(), x));
        }

        for (SearchResponseEntity<Project> searchResponseEntity : searchResponseEntities) {
            final Project source = searchResponseEntity.getSource();
            final Long projectProgramId = source.getProjectProgramId();
            ProjectPreviewDto projectPreviewDto = new ProjectPreviewDto();
            projectPreviewDto.setProjectId(source.getProjectId());
            projectPreviewDto.setProjectNumber(source.getProjectNumber());
            projectPreviewDto.setOriginalProjectId(source.getOriginalProjectId());
            projectPreviewDto.setName(source.getName());
            projectPreviewDto.setDescription(source.getDescription());
            projectPreviewDto.setStartDate(source.getStartDate());
            projectPreviewDto.setEndDate(source.getEndDate());
            projectPreviewDto.setKeywords(KeywordsUtil.sortKeywordList(source.getKeywords()));
            projectPreviewDto.setProjectProgramId(projectProgramId);
            final ProjectProgramPreviewDto projectProgramPreviewDto = projectProgramMap.get(projectProgramId);
            if (projectProgramPreviewDto != null)
                projectPreviewDto.setProjectProgramName(projectProgramPreviewDto.getProjectProgramName());

            expertProjectWrapper.getProjectPreviewDtos().add(projectPreviewDto);
        }
        return expertProjectWrapper;
    }

    @Override
    public ExpertProjectStatistics findExpertProjectStatistics(String expertCode, Set<Long> organizationIds, boolean countConfidential) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        if (!organizationIds.isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("organizationIds", organizationIds));
        }
        if (!countConfidential) {
            boolQueryBuilder.filter(new TermQueryBuilder("confidentiality", "PUBLIC"));
        }
        boolQueryBuilder.filter(new TermQueryBuilder("expertCodes", expertCode));
        searchSourceBuilder.query(boolQueryBuilder).size(0);
        final SearchResponseEntityWrapper<ExpertProjectStatistics> resultWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, ExpertProjectStatistics.class);
        final ProjectSearchService.ExpertProjectStatistics expertProjectStatistics = new ProjectSearchService.ExpertProjectStatistics();
        expertProjectStatistics.setExpertCode(expertCode);
        expertProjectStatistics.setNumberOfInvestmentProjects(resultWrapper.getResultCount());
        return expertProjectStatistics;
    }
}
