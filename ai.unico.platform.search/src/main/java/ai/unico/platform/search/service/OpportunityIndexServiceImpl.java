package ai.unico.platform.search.service;

import ai.unico.platform.extdata.dto.DWHOpportunityDto;
import ai.unico.platform.extdata.service.DWHOpportunityService;
import ai.unico.platform.recommendation.api.enums.RecombeeScenario;
import ai.unico.platform.recommendation.api.service.RecombeeManipulationService;
import ai.unico.platform.search.api.service.OpportunityIndexService;
import ai.unico.platform.search.api.service.SearchMappingsLoaderService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Opportunity;
import ai.unico.platform.search.util.ExpertUtil;
import ai.unico.platform.search.util.OpportunityUtil;
import ai.unico.platform.store.api.dto.OpportunityRegisterDto;
import ai.unico.platform.store.api.enums.IndexHistoryStatus;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.api.service.*;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class OpportunityIndexServiceImpl implements OpportunityIndexService {

    private boolean indexingInProgress = false;

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private SearchMappingsLoaderService searchMappingsLoaderService;

    private RecombeeManipulationService recombeeManipulationService = ServiceRegistryProxy.createProxy(RecombeeManipulationService.class);

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;

    @Override
    public void reindexOpportunity(Integer limit, boolean clean, String target, UserContext userContext) throws IOException {
        final String mappingJson = searchMappingsLoaderService.loadMappings("opportunity");
        if (mappingJson == null) throw new FileNotFoundException();

        if (indexingInProgress) return;

        if (clean) {
            try {
                elasticSearchDao.deleteIndex(indexName);
            } catch (Exception ignore) {
            }
            elasticSearchDao.initIndex(indexName, mappingJson);
            System.out.println("creating index " + indexName + " with mappings:");
            System.out.println(mappingJson);
        }

        indexingInProgress = true;
        final IndexHistoryService indexHistoryService = ServiceRegistryUtil.getService(IndexHistoryService.class);
        final JobTypeService jobTypeService = ServiceRegistryUtil.getService(JobTypeService.class);
        final OpportunityCategoryService categoryService = ServiceRegistryUtil.getService(OpportunityCategoryService.class);
        final Long indexHistoryId = indexHistoryService.saveIndexHistory(IndexTarget.OPPORTUNITY, clean, userContext);
        jobTypeService.updateJobTypesFromDwh();
        categoryService.updateOpportunityCategoriesFromDWH();
        Map<Long, Long> typeTransaction = categoryService.translateOpportunities();
        Map<Long, Long> jobTransaction = jobTypeService.translateJobTypes();


        try {
            if (target == null || "ALL".equals(target) || "DWH".equals(target)) {
                final DWHOpportunityService opportunityService = ServiceRegistryUtil.getService(DWHOpportunityService.class);
                List<DWHOpportunityDto> opportunityDtos;
                Integer offset = 0;
                System.out.println("Starting DWH opportunity index");

                final OrganizationService organizationService = ServiceRegistryUtil.getService(OrganizationService.class);
                do {
                    opportunityDtos = opportunityService.findOpportunities(limit, offset);
                    int size = opportunityDtos.size();
                    if (size == 0) {
                        break;
                    }
                    final Set<String> expertCodes = new HashSet<>();
                    opportunityDtos.stream()
                            .map(DWHOpportunityDto::getExpertIds)
                            .forEach(x -> x.stream().map(ExpertUtil::convertToExpertCode).forEach(expertCodes::add));
                    final Map<String, String> substituteExpertCodes = ExpertUtil.findSubstituteExpertCodes(expertCodes);
                    final List<Opportunity> opportunities = new ArrayList<>();
                    final Set<Long> originalOrganizationIds = new HashSet<>();
                    opportunityDtos.stream().map(DWHOpportunityDto::getOrganizationIds).forEach(originalOrganizationIds::addAll);
                    final Map<Long, Long> organizationMap = organizationService.findOrganizationIdsMap(originalOrganizationIds);
                    for (DWHOpportunityDto opportunityDto : opportunityDtos) {

                        final Opportunity opportunity = OpportunityUtil.convert(opportunityDto);
                        for (Long originalOrganizationId : opportunityDto.getOrganizationIds()) {
                            final Long organizationId = organizationMap.get(originalOrganizationId);
                            if (organizationId != null) opportunity.getOrganizationIds().add(organizationId);
                        }
                        doReplaceExpertCodes(opportunity, substituteExpertCodes);
                        OpportunityUtil.replaceTypes(opportunity, jobTransaction, typeTransaction);
                        opportunities.add(opportunity);
                    }
                    elasticSearchDao.doIndex(indexName, opportunities);

                    offset += limit;
                    System.out.println(offset);
                } while (opportunityDtos.size() > 0);

                System.out.println("DWH index done.");
            }

            if (target == null || "ALL".equals(target) || "ST".equals(target)) {
                final OpportunityService opportunityService = ServiceRegistryUtil.getService(OpportunityService.class);
                List<OpportunityRegisterDto> dtos;
                Integer offset = 0;
                while (true) {
                    dtos = opportunityService.findOpportunities(limit, offset, true);
                    List<OpportunityRegisterDto> withDeletedDtos = opportunityService.findOpportunities(limit, offset, false);
                    offset += limit;
                    if (dtos.isEmpty())
                        break;
                    elasticSearchDao.doIndex(indexName, dtos.stream().map(this::convert).collect(Collectors.toList()));
                    elasticSearchDao.deleteDocuments(indexName,
                            withDeletedDtos.stream()
                            .map(OpportunityRegisterDto::getOriginalId)
                            .filter(Objects::nonNull)
                            .map(IdUtil::convertToExtDataCode)
                            .collect(Collectors.toList()));
                }
            }

            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FINISHED, userContext);
        } catch (Exception e) {
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FAILED, e.getCause().toString(), userContext);
            e.printStackTrace();
        }
        indexingInProgress = false;
    }

    @Override
    public void reindexOpportunity(OpportunityRegisterDto opportunityDto) throws IOException {
        Opportunity opportunity = convert(opportunityDto);
        System.out.println("uploading from reindexOpportunity");
        elasticSearchDao.doIndex(indexName, opportunity);
        recombeeManipulationService.uploadEntry(opportunity, RecombeeScenario.OPPORTUNITY.getPrefix());
    }

    @Override
    public void deleteOpportunity(String opportunityCode) throws IOException {
        elasticSearchDao.deleteDocument(indexName, opportunityCode);
    }

    private void doReplaceExpertCodes(Opportunity opportunity, Map<String, String> replacementMap) {
        ExpertUtil.replaceExpertCodes(opportunity.getExpertIds(), replacementMap);
    }

    @Override
    public void replaceExperts(Map<String, String> replacementMap, boolean originalOnly) throws IOException {
        throw new NotImplementedException();
    }

    private Opportunity convert(OpportunityRegisterDto dto) {
        Opportunity opportunity = new Opportunity();
        opportunity.setId(dto.getOpportunityCode());
        opportunity.setOpportunityId(dto.getOpportunityCode());
        opportunity.setOriginalId(dto.getOriginalId());
        opportunity.setOpportunityName(dto.getOpportunityName());
        opportunity.setOpportunityDescription(dto.getOpportunityDescription());
        opportunity.setOpportunityKw(dto.getOpportunityKw());
        opportunity.setOpportunitySignupDate(dto.getOpportunitySignupDate());
        opportunity.setOpportunityLocation(dto.getOpportunityLocation());
        opportunity.setOpportunityWage(dto.getOpportunityWage());
        opportunity.setOpportunityTechReq(dto.getOpportunityTechReq());
        opportunity.setOpportunityFormReq(dto.getOpportunityFormReq());
        opportunity.setOpportunityOtherReq(dto.getOpportunityOtherReq());
        opportunity.setOpportunityBenefit(dto.getOpportunityBenefit());
        opportunity.setOpportunityJobStartDate(dto.getOpportunityJobStartDate());
        opportunity.setOpportunityExtLink(dto.getOpportunityExtLink());
        opportunity.setOpportunityHomeOffice(dto.getOpportunityHomeOffice());
        opportunity.setOpportunityType(dto.getOpportunityType());
        opportunity.setJobTypes(dto.getJobTypes());
        opportunity.setOrganizationIds(dto.getOrganizationIds());
        opportunity.setExpertIds(dto.getExpertIds());
        opportunity.setHidden(dto.getHidden());
        return opportunity;
    }
}
