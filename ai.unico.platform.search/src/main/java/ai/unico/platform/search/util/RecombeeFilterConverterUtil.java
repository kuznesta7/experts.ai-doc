package ai.unico.platform.search.util;

import ai.unico.platform.recommendation.api.dto.RecombeeFilterDto;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.filter.OpportunityFilter;
import ai.unico.platform.search.api.filter.ProjectFilter;
import ai.unico.platform.search.api.filter.RecommendationFilter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecombeeFilterConverterUtil {

    public static <T> List<T> sortByRecombee(List<T> toSort, List<String> recombeeIds, List<String> elasticIds)  {
        List<T> result = new ArrayList<>(Collections.nCopies(recombeeIds.size(), null));
        List<Integer> sortList = new ArrayList<>();
        for (String item: elasticIds){
            sortList.add(recombeeIds.indexOf(item));
        }
        int i = 0;
        for (T obj: toSort){
            if (i == sortList.size())
                break;
            result.set(sortList.get(i++), obj);
        }
        result.removeAll(Collections.singleton(null));
        return result;
    }

    public static void setBaseFilters(RecombeeFilterDto dto, RecommendationFilter filter) {
        dto.setLimit(filter.getLimit());
        dto.setPage(filter.getPage());
        dto.setQuery(filter.getQuery());
        dto.setRecommId(filter.getRecommId());
        dto.setUser(filter.getUser());
    }

    public static RecombeeFilterDto convert(EvidenceFilter evidenceFilter) {
        RecombeeFilterDto recombeeFilterDto = new RecombeeFilterDto();
        setBaseFilters(recombeeFilterDto, evidenceFilter);

        recombeeFilterDto.setOrganizationId(evidenceFilter.getOrganizationId());
        recombeeFilterDto.setOrganizationIds(evidenceFilter.getOrganizationIds());
        recombeeFilterDto.setSuborganizationIds(evidenceFilter.getSuborganizationIds());
        recombeeFilterDto.setAffiliatedOrganizationIds(evidenceFilter.getAffiliatedOrganizationIds());
        recombeeFilterDto.setResultTypeGroupIds(evidenceFilter.getResultTypeGroupIds());
        recombeeFilterDto.setExpertCodes(evidenceFilter.getExpertCodes());
        recombeeFilterDto.setParticipatingOrganizationIds(evidenceFilter.getParticipatingOrganizationIds());
        recombeeFilterDto.setYearFrom(evidenceFilter.getYearFrom());
        recombeeFilterDto.setYearTo(evidenceFilter.getYearTo());
        recombeeFilterDto.setVerifiedOnly(evidenceFilter.isVerifiedOnly());
        recombeeFilterDto.setNotVerifiedOnly(evidenceFilter.isNotVerifiedOnly());
        recombeeFilterDto.setRetired(evidenceFilter.isRetired());
        recombeeFilterDto.setRestrictOrganization(evidenceFilter.isRestrictOrganization());
        recombeeFilterDto.setConfidentiality(evidenceFilter.getConfidentiality());
        recombeeFilterDto.setCountryCodes(evidenceFilter.getCountryCodes());
        recombeeFilterDto.setRetirementIgnored(evidenceFilter.isRetirementIgnored());
        recombeeFilterDto.setActiveOnly(evidenceFilter.isActiveOnly());

        return recombeeFilterDto;
    }

    public static RecombeeFilterDto convert(ProjectFilter projectFilter) {
        RecombeeFilterDto dto = new RecombeeFilterDto();
        setBaseFilters(dto, projectFilter);

        dto.setOrganizationId(projectFilter.getOrganizationId());
        dto.setYearFrom(projectFilter.getYearFrom());
        dto.setYearTo(projectFilter.getYearTo());
        dto.setOrganizationIds(projectFilter.getOrganizationIds());
        dto.setConfidentiality(projectFilter.getConfidentiality());
        dto.setExpertCodes(projectFilter.getExpertCodes());
        dto.setProjectProgramIds(projectFilter.getProjectProgramIds());
        dto.setParticipatingOrganizationIds(projectFilter.getParticipatingOrganizationIds());
        dto.setRestrictOrganization(projectFilter.isRestrictOrganization());
        dto.setVerifiedOnly(projectFilter.isVerifiedOnly());
        dto.setNotVerifiedOnly(projectFilter.isNotVerifiedOnly());
        return dto;
    }

    public static RecombeeFilterDto convert(OpportunityFilter opportunityFilter) {
        RecombeeFilterDto dto = new RecombeeFilterDto();
        setBaseFilters(dto, opportunityFilter);

        dto.setOrganizationId(opportunityFilter.getOrganizationId());
        dto.setOpportunityType(opportunityFilter.getOpportunityType());
        dto.setJobType(opportunityFilter.getJobType());
        dto.setOrganizationIds(opportunityFilter.getOrganizationIds());
        return dto;
    }


    }
