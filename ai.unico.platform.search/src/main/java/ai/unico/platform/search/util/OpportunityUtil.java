package ai.unico.platform.search.util;

import ai.unico.platform.extdata.dto.DWHOpportunityDto;
import ai.unico.platform.search.model.Opportunity;
import ai.unico.platform.util.IdUtil;

import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;

public class OpportunityUtil {

    public static Opportunity convert(DWHOpportunityDto dto) {
        Opportunity opportunity = new Opportunity();
        opportunity.setId(IdUtil.convertToExtDataCode(dto.getOpportunityId()));
        opportunity.setOriginalId(dto.getOpportunityId());
        opportunity.setOpportunityId(IdUtil.convertToExtDataCode(dto.getOpportunityId()));
        opportunity.setOpportunityName(dto.getOpportunityName());
        opportunity.setOpportunityDescription(dto.getOpportunityDescription());
        opportunity.setOpportunityKw(new HashSet<>(dto.getOpportunityKw()));
        opportunity.setOpportunitySignupDate(dto.getOpportunitySignupDate());
        opportunity.setOpportunityLocation(dto.getOpportunityLocation());
        opportunity.setOpportunityWage(dto.getOpportunityWage());
        opportunity.setOpportunityTechReq(dto.getOpportunityTechReq());
        opportunity.setOpportunityFormReq(dto.getOpportunityFormReq());
        opportunity.setOpportunityOtherReq(dto.getOpportunityOtherReq());
        opportunity.setOpportunityBenefit(dto.getOpportunityBenefit());
        opportunity.setOpportunityJobStartDate(dto.getOpportunityJobStartDate());
        opportunity.setOpportunityExtLink(dto.getOpportunityExtLink());
        opportunity.setOpportunityHomeOffice(dto.getOpportunityHomeOffice());
        opportunity.setOpportunityType(dto.getOpportunityType());
        opportunity.setJobTypes(dto.getJobTypes());
        opportunity.setExpertIds(dto.getExpertIds().stream().map(IdUtil::convertToExtDataCode).collect(Collectors.toSet()));
        return opportunity;
    }

    public static void replaceTypes(Opportunity opportunity, Map<Long, Long> translateJob, Map<Long, Long> translateType) {
        opportunity.setOpportunityType(translateType.get(opportunity.getOpportunityType()));
        opportunity.setJobTypes(opportunity.getJobTypes().stream().map(translateJob::get).collect(Collectors.toSet()));
    }
}
