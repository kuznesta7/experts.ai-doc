package ai.unico.platform.search.util;

public class TypeConversionUtil {

    public static Long getLongValue(Object numberObj) {
        if (numberObj == null) return null;
        final Number number = (Number) numberObj;
        return number.longValue();
    }

    public static Integer getIntegerValue(Object numberObj) {
        final Number number = (Number) numberObj;
        if (number == null) return null;
        return number.intValue();
    }

    public static Double getDoubleValue(Object numberObj) {
        final Number number = (Number) numberObj;
        if (number == null) return null;
        return number.doubleValue();
    }

    public static Float getFloatValue(Object numberObj) {
        final Number number = (Number) numberObj;
        if (number == null) return null;
        return number.floatValue();
    }
}
