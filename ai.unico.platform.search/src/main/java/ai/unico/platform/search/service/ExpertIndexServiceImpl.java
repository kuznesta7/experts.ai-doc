package ai.unico.platform.search.service;

import ai.unico.platform.extdata.dto.ExpertProfilePreviewDto;
import ai.unico.platform.extdata.service.DWHExpertService;
import ai.unico.platform.recommendation.api.enums.RecombeeScenario;
import ai.unico.platform.recommendation.api.service.RecombeeManipulationService;
import ai.unico.platform.search.api.service.*;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Expert;
import ai.unico.platform.search.util.ExpertUtil;
import ai.unico.platform.store.api.dto.ExpertIndexDto;
import ai.unico.platform.store.api.dto.ExpertOrganizationDto;
import ai.unico.platform.store.api.dto.UserProfileIndexDto;
import ai.unico.platform.store.api.enums.IndexHistoryStatus;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.api.service.*;
import ai.unico.platform.ume.api.service.UserRoleService;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Role;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ExpertIndexServiceImpl implements ExpertIndexService {

    private boolean indexingInProgress = false;

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private SearchMappingsLoaderService searchMappingsLoaderService;

    @Autowired
    private ExpertEvaluationService expertEvaluationService;

    @Autowired
    private ItemIndexService itemIndexService;

    @Autowired
    private ProjectIndexService projectIndexService;

    @Autowired
    private CommercializationProjectIndexService commercializationProjectIndexService;

    private RecombeeManipulationService recombeeManipulationService = ServiceRegistryProxy.createProxy(RecombeeManipulationService.class);

    @Override
    public void reindexExperts(Integer limit, boolean clean, UserContext userContext) throws IOException {

        final String mappingJson = searchMappingsLoaderService.loadMappings("experts");
        if (mappingJson == null) throw new FileNotFoundException();

        if (indexingInProgress) return;


        if (clean) {
            try {
                elasticSearchDao.deleteIndex(indexName);
            } catch (Exception ignore) {
            }
            elasticSearchDao.initIndex(indexName, mappingJson);
        }

        indexingInProgress = true;
        final IndexHistoryService indexHistoryService = ServiceRegistryUtil.getService(IndexHistoryService.class);
        final Long indexHistoryId = indexHistoryService.saveIndexHistory(IndexTarget.EXPERTS, true, userContext);

        try {
            final OrganizationService organizationService = ServiceRegistryUtil.getService(OrganizationService.class);
            final ExpertOrganizationService expertOrganizationService = ServiceRegistryUtil.getService(ExpertOrganizationService.class);
            final List<ExpertOrganizationDto> updatedExpertOrganizations = expertOrganizationService.findExpertOrganizations();
            final Map<Long, Set<ExpertOrganizationDto>> expertOrganizationMap = new HashMap<>();
            for (ExpertOrganizationDto updatedExpertOrganization : updatedExpertOrganizations) {
                final Set<ExpertOrganizationDto> expertOrganizationDtos = expertOrganizationMap.computeIfAbsent(updatedExpertOrganization.getExpertId(), x -> new HashSet<>());
                expertOrganizationDtos.add(updatedExpertOrganization);
            }
            final DWHExpertService dwhExpertService = ServiceRegistryUtil.getService(DWHExpertService.class);
            final ExpertService stExpertService = ServiceRegistryUtil.getService(ExpertService.class);
            List<ExpertProfilePreviewDto> expertDtos;
            final ClaimProfileService claimProfileService = ServiceRegistryUtil.getService(ClaimProfileService.class);
            final Map<Long, Long> expertUserMap = claimProfileService.findClaimedExpertIdUserIdMap();
            final Set<Long> verifiedProfilesToClaimIds = claimProfileService.findVerifiedProfilesToClaimIds();
            final List<ExpertIndexDto> updatedExperts = stExpertService.findUpdatedExperts();
            final Map<Long, ExpertIndexDto> updatedExpertMap = updatedExperts.stream().collect(Collectors.toMap(ExpertIndexDto::getExpertId, Function.identity()));
            Integer offset = 0;
            System.out.println("Starting DWH expert index");
            do {
                expertDtos = dwhExpertService.findExperts(limit, offset);
                final Set<Long> originalOrganizationIds = new HashSet<>();
                expertDtos.stream().map(ExpertProfilePreviewDto::getOriginalOrganizationIds).forEach(originalOrganizationIds::addAll);
                final Map<Long, Long> organizationIdsMap = organizationService.findOrganizationIdsMap(originalOrganizationIds);
                final List<Expert> experts = new ArrayList<>();
                if (expertDtos.size() == 0) break;
                for (ExpertProfilePreviewDto expertDto : expertDtos) {
                    final Expert expert = ExpertUtil.convertToEntity(expertDto);
                    final Long expertId = expertDto.getExpertId();
                    if (expertUserMap.containsKey(expertId)) {
                        final Long userId = expertUserMap.get(expertId);
                        expert.setClaimedById(userId);
                        if (verifiedProfilesToClaimIds.contains(userId)) {
                            expert.setClaimable(false);
                        }
                    }
                    expert.setOrganizationIds(expertDto.getOriginalOrganizationIds().stream()
                            .map(organizationIdsMap::get)
                            .collect(Collectors.toSet()));
                    expert.setVisibleOrganizationIds(expert.getOrganizationIds());
                    expert.setActiveOrganizationIds(expert.getOrganizationIds());
                    if (expertOrganizationMap.containsKey(expertId)) {
                        final Set<ExpertOrganizationDto> expertOrganizationDtos = expertOrganizationMap.get(expertId);
                        for (ExpertOrganizationDto expertOrganizationDto : expertOrganizationDtos) {
                            final Long organizationId = expertOrganizationDto.getOrganizationId();
                            if (expertOrganizationDto.isDeleted()) {
                                expert.getOrganizationIds().remove(organizationId);
                            } else {
                                expert.getOrganizationIds().add(organizationId);
                                if (expertOrganizationDto.isVerified()) {
                                    expert.getVerifiedOrganizationIds().add(organizationId);
                                }
                                if (expertOrganizationDto.isRetired()) {
                                    expert.getRetiredOrganizationIds().add(organizationId);
                                }
                                expert.getVisibleOrganizationIds().add(organizationId);
                                if (! expertOrganizationDto.isVisible()) {
                                    expert.getVisibleOrganizationIds().remove(organizationId);
                                }
                                expert.getActiveOrganizationIds().add(organizationId);
                                if (! expertOrganizationDto.isActive()) {
                                    expert.getActiveOrganizationIds().remove(organizationId);
                                }
                            }
                        }
                    }
                    if (updatedExpertMap.containsKey(expertId)) {
                        final ExpertIndexDto expertIndexDto = updatedExpertMap.get(expertId);
                        expert.setRetirementDate(expertIndexDto.getRetirementDate());
                    }
                    experts.add(expert);
                }
                elasticSearchDao.doIndex(indexName, experts);
                offset += limit;
                System.out.println(offset);
            } while (expertDtos.size() > 0);

            System.out.println("DWH index done. Starting ST expert index");
            final UserProfileService userProfileService = ServiceRegistryUtil.getService(UserProfileService.class);
            Integer offset2 = 0;
            List<UserProfileIndexDto> userProfileDtos;
            final int stLimit = limit / 40;
            do {
                userProfileDtos = userProfileService.findUserProfiles(stLimit, offset2);
                if (userProfileDtos.size() == 0) break;
                doIndexUsers(userProfileDtos);
                offset2 += stLimit;
                System.out.println(offset2);
            } while (userProfileDtos.size() > 0);
            indexingInProgress = false;
            System.out.println("Indexing done");
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FINISHED, userContext);

            expertEvaluationService.evaluateExperts(Math.toIntExact(limit), userContext);
        } catch (Exception e) {
            indexingInProgress = false;
            e.printStackTrace();
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FAILED, e.getCause().toString(), userContext);
        }
    }

    private void doIndexUsers(List<UserProfileIndexDto> userProfileDtos) throws IOException {
        final UserRoleService userRoleService = ServiceRegistryUtil.getService(UserRoleService.class);

        final Set<Long> userIds = userProfileDtos.stream().map(UserProfileIndexDto::getUserId).collect(Collectors.toSet());
        final Map<Long, List<Role>> rolesForUsers = userRoleService.findRolesForUsers(userIds);

        for (UserProfileIndexDto userProfileIndexDto : userProfileDtos) {
            final List<Role> roles = rolesForUsers.getOrDefault(userProfileIndexDto.getUserId(), Collections.emptyList());
            userProfileIndexDto.setRoles(new HashSet<>(roles));
        }

        final List<Expert> experts = new ArrayList<>();
        for (UserProfileIndexDto userProfileDto : userProfileDtos) {
            if (userProfileDto.isInvitation()) continue;
            final Expert expert = ExpertUtil.convertToEntity(userProfileDto);
            experts.add(expert);
        }
        elasticSearchDao.doIndex(indexName, experts);
    }

    @Override
    public void updateUser(UserProfileIndexDto userProfileIndexDto) throws IOException {
        if (userProfileIndexDto.isInvitation()) {
            return;
        }
        final Expert expert = ExpertUtil.convertToEntity(userProfileIndexDto);
        System.out.println("Updating user " + expert.getId());
        elasticSearchDao.doIndex(indexName, expert);
        recombeeManipulationService.uploadEntry(expert, RecombeeScenario.EXPERT.getPrefix());
    }

    @Override
    public void replaceExpert(Long expertToReplaceId, Long userId) throws IOException {
        final Map<String, String> expertCodesMap = new HashMap<>();
        expertCodesMap.put(ExpertUtil.convertToExpertCode(expertToReplaceId), ExpertUtil.convertToUserCode(userId));
        doReplace(expertCodesMap, false);
    }

    @Override
    public void replaceUser(Long userToReplaceId, Long userId) throws IOException {
        final Map<String, String> expertCodesMap = new HashMap<>();
        expertCodesMap.put(ExpertUtil.convertToUserCode(userToReplaceId), ExpertUtil.convertToUserCode(userId));
        doReplace(expertCodesMap, false);
    }

    @Override
    public void revertReplaceUser(Long userId, Long replacedUserId) throws IOException {
        final Map<String, String> expertCodesMap = new HashMap<>();
        expertCodesMap.put(ExpertUtil.convertToUserCode(userId), ExpertUtil.convertToUserCode(replacedUserId));
        doReplace(expertCodesMap, true);
    }

    @Override
    public void revertReplaceExpert(Long userId, Long replacedExpertId) throws IOException {
        final Map<String, String> expertCodesMap = new HashMap<>();
        expertCodesMap.put(ExpertUtil.convertToUserCode(userId), ExpertUtil.convertToExpertCode(replacedExpertId));
        doReplace(expertCodesMap, true);
    }

    @Override
    public void updateExpertOrganization(Long expertId, Long organizationId, boolean deleted, boolean verified) throws IOException {
        final ItemAffiliationService associationService = ServiceRegistryUtil.getService(ItemAffiliationService.class);
        final Expert expert = elasticSearchDao.getDocument(indexName, ExpertUtil.convertToExpertCode(expertId), Expert.class);
        if (deleted) {
            expert.getOrganizationIds().remove(organizationId);
        } else {
            expert.getOrganizationIds().add(organizationId);
            if (verified) {
                expert.getVerifiedOrganizationIds().add(organizationId);
            } else {
                expert.getVerifiedOrganizationIds().remove(organizationId);
            }
        }
        elasticSearchDao.doIndex(indexName, expert);
        associationService.updateItemsForExpert(Collections.singleton(expertId), organizationId, !deleted);
    }

    @Override
    public void retireExpertOrganization(Long expertId, Long organizationId, boolean retired) throws IOException {
        final Expert expert = elasticSearchDao.getDocument(indexName, ExpertUtil.convertToExpertCode(expertId), Expert.class);
        if (retired) {
            expert.getRetiredOrganizationIds().add(organizationId);
        } else {
            expert.getRetiredOrganizationIds().remove(organizationId);
        }
        elasticSearchDao.doIndex(indexName, expert);
        recombeeManipulationService.uploadEntry(expert, RecombeeScenario.EXPERT.getPrefix());

    }

    @Override
    public void changeVisibilityInOrganization(Long expertId, Long organizationId, boolean visible) throws IOException {
        final Expert expert = elasticSearchDao.getDocument(indexName, ExpertUtil.convertToExpertCode(expertId), Expert.class);
        if (visible) {
            expert.getVisibleOrganizationIds().add(organizationId);
        } else {
            expert.getVisibleOrganizationIds().remove(organizationId);
        }
        elasticSearchDao.doIndex(indexName, expert);
        recombeeManipulationService.uploadEntry(expert, RecombeeScenario.EXPERT.getPrefix());
    }

    @Override
    public void changeActivityInOrganization(Long expertId, Long organizationId, boolean active) throws IOException {
        final Expert expert = elasticSearchDao.getDocument(indexName, ExpertUtil.convertToExpertCode(expertId), Expert.class);
        if (active) {
            expert.getActiveOrganizationIds().add(organizationId);
        } else {
            expert.getActiveOrganizationIds().remove(organizationId);
        }
        elasticSearchDao.doIndex(indexName, expert);
        recombeeManipulationService.uploadEntry(expert, RecombeeScenario.EXPERT.getPrefix());
    }

    @Override
    public void updateExpert(ExpertIndexDto expertIndexDto) throws IOException {
        final Map<String, Object> retirementDate = new HashMap<>();
        retirementDate.put(ExpertUtil.convertToExpertCode(expertIndexDto.getExpertId()), expertIndexDto.getRetirementDate() != null ? expertIndexDto.getRetirementDate().getTime() : null);
        elasticSearchDao.doUpdate(indexName, "retirementDate", retirementDate);
    }

    @Override
    public void flush() throws IOException {
        elasticSearchDao.flush(null);
    }

    private void doReplace(Map<String, String> expertCodesMap, boolean originalOnly) throws IOException {
        final Collection<String> replacementExpertCodes = expertCodesMap.values();
        final Set<String> expertToReplaceCodes = expertCodesMap.keySet();
        itemIndexService.replaceExperts(expertCodesMap, originalOnly);
        projectIndexService.replaceExperts(expertCodesMap, originalOnly);
        commercializationProjectIndexService.replaceExperts(expertCodesMap, originalOnly);

        elasticSearchDao.flush(null);

        final List<Expert> expertsToReplace = elasticSearchDao.getDocuments(indexName, expertToReplaceCodes, Expert.class);
        for (Expert expert : expertsToReplace) {
            final Long userId = ExpertUtil.getUserId(expertCodesMap.get(expert.getExpertCode()));
            if (expert.isClaimable()) expert.setClaimedById(userId);
        }
        elasticSearchDao.doIndex(indexName, expertsToReplace);

        final UserProfileService userProfileService = ServiceRegistryUtil.getService(UserProfileService.class);
        final Set<Long> userIds = replacementExpertCodes.stream().map(ExpertUtil::getUserId).collect(Collectors.toSet());
        userProfileService.loadAndIndexUserProfiles(userIds);
        flush();

        final Set<String> expertCodes = new HashSet<>();
        expertCodes.addAll(expertCodesMap.keySet());
        expertCodes.addAll(expertCodesMap.values());
        expertEvaluationService.recalculateComputedFieldsForExperts(expertCodes);
        flush();
    }
}
