package ai.unico.platform.search.model;

import java.util.ArrayList;
import java.util.List;

public class EquipmentMultiLanguage extends ElasticSearchModel {
    private Long equipmentId;
    private List<Equipment> translations = new ArrayList<>();

    public Long getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(Long equipmentId) {
        this.equipmentId = equipmentId;
    }

    public List<Equipment> getTranslations() {
        return translations;
    }

    public void setTranslations(List<Equipment> translations) {
        this.translations = translations;
    }
}
