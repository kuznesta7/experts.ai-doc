package ai.unico.platform.search.model;

import ai.unico.platform.recommendation.api.enums.RecombeeColumn;
import ai.unico.platform.recommendation.api.interfaces.RecombeeSearchEntity;
import ai.unico.platform.search.api.dto.EvidenceItemPreviewDto;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.service.EvidenceItemService;
import ai.unico.platform.search.api.wrapper.EvidenceItemWrapper;
import ai.unico.platform.util.ServiceRegistryUtil;

import java.util.*;

public class Expert extends ElasticSearchModel implements RecombeeSearchEntity {

    private Long userId;

    private Set<Long> expertIds = new HashSet<>();

    private String expertCode;

    private String fullName;

    private String username;

    private String email;

    private String description;

    private List<String> keywords = new ArrayList<>();

    private Set<Long> organizationIds = new HashSet<>();

    private Set<Long> verifiedOrganizationIds = new HashSet<>();

    private Set<Long> favoriteOrganizationIds = new HashSet<>();

    private Set<Long> retiredOrganizationIds = new HashSet<>();

    private Set<Long> visibleOrganizationIds = new HashSet<>();

    private Set<Long> activeOrganizationIds = new HashSet<>();

    private boolean claimable;

    private Long claimedById;

    private Date retirementDate;

    private Double expertRank = 0D;

    private Set<String> countryCodes = new HashSet<>();

    private Set<Long> affiliatedOrganizationIds = new HashSet<>();

    private Set<String> roles = new HashSet<>();

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Set<Long> getExpertIds() {
        return expertIds;
    }

    public void setExpertIds(Set<Long> expertIds) {
        this.expertIds = expertIds;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getExpertCode() {
        return expertCode;
    }

    public void setExpertCode(String expertCode) {
        this.expertCode = expertCode;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public boolean isClaimable() {
        return claimable;
    }

    public void setClaimable(boolean claimable) {
        this.claimable = claimable;
    }

    public Long getClaimedById() {
        return claimedById;
    }

    public void setClaimedById(Long claimedById) {
        this.claimedById = claimedById;
    }

    public Double getExpertRank() {
        return expertRank;
    }

    public void setExpertRank(Double expertRank) {
        this.expertRank = expertRank;
    }

    public Set<Long> getVerifiedOrganizationIds() {
        return verifiedOrganizationIds;
    }

    public void setVerifiedOrganizationIds(Set<Long> verifiedOrganizationIds) {
        this.verifiedOrganizationIds = verifiedOrganizationIds;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Long> getFavoriteOrganizationIds() {
        return favoriteOrganizationIds;
    }

    public void setFavoriteOrganizationIds(Set<Long> favoriteOrganizationIds) {
        this.favoriteOrganizationIds = favoriteOrganizationIds;
    }

    public Date getRetirementDate() {
        return retirementDate;
    }

    public void setRetirementDate(Date retirementDate) {
        this.retirementDate = retirementDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Set<Long> getAffiliatedOrganizationIds() {
        return affiliatedOrganizationIds;
    }

    public void setAffiliatedOrganizationIds(Set<Long> affiliatedOrganizationIds) {
        this.affiliatedOrganizationIds = affiliatedOrganizationIds;
    }

    public Set<String> getCountryCodes() {
        return countryCodes;
    }

    public void setCountryCodes(Set<String> countryCodes) {
        this.countryCodes = countryCodes;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public Set<Long> getRetiredOrganizationIds() {
        return retiredOrganizationIds;
    }

    public void setRetiredOrganizationIds(Set<Long> retiredOrganizationIds) {
        this.retiredOrganizationIds = retiredOrganizationIds;
    }

    public Set<Long> getVisibleOrganizationIds() {
        return visibleOrganizationIds;
    }

    public void setVisibleOrganizationIds(Set<Long> visibleOrganizationIds) {
        this.visibleOrganizationIds = visibleOrganizationIds;
    }

    public Set<Long> getActiveOrganizationIds() {
        return activeOrganizationIds;
    }

    public void setActiveOrganizationIds(Set<Long> activeOrganizationIds) {
        this.activeOrganizationIds = activeOrganizationIds;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put(RecombeeColumn.ID.getColumnName(), getUserId());
        map.put(RecombeeColumn.FULL_NAME.getColumnName(), getFullName());
        map.put(RecombeeColumn.DESCRIPTION.getColumnName(), getDescription());
        map.put(RecombeeColumn.IS_EXPERT.getColumnName(), true);
        map.put(RecombeeColumn.KEYWORDS.getColumnName(), getKeywords());
        map.put(RecombeeColumn.RETIREMENT_DATE.getColumnName(), getRetirementDate());
        map.put(RecombeeColumn.CLAIMED_BY_ID.getColumnName(), getClaimedById());
        map.put(RecombeeColumn.COUNTRY_CODES.getColumnName(), getCountryCodes());
        map.put(RecombeeColumn.ORGANIZATION_IDS.getColumnName(), getOrganizationIds());
        map.put(RecombeeColumn.VERIFIED_ORGANIZATION_IDS.getColumnName(), getVerifiedOrganizationIds());
        map.put(RecombeeColumn.RETIRED_ORGANIZATION_IDS.getColumnName(), getRetiredOrganizationIds());
        final EvidenceItemService service = ServiceRegistryUtil.getService(EvidenceItemService.class);
        final EvidenceFilter filter = new EvidenceFilter();
        filter.setLimit(200);
        filter.setExpertCodes(Collections.singleton(getExpertCode()));
        try {
            final EvidenceItemWrapper items = service.findAnalyticsItems(filter);
            Set<String> names = new HashSet<>();
            Set<String> keywords = new HashSet<>();
            for (EvidenceItemPreviewDto dto : items.getItemPreviewDtos()) {
                names.add(dto.getItemName());
                keywords.addAll(dto.getKeywords());
            }
            map.put(RecombeeColumn.MERGED_NAMES.getColumnName(), names);
            map.put(RecombeeColumn.MERGED_KEYWORDS.getColumnName(), keywords);
        } catch (Exception e) {
            System.out.printf("Error when uploading merged Expert's description and merged keywords. Continuing... Error message: " + e.getMessage());
            e.printStackTrace();
        }
        return map;
    }
}
