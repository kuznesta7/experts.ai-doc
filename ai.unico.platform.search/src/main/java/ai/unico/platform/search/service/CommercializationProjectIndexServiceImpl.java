package ai.unico.platform.search.service;

import ai.unico.platform.search.api.service.CommercializationProjectIndexService;
import ai.unico.platform.search.api.service.ExpertEvaluationService;
import ai.unico.platform.search.api.service.SearchMappingsLoaderService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.CommercializationProject;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.ExpertUtil;
import ai.unico.platform.store.api.dto.CommercializationProjectIndexDto;
import ai.unico.platform.store.api.enums.IndexHistoryStatus;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.api.service.CommercializationProjectService;
import ai.unico.platform.store.api.service.IndexHistoryService;
import ai.unico.platform.store.api.service.OrganizationService;
import ai.unico.platform.util.CollectionsUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.dto.OrganizationDto;
import ai.unico.platform.util.model.UserContext;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class CommercializationProjectIndexServiceImpl implements CommercializationProjectIndexService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private SearchMappingsLoaderService searchMappingsLoaderService;

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;

    private boolean indexingInProgress = false;

    @Override
    public void reindexInvestmentProjects(Integer limit, UserContext userContext) throws IOException {
        if (indexingInProgress) return;
        indexingInProgress = true;
        final String mappingJson = searchMappingsLoaderService.loadMappings("investment-projects");
        if (mappingJson == null) throw new FileNotFoundException();
        try {
            elasticSearchDao.deleteIndex(indexName);
        } catch (Exception ignore) {
        }
        elasticSearchDao.initIndex(indexName, mappingJson);
        final IndexHistoryService indexHistoryService = ServiceRegistryUtil.getService(IndexHistoryService.class);
        final Long indexHistoryId = indexHistoryService.saveIndexHistory(IndexTarget.COMMERCIALIZATION_PROJECTS, true, userContext);

        try {
            final CommercializationProjectService commercializationProjectService = ServiceRegistryUtil.getService(CommercializationProjectService.class);
            final OrganizationService organizationService = ServiceRegistryUtil.getService(OrganizationService.class);
            Integer offset = 0;
            System.out.println("Starting commercialization project index");
            while (true) {
                final List<CommercializationProjectIndexDto> dtos = commercializationProjectService.findCommercializationProjects(limit, offset);
                if (dtos.size() == 0) break;
                final Set<Long> organizationIds = new HashSet<>();
                dtos.stream().map(CommercializationProjectIndexDto::getCommercializationOrganizationRelationMap).forEach(map -> map.values().forEach(organizationIds::addAll));
                final List<OrganizationDto> organizationsByIds = organizationService.findOrganizationsByIds(organizationIds);
                final Map<Long, Long> organizationSubstitutionsMap = organizationsByIds.stream()
                        .filter(x -> x.getSubstituteOrganizationId() != null)
                        .collect(Collectors.toMap(OrganizationDto::getOrganizationId, OrganizationDto::getSubstituteOrganizationId));

                for (CommercializationProjectIndexDto dto : dtos) {
                    for (Set<Long> commercializationOrganizationIds : dto.getCommercializationOrganizationRelationMap().values()) {
                        final Set<Long> replacedOrganizationIds = commercializationOrganizationIds.stream()
                                .filter(organizationSubstitutionsMap::containsKey)
                                .collect(Collectors.toSet());
                        commercializationOrganizationIds.removeAll(replacedOrganizationIds);
                        commercializationOrganizationIds.addAll(replacedOrganizationIds.stream()
                                .map(organizationSubstitutionsMap::get)
                                .collect(Collectors.toSet()));
                    }
                }

                final List<CommercializationProject> projectsToIndex = dtos.stream().map(this::convert).collect(Collectors.toList());
                final Set<String> allExpertCodes = new HashSet<>();
                projectsToIndex.stream()
                        .map(CommercializationProject::getExpertCodes)
                        .forEach(allExpertCodes::addAll);

                final Map<String, String> substitutionMap = ExpertUtil.findSubstituteExpertCodes(allExpertCodes);
                projectsToIndex.stream()
                        .map(CommercializationProject::getExpertCodes)
                        .forEach(x -> ExpertUtil.replaceExpertCodes(x, substitutionMap));
                projectsToIndex.stream()
                        .map(CommercializationProject::getTeamLeaderExpertCodes)
                        .forEach(x -> ExpertUtil.replaceExpertCodes(x, substitutionMap));

                elasticSearchDao.doIndex(indexName, projectsToIndex);
                offset += limit;
                System.out.println(offset);
            }
            replaceExperts(ExpertUtil.findSubstituteExpertCodes(), false);
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FINISHED, userContext);
            System.out.println("Commercialization project index finished");
        } catch (Exception e) {
            e.printStackTrace();
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FAILED, e.getCause().toString(), userContext);
        }
        indexingInProgress = false;
    }

    @Override
    public void indexCommercializationProject(CommercializationProjectIndexDto commercializationProjectIndexDto) throws IOException {
        final String id = String.valueOf(commercializationProjectIndexDto.getCommercializationProjectId());
        final CommercializationProject currentCommercialization = elasticSearchDao.getDocument(indexName, id, CommercializationProject.class);
        final CommercializationProject commercializationProject = convert(commercializationProjectIndexDto);
        final Set<String> expertCodes = commercializationProject.getExpertCodes();
        final Map<String, String> substituteExpertCodes = ExpertUtil.findSubstituteExpertCodes(expertCodes);
        ExpertUtil.replaceExpertCodes(commercializationProject.getExpertCodes(), substituteExpertCodes);
        ExpertUtil.replaceExpertCodes(commercializationProject.getTeamLeaderExpertCodes(), substituteExpertCodes);
        elasticSearchDao.doIndex(indexName, commercializationProject);
        try {
            elasticSearchDao.flush(indexName);
            final ExpertEvaluationService expertEvaluationService = ServiceRegistryUtil.getService(ExpertEvaluationService.class);
            if (currentCommercialization != null) {
                final Set<String> mergedExpertCodes = CollectionsUtil.mergeCollections(expertCodes, currentCommercialization.getExpertCodes());
                expertEvaluationService.recalculateComputedFieldsForExperts(mergedExpertCodes);
            } else {
                expertEvaluationService.recalculateComputedFieldsForExperts(expertCodes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void replaceExperts(Map<String, String> replacementMap, boolean originalOnly) throws IOException {
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder().filter(new TermsQueryBuilder("expertCodes", replacementMap.keySet()));
        if (originalOnly) {
            boolQueryBuilder.filter(new TermsQueryBuilder("originalExpertCodes", replacementMap.values()));
        }
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(boolQueryBuilder)
                .size(maxResultWindow);
        final SearchResponseEntityWrapper<CommercializationProject> response = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, CommercializationProject.class);
        if (response.getResultCount() == 0) return;
        final List<CommercializationProject> commercializationProjects = response.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource)
                .collect(Collectors.toList());
        for (CommercializationProject commercializationProject : commercializationProjects) {
            ExpertUtil.replaceExpertCodes(commercializationProject.getExpertCodes(), replacementMap);
            ExpertUtil.replaceExpertCodes(commercializationProject.getTeamLeaderExpertCodes(), replacementMap);
        }
        elasticSearchDao.doIndex(indexName, commercializationProjects);
    }

    private CommercializationProject convert(CommercializationProjectIndexDto commercializationProjectIndexDto) {
        CommercializationProject commercializationProject = new CommercializationProject();
        commercializationProject.setCommercializationProjectImgCheckSum(commercializationProjectIndexDto.getImgChecksum());
        commercializationProject.setCommercializationId(commercializationProjectIndexDto.getCommercializationProjectId());
        commercializationProject.setName(commercializationProjectIndexDto.getName());
        commercializationProject.setIdeaScore(commercializationProjectIndexDto.getIdeaScore());
        commercializationProject.setTechnologyReadinessLevel(commercializationProjectIndexDto.getTechnologyReadinessLevel());
        commercializationProject.setExecutiveSummary(commercializationProjectIndexDto.getExecutiveSummary());
        commercializationProject.setUseCaseDescription(commercializationProjectIndexDto.getUseCase());
        commercializationProject.setPainDescription(commercializationProjectIndexDto.getPainDescription());
        commercializationProject.setCompetitiveAdvantageDescription(commercializationProjectIndexDto.getCompetitiveAdvantage());
        commercializationProject.setTechnicalPrinciplesDescription(commercializationProjectIndexDto.getTechnicalPrinciples());
        commercializationProject.setOwnerOrganizationId(commercializationProjectIndexDto.getOwnerOrganizationId());
        commercializationProject.getExpertCodes().addAll(commercializationProjectIndexDto.getTeamMemberUserIds().stream()
                .map(ExpertUtil::convertToUserCode)
                .collect(Collectors.toSet()));
        commercializationProject.getExpertCodes().addAll(commercializationProjectIndexDto.getTeamMemberExpertIds().stream()
                .map(ExpertUtil::convertToExpertCode)
                .collect(Collectors.toSet()));
        commercializationProject.getTeamLeaderExpertCodes().addAll(commercializationProjectIndexDto.getTeamLeaderUserIds().stream()
                .map(ExpertUtil::convertToUserCode)
                .collect(Collectors.toSet()));
        commercializationProject.getTeamLeaderExpertCodes().addAll(commercializationProjectIndexDto.getTeamLeaderExpertIds().stream()
                .map(ExpertUtil::convertToExpertCode)
                .collect(Collectors.toSet()));
        final List<CommercializationProject.CommercializationProjectOrganization> organizations = commercializationProject.getCommercializationProjectOrganizations();
        commercializationProjectIndexDto.getCommercializationOrganizationRelationMap().forEach((key, value) -> value.forEach(id -> organizations.add(new CommercializationProject.CommercializationProjectOrganization(key, id))));

        commercializationProject.setStatusId(commercializationProjectIndexDto.getStatusId());
        commercializationProject.setDomainId(commercializationProjectIndexDto.getCommercializationDomainId());
        commercializationProject.setInvestmentRangeFrom(commercializationProjectIndexDto.getCommercializationInvestmentFrom());
        commercializationProject.setInvestmentRangeTo(commercializationProjectIndexDto.getCommercializationInvestmentTo());
        commercializationProject.setCategoryIds(commercializationProjectIndexDto.getCommercializationCategoryIds());
        commercializationProject.setStartDate(commercializationProjectIndexDto.getStartDate());
        commercializationProject.setEndDate(commercializationProjectIndexDto.getEndDate());
        commercializationProject.setPriorityId(commercializationProjectIndexDto.getCommercializationPriorityId());
        commercializationProject.setId(String.valueOf(commercializationProjectIndexDto.getCommercializationProjectId()));
        commercializationProject.setKeywords(commercializationProjectIndexDto.getKeywords());
        commercializationProject.setLink(commercializationProjectIndexDto.getLink());
        return commercializationProject;

    }
}
