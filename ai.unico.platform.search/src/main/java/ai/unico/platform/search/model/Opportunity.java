package ai.unico.platform.search.model;


import ai.unico.platform.recommendation.api.enums.RecombeeColumn;
import ai.unico.platform.recommendation.api.interfaces.RecombeeSearchEntity;
import ai.unico.platform.search.api.service.EvidenceExpertService;
import ai.unico.platform.util.ServiceRegistryUtil;

import java.util.*;

public class Opportunity extends ElasticSearchModel implements RecombeeSearchEntity {
    private String opportunityId;

    private Long originalId;
    private String opportunityName;
    private String opportunityDescription;
    private Set<String> opportunityKw;
    private Date opportunitySignupDate;
    private String opportunityLocation;
    private String opportunityWage;
    private String opportunityTechReq;
    private String opportunityFormReq;
    private String opportunityOtherReq;
    private String opportunityBenefit;
    private Date opportunityJobStartDate;
    private String opportunityExtLink;
    private String opportunityHomeOffice;

    private Boolean hidden = false;
    private Long opportunityType; //maybe change to type
    private Set<Long> jobTypes = new HashSet<>(); //maybe change to type
    private Set<Long> organizationIds = new HashSet<>();
    private Set<String> expertIds = new HashSet<>();

    public Long getOriginalId() {
        return originalId;
    }

    public void setOriginalId(Long originalId) {
        this.originalId = originalId;
    }

    public String getOpportunityName() {
        return opportunityName;
    }

    public void setOpportunityName(String opportunityName) {
        this.opportunityName = opportunityName;
    }

    public String getOpportunityDescription() {
        return opportunityDescription;
    }

    public void setOpportunityDescription(String opportunityDescription) {
        this.opportunityDescription = opportunityDescription;
    }

    public Set<String> getOpportunityKw() {
        return opportunityKw;
    }

    public void setOpportunityKw(Set<String> opportunityKw) {
        this.opportunityKw = opportunityKw;
    }

    public Date getOpportunitySignupDate() {
        return opportunitySignupDate;
    }

    public void setOpportunitySignupDate(Date opportunitySignupDate) {
        this.opportunitySignupDate = opportunitySignupDate;
    }

    public String getOpportunityLocation() {
        return opportunityLocation;
    }

    public void setOpportunityLocation(String opportunityLocation) {
        this.opportunityLocation = opportunityLocation;
    }

    public String getOpportunityWage() {
        return opportunityWage;
    }

    public void setOpportunityWage(String opportunityWage) {
        this.opportunityWage = opportunityWage;
    }

    public String getOpportunityTechReq() {
        return opportunityTechReq;
    }

    public void setOpportunityTechReq(String opportunityTechReq) {
        this.opportunityTechReq = opportunityTechReq;
    }

    public String getOpportunityFormReq() {
        return opportunityFormReq;
    }

    public void setOpportunityFormReq(String opportunityFormReq) {
        this.opportunityFormReq = opportunityFormReq;
    }

    public String getOpportunityOtherReq() {
        return opportunityOtherReq;
    }

    public void setOpportunityOtherReq(String opportunityOtherReq) {
        this.opportunityOtherReq = opportunityOtherReq;
    }

    public String getOpportunityBenefit() {
        return opportunityBenefit;
    }

    public void setOpportunityBenefit(String opportunityBenefit) {
        this.opportunityBenefit = opportunityBenefit;
    }

    public Date getOpportunityJobStartDate() {
        return opportunityJobStartDate;
    }

    public void setOpportunityJobStartDate(Date opportunityJobStartDate) {
        this.opportunityJobStartDate = opportunityJobStartDate;
    }

    public String getOpportunityExtLink() {
        return opportunityExtLink;
    }

    public void setOpportunityExtLink(String opportunityExtLink) {
        this.opportunityExtLink = opportunityExtLink;
    }

    public String getOpportunityHomeOffice() {
        return opportunityHomeOffice;
    }

    public void setOpportunityHomeOffice(String opportunityHomeOffice) {
        this.opportunityHomeOffice = opportunityHomeOffice;
    }

    public Long getOpportunityType() {
        return opportunityType;
    }

    public void setOpportunityType(Long opportunityType) {
        this.opportunityType = opportunityType;
    }

    public Set<Long> getJobTypes() {
        return jobTypes;
    }

    public void setJobTypes(Set<Long> jobTypes) {
        this.jobTypes = jobTypes;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<String> getExpertIds() {
        return expertIds;
    }

    public void setExpertIds(Set<String> expertIds) {
        this.expertIds = expertIds;
    }

    public String getOpportunityId() {
        return opportunityId;
    }

    public void setOpportunityId(String opportunityId) {
        this.opportunityId = opportunityId;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put(RecombeeColumn.IS_OPPORTUNITY.getColumnName(), true);
        map.put(RecombeeColumn.ITEM_CODE.getColumnName(), opportunityId);
        map.put(RecombeeColumn.NAME.getColumnName(), opportunityName);
        map.put(RecombeeColumn.DESCRIPTION.getColumnName(), opportunityDescription);
        map.put(RecombeeColumn.KEYWORDS.getColumnName(), opportunityKw);
        map.put(RecombeeColumn.JOB_START.getColumnName(), opportunityJobStartDate);
        map.put(RecombeeColumn.LOCATION.getColumnName(), opportunityLocation);
        map.put(RecombeeColumn.WAGE.getColumnName(), opportunityWage);
        map.put(RecombeeColumn.BENEFIT.getColumnName(), opportunityBenefit);
        map.put(RecombeeColumn.HOME_OFFICE.getColumnName(), opportunityHomeOffice);
        map.put(RecombeeColumn.TYPE_ID.getColumnName(), opportunityType);
        map.put(RecombeeColumn.JOB_TYPES.getColumnName(), jobTypes);
        map.put(RecombeeColumn.ORGANIZATION_IDS.getColumnName(), organizationIds);
        map.put(RecombeeColumn.HIDDEN.getColumnName(), hidden);

        final EvidenceExpertService evidenceExpertService = ServiceRegistryUtil.getService(EvidenceExpertService.class);
        try {
            Set<String> names = evidenceExpertService.findExpertNamesByCodes(getExpertIds());
            map.put(RecombeeColumn.EXPERT_NAMES.getColumnName(), names);
        } catch (Exception e) {
            System.out.printf("Error when uploading merged Opportunity's expert names. Continuing... Error message: " + e.getMessage());
            e.printStackTrace();
        }
        return map;
    }
}
