package ai.unico.platform.search.service;

import ai.unico.platform.search.api.dto.CommercializationTeamMemberDto;
import ai.unico.platform.search.api.dto.EvidenceCommercializationPreviewDto;
import ai.unico.platform.search.api.filter.CommercializationFilter;
import ai.unico.platform.search.api.service.EvidenceCommercializationService;
import ai.unico.platform.search.api.service.ExpertLookupService;
import ai.unico.platform.search.api.wrapper.EvidenceCommercializationWrapper;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.CommercializationProject;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.CommercializationEnumUtil;
import ai.unico.platform.store.api.dto.*;
import ai.unico.platform.store.api.enums.CommercializationOrganizationRelation;
import ai.unico.platform.util.dto.FileDto;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class EvidenceCommercializationServiceImpl implements EvidenceCommercializationService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private ExpertLookupService expertLookupService;

    @Override
    public EvidenceCommercializationWrapper findEvidenceCommercialization(CommercializationFilter filter) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder().must(new MatchAllQueryBuilder());
        final Set<String> participatingExpertCodes = filter.getExpertCodes();
        final Set<Long> participatingOrganizationIds = filter.getParticipatingOrganizationIds();
        if (filter.getQuery() != null && !filter.getQuery().isEmpty()) {
            boolQueryBuilder.must(new MultiMatchQueryBuilder(filter.getQueryForSearch(), "name", "executiveSummary"));
        }
        if (!filter.getOrganizationIds().isEmpty()) {
            boolQueryBuilder.filter(prepareOrganizationRelationQuery(filter.getOrganizationIds(), filter.getOrganizationRelations()));
        }
        if (!filter.getIpOwnerOrganizationIds().isEmpty()) {
            final Set<CommercializationOrganizationRelation> relation = Collections.singleton(CommercializationOrganizationRelation.INTELLECTUAL_PROPERTY_OWNER);
            boolQueryBuilder.filter(prepareOrganizationRelationQuery(filter.getIpOwnerOrganizationIds(), relation));
        }
        if (!filter.getInvestorOrganizationIds().isEmpty()) {
            final Set<CommercializationOrganizationRelation> relation = Collections.singleton(CommercializationOrganizationRelation.INVESTOR);
            boolQueryBuilder.filter(prepareOrganizationRelationQuery(filter.getInvestorOrganizationIds(), relation));
        }
        if (!filter.getCommercializationPartnerOrganizationIds().isEmpty()) {
            final Set<CommercializationOrganizationRelation> relation = Collections.singleton(CommercializationOrganizationRelation.COMMERCIALIZATION_PARTNER);
            boolQueryBuilder.filter(prepareOrganizationRelationQuery(filter.getCommercializationPartnerOrganizationIds(), relation));
        }
        if (filter.getYearFrom() != null) {
            boolQueryBuilder.filter(new RangeQueryBuilder("endDate").format("yyyy").gte(filter.getYearFrom()));
        }
        if (filter.getYearTo() != null) {
            boolQueryBuilder.filter(new RangeQueryBuilder("startDate").format("yyyy").lte(filter.getYearTo()));
        }
        if (filter.getBudgetFrom() != null) {
            boolQueryBuilder.filter(new RangeQueryBuilder("investmentRangeTo").gte(filter.getBudgetFrom()));
        }
        if (filter.getBudgetTo() != null) {
            boolQueryBuilder.filter(new RangeQueryBuilder("investmentRangeFrom").lte(filter.getBudgetTo()));
        }
        if (filter.getIdeaScoreFrom() != null || filter.getIdeaScoreTo() != null) {
            boolQueryBuilder.filter(new RangeQueryBuilder("ideaScore")
                    .lte(filter.getIdeaScoreTo())
                    .gte(filter.getIdeaScoreFrom()));
        }
        if (!filter.getCommercializationStatusIds().isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("statusId", filter.getCommercializationStatusIds()));
        }
        if (!filter.getCommercializationCategoryIds().isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("categoryIds", filter.getCommercializationCategoryIds()));
        }
        if (!filter.getCommercializationDomainIds().isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("domainId", filter.getCommercializationDomainIds()));
        }
        if (!filter.getCommercializationPriorityIds().isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("priorityId", filter.getCommercializationPriorityIds()));
        }
        if (!participatingExpertCodes.isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("expertCodes", participatingExpertCodes));
        }
        if (!participatingOrganizationIds.isEmpty()) {
            boolQueryBuilder.filter(prepareOrganizationRelationQuery(filter.getOrganizationIds(), Arrays.asList(CommercializationOrganizationRelation.values())));
        }
        searchSourceBuilder.query(boolQueryBuilder).size(filter.getLimit()).from(filter.getLimit() * (filter.getPage() - 1)).sort("ideaScore", SortOrder.DESC);
        final SearchResponseEntityWrapper<CommercializationProject> responseWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, CommercializationProject.class);
        final Long resultCount = responseWrapper.getResultCount();
        final EvidenceCommercializationWrapper resultWrapper = new EvidenceCommercializationWrapper();
        resultWrapper.setNumberOfAllItems(resultCount);
        resultWrapper.setLimit(filter.getLimit());
        resultWrapper.setPage(filter.getPage());
        final Set<String> expertToLoadCodes = new HashSet<>();
        responseWrapper.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource)
                .map(CommercializationProject::getExpertCodes)
                .forEach(expertToLoadCodes::addAll);


        final Map<String, UserExpertBaseDto> expertCodeDtoMap = expertLookupService.findUserExpertBasesMap(expertToLoadCodes);
        final Map<Long, CommercializationCategoryPreviewDto> categoryMap = CommercializationEnumUtil.findCategoryMap();
        final Map<Long, CommercializationDomainDto> domainMap = CommercializationEnumUtil.findDomainMap();
        final Map<Long, CommercializationPriorityTypeDto> priorityMap = CommercializationEnumUtil.findPriorityMap();
        final Map<Long, CommercializationStatusTypeDto> statusMap = CommercializationEnumUtil.findStatusMap();

        for (SearchResponseEntity<CommercializationProject> searchResponseEntity : responseWrapper.getSearchResponseEntities()) {
            final CommercializationProject source = searchResponseEntity.getSource();
            final EvidenceCommercializationPreviewDto dto = new EvidenceCommercializationPreviewDto();
            dto.setImgCheckSum(source.getCommercializationProjectImgCheckSum());
            dto.setName(source.getName());
            dto.setExecutiveSummary(source.getExecutiveSummary());
            dto.setId(source.getCommercializationId());
            dto.setInvestmentFrom(source.getInvestmentRangeFrom());
            dto.setInvestmentTo(source.getInvestmentRangeTo());
            dto.setReadinessLevel(source.getTechnologyReadinessLevel());
            dto.setIdeaScore(source.getIdeaScore());
            dto.setKeywords(source.getKeywords());
            dto.setStartDate(source.getStartDate());
            dto.setEndDate(source.getEndDate());
            dto.setLink(source.getLink());

            final Long domainId = source.getDomainId();
            if (domainMap.containsKey(domainId)) {
                dto.setDomainId(domainId);
                dto.setDomainName(domainMap.get(domainId).getDomainName());
            }
            final Long statusId = source.getStatusId();
            if (statusMap.containsKey(statusId)) {
                dto.setStatusId(statusId);
                dto.setStatusName(statusMap.get(statusId).getStatusName());
            }
            final Set<Long> categoryIds = source.getCategoryIds();
            for (Long categoryId :
                    categoryIds) {
                if (categoryMap.containsKey(categoryId)) {
                    dto.getCommercializationCategoryPreviews().add(categoryMap.get(categoryId));
                }
            }
            final Long priorityId = source.getPriorityId();
            if (priorityMap.containsKey(priorityId)) {
                dto.setCommercializationPriorityId(priorityId);
                dto.setCommercializationPriorityName(priorityMap.get(priorityId).getPriorityTypeName());
            }

            for (String expertCode : source.getExpertCodes()) {
                final UserExpertBaseDto userExpertBaseDto = expertCodeDtoMap.get(expertCode);
                if (userExpertBaseDto == null) continue;
                final CommercializationTeamMemberDto memberDto = new CommercializationTeamMemberDto(userExpertBaseDto);
                memberDto.setTeamLeader(source.getTeamLeaderExpertCodes().contains(expertCode));
                dto.getTeamMemberDtos().add(memberDto);
            }
            resultWrapper.getEvidenceCommercializationDtos().add(dto);
        }
        if (filter.getOrganizationId() != null) {
            resultWrapper.setOrganizationId(filter.getOrganizationId());
            resultWrapper.setOrganizationUnitIds(filter.getOrganizationIds().stream()
                    .filter(id -> !filter.getOrganizationId().equals(id))
                    .collect(Collectors.toSet()));
        }
        return resultWrapper;
    }

    @Override
    public FileDto exportEvidenceCommercialization(CommercializationFilter evidenceFilter) {
        //todo needs to be implemented
        return null;
    }

    private NestedQueryBuilder prepareOrganizationRelationQuery(Collection<Long> organizationIds, Collection<CommercializationOrganizationRelation> relations) {
        final BoolQueryBuilder query = new BoolQueryBuilder()
                .must(new TermsQueryBuilder("commercializationProjectOrganizations.organizationId", organizationIds));
        if (relations != null && !relations.isEmpty()) {
            query.must(new TermsQueryBuilder("commercializationProjectOrganizations.relation", relations));
        }
        return new NestedQueryBuilder("commercializationProjectOrganizations", query, ScoreMode.Max);
    }

    @Override
    public CommercializationStatisticsDto findCommercializationStatistics(Set<Long> organizationIds) throws IOException {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        CommercializationStatisticsDto commercializationStatisticsDto = new CommercializationStatisticsDto();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder()
                .filter(new ExistsQueryBuilder("name"));
        boolQueryBuilder.filter(prepareOrganizationRelationQuery(organizationIds, null));
        searchSourceBuilder.query(boolQueryBuilder);
        searchSourceBuilder.size(10);
        final SearchResponseEntityWrapper<CommercializationProject> responseWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, CommercializationProject.class);
        commercializationStatisticsDto.setNumberOfAllProjects(responseWrapper.getResultCount());
        return commercializationStatisticsDto;
    }
}
