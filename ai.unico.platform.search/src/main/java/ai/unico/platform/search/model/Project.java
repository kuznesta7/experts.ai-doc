package ai.unico.platform.search.model;

import ai.unico.platform.recommendation.api.enums.RecombeeColumn;
import ai.unico.platform.recommendation.api.interfaces.RecombeeSearchEntity;
import ai.unico.platform.search.api.service.EvidenceExpertService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;

import java.util.*;

public class Project extends ElasticSearchModel implements RecombeeSearchEntity {

    private Long projectId;

    private Long originalProjectId;

    private String name;

    private String description;

    private Date startDate;

    private Date endDate;

    private String countryCode;

    private String projectNumber;

    private Long projectProgramId;

    private Confidentiality confidentiality;

    private Long ownerOrganizationId;

    private String projectLink;

    private List<String> keywords = new ArrayList<>();

    private Set<Long> providingOrganizationIds = new HashSet<>();

    private Set<Long> verifiedOrganizationIds = new HashSet<>();

    private Set<Long> evidenceOrganizationIds = new HashSet<>();

    private Set<Long> organizationIds = new HashSet<>();

    private List<ProjectOrganizationBudget> finances = new ArrayList<>();

    private Set<String> expertCodes = new HashSet<>();

    private Set<String> originalExpertCodes = new HashSet<>();

    private List<ProjectOrganizationParentProject> parentProjects = new ArrayList<>();

    private Set<String> itemCodes = new HashSet<>();

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put(RecombeeColumn.IS_PROJECT.getColumnName(), true);
        map.put(RecombeeColumn.ID.getColumnName(), projectId);
        map.put(RecombeeColumn.NAME.getColumnName(), name);
        map.put(RecombeeColumn.DESCRIPTION.getColumnName(), description);
        map.put(RecombeeColumn.START_DATE.getColumnName(), startDate);
        map.put(RecombeeColumn.END_DATE.getColumnName(), endDate);
        map.put(RecombeeColumn.COUNTRY_CODE.getColumnName(), countryCode);
        map.put(RecombeeColumn.CONFIDENTIALITY.getColumnName(), confidentiality);
        map.put(RecombeeColumn.OWNER_ORGANIZATION_ID.getColumnName(), ownerOrganizationId);
        map.put(RecombeeColumn.LINK.getColumnName(), projectLink);
        map.put(RecombeeColumn.KEYWORDS.getColumnName(), keywords);
        map.put(RecombeeColumn.VERIFIED_ORGANIZATION_IDS.getColumnName(), verifiedOrganizationIds);
        map.put(RecombeeColumn.ORGANIZATION_IDS.getColumnName(), organizationIds);
        map.put(RecombeeColumn.EXPERT_CODES.getColumnName(), expertCodes);
        map.put(RecombeeColumn.PROJECT_PROGRAM_ID.getColumnName(), projectProgramId);

        final EvidenceExpertService evidenceExpertService = ServiceRegistryUtil.getService(EvidenceExpertService.class);
        try {
            Set<String> names = evidenceExpertService.findExpertNamesByCodes(getExpertCodes());
            map.put(RecombeeColumn.EXPERT_NAMES.getColumnName(), names);
        } catch (Exception e) {
            System.out.printf("Error when uploading merged Project's expert names. Continuing... Error message: " + e.getMessage());
            e.printStackTrace();
        }
        return map;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getOriginalProjectId() {
        return originalProjectId;
    }

    public void setOriginalProjectId(Long originalProjectId) {
        this.originalProjectId = originalProjectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<String> getItemCodes() {
        return itemCodes;
    }

    public void setItemCodes(Set<String> itemCodes) {
        this.itemCodes = itemCodes;
    }

    public List<ProjectOrganizationParentProject> getParentProjects() {
        return parentProjects;
    }

    public void setParentProjects(List<ProjectOrganizationParentProject> parentProjects) {
        this.parentProjects = parentProjects;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public List<ProjectOrganizationBudget> getFinances() {
        return finances;
    }

    public void setFinances(List<ProjectOrganizationBudget> finances) {
        this.finances = finances;
    }

    public Set<Long> getProvidingOrganizationIds() {
        return providingOrganizationIds;
    }

    public void setProvidingOrganizationIds(Set<Long> providingOrganizationIds) {
        this.providingOrganizationIds = providingOrganizationIds;
    }

    public Set<Long> getVerifiedOrganizationIds() {
        return verifiedOrganizationIds;
    }

    public void setVerifiedOrganizationIds(Set<Long> verifiedOrganizationIds) {
        this.verifiedOrganizationIds = verifiedOrganizationIds;
    }

    public Long getProjectProgramId() {
        return projectProgramId;
    }

    public void setProjectProgramId(Long projectProgramId) {
        this.projectProgramId = projectProgramId;
    }

    public Confidentiality getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Confidentiality confidentiality) {
        this.confidentiality = confidentiality;
    }

    public Long getOwnerOrganizationId() {
        return ownerOrganizationId;
    }

    public void setOwnerOrganizationId(Long ownerOrganizationId) {
        this.ownerOrganizationId = ownerOrganizationId;
    }

    public Set<String> getExpertCodes() {
        return expertCodes;
    }

    public void setExpertCodes(Set<String> expertCodes) {
        this.expertCodes = expertCodes;
    }

    public Set<String> getOriginalExpertCodes() {
        return originalExpertCodes;
    }

    public void setOriginalExpertCodes(Set<String> originalExpertCodes) {
        this.originalExpertCodes = originalExpertCodes;
    }

    public Set<Long> getEvidenceOrganizationIds() {
        return evidenceOrganizationIds;
    }

    public void setEvidenceOrganizationIds(Set<Long> evidenceOrganizationIds) {
        this.evidenceOrganizationIds = evidenceOrganizationIds;
    }

    public String getProjectLink() {
        return projectLink;
    }

    public void setProjectLink(String projectLink) {
        this.projectLink = projectLink;
    }

    public static class ProjectOrganizationBudget {
        private Long organizationId;

        private Integer year;

        private Double total;

        private Double nationalSupport;

        private Double privateFinances;

        private Double otherFinances;

        public Long getOrganizationId() {
            return organizationId;
        }

        public void setOrganizationId(Long organizationId) {
            this.organizationId = organizationId;
        }

        public Integer getYear() {
            return year;
        }

        public void setYear(Integer year) {
            this.year = year;
        }

        public Double getTotal() {
            return total;
        }

        public void setTotal(Double total) {
            this.total = total;
        }

        public Double getNationalSupport() {
            return nationalSupport;
        }

        public void setNationalSupport(Double nationalSupport) {
            this.nationalSupport = nationalSupport;
        }

        public Double getPrivateFinances() {
            return privateFinances;
        }

        public void setPrivateFinances(Double privateFinances) {
            this.privateFinances = privateFinances;
        }

        public Double getOtherFinances() {
            return otherFinances;
        }

        public void setOtherFinances(Double otherFinances) {
            this.otherFinances = otherFinances;
        }
    }

    public static class ProjectOrganizationParentProject {
        private Long organizationId;
        private String projectCode;

        public Long getOrganizationId() {
            return organizationId;
        }

        public void setOrganizationId(Long organizationId) {
            this.organizationId = organizationId;
        }

        public String getProjectCode() {
            return projectCode;
        }

        public void setProjectCode(String projectCode) {
            this.projectCode = projectCode;
        }
    }
}
