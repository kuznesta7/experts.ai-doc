package ai.unico.platform.search.service;

import ai.unico.platform.search.api.dto.ItemLookupPreviewDto;
import ai.unico.platform.search.api.filter.ItemLookupFilter;
import ai.unico.platform.search.api.service.ExpertLookupService;
import ai.unico.platform.search.api.service.ItemIndexService;
import ai.unico.platform.search.api.service.ItemLookupService;
import ai.unico.platform.search.api.service.OrganizationSearchService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Item;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.ExpertUtil;
import ai.unico.platform.search.util.ItemUtil;
import ai.unico.platform.store.api.dto.*;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import ai.unico.platform.util.security.SecurityUtil;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ItemLookupServiceImpl implements ItemLookupService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private ExpertLookupService expertLookupService;

    @Autowired
    private OrganizationSearchService organizationSearchService;

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;

    @Override
    public ItemDetailDto findItemDetail(UserContext userContext, String itemCode) throws IOException, NonexistentEntityException {
        final Item document = elasticSearchDao.getDocument(indexName, itemCode, Item.class);
        if (document == null) throw new NonexistentEntityException(Item.class);
        if (!SecurityUtil.canDetailBeLoaded(userContext, document.getEvidenceOrganizationIds(), document.getOwnerOrganizationId(), document.getConfidentiality())) {
            throw new NonexistentEntityException(Item.class);
        }

        final ItemDetailDto itemDetailDto = new ItemDetailDto();
        itemDetailDto.setItemName(document.getItemName());
        itemDetailDto.setItemDescription(document.getItemDescription());
        itemDetailDto.setItemId(document.getItemId());
        itemDetailDto.setOriginalItemId(document.getOriginalItemId());
        itemDetailDto.setItemTypeId(document.getItemTypeId());
        itemDetailDto.setYear(document.getYear());
        itemDetailDto.setKeywords(document.getKeywords());
        itemDetailDto.setConfidentiality(document.getConfidentiality());
        final Long ownerOrganizationId = document.getOwnerOrganizationId();
        itemDetailDto.setOwnerOrganizationId(ownerOrganizationId);
        itemDetailDto.setOwnerUserId(document.getOwnerUserId());
        itemDetailDto.setTranslationUnavailable(document.getTranslationUnavailable());
        itemDetailDto.setDoi(document.getDoi());
        itemDetailDto.setItemLink(document.getItemLink());

        final Set<Long> notRegisteredExpertIds = document.getNotRegisteredExpertsIds();
        if (!notRegisteredExpertIds.isEmpty()) {
            final List<ItemNotRegisteredExpertDto> notRegisteredExpertDtos
                    = expertLookupService.findNotRegisteredExperts(notRegisteredExpertIds);
            itemDetailDto.setItemNotRegisteredExperts(notRegisteredExpertDtos);
        }

        final Set<String> expertCodes = document.getExpertCodes();
        if (!expertCodes.isEmpty()) {
            final List<UserExpertBaseDto> userExpertBases = expertLookupService.findUserExpertBases(expertCodes);
            itemDetailDto.setItemExperts(userExpertBases);
        }
        final Set<Long> organizationToLoadIds = new HashSet<>(document.getOrganizationIds());
        if (ownerOrganizationId != null) organizationToLoadIds.add(ownerOrganizationId);
        if (!organizationToLoadIds.isEmpty()) {
            final List<OrganizationBaseDto> organizations = organizationSearchService.findBaseOrganizations(organizationToLoadIds);
            //To avoid null pointer exceptions
            organizations.removeAll(Collections.singleton(null));
            final Map<Long, OrganizationBaseDto> organizationsMap = organizations.stream().collect(Collectors.toMap(OrganizationBaseDto::getOrganizationId, Function.identity()));
            itemDetailDto.setItemOrganizations(document.getOrganizationIds().stream().map(organizationsMap::get).collect(Collectors.toList()));
            if (ownerOrganizationId != null && organizationsMap.containsKey(ownerOrganizationId)) {
                final OrganizationBaseDto ownerOrganization = organizationsMap.get(ownerOrganizationId);
                itemDetailDto.setOwnerOrganizationName(ownerOrganization.getOrganizationName());
            }
        }

        return itemDetailDto;
    }

    @Override
    public ItemRegistrationDto findOriginalItem(Long originalItemId) throws IOException, NonexistentEntityException {
        final String itemCode = ItemUtil.convertToDWHItemCode(originalItemId);
        final Item document = elasticSearchDao.getDocument(indexName, itemCode, Item.class);
        if (document == null) throw new NonexistentEntityException(Item.class);
        return convertToRegistration(document);
    }

    @Override
    public List<ItemRegistrationDto> findItemsForExpertToClaim(Long expertId) throws IOException {
        final Integer size = 10;
        final String expertCode = ExpertUtil.convertToExpertCode(expertId);
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder().filter(new TermQueryBuilder("expertCodes", expertCode));
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().query(boolQueryBuilder).size(size);
        final List<ItemRegistrationDto> itemRegistrationDtos = new ArrayList<>();
        Integer from = 0;
        do {
            searchSourceBuilder.from(from);
            final SearchResponseEntityWrapper<Item> itemsWrapper = elasticSearchDao.doSearchAndGetHits(ItemIndexService.indexName, searchSourceBuilder, Item.class);
            final List<SearchResponseEntity<Item>> searchResponseEntities = itemsWrapper.getSearchResponseEntities();
            itemRegistrationDtos.addAll(searchResponseEntities.stream()
                    .map(SearchResponseEntity::getSource)
                    .map(this::convertToRegistration)
                    .collect(Collectors.toList()));
            if (itemsWrapper.getResultCount() < size + from) break;
            from += size;
        } while (true);
        return itemRegistrationDtos;
    }

    @Override
    public List<ItemLookupPreviewDto> lookupItems(ItemLookupFilter itemLookupFilter) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.filter(new TermQueryBuilder("confidentiality", Confidentiality.PUBLIC));
        boolQueryBuilder.must(new MatchQueryBuilder("itemName", itemLookupFilter.getQuery()));
        if (!itemLookupFilter.getOrganizationIds().isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("organizationIds", itemLookupFilter.getOrganizationIds()));
        }
        if (!itemLookupFilter.getDisallowedOrganizationIds().isEmpty()) {
            boolQueryBuilder.mustNot(new TermsQueryBuilder("organizationIds", itemLookupFilter.getDisallowedOrganizationIds()));
        }
        if (itemLookupFilter.isApprovedOnly()) {
            boolQueryBuilder.filter(new ExistsQueryBuilder("itemId"));
        }
        searchSourceBuilder.query(boolQueryBuilder);
        final SearchResponseEntityWrapper<Item> itemSearchResponseEntityWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Item.class);
        return itemSearchResponseEntityWrapper.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource)
                .map(ItemUtil::convertToLookupPreview)
                .collect(Collectors.toList());
    }

    @Override
    public List<ItemLookupPreviewDto> findItems(Set<String> itemCodes) throws IOException {
        final List<Item> documents = elasticSearchDao.getDocuments(indexName, itemCodes, Item.class);
        return documents.stream().filter(Objects::nonNull).map(ItemUtil::convertToLookupPreview).collect(Collectors.toList());
    }

    private ItemRegistrationDto convertToRegistration(Item document) {
        final ItemRegistrationDto itemRegistrationDto = new ItemRegistrationDto();
        itemRegistrationDto.setItemId(document.getItemId());
        itemRegistrationDto.setOriginalItemId(document.getOriginalItemId());
        itemRegistrationDto.setItemName(document.getItemName());
        itemRegistrationDto.setItemDescription(document.getItemDescription());
        itemRegistrationDto.setKeywords(document.getKeywords());
        itemRegistrationDto.setItemTypeId(document.getItemTypeId());
        itemRegistrationDto.setYear(document.getYear());
        itemRegistrationDto.setOrganizationIds(document.getOrganizationIds());
        itemRegistrationDto.setConfidentiality(document.getConfidentiality());
        itemRegistrationDto.setNotRegisteredExpertsIds(document.getNotRegisteredExpertsIds());

        final Set<String> expertCodes = document.getExpertCodes();
        for (String expertCode : expertCodes) {
            final Long userId = ExpertUtil.getUserId(expertCode);
            if (userId != null) {
                itemRegistrationDto.getUserIds().add(userId);
            } else {
                final Long expertId = ExpertUtil.getExpertId(expertCode);
                itemRegistrationDto.getExpertIds().add(expertId);
            }
        }
        return itemRegistrationDto;
    }
}
