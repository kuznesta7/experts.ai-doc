package ai.unico.platform.search.service;

import ai.unico.platform.search.api.dto.LecturePreviewDto;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.service.ExpertLookupService;
import ai.unico.platform.search.api.service.LectureSearchService;
import ai.unico.platform.search.api.service.OntologySearchService;
import ai.unico.platform.search.api.service.OrganizationSearchService;
import ai.unico.platform.search.api.wrapper.LectureWrapper;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Lecture;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.ElasticSearchUtil;
import ai.unico.platform.search.util.LectureUtil;
import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.dto.UserExpertBaseDto;
import ai.unico.platform.util.KeywordsUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.enums.ExpertSearchType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.ScriptSortBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.elasticsearch.search.sort.SortOrder.DESC;

public class LectureSearchServiceImpl implements LectureSearchService {


    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private ExpertLookupService expertLookupService;

    @Autowired
    private OrganizationSearchService organizationSearchService;

    @Override
    public LectureWrapper findLecture(EvidenceFilter evidenceFilter, ExpertSearchResultFilter.Order order) throws IOException {

        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder query = new BoolQueryBuilder();

        final Long organizationId = evidenceFilter.getOrganizationId();
        final Set<Long> participatingOrganizationIds = evidenceFilter.getParticipatingOrganizationIds();
        final Set<String> participatingExpertCodes = evidenceFilter.getExpertCodes();
        final Set<Confidentiality> confidentiality = evidenceFilter.getConfidentiality();
        if (confidentiality.isEmpty()) {
            confidentiality.add(Confidentiality.PUBLIC);
        }

        query.filter(new TermsQueryBuilder("confidentiality", confidentiality));
        query.filter(new ExistsQueryBuilder("lectureName"));

        if (evidenceFilter.getYearFrom() != null || evidenceFilter.getYearTo() != null) {
            final RangeQueryBuilder rangeQueryBuilder = new RangeQueryBuilder("year")
                    .from(evidenceFilter.getYearFrom())
                    .to(evidenceFilter.getYearTo());
            query.filter(rangeQueryBuilder);
        }
        if (!evidenceFilter.getOrganizationIds().isEmpty()) {
            query.filter(new TermsQueryBuilder("ownerOrganizationId", evidenceFilter.getOrganizationIds()));
        }

        if (!participatingOrganizationIds.isEmpty()) {
            query.filter(new TermsQueryBuilder("organizationIds", participatingOrganizationIds));
        }

        if (!participatingExpertCodes.isEmpty()) {
            query.filter(new TermsQueryBuilder("expertCodes", participatingExpertCodes));
        }

        if (evidenceFilter.getQuery() != null && !evidenceFilter.getQuery().equals("")) {
            if (evidenceFilter.getExpertSearchType() == null || evidenceFilter.getExpertSearchType() == ExpertSearchType.EXPERTISE) {
                final OntologySearchService ontologySearchService = ServiceRegistryUtil.getService(OntologySearchService.class);
                ontologySearchService.fillItemSearchOntology(evidenceFilter);
                final Map<String, Float> alternateQueries = evidenceFilter.getAlternateQueries();
                alternateQueries.put(evidenceFilter.getQueryForSearch(), 1F);
                for (Map.Entry<String, Float> q : alternateQueries.entrySet()) {
                    final Map<String, Float> fields = new HashMap<>();
                    fields.put("lectureName", 2.0F);
                    fields.put("lectureDescription", 1.0F);
                    fields.put("keywords", 3.0F);
                    final QueryStringQueryBuilder queryStringQueryBuilder = new QueryStringQueryBuilder(ElasticSearchUtil.prepareQueryStringQuery(q.getKey()))
                            .defaultOperator(Operator.AND).fields(fields).boost(q.getValue());
                    query.should(queryStringQueryBuilder);
                    query.minimumShouldMatch(1);
                }
            }
        }
        if (order.equals(ExpertSearchResultFilter.Order.QUALITY) && organizationId != null) {
            final HashMap<String, Object> params = new HashMap<>();
            params.put("organizationId", organizationId);
            params.put("organizationIds", evidenceFilter.getOrganizationIds());
            final Script sortScript = new Script(ScriptType.INLINE, "painless", "if (doc['ownerOrganizationId']==params.organizationId) return 2; for (o1 in doc['organizationIds'].values) {for (o2 in params.organizationIds) {if (o1==o2) return 1;}} return 0;", params);
            searchSourceBuilder.sort(new ScriptSortBuilder(sortScript, ScriptSortBuilder.ScriptSortType.NUMBER).order(DESC));

        }
        searchSourceBuilder.query(query)
                .size(evidenceFilter.getLimit())
                .from((evidenceFilter.getPage() - 1) * evidenceFilter.getLimit());

        LectureUtil.addLectureSortFunction(searchSourceBuilder, order, null);
        final SearchResponseEntityWrapper<Lecture> result = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Lecture.class);
        final LectureWrapper lectureWrapper = new LectureWrapper();
        lectureWrapper.setLimit(evidenceFilter.getLimit());
        lectureWrapper.setPage(evidenceFilter.getPage());
        lectureWrapper.setNumberOfAllItems(result.getResultCount());
        Set<String> expertIds = result.getSearchResponseEntities()
                .stream().map(SearchResponseEntity::getSource)
                .map(Lecture::getExpertCodes)
                .flatMap(Set::stream).filter(Objects::nonNull).collect(Collectors.toSet());
        Set<Long> organizationIds = result.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource).map(Lecture::getOrganizationIds)
                .flatMap(Set::stream).filter(Objects::nonNull).collect(Collectors.toSet());
        final Map<String, UserExpertBaseDto> expertBaseDtoMap = expertLookupService.findUserExpertBases(expertIds).stream().collect(Collectors.toMap(UserExpertBaseDto::getExpertCode, Function.identity()));
        final Map<Long, OrganizationBaseDto> organizationBaseMap = organizationSearchService.findBaseOrganizationsMap(organizationIds);
        for (SearchResponseEntity<Lecture> entity : result.getSearchResponseEntities()) {
            final Lecture lecture = entity.getSource();
            final LecturePreviewDto lecturePreviewDto = convert(lecture);
            lecturePreviewDto.setOrganizationBaseDtos(lecture.getOrganizationIds().stream().map(organizationBaseMap::get).collect(Collectors.toList()));
            lecturePreviewDto.setExpertPreviews(lecture.getExpertCodes().stream().map(expertBaseDtoMap::get).collect(Collectors.toList()));
            lectureWrapper.getLecturePreviewDtos().add(lecturePreviewDto);
        }
        return lectureWrapper;
    }

    @Override
    public LecturePreviewDto getLecture(String lectureCode) throws IOException {
        Lecture lecture = elasticSearchDao.getDocument(indexName, lectureCode, Lecture.class);
        final LecturePreviewDto lecturePreviewDto = convert(lecture);
        lecturePreviewDto.setExpertPreviews(expertLookupService.findUserExpertBases(lecture.getExpertCodes()));
        lecturePreviewDto.setOrganizationBaseDtos(organizationSearchService.findBaseOrganizations(lecture.getOrganizationIds()));
        return lecturePreviewDto;
    }

    @Override
    public ExpertLectureStatistics findExpertLectureStatistics(String expertCode, Set<Long> organizationIds, boolean countConfidential) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        if (!organizationIds.isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("organizationIds", organizationIds));
        }
        if (!countConfidential) {
            boolQueryBuilder.filter(new TermQueryBuilder("confidentiality", "PUBLIC"));
        }
        boolQueryBuilder.filter(new TermsQueryBuilder("expertCodes", expertCode));
        searchSourceBuilder.query(boolQueryBuilder).size(0);
        final SearchResponseEntityWrapper<ExpertLectureStatistics> resultWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, ExpertLectureStatistics.class);
        final ExpertLectureStatistics expertLectureStatistics = new ExpertLectureStatistics();
        expertLectureStatistics.setExpertCode(expertCode);
        expertLectureStatistics.setNumberOfLecture(resultWrapper.getResultCount());
        return expertLectureStatistics;
    }

    private LecturePreviewDto convert(Lecture lecture) {
        final LecturePreviewDto lecturePreviewDto = new LecturePreviewDto();
        lecturePreviewDto.setLectureCode(lecture.getId());
        lecturePreviewDto.setLectureId(lecture.getLectureId());
        lecturePreviewDto.setLectureName(lecture.getLectureName());
        lecturePreviewDto.setLectureDescription(lecture.getLectureDescription());
        lecturePreviewDto.setItemTypeId(lecture.getItemTypeId());
        lecturePreviewDto.setYear(lecture.getYear());
        lecturePreviewDto.setConfidentiality(lecture.getConfidentiality());
        lecturePreviewDto.setKeywords(KeywordsUtil.sortKeywordList(lecture.getKeywords()));
        lecturePreviewDto.setItemDomain(lecture.getItemDomain());
        lecturePreviewDto.setItemSpecification(lecture.getItemSpecification());
        lecturePreviewDto.setItemScienceSpecification(lecture.getItemScienceSpecification());
        return lecturePreviewDto;
    }
}
