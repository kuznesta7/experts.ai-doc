package ai.unico.platform.search.util;

import ai.unico.platform.extdata.dto.ExpertThesisPreviewDto;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.model.Thesis;
import ai.unico.platform.store.api.dto.ItemTypeSearchDto;
import ai.unico.platform.store.api.service.ItemTypeService;
import ai.unico.platform.util.IdUtil;
import ai.unico.platform.util.KeywordsUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.ScoreSortBuilder;
import org.elasticsearch.search.sort.ScriptSortBuilder;
import org.elasticsearch.search.sort.SortOrder;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ThesisUtil {

    public static Thesis convert(ExpertThesisPreviewDto expertThesisPreviewDto) {
        final Thesis thesis = new Thesis();
        thesis.setId(ItemUtil.convertToDWHItemCode(expertThesisPreviewDto.getItemId()));
        thesis.setOriginalThesisId(expertThesisPreviewDto.getItemId());
        thesis.setThesisBk(expertThesisPreviewDto.getItemBk());
        thesis.setThesisTypeId(expertThesisPreviewDto.getItemTypeId());
        thesis.setThesisName(expertThesisPreviewDto.getItemName());
        thesis.setThesisDescription(expertThesisPreviewDto.getItemDescription());
        thesis.setYear(expertThesisPreviewDto.getYear());
        thesis.setTranslationUnavailable(expertThesisPreviewDto.isTranslationUnavailable());
        thesis.setKeywords(KeywordsUtil.sortKeywordList(expertThesisPreviewDto.getKeywords().stream().distinct().collect(Collectors.toList())));
        thesis.setConfidentiality(expertThesisPreviewDto.isConfidential() ? Confidentiality.CONFIDENTIAL : Confidentiality.PUBLIC);
        thesis.setSupervisor(IdUtil.convertToExtDataCode(expertThesisPreviewDto.getExpertIds().get("supervisor")));
        thesis.setSupervisor(IdUtil.convertToExtDataCode(expertThesisPreviewDto.getExpertIds().get("reviewer")));
        thesis.setSupervisor(IdUtil.convertToExtDataCode(expertThesisPreviewDto.getExpertIds().get("assignee")));
        return thesis;
    }

    public static void addThesisSortFunction(SearchSourceBuilder searchSourceBuilder, ExpertSearchResultFilter.Order orderBy, String expertCode) {
        searchSourceBuilder.trackScores(true);
        if (orderBy.equals(ExpertSearchResultFilter.Order.YEAR_DESC)) {
            searchSourceBuilder.sort("year", SortOrder.DESC);
        } else if (orderBy.equals(ExpertSearchResultFilter.Order.YEAR_ASC)) {
            searchSourceBuilder.sort("year", SortOrder.ASC);
        }
        searchSourceBuilder.sort(new ScoreSortBuilder().order(SortOrder.DESC));
        final ItemTypeService itemTypeService = ServiceRegistryUtil.getService(ItemTypeService.class);
        final List<ItemTypeSearchDto> itemTypesSearch = itemTypeService.findItemTypesSearch();
        final Map<String, Object> sortScriptParams = new HashMap<>();
        final Map<String, Float> scoresMap = new HashMap<>();
        sortScriptParams.put("scores", scoresMap);
        sortScriptParams.put("expertCode", expertCode);
        sortScriptParams.put("currentYear", Calendar.getInstance().get(Calendar.YEAR));
        for (ItemTypeSearchDto itemType : itemTypesSearch) {
            scoresMap.put(itemType.getTypeId().toString(), itemType.getWeight());
        }

        if (orderBy.equals(ExpertSearchResultFilter.Order.YEAR_DESC) || orderBy.equals(ExpertSearchResultFilter.Order.YEAR_ASC)) {
            final Script sortScript = new Script(ScriptType.INLINE, "painless", "if(params.scores.containsKey(doc['thesisTypeId'].value.toString())) { return params.scores[doc['thesisTypeId'].value.toString()];} return 0;", sortScriptParams);
            searchSourceBuilder.sort(new ScriptSortBuilder(sortScript, ScriptSortBuilder.ScriptSortType.NUMBER).order(SortOrder.DESC));
        } else {
            final Script sortScript = new Script(ScriptType.INLINE, "painless", "if(params.scores.containsKey(doc['thesisTypeId'].value.toString())) { return params.scores[doc['thesisTypeId'].value.toString()] * Math.pow(1.05,doc['year'].value - params.currentYear);} return 0;", sortScriptParams);
            searchSourceBuilder.sort(new ScriptSortBuilder(sortScript, ScriptSortBuilder.ScriptSortType.NUMBER).order(SortOrder.DESC));
        }
    }

}
