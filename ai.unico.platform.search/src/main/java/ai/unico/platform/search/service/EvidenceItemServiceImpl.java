package ai.unico.platform.search.service;

import ai.unico.platform.recommendation.api.dto.RecommendationWrapperDto;
import ai.unico.platform.recommendation.api.enums.RecombeeScenario;
import ai.unico.platform.recommendation.api.service.RecombeeRecommendationService;
import ai.unico.platform.search.api.dto.EvidenceItemPreviewDto;
import ai.unico.platform.search.api.dto.EvidenceItemPreviewWithExpertsDto;
import ai.unico.platform.search.api.dto.ItemStatisticsDto;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.service.EvidenceItemService;
import ai.unico.platform.search.api.service.ExpertLookupService;
import ai.unico.platform.search.api.service.OntologySearchService;
import ai.unico.platform.search.api.service.OrganizationSearchService;
import ai.unico.platform.search.api.wrapper.EvidenceItemWrapper;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Item;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.*;
import ai.unico.platform.store.api.dto.*;
import ai.unico.platform.store.api.service.ItemTypeService;
import ai.unico.platform.util.CSVUtil;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.enums.Confidentiality;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.*;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.histogram.HistogramAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.support.ValueType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.ScriptSortBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.elasticsearch.search.sort.SortOrder.DESC;

public class EvidenceItemServiceImpl implements EvidenceItemService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private ExpertLookupService expertLookupService;

    @Autowired
    private OrganizationSearchService organizationSearchService;

    private final RecombeeRecommendationService recombeeRecommendationService = ServiceRegistryProxy.createProxy(RecombeeRecommendationService.class);

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;

    private BoolQueryBuilder setupFilters(EvidenceFilter evidenceFilter) {
        final BoolQueryBuilder query = new BoolQueryBuilder();
        final Long organizationId = evidenceFilter.getOrganizationId();
        final Set<Long> organizationIds = evidenceFilter.getOrganizationIds();
        final Set<Long> participatingOrganizationIds = evidenceFilter.getParticipatingOrganizationIds();
        final Set<String> participatingExpertCodes = evidenceFilter.getExpertCodes();

        final Set<Confidentiality> confidentiality = evidenceFilter.getConfidentiality();
        if (confidentiality.isEmpty()) {
            confidentiality.add(Confidentiality.PUBLIC);
        }

        query.filter(new TermsQueryBuilder("confidentiality", confidentiality));
        query.filter(new ExistsQueryBuilder("itemName"));
        if (evidenceFilter.getResultTypeGroupIds().size() > 0) {
            final ItemTypeService itemTypeService = ServiceRegistryUtil.getService(ItemTypeService.class);
            final Set<Long> itemTypeIds = itemTypeService.findItemTypeIds(evidenceFilter.getResultTypeGroupIds());
            query.filter(new TermsQueryBuilder("itemTypeId", itemTypeIds));
        }
        if (evidenceFilter.getYearFrom() != null || evidenceFilter.getYearTo() != null) {
            final RangeQueryBuilder rangeQueryBuilder = new RangeQueryBuilder("year")
                    .from(evidenceFilter.getYearFrom())
                    .to(evidenceFilter.getYearTo());
            query.filter(rangeQueryBuilder);
        }
        if (!organizationIds.isEmpty()) {
            query.filter(new TermsQueryBuilder("organizationIds", organizationIds));
            final BoolQueryBuilder confidentialityBoolQueryBuilder = new BoolQueryBuilder();
            query.filter(confidentialityBoolQueryBuilder.minimumShouldMatch(1)
                    .should(new TermQueryBuilder("confidentiality", Confidentiality.PUBLIC))
                    .should(new TermQueryBuilder("confidentiality", Confidentiality.CONFIDENTIAL)));
            if (confidentiality.contains(Confidentiality.SECRET)) {
                confidentialityBoolQueryBuilder.should(new BoolQueryBuilder()
                        .must(new TermQueryBuilder("confidentiality", Confidentiality.SECRET))
                        .must(new TermsQueryBuilder("ownerOrganizationId", organizationIds)));
            }
        }

        final Set<Long> affiliatedOrganizationIds = evidenceFilter.getAffiliatedOrganizationIds();
        if (!affiliatedOrganizationIds.isEmpty()) {
            query.mustNot(new TermsQueryBuilder("organizationIds", affiliatedOrganizationIds));
            query.filter(new TermsQueryBuilder("affiliatedOrganizationIds", affiliatedOrganizationIds));
        }

        if (!participatingOrganizationIds.isEmpty()) {
            query.filter(new TermsQueryBuilder("organizationIds", participatingOrganizationIds));
        }

        if (!participatingExpertCodes.isEmpty()) {
            query.filter(new TermsQueryBuilder("expertCodes", participatingExpertCodes));
        }

        if (evidenceFilter.isVerifiedOnly()) {
            if (organizationIds.isEmpty()) {
                query.filter(new ExistsQueryBuilder("verifiedOrganizationIds"));
            } else {
                query.filter(new TermsQueryBuilder("verifiedOrganizationIds", organizationIds));
            }
        }
        if (evidenceFilter.isNotVerifiedOnly()) {
            if (organizationIds.isEmpty()) {
                query.mustNot(new ExistsQueryBuilder("verifiedOrganizationIds"));
            } else {
                query.mustNot(new TermsQueryBuilder("verifiedOrganizationIds", organizationIds));
            }
        }
        if (evidenceFilter.isActiveOnly() && organizationId != null){
            query.must(new TermQueryBuilder("activeOrganizationIds", organizationId));
        }
        return query;
    }

    @Override
    public EvidenceItemWrapper findAnalyticsItems(EvidenceFilter evidenceFilter) throws IOException {
        return findAnalyticsItemsWithOrder(evidenceFilter, ExpertSearchResultFilter.Order.QUALITY);
    }

    /**
     * This method is used to check whether organization or user has item or not.
     * @param evidenceFilter Filter with preset organization id or user code.
     * @param name Name of the item to be checked.
     * @return Boolean value indicating whether item exists or not.
     * @throws IOException
     */
    @Override
    public Boolean entityHasItem(EvidenceFilter evidenceFilter, String name) throws IOException {
        evidenceFilter.setQuery(name);
        evidenceFilter.setActiveOnly(true);
        List<EvidenceItemPreviewWithExpertsDto> items = findAnalyticsItems(evidenceFilter).getItemPreviewDtos();
        int matches = (int) items.stream()
                .map(EvidenceItemPreviewWithExpertsDto::getItemName) // extract names
                .filter(name::equals)   // keep only matches
                .count();               // count the matches
        return matches > 0;
    }

    @Override
    public List<EvidenceItemPreviewDto> findAnalyticsItems(Collection<String> itemCodes) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        /*removed null values from item codes collection
        in order to avoid 500 errors while getting project
        with item preview details.*/
        itemCodes.removeAll(Collections.singleton(null));
        searchSourceBuilder.size(maxResultWindow);
        searchSourceBuilder.query(new TermsQueryBuilder("id", itemCodes));
        final SearchResponseEntityWrapper<Item> itemWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Item.class);
        final Set<Long> organizationIds = itemWrapper.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource)
                .map(Item::getOrganizationIds)
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
        final Map<Long, OrganizationBaseDto> baseOrganizationsMap = organizationSearchService.findBaseOrganizationsMap(organizationIds);
        return itemWrapper.getSearchResponseEntities().stream().map(itemHit -> {
            final Item source = itemHit.getSource();
            final EvidenceItemPreviewDto evidenceItemPreviewDto = ItemUtil.convertToEvidencePreview(source);
            evidenceItemPreviewDto.setOrganizationBaseDtos(source.getOrganizationIds().stream().map(baseOrganizationsMap::get).collect(Collectors.toList()));
            return evidenceItemPreviewDto;
        }).collect(Collectors.toList());
    }


    @Override
    public EvidenceItemWrapper findAnalyticsItemsWithOrder(EvidenceFilter evidenceFilter, ExpertSearchResultFilter.Order order) throws IOException {
        final EvidenceItemWrapper evidenceItemWrapper = new EvidenceItemWrapper();
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder query = setupFilters(evidenceFilter);

        final Long organizationId = evidenceFilter.getOrganizationId();
        final Set<Long> organizationIds = evidenceFilter.getOrganizationIds();

        if (evidenceFilter.getQuery() != null && !evidenceFilter.getQuery().equals("")) {
            final OntologySearchService ontologySearchService = ServiceRegistryUtil.getService(OntologySearchService.class);
            ontologySearchService.fillItemSearchOntology(evidenceFilter);
            final Map<String, Float> alternateQueries = evidenceFilter.getAlternateQueries();
            alternateQueries.put(evidenceFilter.getQueryForSearch(), 1F);
            for (Map.Entry<String, Float> q : alternateQueries.entrySet()) {
                final Map<String, Float> fields = new HashMap<>();
                fields.put("itemName", 2.0F);
                fields.put("itemDescription", 1.0F);
                fields.put("keywords", 3.0F);
                final QueryStringQueryBuilder queryStringQueryBuilder = new QueryStringQueryBuilder(ElasticSearchUtil.prepareQueryStringQuery(q.getKey()))
                        .defaultOperator(Operator.AND).fields(fields).boost(q.getValue());
                query.should(queryStringQueryBuilder);
                query.minimumShouldMatch(1);
            }
        }
        if (order.equals(ExpertSearchResultFilter.Order.QUALITY)) {
            final HashMap<String, Object> params = new HashMap<>();
            params.put("organizationId", organizationId);
            params.put("organizationIds", organizationIds);
            final Script sortScript = new Script(ScriptType.INLINE, "painless", "for (o1 in doc['verifiedOrganizationIds'].values) {if (o1==params.organizationId) return 2; for (o2 in params.organizationIds) {if (o1==o2) return 1;}} return 0;", params);
            searchSourceBuilder.sort(new ScriptSortBuilder(sortScript, ScriptSortBuilder.ScriptSortType.NUMBER).order(DESC));

        }
        searchSourceBuilder.query(query)
                .size(evidenceFilter.getLimit())
                .from((evidenceFilter.getPage() - 1) * evidenceFilter.getLimit());
        sourceBuilderToEvidenceWrapper(searchSourceBuilder, order, organizationId, organizationIds, evidenceItemWrapper, evidenceFilter);
        return evidenceItemWrapper;
    }

    @Override
    public ItemStatisticsDto findItemStatistics(Set<Long> organizationIds, boolean verified) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final ItemStatisticsDto itemStatisticsDto = new ItemStatisticsDto();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder()
                .filter(new TermsQueryBuilder("organizationIds", organizationIds))
                .filter(new BoolQueryBuilder().minimumShouldMatch(1)
                        .should(new TermQueryBuilder("confidentiality", Confidentiality.PUBLIC))
                        .should(new TermQueryBuilder("confidentiality", Confidentiality.CONFIDENTIAL))
                        .should(new BoolQueryBuilder()
                                .must(new TermQueryBuilder("confidentiality", Confidentiality.SECRET))
                                .must(new TermsQueryBuilder("ownerOrganizationId", organizationIds))))
                .filter(new ExistsQueryBuilder("itemName"));
        if (verified)
            boolQueryBuilder.filter(new TermsQueryBuilder("verifiedOrganizationIds", organizationIds));
        searchSourceBuilder.query(boolQueryBuilder);
        searchSourceBuilder
                .aggregation(new HistogramAggregationBuilder("yearHistogram").interval(1D).field("year"))
                .aggregation(new TermsAggregationBuilder("itemTypeCounts", ValueType.LONG).field("itemTypeId").size(maxResultWindow)
                        .subAggregation(new HistogramAggregationBuilder("yearHistogram").interval(1D).field("year")));
        final SearchResponse searchResponse = elasticSearchDao.doSearch(indexName, searchSourceBuilder);
        final Terms itemTypeCounts = searchResponse.getAggregations().get("itemTypeCounts");
        final List<? extends Terms.Bucket> buckets = itemTypeCounts.getBuckets();
        itemStatisticsDto.setNumberOfAllItems(searchResponse.getHits().getTotalHits());
        final Map<Integer, Long> allItemsYearHistogram = new HashMap<>();
        final Histogram allHistogram = searchResponse.getAggregations().get("yearHistogram");
        for (Histogram.Bucket year : allHistogram.getBuckets()) {
            allItemsYearHistogram.put(TypeConversionUtil.getIntegerValue(year.getKey()), year.getDocCount());
        }
        itemStatisticsDto.setAllItemsYearHistogram(allItemsYearHistogram);
        for (Terms.Bucket bucket : buckets) {
            final long key = bucket.getKeyAsNumber().longValue();
            final Histogram yearHistogram = bucket.getAggregations().get("yearHistogram");
            final List<? extends Histogram.Bucket> years = yearHistogram.getBuckets();
            final Map<Integer, Long> itemTypeYearHistogram = new HashMap<>();
            for (Histogram.Bucket year : years) {
                itemTypeYearHistogram.put(TypeConversionUtil.getIntegerValue(year.getKey()), year.getDocCount());
            }

            itemStatisticsDto.getItemTypesYearHistogram().put(key, itemTypeYearHistogram);
            itemStatisticsDto.getNumberOfItemsByType().put(key, bucket.getDocCount());
        }
        return itemStatisticsDto;
    }

    @Override
    public Map<String, Long> findItemExpertStatistics(Set<Long> organizationIds, boolean verified) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder()
                .filter(new TermsQueryBuilder("organizationIds", organizationIds))
                .filter(new BoolQueryBuilder().minimumShouldMatch(1)
                        .should(new TermQueryBuilder("confidentiality", Confidentiality.PUBLIC))
                        .should(new TermQueryBuilder("confidentiality", Confidentiality.CONFIDENTIAL))
                        .should(new BoolQueryBuilder()
                                .must(new TermQueryBuilder("confidentiality", Confidentiality.SECRET))
                                .must(new TermsQueryBuilder("ownerOrganizationId", organizationIds))))
                .filter(new ExistsQueryBuilder("itemName"));
        if (verified)
            boolQueryBuilder.filter(new TermsQueryBuilder("verifiedOrganizationIds", organizationIds));
        searchSourceBuilder.query(boolQueryBuilder);
        searchSourceBuilder.size(0);
        searchSourceBuilder.aggregation(new TermsAggregationBuilder("expertCodes", ValueType.STRING).field("expertCodes").size(maxResultWindow));
        final SearchResponse searchResponse = elasticSearchDao.doSearch(indexName, searchSourceBuilder);
        Terms expertCodes = searchResponse.getAggregations().get("expertCodes");
        Map<String, Long> result = new HashMap<>();
        for (Terms.Bucket entry : expertCodes.getBuckets()) {
            result.put((String) entry.getKey(), entry.getDocCount());
        }
        return result;
    }

    @Override
    public FileDto exportAnalyticsItems(EvidenceFilter evidenceFilter) throws IOException {
        final ItemTypeService itemTypeService = ServiceRegistryUtil.getService(ItemTypeService.class);
        evidenceFilter.setLimit(maxResultWindow);
        evidenceFilter.setPage(1);
        final EvidenceItemWrapper analyticsItems = findAnalyticsItems(evidenceFilter);
        final List<ItemTypeGroupDto> itemTypeGroups = itemTypeService.findItemTypeGroups();
        final List<ItemTypeDto> itemTypes = itemTypeService.findItemTypes();

        final Map<Long, String> itemTypeIdGroupNameMap = new HashMap<>();
        itemTypeGroups.forEach(g -> g.getItemTypeIds().forEach(t -> itemTypeIdGroupNameMap.put(t, g.getItemTypeGroupTitle())));
        final Map<Long, String> itemTypeIdNameMap = itemTypes.stream()
                .collect(Collectors.toMap(ItemTypeDto::getTypeId, ItemTypeDto::getTypeTitle));

        final List<List<String>> rows = new ArrayList<>();
        final List<String> header = new ArrayList<>();
        header.add("item code");
        header.add("outcome name");
        header.add("year");
        header.add("keywords");
        header.add("outcome group type");
        header.add("outcome type");

        rows.add(header);

        for (EvidenceItemPreviewDto itemDto : analyticsItems.getItemPreviewDtos()) {
            final List<String> row = new ArrayList<>();
            row.add(itemDto.getItemCode());
            row.add(itemDto.getItemName());
            row.add(itemDto.getYear() != null ? String.valueOf(itemDto.getYear()) : "");
            row.add(itemDto.getKeywords().toString());
            row.add(itemDto.getItemTypeId() != null ? itemTypeIdGroupNameMap.get(itemDto.getItemTypeId()) : "");
            row.add(itemDto.getItemTypeId() != null ? itemTypeIdNameMap.get(itemDto.getItemTypeId()) : "");
            rows.add(row);
        }
        return CSVUtil.createFile(rows, "experts-ai_analytics_outcomes", false);
    }

    @Override
    public EvidenceItemWrapper findAnalyticsItemsWithOrderAndRecommendation(EvidenceFilter evidenceFilter, ExpertSearchResultFilter.Order order,
                                                           OrganizationDetailDto organizationDetailDto) throws IOException{
        final EvidenceItemWrapper evidenceItemWrapper = new EvidenceItemWrapper();
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder query = setupFilters(evidenceFilter);
        final Long organizationId = evidenceFilter.getOrganizationId();
        final Set<Long> organizationIds = evidenceFilter.getOrganizationIds();

        RecommendationWrapperDto recommendationWrapperDto = evidenceItemWrapper.getRecommendationWrapperDto();

        final Set<Confidentiality> confidentiality = evidenceFilter.getConfidentiality();
        if (confidentiality.isEmpty()) {
            confidentiality.add(Confidentiality.PUBLIC);
        }
        String[] recombeeIds;
        try {
            recombeeIds = recombeeRecommendationService.recommend(organizationDetailDto, RecombeeFilterConverterUtil.convert(evidenceFilter), RecombeeScenario.ITEM, recommendationWrapperDto);
        } catch (RuntimeException e ){
            System.out.println("Recommendation with Recombee failed - using Elastic instead");
            return findAnalyticsItemsWithOrder(evidenceFilter, order);
        }

        Long docCount = (long) recombeeRecommendationService.numberOfRecomms(recommendationWrapperDto.getRecommId());
        query.filter(new TermsQueryBuilder("id", recombeeIds));
        searchSourceBuilder.query(query).size(evidenceFilter.getLimit());
        sourceBuilderToEvidenceWrapper(searchSourceBuilder, order, organizationId, organizationIds, evidenceItemWrapper, evidenceFilter);
        List<EvidenceItemPreviewWithExpertsDto> sorted = RecombeeFilterConverterUtil.sortByRecombee(evidenceItemWrapper.getItemPreviewDtos(),
                Arrays.stream(recombeeIds).collect(Collectors.toList()),
                evidenceItemWrapper.getItemPreviewDtos().stream().map(EvidenceItemPreviewWithExpertsDto::getItemCode).collect(Collectors.toList()));
        evidenceItemWrapper.setItemPreviewDtos(sorted);
        evidenceItemWrapper.setNumberOfAllItems(docCount);
        return evidenceItemWrapper;
    }

    private void sourceBuilderToEvidenceWrapper(SearchSourceBuilder searchSourceBuilder, ExpertSearchResultFilter.Order order, Long organizationId,
                                                Set<Long> organizationIds, EvidenceItemWrapper evidenceItemWrapper, EvidenceFilter evidenceFilter) throws IOException {
        ItemUtil.addItemSortFunction(searchSourceBuilder, order, null);
        final Set<String> expertToLoadCodes = new HashSet<>();
        final SearchResponseEntityWrapper<Item> itemSearchResponseEntityWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Item.class);
        final List<SearchResponseEntity<Item>> searchResponseEntities = itemSearchResponseEntityWrapper.getSearchResponseEntities();
        searchResponseEntities.stream()
                .map(SearchResponseEntity::getSource)
                .map(Item::getExpertCodes)
                .forEach(expertToLoadCodes::addAll);
        final List<UserExpertBaseDto> userExpertBases = expertLookupService.findUserExpertBases(expertToLoadCodes);
        final Map<String, UserExpertBaseDto> expertCodeDtoMap = userExpertBases.stream().collect(Collectors.toMap(UserExpertBaseDto::getExpertCode, Function.identity()));
        final Set<Long> organizationToLoadIds = new HashSet<>();
        searchResponseEntities.stream()
                .map(SearchResponseEntity::getSource)
                .map(Item::getOrganizationIds)
                .forEach(organizationToLoadIds::addAll);
        final Map<Long, OrganizationBaseDto> baseOrganizationsMap = organizationSearchService.findBaseOrganizationsMap(organizationToLoadIds);
        for (SearchResponseEntity<Item> hit : searchResponseEntities) {
            final Item source = hit.getSource();
            final EvidenceItemPreviewWithExpertsDto itemDto = ItemUtil.convertToEvidencePreview(source);
            final Set<Long> verifiedOrganizationIds = source.getVerifiedOrganizationIds();
            final Set<Long> activeOrganizationIds = source.getActiveOrganizationIds();
            itemDto.setVerified(verifiedOrganizationIds.stream().anyMatch(x -> x.equals(organizationId)));
            itemDto.setUnitVerified(verifiedOrganizationIds.stream().anyMatch(x -> organizationIds.stream().anyMatch(x::equals)));
            System.out.println(activeOrganizationIds.stream().anyMatch(x -> x.equals(organizationId)));
            itemDto.setActive(activeOrganizationIds.stream().anyMatch(x -> x.equals(organizationId)));
            final List<OrganizationBaseDto> organizations = OrganizationUtil.findAndSortOrganizationBases(baseOrganizationsMap, source.getOrganizationIds(), organizationId, organizationIds);
            itemDto.setOrganizationBaseDtos(organizations);
            final List<String> expertCodes = new ArrayList<>(source.getExpertCodes());
            for (String expertCode : expertCodes) {
                if (!expertCodeDtoMap.containsKey(expertCode)) continue;
                itemDto.getExpertPreviews().add(expertCodeDtoMap.get(expertCode));
            }
            evidenceItemWrapper.getItemPreviewDtos().add(itemDto);
        }
        evidenceItemWrapper.setLimit(evidenceFilter.getLimit());
        evidenceItemWrapper.setPage(evidenceFilter.getPage());
        evidenceItemWrapper.setNumberOfAllItems(itemSearchResponseEntityWrapper.getResultCount());
        if (organizationId != null) {
            evidenceItemWrapper.setOrganizationId(organizationId);
            evidenceItemWrapper.setOrganizationUnitIds(organizationIds.stream()
                    .filter(id -> !organizationId.equals(id))
                    .collect(Collectors.toSet()));
        }

    }
    private List<Integer> setToList(Set<Long> set){
        return set.stream().map(Long::intValue).collect(Collectors.toList());
    }
    private List<String> setToStringList(Set<Long> set){
        return set.stream().map(Object::toString).collect(Collectors.toList());
    }
}
