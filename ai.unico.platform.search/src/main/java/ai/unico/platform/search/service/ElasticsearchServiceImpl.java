package ai.unico.platform.search.service;

import ai.unico.platform.search.api.service.ElasticsearchService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import org.springframework.beans.factory.annotation.Autowired;

public class ElasticsearchServiceImpl implements ElasticsearchService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Override
    public void flushSyncedAll() {
        try {
            elasticSearchDao.flush(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
