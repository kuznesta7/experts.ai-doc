package ai.unico.platform.search.dao;

import ai.unico.platform.search.model.ElasticSearchModel;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.reindex.UpdateByQueryRequest;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ElasticSearchDao {

    String baseDocumentTypeName = "_doc";

    void initIndex(String indexName, String optionsJson) throws IOException;

    void doIndex(String indexName, List<? extends ElasticSearchModel> items) throws IOException;

    <T extends ElasticSearchModel> void doIndex(String indexName, T item) throws IOException;

    <T> void doUpdate(String indexName, String fieldName, Map<String, T> valuesMap) throws IOException;

    void doUpdate(UpdateByQueryRequest updateByQueryRequest) throws IOException;

    void doIndexObjects(String indexName, List items) throws IOException;

    void deleteDocument(String indexName, String documentId) throws IOException;

    void deleteDocuments(String indexName, Collection<String> documentIds) throws IOException;

    void deleteIndex(String indexName) throws IOException;

    SearchHits doSearchAndGetHits(String indexName, SearchSourceBuilder searchSourceBuilder) throws IOException;

    SearchResponse doSearch(String indexName, SearchSourceBuilder searchSourceBuilder) throws IOException;

    <T> SearchResponseEntityWrapper<T> doSearchAndGetHits(String indexName, SearchSourceBuilder searchSourceBuilder, Class<T> resultClass) throws IOException;

    GetResponse getDocument(String indexName, String id) throws IOException;

    <T> T getDocument(String indexName, String id, Class<T> resultClass) throws IOException;

    MultiGetItemResponse[] getDocuments(String indexName, Set<String> ids) throws IOException;

    <T> List<T> getDocuments(String indexName, Collection<String> ids, Class<T> resultClass) throws IOException;

    void renameIndex(String currentName, String newName) throws IOException;

    void flush(String indexName) throws IOException;
}
