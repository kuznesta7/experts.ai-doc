package ai.unico.platform.search.service;

import ai.unico.platform.search.api.service.ExpertKeywordService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Item;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ExpertKeywordServiceImpl implements ExpertKeywordService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Override
    public Map<String, Long> expertKeywords(String expertCode, Set<Long> verifiedOrganizationId) throws IOException {
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(new TermQueryBuilder("expertCodes", expertCode));
        boolQueryBuilder.filter(new TermQueryBuilder("confidentiality", "PUBLIC"));
        if (verifiedOrganizationId != null && !verifiedOrganizationId.isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("organizationIds", verifiedOrganizationId));
        }
        sourceBuilder.query(boolQueryBuilder);
        SearchResponseEntityWrapper<Item> items = elasticSearchDao.doSearchAndGetHits(indexName, sourceBuilder, Item.class);
        Map<String, Long> result = new HashMap<>();
        items.getSearchResponseEntities().forEach(itemSearchResponseEntity -> {
            itemSearchResponseEntity.getSource().getKeywords().forEach(keyword -> {
                result.merge(keyword, 1L, Long::sum);
            });
        });
        return result;
    }
}
