package ai.unico.platform.search.service;

import ai.unico.platform.search.api.dto.OrganizationExpertsDto;
import ai.unico.platform.search.api.service.ExpertLookupService;
import ai.unico.platform.search.api.service.ItemLookupService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Expert;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.ExpertUtil;
import ai.unico.platform.store.api.dto.ExpertClaimDto;
import ai.unico.platform.store.api.dto.ItemNotRegisteredExpertDto;
import ai.unico.platform.store.api.dto.UserExpertBaseDto;
import ai.unico.platform.store.api.service.ItemService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.ExistsQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ExpertLookupServiceImpl implements ExpertLookupService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private ItemLookupService itemLookupService;

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;

    @Override
    public UserExpertBaseDto findUserExpertBase(String expertCode) throws IOException, NonexistentEntityException {
        final Expert expert = elasticSearchDao.getDocument(indexName, expertCode, Expert.class);
        if (expert == null) throw new NonexistentEntityException(Expert.class);
        return convertToBase(expert);
    }

    @Override
    public UserExpertBaseDto findUserExpertBase(Long expertId) throws IOException, NonexistentEntityException {
        return findUserExpertBase(ExpertUtil.convertToExpertCode(expertId));
    }

    @Override
    public Map<String, UserExpertBaseDto> findUserExpertBasesMap(Set<String> expertCodes) throws IOException {
        final List<Expert> experts = elasticSearchDao.getDocuments(indexName, expertCodes, Expert.class);
        final Map<String, UserExpertBaseDto> expertBaseDtoMap = experts.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(Expert::getExpertCode, this::convertToBase));
        final Set<String> claimedExpertCodes = experts.stream()
                .filter(Objects::nonNull)
                .map(Expert::getClaimedById)
                .filter(Objects::nonNull)
                .map(ExpertUtil::convertToUserCode)
                .collect(Collectors.toSet());
        if (!claimedExpertCodes.isEmpty()) {
            final List<Expert> claimingExperts = elasticSearchDao.getDocuments(indexName, expertCodes, Expert.class);
            for (Expert claimingExpert : claimingExperts) {
                claimingExpert.getExpertIds().stream()
                        .map(ExpertUtil::convertToExpertCode)
                        .filter(expertCodes::contains)
                        .findFirst()
                        .ifPresent(x -> expertBaseDtoMap.put(x, convertToBase(claimingExpert)));
            }
        }
        return expertBaseDtoMap;
    }

    @Override
    public List<ItemNotRegisteredExpertDto> findNotRegisteredExperts(Set<Long> expertIds) throws IOException {
        final ItemService service = ServiceRegistryUtil.getService(ItemService.class);
        return service.findNotRegisteredExperts(expertIds);
    }

    @Override
    public List<UserExpertBaseDto> findUserExpertBases(Set<String> expertCodes) throws IOException {
        final List<Expert> experts = elasticSearchDao.getDocuments(indexName, expertCodes, Expert.class);
        return experts.stream().filter(Objects::nonNull).map(this::convertToBase).collect(Collectors.toList());
    }

    @Override
    public List<UserExpertBaseDto> findExpertBases(Set<Long> expertIds) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(
                new BoolQueryBuilder()
                        .filter(new TermsQueryBuilder("expertIds", expertIds))
                        .mustNot(new ExistsQueryBuilder("claimedById")));
        final SearchResponseEntityWrapper<Expert> expertSearchResponseEntityWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Expert.class);
        return expertSearchResponseEntityWrapper.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource)
                .map(this::convertToBase)
                .collect(Collectors.toList());
    }

    @Override
    public ExpertClaimDto findExpertToClaim(Long expertId) throws IOException, NonexistentEntityException {
        final String expertCode = ExpertUtil.convertToExpertCode(expertId);
        final Expert expert = elasticSearchDao.getDocument(indexName, expertCode, Expert.class);
        if (expert == null) throw new NonexistentEntityException(Expert.class);
        final ExpertClaimDto expertClaimDto = new ExpertClaimDto();
        expertClaimDto.setExpertId(expertId);
        expertClaimDto.setFullName(expert.getFullName());
        expertClaimDto.setOrganizationIds(expert.getOrganizationIds());
        expertClaimDto.setVerifiedOrganizationIds(expert.getVerifiedOrganizationIds());
        expertClaimDto.setExpertItemDtos(itemLookupService.findItemsForExpertToClaim(expertId));

        return expertClaimDto;
    }

    @Override
    public Set<String> findOrganizationsExpertCodes(Set<Long> organizationIds, boolean verified) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = prepareSearchSourceBuilderForOrganizationLookup(organizationIds, verified)
                .fetchSource(new String[]{"id"}, new String[]{});
        final SearchHits searchHits = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder);
        final SearchHit[] hits = searchHits.getHits();
        return Arrays.stream(hits).map(SearchHit::getId).collect(Collectors.toSet());
    }

    @Override
    public OrganizationExpertsDto findOrganizationsExpertIds(Set<Long> organizationIds) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = prepareSearchSourceBuilderForOrganizationLookup(organizationIds, false).fetchSource(new String[]{"userId", "expertIds"}, new String[]{});
        final SearchResponseEntityWrapper<Expert> expertSearchResponseEntityWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Expert.class);
        final OrganizationExpertsDto organizationExperts = new OrganizationExpertsDto();
        for (SearchResponseEntity<Expert> expert : expertSearchResponseEntityWrapper.getSearchResponseEntities()) {
            final Long userId = expert.getSource().getUserId();
            if (userId != null) {
                organizationExperts.getUserIds().add(userId);
            } else {
                expert.getSource().getExpertIds().stream().findFirst().ifPresent(expertId -> organizationExperts.getExpertIds().add(expertId));
            }
        }
        return organizationExperts;
    }

    private UserExpertBaseDto convertToBase(Expert expert) {
        final UserExpertBaseDto userExpertBaseDto = new UserExpertBaseDto();
        if (expert.getUserId() != null) {
            userExpertBaseDto.setUserId(expert.getUserId());
        } else if (expert.getExpertIds() != null && expert.getExpertIds().size() == 1) {
            final Long expertId = expert.getExpertIds().iterator().next();
            userExpertBaseDto.setExpertId(expertId);
        }

        userExpertBaseDto.setExpertCode(expert.getExpertCode());
        userExpertBaseDto.setName(expert.getFullName());
        userExpertBaseDto.setOrganizationIds(expert.getOrganizationIds());
        return userExpertBaseDto;
    }

    private SearchSourceBuilder prepareSearchSourceBuilderForOrganizationLookup(Set<Long> organizationIds, boolean verified) {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final TermsQueryBuilder termsQueryBuilder = new TermsQueryBuilder(verified ? "verifiedOrganizationIds" : "organizationIds", organizationIds);
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder()
                .filter(termsQueryBuilder)
                .filter(new ExistsQueryBuilder("organizationIds"))
                .mustNot(new ExistsQueryBuilder("claimedById"));
        searchSourceBuilder.query(boolQueryBuilder)
                .size(maxResultWindow);
        return searchSourceBuilder;
    }
}
