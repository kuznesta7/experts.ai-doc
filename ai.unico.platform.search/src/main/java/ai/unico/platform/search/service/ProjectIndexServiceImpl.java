package ai.unico.platform.search.service;

import ai.unico.platform.extdata.dto.DWHProjectDto;
import ai.unico.platform.extdata.dto.ProjectFinanceDto;
import ai.unico.platform.extdata.service.DWHProjectService;
import ai.unico.platform.recommendation.api.enums.RecombeeScenario;
import ai.unico.platform.recommendation.api.service.RecombeeManipulationService;
import ai.unico.platform.search.api.service.ExpertEvaluationService;
import ai.unico.platform.search.api.service.ProjectIndexService;
import ai.unico.platform.search.api.service.SearchMappingsLoaderService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Project;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.ExpertUtil;
import ai.unico.platform.search.util.ItemUtil;
import ai.unico.platform.search.util.ProjectUtil;
import ai.unico.platform.store.api.dto.ProjectIndexDto;
import ai.unico.platform.store.api.enums.IndexHistoryStatus;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.api.service.*;
import ai.unico.platform.util.CollectionsUtil;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.model.UserContext;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ProjectIndexServiceImpl implements ProjectIndexService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private SearchMappingsLoaderService searchMappingsLoaderService;

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;

    private final RecombeeManipulationService recombeeManipulationService = ServiceRegistryProxy.createProxy(RecombeeManipulationService.class);

    private boolean indexingInProgress = false;

    @Override
    public void reindexProjects(Integer limit, boolean clean, String target, UserContext userContext) {

        if (indexingInProgress) return;
        indexingInProgress = true;
        final IndexHistoryService indexHistoryService = ServiceRegistryUtil.getService(IndexHistoryService.class);
        final DWHProjectService dwhProjectService = ServiceRegistryUtil.getService(DWHProjectService.class);
        final ItemService itemService = ServiceRegistryUtil.getService(ItemService.class);
        final OrganizationService organizationService = ServiceRegistryUtil.getService(OrganizationService.class);
        final ProjectProgramService projectProgramService = ServiceRegistryUtil.getService(ProjectProgramService.class);

        final Long indexHistoryId = indexHistoryService.saveIndexHistory(IndexTarget.PROJECTS, true, userContext);

        try {
            if (clean) {
                final String mappings = searchMappingsLoaderService.loadMappings("projects");
                if (mappings == null) throw new FileNotFoundException();
                try {
                    elasticSearchDao.deleteIndex(indexName);
                } catch (Exception ignore) {
                }
                elasticSearchDao.initIndex(indexName, mappings);
            }
            if (target == null || "ALL".equals(target) || "DWH".equals(target)) {
                System.out.println("Starting DWH project index.");
                Integer offset1 = 0;
                final Map<Long, Long> projectProgramIdsMap = projectProgramService.updateProgramsFromDWH();

                while (true) {
                    final List<DWHProjectDto> projectDtos = dwhProjectService.findProjects(limit, offset1);
                    if (projectDtos.size() == 0) break;

                    final Set<Long> originalItemIds = new HashSet<>();
                    final Set<Long> originalOrganizationIds = new HashSet<>();
                    final Set<String> expertCodes = new HashSet<>();

                    for (DWHProjectDto projectDto : projectDtos) {
                        final Set<Long> participatingOriginalOrganizationIds = projectDto.getParticipatingOriginalOrganizationIds();
                        final Set<Long> providingOriginalOrganizationIds = projectDto.getProvidingOriginalOrganizationIds();
                        final Set<Long> projectOriginalItemIds = projectDto.getProjectOriginalItemIds();
                        final Set<String> projectExpertCodes = projectDto.getProjectExpertIds().stream().map(ExpertUtil::convertToExpertCode).collect(Collectors.toSet());
                        originalOrganizationIds.addAll(participatingOriginalOrganizationIds);
                        originalOrganizationIds.addAll(providingOriginalOrganizationIds);
                        originalItemIds.addAll(projectOriginalItemIds);
                        expertCodes.addAll(projectExpertCodes);
                    }

                    projectDtos.stream().map(DWHProjectDto::getProjectFinances)
                            .forEach(x -> x.forEach(y -> {
                                if (y.getOriginalOrganizationId() != null) {
                                    originalOrganizationIds.add(y.getOriginalOrganizationId());
                                }
                            }));

                    final Map<Long, Long> verifiedItemIdsMap = itemService.findVerifiedItemIdsMap(originalItemIds);
                    final Map<Long, Long> organizationIdsMap = organizationService.findOrganizationIdsMap(originalOrganizationIds);
                    final Map<String, String> substituteExpertCodes = ExpertUtil.findSubstituteExpertCodes(expertCodes);

                    final List<Project> projectsToIndex = new ArrayList<>();
                    for (DWHProjectDto projectDto : projectDtos) {
                        final Project project = ProjectUtil.convert(projectDto);
                        final Set<String> itemCodes = projectDto.getProjectOriginalItemIds().stream()
                                .map(x -> convertOriginalItemIdToCode(x, verifiedItemIdsMap))
                                .collect(Collectors.toSet());
                        final Set<Long> providingOrganizationIds = projectDto.getProvidingOriginalOrganizationIds().stream()
                                .map(organizationIdsMap::get)
                                .collect(Collectors.toSet());
                        final Set<Long> participatingOrganizationIds = projectDto.getParticipatingOriginalOrganizationIds().stream()
                                .map(organizationIdsMap::get)
                                .collect(Collectors.toSet());

                        final Set<String> projectExpertCodes = projectDto.getProjectExpertIds().stream().map(ExpertUtil::convertToExpertCode).collect(Collectors.toSet());
                        ExpertUtil.replaceExpertCodes(projectExpertCodes, substituteExpertCodes);
                        project.setExpertCodes(projectExpertCodes);

                        final Set<Long> evidenceOrganizationIds = new HashSet<>();
                        evidenceOrganizationIds.addAll(providingOrganizationIds);
                        evidenceOrganizationIds.addAll(participatingOrganizationIds);

                        project.setItemCodes(itemCodes);
                        project.setProvidingOrganizationIds(providingOrganizationIds);
                        project.setOrganizationIds(participatingOrganizationIds);
                        project.setEvidenceOrganizationIds(evidenceOrganizationIds);
                        if (projectDto.getProjectProgramId() != null) {
                            project.setProjectProgramId(projectProgramIdsMap.get(projectDto.getProjectProgramId()));
                        }
                        for (ProjectFinanceDto projectFinanceDto : projectDto.getProjectFinances()) {
                            final Project.ProjectOrganizationBudget finance = new Project.ProjectOrganizationBudget();
                            final Long organizationId = organizationIdsMap.get(projectFinanceDto.getOriginalOrganizationId());
                            finance.setOrganizationId(organizationId);
                            finance.setYear(projectFinanceDto.getYear());
                            finance.setTotal(projectFinanceDto.getTotal());
                            finance.setNationalSupport(projectFinanceDto.getNationalSupport());
                            finance.setPrivateFinances(projectFinanceDto.getPrivateFinances());
                            finance.setOtherFinances(projectFinanceDto.getOtherFinances());
                            project.getFinances().add(finance);
                        }

                        projectsToIndex.add(project);
                    }

                    elasticSearchDao.doIndex(indexName, projectsToIndex);
                    offset1 += limit;
                    System.out.println(offset1);
                }
            }

            // todo - from here
            if (target == null || "ALL".equals(target) || "ST".equals(target)) {
                System.out.println("Starting ST project index.");
                final ProjectService projectService = ServiceRegistryUtil.getService(ProjectService.class);
                Integer offset2 = 0;
                while (true) {
                    List<ProjectIndexDto> projectIndexDtos = projectService.findAllProjects(limit, offset2);
                    if (projectIndexDtos.size() == 0) break;
                    final Set<String> projectToRemoveCodes = projectIndexDtos.stream()
                            .map(ProjectIndexDto::getOriginalProjectId)
                            .filter(Objects::nonNull)
                            .map(ProjectUtil::toOriginalProjectCode)
                            .collect(Collectors.toSet());

                    elasticSearchDao.doIndex(indexName, projectIndexDtos.stream().map(ProjectUtil::convert).collect(Collectors.toList()));
                    elasticSearchDao.deleteDocuments(indexName, projectToRemoveCodes);
                    offset2 += limit;
                }
            }
            // todo - to here
            replaceExperts(ExpertUtil.findSubstituteExpertCodes(), false);
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FINISHED, userContext);
        } catch (Exception e) {
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FAILED, e.getCause().toString(), userContext);
            e.printStackTrace();
        }
        indexingInProgress = false;
        System.out.println("Project index done.");
    }

    @Override
    public void updateProject(ProjectIndexDto projectIndexDto) throws IOException {
        final Project currentProject = elasticSearchDao.getDocument(indexName, ProjectUtil.toConfirmedProjectCode(projectIndexDto.getProjectId()), Project.class);
        final Project project = ProjectUtil.convert(projectIndexDto);
        elasticSearchDao.doIndex(indexName, project);
        recombeeManipulationService.uploadEntry(project, RecombeeScenario.PROJECT.getPrefix());
        final Long originalProjectId = project.getOriginalProjectId();
        if (project.getProjectId() != null && originalProjectId != null) {
            try {
                elasticSearchDao.deleteDocument(indexName, ProjectUtil.toOriginalProjectCode(originalProjectId));
            } catch (Exception ignore) {
            }
        }
        try {
            elasticSearchDao.flush(indexName);
            final Set<String> newExpertCodes = project.getExpertCodes();
            final ExpertEvaluationService expertEvaluationService = ServiceRegistryUtil.getService(ExpertEvaluationService.class);
            if (currentProject != null) {
                final Set<String> expertCodes = CollectionsUtil.mergeCollections(currentProject.getExpertCodes(), newExpertCodes);
                expertEvaluationService.recalculateComputedFieldsForExperts(expertCodes);
            } else {
                expertEvaluationService.recalculateComputedFieldsForExperts(newExpertCodes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void replaceExperts(Map<String, String> replacementMap, boolean originalOnly) throws IOException {
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder().filter(new TermsQueryBuilder("expertCodes", replacementMap.keySet()));
        if (originalOnly) {
            boolQueryBuilder.filter(new TermsQueryBuilder("originalExpertCodes", replacementMap.values()));
        }
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(boolQueryBuilder)
                .size(maxResultWindow);
        final SearchResponseEntityWrapper<Project> response = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Project.class);
        if (response.getResultCount() == 0) return;
        final List<Project> projects = response.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource)
                .collect(Collectors.toList());
        for (Project project : projects) {
            doReplaceExpertCodes(project, replacementMap);
        }
        elasticSearchDao.doIndex(indexName, projects);
    }

    private void doReplaceExpertCodes(Project project, Map<String, String> replacementMap) {
        ExpertUtil.replaceExpertCodes(project.getExpertCodes(), replacementMap);
    }

    private String convertOriginalItemIdToCode(Long originalItemId, Map<Long, Long> verifiedItemIdsMap) {
        if (verifiedItemIdsMap.containsKey(originalItemId)) {
            return ItemUtil.convertToItemCode(verifiedItemIdsMap.get(originalItemId));
        }
        return ItemUtil.convertToDWHItemCode(originalItemId);
    }
}
