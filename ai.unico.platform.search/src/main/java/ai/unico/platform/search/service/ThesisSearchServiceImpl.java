package ai.unico.platform.search.service;

import ai.unico.platform.search.api.dto.ThesisPreviewDto;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.service.ExpertLookupService;
import ai.unico.platform.search.api.service.OntologySearchService;
import ai.unico.platform.search.api.service.OrganizationSearchService;
import ai.unico.platform.search.api.service.ThesisSearchService;
import ai.unico.platform.search.api.wrapper.ThesisWrapper;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.model.Thesis;
import ai.unico.platform.search.util.ElasticSearchUtil;
import ai.unico.platform.search.util.ThesisUtil;
import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.dto.UserExpertBaseDto;
import ai.unico.platform.store.api.service.ItemTypeService;
import ai.unico.platform.util.KeywordsUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;
import ai.unico.platform.util.enums.ExpertSearchType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.ScriptSortBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.elasticsearch.search.sort.SortOrder.DESC;

public class ThesisSearchServiceImpl implements ThesisSearchService {


    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private ExpertLookupService expertLookupService;

    @Autowired
    private OrganizationSearchService organizationSearchService;

    @Override
    public ThesisWrapper findThesis(EvidenceFilter evidenceFilter, ExpertSearchResultFilter.Order order) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder query = new BoolQueryBuilder();

        final Long organizationId = evidenceFilter.getOrganizationId();
        final Set<Long> participatingOrganizationIds = evidenceFilter.getParticipatingOrganizationIds();
        final Set<String> participatingExpertCodes = evidenceFilter.getExpertCodes();
        final Set<Confidentiality> confidentiality = evidenceFilter.getConfidentiality();
        if (confidentiality.isEmpty()) {
            confidentiality.add(Confidentiality.PUBLIC);
        }

        query.filter(new TermsQueryBuilder("confidentiality", confidentiality));
        query.filter(new ExistsQueryBuilder("thesisName"));
        if (evidenceFilter.getResultTypeGroupIds().size() > 0) {
            final ItemTypeService itemTypeService = ServiceRegistryUtil.getService(ItemTypeService.class);
            final Set<Long> thesisTypeIds = itemTypeService.findItemTypeIds(evidenceFilter.getResultTypeGroupIds());
            query.filter(new TermsQueryBuilder("thesisTypeId", thesisTypeIds));
        }
        if (evidenceFilter.getYearFrom() != null || evidenceFilter.getYearTo() != null) {
            final RangeQueryBuilder rangeQueryBuilder = new RangeQueryBuilder("year")
                    .from(evidenceFilter.getYearFrom())
                    .to(evidenceFilter.getYearTo());
            query.filter(rangeQueryBuilder);
        }
        if (!evidenceFilter.getOrganizationIds().isEmpty()) {
            query.filter(new TermsQueryBuilder("universityId", evidenceFilter.getOrganizationIds()));
        }

        if (!participatingOrganizationIds.isEmpty()) {
            query.filter(new TermsQueryBuilder("organizationIds", participatingOrganizationIds));
        }

        if (!participatingExpertCodes.isEmpty()) {
            query.filter(new TermsQueryBuilder("supervisor", participatingExpertCodes));
        }

        if (evidenceFilter.getQuery() != null && !evidenceFilter.getQuery().equals("")) {
            if (evidenceFilter.getExpertSearchType() == null || evidenceFilter.getExpertSearchType() == ExpertSearchType.EXPERTISE) {
                final OntologySearchService ontologySearchService = ServiceRegistryUtil.getService(OntologySearchService.class);
                ontologySearchService.fillItemSearchOntology(evidenceFilter);
                final Map<String, Float> alternateQueries = evidenceFilter.getAlternateQueries();
                alternateQueries.put(evidenceFilter.getQueryForSearch(), 1F);
                for (Map.Entry<String, Float> q : alternateQueries.entrySet()) {
                    final Map<String, Float> fields = new HashMap<>();
                    fields.put("thesisName", 2.0F);
                    fields.put("thesisDescription", 1.0F);
                    fields.put("keywords", 3.0F);
                    final QueryStringQueryBuilder queryStringQueryBuilder = new QueryStringQueryBuilder(ElasticSearchUtil.prepareQueryStringQuery(q.getKey()))
                            .defaultOperator(Operator.AND).fields(fields).boost(q.getValue());
                    query.should(queryStringQueryBuilder);
                    query.minimumShouldMatch(1);
                }
            }
        }
        if (order.equals(ExpertSearchResultFilter.Order.QUALITY) && organizationId != null) {
            final HashMap<String, Object> params = new HashMap<>();
            params.put("organizationId", organizationId);
            params.put("organizationIds", evidenceFilter.getOrganizationIds());
            final Script sortScript = new Script(ScriptType.INLINE, "painless", "if (doc['universityId']==params.organizationId) return 2; for (o1 in doc['organizationIds'].values) {for (o2 in params.organizationIds) {if (o1==o2) return 1;}} return 0;", params);
            searchSourceBuilder.sort(new ScriptSortBuilder(sortScript, ScriptSortBuilder.ScriptSortType.NUMBER).order(DESC));

        }
        searchSourceBuilder.query(query)
                .size(evidenceFilter.getLimit())
                .from((evidenceFilter.getPage() - 1) * evidenceFilter.getLimit());
        ThesisUtil.addThesisSortFunction(searchSourceBuilder, order, null);

        searchSourceBuilder.size(evidenceFilter.getLimit()).from((evidenceFilter.getPage() - 1) * evidenceFilter.getLimit());
        final SearchResponseEntityWrapper<Thesis> result = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Thesis.class);
        final ThesisWrapper thesisWrapper = new ThesisWrapper();
        thesisWrapper.setLimit(evidenceFilter.getLimit());
        thesisWrapper.setPage(evidenceFilter.getPage());
        thesisWrapper.setNumberOfAllItems(result.getResultCount());
        Set<String> expertIds = result.getSearchResponseEntities()
                .stream().map(SearchResponseEntity::getSource)
                .map(x -> Arrays.asList(x.getAssignee(), x.getReviewer(), x.getSupervisor()))
                .flatMap(List::stream).filter(Objects::nonNull).collect(Collectors.toSet());
        Set<Long> organizationIds = result.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource).map(Thesis::getUniversityId).filter(Objects::nonNull).collect(Collectors.toSet());
        final Map<String, UserExpertBaseDto> expertBaseDtoMap = expertLookupService.findUserExpertBases(expertIds).stream().collect(Collectors.toMap(UserExpertBaseDto::getExpertCode, Function.identity()));
        final Map<Long, OrganizationBaseDto> organizationBaseMap = organizationSearchService.findBaseOrganizationsMap(organizationIds);
        for (SearchResponseEntity<Thesis> entity : result.getSearchResponseEntities()) {
            final Thesis thesis = entity.getSource();
            final ThesisPreviewDto thesisDto = convert(thesis);
            thesisDto.setAssignee(expertBaseDtoMap.get(thesis.getAssignee()));
            thesisDto.setReviewer(expertBaseDtoMap.get(thesis.getReviewer()));
            thesisDto.setSupervisor(expertBaseDtoMap.get(thesis.getSupervisor()));
            thesisDto.setUniversity(organizationBaseMap.get(thesis.getUniversityId()));
            thesisWrapper.getThesisPreviewDtos().add(thesisDto);
        }

        return thesisWrapper;
    }

    @Override
    public ThesisPreviewDto getThesis(String thesisCode) throws IOException {
        final Thesis thesis = elasticSearchDao.getDocument(indexName, thesisCode, Thesis.class);

        final Map<String, UserExpertBaseDto> expertBaseDtoMap = expertLookupService.findUserExpertBases(Stream.of(thesis.getSupervisor(), thesis.getReviewer(), thesis.getAssignee()).filter(Objects::nonNull).collect(Collectors.toSet())).stream().collect(Collectors.toMap(UserExpertBaseDto::getExpertCode, Function.identity()));
        final Map<Long, OrganizationBaseDto> organizationBaseMap = organizationSearchService.findBaseOrganizationsMap(thesis.getOrganizationIds());

        final ThesisPreviewDto thesisDto = convert(thesis);
        thesisDto.setAssignee(expertBaseDtoMap.get(thesis.getAssignee()));
        thesisDto.setReviewer(expertBaseDtoMap.get(thesis.getReviewer()));
        thesisDto.setSupervisor(expertBaseDtoMap.get(thesis.getSupervisor()));
        thesisDto.setUniversity(organizationBaseMap.get(thesis.getUniversityId()));
        return thesisDto;
    }

    @Override
    public ExpertThesisStatistics findExpertThesisStatistics(String expertCode, Set<Long> organizationIds, boolean countConfidential) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        if (!organizationIds.isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("organizationIds", organizationIds));
        }
        if (!countConfidential) {
            boolQueryBuilder.filter(new TermQueryBuilder("confidentiality", "PUBLIC"));
        }
        boolQueryBuilder.filter(new TermQueryBuilder("supervisor", expertCode));
        searchSourceBuilder.query(boolQueryBuilder).size(0);
        final SearchResponseEntityWrapper<ThesisSearchService.ExpertThesisStatistics> resultWrapper = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, ThesisSearchService.ExpertThesisStatistics.class);
        final ThesisSearchService.ExpertThesisStatistics expertThesisStatistics = new ThesisSearchService.ExpertThesisStatistics();
        expertThesisStatistics.setExpertCode(expertCode);
        expertThesisStatistics.setNumberOfThesis(resultWrapper.getResultCount());
        return expertThesisStatistics;
    }

    private ThesisPreviewDto convert(Thesis thesis) {
        final ThesisPreviewDto thesisPreviewDto = new ThesisPreviewDto();
        thesisPreviewDto.setThesisCode(thesis.getId());
        thesisPreviewDto.setThesisId(thesis.getThesisId());
        thesisPreviewDto.setThesisName(thesis.getThesisName());
        thesisPreviewDto.setThesisDescription(thesis.getThesisDescription());
        thesisPreviewDto.setThesisTypeId(thesis.getThesisTypeId());
        thesisPreviewDto.setYear(thesis.getYear());
        thesisPreviewDto.setConfidentiality(thesis.getConfidentiality());
        thesisPreviewDto.setKeywords(KeywordsUtil.sortKeywordList(thesis.getKeywords()));
        return thesisPreviewDto;
    }
}
