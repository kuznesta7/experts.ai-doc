package ai.unico.platform.search.service;

import ai.unico.platform.recommendation.api.dto.RecommendationWrapperDto;
import ai.unico.platform.recommendation.api.enums.RecombeeScenario;
import ai.unico.platform.recommendation.api.service.RecombeeRecommendationService;
import ai.unico.platform.search.api.dto.EvidenceExpertPreviewDto;
import ai.unico.platform.search.api.dto.EvidenceExpertPreviewWithItemsDto;
import ai.unico.platform.search.api.dto.ExpertStatisticsDto;
import ai.unico.platform.search.api.dto.SearchResultBucketDto;
import ai.unico.platform.search.api.filter.EvidenceFilter;
import ai.unico.platform.search.api.filter.ExpertSearchFilter;
import ai.unico.platform.search.api.service.*;
import ai.unico.platform.search.api.wrapper.EvidenceExpertWrapper;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Expert;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.OrganizationUtil;
import ai.unico.platform.search.util.RecombeeFilterConverterUtil;
import ai.unico.platform.store.api.dto.ItemTypeGroupDto;
import ai.unico.platform.store.api.dto.OrganizationBaseDto;
import ai.unico.platform.store.api.dto.OrganizationDetailDto;
import ai.unico.platform.store.api.dto.PreClaimDto;
import ai.unico.platform.store.api.service.ClaimProfileService;
import ai.unico.platform.store.api.service.ItemTypeService;
import ai.unico.platform.util.CSVUtil;
import ai.unico.platform.util.ServiceRegistryProxy;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.enums.ExpertSearchType;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.*;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScriptScoreFunctionBuilder;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filter.FilterAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.missing.Missing;
import org.elasticsearch.search.aggregations.bucket.missing.MissingAggregationBuilder;
import org.elasticsearch.search.aggregations.support.ValueType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.ScriptSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class EvidenceExpertServiceImpl implements EvidenceExpertService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private ItemSearchService itemSearchService;

    @Autowired
    private OrganizationSearchService organizationSearchService;

    @Autowired
    private ProjectSearchService projectSearchService;

    @Autowired
    private CommercializationProjectSearchService commercializationProjectSearchService;

    @Autowired
    private OntologySearchService ontologySearchService;

    private final RecombeeRecommendationService recombeeRecommendationService = ServiceRegistryProxy.createProxy(RecombeeRecommendationService.class);

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;

    private BoolQueryBuilder setupFilters(EvidenceFilter filter) {
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        final Long organizationId = filter.getOrganizationId();
        final Set<Long> organizationIds = filter.getOrganizationIds();

        boolQueryBuilder.must(new ExistsQueryBuilder("organizationIds"))
                .mustNot(new ExistsQueryBuilder("claimedById"));
        if (!organizationIds.isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("organizationIds", organizationIds));
        }
        final Set<Long> affiliatedOrganizationIds = filter.getAffiliatedOrganizationIds();
        if (!affiliatedOrganizationIds.isEmpty()) {
            boolQueryBuilder.mustNot(new TermsQueryBuilder("organizationIds", affiliatedOrganizationIds));
            boolQueryBuilder.filter(new TermsQueryBuilder("affiliatedOrganizationIds", affiliatedOrganizationIds));
        }

        if(!filter.getSuborganizationIds().isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("organizationIds", filter.getSuborganizationIds()));
        }

        if (filter.isVerifiedOnly()) {
            if (organizationId != null) {
                boolQueryBuilder.filter(new TermsQueryBuilder("verifiedOrganizationIds", organizationIds));
            } else {
                boolQueryBuilder.filter(new ExistsQueryBuilder("verifiedOrganizationIds"));
            }
        }
        if (filter.isNotVerifiedOnly()) {
            if (organizationId != null) {
                boolQueryBuilder.mustNot(new TermsQueryBuilder("verifiedOrganizationIds", organizationIds));
            } else {
                boolQueryBuilder.mustNot(new ExistsQueryBuilder("verifiedOrganizationIds"));
            }
        }
        if (filter.isRetired()) {
            boolQueryBuilder.filter(new ExistsQueryBuilder("retirementDate"));
        }

        if (!filter.isRetirementIgnored() ) {
            boolQueryBuilder.mustNot(new ExistsQueryBuilder("retirementDate"));
            if (organizationId != null)
                boolQueryBuilder.mustNot(new TermQueryBuilder("retiredOrganizationIds", organizationId));
        }

        if (filter.getCountryCodes() != null && !filter.getCountryCodes().isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("countryCodes", filter.getCountryCodes()));
        }

        if (filter.isActiveOnly() && organizationId != null){
            boolQueryBuilder.mustNot(new TermQueryBuilder("retiredOrganizationIds", organizationId));
        }
        return boolQueryBuilder;
    }

    @Override
    public EvidenceExpertWrapper findAnalyticsExperts(EvidenceFilter filter) throws IOException {
        final EvidenceExpertWrapper evidenceExpertWrapper = new EvidenceExpertWrapper();
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final Long organizationId = filter.getOrganizationId();
        final Set<Long> organizationIds = filter.getOrganizationIds();
        final BoolQueryBuilder boolQueryBuilder = setupFilters(filter);

        if (filter.getQuery() != null && !filter.getQuery().isEmpty()) {
            final BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
            if (filter.getExpertSearchType() == null || filter.getExpertSearchType().equals(ExpertSearchType.EXPERTISE)) {
                addSearchByExpertiseCriteria(filter.getQueryForSearch(), queryBuilder);
                searchSourceBuilder.minScore(1.1F);
            }
            if (filter.getExpertSearchType() == null || filter.getExpertSearchType().equals(ExpertSearchType.NAME)) {
                queryBuilder.should(new MatchQueryBuilder("fullName", filter.getQueryForSearch()).operator(Operator.AND));
            }
            queryBuilder.minimumShouldMatch(1);
            boolQueryBuilder.must(queryBuilder);
        } else if (!filter.getSortByFullName()) {
            final HashMap<String, Object> params = new HashMap<>();
            params.put("organizationId", organizationId);
            params.put("organizationIds", organizationIds);
            searchSourceBuilder.sort(new ScriptSortBuilder(new Script("if (doc['retirementDate'].isEmpty()) return 1; return 0;"), ScriptSortBuilder.ScriptSortType.NUMBER).order(SortOrder.DESC));
            if (!filter.isIgnoreVerifications()) {
                final Script sortScript = new Script(ScriptType.INLINE, "painless", "for (o1 in doc['verifiedOrganizationIds'].values) {if (o1==params.organizationId) return 2; for (o2 in params.organizationIds) {if (o1==o2) return 1;}} return 0;", params);
                searchSourceBuilder.sort(new ScriptSortBuilder(sortScript, ScriptSortBuilder.ScriptSortType.NUMBER).order(SortOrder.DESC));
            }
            searchSourceBuilder.sort("expertRank", SortOrder.DESC);
        }

        if (filter.getSortByFullName()) {
            searchSourceBuilder.sort("fullName.raw", SortOrder.ASC);
        }

        searchSourceBuilder.query(boolQueryBuilder)
                .size(filter.getLimit())
                .from(filter.getLimit() * (filter.getPage() - 1));
        sourceBuilderToEvidenceWrapper(searchSourceBuilder, filter, evidenceExpertWrapper);
        return evidenceExpertWrapper;
    }

    @Override
    public ExpertStatisticsDto findExpertStatistics(Set<Long> organizationIds) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(new BoolQueryBuilder()
                .filter(new TermsQueryBuilder("organizationIds", organizationIds))
                .mustNot(new ExistsQueryBuilder("claimedById")))
                .fetchSource(false);
        searchSourceBuilder
                .aggregation(new MissingAggregationBuilder("allUnclaimedExperts", ValueType.LONG).field("claimedBy")
                        .subAggregation(new FilterAggregationBuilder("notVerifiedExperts", new BoolQueryBuilder().mustNot(new TermsQueryBuilder("verifiedOrganizationIds", organizationIds))))
                        .subAggregation(new FilterAggregationBuilder("verifiedExperts", new TermsQueryBuilder("verifiedOrganizationIds", organizationIds))));
        final SearchResponse searchResponse = elasticSearchDao.doSearch(indexName, searchSourceBuilder);
        final Missing allUnclaimedExperts = searchResponse.getAggregations().get("allUnclaimedExperts");
        final Filter notVerifiedExperts = allUnclaimedExperts.getAggregations().get("notVerifiedExperts");
        final Filter verifiedExperts = allUnclaimedExperts.getAggregations().get("verifiedExperts");

        final ExpertStatisticsDto expertStatisticsDto = new ExpertStatisticsDto();
        expertStatisticsDto.setNumberOfAllExperts(allUnclaimedExperts.getDocCount());
        expertStatisticsDto.setNumberOfNotVerifiedExperts(notVerifiedExperts.getDocCount());
        expertStatisticsDto.setNumberOfVerifiedExperts(verifiedExperts.getDocCount());

        return expertStatisticsDto;
    }

    @Override
    public FileDto exportAnalyticsExperts(EvidenceFilter evidenceFilter) throws IOException {
        final ItemTypeService itemTypeService = ServiceRegistryUtil.getService(ItemTypeService.class);
        evidenceFilter.setLimit(maxResultWindow);
        evidenceFilter.setPage(1);
        evidenceFilter.setRetired(true);
        final EvidenceExpertWrapper analyticsExperts = findAnalyticsExperts(evidenceFilter);
        final List<ItemTypeGroupDto> itemTypeGroups = itemTypeService.findItemTypeGroups();

        final List<List<String>> rows = new ArrayList<>();
        final List<String> header = new ArrayList<>();
        header.add("expert code");
        header.add("expert_name");
        header.add("number of all outcomes");
        for (ItemTypeGroupDto itemTypeGroup : itemTypeGroups) {
            header.add("number of " + itemTypeGroup.getItemTypeGroupTitle().toLowerCase());
        }

        rows.add(header);

        for (EvidenceExpertPreviewDto expertDto : analyticsExperts.getAnalyticsExpertPreviewDtos()) {
            final List<String> row = new ArrayList<>();
            row.add(expertDto.getExpertCode());
            row.add(expertDto.getName());
            final Long numberOfItems = expertDto.getNumberOfItems();
            row.add(numberOfItems != null ? numberOfItems.toString() : "0");
            for (ItemTypeGroupDto itemTypeGroup : itemTypeGroups) {
                final Long groupId = itemTypeGroup.getItemTypeGroupId();
                row.add(String.valueOf(expertDto.getItemTypeGroupCounts().get(groupId)));
            }
            rows.add(row);
        }
        return CSVUtil.createFile(rows, "experts-ai_analytics_experts", false);
    }

    private Map<String, SearchResultBucketDto> addSearchByExpertiseCriteria(String expertise, BoolQueryBuilder boolQueryBuilder) throws IOException {
        final Map<String, SearchResultBucketDto> searchResultBucketMap = new HashMap<>();
        final ExpertSearchFilter expertSearchFilter = new ExpertSearchFilter();
        expertSearchFilter.setQuery(expertise);
        ontologySearchService.fillItemSearchOntology(expertSearchFilter);
        if (!expertSearchFilter.isExcludeOutcomes()) {
            final List<SearchResultBucketDto> expertsForItemSearch = itemSearchService.findExpertsForItemSearch(expertSearchFilter);
            final List<SearchResultBucketDto> expertsForItemSearchCut;
            if (expertsForItemSearch.size() > 200) {
                final Optional<SearchResultBucketDto> best = expertsForItemSearch.stream().max(Comparator.comparing(SearchResultBucketDto::getRank));
                expertsForItemSearchCut = expertsForItemSearch.stream().filter(r -> r.getRank() > best.get().getRank() / 50).collect(Collectors.toList());
            } else {
                expertsForItemSearchCut = expertsForItemSearch;
            }
            expertsForItemSearchCut.forEach(x -> searchResultBucketMap.put(x.getExpertCode(), x));
        }
        if (!expertSearchFilter.isExcludeInvestProjects()) {
            final List<SearchResultBucketDto> expertsForProjectSearchBuckets = commercializationProjectSearchService.findExpertsForProjectSearch(expertSearchFilter);
            for (SearchResultBucketDto expertsForProjectSearchBucket : expertsForProjectSearchBuckets) {
                final String expertCode = expertsForProjectSearchBucket.getExpertCode();
                if (searchResultBucketMap.containsKey(expertCode)) {
                    final SearchResultBucketDto searchResultBucketDto = searchResultBucketMap.get(expertCode);
                    searchResultBucketDto.setInvestmentProjectAppearance(searchResultBucketDto.getInvestmentProjectAppearance() + expertsForProjectSearchBucket.getInvestmentProjectAppearance());
                    searchResultBucketDto.setRank(searchResultBucketDto.getRank() + expertsForProjectSearchBucket.getRank());
                } else {
                    searchResultBucketMap.put(expertCode, expertsForProjectSearchBucket);
                }
            }
        }
        final HashMap<String, Object> params = new HashMap<>();
        params.put("resultBuckets", searchResultBucketMap.values().stream().collect(Collectors.toMap(SearchResultBucketDto::getExpertCode, SearchResultBucketDto::getRank)));
        final Script script = new Script(ScriptType.INLINE, "painless", "if (params.resultBuckets.containsKey(doc['id'].value)) {return params.resultBuckets[doc['id'].value];} return 0;", params);
        final List<FunctionScoreQueryBuilder.FilterFunctionBuilder> filterFunctionBuilders = new ArrayList<>();
        filterFunctionBuilders.add(new FunctionScoreQueryBuilder.FilterFunctionBuilder(new ScriptScoreFunctionBuilder(script)));
        final FunctionScoreQueryBuilder.FilterFunctionBuilder[] filterFunctionBuildersArray = filterFunctionBuilders.toArray(new FunctionScoreQueryBuilder.FilterFunctionBuilder[0]);
        boolQueryBuilder.should(new FunctionScoreQueryBuilder(new MatchAllQueryBuilder(), filterFunctionBuildersArray));
        return searchResultBucketMap;
    }

    @Override
    public EvidenceExpertWrapper findAnalyticsExpertsWithRecommendation(EvidenceFilter filter, OrganizationDetailDto organizationDetailDto) throws IOException {
        final EvidenceExpertWrapper evidenceExpertWrapper = new EvidenceExpertWrapper();
        final BoolQueryBuilder boolQueryBuilder = setupFilters(filter);
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        RecommendationWrapperDto recommendationWrapperDto = evidenceExpertWrapper.getRecommendationWrapperDto();
        String[] recombeeIds;
        try {
            recombeeIds = recombeeRecommendationService.recommend(organizationDetailDto, RecombeeFilterConverterUtil.convert(filter), RecombeeScenario.EXPERT, recommendationWrapperDto);
        } catch (RuntimeException e ){
            System.out.println("Recommendation with Recombee failed - using Elastic instead");
            return findAnalyticsExperts(filter);
        }
        Long docCount = (long) recombeeRecommendationService.numberOfRecomms(recommendationWrapperDto.getRecommId());
        boolQueryBuilder.filter(new TermsQueryBuilder("_id", recombeeIds));
        searchSourceBuilder.query(boolQueryBuilder).size(filter.getLimit());
        sourceBuilderToEvidenceWrapper(searchSourceBuilder, filter, evidenceExpertWrapper);
        List<EvidenceExpertPreviewDto> sorted = RecombeeFilterConverterUtil.sortByRecombee(evidenceExpertWrapper.getAnalyticsExpertPreviewDtos(),
                Arrays.stream(recombeeIds).collect(Collectors.toList()),
                evidenceExpertWrapper.getAnalyticsExpertPreviewDtos().stream().map(EvidenceExpertPreviewDto::getExpertCode).collect(Collectors.toList()));
        evidenceExpertWrapper.setAnalyticsExpertPreviewDtos(sorted);
        evidenceExpertWrapper.setNumberOfAllItems(docCount);
        return evidenceExpertWrapper;
    }

    private void sourceBuilderToEvidenceWrapper(SearchSourceBuilder searchSourceBuilder, EvidenceFilter filter,
                                                EvidenceExpertWrapper evidenceExpertWrapper) throws IOException {

        final SearchResponseEntityWrapper<Expert> hits = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder, Expert.class);
        final Set<Long> organizationToLoadIds = new HashSet<>();
        final Set<String> resultExpertsCodes = new HashSet<>();
        final Long organizationId = filter.getOrganizationId();
        final Set<Long> organizationIds = filter.getOrganizationIds();
        final ClaimProfileService claimProfileService = ServiceRegistryUtil.getService(ClaimProfileService.class);
        final Set<Long> affiliatedOrganizationIds = filter.getAffiliatedOrganizationIds();
        hits.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource)
                .map(Expert::getOrganizationIds)
                .forEach(organizationToLoadIds::addAll);
        final Map<Long, OrganizationBaseDto> baseOrganizationsMap = organizationSearchService.findBaseOrganizationsMap(organizationToLoadIds);

        final Set<Long> userIds = hits.getSearchResponseEntities().stream().map(SearchResponseEntity::getSource)
                .map(Expert::getUserId).filter(Objects::nonNull).collect(Collectors.toSet());
        final Set<Long> expertIds = hits.getSearchResponseEntities().stream().map(SearchResponseEntity::getSource)
                .map(Expert::getExpertIds).flatMap(Set::stream).filter(Objects::nonNull).collect(Collectors.toSet());
        final List<PreClaimDto> preClaimsForUsersExperts = claimProfileService.findPreClaimsForUsersExperts(userIds, expertIds);

        for (SearchResponseEntity<Expert> searchResponseEntity : hits.getSearchResponseEntities()) {
            final Expert source = searchResponseEntity.getSource();

            final EvidenceExpertPreviewWithItemsDto evidenceExpertPreviewWithItemsDto = new EvidenceExpertPreviewWithItemsDto();
            final String expertCode = source.getExpertCode();
            final Set<Long> verifiedOrganizationIds = source.getVerifiedOrganizationIds();
            final Set<Long> retiredOrganizationIds = source.getRetiredOrganizationIds();
            evidenceExpertPreviewWithItemsDto.setExpertCode(expertCode);
            evidenceExpertPreviewWithItemsDto.setName(source.getFullName());
            if (source.getUserId() != null) {
                evidenceExpertPreviewWithItemsDto.setUserId(source.getUserId());
                preClaimsForUsersExperts.stream()
                        .filter(x -> evidenceExpertPreviewWithItemsDto.getUserId().equals(x.getUserId()))
                        .findFirst()
                        .ifPresent(x -> evidenceExpertPreviewWithItemsDto.setInvitationEmail(x.getUserEmail()));
            } else {
                source.getExpertIds().stream().findFirst().ifPresent(evidenceExpertPreviewWithItemsDto::setExpertId);
                preClaimsForUsersExperts.stream()
                        .filter(x -> evidenceExpertPreviewWithItemsDto.getExpertId().equals(x.getExpertToClaimId()))
                        .findFirst()
                        .ifPresent(x -> evidenceExpertPreviewWithItemsDto.setInvitationEmail(x.getUserEmail()));
            }
            final List<OrganizationBaseDto> organizations = OrganizationUtil.findAndSortOrganizationBases(baseOrganizationsMap, source.getOrganizationIds(), organizationId, organizationIds);
            organizations.removeAll(Collections.singleton(null));
            final List<OrganizationBaseDto> visibleOrganizations = OrganizationUtil.findAndSortOrganizationBases(baseOrganizationsMap, source.getVisibleOrganizationIds(), organizationId, organizationIds);
            organizations.removeAll(Collections.singleton(null));
            final List<OrganizationBaseDto> activeOrganizations = OrganizationUtil.findAndSortOrganizationBases(baseOrganizationsMap, source.getActiveOrganizationIds(), organizationId, organizationIds);
            organizations.removeAll(Collections.singleton(null));
            evidenceExpertPreviewWithItemsDto.setOrganizationBaseDtos(organizations);
            evidenceExpertPreviewWithItemsDto.setVisibleOrganizationBaseDtos(visibleOrganizations);
            evidenceExpertPreviewWithItemsDto.setActiveOrganizationBaseDtos(activeOrganizations);
            evidenceExpertPreviewWithItemsDto.setVerified(verifiedOrganizationIds.stream().anyMatch(x -> x.equals(organizationId)));
            evidenceExpertPreviewWithItemsDto.setUnitVerified(verifiedOrganizationIds.stream().anyMatch(x -> organizationIds.stream().anyMatch(x::equals)));
            evidenceExpertPreviewWithItemsDto.setRetired(source.getRetirementDate() != null);
            evidenceExpertPreviewWithItemsDto.setRetiredInOrganization(retiredOrganizationIds.stream().anyMatch(x -> x.equals(organizationId)));
            evidenceExpertPreviewWithItemsDto.setClaimable(source.isClaimable());
            evidenceExpertPreviewWithItemsDto.setKeywords(source.getKeywords());
            evidenceExpertWrapper.getAnalyticsExpertPreviewDtos().add(evidenceExpertPreviewWithItemsDto);
            resultExpertsCodes.add(expertCode);
        }
        final Set<Long> statisticsOrganizationIds = new HashSet<>();
        statisticsOrganizationIds.addAll(organizationIds);
        statisticsOrganizationIds.addAll(affiliatedOrganizationIds);
        final List<ItemSearchService.ExpertItemStatistics> itemGroupCountsForExperts = itemSearchService.findItemStatisticsForExperts(resultExpertsCodes, filter.isOrganizationRelatedStatistics() ? statisticsOrganizationIds : Collections.emptySet());
        for (EvidenceExpertPreviewDto expertPreviewDto : evidenceExpertWrapper.getAnalyticsExpertPreviewDtos()) {
            for (ItemSearchService.ExpertItemStatistics itemGroupCountsForExpert : itemGroupCountsForExperts) {
                final String expertCode = expertPreviewDto.getExpertCode();
                if (itemGroupCountsForExpert.getExpertCode().equals(expertCode)) {
                    expertPreviewDto.setNumberOfItems(itemGroupCountsForExpert.getNumberOfItems());
                    expertPreviewDto.setItemTypeGroupCounts(itemGroupCountsForExpert.getItemTypeGroupsWithCounts());
                    expertPreviewDto.setYearActivityHistogram(itemGroupCountsForExpert.getYearActivityCounts());
                }
            }

            expertPreviewDto.setNumberOfProjects(projectSearchService.findExpertProjectStatistics(expertPreviewDto.getExpertCode(), filter.isOrganizationRelatedStatistics() ? statisticsOrganizationIds : Collections.emptySet(), true).getNumberOfInvestmentProjects());
            expertPreviewDto.setNumberOfCommercializationProjects(commercializationProjectSearchService.findExpertInvestmentProjectStatistics(expertPreviewDto.getExpertCode(), filter.isOrganizationRelatedStatistics() ? statisticsOrganizationIds : Collections.emptySet()).getNumberOfInvestmentProjects());
        }

        evidenceExpertWrapper.setLimit(filter.getLimit());
        evidenceExpertWrapper.setPage(filter.getPage());
        evidenceExpertWrapper.setNumberOfAllItems(hits.getResultCount());
        if (organizationId != null) {
            evidenceExpertWrapper.setOrganizationId(organizationId);
            evidenceExpertWrapper.setOrganizationUnitIds(organizationIds.stream()
                    .filter(id -> !organizationId.equals(id))
                    .collect(Collectors.toSet()));
        }
    }

    @Override
    public Set<String> findExpertNamesByCodes(Set<String> expertCodes) throws IOException {
        expertCodes.remove(null);
        if (expertCodes.isEmpty())
            return new HashSet<>();
        final BoolQueryBuilder query = new BoolQueryBuilder();
        query.must(new TermsQueryBuilder("_id", expertCodes));
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder()
                .query(query)
                .size(expertCodes.size() * 2);
        final SearchResponseEntityWrapper<Expert> hits = elasticSearchDao.doSearchAndGetHits(indexName, sourceBuilder, Expert.class);
        return hits.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource)
                .map(Expert::getFullName)
                .collect(Collectors.toSet());
    }
}
