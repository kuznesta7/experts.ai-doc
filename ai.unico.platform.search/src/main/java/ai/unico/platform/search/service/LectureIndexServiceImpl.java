package ai.unico.platform.search.service;

import ai.unico.platform.extdata.dto.ExpertLectureDto;
import ai.unico.platform.extdata.service.DWHLectureService;
import ai.unico.platform.search.api.service.LectureIndexService;
import ai.unico.platform.search.api.service.SearchMappingsLoaderService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Lecture;
import ai.unico.platform.search.util.ExpertUtil;
import ai.unico.platform.search.util.LectureUtil;
import ai.unico.platform.store.api.dto.ItemIndexDto;
import ai.unico.platform.store.api.enums.IndexHistoryStatus;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.api.service.IndexHistoryService;
import ai.unico.platform.store.api.service.OrganizationService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class LectureIndexServiceImpl implements LectureIndexService {

    private boolean indexingInProgress = false;

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private SearchMappingsLoaderService searchMappingsLoaderService;

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;

    @Override
    public void reindexLectures(Integer limit, boolean clean, String target, UserContext userContext) throws IOException {
        final String mappingJson = searchMappingsLoaderService.loadMappings("lectures");
        if (mappingJson == null) throw new FileNotFoundException();

        if (indexingInProgress) return;

        if (clean) {
            try {
                elasticSearchDao.deleteIndex(indexName);
            } catch (Exception ignore) {
            }
            elasticSearchDao.initIndex(indexName, mappingJson);
            System.out.println("creating index " + indexName + " with mappings:");
            System.out.println(mappingJson);
        }

        indexingInProgress = true;
        final IndexHistoryService indexHistoryService = ServiceRegistryUtil.getService(IndexHistoryService.class);
        final Long indexHistoryId = indexHistoryService.saveIndexHistory(IndexTarget.LECTURES, clean, userContext);


        try {
            if (target == null || "ALL".equals(target) || "DWH".equals(target)) {
                final DWHLectureService dwhLectureService = ServiceRegistryUtil.getService(DWHLectureService.class);
                List<ExpertLectureDto> itemDtos;
                Integer offset = 0;
                System.out.println("Starting DWH lectures index");

                final OrganizationService organizationService = ServiceRegistryUtil.getService(OrganizationService.class);
                do {
                    itemDtos = dwhLectureService.findItemPreviews(limit, offset);
                    int size = itemDtos.size();
                    if (size == 0) {
                        break;
                    }
                    if (size % 10 !=0)
                        size += 10; //index last items when size%10 != 0
                    final Set<String> expertCodes = new HashSet<>();
                    itemDtos.stream()
                            .map(ExpertLectureDto::getExpertIds)
                            .forEach(x -> x.stream().map(ExpertUtil::convertToExpertCode).forEach(expertCodes::add));
                    final Map<String, String> substituteExpertCodes = ExpertUtil.findSubstituteExpertCodes(expertCodes);
                    for (int i = 0; i < 10; i++) {
                        final List<Lecture> lectures = new ArrayList<>();
                        final int start = i * (size / 10);
                        final int end = start + size / 10;
                        final Set<Long> originalOrganizationIds = new HashSet<>();
                        itemDtos.stream().map(ExpertLectureDto::getOriginalOrganizationIds).forEach(originalOrganizationIds::addAll);
                        final Map<Long, Long> organizationMap = organizationService.findOrganizationIdsMap(originalOrganizationIds);
                        for (ExpertLectureDto lecturePreviewDto : itemDtos.subList(start, Math.min(end,itemDtos.size()))) {

                            final Lecture lecture = LectureUtil.convert(lecturePreviewDto);
                            for (Long originalOrganizationId : lecturePreviewDto.getOriginalOrganizationIds()) {
                                final Long organizationId = organizationMap.get(originalOrganizationId);
                                if (organizationId != null) lecture.getOrganizationIds().add(organizationId);
                            }
                            if (lecturePreviewDto.getOriginalOrganizationIds().size() == 1) {
                                final Long ownerId = lecturePreviewDto.getOriginalOrganizationIds().iterator().next();
                                lecture.setOwnerOrganizationId(organizationMap.getOrDefault(ownerId, ownerId));
                            }
                            lecture.getEvidenceOrganizationIds().addAll(lecture.getOrganizationIds());
                            doReplaceExpertCodes(lecture, substituteExpertCodes);
                            lectures.add(lecture);
                        }
                        elasticSearchDao.doIndex(indexName, lectures);
                    }
                    offset += limit;
                    System.out.println(offset);
                } while (itemDtos.size() > 0);

                System.out.println("DWH index done.");
            }
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FINISHED, userContext);
        } catch (Exception e) {
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FAILED, e.getCause().toString(), userContext);
            e.printStackTrace();
        }
        indexingInProgress = false;
    }

    private void doReplaceExpertCodes(Lecture lecture, Map<String, String> replacementMap) {
        ExpertUtil.replaceExpertCodes(lecture.getExpertCodes(), replacementMap);
        ExpertUtil.replaceExpertCodes(lecture.getAllExpertCodes(), replacementMap);
    }


    @Override
    public void indexLecture(ItemIndexDto itemIndexDto) throws IOException {
        throw new NotImplementedException();
    }

    @Override
    public void replaceExperts(Map<String, String> replacementMap, boolean originalOnly) throws IOException {
        throw new NotImplementedException();
    }


}
