package ai.unico.platform.search.service;

import ai.unico.platform.search.api.dto.ItemSearchPreviewDto;
import ai.unico.platform.search.api.dto.SearchResultBucketDto;
import ai.unico.platform.search.api.filter.ExpertSearchFilter;
import ai.unico.platform.search.api.filter.ExpertSearchResultFilter;
import ai.unico.platform.search.api.service.ItemSearchService;
import ai.unico.platform.search.api.service.OntologySearchService;
import ai.unico.platform.search.api.wrapper.ExpertItemWrapper;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.util.*;
import ai.unico.platform.store.api.dto.ItemTypeGroupDto;
import ai.unico.platform.store.api.service.ItemTypeService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Confidentiality;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.BucketOrder;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.histogram.HistogramAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.IncludeExclude;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import org.elasticsearch.search.aggregations.metrics.sum.SumAggregationBuilder;
import org.elasticsearch.search.aggregations.support.ValueType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.*;

public class ItemSearchServiceImpl implements ItemSearchService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Autowired
    private OntologySearchService ontologySearchService;

    @Value("${elasticsearch.settings.maxBucketSize}")
    private Integer maxBucketSize;

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;


    @Override
    public List<SearchResultBucketDto> findExpertsForItemSearch(ExpertSearchFilter expertSearchFilter) throws IOException {
        final List<? extends Terms.Bucket> buckets = findForItemSearch(expertSearchFilter, ValueType.STRING, "experts", "expertCodes");
        final List<SearchResultBucketDto> searchResultBucketDtos = new ArrayList<>();
        for (Terms.Bucket bucket : buckets) {
            final SearchResultBucketDto searchResultBucketDto = new SearchResultBucketDto();
            final Sum score = bucket.getAggregations().get("score");
            searchResultBucketDto.setExpertCode((String) bucket.getKey());
            searchResultBucketDto.setAppearance(bucket.getDocCount());
            searchResultBucketDto.setRank(score.getValue());
            searchResultBucketDtos.add(searchResultBucketDto);
        }
        return searchResultBucketDtos;
    }

    @Override
    public List<SearchResultBucketDto> findOrganizationsForItemSearch(ExpertSearchFilter expertSearchFilter) throws IOException {
        final List<? extends Terms.Bucket> buckets = findForItemSearch(expertSearchFilter, ValueType.LONG, "organization", "organizationIds");
        final List<SearchResultBucketDto> searchResultBucketDtos = new ArrayList<>();
        for (Terms.Bucket bucket : buckets) {
            final SearchResultBucketDto searchResultBucketDto = new SearchResultBucketDto();
            final Sum score = bucket.getAggregations().get("score");
            searchResultBucketDto.setOrganizationId((Long) bucket.getKey());
            searchResultBucketDto.setAppearance(bucket.getDocCount());
            searchResultBucketDto.setRank(score.getValue());
            searchResultBucketDtos.add(searchResultBucketDto);
        }
        return searchResultBucketDtos;
    }

    @Override
    public ExpertItemWrapper findExpertItems(String expertCode, ExpertSearchResultFilter expertSearchResultFilter) throws IOException {
        SearchQueryTranslationUtil.updateFilter(expertSearchResultFilter);
        ontologySearchService.fillItemSearchOntology(expertSearchResultFilter);
        final String query = expertSearchResultFilter.getQuery();
        final FunctionScoreQueryBuilder functionScoreQueryBuilder = ItemSearchUtil.prepareFindByExpertiseScoreQueryBuilder(expertSearchResultFilter, query == null || query.isEmpty(), expertCode);
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final Integer limit = expertSearchResultFilter.getItemsLimit();
        final Integer page = expertSearchResultFilter.getItemsPage();
        final Map<String, Object> params = new HashMap<>();

        params.put("minScore", ItemSearchService.minScore);
        final Script script = new Script(ScriptType.INLINE, "painless", "_score > params.minScore ? _score : 0;", params);

        if (query != null && !query.isEmpty()) {
            final HighlightBuilder highlightBuilder = new HighlightBuilder().numOfFragments(0).highlightQuery(
                    QueryBuilders.multiMatchQuery(expertSearchResultFilter.getQueryForSearch(), "itemName", "itemDescription", "keywords"))
                    .field("itemName")
                    .field("itemDescription")
                    .field("keywords");
            searchSourceBuilder.highlighter(highlightBuilder);
        }
        searchSourceBuilder.query(functionScoreQueryBuilder)
                .size(limit)
                .from((page - 1) * limit)
                .aggregation(new SumAggregationBuilder("scoreSum").script(script));
        ItemUtil.addItemSortFunction(searchSourceBuilder, expertSearchResultFilter.getOrderBy(), expertCode);

        final SearchResponse searchResponse = elasticSearchDao.doSearch(indexName, searchSourceBuilder);
        final SearchHits searchHits = searchResponse.getHits();
        final ExpertItemWrapper expertItemWrapper = new ExpertItemWrapper();
        final Sum scoreSum = searchResponse.getAggregations().get("scoreSum");
        expertItemWrapper.setPage(page);
        expertItemWrapper.setLimit(limit);
        expertItemWrapper.setNumberOfRelevantItems(searchHits.totalHits);
        if (scoreSum != null) {
            expertItemWrapper.setScoreSum(scoreSum.getValue());
        }
        for (SearchHit searchHit : searchHits) {
            final Map<String, Object> sourceAsMap = searchHit.getSourceAsMap();
            final float score = searchHit.getScore();
            final Double rank = SearchRankUtil.recalculateRank((double) score);
            final ItemSearchPreviewDto itemSearchPreviewDto = new ItemSearchPreviewDto();
            itemSearchPreviewDto.setItemCode(searchHit.getId());
            itemSearchPreviewDto.setItemRank(rank);
            itemSearchPreviewDto.setItemName(ElasticSearchUtil.getHighlightedText(searchHit, "itemName"));
            itemSearchPreviewDto.setItemDescription(ElasticSearchUtil.getHighlightedText(searchHit, "itemDescription"));
            itemSearchPreviewDto.setItemId(TypeConversionUtil.getLongValue(sourceAsMap.get("itemId")));
            itemSearchPreviewDto.setOriginalItemId(TypeConversionUtil.getLongValue(sourceAsMap.get("originalItemId")));
            itemSearchPreviewDto.setItemTypeId(TypeConversionUtil.getLongValue(sourceAsMap.get("itemTypeId")));
            itemSearchPreviewDto.setYear(TypeConversionUtil.getIntegerValue(sourceAsMap.get("year")));
            itemSearchPreviewDto.setKeywords(ElasticSearchUtil.getHighlightedTexts(searchHit, "keywords"));
            itemSearchPreviewDto.setTranslationUnavailable((Boolean) sourceAsMap.get("translationUnavailable"));

            final Set<String> favoriteExpertCodes = new HashSet<>((ArrayList<String>) sourceAsMap.get("favoriteExpertCodes"));
            final Set<String> expertCodes = new HashSet<>((ArrayList<String>) sourceAsMap.get("expertCodes"));

            itemSearchPreviewDto.setFavorite(favoriteExpertCodes.contains(expertCode));
            itemSearchPreviewDto.setHidden(!expertCodes.contains(expertCode));

            expertItemWrapper.getItemSearchPreviewDtos().add(itemSearchPreviewDto);
        }
        return expertItemWrapper;
    }

    @Override
    public List<ExpertItemStatistics> findItemStatisticsForExperts(Set<String> expertCodes) throws IOException {
        return findItemStatisticsForExperts(expertCodes, Collections.EMPTY_SET);
    }

    @Override
    public List<ExpertItemStatistics> findItemStatisticsForExperts(Set<String> expertCodes, Set<Long> organizationIds) throws IOException {
        final List<ExpertItemStatistics> result = new ArrayList<>();
        final TermsQueryBuilder termsQueryBuilder = new TermsQueryBuilder("expertCodes", expertCodes);
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        if (!organizationIds.isEmpty()) {
            boolQueryBuilder.filter(new TermsQueryBuilder("organizationIds", organizationIds));
        }
        searchSourceBuilder.size(0)
                .query(boolQueryBuilder
                        .filter(termsQueryBuilder)
                        .filter(new TermQueryBuilder("confidentiality", Confidentiality.PUBLIC)))
                .aggregation(new TermsAggregationBuilder("experts", ValueType.STRING)
                        .field("expertCodes")
                        .size(maxResultWindow)
                        .includeExclude(new IncludeExclude(expertCodes.toArray(new String[0]), null))
                        .subAggregation(new TermsAggregationBuilder("itemTypes", ValueType.DOUBLE)
                                .size(maxResultWindow)
                                .field("itemTypeId"))
                        .subAggregation(new HistogramAggregationBuilder("activityHistogram")
                                .interval(1D)
                                .field("year")));
        final SearchResponse searchResponse = elasticSearchDao.doSearch(indexName, searchSourceBuilder);
        final Terms terms = searchResponse.getAggregations().get("experts");
        final List<? extends Terms.Bucket> buckets = terms.getBuckets();

        final ItemTypeService itemTypeService = ServiceRegistryUtil.getService(ItemTypeService.class);
        final List<ItemTypeGroupDto> itemTypeGroups = itemTypeService.findItemTypeGroups();
        final Map<Long, Long> typeToGroupMap = new HashMap<>();
        itemTypeGroups.forEach(x ->
                x.getItemTypeIds().forEach(y ->
                        typeToGroupMap.put(y, x.getItemTypeGroupId())));

        for (Terms.Bucket bucket : buckets) {
            final ExpertItemStatistics expertGroups = new ExpertItemStatistics();
            expertGroups.setExpertCode(bucket.getKeyAsString());
            final long numberOfItems = bucket.getDocCount();
            expertGroups.setNumberOfItems(numberOfItems);
            final Map<Long, Long> groupsWithCounts = expertGroups.getItemTypeGroupsWithCounts();
            for (ItemTypeGroupDto itemTypeGroup : itemTypeGroups) {
                groupsWithCounts.put(itemTypeGroup.getItemTypeGroupId(), 0L);
            }
            final long otherItemTypeGroup = 4L;
            groupsWithCounts.put(otherItemTypeGroup, numberOfItems);
            Terms groupTerms = bucket.getAggregations().get("itemTypes");
            for (Terms.Bucket groupTerm : groupTerms.getBuckets()) {
                final Long typeId = TypeConversionUtil.getLongValue(groupTerm.getKeyAsNumber());
                final Long groupId = typeToGroupMap.get(typeId);
                if (groupId == null || groupId.equals(otherItemTypeGroup)) continue;
                final Long docCount = groupTerm.getDocCount();
                groupsWithCounts.put(groupId, groupsWithCounts.get(groupId) + docCount);
                groupsWithCounts.put(otherItemTypeGroup, groupsWithCounts.get(otherItemTypeGroup) - docCount);
            }
            Histogram histogram = bucket.getAggregations().get("activityHistogram");
            for (Histogram.Bucket histogramBucket : histogram.getBuckets()) {
                final Integer year = TypeConversionUtil.getIntegerValue(histogramBucket.getKey());
                final long count = histogramBucket.getDocCount();
                expertGroups.getYearActivityCounts().put(year, count);
            }
            result.add(expertGroups);
        }
        return result;
    }

    @Override
    public ExpertItemStatistics findItemStatisticsForExpert(String expertCode) throws IOException {
        final List<ExpertItemStatistics> itemStatisticsForExperts = findItemStatisticsForExperts(Collections.singleton(expertCode));
        final ExpertItemStatistics other = new ExpertItemStatistics();
        other.setExpertCode(expertCode);
        return itemStatisticsForExperts.stream().findFirst().orElse(other);
    }

    private List<? extends Terms.Bucket> findForItemSearch(ExpertSearchFilter expertSearchFilter, ValueType valueType, String aggregationName, String fieldName) throws IOException {
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder()
                .query(ItemSearchUtil.prepareFindByExpertiseScoreQueryBuilder(expertSearchFilter, false, null))
                .minScore(minScore)
                .aggregation(
                        new TermsAggregationBuilder(aggregationName, valueType)
                                .field(fieldName)
                                .order(BucketOrder.aggregation("score", false))
                                .size(maxBucketSize)
                                .subAggregation(new SumAggregationBuilder("score").script(new Script("_score"))));
        final SearchResponse searchResponse = elasticSearchDao.doSearch(indexName, sourceBuilder);
        final Terms terms = searchResponse.getAggregations().get(aggregationName);
        return terms.getBuckets();
    }
}
