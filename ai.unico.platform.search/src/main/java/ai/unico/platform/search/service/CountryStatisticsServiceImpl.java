package ai.unico.platform.search.service;

import ai.unico.platform.search.api.dto.CountryStatisticsDto;
import ai.unico.platform.search.api.service.CountryStatisticsService;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.util.enums.Role;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class CountryStatisticsServiceImpl implements CountryStatisticsService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Override
    public CountryStatisticsDto getCountryStatistics(String countryCode) throws IOException {
        CountryStatisticsDto countryStatisticsDto = new CountryStatisticsDto();
        countryStatisticsDto.setCountryCode(countryCode);
        countryStatisticsDto.setAllExperts(getAllCountryExperts(countryCode));
        countryStatisticsDto.setActiveExperts(getActiveCountryExperts(countryCode));
        countryStatisticsDto.setVerifiedExperts(getVerifiedCountryExperts(countryCode));
        countryStatisticsDto.setRetiredExperts(getRetiredCountryExperts(countryCode));
        countryStatisticsDto.setRoleCount(getActiveByRole(countryCode));

        return countryStatisticsDto;
    }

    private Long getAllCountryExperts(String countryCode) throws IOException {
        final SearchSourceBuilder allSearchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        if (countryCode != null) {
            boolQueryBuilder.filter(new MatchQueryBuilder("countryCodes", countryCode));
        }
        boolQueryBuilder.must(new MatchAllQueryBuilder());
        allSearchSourceBuilder.query(boolQueryBuilder).size(0);
        final SearchResponse allSearchResponse = elasticSearchDao.doSearch(indexName, allSearchSourceBuilder);
        return allSearchResponse.getHits().totalHits;
    }

    private Long getActiveCountryExperts(String countryCode) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        if (countryCode != null) {
            boolQueryBuilder.filter(new TermQueryBuilder("countryCodes", countryCode));
        }
        boolQueryBuilder.must(new ExistsQueryBuilder("userId")).must(new TermQueryBuilder("claimable", false));
        searchSourceBuilder.query(boolQueryBuilder).size(0);
        final SearchResponse allSearchResponse = elasticSearchDao.doSearch(indexName, searchSourceBuilder);
        return allSearchResponse.getHits().totalHits;
    }

    private Long getVerifiedCountryExperts(String countryCode) throws IOException {
        final SearchSourceBuilder allSearchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        if (countryCode != null) {
            boolQueryBuilder.filter(new TermQueryBuilder("countryCodes", countryCode));
        }
        boolQueryBuilder.must(new ExistsQueryBuilder("verifiedOrganizationIds"));
        allSearchSourceBuilder.query(boolQueryBuilder).size(0);
        final SearchResponse allSearchResponse = elasticSearchDao.doSearch(indexName, allSearchSourceBuilder);
        return allSearchResponse.getHits().totalHits;
    }

    private Long getRetiredCountryExperts(String countryCode) throws IOException {
        final SearchSourceBuilder allSearchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        if (countryCode != null) {
            boolQueryBuilder.filter(new TermQueryBuilder("countryCodes", countryCode));
        }
        boolQueryBuilder.must(new ExistsQueryBuilder("retirementDate"));
        allSearchSourceBuilder.query(boolQueryBuilder).size(0);
        final SearchResponse allSearchResponse = elasticSearchDao.doSearch(indexName, allSearchSourceBuilder);
        return allSearchResponse.getHits().totalHits;
    }

    private Map<String, Long> getActiveByRole(String countryCode) throws IOException {
        Map<String, Long> activeRoles = new HashMap<>();
        for (Role role : Role.values()) {
            if (!role.isOpen()) continue;
            final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
            if (countryCode != null) {
                boolQueryBuilder.filter(new TermQueryBuilder("countryCodes", countryCode));
            }
            boolQueryBuilder.must(new ExistsQueryBuilder("userId"))
                    .must(new TermQueryBuilder("claimable", false))
                    .must(new TermQueryBuilder("roles", role.toString()));
            searchSourceBuilder.query(boolQueryBuilder).size(0);
            final SearchResponse allSearchResponse = elasticSearchDao.doSearch(indexName, searchSourceBuilder);
            activeRoles.put(role.toString(), allSearchResponse.getHits().totalHits);
        }
        return activeRoles;
    }
}
