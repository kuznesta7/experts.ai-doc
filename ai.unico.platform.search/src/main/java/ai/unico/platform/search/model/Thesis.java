package ai.unico.platform.search.model;

import ai.unico.platform.util.enums.Confidentiality;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Thesis extends ElasticSearchModel {

    private Long thesisId;

    private Long originalThesisId;

    private String thesisBk;

    private String thesisName;

    private String thesisDescription;

    private Long thesisTypeId;

    private Integer year;

    private Confidentiality confidentiality;

    private String assignee;

    private String supervisor;

    private String reviewer;

    private Long universityId;

    //todo decide if i need so many organizations
    private Boolean translationUnavailable;

    private List<String> keywords = new ArrayList<>();

    private Set<Long> organizationIds = new HashSet<>();

    private Set<Long> verifiedOrganizationIds = new HashSet<>();

    private Set<Long> evidenceOrganizationIds = new HashSet<>();

    public Long getThesisId() {
        return thesisId;
    }

    public void setThesisId(Long thesisId) {
        this.thesisId = thesisId;
    }

    public Long getOriginalThesisId() {
        return originalThesisId;
    }

    public void setOriginalThesisId(Long originalThesisId) {
        this.originalThesisId = originalThesisId;
    }

    public String getThesisBk() {
        return thesisBk;
    }

    public void setThesisBk(String thesisBk) {
        this.thesisBk = thesisBk;
    }

    public String getThesisName() {
        return thesisName;
    }

    public void setThesisName(String thesisName) {
        this.thesisName = thesisName;
    }

    public String getThesisDescription() {
        return thesisDescription;
    }

    public void setThesisDescription(String thesisDescription) {
        this.thesisDescription = thesisDescription;
    }

    public Long getThesisTypeId() {
        return thesisTypeId;
    }

    public void setThesisTypeId(Long thesisTypeId) {
        this.thesisTypeId = thesisTypeId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Confidentiality getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(Confidentiality confidentiality) {
        this.confidentiality = confidentiality;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public Long getUniversityId() {
        return universityId;
    }

    public void setUniversityId(Long universityId) {
        this.universityId = universityId;
    }

    public Boolean getTranslationUnavailable() {
        return translationUnavailable;
    }

    public void setTranslationUnavailable(Boolean translationUnavailable) {
        this.translationUnavailable = translationUnavailable;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<Long> getVerifiedOrganizationIds() {
        return verifiedOrganizationIds;
    }

    public void setVerifiedOrganizationIds(Set<Long> verifiedOrganizationIds) {
        this.verifiedOrganizationIds = verifiedOrganizationIds;
    }

    public Set<Long> getEvidenceOrganizationIds() {
        return evidenceOrganizationIds;
    }

    public void setEvidenceOrganizationIds(Set<Long> evidenceOrganizationIds) {
        this.evidenceOrganizationIds = evidenceOrganizationIds;
    }
}
