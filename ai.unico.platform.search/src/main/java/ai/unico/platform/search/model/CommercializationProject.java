package ai.unico.platform.search.model;

import ai.unico.platform.store.api.enums.CommercializationOrganizationRelation;

import java.util.*;

public class CommercializationProject extends ElasticSearchModel {

    private Long commercializationId;

    private String commercializationProjectImgCheckSum;

    private String name;

    private Integer ideaScore;

    private Integer technologyReadinessLevel;

    private String executiveSummary;

    private String useCaseDescription;

    private String painDescription;

    private String competitiveAdvantageDescription;

    private String technicalPrinciplesDescription;

    private Set<String> originalExpertCodes = new HashSet<>();

    private Set<String> expertCodes = new HashSet<>();

    private Set<String> teamLeaderExpertCodes = new HashSet<>();

    private Long domainId;

    private Integer investmentRangeFrom;

    private Integer investmentRangeTo;

    private Long statusId;

    private Set<Long> categoryIds;

    private List<String> keywords;

    private Date startDate;

    private Date endDate;

    private Long priorityId;

    private Long ownerOrganizationId;

    private String link;

    private List<CommercializationProjectOrganization> commercializationProjectOrganizations = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdeaScore() {
        return ideaScore;
    }

    public void setIdeaScore(Integer ideaScore) {
        this.ideaScore = ideaScore;
    }

    public Integer getTechnologyReadinessLevel() {
        return technologyReadinessLevel;
    }

    public void setTechnologyReadinessLevel(Integer technologyReadinessLevel) {
        this.technologyReadinessLevel = technologyReadinessLevel;
    }

    public String getExecutiveSummary() {
        return executiveSummary;
    }

    public void setExecutiveSummary(String executiveSummary) {
        this.executiveSummary = executiveSummary;
    }

    public String getUseCaseDescription() {
        return useCaseDescription;
    }

    public void setUseCaseDescription(String useCaseDescription) {
        this.useCaseDescription = useCaseDescription;
    }

    public String getPainDescription() {
        return painDescription;
    }

    public void setPainDescription(String painDescription) {
        this.painDescription = painDescription;
    }

    public String getCompetitiveAdvantageDescription() {
        return competitiveAdvantageDescription;
    }

    public void setCompetitiveAdvantageDescription(String competitiveAdvantageDescription) {
        this.competitiveAdvantageDescription = competitiveAdvantageDescription;
    }

    public String getTechnicalPrinciplesDescription() {
        return technicalPrinciplesDescription;
    }

    public void setTechnicalPrinciplesDescription(String technicalPrinciplesDescription) {
        this.technicalPrinciplesDescription = technicalPrinciplesDescription;
    }

    public Set<String> getExpertCodes() {
        return expertCodes;
    }

    public void setExpertCodes(Set<String> expertCodes) {
        this.expertCodes = expertCodes;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Long getDomainId() {
        return domainId;
    }

    public void setDomainId(Long domainId) {
        this.domainId = domainId;
    }

    public Set<String> getTeamLeaderExpertCodes() {
        return teamLeaderExpertCodes;
    }

    public void setTeamLeaderExpertCodes(Set<String> teamLeaderExpertCodes) {
        this.teamLeaderExpertCodes = teamLeaderExpertCodes;
    }

    public Integer getInvestmentRangeFrom() {
        return investmentRangeFrom;
    }

    public void setInvestmentRangeFrom(Integer investmentRangeFrom) {
        this.investmentRangeFrom = investmentRangeFrom;
    }

    public Integer getInvestmentRangeTo() {
        return investmentRangeTo;
    }

    public void setInvestmentRangeTo(Integer investmentRangeTo) {
        this.investmentRangeTo = investmentRangeTo;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Set<Long> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(Set<Long> categoryId) {
        this.categoryIds = categoryId;
    }

    public Long getCommercializationId() {
        return commercializationId;
    }

    public void setCommercializationId(Long commercializationId) {
        this.commercializationId = commercializationId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(Long priorityId) {
        this.priorityId = priorityId;
    }

    public String getCommercializationProjectImgCheckSum() {
        return commercializationProjectImgCheckSum;
    }

    public void setCommercializationProjectImgCheckSum(String commercializationProjectImgCheckSum) {
        this.commercializationProjectImgCheckSum = commercializationProjectImgCheckSum;
    }

    public List<CommercializationProjectOrganization> getCommercializationProjectOrganizations() {
        return commercializationProjectOrganizations;
    }

    public void setCommercializationProjectOrganizations(List<CommercializationProjectOrganization> commercializationProjectOrganizations) {
        this.commercializationProjectOrganizations = commercializationProjectOrganizations;
    }

    public Set<String> getOriginalExpertCodes() {
        return originalExpertCodes;
    }

    public void setOriginalExpertCodes(Set<String> originalExpertCodes) {
        this.originalExpertCodes = originalExpertCodes;
    }

    public Long getOwnerOrganizationId() {
        return ownerOrganizationId;
    }

    public void setOwnerOrganizationId(Long ownerOrganizationId) {
        this.ownerOrganizationId = ownerOrganizationId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public static class CommercializationProjectOrganization {
        private CommercializationOrganizationRelation relation;

        private Long organizationId;

        public CommercializationProjectOrganization() {

        }

        public CommercializationProjectOrganization(CommercializationOrganizationRelation relation, Long organizationId) {
            this.relation = relation;
            this.organizationId = organizationId;
        }

        public CommercializationOrganizationRelation getRelation() {
            return relation;
        }

        public void setRelation(CommercializationOrganizationRelation relation) {
            this.relation = relation;
        }

        public Long getOrganizationId() {
            return organizationId;
        }

        public void setOrganizationId(Long organizationId) {
            this.organizationId = organizationId;
        }
    }
}
