package ai.unico.platform.search.service;

import ai.unico.platform.search.api.filter.ExpertSearchFilter;
import ai.unico.platform.search.api.service.*;
import ai.unico.platform.search.dao.ElasticSearchDao;
import ai.unico.platform.search.model.Organization;
import ai.unico.platform.search.model.SearchResponseEntity;
import ai.unico.platform.search.model.SearchResponseEntityWrapper;
import ai.unico.platform.search.util.ExpertUtil;
import ai.unico.platform.search.util.ItemSearchUtil;
import ai.unico.platform.search.util.TypeConversionUtil;
import ai.unico.platform.store.api.enums.IndexHistoryStatus;
import ai.unico.platform.store.api.enums.IndexTarget;
import ai.unico.platform.store.api.service.IndexHistoryService;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.model.UserContext;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.ExistsQueryBuilder;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.nested.NestedAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.IncludeExclude;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import org.elasticsearch.search.aggregations.metrics.sum.SumAggregationBuilder;
import org.elasticsearch.search.aggregations.support.ValueType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ExpertEvaluationServiceImpl implements ExpertEvaluationService {

    @Autowired
    private ElasticSearchDao elasticSearchDao;

    @Value("${elasticsearch.settings.maxResultWindow}")
    private Integer maxResultWindow;

    private boolean indexingInProgress = false;

    private final Integer maxBucketSize = 50000;

    @Override
    public void evaluateExperts(Integer limit, UserContext userContext) throws IOException {
        if (indexingInProgress) return;
        final IndexHistoryService indexHistoryService = ServiceRegistryUtil.getService(IndexHistoryService.class);
        final Long indexHistoryId = indexHistoryService.saveIndexHistory(IndexTarget.EXPERT_EVALUATION, false, userContext);
        try {
            indexingInProgress = true;
            Long lastId = 0L;
            System.out.println("Starting user evaluation");
            while (true) {
                final BoolQueryBuilder query = new BoolQueryBuilder()
                        .filter(new ExistsQueryBuilder("userId"))
                        .filter(new RangeQueryBuilder("userId").gt(lastId));
                final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                        .query(query)
                        .size(limit)
                        .sort("userId", SortOrder.ASC);
                final SearchHits searchHits = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder);
                final Set<String> expertCodes = new HashSet<>();
                for (SearchHit searchHit : searchHits) {
                    final String expertCode = searchHit.getId();
                    expertCodes.add(expertCode);
                    final Long userId = ExpertUtil.getUserId(expertCode);
                    if (userId != null && lastId < userId) lastId = userId;
                }
                recalculateComputedFieldsForExperts(expertCodes);
                System.out.println(lastId);
                if (searchHits.totalHits == 0) break;
            }
            lastId = 0L;
            System.out.println("User evaluation finished; Starting expert evaluation");
            while (true) {
                final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
                final BoolQueryBuilder query = new BoolQueryBuilder();
                query.filter(new RangeQueryBuilder("expertIds")
                        .gt(lastId))
                        .mustNot(new ExistsQueryBuilder("userId"));
                final SearchHits searchHits = elasticSearchDao.doSearchAndGetHits(indexName, searchSourceBuilder
                        .query(query)
                        .size(limit)
                        .sort("expertIds", SortOrder.ASC));
                final Set<String> expertCodes = new HashSet<>();
                for (SearchHit searchHit : searchHits) {
                    final String expertCode = searchHit.getId();
                    expertCodes.add(expertCode);
                    final Long expertId = ExpertUtil.getExpertId(expertCode);
                    if (expertId != null && lastId < expertId) lastId = expertId;
                }
                recalculateComputedFieldsForExperts(expertCodes);
                System.out.println(lastId);
                if (searchHits.totalHits == 0) break;
            }
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FINISHED, userContext);
            System.out.println("Expert evaluation finished");
        } catch (Exception e) {
            e.printStackTrace();
            indexHistoryService.updateIndexHistory(indexHistoryId, IndexHistoryStatus.FAILED, userContext);
        }
        indexingInProgress = false;
    }

    @Override
    public void recalculateComputedFieldsForExperts(Set<String> expertCodes) throws IOException {
        try {
            elasticSearchDao.flush(ItemIndexService.indexName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (expertCodes.isEmpty()) return;
        elasticSearchDao.doUpdate(indexName, "expertRank", getExpertRankMap(expertCodes));
        elasticSearchDao.doUpdate(indexName, "countryCodes", getExpertCountryCodesMap(expertCodes));
        try {
            elasticSearchDao.doUpdate(indexName, "affiliatedOrganizationIds", getExpertAffiliatedOrganizationIdsMap(expertCodes));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void recalculateComputedFieldsForUser(Long userId) throws IOException {
        recalculateComputedFieldsForExperts(Collections.singleton(ExpertUtil.convertToUserCode(userId)));
    }

    private Map<String, Set<String>> getExpertCountryCodesMap(Set<String> expertCodes) throws IOException {
        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(new TermsQueryBuilder("expertCode", expertCodes))
                .aggregation(new TermsAggregationBuilder("experts", ValueType.STRING)
                        .field("expertCode")
                        .size(maxBucketSize)
                        .includeExclude(new IncludeExclude(expertCodes.toArray(new String[0]), null))
                        .subAggregation(new TermsAggregationBuilder("organizationIds", ValueType.STRING)
                                .field("organizationIds")
                                .size(maxBucketSize)));
        final SearchResponse searchResponse = elasticSearchDao.doSearch(indexName, searchSourceBuilder);
        final Terms experts = searchResponse.getAggregations().get("experts");
        final Map<String, Set<Long>> expertOrganizationIdsMap = new HashMap<>();
        final Set<Long> organizationToLoadIds = new HashSet<>();
        for (Terms.Bucket bucket : experts.getBuckets()) {
            final String expertCode = bucket.getKeyAsString();
            final Terms organizationBuckets = bucket.getAggregations().get("organizationIds");
            final Set<Long> organizationIds = organizationBuckets.getBuckets().stream()
                    .map(Terms.Bucket::getKeyAsNumber)
                    .map(TypeConversionUtil::getLongValue)
                    .collect(Collectors.toSet());
            organizationToLoadIds.addAll(organizationIds);
            expertOrganizationIdsMap.put(expertCode, organizationIds);
        }

        final SearchSourceBuilder organizationSearchSourceBuilder = new SearchSourceBuilder()
                .query(new BoolQueryBuilder()
                        .filter(new TermsQueryBuilder("organizationId", organizationToLoadIds))
                        .filter(new ExistsQueryBuilder("countryCode")))
                .size(maxResultWindow)
                .fetchSource(new String[]{"organizationId", "countryCode"}, new String[]{});
        final SearchResponseEntityWrapper<Organization> organizationSearchResponseEntityWrapper = elasticSearchDao.doSearchAndGetHits(OrganizationIndexService.indexName, organizationSearchSourceBuilder, Organization.class);
        final Map<Long, String> organizationIdCountryCodeMap = organizationSearchResponseEntityWrapper.getSearchResponseEntities().stream()
                .map(SearchResponseEntity::getSource)
                .filter(o -> o.getCountryCode() != null)
                .collect(Collectors.toMap(Organization::getOrganizationId, Organization::getCountryCode));

        final Map<String, Set<String>> resultMap = new HashMap<>();
        for (String expertCode : expertCodes) {
            final Set<Long> expertOrganizationIds = expertOrganizationIdsMap.getOrDefault(expertCode, Collections.emptySet());
            final Set<String> countryCodes = expertOrganizationIds.stream()
                    .map(organizationIdCountryCodeMap::get)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
            resultMap.put(expertCode, countryCodes);
        }
        return resultMap;
    }

    private Map<String, Set<Long>> getExpertAffiliatedOrganizationIdsMap(Set<String> expertCodes) throws IOException {
        final Map<String, Set<Long>> itemAffiliatedIds = findItemAffiliatedOrganizationsForExperts(expertCodes);
        final Map<String, Set<Long>> projectAffiliatedIds = findProjectAffiliatedOrganizationsForExperts(expertCodes);
        final Map<String, Set<Long>> commercializationAffiliatedIds = findCommercializationAffiliatedOrganizationsForExperts(expertCodes);
        final Map<String, Set<Long>> resultMap = new HashMap<>();
        for (String expertCode : expertCodes) {
            final Set<Long> organizationIds = new HashSet<>();
            organizationIds.addAll(itemAffiliatedIds.getOrDefault(expertCode, Collections.emptySet()));
            organizationIds.addAll(projectAffiliatedIds.getOrDefault(expertCode, Collections.emptySet()));
            organizationIds.addAll(commercializationAffiliatedIds.getOrDefault(expertCode, Collections.emptySet()));
            resultMap.put(expertCode, organizationIds);
        }
        return resultMap;
    }

    private Map<String, Double> getExpertRankMap(Set<String> expertCodes) throws IOException {
        final ExpertSearchFilter expertSearchFilter = new ExpertSearchFilter();
        expertSearchFilter.setAvailableExpertCodes(expertCodes);
        final FunctionScoreQueryBuilder functionScoreQueryBuilder = ItemSearchUtil.prepareFindByExpertiseScoreQueryBuilder(expertSearchFilter, true, 1F, null);
        final String[] includeValues = expertCodes.toArray(new String[0]);
        final SearchSourceBuilder expertItemSearchSourceBuilder = new SearchSourceBuilder()
                .size(0)
                .query(functionScoreQueryBuilder).aggregation(
                        new TermsAggregationBuilder("experts", ValueType.STRING)
                                .field("expertCodes")
                                .size(maxBucketSize)
                                .includeExclude(new IncludeExclude(includeValues, null))
                                .subAggregation(new SumAggregationBuilder("score").script(new Script("_score"))));
        final SearchResponse searchResponse = elasticSearchDao.doSearch(ItemIndexService.indexName, expertItemSearchSourceBuilder);
        final Terms experts = searchResponse.getAggregations().get("experts");
        final Map<String, Double> stringFloatHashMap = new HashMap<>();
        for (Terms.Bucket bucket : experts.getBuckets()) {
            final Sum score = bucket.getAggregations().get("score");
            stringFloatHashMap.put(bucket.getKeyAsString(), score.getValue());
        }
        return stringFloatHashMap;
    }

    private Map<String, Set<Long>> findItemAffiliatedOrganizationsForExperts(Set<String> expertCodes) throws IOException {
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder()
                .query(new TermsQueryBuilder("expertCodes", expertCodes))
                .size(0)
                .aggregation(new TermsAggregationBuilder("experts", ValueType.STRING)
                        .field("expertCodes")
                        .size(maxBucketSize)
                        .includeExclude(new IncludeExclude(expertCodes.toArray(new String[0]), null))
                        .subAggregation(new TermsAggregationBuilder("organizations", ValueType.STRING)
                                .field("evidenceOrganizationIds")
                                .size(maxBucketSize)));
        final SearchResponse searchResponse = elasticSearchDao.doSearch(ItemIndexService.indexName, sourceBuilder);
        final Terms experts = searchResponse.getAggregations().get("experts");
        return experts.getBuckets().stream()
                .collect(Collectors.toMap(Terms.Bucket::getKeyAsString, x -> this.getAggregatedOrganizationIds(x.getAggregations().get("organizations"))));
    }

    private Map<String, Set<Long>> findProjectAffiliatedOrganizationsForExperts(Set<String> expertCodes) throws IOException {
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder()
                .query(new TermsQueryBuilder("expertCodes", expertCodes))
                .size(0)
                .aggregation(new TermsAggregationBuilder("experts", ValueType.STRING)
                        .field("expertCodes")
                        .size(maxBucketSize)
                        .includeExclude(new IncludeExclude(expertCodes.toArray(new String[0]), null))
                        .subAggregation(new TermsAggregationBuilder("organizations", ValueType.STRING)
                                .field("evidenceOrganizationIds")
                                .size(maxBucketSize)));
        final SearchResponse searchResponse = elasticSearchDao.doSearch(ProjectIndexService.indexName, sourceBuilder);
        final Terms experts = searchResponse.getAggregations().get("experts");
        final Map<String, Set<Long>> expertCodeAffiliatedOrganizationsMap = new HashMap<>();
        for (Terms.Bucket expertBucket : experts.getBuckets()) {
            final String expertCode = expertBucket.getKeyAsString();
            final Set<Long> organizationIds = getAggregatedOrganizationIds(expertBucket.getAggregations().get("organizations"));
            expertCodeAffiliatedOrganizationsMap.put(expertCode, organizationIds);
        }
        return expertCodeAffiliatedOrganizationsMap;
    }

    private Map<String, Set<Long>> findCommercializationAffiliatedOrganizationsForExperts(Set<String> expertCodes) throws IOException {
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder()
                .query(new TermsQueryBuilder("expertCodes", expertCodes))
                .size(0)
                .aggregation(new TermsAggregationBuilder("experts", ValueType.STRING)
                        .field("expertCodes")
                        .size(maxBucketSize)
                        .includeExclude(new IncludeExclude(expertCodes.toArray(new String[0]), null))
                        .subAggregation(new NestedAggregationBuilder("organizationsNested", "commercializationProjectOrganizations")
                                .subAggregation(new TermsAggregationBuilder("organizations", ValueType.STRING)
                                        .field("commercializationProjectOrganizations.organizationId")
                                        .size(maxBucketSize))));
        final SearchResponse searchResponse = elasticSearchDao.doSearch(CommercializationProjectIndexService.indexName, sourceBuilder);
        final Terms experts = searchResponse.getAggregations().get("experts");
        final Map<String, Set<Long>> expertCodeAffiliatedOrganizationsMap = new HashMap<>();
        for (Terms.Bucket expertBucket : experts.getBuckets()) {
            final Nested organizationsNested = expertBucket.getAggregations().get("organizationsNested");
            final String expertCode = expertBucket.getKeyAsString();
            final Set<Long> organizationIds = getAggregatedOrganizationIds(organizationsNested.getAggregations().get("organizations"));
            expertCodeAffiliatedOrganizationsMap.put(expertCode, organizationIds);
        }
        return expertCodeAffiliatedOrganizationsMap;
    }

    private Set<Long> getAggregatedOrganizationIds(Terms aggregation) {
        return aggregation.getBuckets().stream()
                .map(Terms.Bucket::getKeyAsNumber)
                .map(TypeConversionUtil::getLongValue)
                .collect(Collectors.toSet());
    }
}
