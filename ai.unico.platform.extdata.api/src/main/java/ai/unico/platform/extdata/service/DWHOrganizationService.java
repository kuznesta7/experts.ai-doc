package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.DWHOrganizationDto;
import ai.unico.platform.extdata.dto.DWHOrganizationStructureDto;

import java.util.List;

public interface DWHOrganizationService {

    List<DWHOrganizationDto> findOrganizations();

    List<DWHOrganizationStructureDto> findOrganizationStructure();

    List<DWHOrganizationStructureDto> findOrganizationMembers();

}
