package ai.unico.platform.extdata.dto;

public class ProjectFinanceDto {

    private Long originalOrganizationId;

    private Integer year;

    private Double total;

    private Double nationalSupport;

    private Double privateFinances;

    private Double otherFinances;

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getNationalSupport() {
        return nationalSupport;
    }

    public void setNationalSupport(Double nationalSupport) {
        this.nationalSupport = nationalSupport;
    }

    public Double getPrivateFinances() {
        return privateFinances;
    }

    public void setPrivateFinances(Double privateFinances) {
        this.privateFinances = privateFinances;
    }

    public Double getOtherFinances() {
        return otherFinances;
    }

    public void setOtherFinances(Double otherFinances) {
        this.otherFinances = otherFinances;
    }

    public Long getOriginalOrganizationId() {
        return originalOrganizationId;
    }

    public void setOriginalOrganizationId(Long originalOrganizationId) {
        this.originalOrganizationId = originalOrganizationId;
    }
}
