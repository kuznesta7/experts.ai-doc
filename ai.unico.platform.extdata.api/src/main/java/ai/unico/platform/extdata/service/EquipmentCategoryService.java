package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.CategoryDwhDto;

import java.util.List;

public interface EquipmentCategoryService {

    List<CategoryDwhDto> getEquipmentType();

    List<CategoryDwhDto> getEquipmentDomain();
}
