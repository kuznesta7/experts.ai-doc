package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.ExpertItemPreviewDto;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface DWHItemService {

    List<ExpertItemPreviewDto> findItemPreviews(Integer limit, Integer offset);

    Map<Long, Set<Long>> findExpertIdsForItems(Set<Long> itemId);

    class ItemOrganization {
        private Long itemId;
        private Long organizationId;

        public Long getItemId() {
            return itemId;
        }

        public void setItemId(Long itemId) {
            this.itemId = itemId;
        }

        public Long getOrganizationId() {
            return organizationId;
        }

        public void setOrganizationId(Long organizationId) {
            this.organizationId = organizationId;
        }
    }

    class ItemExpert {
        private Long itemId;
        private Long expertId;
        private String role;

        public Long getItemId() {
            return itemId;
        }

        public void setItemId(Long itemId) {
            this.itemId = itemId;
        }

        public Long getExpertId() {
            return expertId;
        }

        public void setExpertId(Long expertId) {
            this.expertId = expertId;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }
    }
}
