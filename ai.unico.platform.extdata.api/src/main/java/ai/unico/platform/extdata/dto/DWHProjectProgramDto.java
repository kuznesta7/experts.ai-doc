package ai.unico.platform.extdata.dto;

public class DWHProjectProgramDto {

    private Long projectProgramId;

    private String projectProgramName;

    public Long getProjectProgramId() {
        return projectProgramId;
    }

    public void setProjectProgramId(Long projectProgramId) {
        this.projectProgramId = projectProgramId;
    }

    public String getProjectProgramName() {
        return projectProgramName;
    }

    public void setProjectProgramName(String projectProgramName) {
        this.projectProgramName = projectProgramName;
    }
}
