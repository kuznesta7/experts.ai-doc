package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.DWHProjectDto;

import java.util.List;

public interface DWHProjectService {

    List<DWHProjectDto> findProjects(Integer limit, Integer offset);

    DWHProjectDto findProject(Long originalProjectId);

}
