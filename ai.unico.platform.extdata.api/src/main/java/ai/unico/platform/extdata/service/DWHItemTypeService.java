package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.DWHItemTypeDto;

import java.util.List;

public interface DWHItemTypeService {

    List<DWHItemTypeDto> findItemTypes();

}
