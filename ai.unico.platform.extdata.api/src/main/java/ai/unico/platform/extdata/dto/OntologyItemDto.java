package ai.unico.platform.extdata.dto;

import java.util.ArrayList;
import java.util.List;

public class OntologyItemDto {

    private String value;

    private List<RelatedItem> RelatedItems = new ArrayList<>();

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<RelatedItem> getRelatedItems() {
        return RelatedItems;
    }

    public void setRelatedItems(List<RelatedItem> RelatedItems) {
        this.RelatedItems = RelatedItems;
    }

    public static class RelatedItem {

        private String value;

        private Double relevancy;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Double getRelevancy() {
            return relevancy;
        }

        public void setRelevancy(Double relevancy) {
            this.relevancy = relevancy;
        }
    }
}
