package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.DWHProjectProgramDto;

import java.util.List;

public interface DWHProjectProgramService {

    List<DWHProjectProgramDto> findProjectPrograms();

}
