package ai.unico.platform.extdata.dto;

public class DWHOrganizationDto {

    private Long originalOrganizationId;
    private String organizationName;
    private Long rank;
    private Long substituteOrganizationId;
    private String countryCode;

    public Long getOriginalOrganizationId() {
        return originalOrganizationId;
    }

    public void setOriginalOrganizationId(Long originalOrganizationId) {
        this.originalOrganizationId = originalOrganizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Long getRank() {
        return rank;
    }

    public void setRank(Long rank) {
        this.rank = rank;
    }

    public Long getSubstituteOrganizationId() {
        return substituteOrganizationId;
    }

    public void setSubstituteOrganizationId(Long substituteOrganizationId) {
        this.substituteOrganizationId = substituteOrganizationId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

}
