package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.ExpertThesisPreviewDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface DWHThesisService {

    List<ExpertThesisPreviewDto> findThesisPreviews(Integer limit, Integer offset);

    Map<Long, HashMap<String, Set<Long>>> findExpertIdsForThesis(Set<Long> itemIds);

    class ThesisOrganization {
        private Long itemId;
        private Long organizationId;

        public Long getItemId() {
            return itemId;
        }

        public void setItemId(Long itemId) {
            this.itemId = itemId;
        }

        public Long getOrganizationId() {
            return organizationId;
        }

        public void setOrganizationId(Long organizationId) {
            this.organizationId = organizationId;
        }
    }

    class ThesisExpert {
        private Long itemId;
        private Long expertId;

        public Long getItemId() {
            return itemId;
        }

        public void setItemId(Long itemId) {
            this.itemId = itemId;
        }

        public Long getExpertId() {
            return expertId;
        }

        public void setExpertId(Long expertId) {
            this.expertId = expertId;
        }
    }
}
