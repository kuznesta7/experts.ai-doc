package ai.unico.platform.extdata.dto;

public class DWHOrganizationStructureDto {

    private Long parentOrganizationId;

    private Long childOrganizationId;

    private String relationType;

    public Long getParentOrganizationId() {
        return parentOrganizationId;
    }

    public void setParentOrganizationId(Long parentOrganizationId) {
        this.parentOrganizationId = parentOrganizationId;
    }

    public Long getChildOrganizationId() {
        return childOrganizationId;
    }

    public void setChildOrganizationId(Long childOrganizationId) {
        this.childOrganizationId = childOrganizationId;
    }

    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }
}
