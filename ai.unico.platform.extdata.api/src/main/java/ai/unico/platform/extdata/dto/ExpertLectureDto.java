package ai.unico.platform.extdata.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ExpertLectureDto {

    private Long itemId;

    private String itemBk;

    private String itemName;

    private String itemDescription;

    private Long itemTypeId;

    private Integer year;

    private String itemSpecification;

    private String itemDomain;

    private String itemScienceSpecification;

    private List<String> keywords = new ArrayList<>();

    private Set<Long> expertIds = new HashSet<>();

    private Set<Long> originalOrganizationIds = new HashSet<>();

    private boolean translationUnavailable;

    private boolean confidential;

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getItemBk() {
        return itemBk;
    }

    public void setItemBk(String itemBk) {
        this.itemBk = itemBk;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Long getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(Long itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getItemSpecification() {
        return itemSpecification;
    }

    public void setItemSpecification(String itemSpecification) {
        this.itemSpecification = itemSpecification;
    }

    public String getItemDomain() {
        return itemDomain;
    }

    public void setItemDomain(String itemDomain) {
        this.itemDomain = itemDomain;
    }

    public String getItemScienceSpecification() {
        return itemScienceSpecification;
    }

    public void setItemScienceSpecification(String itemScienceSpecification) {
        this.itemScienceSpecification = itemScienceSpecification;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Set<Long> getExpertIds() {
        return expertIds;
    }

    public void setExpertIds(Set<Long> expertIds) {
        this.expertIds = expertIds;
    }

    public Set<Long> getOriginalOrganizationIds() {
        return originalOrganizationIds;
    }

    public void setOriginalOrganizationIds(Set<Long> originalOrganizationIds) {
        this.originalOrganizationIds = originalOrganizationIds;
    }

    public boolean isTranslationUnavailable() {
        return translationUnavailable;
    }

    public void setTranslationUnavailable(boolean translationUnavailable) {
        this.translationUnavailable = translationUnavailable;
    }

    public boolean isConfidential() {
        return confidential;
    }

    public void setConfidential(boolean confidential) {
        this.confidential = confidential;
    }
}
