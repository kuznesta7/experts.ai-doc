package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.DWHEquipmentDto;

import java.util.List;

public interface DWHEquipmentService {
    List<DWHEquipmentDto> findEquipment(Integer offset, Integer limit);

}
