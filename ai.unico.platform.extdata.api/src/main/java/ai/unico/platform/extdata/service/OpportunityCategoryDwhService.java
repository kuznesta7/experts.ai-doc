package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.CategoryDwhDto;

import java.util.List;

public interface OpportunityCategoryDwhService {

    List<CategoryDwhDto> getOpportunityTypes();

    List<CategoryDwhDto> getJobTypes();
}
