package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.OntologyItemDto;

import java.util.Set;

public interface OntologyService {

    Set<OntologyItemDto> getOntologyItems(int firstResult, int maxResult);

}
