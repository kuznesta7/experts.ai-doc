package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.DWHOpportunityDto;

import java.util.List;

public interface DWHOpportunityService {

    List<DWHOpportunityDto> findOpportunities(Integer limit, Integer offset);
}
