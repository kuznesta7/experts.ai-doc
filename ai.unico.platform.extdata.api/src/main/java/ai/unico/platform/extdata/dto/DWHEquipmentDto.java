package ai.unico.platform.extdata.dto;

import java.util.List;

public class DWHEquipmentDto {
    private Long equipmentId;
    private String equipmentName;
    private String equipmentMarking;
    private String equipmentDesc;
    private List<String> equipmentSpecialization;
    private Integer year;
    private Integer equipmentServiceLife;
    private Boolean portableDevice;
    private Long equipmentDomainId;
    private Long equipmentTypeId;
    private boolean translationUnavailable;
    private List<Long> organizationIds;

    public Long getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(Long equipmentId) {
        this.equipmentId = equipmentId;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getEquipmentMarking() {
        return equipmentMarking;
    }

    public void setEquipmentMarking(String equipmentMarking) {
        this.equipmentMarking = equipmentMarking;
    }

    public String getEquipmentDesc() {
        return equipmentDesc;
    }

    public void setEquipmentDesc(String equipmentDesc) {
        this.equipmentDesc = equipmentDesc;
    }

    public List<String> getEquipmentSpecialization() {
        return equipmentSpecialization;
    }

    public void setEquipmentSpecialization(List<String> equipmentSpecialization) {
        this.equipmentSpecialization = equipmentSpecialization;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getEquipmentServiceLife() {
        return equipmentServiceLife;
    }

    public void setEquipmentServiceLife(Integer equipmentServiceLife) {
        this.equipmentServiceLife = equipmentServiceLife;
    }

    public Boolean getPortableDevice() {
        return portableDevice;
    }

    public void setPortableDevice(Boolean portableDevice) {
        this.portableDevice = portableDevice;
    }

    public Long getEquipmentDomainId() {
        return equipmentDomainId;
    }

    public void setEquipmentDomainId(Long equipmentDomainId) {
        this.equipmentDomainId = equipmentDomainId;
    }

    public Long getEquipmentTypeId() {
        return equipmentTypeId;
    }

    public void setEquipmentTypeId(Long equipmentTypeId) {
        this.equipmentTypeId = equipmentTypeId;
    }

    public boolean isTranslationUnavailable() {
        return translationUnavailable;
    }

    public void setTranslationUnavailable(boolean translationUnavailable) {
        this.translationUnavailable = translationUnavailable;
    }

    public List<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(List<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }
}
