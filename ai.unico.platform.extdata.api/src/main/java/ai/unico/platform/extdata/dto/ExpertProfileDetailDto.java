package ai.unico.platform.extdata.dto;

import ai.unico.platform.util.dto.OrganizationDto;

import java.util.ArrayList;
import java.util.List;

public class ExpertProfileDetailDto {

    private Long expertId;

    private String expertFullName;

    private List<ExpertItemPreviewDto> itemPreviewDtos = new ArrayList<>();

    private List<OrganizationDto> organizationDtos = new ArrayList<>();

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public String getExpertFullName() {
        return expertFullName;
    }

    public void setExpertFullName(String expertFullName) {
        this.expertFullName = expertFullName;
    }

    public List<ExpertItemPreviewDto> getExpertItemPreviewDtos() {
        return itemPreviewDtos;
    }

    public void setExpertItemPreviewDtos(List<ExpertItemPreviewDto> itemPreviewDtos) {
        this.itemPreviewDtos = itemPreviewDtos;
    }

    public List<OrganizationDto> getOrganizationDtos() {
        return organizationDtos;
    }

    public void setOrganizationDtos(List<OrganizationDto> organizationDtos) {
        this.organizationDtos = organizationDtos;
    }
}
