package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.ExpertLectureDto;

import java.util.List;

public interface DWHLectureService {

    List<ExpertLectureDto> findItemPreviews(Integer limit, Integer offset);

}
