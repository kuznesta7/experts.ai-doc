package ai.unico.platform.extdata.dto;

import java.util.HashSet;
import java.util.Set;

public class ExpertProfilePreviewDto {

    private Long expertId;

    private String expertFullName;

    private Set<Long> originalOrganizationIds = new HashSet<>();

    private Set<Long> retiredOrganizationIds = new HashSet<>();

    private Set<Long> visibleOrganizationIds = new HashSet<>();

    private Set<Long> activeOrganizationIds = new HashSet<>();

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public String getExpertFullName() {
        return expertFullName;
    }

    public void setExpertFullName(String expertFullName) {
        this.expertFullName = expertFullName;
    }

    public Set<Long> getOriginalOrganizationIds() {
        return originalOrganizationIds;
    }

    public void setOriginalOrganizationIds(Set<Long> originalOrganizationIds) {
        this.originalOrganizationIds = originalOrganizationIds;
    }

    public Set<Long> getRetiredOrganizationIds() {
        return retiredOrganizationIds;
    }

    public void setRetiredOrganizationIds(Set<Long> retiredOrganizationIds) {
        this.retiredOrganizationIds = retiredOrganizationIds;
    }

    public Set<Long> getVisibleOrganizationIds() {
        return visibleOrganizationIds;
    }

    public void setVisibleOrganizationIds(Set<Long> visibleOrganizationIds) {
        this.visibleOrganizationIds = visibleOrganizationIds;
    }

    public Set<Long> getActiveOrganizationIds() {
        return activeOrganizationIds;
    }

    public void setActiveOrganizationIds(Set<Long> activeOrganizationIds) {
        this.activeOrganizationIds = activeOrganizationIds;
    }
}
