package ai.unico.platform.extdata.dto;

import java.util.Date;
import java.util.List;
import java.util.Set;

public class DWHOpportunityDto {
    private Long opportunityId;
    private String opportunityName;
    private String opportunityDescription;
    private List<String> opportunityKw; // split by , to list
    private Date opportunitySignupDate;
    private String opportunityLocation;
    private String opportunityWage;
    private String opportunityTechReq;
    private String opportunityFormReq;
    private String opportunityOtherReq;
    private String opportunityBenefit;
    private Date opportunityJobStartDate;
    private String opportunityExtLink;
    private String opportunityHomeOffice;
    private Long opportunityType; //maybe change to type
    private Set<Long> jobTypes; //maybe change to type
    private Set<Long> organizationIds;
    private Set<Long> expertIds;

    public Long getOpportunityId() {
        return opportunityId;
    }

    public void setOpportunityId(Long opportunityId) {
        this.opportunityId = opportunityId;
    }

    public String getOpportunityName() {
        return opportunityName;
    }

    public void setOpportunityName(String opportunityName) {
        this.opportunityName = opportunityName;
    }

    public String getOpportunityDescription() {
        return opportunityDescription;
    }

    public void setOpportunityDescription(String opportunityDescription) {
        this.opportunityDescription = opportunityDescription;
    }

    public List<String> getOpportunityKw() {
        return opportunityKw;
    }

    public void setOpportunityKw(List<String> opportunityKw) {
        this.opportunityKw = opportunityKw;
    }

    public Date getOpportunitySignupDate() {
        return opportunitySignupDate;
    }

    public void setOpportunitySignupDate(Date opportunitySignupDate) {
        this.opportunitySignupDate = opportunitySignupDate;
    }

    public String getOpportunityLocation() {
        return opportunityLocation;
    }

    public void setOpportunityLocation(String opportunityLocation) {
        this.opportunityLocation = opportunityLocation;
    }

    public String getOpportunityWage() {
        return opportunityWage;
    }

    public void setOpportunityWage(String opportunityWage) {
        this.opportunityWage = opportunityWage;
    }

    public String getOpportunityTechReq() {
        return opportunityTechReq;
    }

    public void setOpportunityTechReq(String opportunityTechReq) {
        this.opportunityTechReq = opportunityTechReq;
    }

    public String getOpportunityFormReq() {
        return opportunityFormReq;
    }

    public void setOpportunityFormReq(String opportunityFormReq) {
        this.opportunityFormReq = opportunityFormReq;
    }

    public String getOpportunityOtherReq() {
        return opportunityOtherReq;
    }

    public void setOpportunityOtherReq(String opportunityOtherReq) {
        this.opportunityOtherReq = opportunityOtherReq;
    }

    public String getOpportunityBenefit() {
        return opportunityBenefit;
    }

    public void setOpportunityBenefit(String opportunityBenefit) {
        this.opportunityBenefit = opportunityBenefit;
    }

    public Date getOpportunityJobStartDate() {
        return opportunityJobStartDate;
    }

    public void setOpportunityJobStartDate(Date opportunityJobStartDate) {
        this.opportunityJobStartDate = opportunityJobStartDate;
    }

    public String getOpportunityExtLink() {
        return opportunityExtLink;
    }

    public void setOpportunityExtLink(String opportunityExtLink) {
        this.opportunityExtLink = opportunityExtLink;
    }

    public String getOpportunityHomeOffice() {
        return opportunityHomeOffice;
    }

    public void setOpportunityHomeOffice(String opportunityHomeOffice) {
        this.opportunityHomeOffice = opportunityHomeOffice;
    }

    public Long getOpportunityType() {
        return opportunityType;
    }

    public void setOpportunityType(Long opportunityType) {
        this.opportunityType = opportunityType;
    }

    public Set<Long> getJobTypes() {
        return jobTypes;
    }

    public void setJobTypes(Set<Long> jobTypes) {
        this.jobTypes = jobTypes;
    }

    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public Set<Long> getExpertIds() {
        return expertIds;
    }

    public void setExpertIds(Set<Long> expertIds) {
        this.expertIds = expertIds;
    }
}
