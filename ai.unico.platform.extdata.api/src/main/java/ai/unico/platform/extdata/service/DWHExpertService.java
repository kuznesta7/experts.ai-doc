package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.ExpertProfilePreviewDto;

import java.util.List;

public interface DWHExpertService {

    List<ExpertProfilePreviewDto> findExperts(Integer limit, Integer offset);

}
