package ai.unico.platform.extdata.dto;

import java.util.HashSet;
import java.util.Set;

public class ClaimSuggestionDto {

    private Long expertId;

    private String expertFullName;

    private Set<String> organizationNames = new HashSet<>();

    private Set<String> itemNames = new HashSet<>();

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public String getExpertFullName() {
        return expertFullName;
    }

    public void setExpertFullName(String expertFullName) {
        this.expertFullName = expertFullName;
    }

    public Set<String> getOrganizationNames() {
        return organizationNames;
    }

    public void setOrganizationNames(Set<String> organizationNames) {
        this.organizationNames = organizationNames;
    }

    public Set<String> getItemNames() {
        return itemNames;
    }

    public void setItemNames(Set<String> itemNames) {
        this.itemNames = itemNames;
    }
}
