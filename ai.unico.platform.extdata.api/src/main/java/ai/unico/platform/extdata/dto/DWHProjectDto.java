package ai.unico.platform.extdata.dto;

import java.util.*;

public class DWHProjectDto {

    private Long originalProjectId;

    private String projectNumber;

    private String projectName;

    private String projectDescription;

    private Long projectProgramId;

    private Date startDate;

    private Date endDate;

    private String countryCode;

    private List<String> keywords;

    private String projectLink;

    private List<ProjectFinanceDto> projectFinances = new ArrayList<>();

    private Set<Long> projectOriginalItemIds = new HashSet<>();

    private Set<Long> participatingOriginalOrganizationIds = new HashSet<>();

    private Set<Long> providingOriginalOrganizationIds = new HashSet<>();

    private Set<Long> projectExpertIds = new HashSet<>();

    private boolean translationUnavailable;

    private boolean confidential;

    public DWHProjectDto() {
    }

    public Long getOriginalProjectId() {
        return originalProjectId;
    }

    public void setOriginalProjectId(Long originalProjectId) {
        this.originalProjectId = originalProjectId;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<ProjectFinanceDto> getProjectFinances() {
        return projectFinances;
    }

    public void setProjectFinances(List<ProjectFinanceDto> projectFinances) {
        this.projectFinances = projectFinances;
    }

    public Set<Long> getProjectOriginalItemIds() {
        return projectOriginalItemIds;
    }

    public void setProjectOriginalItemIds(Set<Long> projectOriginalItemIds) {
        this.projectOriginalItemIds = projectOriginalItemIds;
    }

    public Set<Long> getParticipatingOriginalOrganizationIds() {
        return participatingOriginalOrganizationIds;
    }

    public void setParticipatingOriginalOrganizationIds(Set<Long> participatingOriginalOrganizationIds) {
        this.participatingOriginalOrganizationIds = participatingOriginalOrganizationIds;
    }

    public Set<Long> getProvidingOriginalOrganizationIds() {
        return providingOriginalOrganizationIds;
    }

    public void setProvidingOriginalOrganizationIds(Set<Long> providingOriginalOrganizationIds) {
        this.providingOriginalOrganizationIds = providingOriginalOrganizationIds;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Long getProjectProgramId() {
        return projectProgramId;
    }

    public void setProjectProgramId(Long projectProgramId) {
        this.projectProgramId = projectProgramId;
    }

    public boolean isTranslationUnavailable() {
        return translationUnavailable;
    }

    public void setTranslationUnavailable(boolean translationUnavailable) {
        this.translationUnavailable = translationUnavailable;
    }

    public boolean isConfidential() {
        return confidential;
    }

    public void setConfidential(boolean confidential) {
        this.confidential = confidential;
    }

    public Set<Long> getProjectExpertIds() {
        return projectExpertIds;
    }

    public void setProjectExpertIds(Set<Long> projectExpertIds) {
        this.projectExpertIds = projectExpertIds;
    }

    public String getProjectLink() {
        return projectLink;
    }

    public void setProjectLink(String projectLink) {
        this.projectLink = projectLink;
    }
}
