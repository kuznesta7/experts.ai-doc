import {Component, OnInit} from '@angular/core';
import {LoginService, SidebarMode} from '../services/login.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ContactModalComponent} from '../platform/components/common/contact-modal/contact-modal.component';
import {PathKey} from '../shared/services/path.service';
import {ActivatedRoute} from '@angular/router';
import {FlashService} from '../shared/services/flash.service';
import {UiThemeService} from '../shared/services/ui-theme.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./scss/login.component.scss']

})
export class LoginComponent implements OnInit {

  pathKey: PathKey = 'login';

  constructor(private loginService: LoginService,
              private route: ActivatedRoute,
              private ngbModal: NgbModal,
              private flashService: FlashService,
              private uiThemeService: UiThemeService) {
  }

  ngOnInit(): void {
    this.loginService.preventSidebarClose = true;
    this.loginService.openLoginSidebar(SidebarMode.LOG_IN);
    this.uiThemeService.loadTheme();
    this.loginService.redirectOnLoginSession();
  }

  openContactModal(): void {
    const ngbModalRef = this.ngbModal.open(ContactModalComponent, {size: 'lg'});
    ngbModalRef.componentInstance.contactType = this.pathKey;
    this.flashService.ok('Thank you for contacting us.');
  }
}
