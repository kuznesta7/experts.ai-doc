import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from 'ng-src/app/shared/shared.module';
import {LoginComponent} from "./login.component";
import {LoginRoutingModule} from "./login-routing.module";
import {PlatformModule} from "../platform/platform.module";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {WidgetsModule} from "../widgets/widgets.module";


@NgModule({
  declarations: [LoginComponent],
  bootstrap: [LoginComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    SharedModule,
    MatSlideToggleModule,
    WidgetsModule
  ],
  providers: [
    PlatformModule,

  ]
})
export class LoginModule {
}
