import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoaderComponent} from 'ng-src/app/shared/components/utils/loader/loader.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {PageLoaderComponent} from 'ng-src/app/shared/components/utils/loader/page-loader/page-loader.component';
import {PaginationComponent} from 'ng-src/app/shared/components/utils/pagination/pagination.component';
import {MultiSelectComponent} from 'ng-src/app/shared/components/utils/multiselect/multi-select.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TagsInputComponent} from 'ng-src/app/shared/components/utils/tags-input/tags-input.component';
import {MatChipsModule} from '@angular/material/chips';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTooltipModule} from '@angular/material/tooltip';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {far} from '@fortawesome/free-regular-svg-icons';
import {customIcons} from 'ng-src/app/shared/constants/custom-icons';
import {AssetsPipe} from 'ng-src/app/shared/pipes/assets.pipe';
import {UserImgPipe} from 'ng-src/app/shared/pipes/user-img.pipe';
import {GetPropertyPipe} from 'ng-src/app/shared/pipes/get-property.pipe';
import {PathPipe} from 'ng-src/app/shared/pipes/path.pipe';
import {
  OverlayLoaderComponent
} from 'ng-src/app/shared/components/utils/loader/overlay-loader/overlay-loader.component';
import {OutcomeCardComponent} from 'ng-src/app/shared/components/page-fragments/outcome-card/outcome-card.component';
import {OptionalImgDirective} from 'ng-src/app/shared/directives/optional-img.directive';
import {ExpertCardComponent} from 'ng-src/app/shared/components/page-fragments/expert-card/expert-card.component';
import {ProjectCardComponent} from 'ng-src/app/shared/components/page-fragments/project-card/project-card.component';
import {ConfidentialityColorPipe} from 'ng-src/app/shared/pipes/confidentiality-color.pipe';
import {RouterModule} from '@angular/router';
import {OutcomeTypePipe} from 'ng-src/app/shared/pipes/outcome-type.pipe';
import {OutcomeTypeToGroupPipe} from 'ng-src/app/shared/pipes/outcome-type-to-group.pipe';
import {ConfidentialityTitlePipe} from 'ng-src/app/shared/pipes/confidentiality-title.pipe';
import {
  NgbDateAdapter,
  NgbDateNativeAdapter,
  NgbModule,
  NgbPopoverModule,
  NgbTooltipModule
} from '@ng-bootstrap/ng-bootstrap';
import {
  OrganizationsPopoverComponent
} from 'ng-src/app/shared/components/utils/organizations-popover/organizations-popover.component';
import {KeywordsPopoverComponent} from 'ng-src/app/shared/components/utils/keywords-popover/keywords-popover.component';
import {GoogleChartsModule} from 'angular-google-charts';
import {SimpleBarChartComponent} from 'ng-src/app/shared/components/utils/simple-bar-chart/simple-bar-chart.component';
import {ShortNumberPipe} from 'ng-src/app/shared/pipes/short-number.pipe';
import {RoundPipe} from 'ng-src/app/shared/pipes/round.pipe';
import {
  CommercializationCardComponent
} from 'ng-src/app/shared/components/page-fragments/commercialization-card/commercialization-card.component';
import {CommercializationImgPipe} from 'ng-src/app/shared/pipes/commercialization-img.pipe';
import {MembersComponent} from 'ng-src/app/shared/components/utils/members/members.component';
import {ExpertsPopoverComponent} from 'ng-src/app/shared/components/utils/experts-popover/experts-popover.component';
import {OrganizationImgPipe} from 'ng-src/app/shared/pipes/organization-img.pipe';
import {EntriesPipe} from 'ng-src/app/shared/pipes/entries.pipe';
import {DisableAutofillDirective} from 'ng-src/app/shared/directives/disable-autofill.directive';
import {WebAddressPipe} from 'ng-src/app/shared/pipes/web-address.pipe';
import {
  OrganizationStructureComponent
} from 'ng-src/app/shared/components/page-fragments/organization-structure/organization-structure.component';
import {BaseDatePipe} from 'ng-src/app/shared/pipes/base-date.pipe';
import {
  OrganizationPreviewComponent
} from 'ng-src/app/shared/components/page-fragments/organization-preview/organization-preview.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule} from '@angular/material/list';
import {NgxFileDropModule} from 'ngx-file-drop';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {FlashService} from 'ng-src/app/shared/services/flash.service';
import {FormValidationService} from 'ng-src/app/shared/services/form-validation.service';
import {errorHttpInterceptorProvider} from 'ng-src/app/interceptors/error-interceptor';
import {AuthorizationGuard} from 'ng-src/app/guards/authorization.guard';
import {EvidenceGuard} from 'ng-src/app/guards/evidence.guard';
import {SessionService} from 'ng-src/app/services/session.service';
import {LoginSidebarComponent} from 'ng-src/app/shared/components/page-fragments/login-sidebar/login-sidebar.component';
import {
  ConfirmationModalComponent
} from 'ng-src/app/shared/components/utils/confirmation-modal/confirmation-modal.component';
import {
  HeaderSessionBoxComponent
} from 'ng-src/app/shared/components/page-fragments/header-session-box/header-session-box.component';
import {FileSizePipe} from 'ng-src/app/shared/pipes/file-size.pipe';
import {FileIconPipe} from 'ng-src/app/shared/pipes/file-icon.pipe';
import {ContainsPipe} from 'ng-src/app/shared/pipes/contains.pipe';
import {ExpertCodePipe} from 'ng-src/app/shared/pipes/expert-code.pipe';
import {
  OrganizationCardComponent
} from 'ng-src/app/shared/components/page-fragments/organization-card/organization-card.component';
import {NotEmptyPipe} from 'ng-src/app/shared/pipes/not-empty.pipe';
import {UrlPipe} from 'ng-src/app/shared/pipes/url.pipe';
import {ThesisCardComponent} from 'ng-src/app/shared/components/page-fragments/thesis-card/thesis-card.component';
import {LectureCardComponent} from 'ng-src/app/shared/components/page-fragments/lecture-card/lecture-card.component';
import {
  EquipmentCardComponent
} from 'ng-src/app/shared/components/page-fragments/equipment-card/equipment-card.component';
import {EquipmentDomainPipe} from 'ng-src/app/shared/pipes/equipment-domain.pipe';
import {EquipmentTypePipe} from 'ng-src/app/shared/pipes/equipment-type.pipe';
import {
  OpportunityCardComponent
} from 'ng-src/app/shared/components/page-fragments/opportunity-card/opportunity-card.component';
import {OpportunityTypePipe} from 'ng-src/app/shared/pipes/opportunity-type.pipe';
import {JobTypePipe} from 'ng-src/app/shared/pipes/job-type.pipe';
import {ShortUrlPipe} from 'ng-src/app/shared/pipes/short-url.pipe';
import {widgetResizeHttpInterceptorProvider} from 'ng-src/app/widgets/interceptors/widget-resize-interceptor';
import {
  ExpertActionsComponent
} from 'ng-src/app/shared/components/page-fragments/expert-actions/expert-actions.component';
import {LoginVerificationComponent} from './components/utils/login-verification/login-verification.component';
import {ExpertLinksComponent} from 'ng-src/app/shared/components/page-fragments/expert-link/expert-links.component';

@NgModule({
  declarations: [
    LoaderComponent,
    PageLoaderComponent,
    PaginationComponent,
    MultiSelectComponent,
    TagsInputComponent,
    AssetsPipe,
    UserImgPipe,
    GetPropertyPipe,
    PathPipe,
    OverlayLoaderComponent,
    OutcomeCardComponent,
    ExpertCardComponent,
    ProjectCardComponent,
    OptionalImgDirective,
    ConfidentialityColorPipe,
    OutcomeTypePipe,
    OutcomeTypeToGroupPipe,
    ConfidentialityTitlePipe,
    OrganizationsPopoverComponent,
    KeywordsPopoverComponent,
    SimpleBarChartComponent,
    ShortNumberPipe,
    RoundPipe,
    CommercializationCardComponent,
    CommercializationImgPipe,
    MembersComponent,
    ExpertsPopoverComponent,
    OrganizationImgPipe,
    EntriesPipe,
    DisableAutofillDirective,
    WebAddressPipe,
    OrganizationStructureComponent,
    BaseDatePipe,
    OrganizationPreviewComponent,
    LoginSidebarComponent,
    ConfirmationModalComponent,
    HeaderSessionBoxComponent,
    FileSizePipe,
    FileIconPipe,
    ContainsPipe,
    ExpertCodePipe,
    OrganizationCardComponent,
    NotEmptyPipe,
    UrlPipe,
    ThesisCardComponent,
    LectureCardComponent,
    EquipmentCardComponent,
    EquipmentDomainPipe,
    EquipmentTypePipe,
    OpportunityCardComponent,
    OpportunityTypePipe,
    JobTypePipe,
    ShortUrlPipe,
    ExpertActionsComponent,
    LoginVerificationComponent,
    ExpertLinksComponent
  ],
    imports: [
        CommonModule,
        MatSnackBarModule,
        MatPaginatorModule,
        MatSelectModule,
        ReactiveFormsModule,
        MatChipsModule,
        FontAwesomeModule,
        MatAutocompleteModule,
        MatTooltipModule,
        RouterModule,
        NgbPopoverModule,
        NgbTooltipModule,
        GoogleChartsModule.forRoot(),
        MatMenuModule,
        MatListModule,
        NgbModule,
        NgxFileDropModule,
        MatCheckboxModule,
        FormsModule,
    ],
    exports: [
        LoaderComponent,
        PageLoaderComponent,
        PaginationComponent,
        MultiSelectComponent,
        TagsInputComponent,
        FontAwesomeModule,
        AssetsPipe,
        UserImgPipe,
        GetPropertyPipe,
        PathPipe,
        OverlayLoaderComponent,
        OutcomeCardComponent,
        ExpertCardComponent,
        ProjectCardComponent,
        ConfidentialityColorPipe,
        OptionalImgDirective,
        ShortNumberPipe,
        RoundPipe,
        CommercializationCardComponent,
        OrganizationImgPipe,
        EntriesPipe,
        DisableAutofillDirective,
        WebAddressPipe,
        OrganizationStructureComponent,
        OutcomeTypeToGroupPipe,
        OutcomeTypePipe,
        ConfidentialityTitlePipe,
        BaseDatePipe,
        OrganizationPreviewComponent,
        CommercializationImgPipe,
        OrganizationsPopoverComponent,
        ReactiveFormsModule,
        GoogleChartsModule,
        MatMenuModule,
        MatListModule,
        NgbModule,
        NgxFileDropModule,
        MatCheckboxModule,
        MatSnackBarModule,
        MatPaginatorModule,
        MatSelectModule,
        MatChipsModule,
        MatAutocompleteModule,
        MatTooltipModule,
        RouterModule,
        NgbPopoverModule,
        NgbTooltipModule,
        LoginSidebarComponent,
        HeaderSessionBoxComponent,
        FileSizePipe,
        FileIconPipe,
        ContainsPipe,
        ExpertCodePipe,
        OrganizationCardComponent,
        NotEmptyPipe,
        UrlPipe,
        ThesisCardComponent,
        LectureCardComponent,
        EquipmentCardComponent,
        EquipmentDomainPipe,
        EquipmentTypePipe,
        OpportunityCardComponent,
        JobTypePipe,
        ShortUrlPipe,
        LoginVerificationComponent,
        ExpertLinksComponent
    ],
  providers: [
    FlashService,
    FormValidationService,
    errorHttpInterceptorProvider,
    widgetResizeHttpInterceptorProvider,
    AuthorizationGuard,
    EvidenceGuard,
    SessionService,
    {provide: NgbDateAdapter, useClass: NgbDateNativeAdapter}
  ]
})
export class SharedModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas, far, customIcons as any);
  }
}
