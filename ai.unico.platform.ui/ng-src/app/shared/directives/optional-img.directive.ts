import {
  Directive,
  ElementRef,
  EmbeddedViewRef,
  HostBinding,
  Input,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';

@Directive({
  selector: '[appOptionalImg]',
  host: {
    '(error)': 'handleError()',
    '(load)': 'load()',
    '[src]': 'src'
  }
})
export class OptionalImgDirective {

  constructor(private elementRef: ElementRef,
              private viewContainerRef: ViewContainerRef) {
  }

  @Input()
  src: string = '';

  @Input()
  altSrc: string = '';

  @Input()
  altTemplate: TemplateRef<any>;

  private embeddedView: EmbeddedViewRef<any>;

  @HostBinding('class')
  className = '';

  handleError() {
    if (this.altSrc) {
      this.src = this.altSrc;
    } else if (this.altTemplate) {
      this.elementRef.nativeElement.style.display = 'none';
      if (this.embeddedView) {
        this.embeddedView.destroy();
      }
      this.embeddedView = this.viewContainerRef.createEmbeddedView(this.altTemplate);
    } else {
      this.elementRef.nativeElement.style.display = 'none';
    }
  }

  load() {
    this.elementRef.nativeElement.style.display = null;
    if (this.embeddedView) this.embeddedView.destroy();
    this.className = 'image-loaded';
  }

}
