import { HttpParams } from '@angular/common/http';

export const createHttpParams = (paramsObject: any): HttpParams => {
  let httpParams = new HttpParams();
  Object.entries(paramsObject).forEach(([k, value]) => {
    if (Array.isArray(value)) {
      value.forEach(v => httpParams = httpParams.append(k, String(v)));
    } else if (value || value === 0 || value === '' || value === false) {
      httpParams = httpParams.set(k, String(value));
    }
  });
  return httpParams;
};
