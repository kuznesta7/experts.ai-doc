export function cmp(o1: any, o2: any): boolean {
  return o1 && o2 && o1 == o2;
}
