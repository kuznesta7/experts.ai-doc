export const localStorageStore = (key: string, object: any): void => {
  const value = JSON.stringify(object);
  localStorage.setItem(key, value);
};

export const localStorageGet = (key: string): any | null => {
  const itemString = localStorage.getItem(key);
  if (!itemString) return itemString;
  return JSON.parse(itemString);
};
