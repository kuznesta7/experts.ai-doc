import { AbstractControl, FormArray, FormControl, FormGroup } from '@angular/forms';

export const getFormErrors = (form: AbstractControl): { [errorName: string]: boolean } => {
  let controls;
  if (form.valid) return {};
  if (form instanceof FormControl) return form.errors as any;
  if (form instanceof FormGroup) controls = Object.values(form.controls);
  else if (form instanceof FormArray) controls = form.controls;
  else return {};
  return controls.reduce((result, c) => {
    return {...result, ...getFormErrors(c)};
  }, {});
};
