export const arrayContains = (array: any[] | undefined, element: any | undefined): boolean => {
  if (!array || !element) {
    return false;
  }
  return array.indexOf(element) != -1;
};
