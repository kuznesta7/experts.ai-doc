export const objectsEqual = (o1: any, o2: any): boolean => {
  if ((!o1 && !o2) || o1 === o2) {
    return true;
  }
  if ((!o1 && !!o2) || (!!o1 && !o2)) return false;
  if (Array.isArray(o1) && Array.isArray(o2)) {
    return o1.every((oo1, i) => objectsEqual(oo1, o2[i])) && o2.every((oo2, i) => objectsEqual(oo2, o1[i]));
  }
  if (typeof o1 === 'object' && typeof o2 === 'object') {
    const o1EqualsO2 = Object.entries(o1).every(([k1, v1]) => objectsEqual(v1, o2[k1]));
    const o2EqualsO1 = Object.entries(o2).every(([k2, v2]) => objectsEqual(v2, o1[k2]));
    return o1EqualsO2 && o2EqualsO1;
  }
  return false;
};

export const reduceObjectByPattern = (o: any, patternO: any): any => {
  return Object.keys(patternO).reduce((result, key) => {
    if (o == null || o[key] == null) {
      return result;
    }
    if (patternO[key] === null || typeof patternO[key] !== 'object') {
      result[key] = o[key];
    } else {
      result[key] = reduceObjectByPattern(o[key], patternO[key]);
    }
    return result;
  }, {} as any);
};

export const reduceObject = (o: any, keys: string[]): any => {
  return keys.reduce((result, k) => {
    result[k] = o[k];
    return result;
  }, {} as any);
};
