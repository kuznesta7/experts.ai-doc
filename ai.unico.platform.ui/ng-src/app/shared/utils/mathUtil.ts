export const round = (num: number, decimals?: number): number => {
  const multiplier = Math.pow(10, decimals != null ? decimals : 2);
  return Math.round(num * multiplier) / multiplier
};

export const roundObject = (obj: { [key: string]: number }): { [key: string]: number } => {
  return Object.entries(obj).reduce((result, [key, value]) => {
    const rounded = round(value);
    if (!isNaN(rounded)) {
      result[key] = rounded;
    } else {
      result[key] = value;
    }
    return result;
  }, {} as any);
};

export const sumObjectValues = (object: { [key: string]: any }): number => {
  return Object.values(object).reduce((sum, value) => sum + parseFloat(value), 0);
};

export const sumObjects = (...objects: { [key: string]: number }[]): { [key: string]: number } => {
  return objects.reduce((result, object) => {
    Object.entries(object).forEach(([key, value]) => {
      const v = result[key];
      if (!v) result[key] = 0;
      result[key] += value;
    });
    return result;
  }, {})
};
