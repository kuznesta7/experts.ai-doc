export interface IdDescriptionOption {
  id: number | string;
  description: string;
  subDescription?: string;
  descriptionLong?: string;
}
