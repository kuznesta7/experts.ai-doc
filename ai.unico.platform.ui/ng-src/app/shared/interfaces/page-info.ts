export interface PageInfo {
  page: number;
  limit: number;
}
