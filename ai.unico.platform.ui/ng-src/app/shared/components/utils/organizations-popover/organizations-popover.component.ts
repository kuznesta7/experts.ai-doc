import {Component, Input, OnInit} from '@angular/core';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ContactModalComponent} from 'ng-src/app/platform/components/common/contact-modal/contact-modal.component';
import {WidgetUtilService} from '../../../../widgets/utils/widget-util.service';

@Component({
  selector: 'app-organizations-popover',
  templateUrl: './organizations-popover.component.html'
})
export class OrganizationsPopoverComponent implements OnInit {

  constructor(private router: Router,
              private ngbModal: NgbModal,
              private widgetUtilService: WidgetUtilService) {
  }

  @Input()
  organizations: OrganizationBaseDto[];

  ngOnInit(): void {
  }

  checkOrganizationLicense(organization: OrganizationBaseDto): void {
    this.widgetUtilService.checkOrganizationLicense(organization);
  }

  openContactModal(organizationId: number | undefined): void {
    const ngbModalRef = this.ngbModal.open(ContactModalComponent, {size: 'lg'});
    ngbModalRef.componentInstance.organizationId = organizationId;
    ngbModalRef.componentInstance.contactType = 'organizationDetail';
  }

}
