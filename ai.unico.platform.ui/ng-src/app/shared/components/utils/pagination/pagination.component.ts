import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged, filter, map, tap } from 'rxjs/operators';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { QueryParamService } from 'ng-src/app/shared/services/query-param.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html'
})
export class PaginationComponent implements OnInit {

  @Input()
  disableUrl = false;

  @Input()
  set page(page: number) {
    this.currentPage$.next(page);
  }

  @Input()
  set limit(limit: number) {
    this.currentLimit$.next(limit);
  }

  @Input()
  set numberOfResults(numberOfResults: number | undefined) {
    this.numberOfResults$.next(numberOfResults);
  }

  @Input()
  set pageInfo(pageInfo: PageInfo | null) {
    if (!pageInfo) {
      return;
    }
    this.currentLimit$.next(pageInfo.limit);
    this.currentPage$.next(pageInfo.page);
  }

  @Input()
  options: { skipLocationChange?: boolean };

  @Output()
  pageChanged = new EventEmitter<PageInfo>();

  currentPage$ = new BehaviorSubject<number>(1);
  currentLimit$ = new BehaviorSubject<number>(30);
  numberOfResults$ = new BehaviorSubject<number | undefined>(undefined);
  currentPageFc = new FormControl(this.page);
  numberOfPages$: Observable<number>;
  nextPageAvailable$: Observable<boolean>;
  prevPageAvailable$: Observable<boolean>;
  nextPage$ = new Subject<number | null>();
  private pageInfo$ = new Subject<PageInfo | null>();

  constructor(private queryParamService: QueryParamService) {
  }

  ngOnInit(): void {
    const defaultPageInfo = {page: this.currentPage$.getValue(), limit: this.currentLimit$.getValue()};
    const pageInfoFromUrl = this.queryParamService.getFilter(defaultPageInfo, {numericFields: ['page', 'limit']});
    if (!this.disableUrl) {
      if (pageInfoFromUrl.page !== defaultPageInfo.page || pageInfoFromUrl.limit !== defaultPageInfo.limit) {
        this.currentPage$.next(pageInfoFromUrl.page);
        this.currentLimit$.next(pageInfoFromUrl.limit);
        this.pageChanged.emit(pageInfoFromUrl);
      }
    }

    this.numberOfPages$ = combineLatest([this.numberOfResults$, this.currentLimit$]).pipe(
      filter(([nr]) => nr != null),
      map(([nr, l]) => Math.ceil((nr || 1) / l)),
      distinctUntilChanged()
    );

    this.pageInfo$.pipe(
      tap(pi => {
        if (!pi) return;
        this.currentLimit$.next(pi.limit);
        this.currentPage$.next(pi.page);
        this.nextPage$.next(null);
      }),
      distinctUntilChanged((p1, p2) => {
        if (!p1 || !p2) return false;
        return p1.page === p2.page && p1.limit == p2.limit;
      })
    ).subscribe(pi => {
      if (!pi) return;
      if (!this.disableUrl) {
        this.queryParamService.updateUrlByFilter({page: pi.page}, this.options);
      }
      this.pageChanged.emit(pi);
    });

    this.prevPageAvailable$ = combineLatest([this.currentPage$, this.numberOfPages$]).pipe(
      map(([page, numberOfPages]) => this.pageAvailable(page - 1, numberOfPages))
    );
    this.nextPageAvailable$ = combineLatest([this.currentPage$, this.numberOfPages$]).pipe(
      map(([page, numberOfPages]) => this.pageAvailable(page + 1, numberOfPages))
    );

    this.currentPageFc.valueChanges.pipe(
      debounceTime(300),
      distinctUntilChanged(([p1], [p2]) => p1 === p2)
    ).subscribe(v => {
      const page = parseInt(v);
      if (isNaN(page)) return;
      this.nextPage$.next(page);
    });

    combineLatest([this.nextPage$, this.numberOfPages$]).pipe(
      distinctUntilChanged(([p1], [p2]) => p1 === p2),
      map(([page, numberOfPages]) => {
        if (page == null) {
          return null;
        }
        const limit = this.currentLimit$.getValue();
        if (page > numberOfPages) return {limit, page: numberOfPages};
        if (page < 1) return {limit, page: 1};
        return {limit, page};
      }),
    ).subscribe(pi => this.pageInfo$.next(pi));

    this.currentPage$.subscribe(p => this.currentPageFc.patchValue(p, {emitEvent: false}));
  }

  private pageAvailable(page: number, numberOfPages: number): boolean {
    return ((page >= 1) && (page <= numberOfPages));
  }
}
