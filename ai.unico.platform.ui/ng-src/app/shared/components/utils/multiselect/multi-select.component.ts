import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { IdDescriptionOption } from 'ng-src/app/shared/interfaces/idDescriptionOption';
import { arrayContains } from 'ng-src/app/shared/utils/arraysUtil';
import { distinctUntilChanged, filter, first, map, shareReplay, startWith } from 'rxjs/operators';
import { objectsEqual } from 'ng-src/app/shared/utils/object-util';

@Component({
  selector: 'app-multi-select',
  templateUrl: './multi-select.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => MultiSelectComponent),
    }
  ]
})
export class MultiSelectComponent implements OnInit, ControlValueAccessor {

  private onChange: (v: any) => {};
  private onTouched: () => {};

  internalFormControl = new FormControl([]);
  updateSelection$ = new Subject<void>();
  options$ = new BehaviorSubject<IdDescriptionOption[] | null>(null);
  allSelected$: Observable<boolean>;

  @Input()
  set options(options: IdDescriptionOption[] | null) {
    if (!options) {
      return;
    }
    this.options$.next(options);
  }

  @Input()
  noValueMeansAll = true;

  @Input()
  showAllSelectedValues = false;

  constructor() {
  }

  ngOnInit(): void {
    // this.internalFormControl.statusChanges.subscribe(console.log)
    this.internalFormControl.valueChanges.pipe(
      distinctUntilChanged((v1, v2) => objectsEqual(v1, v2))
    ).subscribe((values: IdDescriptionOption[]) => {
      if (!this.onChange) {
        return;
      }
      this.onChange(values.map(v => v.id));
    });

    this.allSelected$ = combineLatest([this.internalFormControl.valueChanges.pipe(startWith([])), this.options$, this.updateSelection$]).pipe(
      map(([value, options]) => objectsEqual(this.internalFormControl.value, options)),
      shareReplay()
    );
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.internalFormControl.disable();
    } else {
      this.internalFormControl.enable();
    }
  }

  writeValue(obj: any): void {
    this.options$.pipe(
      filter(o => !!o),
      first()
    ).subscribe(options => {
      const newVal = (options as IdDescriptionOption[]).filter(o => arrayContains(obj, o.id));
      this.internalFormControl.setValue(newVal, {emitEvent: false});
      this.updateSelection$.next();
    });
  }

}
