import { Component, Input, OnInit } from '@angular/core';
import { UserExpertBaseDto } from 'ng-src/app/interfaces/user-expert-base-dto';

@Component({
  selector: 'app-experts-popover',
  templateUrl: './experts-popover.component.html'
})
export class ExpertsPopoverComponent implements OnInit {

  constructor() {
  }

  @Input()
  experts: UserExpertBaseDto[];

  clickedExpert(): void {
  }

  ngOnInit(): void {
  }

}
