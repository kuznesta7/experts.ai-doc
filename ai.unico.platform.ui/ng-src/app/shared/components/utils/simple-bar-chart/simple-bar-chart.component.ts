import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-simple-bar-chart',
  templateUrl: './simple-bar-chart.component.html'
})
export class SimpleBarChartComponent implements OnInit {

  constructor() {
  }

  @Input()
  itemName: string;

  @Input()
  itemNamePlural: string;

  @Input()
  set data(data: { [key: number]: number }) {
    this.data$.next(Object.entries(data).map(([k, v]) => {
      return {label: parseInt(k), value: v};
    }));
  }

  data$ = new BehaviorSubject<{ value: number, label: number }[]>([]);
  maxValue$: Observable<number>;
  maxLabel$: Observable<number>;
  minLabel$: Observable<number>;

  ngOnInit(): void {
    this.maxValue$ = this.data$.pipe(map(data => Math.max(...data.map(item => item.value))), shareReplay());
    this.minLabel$ = this.data$.pipe(map(data => Math.min(...data.map(item => item.label))), shareReplay());
    this.maxLabel$ = this.data$.pipe(map(data => Math.max(...data.map(item => item.label))), shareReplay());
  }

}
