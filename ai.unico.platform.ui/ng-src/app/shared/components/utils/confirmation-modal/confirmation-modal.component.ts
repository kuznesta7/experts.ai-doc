import { Component, Input, OnInit } from '@angular/core';
import { ConfirmationModalOptions } from 'ng-src/app/shared/services/confirmation.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html'
})
export class ConfirmationModalComponent implements OnInit {

  constructor(private activeModal: NgbActiveModal) {
  }

  @Input()
  options: ConfirmationModalOptions;

  ngOnInit(): void {
  }

  close() {
    this.activeModal.close();
  }

  dismiss() {
    this.activeModal.dismiss();
  }

}
