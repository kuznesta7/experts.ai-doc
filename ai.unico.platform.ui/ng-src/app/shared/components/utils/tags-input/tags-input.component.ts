import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {IdDescriptionOption} from 'ng-src/app/shared/interfaces/idDescriptionOption';
import {ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';

@Component({
  selector: 'app-tags-input',
  templateUrl: './tags-input.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => TagsInputComponent),
    }
  ]
})
export class TagsInputComponent implements OnInit, ControlValueAccessor {

  private onChange: (v: any) => {};
  private onTouched: () => {};

  inputFormControl = new FormControl('');

  tags$ = new BehaviorSubject<IdDescriptionOption[]>([]);
  disabled$ = new BehaviorSubject(false);
  options$ = new BehaviorSubject<IdDescriptionOption[]>([]);

  @Input()
  minLength = 1;

  @Input()
  searchFunction: (input: string, added?: (string | number)[]) => Observable<IdDescriptionOption[]>;

  @Input()
  loadTagsFunction: (ids: any) => Observable<IdDescriptionOption[]>;

  @Input()
  placeholder: string;

  @Input()
  type: 'outline' | 'filled' = 'outline';

  @Input()
  customTags = false;

  separatorKeysCodes: number[];

  constructor() {
  }

  ngOnInit(): void {
    this.inputFormControl.valueChanges.pipe(
      debounceTime(300)
    ).subscribe(i => {
      this.loadList(i);
    });
    this.separatorKeysCodes = this.customTags ? [ENTER, COMMA] : [];
  }

  loadList(i?: string): void {
    if (! i){ i = this.inputFormControl.value as string; }
    const addedTags = this.tags$.getValue().map(t => t.id);
    this.searchFunction(i, addedTags).subscribe(options => {
      this.options$.next(options.filter(o => !addedTags.some(t => t === o.id)));
    });
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled$.next(isDisabled);
    if (isDisabled) {
      this.inputFormControl.disable();
    } else {
      this.inputFormControl.enable();
    }
  }

  writeValue(valueToWrite: string[]): void {
    const value: IdDescriptionOption[] = this.tags$.getValue();
    const missingIds = valueToWrite.filter(v => !value.find(o => o.id === v));
    if (missingIds.length > 0) {
      if (this.loadTagsFunction) {
        this.loadTagsFunction(missingIds).subscribe(loadedOptions => {
          if (!(loadedOptions?.length > 0)) {
            return;
          }
          this.tags$.next(value.concat(loadedOptions));
        });
      } else {
        this.tags$.next(value.concat(missingIds.map(id => {
          return {id, description: id};
        })));
      }
    }
  }

  remove(tag: IdDescriptionOption): void {
    const filtered = this.tags$.getValue().filter(t => t !== tag);
    this.tags$.next(filtered);
    this.updateControl();
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    const option: IdDescriptionOption = event.option.value;
    const updated = this.tags$.getValue().concat(option);
    this.tags$.next(updated);
    this.inputFormControl.patchValue('');
    this.updateControl();
  }

  private updateControl(): void {
    const value = this.tags$.getValue();
    this.onChange(value.map(v => v.id));
  }

  addInline($event: MatChipInputEvent): void {
    const value = this.inputFormControl.value;
    if (!this.customTags || !value) {
      return;
    }
    const updated = this.tags$.getValue().concat({id: value, description: value});
    this.tags$.next(updated);
    this.inputFormControl.patchValue('');
    this.updateControl();
  }
}
