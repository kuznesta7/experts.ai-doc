import {Component, OnInit} from '@angular/core';
import {Session, SessionService} from 'ng-src/app/services/session.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-login-verification',
  templateUrl: './login-verification.component.html',
})
export class LoginVerificationComponent implements OnInit {

  constructor(private sessionService: SessionService) { }

  isLoggedIn$: Observable<Session>;

  ngOnInit(): void {
    this.isLoggedIn$ = this.sessionService.getCurrentSession();
  }

}
