import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: '[app-overlay-loader]',
  templateUrl: './overlay-loader.component.html'
})
export class OverlayLoaderComponent implements OnInit {

  @Input()
  loading: boolean | null;

  @Input()
  fixed: boolean | null;

  constructor() {
  }

  ngOnInit(): void {
  }

}
