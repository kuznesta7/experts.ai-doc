import { Component, Input, OnInit } from '@angular/core';
import { UserExpertBaseDto } from 'ng-src/app/interfaces/user-expert-base-dto';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html'
})
export class MembersComponent implements OnInit {

  @Input()
  numberOfExpertsShown = 3;

  @Input()
  experts: UserExpertBaseDto[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }

}
