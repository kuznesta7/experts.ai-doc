import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-keywords-popover',
  templateUrl: './keywords-popover.component.html'
})
export class KeywordsPopoverComponent implements OnInit {

  constructor() {
  }

  @Input()
  keywords: string[];

  ngOnInit(): void {
  }

}
