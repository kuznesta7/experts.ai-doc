import {Component, Input} from '@angular/core';
import {ThesisPreview} from 'ng-src/app/interfaces/thesis-preview';
import {PathKey} from 'ng-src/app/shared/services/path.service';

@Component({
  selector: 'app-thesis-card',
  templateUrl: './thesis-card.component.html',
})
export class ThesisCardComponent {
  public readonly KEYWORDS = 3;

  @Input()
  thesisPreview: ThesisPreview;

  @Input()
  editable: boolean;

  pathKey: PathKey = 'thesisDetail';

  scrollParent(): void {
    window.parent.postMessage({
      'function': 'scrollUp',
    }, "*");
  }
}
