import {Component, Input, OnInit} from '@angular/core';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {Router} from '@angular/router';
import {ContactModalComponent} from 'ng-src/app/platform/components/common/contact-modal/contact-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {LinkType} from 'ng-src/app/enums/link-types';
import {PathKey} from 'ng-src/app/shared/services/path.service';
import {WidgetUtilService} from '../../../../widgets/utils/widget-util.service';

@Component({
  selector: 'app-organization-preview',
  templateUrl: './organization-preview.component.html',
})
export class OrganizationPreviewComponent implements OnInit {

  pathKey: PathKey;

  @Input()
  organization: OrganizationBaseDto;

  @Input()
  set linkType(type: LinkType) {
    switch (type) {
      case 'widget':
        this.pathKey = 'widget';
        return;
      case 'standard':
        this.pathKey = 'organizationDetail';
        return;
      default:
        this.pathKey = 'widget';
    }
  }

  constructor(private router: Router,
              private ngbModal: NgbModal,
              private widgetUtilService: WidgetUtilService) {
  }

  ngOnInit(): void {
  }

  checkOrganizationLicense(organization: OrganizationBaseDto): void {
    this.widgetUtilService.checkOrganizationLicense(organization);
  }

  openContactModal(organizationId: number | undefined): void {
    const ngbModalRef = this.ngbModal.open(ContactModalComponent, {size: 'lg'});
    ngbModalRef.componentInstance.organizationId = organizationId;
    ngbModalRef.componentInstance.contactType = 'organizationDetail';
  }

}
