import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EvidenceExpertPreview} from 'ng-src/app/interfaces/evidence-expert-preview';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {RecombeeService} from 'ng-src/app/services/recombee.service';
import {LinkType} from 'ng-src/app/enums/link-types';
import {PathKey} from 'ng-src/app/shared/services/path.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ContactModalComponent} from 'ng-src/app/platform/components/common/contact-modal/contact-modal.component';
import {GoogleAnalyticsService} from 'ng-src/app/services/google-analytics.service';
import {GoogleAnalyticsCategories} from 'ng-src/app/enums/google-analytics-categories';
import {DISPLAY_NUMBER_OF_TAGS} from 'ng-src/app/shared/constants/number-of-tags';
import {Router} from '@angular/router';
import {OrganizationLicenceService} from '../../../../services/organization-licence.service';
import {WidgetUtilService} from '../../../../widgets/utils/widget-util.service';

@Component({
  selector: 'app-expert-card',
  templateUrl: './expert-card.component.html',
})
export class ExpertCardComponent implements OnInit {
  pathKey: PathKey;
  orgPathKey: PathKey;

  initials = '';

  public readonly KEYWORDS = DISPLAY_NUMBER_OF_TAGS;

  @Input()
  expertPreview: EvidenceExpertPreview = {} as EvidenceExpertPreview;

  @Input()
  organizationId: any;

  @Input()
  sendContactEnabled = false;

  @Input()
  organizationSub: number[] = [];

  @Input()
  editable: boolean;

  @Input()
  displayActions = false;

  @Output()
  selected = new EventEmitter<string>();
  @Output() keyword$ = new EventEmitter<string>();
  selectedOrganization: OrganizationBaseDto = {} as OrganizationBaseDto;

  constructor(
    private ngbModal: NgbModal,
    private recombeeService: RecombeeService,
    private googleAnalyticsService: GoogleAnalyticsService,
    private widgetUtilService: WidgetUtilService,
    private router: Router,
    private organizationLicenseService: OrganizationLicenceService
  ) {
  }

  @Input()
  set linkType(type: LinkType) {
    switch (type) {
      case 'widget':
        this.pathKey = 'widgetExpertDetail';
        this.orgPathKey = 'widget';
        return;
      case 'standard':
        this.pathKey = 'expertDetail';
        this.orgPathKey = 'organizationDetail';
        return;
      default:
        this.pathKey = 'expertDetail';
        this.orgPathKey = 'organizationDetail';
    }
  }

  ngOnInit(): void {
    this.shovedOrganization();
    this.initials = this.getInitials(this.expertPreview.name);

    if (this.organizationId) {
      // if this organization is not visible then add it to the visible list
      const org = this.findThisOrganizationBaseDto();
      const thisVisible = this.expertPreview.visibleOrganizationBaseDtos.find((x: OrganizationBaseDto) => {
        return x.organizationId === this.organizationId;
      });
      if (! thisVisible && org) {
        this.expertPreview.visibleOrganizationBaseDtos.unshift(org);
      }
    }
  }

  getInitials(name: any): string {
    const hasTokens = name.indexOf(' ') !== -1;
    return name.substring(0, hasTokens ? 1 : 2) + (hasTokens ? name.charAt(name.lastIndexOf(' ') + 1) : '');
  }

  checkOrganizationLicense(organizationId: number | undefined): void {
    if (organizationId !== undefined) {
      if (this.router.url.indexOf('/widgets/') > -1) {
        this.organizationLicenseService.organizationHasLicense(organizationId).subscribe(result => {
          console.log('result is ', result);
          if (result) {
            console.log('navigate for ', '/widgets/organizations/' + organizationId);
            this.router.navigate(['/widgets/organizations/' + organizationId], {queryParams: {outcomeListMode: 'EXPERT'}, queryParamsHandling: 'merge'});
          } else {
            const ngbModalRef = this.ngbModal.open(ContactModalComponent, {size: 'lg'});
            ngbModalRef.componentInstance.organizationId = organizationId;
            ngbModalRef.componentInstance.contactType = 'organizationDetail';
          }
        });
      } else {
        this.router.navigate(['/organizations/' + organizationId]);
      }
    }
  }

  openContactModal(): void {
    const ngbModalRef = this.ngbModal.open(ContactModalComponent, {
      size: 'lg',
    });
    ngbModalRef.componentInstance.expert = this.expertPreview;
    ngbModalRef.componentInstance.contactType = this.pathKey;
    ngbModalRef.componentInstance.organizationId = this.organizationId;
    this.googleAnalyticsService.purchaseDetailClick(
      this.expertPreview.expertCode,
      GoogleAnalyticsCategories.EXPERT,
      this.expertPreview.index
    );
  }

  clickedExpert(expert: EvidenceExpertPreview): void {
    this.recombeeService.sendDetailView(expert.expertCode);
    this.googleAnalyticsService.view(
      expert.expertCode,
      GoogleAnalyticsCategories.EXPERT,
      expert.index
    );
    this.scrollParent();
  }

  orgNavigate(baseDto: OrganizationBaseDto): void {
    this.widgetUtilService.checkOrganizationLicense(baseDto);
  }

  findThisOrganizationBaseDto(): OrganizationBaseDto | undefined {
    return this.expertPreview.organizationBaseDtos.find((x: OrganizationBaseDto) => {
      return x.organizationId === this.organizationId;
    });
  }

  private shovedOrganization(): void {
    let selected: OrganizationBaseDto | undefined;
    if (this.organizationId) {
      selected = this.findThisOrganizationBaseDto();
    }
    if (!selected && this.organizationSub.length > 1) {
      selected = this.expertPreview.visibleOrganizationBaseDtos.find((x) => {
        return this.organizationSub.indexOf(x.organizationId) !== -1;
      });
    }
    if (!selected && this.expertPreview.visibleOrganizationBaseDtos.length > 0) {
      selected = this.expertPreview.visibleOrganizationBaseDtos[0];
    }
    if (!selected && this.expertPreview.organizationBaseDtos.length > 0) {
      selected = this.expertPreview.organizationBaseDtos[0];
    }
    if (selected) {
      this.selectedOrganization = selected;
    }
  }

  scrollParent(): void {
    window.parent.postMessage({
      'function': 'scrollUp',
    }, "*");
  }
}
