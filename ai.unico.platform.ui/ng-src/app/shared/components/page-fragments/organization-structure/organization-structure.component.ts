import {Component, Input, OnInit} from '@angular/core';
import {OrganizationStructureDto} from 'ng-src/app/services/organization-structure.service';
import {BehaviorSubject, combineLatest} from 'rxjs';
import {OrganizationUtilService} from 'ng-src/app/shared/services/organization-util.service';
import {Router} from '@angular/router';
import {OrganizationLicenceService} from 'ng-src/app/services/organization-licence.service';
import {ContactModalComponent} from 'ng-src/app/platform/components/common/contact-modal/contact-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {WidgetUtilService} from "../../../../widgets/utils/widget-util.service";
import {OrganizationBaseDto} from "../../../../interfaces/organization-base-dto";

@Component({
  selector: 'app-organization-structure',
  templateUrl: './organization-structure.component.html'
})
export class OrganizationStructureComponent implements OnInit {

  @Input()
  set currentOrganizationId(id: number | null) {
    this.organizationId$.next(id);
  }

  @Input()
  set structure(structure: OrganizationStructureDto[] | null) {
    if (!structure) {
      return;
    }
    const richStructure = structure.map(o => {
      const expanded$ = new BehaviorSubject<boolean>(false);
      const organizationUnits = o.organizationUnits.map(so => {
        return {
          ...so,
          organizationName: OrganizationUtilService.trimOrganizationName(so.organizationName, o.organizationName),
          organizationAbbrev: OrganizationUtilService.trimOrganizationName(so.organizationAbbrev, o.organizationAbbrev)
        };
      });
      return {...o, expanded$, organizationUnits};
    });
    this.organizationStructure$.next(richStructure);
  }

  organizationId$ = new BehaviorSubject<number | null>(null);
  organizationStructure$ = new BehaviorSubject<ExtendedOrganizationStructureDto[]>([]);

  constructor(private organizationLicenseService: OrganizationLicenceService,
              private router: Router,
              private ngbModal: NgbModal,
              private widgetUtilService: WidgetUtilService) {
  }

  ngOnInit(): void {
    combineLatest([this.organizationId$, this.organizationStructure$]).subscribe(([id, os]) => {
      if (!id) {
        return;
      }
      const organization = os.find(o => o.organizationId === id || this.searchUnits(id, o.organizationUnits));
      if (organization) {
        organization.expanded$.next(false);
      }
    });
  }

  toggleExpand(organization: ExtendedOrganizationStructureDto): void {
    if (!organization.organizationUnits.length) {
      return;
    }
    const value = !organization.expanded$.value;
    organization.expanded$.next(value);
  }

  private searchUnits(organizationId: number, units: OrganizationStructureDto[]): boolean {
    return (units && units.some(u => u.organizationId === organizationId || this.searchUnits(organizationId, u.organizationUnits)));
  }

  checkOrganizationLicense(organization: OrganizationBaseDto): void {
    this.widgetUtilService.checkOrganizationLicense(organization);
  }

  openContactModal(organizationId: number | undefined): void {
    const ngbModalRef = this.ngbModal.open(ContactModalComponent, {size: 'lg'});
    ngbModalRef.componentInstance.organizationId = organizationId;
    ngbModalRef.componentInstance.contactType = 'organizationDetail';
  }
}

interface ExtendedOrganizationStructureDto extends OrganizationStructureDto {
  expanded$: BehaviorSubject<boolean>;
}


