import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MessageBus} from 'ng-src/app/classes/event.class';
import {EvidenceExpertPreview} from 'ng-src/app/interfaces/evidence-expert-preview';
import {EventBusService} from 'ng-src/app/services/event-bus.service';
import {ExpertActionsService} from 'ng-src/app/services/expert-actions.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ClaimExpertsComponent} from 'ng-src/app/platform/components/common/claim-experts/claim-experts.component';

@Component({
  selector: 'app-expert-actions',
  templateUrl: './expert-actions.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExpertActionsComponent implements OnInit {
  @Input() expertPreview: EvidenceExpertPreview;
  @Input() organizationId: number;
  @Output() reloadExpertsList: EventEmitter<boolean> = new EventEmitter();

  private verifiedLabel = '';

  private retiredInOrganizationLabel = '';

  get retireInOrganizationLabel(): string {
    return this.retiredInOrganizationLabel;
  }

  get verifyLabel(): string {
    return this.verifiedLabel;
  }

  private retiredLabel = '';

  get retireLabel(): string {
    return this.retiredLabel;
  }

  constructor(
    private expertActions: ExpertActionsService,
    private route: ActivatedRoute,
    private eventBusService: EventBusService,
    private ngbModal: NgbModal,
  ) {
  }

  public ngOnInit(): void {
    this.setVerifiedLabel();
    this.setRetiredInOrganizationLabel();
  }

  public onRetiredInOrganizationClick(expert: EvidenceExpertPreview): void {
    const retiredStatus = !expert?.retiredInOrganization || false;

    this.expertActions
      .setExpertOrganizationRetiredStatus(retiredStatus, expert.expertCode, this.organizationId)
      .subscribe(() => {
        this.eventBusService.publish(
          new MessageBus('expertActionExecuted', true)
        );
      });
  }

  public removeFromOrganization(expert: EvidenceExpertPreview): void {
    const organizationId: any =
      this.route.snapshot.paramMap.get('organizationId');

    if (expert.expertId) {
      this.expertActions
        .removeExpertFromOrganization(organizationId, expert.expertId)
        .subscribe(() => {
          this.eventBusService.publish(
            new MessageBus('expertActionExecuted', true)
          );
        });
    } else {
      this.expertActions
        .removeUserFromOrganization(organizationId, expert.userId)
        .subscribe(() => {
          this.eventBusService.publish(
            new MessageBus('expertActionExecuted', true)
          );
        });
    }
  }

  public onVerifyClick(expert: EvidenceExpertPreview): void {
    if (!expert.expertId) {
      this.setUserVerifyStatus(expert);
    } else {
      this.setExpertVerifyStatus(expert);
    }
  }

  private setExpertVerifyStatus(expert: EvidenceExpertPreview): void {
    const verifyStatus: boolean = !expert?.verified || false;
    const organizationId: any =
      this.route.snapshot.paramMap.get('organizationId');

    this.expertActions
      .setExpertVerificationStatus(verifyStatus, organizationId, expert.expertId)
      .subscribe(() => {
        this.eventBusService.publish(
          new MessageBus('expertActionExecuted', true)
        );
      });
  }

  private setUserVerifyStatus(expert: EvidenceExpertPreview): void {
    const verifyStatus: boolean = !expert?.verified || false;
    const organizationId: any =
      this.route.snapshot.paramMap.get('organizationId');

    this.expertActions
      .setUserVerificationStatus(verifyStatus, organizationId, expert.userId)
      .subscribe(() => {
        this.eventBusService.publish(
          new MessageBus('expertActionExecuted', true)
        );
      });
  }

  public openMergeMenu(expert: EvidenceExpertPreview): void {
    const ngbModalRef = this.ngbModal.open(ClaimExpertsComponent, {size: 'xl'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.expert = expert;
    console.log('open merge experts');
  }

  private setVerifiedLabel(): void {
    this.verifiedLabel = this.expertPreview?.verified
      ? 'Cancel verification'
      : 'Verify';
  }

  private setRetiredInOrganizationLabel(): void {
    this.retiredInOrganizationLabel = this.expertPreview?.retiredInOrganization
      ? 'Make active in organization'
      : 'Make inactive in organization';
  }
}
