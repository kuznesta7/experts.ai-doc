import {Component, OnInit} from '@angular/core';
import {LoginService, SidebarMode} from 'ng-src/app/services/login.service';
import {BehaviorSubject} from 'rxjs';
import {debounceTime, filter, finalize, map, shareReplay, switchMap, tap} from 'rxjs/operators';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FlashService} from 'ng-src/app/shared/services/flash.service';
import {FormValidationService} from 'ng-src/app/shared/services/form-validation.service';
import {OauthService, SocialService} from 'ng-src/app/services/oauth.service';
import {localStorageStore} from 'ng-src/app/shared/utils/localStorageUtil';
import {Router} from '@angular/router';
import {ClaimProfileService, ClaimSuggestionProfile} from 'ng-src/app/services/claim-profile.service';
import {UserRegistrationDto} from 'ng-src/app/interfaces/user-registration-dto';
import {PathFragment} from 'ng-src/app/constants/path-fragment';
import {SessionService} from 'ng-src/app/services/session.service';

@Component({
  selector: 'app-login-sidebar',
  templateUrl: './login-sidebar.component.html'
})
export class LoginSidebarComponent implements OnInit {

  constructor(private loginService: LoginService,
              private flashService: FlashService,
              private formValidationService: FormValidationService,
              private oauthService: OauthService,
              private sessionService: SessionService,
              private router: Router,
              private claimProfileService: ClaimProfileService) {
  }

  sidebarOpen$ = this.loginService.getLoginSidebarState().pipe(shareReplay());
  sidebarModes = SidebarMode;
  sidebarMode$ = new BehaviorSubject<SidebarMode>(SidebarMode.LOG_IN);
  oauthInfo$ = this.oauthService.getOauthInfo().pipe(shareReplay());
  claimSuggestions$ = new BehaviorSubject<ClaimSuggestionProfileWithControls[]>([]);

  loginFormGroup = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });
  registerFormGroup = new FormGroup({
    fullName: new FormControl('', Validators.required),
    email: new FormControl('', Validators.email),
    password: new FormControl('', Validators.required)
  }, {updateOn: 'blur'});
  forgottenPasswordFormGroup = new FormGroup({
    email: new FormControl('', Validators.email)
  });
  socialServices = SocialService;
  loading$ = new BehaviorSubject(false);
  passwordResetSuccess$ = new BehaviorSubject(false);
  paths = PathFragment;

  ngOnInit(): void {
    this.sidebarOpen$.pipe(filter(m => !!m)).subscribe(m => this.sidebarMode$.next(m as SidebarMode));
    this.registerFormGroup.controls.fullName.valueChanges.pipe(
      debounceTime(300),
      switchMap(v => {
        if (!v) {
          return [];
        }
        return this.claimProfileService.findClaimSuggestions(v);
      }),
      map(suggestions => {
        return suggestions.slice(0, 5).map(s => {
          return {...s, selected$: new BehaviorSubject<boolean>(false)};
        });
      })
    ).subscribe(suggestions => this.claimSuggestions$.next(suggestions));
  }

  close(): void {
    this.loginService.closeLoginSidebar();
  }

  forceClose(): void {
    this.loginService.forceCloseLoginSidebar();
  }

  login(): void {
    this.formValidationService.validateForm(this.loginFormGroup);
    const value = this.loginFormGroup.value;
    this.loading$.next(true);
    this.loginService.doLogin(value.username, value.password).pipe(
      finalize(() => this.loading$.next(false))
    ).subscribe(() => {
      this.sessionService.getCurrentSession().subscribe(session => {
        this.flashService.ok('You have been successfully signed in');
        this.forceClose();
        this.loginService.redirectBasedOnSession(session);
      });
    }, () => this.flashService.error('Invalid username - password combination'));
  }

  loginWithSocial(service: SocialService): void {
    this.oauthInfo$.subscribe(info => {
      localStorageStore(OauthService.REDIRECT_URL_KEY, this.router.url);
      switch (service) {
        case SocialService.GOOGLE:
          window.location.replace(info.google);
          return;
        case SocialService.LINKED_IN:
          window.location.replace(info.linkedin);
          return;
      }
    });
  }

  register(): void {
    this.registerFormGroup.markAllAsTouched();
    if (this.registerFormGroup.invalid) {
      this.flashService.error('The form contains invalid values');
      return;
    }
    const userRegistration = this.registerFormGroup.value as UserRegistrationDto;
    const selected = this.claimSuggestions$.value.filter(cs => cs.selected$.value);
    const userIdsToClaim = selected.map(cs => cs.userId).filter(id => !!id);
    const expertIdsToClaim = selected.map(cs => cs.expertId).filter(id => !!id);
    const value = {username: userRegistration.email, password: userRegistration.password};
    this.loading$.next(true);
    this.loginService.doRegister(userRegistration, userIdsToClaim, expertIdsToClaim).pipe(
      tap({error: e => this.loading$.next(false)})
    ).subscribe(() => {
      this.loginFormGroup.patchValue(value);
      this.login();
    });
  }

  resetPassword(): void {
    this.formValidationService.validateForm(this.forgottenPasswordFormGroup);
    const value = this.forgottenPasswordFormGroup.value;
    this.loading$.next(true);
    this.loginService.applyForResetPassword(value.email).pipe(
      finalize(() => this.loading$.next(false))
    ).subscribe(() => {
      this.passwordResetSuccess$.next(true);
    });
  }
}

interface ClaimSuggestionProfileWithControls extends ClaimSuggestionProfile {
  selected$: BehaviorSubject<boolean>;
}
