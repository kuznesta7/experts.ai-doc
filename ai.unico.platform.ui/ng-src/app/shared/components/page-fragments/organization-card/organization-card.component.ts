import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {OrganizationDetailService, PublicOrganizationStatistics} from 'ng-src/app/services/organization-detail.service';
import {Observable} from 'rxjs';
import {shareReplay, tap} from 'rxjs/operators';
import {LinkType} from 'ng-src/app/enums/link-types';
import {PathKey} from '../../../services/path.service';
import {Params} from '@angular/router';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {OrganizationTypeService} from 'ng-src/app/services/organization-type.service';

@Component({
  selector: 'app-organization-card',
  templateUrl: './organization-card.component.html'
})
export class OrganizationCardComponent implements OnInit {

  @Input()
  set linkType(type: LinkType) {
    switch (type) {
      case 'widget':
        this.orgPathKey = 'widget';
        return;
      case 'standard':
        this.orgPathKey = 'organizationDetail';
        return;
      default:
        this.orgPathKey = 'organizationDetail';
    }
  }

  constructor(private organizationDetailService: OrganizationDetailService,
              private organizationTypeService: OrganizationTypeService,
              private widgetService: WidgetService) {
  }

  statistics$: Observable<PublicOrganizationStatistics>;
  orgPathKey: PathKey = 'organizationDetail';
  hasGdpr$: Observable<boolean>;

  @Input() organization: OrganizationBaseDto;
  @Input() index: number;
  @Input() organizationsLength: number;
  @Input() isParent = false;
  @Input() isMember = false;

  @Output() loading = new EventEmitter<boolean>();

  organizationType = '';

  listModeDetails: ListModeDetail[] = [
    {
      outcomeListMode: ListMode.EXPERT,
      title: 'Experts',
      countProperty: 'numberOfExperts',
      icon: 'users' as any
    },
    {
      outcomeListMode: ListMode.RESEARCH_OUTCOME,
      title: 'Outcomes',
      countProperty: 'numberOfOutcomes',
      icon: 'file-alt' as any
    }, {
      outcomeListMode: ListMode.RESEARCH_PROJECT,
      title: 'Projects',
      countProperty: 'numberOfProjects',
      icon: 'microscope' as any
    },
  ];

  resolveQueryParams(): Params | null {
    if (this.orgPathKey === 'widget'){
      return {outcomeListMode: 'EXPERT'};
    }
    return null;
  }

  clickedOrganization(organizationId: number): void {
    this.hasGdpr$ = this.widgetService.getGdpr(organizationId).pipe(shareReplay());
    this.hasGdpr$.subscribe(x => {
      if (!x) {
        window.location.reload();
      }
    });
    this.scrollParent();
  }

  ngOnInit(): void {
    console.log(this.organization);
    this.loading.emit(true);

    this.statistics$ = this.organizationDetailService.getOrganizationStatistics(this.organization.organizationId).pipe(tap(() => {
      if (this.index === this.organizationsLength - 1) {
        this.loading.emit(false);
      }
    }), shareReplay());

    this.organizationTypeService.getOrganizationTypes(this.organization.organizationId).subscribe((organizationTypeWrapper) => {
      this.organizationType = organizationTypeWrapper.selected.name;
    });
  }

  scrollParent(): void {
    window.parent.postMessage({
      function: 'scrollUp',
    }, '*');
  }
}

enum ListMode {
  EXPERT, RESEARCH_OUTCOME, RESEARCH_PROJECT
}

interface ListModeDetail {
  outcomeListMode: ListMode;
  title: string;
  countProperty: string;
  icon: any;
}
