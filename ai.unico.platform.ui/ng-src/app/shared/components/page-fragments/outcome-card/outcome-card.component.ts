import {Component, EventEmitter, Input, Output} from '@angular/core';
import {EvidenceItemPreview} from 'ng-src/app/interfaces/evidence-item-preview';
import {PathKey} from 'ng-src/app/shared/services/path.service';
import {LinkType} from 'ng-src/app/enums/link-types';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ContactModalComponent} from 'ng-src/app/platform/components/common/contact-modal/contact-modal.component';
import {DISPLAY_NUMBER_OF_TAGS} from 'ng-src/app/shared/constants/number-of-tags';
import {GoogleAnalyticsCategories} from '../../../../enums/google-analytics-categories';
import {RecombeeService} from '../../../../services/recombee.service';
import {GoogleAnalyticsService} from '../../../../services/google-analytics.service';

@Component({
  selector: 'app-outcome-card',
  templateUrl: './outcome-card.component.html',
})
export class OutcomeCardComponent {
  readonly KEYWORDS = DISPLAY_NUMBER_OF_TAGS;

  pathKey: PathKey;

  @Input()
  set linkType(type: LinkType) {
    switch (type) {
      case 'widget':
        this.pathKey = 'widgetOutcomeDetail';
        return;
      case 'standard':
        this.pathKey = 'outcomeDetail';
        return;
      default:
        this.pathKey = 'outcomeDetail';
    }
  }

  @Input()
  sendRequestEnabled = false;

  @Input()
  itemPreview: EvidenceItemPreview;

  @Input()
  ownerOrganizationId: any;

  @Input()
  editable: boolean;

  @Input()
  position: number;

  @Output() keyword$ = new EventEmitter<string>();

  constructor(private ngbModal: NgbModal,
              private recombeeService: RecombeeService,
              private googleAnalyticsService: GoogleAnalyticsService) {
  }

  clickedOutcome(outcome: EvidenceItemPreview): void {
    this.recombeeService.sendDetailView(outcome.itemCode);
    this.googleAnalyticsService.view(
      outcome.itemCode,
      GoogleAnalyticsCategories.RESEARCH_OUTCOME,
      this.position
    );
  }

  openContactModal(): void {
    const ngbModalRef = this.ngbModal.open(ContactModalComponent, {
      size: 'lg',
    });
    ngbModalRef.componentInstance.itemPreview = this.itemPreview;
    ngbModalRef.componentInstance.organizationId = this.ownerOrganizationId;
    ngbModalRef.componentInstance.contactType = this.pathKey;
  }

  scrollParent(): void {
    window.parent.postMessage({
      'function': 'scrollUp',
    }, "*");
  }
}
