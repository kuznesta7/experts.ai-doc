import {Component, Input, OnInit} from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';
import {OrganizationLicenceService} from 'ng-src/app/services/organization-licence.service';
import {ContactModalComponent} from 'ng-src/app/platform/components/common/contact-modal/contact-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UserExpertBaseDto} from 'ng-src/app/interfaces/user-expert-base-dto';
import {FlashService} from 'ng-src/app/shared/services/flash.service';
import {NotRegisteredExpertDTO} from "ng-src/app/interfaces/item-not-registered-expert";

@Component({
  selector: 'app-expert-links',
  templateUrl: './expert-links.component.html'
})
export class ExpertLinksComponent implements OnInit {

  @Input() experts: UserExpertBaseDto[];
  @Input() notRegisteredExperts: NotRegisteredExpertDTO[];

  constructor(private ngbModal: NgbModal,
              private organizationLicenseService: OrganizationLicenceService,
              private router: Router,
              private flashService: FlashService) { }

  ngOnInit(): void {
  }
  checkOrganizationLicenseForExpert(expertCode: string, organizationIds: number[]): void {
    if (organizationIds !== undefined && organizationIds.length > 0) {
      this.organizationLicenseService.organizationsHasLicenses(organizationIds).subscribe(result => {
        if (result) {
          // this.router.navigate(['/experts/' + expertCode]);
          const navigationExtras: NavigationExtras = {
            queryParams: {header: true, experts: true, projects: true, items: true}
          };
          this.router.navigate(['/widgets/experts/' + expertCode], navigationExtras);
        } else {
          this.openContactModal(organizationIds[0]);
        }
      });
    } else {
      this.flashService.warn('This expert is not assigned to any organization and has no licence. Please contact the administrator.');
    }
  }

  openContactModal(organizationId: number): void {
    const ngbModalRef = this.ngbModal.open(ContactModalComponent, {size: 'lg'});
    ngbModalRef.componentInstance.organizationId = organizationId;
    ngbModalRef.componentInstance.contactType = 'organizationDetail';
  }

}
