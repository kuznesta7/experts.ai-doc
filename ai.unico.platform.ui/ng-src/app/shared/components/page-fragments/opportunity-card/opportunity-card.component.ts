import {Component, EventEmitter, Input, Output} from '@angular/core';
import {LinkType} from 'ng-src/app/enums/link-types';
import {PathKey} from 'ng-src/app/shared/services/path.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OpportunityPreview} from 'ng-src/app/interfaces/opportunity-preview';
import {ContactModalComponent} from 'ng-src/app/platform/components/common/contact-modal/contact-modal.component';
import {RecombeeService} from 'ng-src/app/services/recombee.service';
import {GoogleAnalyticsService} from 'ng-src/app/services/google-analytics.service';
import {GoogleAnalyticsCategories} from 'ng-src/app/enums/google-analytics-categories';
import {DISPLAY_NUMBER_OF_TAGS} from 'ng-src/app/shared/constants/number-of-tags';

@Component({
  selector: 'app-opportunity-card',
  templateUrl: './opportunity-card.component.html',
})
export class OpportunityCardComponent {
  public readonly KEYWORDS = DISPLAY_NUMBER_OF_TAGS;

  @Output() opportunityType$ = new EventEmitter<number>();

  @Output() jobType$ = new EventEmitter<number>();

  @Output() keyword$ = new EventEmitter<string>();

  @Output() organization$ = new EventEmitter<number>();

  @Input()
  opportunityPreview: OpportunityPreview;

  @Input()
  editable: boolean;

  @Input()
  organizationId: number | undefined;

  @Input()
  set linkType(type: LinkType) {
    switch (type) {
      case 'widget':
        this.pathKey = 'widgetOpportunityDetail';
        return;
      default:
        this.pathKey = 'opportunityDetail';
    }
  }

  pathKey: PathKey;

  clickedOpportunity(opportunityPreview: OpportunityPreview): void {
    this.recombeeService.sendDetailView(
      opportunityPreview.opportunityId.toString()
    );
    this.googleAnalyticsService.view(
      opportunityPreview.opportunityId.toString(),
      GoogleAnalyticsCategories.OPPORTUNITY,
      this.opportunityPreview.index
    );
    this.scrollParent();
  }

  constructor(
    private ngbModal: NgbModal,
    private recombeeService: RecombeeService,
    private googleAnalyticsService: GoogleAnalyticsService
  ) {
  }

  openContactModal(): void {
    const ngbModalRef = this.ngbModal.open(ContactModalComponent, {
      size: 'lg',
    });
    let orgId = this.organizationId;
    if (this.opportunityPreview.organizationBaseDtos.length === 1) {
      orgId = this.opportunityPreview.organizationBaseDtos[0].organizationId;
    }
    ngbModalRef.componentInstance.opportunityPreview = this.opportunityPreview;
    ngbModalRef.componentInstance.organizationId = orgId;
    ngbModalRef.componentInstance.contactType = this.pathKey;
    this.googleAnalyticsService.purchaseDetailClick(
      this.opportunityPreview.opportunityId.toString(),
      GoogleAnalyticsCategories.OPPORTUNITY,
      this.opportunityPreview.index
    );
  }

  scrollParent(): void {
    window.parent.postMessage({
      'function': 'scrollUp',
    }, "*");
  }
}
