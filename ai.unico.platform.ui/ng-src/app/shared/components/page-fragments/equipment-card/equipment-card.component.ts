import {Component, Input} from '@angular/core';
import {LinkType} from 'ng-src/app/enums/link-types';
import {PathKey} from 'ng-src/app/shared/services/path.service';
import {EquipmentPreview} from 'ng-src/app/interfaces/equipment-preview';
import {DISPLAY_NUMBER_OF_TAGS} from 'ng-src/app/shared/constants/number-of-tags';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ContactModalComponent} from 'ng-src/app/platform/components/common/contact-modal/contact-modal.component';
import {GoogleAnalyticsService} from 'ng-src/app/services/google-analytics.service';
import {GoogleAnalyticsCategories} from 'ng-src/app/enums/google-analytics-categories';
import {MultilingualDto} from "ng-src/app/interfaces/multilingual-dto";

@Component({
  selector: 'app-equipment-card',
  templateUrl: './equipment-card.component.html',
})
export class EquipmentCardComponent {
  public readonly KEYWORDS = DISPLAY_NUMBER_OF_TAGS;

  @Input()
  equipment: MultilingualDto<EquipmentPreview>;

  @Input()
  editable: boolean;

  @Input()
  organizationId: number | undefined;

  @Input()
  set linkType(type: LinkType) {
    switch (type) {
      case 'widget':
        this.pathKey = 'widgetEquipmentDetail';
        return;
      default:
        this.pathKey = 'equipmentDetail';
    }
  }

  pathKey: PathKey;

  languageCodes: string[] = [];
  selectedLanguageCode: string;

  constructor(private modalService: NgbModal,
              private googleAnalyticsService: GoogleAnalyticsService,
  ) {
  }

  ngOnInit(): void {
    this.selectedLanguageCode = this.equipment.preview.languageCode;
    this.languageCodes = this.equipment.translations.map(t => t.languageCode);
  }

  onLanguageChange() {
    const selectedTranslation = this.equipment.translations.find(translation => translation.languageCode === this.selectedLanguageCode);
    if (selectedTranslation) {
      this.equipment.preview = selectedTranslation;
    }
  }

  openContactModal(): void {
    const ngbModalRef = this.modalService.open(ContactModalComponent, {size: 'lg'});
    let orgId = this.organizationId;
    if (orgId === undefined || this.equipment.preview.organizations.length === 1) {
      orgId = this.equipment.preview.organizations[0].organizationId;
    }
    ngbModalRef.componentInstance.equipmentPreview = this.equipment.preview;
    ngbModalRef.componentInstance.organizationId = orgId;
    ngbModalRef.componentInstance.contactType = this.pathKey;
    this.googleAnalyticsService.purchaseDetailClick(
      this.equipment.preview.equipmentCode.toString(),
      GoogleAnalyticsCategories.EQUIPMENT,
      -1
    );
  }

  scrollParent(): void {
    window.parent.postMessage({
      function: 'scrollUp',
    }, '*');
  }
}
