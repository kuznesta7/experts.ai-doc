import {Component, Input} from '@angular/core';
import {PathKey} from 'ng-src/app/shared/services/path.service';
import {LecturePreview} from 'ng-src/app/interfaces/lecture-preview';
import {LinkType} from 'ng-src/app/enums/link-types';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OrderModalComponent} from 'ng-src/app/platform/components/common/order-modal/order-modal.component';
import {DISPLAY_NUMBER_OF_TAGS} from 'ng-src/app/shared/constants/number-of-tags';

@Component({
  selector: 'app-lecture-card',
  templateUrl: './lecture-card.component.html',
})
export class LectureCardComponent {
  public readonly KEYWORDS = DISPLAY_NUMBER_OF_TAGS;

  @Input()
  lecturePreview: LecturePreview;

  @Input()
  editable: boolean;

  @Input()
  organizationId: number | undefined;

  @Input()
  set linkType(type: LinkType) {
    switch (type) {
      case 'widget':
        this.pathKey = 'widgetLectureDetail';
        return;
      default:
        this.pathKey = 'lectureDetail';
    }
  }

  pathKey: PathKey;

  constructor(private ngbModal: NgbModal) {
  }

  openOrderModal(): void {
    const ngbModalRef = this.ngbModal.open(OrderModalComponent, {size: 'lg'});
    ngbModalRef.componentInstance.lecture = this.lecturePreview;
    ngbModalRef.componentInstance.organizationId = this.organizationId;
  }

  scrollParent(): void {
    window.parent.postMessage({
      'function': 'scrollUp',
    }, "*");
  }
}
