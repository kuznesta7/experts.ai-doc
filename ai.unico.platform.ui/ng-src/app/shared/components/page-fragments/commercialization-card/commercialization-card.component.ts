import {Component, Input, OnInit} from '@angular/core';
import {CommercializationProjectPreview} from 'ng-src/app/interfaces/commercialization-project-preview';
import {PathKey} from 'ng-src/app/shared/services/path.service';
import {LinkType} from 'ng-src/app/enums/link-types';
import {DISPLAY_NUMBER_OF_TAGS} from 'ng-src/app/shared/constants/number-of-tags';

@Component({
  selector: 'app-commercialization-card',
  templateUrl: './commercialization-card.component.html',
})
export class CommercializationCardComponent implements OnInit {
  constructor() {
  }

  @Input()
  set linkType(type: LinkType) {
    switch (type) {
      case 'widget':
        this.pathKey = 'widgetCommercializationDetail';
        return;
      default:
        this.pathKey = 'commercializationDetail';
    }
  }

  @Input()
  project: CommercializationProjectPreview;
  SHOWN_KEYWORDS = DISPLAY_NUMBER_OF_TAGS;

  pathKey: PathKey = 'commercializationDetail';

  ngOnInit(): void {
  }

  scrollParent(): void {
    window.parent.postMessage({
      'function': 'scrollUp',
    }, "*");
  }
}
