import { Component, OnInit } from '@angular/core';
import { SessionService } from 'ng-src/app/services/session.service';
import { LoginService, SidebarMode } from 'ng-src/app/services/login.service';
import { FlashService } from 'ng-src/app/shared/services/flash.service';
import { SessionOrganizationService } from 'ng-src/app/services/session-organization.service';

@Component({
  selector: 'app-header-session-box',
  templateUrl: './header-session-box.component.html'
})
export class HeaderSessionBoxComponent implements OnInit {

  activeOrganization$ = this.sessionOrganizationService.getSessionOrganizationWithUpdates();

  session$ = this.sessionService.getCurrentSessionWithUpdates();

  constructor(private loginService: LoginService,
              private flashService: FlashService,
              private sessionService: SessionService,
              private sessionOrganizationService: SessionOrganizationService) {
  }

  ngOnInit(): void {
  }

  logOut(): void {
    this.loginService.doLogout().subscribe(() => {
      this.flashService.ok('You have been signed out');
    });
  }

  logIn(): void {
    this.loginService.openLoginSidebar(SidebarMode.LOG_IN);
  }

  register(): void {
    this.loginService.openLoginSidebar(SidebarMode.REGISTER);
  }
}
