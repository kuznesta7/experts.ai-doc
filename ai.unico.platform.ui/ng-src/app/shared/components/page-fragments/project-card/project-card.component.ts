import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProjectPreviewDto} from 'ng-src/app/interfaces/project-preview-dto';
import {PathKey} from 'ng-src/app/shared/services/path.service';
import {LinkType} from 'ng-src/app/enums/link-types';
import {ContactModalComponent} from 'ng-src/app/platform/components/common/contact-modal/contact-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DISPLAY_NUMBER_OF_TAGS} from 'ng-src/app/shared/constants/number-of-tags';
import {Router} from '@angular/router';
import {OrganizationLicenceService} from '../../../../services/organization-licence.service';
import {GoogleAnalyticsCategories} from '../../../../enums/google-analytics-categories';
import {RecombeeService} from '../../../../services/recombee.service';
import {GoogleAnalyticsService} from '../../../../services/google-analytics.service';

@Component({
  selector: 'app-project-card',
  templateUrl: './project-card.component.html',
})
export class ProjectCardComponent implements OnInit {
  public readonly SHOWN_ORGANIZATIONS = 1;

  public readonly SHOWN_KEYWORDS = DISPLAY_NUMBER_OF_TAGS;

  public readonly SHOWN_EXPERTS = DISPLAY_NUMBER_OF_TAGS;

  pathKey: PathKey = 'projectDetail';

  @Input() project: ProjectPreviewDto;

  @Input()
  sendRequestEnabled = false;

  @Input()
  ownerOrganizationId: any;

  @Input()
  position: number;

  @Input()
  set linkType(type: LinkType) {
    switch (type) {
      case 'widget':
        this.pathKey = 'widgetProjectDetail';
        return;
      case 'standard':
        this.pathKey = 'projectDetail';
        return;
      default:
        this.pathKey = 'projectDetail';
    }
  }

  @Output() keyword$ = new EventEmitter<string>();

  constructor(private ngbModal: NgbModal,
              private router: Router,
              private organizationLicenseService: OrganizationLicenceService,
              private recombeeService: RecombeeService,
              private googleAnalyticsService: GoogleAnalyticsService) {
  }

  ngOnInit(): void {
  }

  checkOrganizationLicense(organizationId: number | undefined): void {
    if (organizationId !== undefined) {
      if (this.router.url.indexOf('/widgets/') > -1) {
        this.organizationLicenseService.organizationHasLicense(organizationId).subscribe(result => {
          console.log('result is ', result);
          if (result) {
            console.log('navigate for ', '/widgets/organizations/' + organizationId);
            this.router.navigate(['/widgets/organizations/' + organizationId], {queryParams: {outcomeListMode: 'EXPERT'}, queryParamsHandling: 'merge'});
          } else {
            const ngbModalRef = this.ngbModal.open(ContactModalComponent, {size: 'lg'});
            ngbModalRef.componentInstance.organizationId = organizationId;
            ngbModalRef.componentInstance.contactType = 'organizationDetail';
          }
        });
      } else {
        this.router.navigate(['/organizations/' + organizationId]);
      }
    }
  }

  clickedProject(project: ProjectPreviewDto): void {
    this.recombeeService.sendDetailView(project.projectCode);
    this.googleAnalyticsService.view(
      project.projectCode,
      GoogleAnalyticsCategories.RESEARCH_PROJECT,
      this.position
    );
  }

  openContactModal(): void {
    const ngbModalRef = this.ngbModal.open(ContactModalComponent, {
      size: 'lg',
    });
    ngbModalRef.componentInstance.project = this.project;
    ngbModalRef.componentInstance.organizationId = this.ownerOrganizationId;
    ngbModalRef.componentInstance.contactType = this.pathKey;
  }

  scrollParent(): void {
    window.parent.postMessage({
      'function': 'scrollUp',
    }, "*");
  }
}
