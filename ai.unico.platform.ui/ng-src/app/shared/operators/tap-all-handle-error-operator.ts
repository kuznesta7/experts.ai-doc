import { EMPTY, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

export function tapAllHandleError(fn?: () => void) {
  return function <T>(source: Observable<T>) {
    return source.pipe(
      tap(fn),
      catchError(e => {
        console.error(e);
        if (fn) fn();
        return EMPTY;
      })
    );
  };
}
