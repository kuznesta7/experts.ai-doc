import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'baseDate'
})
export class BaseDatePipe implements PipeTransform {

  transform(date: Date | null): string {
    return moment(date).format('DD.MM.yyyy');
  }

}
