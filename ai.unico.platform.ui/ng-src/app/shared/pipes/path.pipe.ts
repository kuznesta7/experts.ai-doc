import { Pipe, PipeTransform } from '@angular/core';
import { PathKey, PathService } from 'ng-src/app/shared/services/path.service';

@Pipe({
  name: 'path'
})
export class PathPipe implements PipeTransform {

  constructor(private pathService: PathService) {
  }

  transform(value: PathKey, ...args: (string | number | null)[]): string {
    return this.pathService.getPathWithReplacedParams(value, ...args);
  }

}
