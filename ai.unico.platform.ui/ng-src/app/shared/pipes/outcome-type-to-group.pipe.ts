import { Pipe, PipeTransform } from '@angular/core';
import { ItemTypeService } from 'ng-src/app/services/item-type.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'outcomeTypeToGroup'
})
export class OutcomeTypeToGroupPipe implements PipeTransform {

  constructor(private itemTypeService: ItemTypeService) {
  }

  transform(itemTypeId: number): Observable<string | null> {
    return this.itemTypeService.findItemTypeGroups().pipe(
      map(groups => {
        const group = groups.find(g => g.itemTypeIds.some(t => t == itemTypeId));
        if (!group) {
          return null;
        }
        return group.itemTypeGroupTitle;
      })
    );
  }

}
