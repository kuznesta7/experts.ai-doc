import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'entries'
})
export class EntriesPipe implements PipeTransform {

  transform(value: { [key: string]: any } | { [key: number]: any } | null): { key: number | string, value: any }[] {
    if (!value) return [];
    return Object.entries(value).map(([key, value]) => {
      return {key, value}
    });
  }

}
