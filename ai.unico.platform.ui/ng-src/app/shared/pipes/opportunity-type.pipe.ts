import { Pipe, PipeTransform } from '@angular/core';
import { OpportunityCategoryService } from 'ng-src/app/services/opportunity-category.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'opportunityType'
})
export class OpportunityTypePipe implements PipeTransform {

  constructor(private opportunityCategoryService: OpportunityCategoryService) {
  }

  transform(opportunityTypeId: number): Observable<string | null> {
    return this.opportunityCategoryService.getOpportunityType().pipe(
      map(types => {
        const type = types.find(t => String(t.id) === String(opportunityTypeId));
        if (!type) {
          console.log('cant find: ' + opportunityTypeId);
          return null;
        }
        return type.name;
      })
    );
  }

}
