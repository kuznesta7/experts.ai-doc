import {Pipe, PipeTransform} from '@angular/core';
import {environment} from 'ng-src/environments/environment.prod';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';

@Pipe({
  name: 'commercializationImg'
})
export class CommercializationImgPipe implements PipeTransform {

  transform(projectId: number | string, checkSum: string, size?: 's' | 'S' | 'M' | 'L'): string {
    if (!projectId || checkSum === null) {
      return '';
    }
    const httpParams = createHttpParams({checkSum, size: size || 'M'});
    return environment.restUrl + 'commercialization/projects/' + projectId + '/image?' + httpParams.toString();
  }

}
