import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'ng-src/environments/environment.prod';
import { UserProfileImgType } from 'ng-src/app/services/user-profile.service';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';

@Pipe({
  name: 'userImg'
})
export class UserImgPipe implements PipeTransform {

  transform(userId: number | string | undefined, size?: 's' | 'S' | 'M' | 'L' | 'XL', type?: UserProfileImgType): string {
    if (!userId) {
      return '';
    }
    const params = createHttpParams({size: size || 'M', type});
    return environment.restUrl + 'profiles/users/' + userId + '/img?' + params.toString();
  }

}
