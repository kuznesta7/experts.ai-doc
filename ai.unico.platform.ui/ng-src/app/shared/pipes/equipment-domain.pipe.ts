import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { EquipmentCategoryService } from 'ng-src/app/services/equipment-category.service';

@Pipe({
  name: 'equipmentDomain'
})
export class EquipmentDomainPipe implements PipeTransform {

  constructor(private equipmentCategoryService: EquipmentCategoryService) {
  }

  transform(itemTypeId: number): Observable<string | null> {
    return this.equipmentCategoryService.getEquipmentDomain().pipe(
      map(types => {
        const type = types.find(t => String(t.id) === String(itemTypeId));
        if (!type) {
          return null;
        }
        return type.name;
      })
    );
  }

}
