import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'notEmpty'
})
export class NotEmptyPipe implements PipeTransform {

  transform(value: null | any[]): boolean {
    if (!value) {
      return false;
    }
    return value.length > 0;
  }

}
