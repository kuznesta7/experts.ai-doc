import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shortUrl'
})
export class ShortUrlPipe implements PipeTransform {

  transform(url: string): unknown {
    let matches;
    let output = '';
    const urls = /\w+:\/\/([\w|\.]+)/;

    matches = urls.exec(url);

    if (matches !== null) {
      output = matches[1];
    }

    return output;
  }

}
