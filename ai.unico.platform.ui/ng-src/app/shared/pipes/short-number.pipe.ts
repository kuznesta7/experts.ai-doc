import { Pipe, PipeTransform } from '@angular/core';
import { NumberFormatService } from 'ng-src/app/shared/services/number-format.service';

@Pipe({
  name: 'shortNumber'
})
export class ShortNumberPipe implements PipeTransform {


  constructor(private numberFormatService: NumberFormatService) {
  }

  transform(value: number): string | null {
    return this.numberFormatService.shortenNumber(value);
  }

}
