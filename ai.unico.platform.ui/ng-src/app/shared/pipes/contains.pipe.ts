import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contains'
})
export class ContainsPipe implements PipeTransform {

  transform(arr: any[] | any, value: any): boolean {
    if (!arr) {
      return false;
    }
    return arr.indexOf(value) !== -1;
  }

}
