import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getProperty'
})
export class GetPropertyPipe implements PipeTransform {

  transform(value: any | null, propertyName: string | number): any | null {
    if (!value) return null;
    return value[propertyName];
  }

}
