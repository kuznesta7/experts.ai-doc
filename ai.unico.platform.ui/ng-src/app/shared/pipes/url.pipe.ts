import { Pipe, PipeTransform } from '@angular/core';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Pipe({
  name: 'url'
})
export class UrlPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {
  }

  transform(baseUrl: string, params?: any): SafeUrl {
    if (!params) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(baseUrl);
    }
    const httpParams = createHttpParams(params);
    return this.sanitizer.bypassSecurityTrustResourceUrl(baseUrl + '?' + httpParams.toString());
  }

}
