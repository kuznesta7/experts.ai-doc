import { Pipe, PipeTransform } from '@angular/core';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { environment } from 'ng-src/environments/environment.prod';
import { OrganizationImgType } from 'ng-src/app/services/organization-detail.service';

@Pipe({
  name: 'organizationImg'
})
export class OrganizationImgPipe implements PipeTransform {

  transform(organizationId: number | string, size?: 's' | 'S' | 'M' | 'L' | 'XL', type?: OrganizationImgType): string {
    if (!organizationId) {
      return '';
    }
    const params = createHttpParams({size: size || 'M', type});
    return environment.restUrl + 'organizations/' + organizationId + '/img?' + params.toString();
  }

}
