import { Pipe, PipeTransform } from '@angular/core';
import { round } from 'ng-src/app/shared/utils/mathUtil';

@Pipe({
  name: 'round'
})
export class RoundPipe implements PipeTransform {

  transform(value: any, decimal: number = 2): any {
    if (!value) return value;
    const numValue = parseFloat(value);
    if (isNaN(numValue)) return value;
    return round(numValue, decimal);
  }

}
