import { Pipe, PipeTransform } from '@angular/core';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

@Pipe({
  name: 'fileIcon'
})
export class FileIconPipe implements PipeTransform {

  transform(fileType: string): IconProp {
    fileType = fileType ? fileType.toUpperCase() : '';
    switch (fileType) {
      case 'DOC':
      case 'DOCX':
      case 'DOCM':
      case 'DOT':
      case 'DOTM':
      case 'DOTX':
      case 'ODT':
        return 'file-word';
      case 'CSV':
      case 'DBF':
      case 'DIF':
      case 'ODS':
      case 'XLS':
      case 'XLSB':
      case 'XLSM':
      case 'XLSX':
      case 'XLT':
      case 'XLTM':
      case 'XLTX':
      case 'XLW':
        return 'file-excel';
      case 'ODP':
      case 'POT':
      case 'POTX':
      case 'POTM':
      case 'PPA':
      case 'PPAM':
      case 'PPS':
      case 'PPSM':
      case 'PPSX':
      case 'PPT':
      case 'PPTM':
      case 'PPTX':
        return 'file-powerpoint';
      case 'PDF':
        return 'file-pdf';
      case 'JPG':
      case 'JPEG':
      case 'GIF':
      case 'PNG':
      case 'BMP':
      case 'TIFF':
      case 'TIF':
      case 'SVG':
      case 'PSD':
        return 'file-image';
      case 'TXT':
        return 'file-alt';
      case 'MP3':
      case 'WAV':
      case 'WMA':
      case 'OGG':
      case 'AAC':
        return 'file-audio';
      case 'AVI':
      case 'MOV':
      case 'MKV':
      case 'WMV':
      case 'FLV':
      case 'MPEG':
      case 'MP4':
        return 'file-video';
      case 'ZIP':
      case 'RAR':
      case '7Z':
        return 'file-archive';
      default:
        return 'file';
    }
  }

}
