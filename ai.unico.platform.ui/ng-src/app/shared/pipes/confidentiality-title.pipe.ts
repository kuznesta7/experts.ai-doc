import { Pipe, PipeTransform } from '@angular/core';
import { Confidentiality } from 'ng-src/app/interfaces/confidetiality';

@Pipe({
  name: 'confidentialityTitle'
})
export class ConfidentialityTitlePipe implements PipeTransform {

  transform(confidentiality: Confidentiality): string | null {
    switch (confidentiality) {
      case Confidentiality.CONFIDENTIAL:
        return 'Confidential';
      case Confidentiality.PUBLIC:
        return 'Public';
      case Confidentiality.SECRET:
        return 'Secret'
    }
    return null;
  }

}
