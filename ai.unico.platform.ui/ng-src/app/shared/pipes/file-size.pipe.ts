import { Pipe, PipeTransform } from '@angular/core';
import { round } from 'ng-src/app/shared/utils/mathUtil';

@Pipe({
  name: 'fileSize'
})
export class FileSizePipe implements PipeTransform {

  transform(value: number): string {
    if (!value) {
      return '0B';
    }
    const endings = ['', 'k', 'M', 'G', 'T'];
    const divider = 1024;
    const resultEnding = endings.find(e => {
      const shortenValue = value / divider;
      if (shortenValue < 1) {
        return true;
      }
      value = shortenValue;
      return false;
    });

    return round(value, 2) + (resultEnding || '') + 'B';
  }

}
