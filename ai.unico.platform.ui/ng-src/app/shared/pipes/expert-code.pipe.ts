import { Pipe, PipeTransform } from '@angular/core';
import { PlatformDataSource } from 'ng-src/app/enums/platform-data-source';

@Pipe({
  name: 'expertCode'
})
export class ExpertCodePipe implements PipeTransform {

  transform(value: any): string {
    if (!value) {
      return '';
    }
    if (value.userId) {
      return PlatformDataSource.STORE + value.userId;
    } else if (value.expertId) {
      return PlatformDataSource.DWH + value.expertId;
    }
    return '';
  }

}
