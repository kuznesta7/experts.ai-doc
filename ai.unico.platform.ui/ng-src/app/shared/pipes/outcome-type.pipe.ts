import { Pipe, PipeTransform } from '@angular/core';
import { ItemTypeService } from 'ng-src/app/services/item-type.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'outcomeType'
})
export class OutcomeTypePipe implements PipeTransform {

  constructor(private itemTypeService: ItemTypeService) {
  }

  transform(itemTypeId: number): Observable<string | null> {
    return this.itemTypeService.findDWHItemTypes().pipe(
      map(types => {
        const type = types.find(t => String(t.typeId) === String(itemTypeId));
        if (!type) {
          return null;
        }
        return type.typeNameEn;
      })
    );
  }

}
