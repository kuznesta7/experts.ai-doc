import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'ng-src/environments/environment';

@Pipe({
  name: 'assets'
})
export class AssetsPipe implements PipeTransform {

  transform(value: string): string {
    return environment.assetsUrl + value;
  }

}
