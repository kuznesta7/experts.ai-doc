import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'confidentialityColor'
})
export class ConfidentialityColorPipe implements PipeTransform {

  transform(value: string): string {
    if (value === 'CONFIDENTIAL') {
      return 'purple';
    } else if (value === 'PUBLIC') {
      return 'green';
    }
    return 'red';
  }

}
