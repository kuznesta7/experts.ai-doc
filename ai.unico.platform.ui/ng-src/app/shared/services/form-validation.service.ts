import { Injectable } from '@angular/core';
import { FlashService } from 'ng-src/app/shared/services/flash.service';
import { AbstractControl } from '@angular/forms';
import { getFormErrors } from 'ng-src/app/shared/utils/formValidationUtil';

@Injectable({
  providedIn: 'root'
})
export class FormValidationService {

  constructor(private flashService: FlashService) {
  }

  validateForm(formGroup: AbstractControl): void {
    formGroup.markAllAsTouched();
    if (formGroup.invalid) {
      this.flashService.error('The form is not valid');
      const formErrors = getFormErrors(formGroup);
      throw new Error('Form has couldn\'t be submitted because it contains following errors: ' + Object.keys(formErrors).join(','));
    }
  }

}
