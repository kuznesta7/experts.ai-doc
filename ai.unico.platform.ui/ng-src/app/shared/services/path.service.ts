import {Injectable} from '@angular/core';
import {UrlParams} from 'ng-src/app/constants/url-params';
import {PathFragment} from 'ng-src/app/constants/path-fragment';

@Injectable({
  providedIn: 'root'
})
export class PathService {

  private readonly PATH_VARIABLE_PREFIX = ':';

  private readonly Paths = new Map<PathKey, Path>([
    ['expertDetail', {fragments: [PathFragment.EXPERTS, this.param(UrlParams.EXPERT_CODE)]}],
    ['organizationExperts', {fragments: [PathFragment.EVIDENCE, PathFragment.ORGANIZATIONS, this.param(UrlParams.ORGANIZATION_ID), PathFragment.EXPERTS]}],
    ['organizationOutcomes', {fragments: [PathFragment.EVIDENCE, PathFragment.ORGANIZATIONS, this.param(UrlParams.ORGANIZATION_ID), PathFragment.OUTCOMES]}],
    ['organizationProjects', {fragments: [PathFragment.EVIDENCE, PathFragment.ORGANIZATIONS, this.param(UrlParams.ORGANIZATION_ID), PathFragment.PROJECTS]}],
    ['organizationDashboard', {fragments: [PathFragment.EVIDENCE, PathFragment.ORGANIZATIONS, this.param(UrlParams.ORGANIZATION_ID)]}],
    ['organizationDetail', {fragments: [PathFragment.ORGANIZATIONS, this.param(UrlParams.ORGANIZATION_ID)]}],
    ['organizationSettings', {fragments: [PathFragment.EVIDENCE, PathFragment.ORGANIZATIONS, this.param(UrlParams.ORGANIZATION_ID), PathFragment.SETTINGS, PathFragment.ORGANIZATION_LICENSES]}],
    ['organizationWidgets', {fragments: [PathFragment.EVIDENCE, PathFragment.ORGANIZATIONS, this.param(UrlParams.ORGANIZATION_ID), PathFragment.SETTINGS, PathFragment.WIDGETS]}],
    ['organizationPublicProfiles', {fragments: [PathFragment.EVIDENCE, PathFragment.ORGANIZATIONS, this.param(UrlParams.ORGANIZATION_ID), PathFragment.SETTINGS, PathFragment.ORGANIZATION_PUBLIC_PROFILES]}],
    ['outcomeDetail', {fragments: [PathFragment.OUTCOMES, this.param(UrlParams.OUTCOME_CODE)]}],
    ['projectDetail', {fragments: [PathFragment.PROJECTS, this.param(UrlParams.PROJECT_CODE)]}],
    ['commercializationDetail', {fragments: [PathFragment.COMMERCIALIZATION, this.param(UrlParams.COMMERCIALIZATION_ID)]}],
    ['thesisDetail', {fragments: [PathFragment.THESIS, this.param(UrlParams.THESIS_CODE)]}],
    ['lectureDetail', {fragments: [PathFragment.LECTURE, this.param(UrlParams.LECTURE_CODE)]}],
    ['login', {fragments: [PathFragment.LOGIN]}],
    ['equipmentDetail', {fragments: [PathFragment.EQUIPMENT, this.param(UrlParams.EQUIPMENT_CODE)]}],
    ['opportunityDetail', {fragments: [PathFragment.OPPORTUNITY, this.param(UrlParams.OPPORTUNITY_ID)]}],
    ['exploreLectures', {fragments: [PathFragment.EXPLORE, PathFragment.LECTURE]}],
    ['exploreExperts', {fragments: [PathFragment.EXPLORE, PathFragment.EXPERTS]}],
    ['exploreCommercialization', {fragments: [PathFragment.EXPLORE, PathFragment.COMMERCIALIZATION]}],
    ['exploreEquipment', {fragments: [PathFragment.EXPLORE, PathFragment.EQUIPMENT]}],
    ['exploreOrganizations', {fragments: [PathFragment.EXPLORE, PathFragment.ORGANIZATIONS]}],
    ['exploreThesis', {fragments: [PathFragment.EXPLORE, PathFragment.THESIS]}],
    ['userSettings', {fragments: [PathFragment.MY_ACCOUNT, PathFragment.SETTINGS]}],
    ['userSettingsPassword', {fragments: [PathFragment.MY_ACCOUNT, PathFragment.SETTINGS, PathFragment.PASSWORD]}],
    ['userSettingsConsents', {fragments: [PathFragment.MY_ACCOUNT, PathFragment.SETTINGS, PathFragment.CONSENTS]}],
    ['userSettingsClaimProfiles', {fragments: [PathFragment.MY_ACCOUNT, PathFragment.SETTINGS, PathFragment.CLAIM_PROFILES]}],
    ['userSearchHistory', {fragments: [PathFragment.MY_ACCOUNT, PathFragment.SEARCH_HISTORY]}],
    ['userFollowings', {fragments: [PathFragment.MY_ACCOUNT, PathFragment.FOLLOWINGS]}],
    ['platformSettings', {fragments: [PathFragment.PLATFORM_SETTINGS]}],
    ['platformSettingsIndex', {fragments: [PathFragment.PLATFORM_SETTINGS, PathFragment.REINDEX]}],
    ['platformSettingsOrganizations', {fragments: [PathFragment.PLATFORM_SETTINGS, PathFragment.ORGANIZATIONS]}],
    ['platformSettingsUsers', {fragments: [PathFragment.PLATFORM_SETTINGS, PathFragment.USERS]}],
    ['widget', {fragments: [PathFragment.WIDGETS, PathFragment.ORGANIZATIONS, this.param(UrlParams.ORGANIZATION_ID)]}],
    ['widgetExpertDetail', {fragments: [PathFragment.WIDGETS, PathFragment.EXPERTS, this.param(UrlParams.EXPERT_CODE)]}],
    ['widgetProjectDetail', {fragments: [PathFragment.WIDGETS, PathFragment.PROJECTS, this.param(UrlParams.PROJECT_CODE)]}],
    ['widgetOutcomeDetail', {fragments: [PathFragment.WIDGETS, PathFragment.OUTCOMES, this.param(UrlParams.OUTCOME_CODE)]}],
    ['widgetCommercializationDetail', {fragments: [PathFragment.WIDGETS, PathFragment.COMMERCIALIZATION, this.param(UrlParams.COMMERCIALIZATION_ID)]}],
    ['widgetLectureDetail', {fragments: [PathFragment.WIDGETS, PathFragment.LECTURE, this.param(UrlParams.LECTURE_CODE)]}],
    ['widgetEquipmentDetail', {fragments: [PathFragment.WIDGETS, PathFragment.EQUIPMENT, this.param(UrlParams.EQUIPMENT_CODE)]}],
    ['widgetOpportunityDetail', {fragments: [PathFragment.WIDGETS, PathFragment.OPPORTUNITY, this.param(UrlParams.OPPORTUNITY_ID)]}],
    ['widgetOrganizationDetail', {fragments: [PathFragment.WIDGETS, PathFragment.ORGANIZATIONS, this.param(UrlParams.ORGANIZATION_ID)]}],
    ['widgetOrganizationChildren', {fragments: [PathFragment.WIDGETS, PathFragment.INNER_ORGANIZATIONS, this.param(UrlParams.ORGANIZATION_ID)]}],
  ]);

  getPathWithReplacedParams(key: PathKey, ...params: (string | number | null)[]): string {
    const path = this.Paths.get(key);
    if (!path) {
      throw new Error('Path with ' + key + ' doesn\'t exist');
    }
    let i = 0;
    const updatedFragments = path.fragments.map(fragment => {
      if (!fragment.startsWith(this.PATH_VARIABLE_PREFIX)) {
        return fragment;
      }
      if (params[i] == null) {
        console.warn('Missing param ' + fragment + ' for path with key ' + key);
        return fragment;
      }
      return params[i++];
    });
    if (i !== params.length) {
      const requiredParamsJoined = path.fragments.filter(f => f.startsWith(this.PATH_VARIABLE_PREFIX)).join(', ');
      const paramsJoined = params.join(', ');
      console.warn('Invalid number of params for path with key "' + key + '". Required "' + requiredParamsJoined + '", but found ' + paramsJoined);
    }
    return '/' + updatedFragments.join('/');
  }

  private param(paramName: string): string {
    return this.PATH_VARIABLE_PREFIX + paramName;
  }
}

export type PathKey =
  'expertDetail'
  | 'organizationExperts'
  | 'organizationOutcomes'
  | 'organizationProjects'
  | 'exploreExperts'
  | 'organizationDashboard'
  | 'organizationDetail'
  | 'organizationSettings'
  | 'organizationWidgets'
  | 'organizationPublicProfiles'
  | 'outcomeDetail'
  | 'projectDetail'
  | 'commercializationDetail'
  | 'thesisDetail'
  | 'lectureDetail'
  | 'login'
  | 'equipmentDetail'
  | 'opportunityDetail'
  | 'exploreLectures'
  | 'exploreEquipment'
  | 'exploreCommercialization'
  | 'exploreOrganizations'
  | 'exploreThesis'
  | 'userSettings'
  | 'userSettingsPassword'
  | 'userSettingsConsents'
  | 'userSettingsClaimProfiles'
  | 'userFollowings'
  | 'userSearchHistory'
  | 'platformSettings'
  | 'platformSettingsOrganizations'
  | 'platformSettingsUsers'
  | 'platformSettingsIndex'
  | 'widgetExpertDetail'
  | 'widget'
  | 'widgetProjectDetail'
  | 'widgetCommercializationDetail'
  | 'widgetOutcomeDetail'
  | 'widgetLectureDetail'
  | 'widgetEquipmentDetail'
  | 'widgetOpportunityDetail'
  | 'widgetOrganizationDetail'
  | 'widgetOrganizationChildren';

export interface Path {
  fragments: string[];
}
