import {Injectable} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {FormGroup} from '@angular/forms';
import {arrayContains} from 'ng-src/app/shared/utils/arraysUtil';
import {objectsEqual} from 'ng-src/app/shared/utils/object-util';
import {Observable} from 'rxjs';
import {distinctUntilChanged, filter, map, shareReplay} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QueryParamService {

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  private readonly ARRAY_SEPARATOR = ',';
  private readonly DATE_FORMAT = 'YYYY-MM-DD';

  updateUrlByFilter(filterObject: any, options?: any): void {
    const mixedOptions = {skipLocationChange: false, ...options};
    const updatedFilter = this.createQueryParamsObject(filterObject);
    this.router.navigate([], {...mixedOptions, queryParams: updatedFilter});
  }

  createQueryParamsObject(filterObject: any): any {
    const updatedFilter = {...this.activatedRoute.snapshot.queryParams};
    Object.entries(filterObject).forEach(([k, v]) => {
      if (k !== 'page') {
        updatedFilter['page'] = 1;
      }
      if (!v) {
        delete updatedFilter[k];
        return;
      }
      if (typeof v === 'boolean') {
        updatedFilter[k] = 'true';
      }
      if (Array.isArray(v)) {
        if (v.length === 0) {
          delete updatedFilter[k];
          return;
        }
        updatedFilter[k] = v.join(this.ARRAY_SEPARATOR);
        return;
      }
      if (v instanceof Date) {
        updatedFilter[k] = this.dateToString(v);
        return;
      }
      if (typeof v === 'string' || typeof v === 'number') {
        updatedFilter[k] = String(v);
        return;
      }
    });
    return updatedFilter;
  }

  getFilter<T>(defaultFilter: T, options?: UrlParseOptions): T {
    const updatedFilter = {...defaultFilter} as any;
    const queryParams = this.activatedRoute.snapshot.queryParams;
    Object.keys(updatedFilter).forEach(k => {
      if (typeof updatedFilter[k] === 'boolean') {
        updatedFilter[k] = queryParams.hasOwnProperty(k);
        return;
      }
      if (!queryParams.hasOwnProperty(k)) {
        return;
      }
      const queryParam = queryParams[k];
      if (Array.isArray(updatedFilter[k])) {
        const strings = queryParam.split(this.ARRAY_SEPARATOR);
        updatedFilter[k] = strings.map((s: string) => this.parseParam(s, k, options));
      } else {
        updatedFilter[k] = this.parseParam(queryParam, k, options);
      }
    });
    return updatedFilter;
  }

  handleFilterFormGroup(formGroup: FormGroup, options?: UrlParseOptions): void {
    const initialValue = formGroup.getRawValue();
    const filterFromUrl = this.getFilter(initialValue, options);
    if (!objectsEqual(initialValue, filterFromUrl)) {
      formGroup.patchValue(filterFromUrl);
    }
    formGroup.valueChanges.subscribe(v => {
      this.updateUrlByFilter(v);
    });
  }

  parseDate(dateString: string): Date | null {
    const date = moment(dateString);
    if (!date.isValid()) {
      return null;
    }
    return date.toDate();
  }

  dateToString(date: Date): string {
    return moment(date).format(this.DATE_FORMAT);
  }

  getParam(paramName: string): Observable<string> {
    return this.activatedRoute.queryParams.pipe(
      map(params => params[paramName]),
      filter(v => !!v),
      distinctUntilChanged(),
      shareReplay()
    );
  }

  private parseParam(paramToParse: string, fieldName: string, options?: UrlParseOptions): any {
    if (arrayContains(options?.dateFields, fieldName)) {
      return this.parseDate(paramToParse);
    }

    if (arrayContains(options?.numericFields, fieldName)) {
      return parseInt(paramToParse, 10);
    }

    return paramToParse;
  }

}

export interface UrlParseOptions {
  dateFields?: string[];
  numericFields?: string[];
}
