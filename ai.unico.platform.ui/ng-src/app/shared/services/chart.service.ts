import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ChartService {

  static readonly COLORS = ['#3747f2', '#ff9131', '#00c96a', '#a565d6'];

}
