import {Inject, Injectable, Renderer2, RendererFactory2} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {first, tap} from 'rxjs/operators';
import {DOCUMENT} from '@angular/common';
import {environment} from 'ng-src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UiThemeService {

  private renderer: Renderer2;
  private head: HTMLElement;
  private linkId = 'scheme-styles';
  private oldLinkEl: HTMLElement;
  private currentLinkEl: HTMLElement;

  private readonly DEFAULT_THEME = 'theme-default';

  constructor(private rendererFactory: RendererFactory2,
              @Inject(DOCUMENT) private document: Document) {
  }

  loadTheme(theme?: string): Observable<any> {
    this.head = document.head;
    this.renderer = this.rendererFactory.createRenderer(null, null);
    return this.loadCss((environment.static + (theme || this.DEFAULT_THEME)) + '.css').pipe(
      tap(() => {
        if (this.oldLinkEl) {
          this.renderer.removeChild(this.head, this.oldLinkEl);
        }
      }),
    );
  }

  getDefaultTheme(): string {
    return this.DEFAULT_THEME;
  }

  isSessionStorageAvailable(): boolean {
    try {
      return typeof window.sessionStorage !== 'undefined';
    } catch (e) {
      return false;
    }
  }

  private loadCss(filename: string): Observable<string> {
    const resolve$ = new Subject<string>();

    this.oldLinkEl = this.currentLinkEl;
    this.currentLinkEl = this.renderer.createElement('link');

    this.renderer.setAttribute(this.currentLinkEl, 'rel', 'stylesheet');
    this.renderer.setAttribute(this.currentLinkEl, 'type', 'text/css');
    this.renderer.setAttribute(this.currentLinkEl, 'href', filename);
    this.renderer.setAttribute(this.currentLinkEl, 'id', this.linkId);
    this.renderer.setProperty(this.currentLinkEl, 'onload', () => resolve$.next(this.linkId));
    this.renderer.setProperty(this.currentLinkEl, 'onerror', (e: Error) => resolve$.error(e));
    this.renderer.appendChild(this.head, this.currentLinkEl);
    return resolve$.pipe(first());
  }
}
