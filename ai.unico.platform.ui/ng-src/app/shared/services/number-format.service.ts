import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NumberFormatService {

  constructor() {
  }

  shortenNumber(value: number): string | null {
    if (value == null) return null;
    if (value >= 1000000000) {
      return (Math.round(value / 100000000) / 10) + 'B';
    } else if (value >= 1000000) {
      return (Math.round(value / 100000) / 10) + 'M';
    } else if (value >= 1000) {
      return (Math.round(value / 100) / 10) + 'K';
    } else {
      return Math.round(value).toString();
    }
  }
}
