import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SingleRequestHelperService {

  constructor(private httpClient: HttpClient) {
  }

  private terminators: { [key: string]: Subject<void> } = {};

  get<T>(url: string, options?: HttpOptions): Observable<T> {
    if (!this.terminators[url]) {
      this.terminators[url] = new Subject<void>();
    }
    this.terminators[url].next();
    return this.httpClient.get<T>(url, options).pipe(
      takeUntil(this.terminators[url])
    );
  }
}

interface HttpOptions {
  headers?: HttpHeaders;
  params?: HttpParams;
  reportProgress?: boolean;
  withCredentials?: boolean;
}
