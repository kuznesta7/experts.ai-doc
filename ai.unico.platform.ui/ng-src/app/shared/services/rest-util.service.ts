import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class RestUtilService {

  constructor() {
  }

  static formatDateForRest(date: Date): string | null {
    if (!date) {
      return date;
    }
    return moment(date).format('YYYY-MM-DD');
  }
}
