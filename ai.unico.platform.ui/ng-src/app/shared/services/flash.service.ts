import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class FlashService {

  defaultCloseTitle = 'Close';
  defaultSettings = {duration: 8000};

  constructor(private matSnackBar: MatSnackBar) {
  }

  default(text: string, extraSettings?: any): MatSnackBarRef<SimpleSnackBar> {
    if (!extraSettings) extraSettings = {};
    return this.matSnackBar.open(text, this.defaultCloseTitle, {...this.defaultSettings, ...extraSettings});
  }

  ok(text: string, extraSettings?: any): MatSnackBarRef<SimpleSnackBar> {
    return this.default(text, {panelClass: ['snackbar-success'], ...extraSettings});
  }

  error(text: string, extraSettings?: any): MatSnackBarRef<SimpleSnackBar> {
    return this.default(text, {panelClass: ['snackbar-error'], ...extraSettings});
  }

  warn(text: string, extraSettings?: any): MatSnackBarRef<SimpleSnackBar> {
    return this.default(text, {panelClass: ['snackbar-warn'], ...extraSettings});
  }

  dismissAll(): void {
    this.matSnackBar.dismiss();
  }
}
