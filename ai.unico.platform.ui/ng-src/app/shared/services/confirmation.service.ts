import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationModalComponent } from 'ng-src/app/shared/components/utils/confirmation-modal/confirmation-modal.component';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfirmationService {

  constructor(private ngbModal: NgbModal) {
  }

  private readonly DEFAULT_OPTIONS: ConfirmationModalOptions = {
    title: 'Confirm',
    content: 'Are you sure?',
    confirmLabel: 'Confirm',
    declineLabel: 'Cancel',
    confirmClass: 'success',
    declineClass: 'light'
  };

  confirm(options?: ConfirmationModalOptions): Observable<void> {
    const ngbModalRef = this.ngbModal.open(ConfirmationModalComponent, {size: 'M'});
    const componentInstance: ConfirmationModalComponent = ngbModalRef.componentInstance;
    componentInstance.options = {...this.DEFAULT_OPTIONS, ...options};
    return ngbModalRef.closed;
  }
}

export interface ConfirmationModalOptions {
  title?: string;
  content?: string;
  confirmLabel?: string;
  declineLabel?: string;
  confirmClass?: 'primary' | 'success' | 'danger' | 'light';
  declineClass?: 'primary' | 'success' | 'danger' | 'light';
}
