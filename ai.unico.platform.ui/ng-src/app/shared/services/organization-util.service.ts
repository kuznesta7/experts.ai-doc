import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrganizationUtilService {

  static trimOrganizationName(name: string, parentName: string): string {
    if (name == null) return '';
    const regExp = new RegExp(parentName + '[ ]*/');
    const parsed = regExp.exec(name);
    if (parsed) {
      return name.slice(parsed[0].length).trim();
    }
    return name;
  }

}
