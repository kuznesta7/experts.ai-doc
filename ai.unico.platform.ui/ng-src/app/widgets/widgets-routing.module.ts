import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {WidgetComponent} from 'ng-src/app/widgets/components/widget/widget.component';
import {
  WidgetExpertDetailComponent
} from 'ng-src/app/widgets/components/widget-expert-detail/widget-expert-detail.component';
import {
  WidgetCommercializationDetailComponent
} from 'ng-src/app/widgets/components/widget-commercialization-detail/widget-commercialization-detail.component';
import {
  WidgetProjectDetailComponent
} from 'ng-src/app/widgets/components/widget-project-detail/widget-project-detail.component';
import {
  WidgetOutcomeDetailComponent
} from 'ng-src/app/widgets/components/widget-outcome-detail/widget-outcome-detail.component';
import {
  WidgetLectureDetailComponent
} from 'ng-src/app/widgets/components/widget-lecture-detail/widget-lecture-detail.component';
import {
  WidgetEquipmentDetailComponent
} from 'ng-src/app/widgets/components/widget-equipment-detail/widget-equipment-detail.component';
import {
  WidgetOpportunityDetailComponent
} from 'ng-src/app/widgets/components/widget-opportunity-detail/widget-opportunity-detail.component';
import {WidgetAppComponent} from './widget-app.component';

const routes: Routes = [
  {
    path: '',
    component: WidgetAppComponent,
    children: [
      {
        path: '',
        component: WidgetComponent
      },
      {
        path: 'organizations/:organizationId',
        component: WidgetComponent
      },
      {
        path: 'experts/:expertCode',
        component: WidgetExpertDetailComponent
      },
      {
        path: 'outcomes/:outcomeCode',
        component: WidgetOutcomeDetailComponent
      },
      {
        path: 'projects/:projectCode',
        component: WidgetProjectDetailComponent
      },
      {
        path: 'commercialization/:commercializationId',
        component: WidgetCommercializationDetailComponent
      },
      {
        path: 'lecture/:lectureCode',
        component: WidgetLectureDetailComponent
      },
      {
        path: 'equipment/:equipmentCode',
        component: WidgetEquipmentDetailComponent
      },
      {
        path: 'opportunity/:opportunityId',
        component: WidgetOpportunityDetailComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WidgetsRoutingModule {
}
