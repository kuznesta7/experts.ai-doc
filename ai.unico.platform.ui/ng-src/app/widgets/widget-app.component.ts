import {Component, OnDestroy, OnInit} from '@angular/core';
import {UiThemeService} from '../shared/services/ui-theme.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subject} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {Title} from "@angular/platform-browser";
import {Clipboard} from "@angular/cdk/clipboard";

@Component({
  selector: 'app-widget-app',
  templateUrl: './widget-app.component.html',
})
export class WidgetAppComponent implements OnInit, OnDestroy {

  private destroyed$ = new Subject<void>();

  constructor(private uiThemeService: UiThemeService,
              private route: ActivatedRoute,
              private titleService: Title,
              private router: Router,
              private clipboard: Clipboard) {
    this.titleService.setTitle('Widget');
  }

  ngOnInit(): void {
    this.route.queryParams.pipe(
      map(p => p.theme),
      takeUntil(this.destroyed$)
    ).subscribe(theme => {
      if (this.uiThemeService.isSessionStorageAvailable()) {
        if (theme) {
          sessionStorage.setItem('WIDGET_THEME', theme);
        } else {
          theme = sessionStorage.getItem('WIDGET_THEME');
        }
      }
      if (!theme) {
        theme = this.uiThemeService.getDefaultTheme();
      }
      this.uiThemeService.loadTheme(theme).subscribe({
        error: () => {
          if (this.uiThemeService.isSessionStorageAvailable()) {
            sessionStorage.removeItem('WIDGET_THEME');
          }
          this.uiThemeService.loadTheme();
        }
      });
    });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
  showCopiedPopup: boolean = false;
  shareLink() {
    let searchParam = new URLSearchParams( document.location.search);
    if (searchParam.has('base_url')){
      // @ts-ignore
      const url = new URL(searchParam.get('base_url').toString());
      url.searchParams.append('widget', document.location.href);
      this.clipboard.copy(url.toString());
    } else {
      this.clipboard.copy(document.location.href.toString());
    }
    this.showCopiedPopup = true;
    setTimeout(() => this.showCopiedPopup = false, 2000);
  }

}
