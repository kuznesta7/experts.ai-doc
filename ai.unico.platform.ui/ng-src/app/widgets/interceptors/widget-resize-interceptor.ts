import { Injectable } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { WidgetUtilService } from 'ng-src/app/widgets/utils/widget-util.service';

@Injectable()
export class WidgetResizeInterceptor implements HttpInterceptor {

  constructor(private widgetUtilService: WidgetUtilService) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(req).pipe(
      tap(() => {
        this.widgetUtilService.dispatchResizeEvent();
      })
    );
  }

}

export const widgetResizeHttpInterceptorProvider = [
  {provide: HTTP_INTERCEPTORS, useClass: WidgetResizeInterceptor, multi: true}
];
