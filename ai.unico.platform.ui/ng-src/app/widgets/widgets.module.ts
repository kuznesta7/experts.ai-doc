import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from 'ng-src/app/shared/shared.module';
import {WidgetComponent} from 'ng-src/app/widgets/components/widget/widget.component';
import {WidgetsRoutingModule} from 'ng-src/app/widgets/widgets-routing.module';
import {
  CommercializationComponent
} from 'ng-src/app/widgets/components/widget-commercialization/commercialization.component';
import {WidgetProjectsComponent} from 'ng-src/app/widgets/components/widget-projects/widget-projects.component';
import {WidgetExpertsComponent} from 'ng-src/app/widgets/components/widget-experts/widget-experts.component';
import {PreviewComponent} from 'ng-src/app/widgets/components/preview/preview.component';
import {WidgetOutcomesComponent} from 'ng-src/app/widgets/components/widget-outcomes/widget-outcomes.component';
import {
  WidgetExpertDetailComponent
} from 'ng-src/app/widgets/components/widget-expert-detail/widget-expert-detail.component';
import {
  WidgetExpertDetailProjectsComponent
} from 'ng-src/app/widgets/components/widget-expert-detail/expert-detail-projects/widget-expert-detail-projects.component';
import {
  WidgetExpertDetailOutcomesComponent
} from 'ng-src/app/widgets/components/widget-expert-detail/expert-detail-outcomes/widget-expert-detail-outcomes.component';
import {
  WidgetExpertDetailCommercializationComponent
} from 'ng-src/app/widgets/components/widget-expert-detail/expert-detail-commercialization/widget-expert-detail-commercialization.component';
import {
  WidgetCommercializationDetailComponent
} from 'ng-src/app/widgets/components/widget-commercialization-detail/widget-commercialization-detail.component';
import {
  WidgetOutcomeDetailComponent
} from 'ng-src/app/widgets/components/widget-outcome-detail/widget-outcome-detail.component';
import {
  WidgetProjectDetailComponent
} from 'ng-src/app/widgets/components/widget-project-detail/widget-project-detail.component';
import {WidgetLecturesComponent} from 'ng-src/app/widgets/components/widget-lectures/widget-lectures.component';
import {
  WidgetLectureDetailComponent
} from 'ng-src/app/widgets/components/widget-lecture-detail/widget-lecture-detail.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {FormsModule} from '@angular/forms';
import {ContactModalComponent} from 'ng-src/app/platform/components/common/contact-modal/contact-modal.component';
import {WidgetEquipmentComponent} from 'ng-src/app/widgets/components/widget-equipment/widget-equipment.component';
import {
  WidgetEquipmentDetailComponent
} from 'ng-src/app/widgets/components/widget-equipment-detail/widget-equipment-detail.component';
import {
  WidgetOpportunityComponent
} from 'ng-src/app/widgets/components/widget-opportunity/widget-opportunity.component';
import {
  WidgetOpportunityDetailComponent
} from 'ng-src/app/widgets/components/widget-opportunity-detail/widget-opportunity-detail.component';
import {NgxCaptchaModule} from 'ngx-captcha';
import {
  WidgetOrganizationComponent
} from 'ng-src/app/widgets/components/widget-organization/widget-organization.component';
import {InfiniteScrollComponent} from './components/infinite-scroll/infinite-scroll.component';
import {WidgetFilterWrapperComponent} from './components/widget-filter-wrapper/widget-filter-wrapper.component';
import {OrganizationTypeComponent} from './components/organization-type/organization-type.component';
import {WidgetAppComponent} from './widget-app.component';
import {SpecialAbilitiesBarComponent} from './components/special-abilities-bar/special-abilities-bar.component';
import {MatIconModule} from '@angular/material/icon';
import {SubscribeModalComponent} from './components/special-abilities-bar/subscribe-modal/subscribe-modal.component';

@NgModule({
    declarations: [
      WidgetComponent,
      CommercializationComponent,
      WidgetProjectsComponent,
      WidgetExpertsComponent,
      PreviewComponent,
      WidgetOutcomesComponent,
      WidgetExpertDetailComponent,
      WidgetExpertDetailProjectsComponent,
      WidgetExpertDetailOutcomesComponent,
      WidgetExpertDetailCommercializationComponent,
      WidgetCommercializationDetailComponent,
      WidgetOutcomeDetailComponent,
      WidgetProjectDetailComponent,
      WidgetLecturesComponent,
      WidgetLectureDetailComponent,
      ContactModalComponent,
      WidgetEquipmentComponent,
      WidgetEquipmentDetailComponent,
      WidgetOpportunityComponent,
      WidgetOpportunityDetailComponent,
      WidgetOrganizationComponent,
      InfiniteScrollComponent,
      WidgetFilterWrapperComponent,
      OrganizationTypeComponent,
      WidgetAppComponent,
      SpecialAbilitiesBarComponent,
      SubscribeModalComponent
    ],
  imports: [
    CommonModule,
    WidgetsRoutingModule,
    SharedModule,
    MatDatepickerModule,
    FormsModule,
    NgxCaptchaModule,
    MatIconModule
  ],
    exports: [
        OrganizationTypeComponent
    ],
    bootstrap: [
        WidgetComponent
    ]
})
export class WidgetsModule {

}
