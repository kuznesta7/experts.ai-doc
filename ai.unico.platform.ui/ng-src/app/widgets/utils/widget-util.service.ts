import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {OrganizationBaseDto} from '../../interfaces/organization-base-dto';
import {NavigationExtras, Router} from '@angular/router';
import {ContactModalComponent} from '../../platform/components/common/contact-modal/contact-modal.component';
import {OrganizationLicenceService} from '../../services/organization-licence.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class WidgetUtilService {

  private dispatch$ = new Subject<void>();

  constructor(
    private router: Router,
    private organizationLicenseService: OrganizationLicenceService,
    private ngbModal: NgbModal
  ) {
    this.dispatch$.pipe(debounceTime(0)).subscribe(() => {
      const scrollHeight = document.body.scrollHeight;
      window.parent.postMessage(scrollHeight, '*');
    });
  }

  checkOrganizationLicense(baseDto: OrganizationBaseDto): void {
    const organizationId = baseDto.organizationId;
    if (this.router.url.indexOf('/widgets/') > -1) {
      if (!baseDto.gdpr) {
        console.log('No gdpr.' + baseDto.organizationId);
        this.openContactModal(organizationId);
        return;
      }
      this.organizationLicenseService.organizationHasLicense(organizationId).subscribe(result => {
        console.log('result is ', result);
        if (result) {
          const navigationExtras: NavigationExtras = {
            queryParams: {header: true, experts: true, projects: true, items: true}
          };
          console.log('navigate for ', '/widgets/organizations/' + organizationId);
          const url = this.router.serializeUrl(this.router.createUrlTree(['/widgets/organizations/' + organizationId], navigationExtras));
          window.open(url, '_blank');
        } else {
          this.openContactModal(organizationId);
        }
      });
    } else {
      this.router.navigate(['/organizations/' + organizationId]);
    }
  }

  openContactModal(organizationId: number | undefined): void {
    const ngbModalRef = this.ngbModal.open(ContactModalComponent, {size: 'lg'});
    ngbModalRef.componentInstance.organizationId = organizationId;
    ngbModalRef.componentInstance.contactType = 'organizationDetail';
  }

  dispatchResizeEvent(): void {
    this.dispatch$.next();
  }
}
