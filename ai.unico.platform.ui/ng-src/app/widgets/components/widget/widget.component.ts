import {Component, EventEmitter, HostListener, OnDestroy, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {BehaviorSubject, combineLatest, Observable, Subject} from 'rxjs';
import {UrlParams} from 'ng-src/app/constants/url-params';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {ExternalMessageDto} from 'ng-src/app/interfaces/external-message';
import {EvidenceFilter} from 'ng-src/app/interfaces/evidence-filter';
import {FlashService} from 'ng-src/app/shared/services/flash.service';
import {Title} from '@angular/platform-browser';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {InitConfigService} from 'ng-src/app/services/init-config.service';
import {debounceTime, filter, map, shareReplay, switchMap, takeUntil} from 'rxjs/operators';
import {ListMode, OrganizationDetailService} from '../../../services/organization-detail.service';
import {MatSnackBarConfig} from '@angular/material/snack-bar';
import {ContactModalComponent} from '../../../platform/components/common/contact-modal/contact-modal.component';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html'
})
export class WidgetComponent implements OnInit, OnDestroy {
  subscribeFormGroup: FormGroup;
  submitted = false;
  siteKey$ = this.initConfigService.getRecaptchaPublicKey();
  currentOutcomeListMode: ListMode;
  currentOutcomeListMode$: BehaviorSubject<ListMode> = new BehaviorSubject<ListMode>('EXPERT');
  hasGdpr$: Observable<boolean>;

  @Output() keyword$ = new EventEmitter<string>();

  private $unsubscribe: Subject<boolean> = new Subject();

  constructor(
    private organizationDetailService: OrganizationDetailService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private flashService: FlashService,
    private ngbModal: NgbModal,
    private widgetService: WidgetService,
    private titleService: Title,
    private initConfigService: InitConfigService,
    private router: Router,
  ) {
    this.titleService.setTitle('Widget');

    this.route.queryParams.subscribe((params) => {
      this.availableModes.RESEARCH_PROJECT = params.hasOwnProperty(
        UrlParams.WIDGET_PROJECTS
      );
      this.availableModes.COMMERCIALIZATION_PROJECT = params.hasOwnProperty(
        UrlParams.WIDGET_COMMERCIALIZATION
      );
      this.availableModes.EXPERT = params.hasOwnProperty(
        UrlParams.WIDGET_EXPERTS
      );
      this.availableModes.RESEARCH_OUTCOME = params.hasOwnProperty(
        UrlParams.WIDGET_OUTCOMES
      );
      this.availableModes.LECTURE = params.hasOwnProperty(
        UrlParams.WIDGET_LECTURE
      );
      this.availableModes.EQUIPMENT = params.hasOwnProperty(
        UrlParams.WIDGET_EQUIPMENT
      );
      this.availableModes.OPPORTUNITY = params.hasOwnProperty(
        UrlParams.WIDGET_OPPORTUNITY
      );
      this.availableModes.ORGANIZATION_CHILDREN = params.hasOwnProperty(
        UrlParams.WIDGET_ORGANIZATION_CHILDREN
      );

      this.showPreview = params.hasOwnProperty(UrlParams.WIDGET_OVERVIEW);
      const firstAvailable = this.listModeDetails.find(
        (m) => this.availableModes[m.outcomeListMode]
      );
      this.showModeSelection = Object.entries(this.availableModes).filter(k => k[1]).length > 1;

      if (params.hasOwnProperty(UrlParams.DATA_LIST_MODE)) {
        this.currentOutcomeListMode = params[UrlParams.DATA_LIST_MODE];
        this.currentOutcomeListMode$.next(this.currentOutcomeListMode);
        this.showList = true;
      } else if (firstAvailable) {
        this.showList = true;
        this.currentOutcomeListMode = firstAvailable?.outcomeListMode;
        this.currentOutcomeListMode$.next(this.currentOutcomeListMode);
        this.setListModeType(firstAvailable?.outcomeListMode);
      }

      if (params.hasOwnProperty(UrlParams.QUERY_PARAM)) {
        this.currentQuery = params[UrlParams.QUERY_PARAM];
        this.filterFormGroup.patchValue({query: this.currentQuery});
      }
    });
  }

  organizationId: number;
  showPreview: boolean;
  showList = false;
  showModeSelection = true;
  kwSearchChange: string;
  contactFormResult = new ExternalMessageDto();
  closeResult = '';
  availableModes: { [key: string]: boolean } = {};
  filter$: Observable<EvidenceFilter>;
  screenWidth$ = new BehaviorSubject<number>(window.innerWidth);
  organizationKeywords$: Observable<{ keyword: string, occurrence: number }[]>;
  filterFormGroup = new FormGroup({
    query: new FormControl(''),
  });
  listModeDetails: ListModeDetail[] = [
    {
      outcomeListMode: 'OPPORTUNITY',
      title: 'Opportunities',
      icon: 'archway' as any,
    },
    {
      outcomeListMode: 'EXPERT',
      title: 'Experts',
      icon: 'users' as any,
    },
    {
      outcomeListMode: 'RESEARCH_OUTCOME',
      title: 'Research outcomes',
      icon: 'file-alt' as any,
    },
    {
      outcomeListMode: 'RESEARCH_PROJECT',
      title: 'Research projects',
      icon: 'microscope' as any,
    },
    {
      outcomeListMode: 'COMMERCIALIZATION_PROJECT',
      title: 'Commercialization',
      icon: 'rocket' as any,
    },
    {
      outcomeListMode: 'LECTURE',
      title: 'Lectures',
      icon: 'graduation-cap' as any,
    },
    {
      outcomeListMode: 'EQUIPMENT',
      title: 'Equipment/Service',
      icon: 'atom' as any,
    },
    {
      outcomeListMode: 'ORGANIZATION_CHILDREN',
      title: 'Organizations',
      icon: 'university' as any,
    }
  ];
  private contactModalRef: NgbModalRef;
  private currentQuery = '';

  ngOnInit(): void {
    this.screenWidth$.next(window.innerWidth);
    this.route.params.subscribe((params: Params) => this.organizationId = Number(params.organizationId));
    this.hasGdpr$ = this.widgetService.getGdpr(this.organizationId).pipe(shareReplay());
    this.hasGdpr$.subscribe(x => {
      if (!x) {
        const ngbModalRef = this.ngbModal.open(ContactModalComponent, {
          size: 'lg',
        });
        const orgId = this.organizationId;
        ngbModalRef.componentInstance.organizationId = orgId;
        ngbModalRef.componentInstance.contactType = 'organizationDetail';
      }
    });
    this.subscribeFormGroup = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      // recaptcha: ['', Validators.required],
      agreeTerms: [false],
    });

    this.filterFormGroup
      .get('query')
      ?.valueChanges.pipe(takeUntil(this.$unsubscribe))
      .subscribe((query: string) => {
        this.setQuery(query);
      });

    const organizationId$ = this.route.params.pipe(
      map(p => this.organizationId)
    );

    this.organizationKeywords$ = combineLatest([organizationId$, this.currentOutcomeListMode$, this.screenWidth$]).pipe(
      debounceTime(100),
      switchMap(([organizationId, _, w]) => this.organizationDetailService.getOrganizationSuggestedKeywords(organizationId, this.currentOutcomeListMode)),
      filter(keywords => !!keywords),
      map((keywords) => {
        const newList = Object.entries(keywords).map(([k, o]) => {
          return {keyword: k as string, occurrence: o as number};
        });
        newList.sort((a, b) => b.occurrence - a.occurrence);
        const MAX_LENGTH = window.innerWidth / 8;
        let currentLength = 0;
        const result: {keyword: string, occurrence: number}[] = [];
        while (newList.length > 0){
          const tmp = newList.shift();
          if (tmp == null) {
            break;
          }
          currentLength += tmp.keyword.length + 8;  // 8 is the estimated width between words in boxes
          if (currentLength <= MAX_LENGTH){
            result.push(tmp);
          } else {
            break;
          }
        }
        return result;
      }),
      shareReplay()
    );
  }

  back(): void {
    window.history.back();
  }

  // tslint:disable-next-line:typedef
  get subForm() {
    return this.subscribeFormGroup.controls;
  }

  updateQuery(kw: string): void {
    this.kwSearchChange = kw;
  }

  open(content: any): any {
    this.contactModalRef = this.ngbModal.open(content, {
      ariaLabelledBy: 'modal-basic-title',
    });
  }

  public setTitle(newTitle: string): void {
    this.titleService.setTitle(newTitle);
  }


  public setListModeType(mode: ListMode): void {
    this.router.navigate([], {
      queryParams: {
        outcomeListMode: mode,
      },
      queryParamsHandling: 'merge',
    });
  }

  public setQuery(query: string): void {
    this.router.navigate([], {
      queryParams: {
        [UrlParams.QUERY_PARAM]: query,
      },
      queryParamsHandling: 'merge',
    });
  }

  ngOnDestroy(): void {
    this.$unsubscribe.next(true);
    this.$unsubscribe.complete();
  }

  @HostListener('window:resize')
  onResize(): void {
    this.screenWidth$.next(window.innerWidth);
  }

  onSubmit(): void {
    // invoke change on the form control to trigger the update
    this.filterFormGroup.get('query')?.setValue(this.filterFormGroup.get('query')?.value);
  }
}

interface ListModeDetail {
  outcomeListMode: ListMode;
  title: string;
  icon: any;
}
