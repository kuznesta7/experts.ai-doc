import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {ProjectDetailDto} from 'ng-src/app/interfaces/project-detail-dto';
import {ActivatedRoute, Router} from '@angular/router';
import {distinctUntilChanged, filter, map, shareReplay, startWith, switchMap, tap} from 'rxjs/operators';
import {UrlParams} from 'ng-src/app/constants/url-params';
import {tapAllHandleError} from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {ContactModalComponent} from 'ng-src/app/platform/components/common/contact-modal/contact-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OrganizationLicenceService} from 'ng-src/app/services/organization-licence.service';

@Component({
  selector: 'app-widget-project-detail',
  templateUrl: './widget-project-detail.component.html',
})
export class WidgetProjectDetailComponent implements OnInit {
  loading$ = new BehaviorSubject<boolean>(true);
  projectDetail$: Observable<ProjectDetailDto>;
  mode$: Observable<ProjectDetailMode>;
  years$: Observable<number[]>;
  modes = ProjectDetailMode;

  constructor(private route: ActivatedRoute,
              private ngbModal: NgbModal,
              private router: Router,
              private organizationLicenseService: OrganizationLicenceService,
              private widgetService: WidgetService) {
  }

  ngOnInit(): void {
    const projectCode$ = combineLatest([this.route.params]).pipe(
      map(([p]) => p[UrlParams.PROJECT_CODE])
    );

    this.projectDetail$ = projectCode$.pipe(
      tap(() => this.loading$.next(true)),
      switchMap(code => this.widgetService.getProjectDetail(code)),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );

    this.years$ = this.projectDetail$.pipe(
      map(project => {
        const set = Object.values(project.organizationFinanceMap).reduce((result, yv) => {
          Object.keys(yv).forEach(y => result.add(parseInt(y, 10)));
          return result;
        }, new Set<number>());
        return [...set];
      }),
      shareReplay()
    );

    this.mode$ = this.route.queryParams.pipe(
      map(params => params.mode),
      filter(m => !!m),
      startWith(ProjectDetailMode.OVERVIEW),
      distinctUntilChanged(),
      shareReplay()
    );
  }

  back(): void {
    window.history.back();
  }

  checkOrganizationLicense(organizationId: number | undefined): void {
    if (organizationId !== undefined) {
      this.organizationLicenseService.organizationHasLicense(organizationId).subscribe(result => {
        if (result) {
          this.router.navigate(['/organizations/' + organizationId]);
        } else {
          this.openContactModal(organizationId);
        }
      });
    }
  }

  openContactModal(organizationId: number | undefined): void {
    const ngbModalRef = this.ngbModal.open(ContactModalComponent, {size: 'lg'});
    ngbModalRef.componentInstance.organizationId = organizationId;
    ngbModalRef.componentInstance.contactType = 'organizationDetail';
  }
}

export enum ProjectDetailMode {
  OVERVIEW = 'OVERVIEW',
  OUTCOMES = 'OUTCOMES',
}
