import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBarConfig} from '@angular/material/snack-bar';
import {SubscriptionDto} from 'ng-src/app/interfaces/subscription-dto';
import {RecombeeService} from 'ng-src/app/services/recombee.service';
import {FlashService} from 'ng-src/app/shared/services/flash.service';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {GoogleAnalyticsService} from 'ng-src/app/services/google-analytics.service';
import {SubscriptionService} from 'ng-src/app/services/subscription.service';


@Component({
  selector: "subscribe-modal",
  templateUrl: 'subscribe-modal.component.html',
})
export class SubscribeModalComponent implements OnInit {


  subscribeFormGroup: FormGroup;
  submitted = false;
  organizationId: number;
  @Output()  closeModal = new EventEmitter<boolean>();

  constructor(
     private formBuilder: FormBuilder,
     private flashService: FlashService,
     private widgetService: WidgetService,
     private googleAnalyticsService: GoogleAnalyticsService,
     private subscriptionService: SubscriptionService
  ) {}

  ngOnInit(): void {
    this.subscribeFormGroup = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      // recaptcha: ['', Validators.required],
      agreeTerms: [false],
    });
  }
  // tslint:disable-next-line:typedef
  get subForm() {
    return this.subscribeFormGroup.controls;
  }

  send(): void {
    this.submitted = true;
    const snackBarConfig: MatSnackBarConfig = {
      verticalPosition: 'top',
      horizontalPosition: 'center',
    };

    if (this.subForm.agreeTerms.value === false) {
      this.flashService.error('Please check agree terms box!', snackBarConfig);
      return;
    } else if (this.subscribeFormGroup.invalid) {
      this.flashService.error('Please fill form fields!', snackBarConfig);
      return;
    }

    const value = this.subscribeFormGroup.value;
    const subscriptionDto: SubscriptionDto = {
      email: value.email,
      organizationId: this.organizationId,
      userToken: new RecombeeService().getUserRecombeeToken(),
      subscriptionTypeId: 3
    };
    this.googleAnalyticsService.subscribe(value.email, this.organizationId);
    this.subscriptionService
      // .widgetEmailSubscription(subscriptionDto, value.recaptcha)
      .widgetEmailSubscription(subscriptionDto, '')  // todo - change to value.recaptcha for recaptcha
      .subscribe((response) => {
        this.subscribeFormGroup.reset();
        this.flashService.ok(response.resultMessage, snackBarConfig);
      });
  }

}
