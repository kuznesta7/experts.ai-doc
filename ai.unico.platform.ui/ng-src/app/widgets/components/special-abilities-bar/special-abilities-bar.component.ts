import {Component, Input, OnInit} from "@angular/core";
import {fromEvent} from "rxjs";
import {Clipboard} from "@angular/cdk/clipboard";
import {NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {SubscribeModalComponent} from "./subscribe-modal/subscribe-modal.component";
import {ActivatedRoute} from "@angular/router";
import {debounceTime} from "rxjs/operators";


@Component({
  selector: "special-abilities-bar",
  templateUrl: 'special-abilities-bar.component.html',
  styleUrls: ['special-abilities-bar.component.scss'],
})

export class SpecialAbilitiesBarComponent implements OnInit{
  @Input() showSubscribe: boolean;
  scroll = window.pageYOffset
  showCopiedTooltip: boolean = false;
  timer$ = fromEvent(window, 'scroll').pipe(debounceTime(100));
  subscribeTooltipText = "Subscribe to get informed when a new relevant opportunity appears.\n" +
    "      Get occasional newsletters with personalized opportunities.\n" +
    "      Do not worry, we won't spam you and you can unsubscribe anytime.";
  shareTooltipText = "Share widget url";
  shareTooltipTextCopied = "Copied";

  constructor(private clipboard: Clipboard,
              private ngbModal: NgbModal,
              private route: ActivatedRoute,) {}
  organizationId: number;
  private contactModalRef: NgbModalRef;

  ngOnInit() {
     this.timer$.subscribe(()=>{
      this.scroll = window.pageYOffset;
    })
    this.route.params.subscribe((params: any) => this.organizationId = params.organizationId);
  }

  copyToClipboard() {
    const searchParam = new URLSearchParams( document.location.search);
    if (searchParam.has('base_url')){
      // @ts-ignore
      const url = new URL(searchParam.get('base_url').toString());
      url.searchParams.append('widget', document.location.href);
      this.clipboard.copy(url.toString());
    } else {
      this.clipboard.copy(document.location.href.toString());
    }
    this.showCopiedTooltip = true;
    setTimeout(() => {
      this.showCopiedTooltip = false;
    }, 3000);
  }

  fullScreen = () => {
    if (document.fullscreenElement) {
      document.exitFullscreen().then();
    } else {
      document.documentElement.requestFullscreen().then();
    }
  }
  open(): any {
    this.contactModalRef = this.ngbModal.open(SubscribeModalComponent, {
      ariaLabelledBy: 'modal-basic-title',
    });
    this.contactModalRef.componentInstance.organizationId = this.organizationId;
    this.contactModalRef.componentInstance.closeModal.subscribe(() => {
      this.contactModalRef.close();
    });
  }
}
