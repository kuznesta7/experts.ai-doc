import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-widget-filter-wrapper',
  templateUrl: './widget-filter-wrapper.component.html',
})
export class WidgetFilterWrapperComponent implements OnInit {

  isCollapsed = true;
  status = true;

  constructor() { }

  ngOnInit(): void {
  }

  changeClass(): void {
    this.status = ! this.status;
  }

}
