import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {filter, map, shareReplay, switchMap, tap} from 'rxjs/operators';
import {tapAllHandleError} from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {OrganizationDetail, OrganizationDetailService} from 'ng-src/app/services/organization-detail.service';
import {ActivatedRoute} from '@angular/router';
import {AuthorizationService} from 'ng-src/app/services/authorization.service';
import {QueryParamService} from 'ng-src/app/shared/services/query-param.service';
import {UploadService} from 'ng-src/app/services/upload.service';
import {
  OrganizationStructureDto,
  OrganizationStructureService
} from 'ng-src/app/services/organization-structure.service';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ExternalMessageDto} from 'ng-src/app/interfaces/external-message';
import {InitConfigService} from 'ng-src/app/services/init-config.service';
import {FlashService} from 'ng-src/app/shared/services/flash.service';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: []
})
export class PreviewComponent implements OnInit {
  @Input()
  organizationId: number;

  @Output() keyword$ = new EventEmitter<string>();

  siteKey$ = this.initConfigService.getRecaptchaPublicKey();
  organizationDetail$: Observable<OrganizationDetail>;
  organizationStructure$: Observable<OrganizationStructureDto[]>;
  organizationKeywords$: Observable<{ keyword: string, occurrence: number }[]>;
  editable$: Observable<boolean>;
  reloadImgUrl$: Observable<string>;
  loading$ = new BehaviorSubject(true);
  contactForm: FormGroup;
  contactFormResult = new ExternalMessageDto();
  private contactModalRef: NgbModalRef;


  constructor(private organizationDetailService: OrganizationDetailService,
              private route: ActivatedRoute,
              private authorizationService: AuthorizationService,
              private queryParamService: QueryParamService,
              private uploadService: UploadService,
              private organizationStructureService: OrganizationStructureService,
              private ngbModal: NgbModal,
              private widgetService: WidgetService,
              private fb: FormBuilder,
              private initConfigService: InitConfigService,
              private flashService: FlashService) {
    this.contactForm = this.fb.group({
      senderEmail: ['', [Validators.required, Validators.email]],
      message: ['', Validators.required],
      recaptcha: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    const organizationId$ = this.route.params.pipe(
      map(p => this.organizationId)
    );

    this.editable$ = organizationId$.pipe(
      switchMap(id => this.authorizationService.canEditOrganizationProfile(id))
    );

    this.organizationDetail$ = organizationId$.pipe(
      tap(() => this.loading$.next(true)),
      switchMap(organizationId => this.widgetService.getOrganizationDetail(organizationId).pipe(
        tapAllHandleError(() => this.loading$.next(false))
      )),
      shareReplay()
    );
    this.organizationKeywords$ = organizationId$.pipe(
      switchMap(organizationId => this.organizationDetailService.getOrganizationKeywords(organizationId)),
      filter(keywords => !!keywords),
      map(keywords => {
        const newList = Object.entries(keywords).map(([k, o]) => {
          return {keyword: k, occurrence: o};
        });
        newList.sort((a, b) => b.occurrence - a.occurrence);
        return newList;
      }),
      shareReplay()
    );

    this.organizationStructure$ = organizationId$.pipe(
      switchMap(organizationId => this.organizationStructureService.getStructure(organizationId)),
      shareReplay()
    );
  }

  open(content: any): any {
    this.contactModalRef = this.ngbModal.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

  send(): void {
    if (this.contactForm.invalid) {
      this.flashService.error('The form contains invalid value');
      return;
    }
    this.contactFormResult = Object.assign({}, this.contactForm.value);
    this.contactFormResult.organizationId = this.organizationId;
    this.contactFormResult.messageType = 'ORGANIZATION';
    this.widgetService.sendEmailWidget(this.contactFormResult, this.contactForm.value.recaptcha).subscribe(value => {
      this.flashService.ok('Message has been successfully sent');
      this.contactModalRef.close(true);
    });
  }
}


