import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, combineLatest, Observable, of, Subject} from 'rxjs';
import {EquipmentPreview} from 'ng-src/app/interfaces/equipment-preview';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PathService} from 'ng-src/app/shared/services/path.service';
import {map, shareReplay, startWith, switchMap, take, tap} from 'rxjs/operators';
import {UrlParams} from 'ng-src/app/constants/url-params';
import {tapAllHandleError} from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {MultilingualDto} from 'ng-src/app/interfaces/multilingual-dto';
import {ContactModalComponent} from 'ng-src/app/platform/components/common/contact-modal/contact-modal.component';
import {GoogleAnalyticsCategories} from 'ng-src/app/enums/google-analytics-categories';
import {GoogleAnalyticsService} from 'ng-src/app/services/google-analytics.service';

@Component({
  selector: 'app-widget-equipment-detail',
  templateUrl: './widget-equipment-detail.component.html'
})
export class WidgetEquipmentDetailComponent implements OnInit {

  loading$ = new BehaviorSubject<boolean>(true);
  equipmentDetail$: Observable<EquipmentPreview>;
  multilingualDetail$: Observable<MultilingualDto<EquipmentPreview>>;
  private update$ = new Subject<void>();
  languageCodes: string[] = [];
  selectedLanguageCode: string;

  constructor(private modalService: NgbModal,
              private googleAnalyticsService: GoogleAnalyticsService,
              private route: ActivatedRoute,
              private ngbModal: NgbModal,
              private router: Router,
              private pathService: PathService,
              private equipmentService: WidgetService) {
  }

  ngOnInit(): void {
    const equipmentId$ = this.route.params.pipe(
      map(p => p[UrlParams.EQUIPMENT_CODE])
    );

    this.multilingualDetail$ = combineLatest([equipmentId$, this.update$.pipe(startWith(true))]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([code]) => this.equipmentService.getEquipmentDetail(code)),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );
    this.multilingualDetail$.subscribe((multilingual) => {
      this.selectedLanguageCode = multilingual.preview.languageCode;
      this.equipmentDetail$ = new BehaviorSubject(multilingual.preview);
      this.languageCodes = multilingual.translations.map(t => t.languageCode);
    });
  }

  onLanguageChange(): void {
    this.multilingualDetail$.pipe(
      take(1),
      map(multilingual => {
        const selectedTranslation = multilingual.translations.find(translation => translation.languageCode === this.selectedLanguageCode);
        return {
          ...multilingual,
          preview: selectedTranslation || multilingual.preview
        };
      })
    ).subscribe(newMultilingual => {
      this.multilingualDetail$ = of(newMultilingual);
    });
  }

  back(): void {
    window.history.back();
  }


  openContactModal(): void {
    const ngbModalRef = this.modalService.open(ContactModalComponent, {size: 'lg'});
    this.multilingualDetail$.subscribe((multilingual) => {
      ngbModalRef.componentInstance.equipmentPreview = multilingual.preview;
      if (multilingual.preview.organizations.length !== 0) {
        ngbModalRef.componentInstance.organizationId = multilingual.preview.organizations[0].organizationId;
      }
      ngbModalRef.componentInstance.contactType = 'widgetEquipmentDetail';
      this.googleAnalyticsService.purchaseDetailClick(
        multilingual.preview.equipmentCode.toString(),
        GoogleAnalyticsCategories.EQUIPMENT,
        -1
      );
    });
  }

}
