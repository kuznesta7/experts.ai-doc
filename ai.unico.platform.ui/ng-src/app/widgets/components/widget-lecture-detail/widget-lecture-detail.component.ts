import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { WidgetService } from 'ng-src/app/services/widget.service';
import { map, shareReplay, startWith, switchMap, tap } from 'rxjs/operators';
import { UrlParams } from 'ng-src/app/constants/url-params';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { LecturePreview } from 'ng-src/app/interfaces/lecture-preview';

@Component({
  selector: 'app-widget-lecture-detail',
  templateUrl: './widget-lecture-detail.component.html'
})
export class WidgetLectureDetailComponent implements OnInit {

  loading$ = new BehaviorSubject<boolean>(true);
  update$ = new Subject<void>();
  lectureDetail$: Observable<LecturePreview>;

  constructor(private route: ActivatedRoute,
              private widgetService: WidgetService) {
  }

  ngOnInit(): void {
    const outcomeCode$ = this.route.params.pipe(
      map(p => p[UrlParams.LECTURE_CODE])
    );

    this.lectureDetail$ = combineLatest([outcomeCode$, this.update$.pipe(startWith(true))]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([code]) => this.widgetService.getLectureDetail(code)),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );
  }

  back(): void {
    window.history.back();
  }


}
