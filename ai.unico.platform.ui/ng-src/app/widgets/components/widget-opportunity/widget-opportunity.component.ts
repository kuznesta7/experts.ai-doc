import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {ActivatedRoute} from '@angular/router';
import {QueryParamService} from 'ng-src/app/shared/services/query-param.service';
import {OrganizationAutocompleteService} from 'ng-src/app/services/organization-autocomplete.service';
import {OpportunityWrapper} from 'ng-src/app/interfaces/opportunity-wrapper';
import {OpportunityFilter} from 'ng-src/app/interfaces/opportunity-filter';
import {OpportunityCategoryService} from 'ng-src/app/services/opportunity-category.service';
import {OpportunityPreview} from 'ng-src/app/interfaces/opportunity-preview';
import {GoogleAnalyticsService} from 'ng-src/app/services/google-analytics.service';
import {GoogleAnalyticsCategories} from 'ng-src/app/enums/google-analytics-categories';
import {cmp} from '../../../shared/utils/compare-util';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-widget-opportunity',
  templateUrl: './widget-opportunity.component.html',
})
export class WidgetOpportunityComponent implements OnInit, OnDestroy {
  @Input()
  organizationId: number;

  @Input()
  parentFormGroupQueryControl: AbstractControl;

  @Input() set kwUpdate(value: string) {
    this.parentFormGroupQueryControl?.setValue(value);
  }

  filterFormGroup = new FormGroup({
    query: new FormControl(''),
    jobType: new FormControl([]),
    opportunityType: new FormControl([]),
    organizationId: new FormControl([]),
  });

  opportunityWrapper: OpportunityWrapper;
  opportunityWrapper$: Observable<OpportunityWrapper>;
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 10});
  jobTypes$ = this.opportunityCategoryService.getJobType();
  opportunityTypes$ = this.opportunityCategoryService.getOpportunityType();
  loading$ = new BehaviorSubject(true);

  private $unsubscribe: Subject<boolean> = new Subject();

  constructor(
    private widgetService: WidgetService,
    private route: ActivatedRoute,
    private queryParamService: QueryParamService,
    private opportunityCategoryService: OpportunityCategoryService,
    private organizationService: OrganizationAutocompleteService,
    private googleAnalyticsService: GoogleAnalyticsService,
    private titleService: Title,
  ) {
    this.titleService.setTitle('Widget - Opportunities');
    this.queryParamService.handleFilterFormGroup(this.filterFormGroup);
  }

  loadOrganizations = (ids: number[]) => {
    return this.organizationService.getOrganizationsForAutocompleteStandardized(
      ids
    );
  }
  autocompleteOrganizations = (query: string) => {
    return this.organizationService.autocompleteOrganizationOpportunityStandardized(query, this.organizationId);
  }

  acceptOpportunityType(opportunityId: number): void {
    this.filterFormGroup.patchValue({opportunityType: opportunityId});
  }

  acceptJobType(jobId: number): void {
    this.filterFormGroup.patchValue({jobType: jobId});
  }

  acceptQuery(query: string): void {
    this.parentFormGroupQueryControl.setValue(query);
  }

  acceptOrganization(id: number): void {
    this.filterFormGroup.patchValue({organizationId: [id]});
  }

  loadData = (filter: OpportunityFilter,
              pageInfo: {page: number, limit: number}) => {
    filter.studentHash = this.route.snapshot.queryParams.studentHash;
    return this.widgetService.getOpportunities(this.organizationId, filter, pageInfo);
  }

  mergeWrappers = (wrapper: OpportunityWrapper) => {
    this.googleAnalyticsService.receiveRecommendation(
      GoogleAnalyticsCategories.OPPORTUNITY,
      wrapper.page,
      JSON.stringify(wrapper.opportunityPreviewDtos.map((opp: OpportunityPreview) => opp.opportunityId.toString()))
    );
    if (this.opportunityWrapper) {
      this.opportunityWrapper.opportunityPreviewDtos.push(...wrapper.opportunityPreviewDtos);
    } else {
      this.opportunityWrapper = wrapper;
    }
    return {wrapper: this.opportunityWrapper, numOfNewItems: wrapper.opportunityPreviewDtos.length};
  }

  resetData = (wrapper: OpportunityWrapper) => {
    wrapper.opportunityPreviewDtos = [];
  }

  saveInteractions = (filter: OpportunityFilter) => {
    if (filter.jobType) {
      this.googleAnalyticsService.filter(
        GoogleAnalyticsCategories.OPPORTUNITY,
        'job_type',
        filter.jobType
      );
    }
    if (filter.opportunityType) {
      this.googleAnalyticsService.filter(
        GoogleAnalyticsCategories.OPPORTUNITY,
        'opportunity_type',
        filter.opportunityType
      );
    }
    if (filter.query) {
      this.googleAnalyticsService.search(
        GoogleAnalyticsCategories.OPPORTUNITY,
        filter.query
      );
    }
  }

  ngOnInit(): void {
    this.filterFormGroup.setControl('query', this.parentFormGroupQueryControl);
  }

  compare(o1: any, o2: any): boolean {
    return cmp(o1, o2);
  }

  ngOnDestroy(): void {
    this.$unsubscribe.next(true);
    this.$unsubscribe.complete();
  }
}
