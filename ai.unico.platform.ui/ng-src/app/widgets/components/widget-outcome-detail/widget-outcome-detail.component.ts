import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, combineLatest, Observable, Subject} from 'rxjs';
import {ItemDetailDto} from 'ng-src/app/interfaces/item-detail-dto';
import {ActivatedRoute, Router} from '@angular/router';
import {map, shareReplay, startWith, switchMap, tap} from 'rxjs/operators';
import {UrlParams} from 'ng-src/app/constants/url-params';
import {tapAllHandleError} from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OrganizationLicenceService} from 'ng-src/app/services/organization-licence.service';

@Component({
  selector: 'app-widget-outcome-detail',
  templateUrl: './widget-outcome-detail.component.html',
})
export class WidgetOutcomeDetailComponent implements OnInit {

  loading$ = new BehaviorSubject<boolean>(true);
  update$ = new Subject<void>();
  outcomeDetail$: Observable<ItemDetailDto>;

  constructor(private route: ActivatedRoute,
              private widgetService: WidgetService,
              private ngbModal: NgbModal,
              private router: Router,
              private organizationLicenseService: OrganizationLicenceService) {
  }

  ngOnInit(): void {
    const outcomeCode$ = this.route.params.pipe(
      map(p => p[UrlParams.OUTCOME_CODE])
    );

    this.outcomeDetail$ = combineLatest([outcomeCode$, this.update$.pipe(startWith(true))]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([code]) => this.widgetService.getOutcomeDetail(code)),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );
  }

  back(): void {
    window.history.back();
  }
}
