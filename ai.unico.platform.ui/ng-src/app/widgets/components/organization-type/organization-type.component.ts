import {Component, Input, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {OrganizationTypeService, OrganizationTypeWrapper} from '../../../services/organization-type.service';
import {share, switchMap, tap} from 'rxjs/operators';
import {tapAllHandleError} from '../../../shared/operators/tap-all-handle-error-operator';

@Component({
  selector: 'app-organization-type',
  templateUrl: './organization-type.component.html',
})
export class OrganizationTypeComponent implements OnInit {

  organizationTypeWrapper$: Observable<OrganizationTypeWrapper>;
  organizationId: number;
  showSelect = true;
  organizationTypeName = 'start';
  selectedId = 0;

  @Input() organizationId$: Observable<number>;

  constructor(private organizationTypeService: OrganizationTypeService) { }

  ngOnInit(): void {
    const loaderHelper$ = new Subject<void>();
    this.organizationTypeWrapper$ = this.organizationId$.pipe(
      tap(organizationId => this.organizationId = organizationId),
      switchMap(organizationId => this.organizationTypeService.getOrganizationTypes(organizationId)),
      tap(wrapper => {
        this.showSelect = ! wrapper.selected;
        if (wrapper.selected !== null){
          this.selectedId = wrapper.selected.id;
          this.organizationTypeName = wrapper.selected.name;
        } else {
          this.selectedId = 0;
        }
      }),
      tapAllHandleError(() => loaderHelper$.next()),
      share()
    );
  }

  setOrganizationType(event: any): void {
    this.selectedId = event.value;
    this.organizationTypeName = event.source.triggerValue;
    this.organizationTypeService.setOrganizationType(this.organizationId, event.value).subscribe(() => console.log());
  }

  onFocus(): void {
    this.showSelect = true;
  }

  onBlur(): void {
    this.showSelect = false;
  }
}
