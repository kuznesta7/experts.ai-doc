import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';
import {BehaviorSubject, Observable} from 'rxjs';
import {EvidenceItemWrapper} from 'ng-src/app/interfaces/evidence-item-wrapper';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {IdDescriptionOption} from 'ng-src/app/shared/interfaces/idDescriptionOption';
import {map} from 'rxjs/operators';
import {QueryParamService} from 'ng-src/app/shared/services/query-param.service';
import {ItemTypeService} from 'ng-src/app/services/item-type.service';
import {OrganizationAutocompleteService} from 'ng-src/app/services/organization-autocomplete.service';
import {ExpertAutocompleteService} from 'ng-src/app/services/expert-autocomplete.service';
import {EvidenceFilter} from 'ng-src/app/interfaces/evidence-filter';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {cmp} from '../../../shared/utils/compare-util';
import {EvidenceItemPreviewWithExperts} from '../../../interfaces/evidence-item-preview-with-experts';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-widget-outcomes',
  templateUrl: './widget-outcomes.component.html',
})
export class WidgetOutcomesComponent implements OnInit {
  @Input()
  organizationId: number;

  @Input()
  parentFormGroupQueryControl: AbstractControl;

  @Input() set kwUpdate(value: string) {
    if (typeof value !== 'undefined') {
      this.parentFormGroupQueryControl?.setValue(value);
    }
  }

  selectedYear: number;
  years: number[] = [];
  outcomes: EvidenceItemPreviewWithExperts[] = [];
  page = 1;
  itemWrapper: EvidenceItemWrapper;
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 10});
  loading$ = new BehaviorSubject(true);

  filterFormGroup = new FormGroup({
    query: new FormControl(''),
    resultTypeGroupIds: new FormControl([]),
    participatingOrganizationIds: new FormControl([]),
    yearFrom: new FormControl([]),
    yearTo: new FormControl([]),
    expertCodes: new FormControl([]),
  });
  itemTypeGroups$: Observable<IdDescriptionOption[]> = this.itemTypeService
    .findItemTypeGroups()
    .pipe(
      map((groups) =>
        groups.map((g) => {
          return {id: g.itemTypeGroupId, description: g.itemTypeGroupTitle};
        })
      )
    );

  constructor(
    private widgetService: WidgetService,
    private queryParamService: QueryParamService,
    private itemTypeService: ItemTypeService,
    private organizationService: OrganizationAutocompleteService,
    private expertAutocompleteService: ExpertAutocompleteService,
    private titleService: Title,
  ) {
    this.titleService.setTitle('Widget - Outcomes');
    this.selectedYear = new Date().getFullYear();
    for (let year = this.selectedYear; year >= 1970; year--) {
      this.years.push(year);
    }
    this.queryParamService.handleFilterFormGroup(this.filterFormGroup, {
      numericFields: ['resultTypeGroupIds', 'participatingOrganizationIds'],
    });
  }

  loadOrganizations = (ids: number[]) => {
    return this.organizationService.getOrganizationsForAutocompleteStandardized(
      ids
    );
  }
  autocompleteOrganizations = (query: string) => {
    return this.organizationService.autocompleteOrganizationOutcomesStandardized(
      query, this.organizationId
    );
  }
  loadExperts = (expertCodes: string[]) => {
    return this.expertAutocompleteService.findWidgetUserExpertOptions({
      expertCodes,
    });
  }
  autocompleteExperts = (query: string) => {
    return this.expertAutocompleteService.findWidgetUserExpertOptions({
      organizationIds: [this.organizationId],
      query,
    });
  }
  loadData = (filter: EvidenceFilter,
              pageInfo: {page: number, limit: number}) => {
    return this.widgetService.getOrganizationOutcomes(this.organizationId, filter, pageInfo);
  }

  mergeWrappers = (wrapper: EvidenceItemWrapper) => {
    if (this.itemWrapper) {
      this.itemWrapper.itemPreviewDtos.push(...wrapper.itemPreviewDtos);
    } else {
      this.itemWrapper = wrapper;
    }
    return {wrapper: this.itemWrapper, numOfNewItems: wrapper.itemPreviewDtos.length};
  }

  resetData = (wrapper: EvidenceItemWrapper) => {
    wrapper.itemPreviewDtos = [];
  }

  ngOnInit(): void {
    this.filterFormGroup.setControl('query', this.parentFormGroupQueryControl);
  }

  compare(o1: any, o2: any): boolean {
    return cmp(o1, o2);
  }
}
