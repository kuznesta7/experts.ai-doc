import {Component, Input, OnInit} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {CommercializationProjectWrapper} from 'ng-src/app/interfaces/commercialization-project-wrapper';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {map} from 'rxjs/operators';
import {EvidenceFilter} from '../../../interfaces/evidence-filter';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-commercialization',
  templateUrl: './commercialization.component.html',
  styleUrls: [],
})
export class CommercializationComponent implements OnInit {
  organizationId$: Observable<number>;
  commercializationProjectWrapper: CommercializationProjectWrapper;
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 10});
  loading$ = new BehaviorSubject(true);
  filterFormGroup = new FormGroup({
    query: new FormControl(''),
  });

  @Input()
  organizationId: number;

  @Input()
  parentFormGroupQueryControl: AbstractControl;

  @Input() set kwUpdate(value: string) {
    if (typeof value !== 'undefined') {
      this.parentFormGroupQueryControl?.setValue(value);
    }
  }

  constructor(
    private route: ActivatedRoute,
    private widgetService: WidgetService,
    private titleService: Title,
  ) {
    this.titleService.setTitle('Widget - Commercialization');
  }

  loadData = (filter: EvidenceFilter,
              pageInfo: {page: number, limit: number}) => {
    return this.widgetService.getOrganizationCommercialization(this.organizationId, filter, pageInfo);
  }

  mergeWrappers = (wrapper: CommercializationProjectWrapper) => {
    if (this.commercializationProjectWrapper) {
      this.commercializationProjectWrapper.evidenceCommercializationDtos.push(...wrapper.evidenceCommercializationDtos);
    } else {
      this.commercializationProjectWrapper = wrapper;
    }
    return {wrapper: this.commercializationProjectWrapper, numOfNewItems: wrapper.evidenceCommercializationDtos.length};
  }

  resetData = (wrapper: CommercializationProjectWrapper) => {
    wrapper.evidenceCommercializationDtos = [];
  }

  ngOnInit(): void {
    this.filterFormGroup.setControl('query', this.parentFormGroupQueryControl);
    this.organizationId$ = this.route.params.pipe(
      map((p) => this.organizationId)
    );
  }
}
