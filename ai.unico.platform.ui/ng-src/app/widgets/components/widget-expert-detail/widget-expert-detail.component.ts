import {Component, Input, OnInit} from '@angular/core';
import {filter, map, shareReplay, switchMap, tap} from 'rxjs/operators';
import {UrlParams} from 'ng-src/app/constants/url-params';
import {ExpertDetailDto} from 'ng-src/app/services/expert-detail.service';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {tapAllHandleError} from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {AuthorizationService} from 'ng-src/app/services/authorization.service';
import {EvidenceFilter} from 'ng-src/app/interfaces/evidence-filter';
import {QueryParamService} from 'ng-src/app/shared/services/query-param.service';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {ActivatedRoute} from '@angular/router';
import {RecommendationWrapper} from 'ng-src/app/interfaces/recommendation-wrapper';
import {OrganizationBaseDto} from '../../../interfaces/organization-base-dto';
import {WidgetUtilService} from '../../utils/widget-util.service';

@Component({
  selector: 'app-widget-expert-detail',
  templateUrl: './widget-expert-detail.component.html'
})
export class WidgetExpertDetailComponent implements OnInit {
  expertDetail$: Observable<ExpertDetailDto>;
  detailLoading$ = new BehaviorSubject<boolean>(true);

  @Input()
  filter: EvidenceFilter;

  @Input()
  recommendationWrapper: RecommendationWrapper;

  @Input()
  set expertCode(expertCode: string) {
    this.expertCode$.next(expertCode);
  }

  private expertCode$ = new BehaviorSubject<string | null>(null);

  outcomeListModes = OutcomeListMode;
  outcomeListModeDetails: OutcomeListModeDetail[] = [
    {
      outcomeListMode: OutcomeListMode.COMMERCIALIZATION,
      title: 'Commercialization projects',
      countProperty: 'numberOfInvestmentProjects',
      icon: 'rocket' as any
    }, {
      outcomeListMode: OutcomeListMode.RESEARCH_OUTCOME,
      title: 'Research outcomes',
      countProperty: 'numberOfAllItems',
      icon: 'file-alt' as any
    }, {
      outcomeListMode: OutcomeListMode.RESEARCH_PROJECT,
      title: 'Research projects',
      countProperty: 'numberOfProjects',
      icon: 'microscope' as any
    },
  ];
  outcomeListMode$ = new BehaviorSubject<OutcomeListMode | null>(null);
  outcomeListPageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 10});

  constructor(private widgetService: WidgetService,
              private authorizationService: AuthorizationService,
              private queryParamService: QueryParamService,
              private activatedRoute: ActivatedRoute,
              private widgetUtil: WidgetUtilService) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.pipe(
      map(p => p[UrlParams.EXPERT_CODE]),
      filter(code => !!code)
    ).subscribe(code => this.expertCode$.next(code));
    this.outcomeListMode$.subscribe(() => {
      this.outcomeListPageInfo$.next({...this.outcomeListPageInfo$.value, page: 1});
    });
    this.expertDetail$ = combineLatest([this.expertCode$]).pipe(
      filter(([e]) => !!e),
      tap(() => this.detailLoading$.next(true)),
      switchMap(([expertCode]) => this.widgetService.getExpertDetail(expertCode as string).pipe(
        tap(expertDetail => {
          if (expertDetail.numberOfInvestmentProjects > 0) {
            this.outcomeListMode$.next(OutcomeListMode.COMMERCIALIZATION);
          } else if (expertDetail.numberOfAllItems > 0) {
            this.outcomeListMode$.next(OutcomeListMode.RESEARCH_OUTCOME);
          } else if (expertDetail.numberOfProjects > 0) {
            this.outcomeListMode$.next(OutcomeListMode.RESEARCH_PROJECT);
          }
        }),
        tapAllHandleError(() => this.detailLoading$.next(false))
      )),
      shareReplay()
    );

    const filterFromUrl = this.queryParamService.getFilter({[UrlParams.SEARCH_QUERY]: ''});
    this.filter = {query: filterFromUrl[UrlParams.SEARCH_QUERY]};
  }

  setMode(mode: OutcomeListModeDetail, expertDetail: ExpertDetailDto): void {
    if (!(expertDetail as any)[mode.countProperty]) {
      return;
    }
    this.outcomeListMode$.next(mode.outcomeListMode);
  }

  back(): void {
    window.history.back();
  }

  checkOrganizationLicense(baseDto: OrganizationBaseDto): void {
    this.widgetUtil.checkOrganizationLicense(baseDto);
  }
}

enum OutcomeListMode {
  COMMERCIALIZATION, RESEARCH_OUTCOME, RESEARCH_PROJECT
}

interface OutcomeListModeDetail {
  outcomeListMode: OutcomeListMode;
  title: string;
  countProperty: string;
  icon: any;
}

