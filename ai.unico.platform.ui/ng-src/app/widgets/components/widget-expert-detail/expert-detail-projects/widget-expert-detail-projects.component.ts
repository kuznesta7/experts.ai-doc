import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { debounceTime, filter, shareReplay, startWith, switchMap, tap } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';
import { ExpertDetailService } from 'ng-src/app/services/expert-detail.service';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { ProjectWrapper } from 'ng-src/app/interfaces/project-wrapper';

@Component({
  selector: 'app-widget-expert-detail-projects',
  templateUrl: './widget-expert-detail-projects.component.html'
})
export class WidgetExpertDetailProjectsComponent implements OnInit {

  @Input()
  set expertCode(expertCode: string) {
    this.expertCode$.next(expertCode);
  }

  expertCode$ = new BehaviorSubject<string | null>(null);
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 5});
  expertProjectsWrapper$: Observable<ProjectWrapper>;
  loading$ = new BehaviorSubject(true);

  selectedYear: number;
  years: number[] = [];

  filterFormGroup = new FormGroup({
    query: new FormControl(''),
    resultTypeGroupIds: new FormControl([]),
    yearFrom: new FormControl([]),
    yearTo: new FormControl([])
  });

  constructor(private expertDetailService: ExpertDetailService) {
    this.selectedYear = new Date().getFullYear();
    for (let year = this.selectedYear; year >= 1970; year--) {
      this.years.push(year);
    }
  }

  ngOnInit(): void {
    const filter$ = this.filterFormGroup.valueChanges.pipe(
      debounceTime(500),
      tap(() => this.pageInfo$.next({...this.pageInfo$.value, page: 1})),
      startWith(this.filterFormGroup.value)
    );
    this.expertProjectsWrapper$ = combineLatest([this.expertCode$, filter$, this.pageInfo$]).pipe(
      filter(([exertCode]) => !!exertCode),
      debounceTime(0),
      tap(() => this.loading$.next(true)),
      switchMap(([expertCode, f, pageInfo]) => {
        return this.expertDetailService.getExpertProjects(expertCode || '', f, pageInfo).pipe(
          tapAllHandleError(() => this.loading$.next(false))
        );
      }),
      shareReplay()
    );
  }

}
