import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {debounceTime, shareReplay, startWith, switchMap} from 'rxjs/operators';
import {EvidenceWrapper} from '../../../interfaces/evidence-wrapper';
import {BehaviorSubject, combineLatest, Observable, Observer} from 'rxjs';
import {tapAllHandleError} from '../../../shared/operators/tap-all-handle-error-operator';
import {PageInfo} from '../../../shared/interfaces/page-info';
import {ListMode} from '../../../services/organization-detail.service';
import {FormGroup} from '@angular/forms';
import {RecommendationWrapper} from 'ng-src/app/interfaces/recommendation-wrapper';
import {BaseFilter} from '../../../interfaces/base-filter';
import {RecombeeService} from '../../../services/recombee.service';

@Component({
  selector: 'app-infinite-scroll',
  templateUrl: './infinite-scroll.component.html'
})
export class InfiniteScrollComponent<T extends EvidenceWrapper, F extends BaseFilter> implements OnInit, OnDestroy {

  constructor(private recombeeService: RecombeeService) { }

  static LOCAL_STORAGE_NAME = 'infiniteScrollWrapper';
  LIMIT = 10;

  wrapper: T;
  loadingNew$ = new BehaviorSubject<boolean>(true);
  pageInfo$ = new BehaviorSubject<PageInfo>({ page: 1, limit: this.LIMIT });
  lastFilter: F;
  page = 1;
  loading = false;
  scroll = 0;
  stopLoading = false;
  pageStats$ = new BehaviorSubject<{ current: number; all: number; } | null>(null);
  count = 0;
  recommendationWrapper: RecommendationWrapper;
  filter$: Observable<F>;
  addedPlus = false;

  @Input() loading$: BehaviorSubject<boolean>;
  @Input() loadDataFunction: (filter: F, pageInfo: PageInfo) => Observable<T>;
  @Input() resetDataFunction: (wrapper: T) => void;
  @Input() mergeWrappersFunction: (wrapper: T) => {wrapper: T, numOfNewItems: number};
  @Input() widgetType: ListMode;
  @Input() filterFormGroup: FormGroup;
  @ViewChild('infiniteScroll', {static: true}) infiniteScrollComponent: ElementRef;
  @Input() afterSubscribeFunction: (filter: F, pageInfo: PageInfo) => void = () => { return; };

  ngOnInit(): void {
    this.filter$ = this.filterFormGroup.valueChanges.pipe(
      debounceTime(300),
      startWith(this.filterFormGroup.value as F)
    );

    const wrapper$ = combineLatest([
      this.filter$,
      this.pageInfo$
    ]).pipe(
      debounceTime(100),
      switchMap(([filter, pageInfo]) => this.loadData(filter, pageInfo)
        .pipe(tapAllHandleError(() => this.loading$.next(false)))
        .pipe(tapAllHandleError(() => this.loadingNew$.next(false)))),
      shareReplay()
    );
    wrapper$.subscribe((wrapper: T) => {
      const wrapperStats = this.mergeWrappersFunction(wrapper);
      this.wrapper = wrapperStats.wrapper;
      this.wrapper.recommendationWrapper = wrapper.recommendationWrapper;
      this.count += wrapperStats.numOfNewItems;
      this.pageStats$.next({current: this.count, all: wrapper.numberOfAllItems});
      this.setPlus(this.count, wrapper.numberOfAllItems);
      if (wrapper.recommendationWrapper) {
        this.recombeeService.setRecommendationWrapper(wrapper.recommendationWrapper);
      }
      if (wrapperStats.numOfNewItems === 0){
        this.stopLoading = true;
        this.pageStats$.next({current: this.count, all: this.count});
      }
      this.scrollToLastSeen();
    });

    this.loadingNew$.subscribe((loading: boolean) => {
      this.loading = loading;
    });
  }

  loadData(filter: F, pageInfo: PageInfo): Observable<T> {
    if (pageInfo.page === 1) {
      this.loading$.next(true);
    }
    this.loadingNew$.next(true);
    const cachedWrapper = this.loadCachedWrapper();
    this.afterSubscribeFunction(filter, pageInfo);
    if (cachedWrapper) {
      this.page = cachedWrapper.page;
      this.scroll = cachedWrapper.scroll;
      this.recommendationWrapper = cachedWrapper.recommendationWrapper;
      if (this.isLocalStorageAvailable()) {
        localStorage.removeItem(InfiniteScrollComponent.LOCAL_STORAGE_NAME);
      }
      return new Observable((observer: Observer<T>) => {
        observer.next(cachedWrapper.wrapper);
        observer.complete();
      });
    }
    this.persistFilter(filter, pageInfo);
    if (this.wrapper && this.wrapper.recommendationWrapper) {
      filter.recommId = this.wrapper.recommendationWrapper.recommId;
    }
    return this.loadDataFunction(filter, pageInfo);
  }

  setPlus(current: number, all: number): void {
    this.addedPlus = all % 50 === 0 && current !== all;
  }

  scrollToLastSeen(): void {
    if (this.scroll > 0){
      setTimeout(() => {
        this.infiniteScrollComponent.nativeElement.scrollTo({
          top: this.scroll,
          behaviour: 'auto',
        });
        this.scroll = 0;
      }, 50);
    }
  }

  persistFilter(filter: F, pageInfo: PageInfo): void {
    if (! this.lastFilter) {
      this.lastFilter = filter;
      return;
    } else if (this.lastFilter !== filter && this.wrapper){
      this.resetData(pageInfo);
    }
    this.lastFilter = filter;
  }

  onScroll(event: any): void {
    // -5 because the scrolling is not done pixel by pixel
    if (event.target.offsetHeight + event.target.scrollTop >= event.target.scrollHeight - 5) {
      // page scroll to the bottom
      if (!this.loading && !this.stopLoading){
        this.pageInfo$.next({page: ++this.page, limit: this.LIMIT});
        this.loading = true;
      }
    }
  }

  resetData(pageInfo: PageInfo): void {
    this.page = 1;
    this.stopLoading = false;
    this.count = 0;
    this.resetDataFunction(this.wrapper);
    pageInfo.page = 1;
  }

  ngOnDestroy(): void {
    if (this.isLocalStorageAvailable()) {
      localStorage.setItem(InfiniteScrollComponent.LOCAL_STORAGE_NAME, JSON.stringify({
        wrapper: this.wrapper,
        filter: this.lastFilter,
        type: this.widgetType,
        page: this.page,
        scroll: this.infiniteScrollComponent.nativeElement.scrollTop,
        recommendationWrapper: this.wrapper.recommendationWrapper
      }));
    }
  }

  loadCachedWrapper(): {wrapper: T, filter: F, type: ListMode, page: number, scroll: number, recommendationWrapper: RecommendationWrapper} | null {
    if (! this.isLocalStorageAvailable()) {
      return null;
    }
    const cached = JSON.parse(localStorage.getItem(InfiniteScrollComponent.LOCAL_STORAGE_NAME) as string);
    if (! cached || cached.type !== this.widgetType){
      return null;
    }
    return cached;
  }

  isLocalStorageAvailable(): boolean {
    try {
      return typeof window.localStorage !== 'undefined';
    } catch (e) {
      return false;
    }
  }

}
