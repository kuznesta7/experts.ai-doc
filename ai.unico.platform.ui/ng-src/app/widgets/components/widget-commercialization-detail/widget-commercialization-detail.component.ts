import { Component, OnInit } from '@angular/core';
import { CommercializationDetailDto } from 'ng-src/app/interfaces/commercialization-detail-dto';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { CommercializationOrganizationRelation } from 'ng-src/app/services/commercialization-enum.service';
import { ActivatedRoute } from '@angular/router';
import { map, shareReplay, switchMap, tap } from 'rxjs/operators';
import { UrlParams } from 'ng-src/app/constants/url-params';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { WidgetService } from 'ng-src/app/services/widget.service';

@Component({
  selector: 'app-widget-commercialization-detail',
  templateUrl: './widget-commercialization-detail.component.html',
})
export class WidgetCommercializationDetailComponent implements OnInit {

  loading$ = new BehaviorSubject(true);
  commercializationDetail$: Observable<CommercializationDetailDto>;
  commercializationOrganizationRelations = CommercializationOrganizationRelation;

  constructor(private widgetService: WidgetService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    const commercializationId$ = this.route.params.pipe(
      map(p => p[UrlParams.COMMERCIALIZATION_ID])
    );
    this.commercializationDetail$ = combineLatest([commercializationId$]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([id]) => this.widgetService.getCommercializationDetail(id)),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );
  }

  back(): void {
    window.history.back();
  }
}
