import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';
import {BehaviorSubject, Observable} from 'rxjs';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {QueryParamService} from 'ng-src/app/shared/services/query-param.service';
import {OrganizationAutocompleteService} from 'ng-src/app/services/organization-autocomplete.service';
import {ExpertAutocompleteService} from 'ng-src/app/services/expert-autocomplete.service';
import {EvidenceFilter} from 'ng-src/app/interfaces/evidence-filter';
import {EvidenceLectureWrapper} from 'ng-src/app/interfaces/evidence-lecture-wrapper';
import {EvidenceItemWrapper} from '../../../interfaces/evidence-item-wrapper';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-widget-lectures',
  templateUrl: './widget-lectures.component.html',
})
export class WidgetLecturesComponent implements OnInit {
  @Input()
  organizationId: number;

  @Input()
  parentFormGroupQueryControl: AbstractControl;

  @Input() set kwUpdate(value: string) {
    this.parentFormGroupQueryControl?.setValue(value);
  }

  filterFormGroup = new FormGroup({
    query: new FormControl(''),
    participatingOrganizationIds: new FormControl([]),
    expertCodes: new FormControl([]),
  });

  exportUrl$: Observable<string>;
  lectureWrapper: EvidenceLectureWrapper;
  itemWrapper: EvidenceItemWrapper;
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 10});
  loading$ = new BehaviorSubject(true);

  constructor(
    private widgetService: WidgetService,
    private queryParamService: QueryParamService,
    private organizationService: OrganizationAutocompleteService,
    private expertAutocompleteService: ExpertAutocompleteService,
    private titleService: Title,
  ) {
    this.titleService.setTitle('Widget - Lectures');
    this.queryParamService.handleFilterFormGroup(this.filterFormGroup, {
      numericFields: ['participatingOrganizationIds'],
    });
  }

  loadOrganizations = (ids: number[]) => {
    return this.organizationService.getOrganizationsForAutocompleteStandardized(
      ids
    );
  }
  autocompleteOrganizations = (query: string) => {
    return this.organizationService.autocompleteOrganizationsStandardized(
      query
    );
  }
  loadExperts = (expertCodes: string[]) => {
    return this.expertAutocompleteService.findUserExpertOptions({
      expertCodes,
    });
  }
  autocompleteExperts = (query: string) => {
    return this.expertAutocompleteService.findUserExpertOptions({query});
  }

  loadData = (filter: EvidenceFilter,
              pageInfo: {page: number, limit: number}) => {
    return this.widgetService.getLectures(this.organizationId, filter, pageInfo);
  }

  mergeWrappers = (wrapper: EvidenceLectureWrapper) => {
    if (this.lectureWrapper) {
      this.lectureWrapper.lecturePreviewDtos.push(...wrapper.lecturePreviewDtos);
    } else {
      this.lectureWrapper = wrapper;
    }
    return {wrapper: this.lectureWrapper, numOfNewItems: wrapper.lecturePreviewDtos.length};
  }

  resetData = (wrapper: EvidenceLectureWrapper) => {
    wrapper.lecturePreviewDtos = [];
  }

  ngOnInit(): void {
    this.filterFormGroup.setControl('query', this.parentFormGroupQueryControl);
  }
}
