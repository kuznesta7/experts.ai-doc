import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {EvidenceExpertWrapper} from 'ng-src/app/interfaces/evidence-expert-wrapper';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';
import {ExpertOrganizationRelation} from 'ng-src/app/enums/expert-organization-relation';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {map, shareReplay} from 'rxjs/operators';
import {ActivatedRoute, Params} from '@angular/router';
import {EvidenceFilter} from 'ng-src/app/interfaces/evidence-filter';
import {EvidenceExpertPreview} from 'ng-src/app/interfaces/evidence-expert-preview';
import {GoogleAnalyticsService} from 'ng-src/app/services/google-analytics.service';
import {GoogleAnalyticsCategories} from 'ng-src/app/enums/google-analytics-categories';
import {QueryParamService} from '../../../shared/services/query-param.service';
import {OrganizationAutocompleteService} from '../../../services/organization-autocomplete.service';
import {InfiniteScrollComponent} from '../infinite-scroll/infinite-scroll.component';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-widget-experts',
  templateUrl: './widget-experts.component.html',
})
export class WidgetExpertsComponent implements OnInit {

  @Input() set kwUpdate(value: string) {
    if (typeof value !== 'undefined') {
      this.parentFormGroupQueryControl?.setValue(value);
    }
  }

  @Input()
  organizationId: number;

  @Input()
  parentFormGroupQueryControl: AbstractControl;

  @ViewChild('infiniteScrollComponent', {static: true}) infiniteScrollComponent: ElementRef<InfiniteScrollComponent<EvidenceExpertWrapper, EvidenceFilter>>;


  constructor(
    private route: ActivatedRoute,
    private widgetService: WidgetService,
    private googleAnalyticsService: GoogleAnalyticsService,
    private queryParamService: QueryParamService,
    private organizationService: OrganizationAutocompleteService,
    private titleService: Title,
  ) {
    this.titleService.setTitle('Widget - Experts');
    this.organizationId$ = this.route.params.pipe(
      map(() => this.organizationId)
    );
    this.queryParamService.handleFilterFormGroup(this.filterFormGroup);
  }
  selected: Set<string> = new Set<string>();
  filterFormGroup = new FormGroup({
    query: new FormControl(''),
    organizationRelatedStatistics: new FormControl(false),
    verifiedOnly: new FormControl(false),
    notVerifiedOnly: new FormControl(false),
    retired: new FormControl(false),
    expertSearchType: new FormControl(''),
    suborganizationIds: new FormControl([]),
  });


  organizationId$: Observable<number>;
  page = 1;
  experts: EvidenceExpertPreview[] = [];
  expertWrapper: EvidenceExpertWrapper;

  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 10});
  loading$ = new BehaviorSubject(true);
  expertOrganizationRelation$: Observable<ExpertOrganizationRelation> =
    this.route.data.pipe(
      map((d) => d.expertOrganizationRelation),
      shareReplay()
    );

  loadOrganizations = (ids: number[]) => {
    return this.organizationService.getOrganizationsForAutocompleteStandardized(
      ids
    );
  }
  autocompleteOrganizations = (query: string) => {
    return this.organizationService.autocompleteOrganizationSuborganizationsStandardized(query, this.organizationId);
  }

  loadData = (filter: EvidenceFilter, pageInfo: {page: number, limit: number}) => {
    return this.widgetService.getOrganizationExperts(this.organizationId, filter, pageInfo);
  }

  mergeWrappers = (wrapper: EvidenceExpertWrapper) => {
    this.googleAnalyticsService.receiveRecommendation(
      GoogleAnalyticsCategories.EXPERT,
      wrapper.page,
      JSON.stringify(wrapper.analyticsExpertPreviewDtos.map((expert: EvidenceExpertPreview) => expert.expertCode))
    );
    if (this.expertWrapper) {
      this.expertWrapper.analyticsExpertPreviewDtos.push(...wrapper.analyticsExpertPreviewDtos);
    } else {
      this.expertWrapper = wrapper;
    }
    return {wrapper: this.expertWrapper, numOfNewItems: wrapper.analyticsExpertPreviewDtos.length};
  }

  resetData = (wrapper: EvidenceExpertWrapper) => {
    wrapper.analyticsExpertPreviewDtos = [];
  }

  ngOnInit(): void {
    this.filterFormGroup.setControl('query', this.parentFormGroupQueryControl);
    this.route.params.subscribe((params: Params) => {
      if (this.organizationId !== Number(params.organizationId)) {
        this.organizationId = Number(params.organizationId);
        window.location.reload();
      }
    });
  }

  select(expertCode: string): void {
    if (!this.selected.delete(expertCode)) {
      this.selected.add(expertCode);
    }
  }
}
