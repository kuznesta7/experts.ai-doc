import {Component, Input, OnInit} from '@angular/core';
import {ProjectWrapper} from 'ng-src/app/interfaces/project-wrapper';
import {BehaviorSubject, Observable} from 'rxjs';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {ActivatedRoute} from '@angular/router';
import {map} from 'rxjs/operators';
import {EvidenceFilter} from 'ng-src/app/interfaces/evidence-filter';
import {OrganizationAutocompleteService} from 'ng-src/app/services/organization-autocomplete.service';
import {ExpertAutocompleteService} from 'ng-src/app/services/expert-autocomplete.service';
import {cmp} from '../../../shared/utils/compare-util';
import {QueryParamService} from '../../../shared/services/query-param.service';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-projects',
  templateUrl: './widget-projects.component.html',
  styleUrls: [],
})
export class WidgetProjectsComponent implements OnInit {
  filterFormGroup = new FormGroup({
    query: new FormControl(''),
    participatingOrganizationIds: new FormControl([]),
    yearFrom: new FormControl([]),
    yearTo: new FormControl([]),
    expertCodes: new FormControl([]),
  });

  organizationId$: Observable<number>;
  projectWrapper: ProjectWrapper;
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 10});
  loading$ = new BehaviorSubject(true);
  selectedYear: number;
  years: number[] = [];

  @Input()
  organizationId: number;

  @Input()
  parentFormGroupQueryControl: AbstractControl;

  @Input() set kwUpdate(value: string) {
    if (typeof value !== 'undefined') {
      this.parentFormGroupQueryControl?.setValue(value);
    }
  }

  constructor(
    private organizationService: OrganizationAutocompleteService,
    private expertAutocompleteService: ExpertAutocompleteService,
    private route: ActivatedRoute,
    private widgetService: WidgetService,
    private queryParamService: QueryParamService,
    private titleService: Title,
  ) {
    this.titleService.setTitle('Widget - Projects');
    this.organizationId$ = this.route.params.pipe(
      map((p) => this.organizationId, 10)
    );

    this.selectedYear = new Date().getFullYear() + 5;
    for (let year = this.selectedYear; year >= 1970; year--) {
      this.years.push(year);
    }

    this.queryParamService.handleFilterFormGroup(this.filterFormGroup);
  }

  loadOrganizations = (ids: number[]) => {
    return this.organizationService.getOrganizationsForAutocompleteStandardized(
      ids
    );
  }
  autocompleteOrganizations = (query: string) => {
    return this.organizationService.autocompleteOrganizationProjectStandardized(
      query, this.organizationId
    );
  }
  loadExperts = (expertCodes: string[]) => {
    return this.expertAutocompleteService.findWidgetUserExpertOptions({
      expertCodes,
    });
  }
  autocompleteExperts = (query: string) => {
    return this.expertAutocompleteService.findWidgetUserExpertOptions({
      organizationIds: [this.organizationId],
      query,
    });
  }
  loadData = (filter: EvidenceFilter,
              pageInfo: {page: number, limit: number}) => {
    return this.widgetService.getProjects(this.organizationId, filter, pageInfo);
  }

  mergeWrappers = (wrapper: ProjectWrapper) => {
    if (this.projectWrapper) {
      this.projectWrapper.projectPreviewDtos.push(...wrapper.projectPreviewDtos);
    } else {
      this.projectWrapper = wrapper;
    }
    return {wrapper: this.projectWrapper, numOfNewItems: wrapper.projectPreviewDtos.length};
  }

  resetData = (wrapper: ProjectWrapper) => {
    wrapper.projectPreviewDtos = [];
  }

  ngOnInit(): void {
    this.filterFormGroup.setControl('query', this.parentFormGroupQueryControl);
  }

  compare(o1: any, o2: any): boolean {
    return cmp(o1, o2);
  }
}
