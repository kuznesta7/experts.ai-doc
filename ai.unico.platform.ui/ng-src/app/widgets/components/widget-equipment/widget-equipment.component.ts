import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';
import {BehaviorSubject} from 'rxjs';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {QueryParamService} from 'ng-src/app/shared/services/query-param.service';
import {OrganizationAutocompleteService} from 'ng-src/app/services/organization-autocomplete.service';
import {EvidenceEquipmentWrapper} from 'ng-src/app/interfaces/evidence-equipment-wrapper';
import {EquipmentFilter} from 'ng-src/app/interfaces/equipment-filter';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-widget-equipment',
  templateUrl: './widget-equipment.component.html',
})
export class WidgetEquipmentComponent implements OnInit {
  @Input()
  organizationId: number;

  @Input()
  parentFormGroupQueryControl: AbstractControl;

  @Input() set kwUpdate(value: string) {
    this.parentFormGroupQueryControl?.setValue(value);
  }

  filterFormGroup = new FormGroup({
    query: new FormControl(''),
  });

  evidenceEquipmentWrapper: EvidenceEquipmentWrapper;
  loading$ = new BehaviorSubject(true);

  constructor(
    private widgetService: WidgetService,
    private queryParamService: QueryParamService,
    private organizationService: OrganizationAutocompleteService,
    private titleService: Title,
  ) {
    this.titleService.setTitle('Widget - Equipment');
    this.queryParamService.handleFilterFormGroup(this.filterFormGroup, {
      numericFields: ['participatingOrganizationIds'],
    });
  }

  loadOrganizations = (ids: number[]) => {
    return this.organizationService.getOrganizationsForAutocompleteStandardized(
      ids
    );
  }
  autocompleteOrganizations = (query: string) => {
    return this.organizationService.autocompleteOrganizationsStandardized(
      query
    );
  }
  loadData = (filter: EquipmentFilter,
              pageInfo: {page: number, limit: number}) => {
    return this.widgetService.getOrganizationEquipment(this.organizationId, filter, pageInfo);
  }

  mergeWrappers = (wrapper: EvidenceEquipmentWrapper) => {
    if (this.evidenceEquipmentWrapper) {
      this.evidenceEquipmentWrapper.multilingualPreviews.push(...wrapper.multilingualPreviews);
    } else {
      this.evidenceEquipmentWrapper = wrapper;
    }
    console.log(this.evidenceEquipmentWrapper);
    return {wrapper: this.evidenceEquipmentWrapper, numOfNewItems: wrapper.multilingualPreviews.length};
  }

  resetData = (wrapper: EvidenceEquipmentWrapper) => {
    wrapper.multilingualPreviews = [];
  }

  ngOnInit(): void {
    this.filterFormGroup.setControl('query', this.parentFormGroupQueryControl);
    console.log(this.evidenceEquipmentWrapper);
  }
}
