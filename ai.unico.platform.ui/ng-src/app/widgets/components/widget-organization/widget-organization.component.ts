import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';
import {BehaviorSubject, Observable} from 'rxjs';
import {OrganizationWrapper} from 'ng-src/app/interfaces/organization-wrapper';
import {ActivatedRoute} from '@angular/router';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {debounceTime, map, share, startWith, switchMap} from 'rxjs/operators';
import {EvidenceFilter} from '../../../interfaces/evidence-filter';
import {IdDescriptionOption} from "../../../shared/interfaces/idDescriptionOption";
import {OrganizationTypeService} from "../../../services/organization-type.service";
import {QueryParamService} from "../../../shared/services/query-param.service";
import {Title} from "@angular/platform-browser";
import {OrganizationBaseDto} from "../../../interfaces/organization-base-dto";
import {OrganizationDetailService} from "../../../services/organization-detail.service";

@Component({
  selector: 'app-widget-organization',
  templateUrl: './widget-organization.component.html',
})
export class WidgetOrganizationComponent implements OnInit {
  organizationId$: Observable<number>;
  organizationWrapper: OrganizationWrapper;
  parentOrganizations$: Observable<OrganizationBaseDto[]>;
  memberOrganizations$: Observable<OrganizationBaseDto[]>;
  loading$ = new BehaviorSubject(true);
  filterFormGroup = new FormGroup({
    query: new FormControl(''),
    organizationTypesIds: new FormControl([]),
  });

  @Input()
  organizationId: number;

  @Input()
  parentFormGroupQueryControl: AbstractControl;

  @Input() set kwUpdate(value: string) {
    this.parentFormGroupQueryControl?.setValue(value);
  }

  organizationsByType$: Observable<IdDescriptionOption[]> = this.organizationTypeService
    .getOrganizationsTypes()
    .pipe(
      map((types) =>
        types.map((t) => {
          return {id: t.id, description: t.name};
        })
      )
    );

  constructor(
    private route: ActivatedRoute,
    private widgetService: WidgetService,
    private queryParamService: QueryParamService,
    private organizationTypeService: OrganizationTypeService,
    private titleService: Title,
    private organizationDetailService: OrganizationDetailService
  ) {
    this.titleService.setTitle('Widget - Organizations');
    this.queryParamService.handleFilterFormGroup(this.filterFormGroup, {
      numericFields: ['organizationTypesIds'],
    });
  }

  loadData = (filter: EvidenceFilter,
              pageInfo: {page: number, limit: number}) => {
    return this.widgetService.getOrganizationChildren(this.organizationId, filter, pageInfo);
  }

  mergeWrappers = (wrapper: OrganizationWrapper) => {
    if (this.organizationWrapper) {
      this.organizationWrapper.organizationBaseDtos.push(...wrapper.organizationBaseDtos);
    } else {
      this.organizationWrapper = wrapper;
    }
    return {wrapper: this.organizationWrapper, numOfNewItems: wrapper.organizationBaseDtos.length};
  }

  resetData = (wrapper: OrganizationWrapper) => {
    wrapper.organizationBaseDtos = [];
  }

  ngOnInit(): void {
    this.filterFormGroup.setControl('query', this.parentFormGroupQueryControl);
    this.organizationId$ = this.route.params.pipe(
      map((p) => this.organizationId)
    );
    this.filterFormGroup.setControl('query', this.parentFormGroupQueryControl);
    this.parentOrganizations$ = this.organizationId$.pipe(
      switchMap(organizationId => this.organizationDetailService.findParents(organizationId)),
      share()
    );
    this.memberOrganizations$ = this.organizationId$.pipe(
      switchMap(organizationId => this.organizationDetailService.findMyMemberships(organizationId)),
      share()
    )
  }
}
