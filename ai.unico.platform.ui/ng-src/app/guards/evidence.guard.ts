import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot } from '@angular/router';
import { combineLatest, Observable, of } from 'rxjs';
import { FlashService } from 'ng-src/app/shared/services/flash.service';
import { SecurityService } from 'ng-src/app/services/security.service';
import { UrlParams } from 'ng-src/app/constants/url-params';
import { SessionOrganizationService } from 'ng-src/app/services/session-organization.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EvidenceGuard implements CanActivate, CanActivateChild {

  constructor(private securityService: SecurityService,
              private sessionOrganizationService: SessionOrganizationService,
              private flashService: FlashService) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.checkAuthorization(next);
  }

  canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.checkAuthorization(next);
  }

  private checkAuthorization(next: ActivatedRouteSnapshot): Observable<boolean> {
    this.flashService.dismissAll();
    const organizationId = next.params[UrlParams.ORGANIZATION_ID];
    if (!organizationId) {
      return of(true);
    }
    this.sessionOrganizationService.updateActiveOrganization(organizationId);
    return combineLatest([this.securityService.isPortalMaintainer(), this.securityService.hasOrganizationRole(organizationId)]).pipe(
      map(([maintainer, organizationUser]) => maintainer || organizationUser)
    );
  }
}
