import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { FlashService } from 'ng-src/app/shared/services/flash.service';
import { SecurityService } from 'ng-src/app/services/security.service';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationGuard implements CanActivate, CanActivateChild {

  constructor(private securityService: SecurityService,
              private flashService: FlashService) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.checkAuthorization(next);
  }

  canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.checkAuthorization(next);
  }

  private checkAuthorization(next: ActivatedRouteSnapshot): Observable<boolean> {
    this.flashService.dismissAll();
    if (!(next.data?.requiredRoles?.size > 0)) return of(true);
    const result = this.securityService.hasOneOfRoles(next.data.requiredRoles);
    result.subscribe(r => {
      if (!r) this.flashService.error('You don\'t have permissions to access this page');
    });
    return result;
  }
}
