export class MessageBus {
  public channelName: string;
  public data: any;

  constructor(channelName: string, data: any) {
    this.channelName = channelName;
    this.data = data;
  }
}
