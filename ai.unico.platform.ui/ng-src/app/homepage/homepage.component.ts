import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {LoginService, SidebarMode} from 'ng-src/app/services/login.service';
import {FormControl} from '@angular/forms';
import {ExploreService, SearchSuggestion} from 'ng-src/app/services/explore.service';
import {Observable, of} from 'rxjs';
import {debounceTime, shareReplay, switchMap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {PathService} from 'ng-src/app/shared/services/path.service';
import {FlashService} from 'ng-src/app/shared/services/flash.service';
import {UiThemeService} from "../shared/services/ui-theme.service";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./scss/homepage.component.scss']
})
export class HomepageComponent implements OnInit, AfterViewInit {

  formControl = new FormControl('');
  suggestions$: Observable<SearchSuggestion[] | null>;
  trending$ = this.exploreService.findSuggestions('', 10, true);

  @ViewChild('input')
  inputElement: ElementRef;

  constructor(private loginService: LoginService,
              private router: Router,
              private pathService: PathService,
              private flashService: FlashService,
              private exploreService: ExploreService,
              private uiThemeService: UiThemeService,
              private titleService: Title) {
    this.titleService.setTitle('Experts - Homepage');
  }

  ngOnInit(): void {
    this.uiThemeService.loadTheme();
    this.suggestions$ = this.formControl.valueChanges.pipe(
      debounceTime(300),
      switchMap(v => {
        if (!v) {
          return of(null);
        }
        return this.exploreService.findSuggestions(v, 10);
      }),
      shareReplay()
    );
  }

  login(): void {
    this.loginService.openLoginSidebar(SidebarMode.LOG_IN);
  }

  register(): void {
    this.loginService.openLoginSidebar(SidebarMode.REGISTER);
  }

  doSearch(value: string): void {
    this.router.navigate([this.pathService.getPathWithReplacedParams('exploreExperts')], {queryParams: {searchQuery: value}});
  }

  ngAfterViewInit(): void {
    this.inputElement.nativeElement.focus();
  }

  learnMoreAboutExperts(): void {
    this.flashService.error('Not implemented!');
  }
}
