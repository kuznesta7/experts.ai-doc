import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageComponent } from 'ng-src/app/homepage/homepage.component';
import { HomepageRoutingModule } from 'ng-src/app/homepage/homepage-routing.module';
import { SharedModule } from 'ng-src/app/shared/shared.module';


@NgModule({
  declarations: [HomepageComponent],
  bootstrap: [HomepageComponent],
  imports: [
    CommonModule,
    HomepageRoutingModule,
    SharedModule
  ]
})
export class HomepageModule {
}
