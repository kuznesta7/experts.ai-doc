import {ActivatedRoute, Event, NavigationEnd, Router, RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {filter} from 'rxjs/operators';
import {GoogleAnalyticsService} from 'ng-src/app/services/google-analytics.service';


declare var gtag: any;

const routes: Routes = [
  {
    path: 'widgets',
    loadChildren: () => import('ng-src/app/widgets/widgets.module').then(m => m.WidgetsModule),
  },
  {
    path: 'login',
    loadChildren: () => import('ng-src/app/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'subscribe',
    loadChildren: () => import('ng-src/app/subscribe/subscribe.module').then(m => m.SubscribeModule)
  },
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () => import('ng-src/app/homepage/homepage.module').then(m => m.HomepageModule)
  },
  {
    path: '',
    loadChildren: () => import('ng-src/app/platform/platform.module').then(m => m.PlatformModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {paramsInheritanceStrategy: 'always'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor(router: Router, route: ActivatedRoute) {
    const googleAnalyticsService = new GoogleAnalyticsService(route);
    router.events.pipe(
      filter((event: Event) => event instanceof NavigationEnd)
    ).subscribe((event: any) => {
      gtag('config', 'G-GB0J15D8E7', {
        page_path: event.urlAfterRedirects,
        ...googleAnalyticsService.getUserData(),
        cookie_flags: 'SameSite=None;Secure'
      });
    });
  }
}
