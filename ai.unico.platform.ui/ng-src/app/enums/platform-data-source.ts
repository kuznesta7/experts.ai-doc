export enum PlatformDataSource {
  STORE = 'ST',
  DWH = 'DWH'
}
