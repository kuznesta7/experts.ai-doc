export enum OrganizationStructureRelation {
  PARENT = 'PARENT', AFFILIATED = 'AFFILIATED', MEMBER = 'MEMBER'
}
