export enum GoogleAnalyticsCategories {
  EXPERT = 'expert',
  OPPORTUNITY = 'opportunity',
  RESEARCH_OUTCOME = 'item',
  RESEARCH_PROJECT = 'project',
  EQUIPMENT = 'equipment'
}
