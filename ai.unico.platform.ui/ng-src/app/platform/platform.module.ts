import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from 'ng-src/app/platform/components/layout/header/header.component';
import {NavigationComponent} from 'ng-src/app/platform/components/layout/navigation/navigation.component';
import {
  EvidenceExpertComponent
} from 'ng-src/app/platform/components/content/evidence-expert/evidence-expert.component';
import {
  EvidenceOutcomeComponent
} from 'ng-src/app/platform/components/content/evidence-outcome/evidence-outcome.component';
import {Oauth2CallbackComponent} from 'ng-src/app/platform/components/content/oauth2callback/oauth2-callback.component';
import {
  EvidenceProjectComponent
} from 'ng-src/app/platform/components/content/evidence-project/evidence-project.component';
import {
  OrganizationStructureNavigationComponent
} from 'ng-src/app/platform/components/layout/navigation/organizations-structure/organization-structure-navigation.component';
import {ExpertDetailComponent} from 'ng-src/app/platform/components/content/expert-detail/expert-detail.component';
import {
  ExpertDetailCommercializationComponent
} from 'ng-src/app/platform/components/content/expert-detail/expert-detail-commercialization/expert-detail-commercialization.component';
import {
  ExpertDetailOutcomesComponent
} from 'ng-src/app/platform/components/content/expert-detail/expert-detail-outcomes/expert-detail-outcomes.component';
import {
  ExpertDetailProjectsComponent
} from 'ng-src/app/platform/components/content/expert-detail/expert-detail-projects/expert-detail-projects.component';
import {
  ExpertDetailEditModalComponent
} from 'ng-src/app/platform/components/content/expert-detail/expert-detail-edit-modal/expert-detail-edit-modal.component';
import {FileDropComponent} from 'ng-src/app/platform/components/common/file-drop/file-drop.component';
import {
  ExploreHeaderComponent
} from 'ng-src/app/platform/components/content/explore/explore-header/explore-header.component';
import {
  ExploreExpertsComponent
} from 'ng-src/app/platform/components/content/explore/explore-experts/explore-experts.component';
import {
  EvidenceDashboardComponent
} from 'ng-src/app/platform/components/content/evidence-dashboard/evidence-dashboard.component';
import {
  CounterTileComponent
} from 'ng-src/app/platform/components/content/evidence-dashboard/counter-tile/counter-tile.component';
import {
  ProjectStatisticsTileComponent
} from 'ng-src/app/platform/components/content/evidence-dashboard/project-statistics-tile/project-statistics-tile.component';
import {
  OutcomeStatisticsTileComponent
} from 'ng-src/app/platform/components/content/evidence-dashboard/outcome-statistics-tile/outcome-statistics-tile.component';
import {
  KeywordStatisticsTileComponent
} from 'ng-src/app/platform/components/content/evidence-dashboard/keyword-statistics-tile/keyword-statistics-tile.component';
import {
  EvidenceCommercializationComponent
} from 'ng-src/app/platform/components/content/evidence-commercialization/evidence-commercialization.component';
import {
  OrganizationDetailComponent
} from 'ng-src/app/platform/components/content/organization-detail/organization-detail.component';
import {FileDropModalComponent} from 'ng-src/app/platform/components/common/file-drop-modal/file-drop-modal.component';
import {
  OrganizationDetailEditModalComponent
} from 'ng-src/app/platform/components/content/organization-detail/organization-detail-edit-modal/organization-detail-edit-modal.component';
import {
  OrganizationDetailExpertsComponent
} from 'ng-src/app/platform/components/content/organization-detail/organization-detail-experts/organization-detail-experts.component';
import {
  OrganizationDetailOutcomesComponent
} from 'ng-src/app/platform/components/content/organization-detail/organization-detail-outcomes/organization-detail-outcomes.component';
import {
  OrganizationDetailProjectsComponent
} from 'ng-src/app/platform/components/content/organization-detail/organization-detail-projects/organization-detail-projects.component';
import {OutcomeDetailComponent} from 'ng-src/app/platform/components/content/outcome-detail/outcome-detail.component';
import {ProjectDetailComponent} from 'ng-src/app/platform/components/content/project-detail/project-detail.component';
import {
  CommercializationDetailComponent
} from 'ng-src/app/platform/components/content/commercialization-detail/commercialization-detail.component';
import {
  ExpertSearchStatisticsTileComponent
} from 'ng-src/app/platform/components/content/evidence-dashboard/expert-search-statistics-tile/expert-search-statistics-tile.component';
import {
  ExpertVisitationStatisticsTileComponent
} from 'ng-src/app/platform/components/content/evidence-dashboard/expert-visitation-statistics-tile/expert-visitation-statistics-tile.component';
import {SharedModule} from 'ng-src/app/shared/shared.module';
import {PlatformRoutingModule} from 'ng-src/app/platform/platform-routing.module';
import {PlatformComponent} from 'ng-src/app/platform/platform.component';
import {
  UserSettingsHeaderComponent
} from 'ng-src/app/platform/components/content/user-settings/user-settings-header/user-settings-header.component';
import {
  UserSettingsPreferencesComponent
} from 'ng-src/app/platform/components/content/user-settings/user-settings-preferences/user-settings-preferences.component';
import {
  UserSettingsPasswordComponent
} from 'ng-src/app/platform/components/content/user-settings/user-settings-password/user-settings-password.component';
import {
  UserSettingsClaimProfilesComponent
} from 'ng-src/app/platform/components/content/user-settings/user-settings-claim-profiles/user-settings-claim-profiles.component';
import {
  UserSettingsConsentsComponent
} from 'ng-src/app/platform/components/content/user-settings/user-settings-consents/user-settings-consents.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {ResetPasswordComponent} from 'ng-src/app/platform/components/content/reset-password/reset-password.component';
import {
  OutcomeDetailEditModalComponent
} from 'ng-src/app/platform/components/content/outcome-detail/outcome-detail-edit-modal/outcome-detail-edit-modal.component';
import {
  ProjectDetailEditModalComponent
} from 'ng-src/app/platform/components/content/project-detail/project-detail-edit-modal/project-detail-edit-modal.component';
import {
  AddOutcomeModalComponent
} from 'ng-src/app/platform/components/common/add-outcome-modal/add-outcome-modal.component';
import {
  CommercializationDetailEditModalComponent
} from 'ng-src/app/platform/components/content/commercialization-detail/commercialization-detail-edit-modal/commercialization-detail-edit-modal.component';
import {
  AddProjectModalComponent
} from 'ng-src/app/platform/components/common/add-project-modal/add-project-modal.component';
import {FilePreviewComponent} from 'ng-src/app/platform/components/common/file-preview/file-preview.component';
import {SearchHistoryComponent} from 'ng-src/app/platform/components/content/search-history/search-history.component';
import {
  UserFollowingsComponent
} from 'ng-src/app/platform/components/content/user-followings/user-followings.component';
import {
  SavedSearchesComponent
} from 'ng-src/app/platform/components/content/user-followings/saved-searches/saved-searches.component';
import {
  SearchHistoryRowComponent
} from 'ng-src/app/platform/components/common/search-history-row/search-history-row.component';
import {
  SavedExpertsComponent
} from 'ng-src/app/platform/components/content/user-followings/saved-experts/saved-experts.component';
import {
  AddExpertModalComponent
} from 'ng-src/app/platform/components/common/add-expert-modal/add-expert-modal.component';
import {
  ExploreOrganizationsComponent
} from 'ng-src/app/platform/components/content/explore/explore-organizations/explore-organizations.component';
import {
  PlatformSettingsComponent
} from 'ng-src/app/platform/components/content/platform-settings/platform-settings.component';
import {ReindexComponent} from 'ng-src/app/platform/components/content/platform-settings/reindex/reindex.component';
import {
  AdminOrganizationsComponent
} from 'ng-src/app/platform/components/content/platform-settings/admin-organizations/admin-organizations.component';
import {
  AdminUsersComponent
} from 'ng-src/app/platform/components/content/platform-settings/admin-users/admin-users.component';
import {
  AdminOrganizationEditModalComponent
} from 'ng-src/app/platform/components/content/platform-settings/admin-organizations/admin-organization-edit-modal/admin-organization-edit-modal.component';
import {
  AdminOrganizationsLicencesModalComponent
} from 'ng-src/app/platform/components/content/platform-settings/admin-organizations/admin-organizations-licences-modal/admin-organizations-licences-modal.component';
import {
  EvidenceSettingsComponent
} from 'ng-src/app/platform/components/content/evidence-settings/evidence-settings.component';
import {
  ExpertsModalComponent
} from 'ng-src/app/platform/components/content/evidence-settings/experts-modal/experts-modal.component';
import {
  OrganizationSettingsHeaderComponent
} from 'ng-src/app/platform/components/content/evidence-settings/organization-settings-header/organization-settings-header.component';
import {
  OrganizationLicenseComponent
} from 'ng-src/app/platform/components/content/evidence-settings/organization-license/organization-license.component';
import {
  WidgetTypeComponent
} from 'ng-src/app/platform/components/content/evidence-settings/widget-type/widget-type.component';
import {
  PublicProfileComponent
} from 'ng-src/app/platform/components/content/evidence-settings/public-profile/public-profile.component';
import {
  SearchHistoryModalComponent
} from 'ng-src/app/platform/components/content/platform-settings/admin-users/search-history-modal/search-history-modal.component';
import {
  ExpertDetailThesisComponent
} from 'ng-src/app/platform/components/content/expert-detail/expert-detail-thesis/expert-detail-thesis.component';
import {ThesisDetailComponent} from 'ng-src/app/platform/components/content/thesis-detail/thesis-detail.component';
import {
  ExploreThesisComponent
} from 'ng-src/app/platform/components/content/explore/explore-thesis/explore-thesis.component';
import {
  ExpertDetailLecturesComponent
} from 'ng-src/app/platform/components/content/expert-detail/expert-detail-lectures/expert-detail-lectures.component';
import {LectureDetailComponent} from 'ng-src/app/platform/components/content/lecture-detail/lecture-detail.component';
import {
  ExploreLecturesComponent
} from 'ng-src/app/platform/components/content/explore/explore-lectures/explore-lectures.component';
import {OrderModalComponent} from 'ng-src/app/platform/components/common/order-modal/order-modal.component';
import {
  ExploreEquipmentComponent
} from 'ng-src/app/platform/components/content/explore/explore-equipment/explore-equipment.component';
import {
  EquipmentDetailComponent
} from 'ng-src/app/platform/components/content/equipment-detail/equipment-detail.component';
import {
  EvidenceMemberComponent
} from 'ng-src/app/platform/components/content/evidence-member/evidence-member.component';
import {
  MyMembershipsComponent
} from 'ng-src/app/platform/components/content/evidence-dashboard/my-memberships/my-memberships.component';
import {
  AddMembersModalComponent
} from 'ng-src/app/platform/components/common/add-members-modal/add-members-modal.component';
import {
  EvidenceOpportunityComponent
} from 'ng-src/app/platform/components/content/evidence-opportunity/evidence-opportunity.component';
import {
  OpportunityDetailComponent
} from 'ng-src/app/platform/components/content/opportunity-detail/opportunity-detail.component';
import {
  OpportunityEditModalComponent
} from 'ng-src/app/platform/components/content/opportunity-detail/opportunity-edit-modal/opportunity-edit-modal.component';
import {ClaimExpertsComponent} from './components/common/claim-experts/claim-experts.component';
import {WidgetsModule} from '../widgets/widgets.module';
import {
  OrganizationLicensesComponent
} from './components/content/evidence-dashboard/organization-licenses/organization-licenses.component';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {
  PublicProfileOptionsComponent
} from './components/content/evidence-dashboard/ public-profile-options/public-profile-options.component';
import {
  ShortTextComponent
} from './components/content/evidence-dashboard/keyword-statistics-tile/short_text/short-text.component';
import {ResearchResultsStatisticsTileComponent} from './components/content/evidence-dashboard/research-results-statistic-tile/research-results-statistic-tile.component';
import {EvidenceEquipmentComponent} from './components/content/evidence-equipment/evidence-equipment.component';
import {EquipmentEditModalComponent} from './components/content/equipment-detail/equipment-edit-modal/equipment-edit-modal.component';
import {AddOrganizationComponent} from './components/common/add-organization/add-organization.component';
import {AddExpertsComponent} from './components/common/add-experts/add-experts.component';
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {FormsModule} from '@angular/forms';
import {
  ReindexRecombeeComponent
} from './components/content/platform-settings/reindex-recombee/reindex-recombee.component';

@NgModule({
  declarations: [
    PlatformComponent,
    HeaderComponent,
    NavigationComponent,
    EvidenceExpertComponent,
    EvidenceOutcomeComponent,
    Oauth2CallbackComponent,
    EvidenceProjectComponent,
    Oauth2CallbackComponent,
    OrganizationStructureNavigationComponent,
    ExpertDetailComponent,
    ExpertDetailCommercializationComponent,
    ExpertDetailOutcomesComponent,
    ExpertDetailProjectsComponent,
    ExpertDetailEditModalComponent,
    FileDropComponent,
    ExploreHeaderComponent,
    ExploreExpertsComponent,
    EvidenceDashboardComponent,
    CounterTileComponent,
    ProjectStatisticsTileComponent,
    OutcomeStatisticsTileComponent,
    KeywordStatisticsTileComponent,
    EvidenceCommercializationComponent,
    OrganizationDetailComponent,
    FileDropModalComponent,
    OrganizationDetailEditModalComponent,
    OrganizationDetailExpertsComponent,
    OrganizationDetailOutcomesComponent,
    OrganizationDetailProjectsComponent,
    OutcomeDetailComponent,
    ProjectDetailComponent,
    CommercializationDetailComponent,
    ExpertSearchStatisticsTileComponent,
    ExpertVisitationStatisticsTileComponent,
    UserSettingsHeaderComponent,
    UserSettingsPreferencesComponent,
    UserSettingsPasswordComponent,
    UserSettingsClaimProfilesComponent,
    UserSettingsConsentsComponent,
    ResetPasswordComponent,
    OutcomeDetailEditModalComponent,
    ProjectDetailEditModalComponent,
    AddOutcomeModalComponent,
    CommercializationDetailEditModalComponent,
    AddProjectModalComponent,
    FilePreviewComponent,
    SearchHistoryComponent,
    UserFollowingsComponent,
    SavedSearchesComponent,
    SearchHistoryRowComponent,
    SavedExpertsComponent,
    AddExpertModalComponent,
    PlatformSettingsComponent,
    ReindexComponent,
    ExploreOrganizationsComponent,
    AdminOrganizationsComponent,
    AdminUsersComponent,
    AdminOrganizationEditModalComponent,
    AdminOrganizationsLicencesModalComponent,
    SearchHistoryModalComponent,
    EvidenceSettingsComponent,
    ExpertsModalComponent,
    OrganizationSettingsHeaderComponent,
    OrganizationLicenseComponent,
    WidgetTypeComponent,
    PublicProfileComponent,
    ExpertDetailThesisComponent,
    ThesisDetailComponent,
    ExploreThesisComponent,
    ExpertDetailLecturesComponent,
    LectureDetailComponent,
    ExploreLecturesComponent,
    ExploreEquipmentComponent,
    EquipmentDetailComponent,
    OrderModalComponent,
    EvidenceMemberComponent,
    MyMembershipsComponent,
    AddMembersModalComponent,
    EvidenceOpportunityComponent,
    OpportunityDetailComponent,
    OpportunityEditModalComponent,
    ClaimExpertsComponent,
    PublicProfileOptionsComponent,
    OrganizationLicensesComponent,
    ShortTextComponent,
    ResearchResultsStatisticsTileComponent,
    ReindexRecombeeComponent,
    EvidenceEquipmentComponent,
    EquipmentEditModalComponent,
    AddOrganizationComponent,
    AddExpertsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PlatformRoutingModule,
    MatSlideToggleModule,
    ScrollingModule,
    MatSlideToggleModule,
    WidgetsModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
  ],

  bootstrap: [
    PlatformComponent
  ]
})
export class PlatformModule {
}
