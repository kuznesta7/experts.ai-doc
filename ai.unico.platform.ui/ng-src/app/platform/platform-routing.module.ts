import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EvidenceExpertComponent} from 'ng-src/app/platform/components/content/evidence-expert/evidence-expert.component';
import {EvidenceOutcomeComponent} from 'ng-src/app/platform/components/content/evidence-outcome/evidence-outcome.component';
import {Oauth2CallbackComponent} from 'ng-src/app/platform/components/content/oauth2callback/oauth2-callback.component';
import {AuthorizationGuard} from 'ng-src/app/guards/authorization.guard';
import {ExpertOrganizationRelation} from 'ng-src/app/enums/expert-organization-relation';
import {UrlParams} from 'ng-src/app/constants/url-params';
import {EvidenceProjectComponent} from 'ng-src/app/platform/components/content/evidence-project/evidence-project.component';
import {ExpertDetailComponent} from 'ng-src/app/platform/components/content/expert-detail/expert-detail.component';
import {ExploreExpertsComponent} from 'ng-src/app/platform/components/content/explore/explore-experts/explore-experts.component';
import {EvidenceGuard} from 'ng-src/app/guards/evidence.guard';
import {EvidenceDashboardComponent} from 'ng-src/app/platform/components/content/evidence-dashboard/evidence-dashboard.component';
import {EvidenceCommercializationComponent} from 'ng-src/app/platform/components/content/evidence-commercialization/evidence-commercialization.component';
import {OrganizationDetailComponent} from 'ng-src/app/platform/components/content/organization-detail/organization-detail.component';
import {OutcomeDetailComponent} from 'ng-src/app/platform/components/content/outcome-detail/outcome-detail.component';
import {ProjectDetailComponent} from 'ng-src/app/platform/components/content/project-detail/project-detail.component';
import {CommercializationDetailComponent} from 'ng-src/app/platform/components/content/commercialization-detail/commercialization-detail.component';
import {PathFragment} from 'ng-src/app/constants/path-fragment';
import {PlatformComponent} from 'ng-src/app/platform/platform.component';
import {UserSettingsPreferencesComponent} from 'ng-src/app/platform/components/content/user-settings/user-settings-preferences/user-settings-preferences.component';
import {UserSettingsConsentsComponent} from 'ng-src/app/platform/components/content/user-settings/user-settings-consents/user-settings-consents.component';
import {UserSettingsClaimProfilesComponent} from 'ng-src/app/platform/components/content/user-settings/user-settings-claim-profiles/user-settings-claim-profiles.component';
import {UserSettingsPasswordComponent} from 'ng-src/app/platform/components/content/user-settings/user-settings-password/user-settings-password.component';
import {ResetPasswordComponent} from 'ng-src/app/platform/components/content/reset-password/reset-password.component';
import {SearchHistoryComponent} from 'ng-src/app/platform/components/content/search-history/search-history.component';
import {UserFollowingsComponent} from 'ng-src/app/platform/components/content/user-followings/user-followings.component';
import {ReindexComponent} from 'ng-src/app/platform/components/content/platform-settings/reindex/reindex.component';
import {ExploreOrganizationsComponent} from 'ng-src/app/platform/components/content/explore/explore-organizations/explore-organizations.component';
import {PlatformSettingsComponent} from 'ng-src/app/platform/components/content/platform-settings/platform-settings.component';
import {AdminUsersComponent} from 'ng-src/app/platform/components/content/platform-settings/admin-users/admin-users.component';
import {AdminOrganizationsComponent} from 'ng-src/app/platform/components/content/platform-settings/admin-organizations/admin-organizations.component';
import {WidgetTypeComponent} from 'ng-src/app/platform/components/content/evidence-settings/widget-type/widget-type.component';
import {OrganizationLicenseComponent} from 'ng-src/app/platform/components/content/evidence-settings/organization-license/organization-license.component';
import {PublicProfileComponent} from 'ng-src/app/platform/components/content/evidence-settings/public-profile/public-profile.component';
import {ThesisDetailComponent} from 'ng-src/app/platform/components/content/thesis-detail/thesis-detail.component';
import {ExploreThesisComponent} from 'ng-src/app/platform/components/content/explore/explore-thesis/explore-thesis.component';
import {LectureDetailComponent} from 'ng-src/app/platform/components/content/lecture-detail/lecture-detail.component';
import {ExploreLecturesComponent} from 'ng-src/app/platform/components/content/explore/explore-lectures/explore-lectures.component';
import {ExploreEquipmentComponent} from 'ng-src/app/platform/components/content/explore/explore-equipment/explore-equipment.component';
import {EquipmentDetailComponent} from 'ng-src/app/platform/components/content/equipment-detail/equipment-detail.component';
import {EvidenceMemberComponent} from 'ng-src/app/platform/components/content/evidence-member/evidence-member.component';
import {EvidenceOpportunityComponent} from 'ng-src/app/platform/components/content/evidence-opportunity/evidence-opportunity.component';
import {OpportunityDetailComponent} from 'ng-src/app/platform/components/content/opportunity-detail/opportunity-detail.component';
import {EvidenceEquipmentComponent} from 'ng-src/app/platform/components/content/evidence-equipment/evidence-equipment.component';

const routes: Routes = [
  {
    path: '',
    canActivateChild: [AuthorizationGuard],
    component: PlatformComponent,
    children: [
      {
        path: PathFragment.MY_ACCOUNT,
        children: [
          {
            path: PathFragment.SETTINGS,
            children: [
              {
                path: PathFragment.CONSENTS,
                component: UserSettingsConsentsComponent
              },
              {
                path: PathFragment.CLAIM_PROFILES,
                component: UserSettingsClaimProfilesComponent
              },
              {
                path: PathFragment.PASSWORD,
                component: UserSettingsPasswordComponent
              },
              {
                path: '',
                component: UserSettingsPreferencesComponent,
              }
            ]
          },
          {
            path: PathFragment.FOLLOWINGS,
            component: UserFollowingsComponent
          },
          {
            path: PathFragment.SEARCH_HISTORY,
            component: SearchHistoryComponent
          }
        ]
      },
      {
        path: PathFragment.EXPLORE,
        children: [
          {
            path: PathFragment.EXPERTS,
            component: ExploreExpertsComponent
          },
          {
            path: PathFragment.ORGANIZATIONS,
            component: ExploreOrganizationsComponent
          },
          /*
          ExploreCommercialization does not exist
           {
            path: PathFragment.COMMERCIALIZATION,
            component: ExploreCommercialization
          },*/
          {
            path: PathFragment.THESIS,
            component: ExploreThesisComponent
          },
          {
            path: PathFragment.LECTURE,
            component: ExploreLecturesComponent
          },
          {
            path: PathFragment.EQUIPMENT,
            component: ExploreEquipmentComponent
          }
        ]
      },
      {
        path: PathFragment.EXPERTS,
        children: [{
          path: ':' + UrlParams.EXPERT_CODE,
          component: ExpertDetailComponent
        }]
      },
      {
        path: PathFragment.OUTCOMES,
        children: [{
          path: ':' + UrlParams.OUTCOME_CODE,
          component: OutcomeDetailComponent
        }]
      },
      {
        path: PathFragment.PROJECTS,
        children: [{
          path: ':' + UrlParams.PROJECT_CODE,
          component: ProjectDetailComponent
        }]
      },
      {
        path: PathFragment.COMMERCIALIZATION,
        children: [{
          path: ':' + UrlParams.COMMERCIALIZATION_ID,
          component: CommercializationDetailComponent
        }]
      },
      {
        path: PathFragment.THESIS,
        children: [{
          path: ':' + UrlParams.THESIS_CODE,
          component: ThesisDetailComponent
        }]
      },
      {
        path: PathFragment.LECTURE,
        children: [{
          path: ':' + UrlParams.LECTURE_CODE,
          component: LectureDetailComponent
        }]
      },
      {
        path: PathFragment.EQUIPMENT,
        children: [{
          path: ':' + UrlParams.EQUIPMENT_CODE,
          component: EquipmentDetailComponent
        }]
      },
      {
        path: PathFragment.OPPORTUNITY,
        children: [{
          path: ':' + UrlParams.OPPORTUNITY_ID,
          component: OpportunityDetailComponent
        }]
      },
      {
        path: PathFragment.ORGANIZATIONS,
        children: [{
          path: ':' + UrlParams.ORGANIZATION_ID,
          component: OrganizationDetailComponent
        }]
      }, {
        path: PathFragment.EVIDENCE,
        children: [{
          path: PathFragment.ORGANIZATIONS,
          children: [{
            path: ':' + UrlParams.ORGANIZATION_ID,
            canActivateChild: [EvidenceGuard],
            children: [
              {
                path: '',
                component: EvidenceDashboardComponent
              }, {
                path: PathFragment.EXPERTS,
                data: {
                  expertOrganizationRelation: ExpertOrganizationRelation.INTERNAL
                },
                component: EvidenceExpertComponent
              }, {
                path: PathFragment.EXPERTS_AFFILIATED,
                data: {
                  expertOrganizationRelation: ExpertOrganizationRelation.AFFILIATED
                },
                component: EvidenceExpertComponent
              }, {
                path: PathFragment.OUTCOMES,
                component: EvidenceOutcomeComponent
              }, {
                path: PathFragment.PROJECTS,
                component: EvidenceProjectComponent
              }, {
                path: PathFragment.COMMERCIALIZATION,
                component: EvidenceCommercializationComponent
              }, {
                path: PathFragment.MEMBERS,
                component: EvidenceMemberComponent
              }, {
                path: PathFragment.OPPORTUNITY,
                component: EvidenceOpportunityComponent
              }, {
                path: PathFragment.EQUIPMENT,
                component: EvidenceEquipmentComponent
              }, {
                path: PathFragment.SETTINGS,
                children: [
                  {
                    path: PathFragment.ORGANIZATION_LICENSES,
                    component: OrganizationLicenseComponent
                  },
                  {
                    path: PathFragment.WIDGETS,
                    component: WidgetTypeComponent
                  },
                  {
                    path: PathFragment.ORGANIZATION_PUBLIC_PROFILES,
                    component: PublicProfileComponent
                  },
                ]
              }]
          }]
        }]
      },
      {
        path: PathFragment.RESET_PASSWORD,
        component: ResetPasswordComponent
      },
      {
        path: PathFragment.PLATFORM_SETTINGS,
        children: [
          {
            path: '',
            component: PlatformSettingsComponent
          },
          {
            path: PathFragment.REINDEX,
            component: ReindexComponent
          },
          {
            path: PathFragment.USERS,
            component: AdminUsersComponent
          },
          {
            path: PathFragment.ORGANIZATIONS,
            component: AdminOrganizationsComponent
          }
        ]
      }]
  },
  {
    path: 'oauth2callback/:service',
    component: Oauth2CallbackComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlatformRoutingModule {
}
