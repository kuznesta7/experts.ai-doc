import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { OrganizationStructureDto } from 'ng-src/app/services/organization-structure.service';
import { ActivatedRoute } from '@angular/router';
import { debounceTime, tap } from 'rxjs/operators';
import { SessionOrganizationService } from 'ng-src/app/services/session-organization.service';
import { OrganizationUtilService } from 'ng-src/app/shared/services/organization-util.service';
import { PathFragment } from 'ng-src/app/constants/path-fragment';

@Component({
  selector: 'app-organization-structure-navigation',
  templateUrl: './organization-structure-navigation.component.html'
})
export class OrganizationStructureNavigationComponent implements OnInit {

  @Input()
  set structure(structure: OrganizationStructureDto[]) {
    this.userOrganizations$.next(this.convertOrganizationStructure(structure));
  }

  userOrganizations$ = new BehaviorSubject<UserOrganizationWithControls[]>([]);
  activeOrganizationId$ = new BehaviorSubject<number | null>(null);

  paths = PathFragment;

  constructor(private activatedRoute: ActivatedRoute,
              private sessionOrganizationService: SessionOrganizationService) {
  }

  ngOnInit(): void {
    combineLatest([this.sessionOrganizationService.getActiveOrganizationIdWithUpdates(), this.userOrganizations$]).pipe(
      tap(([o]) => {
        o ? this.activeOrganizationId$.next(o) : null;
      }),
      debounceTime(0)
    ).subscribe(([organizationId, userOrganizations]) => {
      if (!organizationId) {
        return;
      }
      userOrganizations.forEach(o => {
        if (o.organizationId === organizationId || this.find(organizationId, o.subOrganizations)) {
          o.expanded$.next(true);
        } else {
          o.expanded$.next(false);
        }
      });
    });
  }

  private find(organizationToExpandId: number, structure: OrganizationStructureDto[]): boolean {
    if (!structure) {
      return false;
    }
    return structure.some(o => o.organizationId === organizationToExpandId || this.find(organizationToExpandId, o.organizationUnits));
  }

  private convertOrganizationStructure(organizations: OrganizationStructureDto[]): UserOrganizationWithControls[] {
    if (!organizations) {
      return [];
    }
    return organizations.map(o => {
      return {
        organizationId: o.organizationId,
        organizationName: o.organizationName,
        organizationAbbrev: o.organizationAbbrev,
        memberOrganization: o.memberOrganization,
        expanded$: new BehaviorSubject<boolean>(false),
        subOrganizations: o.organizationUnits.map(so => {
          return {
            ...so,
            organizationName: OrganizationUtilService.trimOrganizationName(so.organizationName, o.organizationName),
            organizationAbbrev: OrganizationUtilService.trimOrganizationName(so.organizationAbbrev, o.organizationAbbrev)
          };
        })
      };
    });
  }

  toggleOpen(organization: UserOrganizationWithControls): void {
    organization.expanded$.next(!organization.expanded$.getValue());
  }
}

interface UserOrganizationWithControls {
  organizationId: number;
  organizationName: string;
  organizationAbbrev: string;
  memberOrganization: boolean;
  expanded$: BehaviorSubject<boolean>;
  subOrganizations: OrganizationStructureDto[];
}
