import { Component, Input, OnInit, Output } from '@angular/core';
import { OrganizationStructureDto, } from 'ng-src/app/services/organization-structure.service';
import { BehaviorSubject } from 'rxjs';
import { PathFragment } from 'ng-src/app/constants/path-fragment';
import { SecurityService } from 'ng-src/app/services/security.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'aside[app-navigation]',
  templateUrl: './navigation.component.html',
})
export class NavigationComponent implements OnInit {


  constructor(private securityService: SecurityService) {
  }

  paths = PathFragment;

  @Input()
  userOrganizations: OrganizationStructureDto[];

  @Output()
  navigationToggle = new BehaviorSubject<boolean>(true);
  showMaintainerItems$ = this.securityService.isPortalMaintainer();

  ngOnInit(): void {
    // this.userOrganizations$.subscribe(console.log)
  }

  toggleNavigation(): void {
    this.navigationToggle.next(!this.navigationToggle.value);
  }
}
