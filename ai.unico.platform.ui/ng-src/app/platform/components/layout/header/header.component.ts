import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginService } from 'ng-src/app/services/login.service';
import { FormControl, FormGroup } from '@angular/forms';
import { UrlParams } from 'ng-src/app/constants/url-params';
import { debounceTime, distinctUntilChanged, filter, map, share, switchMap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { PathService } from 'ng-src/app/shared/services/path.service';
import { GlobalSearchService, SearchType } from 'ng-src/app/services/global-search.service';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { ExploreService, SearchSuggestion } from 'ng-src/app/services/explore.service';
import { PathFragment } from 'ng-src/app/constants/path-fragment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

  paths = PathFragment;
  searchFormGroup = new FormGroup({
    searchQuery: new FormControl()
  });
  searchSuggestions$: Observable<SearchSuggestion[]>;

  constructor(private loginService: LoginService,
              private activatedRoute: ActivatedRoute,
              private pathService: PathService,
              private router: Router,
              private exploreService: ExploreService,
              private globalSearchService: GlobalSearchService) {
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.pipe(
      map(qp => qp.searchQuery),
      filter(p => !!p),
      distinctUntilChanged()
    ).subscribe(q => this.searchFormGroup.controls.searchQuery.patchValue(q));

    this.searchSuggestions$ = this.searchFormGroup.controls.searchQuery.valueChanges.pipe(
      debounceTime(300),
      switchMap(q => this.exploreService.findSuggestions(q, 10)),
      share()
    );
  }

  search(): void {
    const value = this.searchFormGroup.value.searchQuery;
    if (!value) {
      return;
    }
    const lastUsedSearchType: SearchType = this.globalSearchService.getLastUsedSearchType() || 'organizations';
    const path = this.globalSearchService.getPathForSearchType(lastUsedSearchType);
    console.log('path is : ', path);
    this.router.navigate([path], {queryParams: {[UrlParams.SEARCH_QUERY]: value}});
  }

  searchSuggestionSelected(event: MatAutocompleteSelectedEvent): void {
    const searchQuery = event.option.value.searchExpression;
    this.searchFormGroup.patchValue({searchQuery});
    this.search();
  }
}
