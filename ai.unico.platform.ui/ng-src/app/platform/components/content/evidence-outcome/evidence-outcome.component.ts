import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, combineLatest, Observable, of} from 'rxjs';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {ActivatedRoute, Router} from '@angular/router';
import {debounceTime, finalize, map, mergeMap, shareReplay, startWith, switchMap} from 'rxjs/operators';
import {FormControl, FormGroup} from '@angular/forms';
import {EvidenceItemWrapper} from 'ng-src/app/interfaces/evidence-item-wrapper';
import {EvidenceItemService} from 'ng-src/app/services/evidence-item.service';
import {EvidenceFilter} from 'ng-src/app/interfaces/evidence-filter';
import {ItemTypeService} from 'ng-src/app/services/item-type.service';
import {IdDescriptionOption} from 'ng-src/app/shared/interfaces/idDescriptionOption';
import {QueryParamService} from 'ng-src/app/shared/services/query-param.service';
import {tapAllHandleError} from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {OrganizationAutocompleteService} from 'ng-src/app/services/organization-autocomplete.service';
import {ExpertAutocompleteService} from 'ng-src/app/services/expert-autocomplete.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {
  AddOutcomeModalComponent
} from 'ng-src/app/platform/components/common/add-outcome-modal/add-outcome-modal.component';
import {ItemOrganizationService} from 'ng-src/app/services/item-organization.service';
import {EvidenceItemPreviewWithExperts} from 'ng-src/app/interfaces/evidence-item-preview-with-experts';
import {environment} from 'ng-src/environments/environment.prod';
import {EvidenceItemPreview} from 'ng-src/app/interfaces/evidence-item-preview';
import {ConfirmationService} from 'ng-src/app/shared/services/confirmation.service';
import {Title} from '@angular/platform-browser';
import {ItemDetailDto} from 'ng-src/app/interfaces/item-detail-dto';
import {PlatformDataSource} from 'ng-src/app/enums/platform-data-source';
import {
  OutcomeDetailEditModalComponent
} from 'ng-src/app/platform/components/content/outcome-detail/outcome-detail-edit-modal/outcome-detail-edit-modal.component';
import {PathFragment} from 'ng-src/app/constants/path-fragment';

@Component({
  selector: 'app-evidence-outcome',
  templateUrl: './evidence-outcome.component.html'
})
export class EvidenceOutcomeComponent implements OnInit {
  filterFormGroup = new FormGroup({
    query: new FormControl(''),
    resultTypeGroupIds: new FormControl([]),
    participatingOrganizationIds: new FormControl([]),
    expertCodes: new FormControl([])
  });

  organizationId$: Observable<number>;
  organizationId: number;
  organizationIdSnapshot: string | null;
  exportUrl$: Observable<string>;
  itemWrapper$: Observable<EvidenceItemWrapper>;
  itemList$: Observable<ExtendedItemPreview[]>;
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 15});
  itemTypeGroups$: Observable<IdDescriptionOption[]> = this.itemTypeService.findItemTypeGroups().pipe(
    map(groups => groups.map(g => {
      return {id: g.itemTypeGroupId, description: g.itemTypeGroupTitle};
    }))
  );
  loading$ = new BehaviorSubject(true);

  loadOrganizations = (ids: number[]) => {
    return this.organizationService.getOrganizationsForAutocompleteStandardized(ids);
  }
  autocompleteOrganizations = (query: string) => {
    return this.organizationService.autocompleteOrganizationOutcomesStandardized(query, this.organizationId);
  }
  loadExperts = (expertCodes: string[]) => {
    return this.expertAutocompleteService.findUserExpertOptions({expertCodes});
  }
  autocompleteExperts = (query: string) => {
    if (this.organizationIdSnapshot != null) {
      return this.expertAutocompleteService.findUserExpertOptions({
        organizationIds: [+this.organizationIdSnapshot],
        query
      });
    } else {
      return new Observable<IdDescriptionOption[]>();
    }
  }

  constructor(private evidenceItemService: EvidenceItemService,
              private route: ActivatedRoute,
              private queryParamService: QueryParamService,
              private itemTypeService: ItemTypeService,
              private organizationService: OrganizationAutocompleteService,
              private expertAutocompleteService: ExpertAutocompleteService,
              private itemOrganizationService: ItemOrganizationService,
              private confirmationService: ConfirmationService,
              private ngbModal: NgbModal,
              private titleService: Title,
              private itemService: EvidenceItemService,
              private router: Router) {

    this.titleService.setTitle('Outcomes');

    this.queryParamService
      .handleFilterFormGroup(this.filterFormGroup, {numericFields: ['resultTypeGroupIds', 'participatingOrganizationIds']});

    this.organizationIdSnapshot = this.route.snapshot.paramMap.get('organizationId');

    this.organizationId$ = this.route.params.pipe(
      map(p => parseInt(p.organizationId, 10))
    );

    this.exportUrl$ = this.organizationId$.pipe(map(oId => `${environment.internalRestUrl}evidence/organizations/${oId}/items/csv`));

    const filter$: Observable<EvidenceFilter> = this.filterFormGroup.valueChanges.pipe(
      debounceTime(300),
      startWith(this.filterFormGroup.value as EvidenceFilter),
    );

    this.itemWrapper$ = combineLatest([this.organizationId$, filter$, this.pageInfo$, this.itemTypeGroups$]).pipe(
      debounceTime(10),
      switchMap(([id, filter, pageInfo]) => {
        this.loading$.next(true);
        filter.activeOnly = false;
        this.organizationId = id;
        return this.evidenceItemService.getEvidenceItems(id, filter, pageInfo).pipe(
          tapAllHandleError(() => this.loading$.next(false))
        );
      }),
      shareReplay()
    );

    this.itemList$ = this.itemWrapper$.pipe(
      map(w => w.itemPreviewDtos.map(i => {
        return {
          ...i,
          saving$: new BehaviorSubject(false),
          deleted$: new BehaviorSubject(false),
          verified$: new BehaviorSubject(i.verified),
          active$: new BehaviorSubject(i.active),
        } as ExtendedItemPreview;
      }))
    );
  }

  ngOnInit(): void {
  }

  addOutcome(organizationId: number): void {
    const ngbModalRef = this.ngbModal.open(AddOutcomeModalComponent, {size: 'xl'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.organizationId = organizationId;
    componentInstance.doAddOutcome = (outcome: EvidenceItemPreviewWithExperts) => {
      return this.getItemId(outcome).pipe(mergeMap(id => this.itemOrganizationService.addOrganization(id, organizationId, true)));
    };
  }

  verifyOutcome(outcome: ExtendedItemPreview, organizationId: number, verify: boolean): void {
    outcome.saving$.next(true);
    this.getItemId(outcome).pipe(
      mergeMap(id => this.itemOrganizationService.verifyItemForOrganization([id], organizationId, verify)),
      finalize(() => outcome.saving$.next(false))
    ).subscribe(() => {
      outcome.verified = verify;
      outcome.verified$.next(verify);
    });
  }

  activateOutcome(outcome: ExtendedItemPreview, organizationId: number, activate: boolean): void {
    outcome.saving$.next(true);
    this.getItemId(outcome).pipe(
      mergeMap(id => this.itemOrganizationService.activateItemForOrganization([id], organizationId, activate)),
      finalize(() => outcome.saving$.next(false))
    ).subscribe(() => {
      outcome.active = activate;
      outcome.active$.next(activate);
    });
  }

  editOutcome(item: ExtendedItemPreview): void {
    this.itemService.getItemDetail(item.itemCode).subscribe(itemDetail => {
      const ngbModalRef = this.ngbModal.open(OutcomeDetailEditModalComponent, {size: 'xl'});
      const componentInstance = ngbModalRef.componentInstance;
      componentInstance.outcome$.next(itemDetail);
      ngbModalRef.result.then((id: number) => {
        if (id !== null) {
          this.router.navigate(['/', PathFragment.OUTCOMES, 'ST' + id]);
        }
      });
    });
  }

  removeOutcome(item: ExtendedItemPreview, organizationId: number): void {
    this.confirmationService.confirm({
      title: 'Remove outcome from your organization',
      content: 'Are you sure you want to remove outcome <i>"' + item.itemName + '"</i> from your current organization?',
      confirmClass: 'danger',
      confirmLabel: 'Remove'
    }).subscribe(() => {
      item.saving$.next(true);
      this.getItemId(item).pipe(mergeMap(id => this.itemOrganizationService.removeOrganization(id, organizationId))).pipe(
        finalize(() => item.saving$.next(false))
      ).subscribe(() => item.deleted$.next(true));
    });
  }

  private getItemId(item: EvidenceItemPreview): Observable<number> {
    if (item.itemId) {
      return of(item.itemId);
    }
    return this.evidenceItemService.moveItem(item.originalItemId);
  }
}

interface ExtendedItemPreview extends EvidenceItemPreviewWithExperts {
  saving$: BehaviorSubject<boolean>;
  deleted$: BehaviorSubject<boolean>;
  verified$: BehaviorSubject<boolean>;
  active$: BehaviorSubject<boolean>;
}
