import { Component, OnInit } from '@angular/core';
import { AdminOrganizationDto } from 'ng-src/app/services/admin-organizations.service';
import { QueryParamService } from 'ng-src/app/shared/services/query-param.service';
import { CountryService } from 'ng-src/app/services/country.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OrganizationStructureService } from 'ng-src/app/services/organization-structure.service';
import { ConfirmationService } from 'ng-src/app/shared/services/confirmation.service';
import { FormControl, FormGroup } from '@angular/forms';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { OrganizationStructureRelation } from 'ng-src/app/enums/organization-structure-relation';
import { debounceTime, first, map, shareReplay, startWith, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { UrlParams } from 'ng-src/app/constants/url-params';
import { ActivatedRoute } from '@angular/router';
import { OrganizationWrapper } from 'ng-src/app/interfaces/organization-wrapper';
import { OrganizationDetailService } from 'ng-src/app/services/organization-detail.service';
import { AddMembersModalComponent } from 'ng-src/app/platform/components/common/add-members-modal/add-members-modal.component';
import { OrganizationBaseDto } from 'ng-src/app/interfaces/organization-base-dto';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-evidence-member',
  templateUrl: './evidence-member.component.html'
})
export class EvidenceMemberComponent implements OnInit {

  constructor(private organizationDetailService: OrganizationDetailService,
              private queryParamService: QueryParamService,
              private countryService: CountryService,
              private ngbModal: NgbModal,
              private route: ActivatedRoute,
              private organizationStructureService: OrganizationStructureService,
              private confirmationService: ConfirmationService,
              private titleService: Title) {
    this.organizationId$ = this.route.params.pipe(
      map(p => p[UrlParams.ORGANIZATION_ID])
    );
    this.titleService.setTitle('Members');
  }

  filterFormGroup = new FormGroup({
    query: new FormControl(),
    exactMatch: new FormControl(false),
    countryCodes: new FormControl([])
  });

  organizationId$: Observable<number>;
  organizationId: number;
  members$: Observable<number[]>;
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 30});
  loading$ = new BehaviorSubject<boolean>(true);
  saving$ = new BehaviorSubject<boolean>(false);
  data$: Observable<OrganizationWrapper>;
  countries$ = this.countryService.findCountriesStandardized();

  ngOnInit(): void {
    this.queryParamService.handleFilterFormGroup(this.filterFormGroup);
    this.organizationId$.subscribe(id => {
      this.organizationId = id;
    });

    const filter$ = this.filterFormGroup.valueChanges.pipe(
      debounceTime(300),
      startWith(this.filterFormGroup.value)
    );
    this.data$ = combineLatest([filter$, this.pageInfo$, this.organizationId$]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([filter, pageInfo, id]) => this.organizationDetailService.findOrganizationMemberList(id, filter, pageInfo).pipe(
        tapAllHandleError(() => this.loading$.next(false))
      )),
      shareReplay()
    );
  }

  removeMember(memberId: number): void {
    this.organizationStructureService.removeLowerOrganization(this.organizationId, memberId).subscribe(x => {
      this.reload();
    });
  }

  openEditModal(): void {
    this.organizationId$.pipe(debounceTime(0), first()).subscribe(id => {
      const ngbModalRef = this.ngbModal.open(AddMembersModalComponent, {size: 'xl'});
      const componentInstance = ngbModalRef.componentInstance;
      componentInstance.organizationId = id;
      componentInstance.doAddOrganization = (o: OrganizationBaseDto) => {
        return this.organizationStructureService.addLowerOrganization(id, o.organizationId, OrganizationStructureRelation.MEMBER);
      };
      ngbModalRef.result.then(() => this.reload());
    });
  }


  private reload(): void {
    this.filterFormGroup.patchValue({});
  }
}

interface ExtendedAdminOrganizationDto extends AdminOrganizationDto {
  parentOrganizationIds: number[];
}
