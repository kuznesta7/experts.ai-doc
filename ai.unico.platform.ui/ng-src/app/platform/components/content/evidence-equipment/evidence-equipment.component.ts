import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, combineLatest, Observable, Subject} from 'rxjs';
import {FormControl, FormGroup} from '@angular/forms';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {EvidenceEquipmentWrapper} from 'ng-src/app/interfaces/evidence-equipment-wrapper';
import {ActivatedRoute, Router} from '@angular/router';
import {QueryParamService} from 'ng-src/app/shared/services/query-param.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {debounceTime, map, shareReplay, startWith, switchMap} from 'rxjs/operators';
import {OpportunityFilter} from 'ng-src/app/interfaces/opportunity-filter';
import {tapAllHandleError} from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {EvidenceEquipmentService} from 'ng-src/app/services/evidence-equipment.service';
import {EquipmentPreview} from 'ng-src/app/interfaces/equipment-preview';
import {
  EquipmentEditModalComponent
} from 'ng-src/app/platform/components/content/equipment-detail/equipment-edit-modal/equipment-edit-modal.component';
import {PathFragment} from 'ng-src/app/constants/path-fragment';
import {FlashService} from 'ng-src/app/shared/services/flash.service';
import {Title} from '@angular/platform-browser';
import {MultilingualDto} from "ng-src/app/interfaces/multilingual-dto";

@Component({
  selector: 'app-evidence-equipment',
  templateUrl: './evidence-equipment.component.html',
})
export class EvidenceEquipmentComponent implements OnInit {

  organizationId$: Observable<number>;

  filterFormGroup = new FormGroup({
    query: new FormControl(''),
    includeHidden: new FormControl(false)
  });


  equipmentWrapper$: Observable<EvidenceEquipmentWrapper>;
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 10});
  loading$ = new BehaviorSubject(false);
  update$ = new Subject<void>();

  wrapper: EvidenceEquipmentWrapper;

  constructor(private route: ActivatedRoute,
              private queryParamService: QueryParamService,
              private ngbModal: NgbModal,
              private evidenceEquipmentService: EvidenceEquipmentService,
              private router: Router,
              private flashService: FlashService,
              private titleService: Title) {
    this.titleService.setTitle('Equipment');
  }

  ngOnInit(): void {
    this.organizationId$ = this.route.params.pipe(
      map(p => parseInt(p.organizationId, 10))
    );

    this.queryParamService.handleFilterFormGroup(this.filterFormGroup);

    const filter$ = this.filterFormGroup.valueChanges.pipe(
      debounceTime(400),
      startWith(this.filterFormGroup.value as OpportunityFilter)
    );
    this.equipmentWrapper$ = combineLatest([this.organizationId$, filter$, this.pageInfo$, this.update$]).pipe(
      debounceTime(0),
      switchMap(([organizationId, filter, pageInfo]) => {
        this.loading$.next(true);
        return this.evidenceEquipmentService.getOrganizationEquipment(organizationId, filter, pageInfo);
      }),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );

    this.equipmentWrapper$.subscribe((e) => {
      this.wrapper = e;
      console.log(this.wrapper)
    });

    this.update$.next();
  }

  acceptQuery(query: string): void {
    this.filterFormGroup.patchValue({query});
  }

  openEditEquipmentModal(preview: MultilingualDto<EquipmentPreview> | null): void {
    const ngbModalRef = this.ngbModal.open(EquipmentEditModalComponent, {size: 'xl'});
    // const modalBackdrop = document.getElementsByTagName('ngb-modal-backdrop');
    // // check the class if exists
    // modalBackdrop[0].className = 'modal-backdrop fade show';
    //
    // const modalWindow = document.getElementsByTagName('ngb-modal-window');
    // // check the class if exists
    // modalWindow[0].className = 'modal fade show d-block';
    // modalWindow[0].setAttribute('aria-modal', 'true');
    // modalWindow[0].children[0].className = 'modal-dialog modal-xl';
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.multilingualEquipment$.next(preview);
    componentInstance.equipment$.next(preview);

    if (preview === null) {
      componentInstance.organizationId$ = this.organizationId$;
    }
    ngbModalRef.result.then((equipmentCode: string) => {
      if (equipmentCode !== null && equipmentCode !== '') {
        this.router.navigate(['/', PathFragment.EQUIPMENT, equipmentCode]);
      }
      this.update$.next();
    });

  }

  deleteEquipment(equipment: MultilingualDto<EquipmentPreview>): void {
    equipment.preview.deleted = true;
    this.evidenceEquipmentService.deleteEquipment(equipment.preview.equipmentCode).subscribe(
        () => this.flashService.ok('Equipment successfully deleted'),
        () => {
          equipment.preview.deleted = false;
          this.flashService.error('Failed to delete equipment');
        }
    );
  }

  toggleHidden(equipment: EquipmentPreview): void {
    equipment.hidden = !equipment.hidden;
    this.evidenceEquipmentService.toggleHidden(equipment.equipmentCode, equipment.hidden).subscribe(
      () => this.flashService.ok('Equipment hidden status updated'),
      () => {
        equipment.hidden = !equipment.hidden;
        this.flashService.error('Failed to toggle hidden status');
      }
    );
  }

}
