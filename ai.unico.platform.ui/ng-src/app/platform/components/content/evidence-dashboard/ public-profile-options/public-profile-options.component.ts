import { Component, OnInit } from '@angular/core';
import { UrlParams } from 'ng-src/app/constants/url-params';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { EvidenceSettingsService, PublicProfileVisibilityDto } from 'ng-src/app/services/evidence-settings.service';
import { FormControl, FormGroup } from '@angular/forms';
import { FlashService } from 'ng-src/app/shared/services/flash.service';

@Component({
  selector: 'app-public-profile-options',
  templateUrl: './public-profile-options.component.html',
  styleUrls: ['./public-profile-options.component..scss']
})
export class PublicProfileOptionsComponent implements OnInit {
  organizationId: number;
  data$: Observable<PublicProfileVisibilityDto>;

  formGroup = new FormGroup({
    visible: new FormControl(false),
    keywordsVisible: new FormControl(false),
    graphVisible: new FormControl(false),
    expertsVisible: new FormControl(false),
    outcomesVisible: new FormControl(false),
    projectsVisible: new FormControl(false)
  });

  constructor(private route: ActivatedRoute,
              private flashService: FlashService,
              private evidenceSettingsService: EvidenceSettingsService) {
  }

  ngOnInit(): void {
    this.organizationId = this.route.snapshot.params[UrlParams.ORGANIZATION_ID];
    this.data$ = this.evidenceSettingsService.getVisibilities(this.organizationId);

    this.data$.subscribe(e => {
      // @ts-ignore
      delete e['organizationId'];
      this.formGroup.setValue(e);
    })
  }

  submit(): void {
    const form = this.formGroup.value;
    this.evidenceSettingsService.setVisibilities(this.organizationId, form)
      .subscribe(() => {
        this.flashService.ok('Saved');
      });
  }

  hideProfileCompletely(): void {
    const newProfile = {
      visible: !this.formGroup.value.visible,
      keywordsVisible: !this.formGroup.value.visible,
      graphVisible: !this.formGroup.value.visible,
      expertsVisible: !this.formGroup.value.visible,
      outcomesVisible: !this.formGroup.value.visible,
      projectsVisible: !this.formGroup.value.visible
    }
    this.formGroup.setValue(newProfile);
    this.submit()
  }

    hideCheckboxChecked(): boolean {
      let trueCounter = 0;
      console.log(this.formGroup.value)
      return trueCounter === 0;
    }
}
