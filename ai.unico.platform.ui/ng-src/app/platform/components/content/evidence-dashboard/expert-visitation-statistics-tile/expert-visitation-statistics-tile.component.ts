import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import {
  ExpertVisitationStatisticsWrapper,
  OrganizationStatisticsService
} from 'ng-src/app/services/organization-statistics.service';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { filter, share, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-expert-visitation-statistics-tile',
  templateUrl: './expert-visitation-statistics-tile.component.html',
})
export class ExpertVisitationStatisticsTileComponent implements OnInit {

  @Input()
  set organizationId(organizationId: number | null) {
    this.organizationId$.next(organizationId);
  }

  organizationId$ = new BehaviorSubject<number | null>(null);
  statisticsWrapper$: Observable<ExpertVisitationStatisticsWrapper>;
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 10});

  constructor(private statisticsService: OrganizationStatisticsService) {
  }

  ngOnInit(): void {
    this.statisticsWrapper$ = combineLatest([this.organizationId$, this.pageInfo$]).pipe(
      filter(([id]) => !!id),
      switchMap(([organizationId, pageInfo]) => this.statisticsService.getExpertVisitationStatistics((organizationId as number), pageInfo)),
      share()
    );
  }

}
