import {Component, Input} from '@angular/core';
import {OutcomeStatistics} from 'ng-src/app/services/organization-statistics.service';
import {ChartType, Column, Row} from 'angular-google-charts';
import {ChartService} from 'ng-src/app/shared/services/chart.service';


@Component({
  selector: 'app-research-results-statistics-tile',
  templateUrl: './research-results-statistic-tile.component.html'
})
export class ResearchResultsStatisticsTileComponent  {

  @Input()
  set outcomeStatistics(statistics: OutcomeStatistics) {

    const columns: Column[] = statistics.itemTypeStatistics.map(item => {
      return item.groupTitle as Column;
    });
    columns.unshift('All' as Column);
    columns.unshift('Year' as Column);
    this.columns = columns;
    let fromYear = 2013;
    const endYear = 2021;
    while (!statistics.allItemsYearHistogram[fromYear] && fromYear < endYear) {
      fromYear++;
    }
    const fromYearAllValue: number[] = [];
    for (let i = 0; i < statistics.itemTypeStatistics.length + 1; i++) {
      fromYearAllValue.push(0);
    }
    this.data = [];
    for (let year = fromYear; year <= endYear; year++) {
      const row = [];
      row.push(year);
      fromYearAllValue[0] += statistics.allItemsYearHistogram[year];
      row.push(fromYearAllValue[0]);
      for (let i = 0; i < statistics.itemTypeStatistics.length; i++) {
        fromYearAllValue[i + 1] += statistics.itemTypeStatistics[i] && statistics.itemTypeStatistics[i].itemsYearHistogram[year] ? statistics.itemTypeStatistics[i].itemsYearHistogram[year] : 0;
        row.push(fromYearAllValue[i + 1]);
      }
      this.data.push(row);
    }
  }


  @Input()
  organizationId: number | null;

  chartType = ChartType.LineChart;
  data: Row[] = [];
  columns: Column[] = [];
  readonly options = {
    // curveType: 'function',
    legend: {position: 'bottom'},
    vAxis: {format: ''},
    hAxis: {format: ''},
    chartArea: {width: '90%', height: '70%'},
    pointsVisible: true,
    colors: ['red', ...ChartService.COLORS, '#DD80CC'],
    crosshair: {
      color: '#000',
      orientation: 'vertical',
      trigger: 'both'
    },
  };

}
