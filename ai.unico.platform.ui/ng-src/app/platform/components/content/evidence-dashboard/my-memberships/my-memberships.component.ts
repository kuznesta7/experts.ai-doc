import { Component, Input, OnInit } from '@angular/core';
import { OrganizationBaseDto } from 'ng-src/app/interfaces/organization-base-dto';

@Component({
  selector: 'app-my-memberships',
  templateUrl: './my-memberships.component.html'
})
export class MyMembershipsComponent implements OnInit {

  @Input() myMemberships: OrganizationBaseDto[];

  constructor() {
  }

  ngOnInit(): void {
  }

}
