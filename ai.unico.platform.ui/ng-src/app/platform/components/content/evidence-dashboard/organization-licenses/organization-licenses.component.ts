import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {Observable} from "rxjs";
import {
  EvidenceSettingsService,
  OrganizationLicencePreviewDto, OrganizationUserDto
} from "../../../../../services/evidence-settings.service";
import {ActivatedRoute} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {UrlParams} from "../../../../../constants/url-params";
import {OrganizationRole} from "../../../../../services/session.service";
import {ExpertsModalComponent} from "../../evidence-settings/experts-modal/experts-modal.component";


@Component({
  selector: 'app-organization-licences',
  templateUrl: './organization-licenses.component.html',
  styleUrls: [ './organization-licenses.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrganizationLicensesComponent implements OnInit{
  organizationId: number;
  data$: Observable<OrganizationLicencePreviewDto[]>;
  organizationTypes: OrganizationType[];

  constructor(private route: ActivatedRoute,
              private evidenceSettingsService: EvidenceSettingsService,
              private ngbModal: NgbModal) {
  }

  ngOnInit(): void {
    this.organizationId = this.route.snapshot.params[UrlParams.ORGANIZATION_ID];
    this.data$ = this.evidenceSettingsService.findAvailableOrganizationsRoles(this.organizationId);

    this.organizationTypes = [
      {
        title: 'Organization viewer',
        buttonTitle: 'Add organization viewer',
        licenseRole: OrganizationRole.ORGANIZATION_VIEWER
      },
      {
        title: 'Organization editor',
        buttonTitle: 'Add organization editor',
        licenseRole: OrganizationRole.ORGANIZATION_EDITOR
      },
      {
        title: 'User manager',
        buttonTitle: 'Add user manager',
        licenseRole: OrganizationRole.ORGANIZATION_USER
      },
    ];
  }

  removeRole(user: OrganizationUserDto, role: OrganizationRole): void {
    this.evidenceSettingsService
      .removeOrganizationRoles(user.userId, this.organizationId, role)
      .subscribe(e => location.reload());
  }

  showExpertsModal(role: OrganizationRole): void {
    const ngbModalRef = this.ngbModal.open(ExpertsModalComponent, {size: 'lg'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.role = role;
    componentInstance.organizationId = this.organizationId;
  }


}

export interface OrganizationType {
  title: string;
  buttonTitle: string;
  licenseRole: OrganizationRole;
}


