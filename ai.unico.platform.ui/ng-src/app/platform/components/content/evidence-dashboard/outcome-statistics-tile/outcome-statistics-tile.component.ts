import { Component, Input, OnInit } from '@angular/core';
import { OutcomeStatistics } from 'ng-src/app/services/organization-statistics.service';
import { ChartType, Row } from 'angular-google-charts';
import { BehaviorSubject } from 'rxjs';
import { NumberFormatService } from 'ng-src/app/shared/services/number-format.service';
import { ChartService } from 'ng-src/app/shared/services/chart.service';

@Component({
  selector: 'app-outcome-statistics-tile',
  templateUrl: './outcome-statistics-tile.component.html'
})
export class OutcomeStatisticsTileComponent implements OnInit {

  @Input()
  set outcomeStatistics(statistics: OutcomeStatistics) {
    const data = statistics.itemTypeStatistics.sort((g1, g2) => g1.itemTypeGroupId - g2.itemTypeGroupId).map(g => {
      return [g.groupTitle, {v: g.numberOfItems, f: this.numberFormatService.shortenNumber(g.numberOfItems)}] as Row;
    });
    this.data$.next(data);
  }

  @Input()
  organizationId: number | null;

  chartType = ChartType.PieChart;

  data$ = new BehaviorSubject<Row[]>([]);
  readonly options = {
    legend: {position: 'right', alignment: 'center'},
    chartArea: {width: '95%', height: '100%'},
    colors: ChartService.COLORS,
    pieHole: 0.4
  };

  constructor(private numberFormatService: NumberFormatService) {
  }

  ngOnInit(): void {
  }

}
