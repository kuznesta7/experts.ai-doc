import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

@Component({
  selector: 'app-counter-tile',
  templateUrl: './counter-tile.component.html'
})
export class CounterTileComponent implements OnInit {

  @Input()
  color: 'purple' | 'primary' | 'orange';

  @Input()
  count: number;

  @Input()
  icon: IconProp;

  @Input()
  title: string;

  @Output()
  add = new EventEmitter<void>();

  constructor() {
  }

  ngOnInit(): void {
  }

}
