import { Component, Input, OnInit } from '@angular/core';
import { ProjectStatistics } from 'ng-src/app/services/organization-statistics.service';
import { BehaviorSubject } from 'rxjs';
import { ChartType, Row, Column } from 'angular-google-charts';
import { NumberFormatService } from 'ng-src/app/shared/services/number-format.service';
import { ChartService } from 'ng-src/app/shared/services/chart.service';

@Component({
  selector: 'app-project-statistics-tile',
  templateUrl: './project-statistics-tile.component.html'
})
export class ProjectStatisticsTileComponent implements OnInit {


  @Input()
  set projectStatistics(statistics: ProjectStatistics) {
    const data = Object.entries(statistics.yearProjectFinancesSumHistogram).map(([y, v]) => {
      return [y, {v: v, f: this.numberFormatService.shortenNumber(v) + ' CZK'}] as Row;
    });
    this.data$.next(data);
    this.generalInformation$.next({sumOfInvestment: statistics.allProjectsFinancesSum/1000000, numberOfProject:statistics.numberOfAllProjects})
    const columns = [];
    columns.push('' as Column);
    columns.push('Project investment sum' as Column);
    this.columns$.next(columns);
  }

  @Input()
  organizationId: number | null;

  chartType = ChartType.ColumnChart;
  generalInformation$ = new BehaviorSubject<{sumOfInvestment: number, numberOfProject: number}>({numberOfProject: 0, sumOfInvestment: 0});
  data$ = new BehaviorSubject<Row[]>([]);
  columns$ = new BehaviorSubject<Column[]>([])
  readonly options = {
    // curveType: 'function',
    colors: ChartService.COLORS,
    legend: {position: 'bottom'},
    vAxis: {format: 'short'},
    hAxis: {gridlines: {
        minSpacing: 20
      }
    },
    chartArea: {width: '90%', height: '70%'}
  };

  constructor(private numberFormatService: NumberFormatService) {
  }

  ngOnInit(): void {
  }

}
