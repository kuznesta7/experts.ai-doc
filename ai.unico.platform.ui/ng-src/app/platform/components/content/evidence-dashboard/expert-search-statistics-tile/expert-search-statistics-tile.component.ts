import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import {
  ExpertSearchStatisticsWrapper,
  OrganizationStatisticsService
} from 'ng-src/app/services/organization-statistics.service';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { filter, share, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-expert-search-statistics-tile',
  templateUrl: './expert-search-statistics-tile.component.html'
})
export class ExpertSearchStatisticsTileComponent implements OnInit {

  @Input()
  set organizationId(organizationId: number | null) {
    this.organizationId$.next(organizationId);
  }

  organizationId$ = new BehaviorSubject<number | null>(null);
  statisticsWrapper$: Observable<ExpertSearchStatisticsWrapper>;
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 10});

  constructor(private statisticsService: OrganizationStatisticsService) {
  }

  ngOnInit(): void {
    this.statisticsWrapper$ = combineLatest([this.organizationId$, this.pageInfo$]).pipe(
      filter(([id]) => !!id),
      switchMap(([organizationId, pageInfo]) => this.statisticsService.getExpertSearchStatistics((organizationId as number), pageInfo)),
      share()
    );
  }

}
