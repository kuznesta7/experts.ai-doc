import {Component, OnInit} from '@angular/core';
import {debounceTime, filter, first, map, share, shareReplay, switchMap, tap} from 'rxjs/operators';
import {UrlParams} from 'ng-src/app/constants/url-params';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {
  BasicStatistics,
  CommercializationStatistics,
  ExpertStatistics,
  MemberStatistics,
  OpportunityStatistics,
  OrganizationStatisticsService,
  OutcomeStatistics,
  ProjectStatistics
} from 'ng-src/app/services/organization-statistics.service';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {OrganizationAutocompleteService} from 'ng-src/app/services/organization-autocomplete.service';
import {tapAllHandleError} from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {
  AddExpertModalComponent
} from 'ng-src/app/platform/components/common/add-expert-modal/add-expert-modal.component';
import {UserExpertPreviewDto} from 'ng-src/app/services/expert-autocomplete.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ExpertOrganizationService} from 'ng-src/app/services/expert-organization.service';
import {
  OutcomeDetailEditModalComponent
} from 'ng-src/app/platform/components/content/outcome-detail/outcome-detail-edit-modal/outcome-detail-edit-modal.component';
import {
  ProjectDetailEditModalComponent
} from 'ng-src/app/platform/components/content/project-detail/project-detail-edit-modal/project-detail-edit-modal.component';
import {OrganizationDetailService} from 'ng-src/app/services/organization-detail.service';
import {PathFragment} from 'ng-src/app/constants/path-fragment';
import {AddMembersModalComponent} from '../../common/add-members-modal/add-members-modal.component';
import {OrganizationStructureRelation} from 'ng-src/app/enums/organization-structure-relation';
import {OrganizationStructureService} from 'ng-src/app/services/organization-structure.service';
import {
  CommercializationDetailEditModalComponent
} from '../commercialization-detail/commercialization-detail-edit-modal/commercialization-detail-edit-modal.component';
import {
  OpportunityEditModalComponent
} from '../opportunity-detail/opportunity-edit-modal/opportunity-edit-modal.component';
import {FlashService} from 'ng-src/app/shared/services/flash.service';
import {OrganizationTypeService} from 'ng-src/app/services/organization-type.service';
import {
  EquipmentEditModalComponent
} from 'ng-src/app/platform/components/content/equipment-detail/equipment-edit-modal/equipment-edit-modal.component';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-evidence-dashboard',
  templateUrl: './evidence-dashboard.component.html'
})
export class EvidenceDashboardComponent implements OnInit {

  organizationId$: Observable<number> = this.route.params.pipe(
    map(p => p[UrlParams.ORGANIZATION_ID]),
    shareReplay()
  );
  expertStatistics$: Observable<ExpertStatistics>;
  outcomeStatistics$: Observable<OutcomeStatistics>;
  projectStatistics$: Observable<ProjectStatistics>;
  commercializationStatistics$: Observable<CommercializationStatistics>;
  opportunityStatistics$: Observable<OpportunityStatistics>;
  equipmentStatistics$: Observable<BasicStatistics>;
  organization$: Observable<OrganizationBaseDto>;
  memberCount$: Observable<number>;
  memberStatistics$: Observable<MemberStatistics>;
  myMemberships$: Observable<OrganizationBaseDto[]>;
  loading$ = new BehaviorSubject(true);
  paths = PathFragment;
  organizationId: number;
  loaderHelper$ = new Subject<void>();
  emptyStatistics: Observable<BasicStatistics>;

  constructor(private route: ActivatedRoute,
              private ngbModal: NgbModal,
              private statisticsService: OrganizationStatisticsService,
              private organizationService: OrganizationAutocompleteService,
              private organizationDetailService: OrganizationDetailService,
              private expertOrganizationService: ExpertOrganizationService,
              private organizationStructureService: OrganizationStructureService,
              private flashService: FlashService,
              private organizationTypeService: OrganizationTypeService,
              private router: Router,
              private titleService: Title) {
  }

  ngOnInit(): void {
    this.organizationId$ = this.route.params.pipe(
      map(p => p[UrlParams.ORGANIZATION_ID]),
      shareReplay()
    );
    this.organizationId$.subscribe((id: number) => {
      this.organizationId = id;
    });
    this.organizationId$.subscribe((id: number) => {this.organizationId = id; });

    const loaderHelper$ = new Subject<void>();
    this.organization$ = this.organizationId$.pipe(
      tap(() => this.loading$.next(true)),
      switchMap(organizationId => this.organizationDetailService.getOrganizationDetail(organizationId)),
      tapAllHandleError(() => loaderHelper$.next()),
      share()
    );

    this.organization$.pipe().subscribe(organization => {
      const organizationName = organization.organizationName;
      this.titleService.setTitle('Dashboard - ' + organizationName);
    });

    this.expertStatistics$ = this.loadExpertsStatistics();
    this.outcomeStatistics$ = this.loadOutcomesStatistics();
    this.projectStatistics$ = this.organizationId$.pipe(
      switchMap(organizationId => this.statisticsService.getProjectCumulativeStatistics(organizationId)),
      tapAllHandleError(() => loaderHelper$.next()),
      share()
    );
    this.commercializationStatistics$ = this.organizationId$.pipe(
      switchMap(organizationId => this.statisticsService.getCommercializationStatistics(organizationId)),
      tapAllHandleError(() => loaderHelper$.next()),
      share()
    );
    this.opportunityStatistics$ = this.loadOpportunitiesStatistics();
    this.equipmentStatistics$ = this.organizationId$.pipe(
      switchMap(organizationId => this.statisticsService.getServiceEquipmentStatistics(organizationId)),
      tapAllHandleError(() => loaderHelper$.next()),
      share()
    );
    this.memberStatistics$ = this.loadMemberStatistics();
    this.myMemberships$ = this.organizationId$.pipe(
      switchMap(organizationId => this.organizationDetailService.findMyMemberships(organizationId)),
      tapAllHandleError(() => loaderHelper$.next()),
      share()
    );

    loaderHelper$.pipe(
      filter((v, i) => (i + 1) % 5 === 0)
    ).subscribe(() => this.loading$.next(false));
  }

  openExpertCreateModal(event: any): void {
    if (event.target.tagName !== 'BUTTON') {
      this.router.navigate(['/', this.paths.EVIDENCE, this.paths.ORGANIZATIONS, this.organizationId, this.paths.EXPERTS]);
      return;
    }
    this.organizationId$.pipe(debounceTime(0), first()).subscribe(id => {
      const ngbModalRef = this.ngbModal.open(AddExpertModalComponent, {size: 'xl'});
      const componentInstance = ngbModalRef.componentInstance;
      componentInstance.organizationId = id;
      componentInstance.doAddExpert = (e: UserExpertPreviewDto) => {
        return this.expertOrganizationService.addOrganization(e, id, true);
      };
      ngbModalRef.hidden.subscribe(() => {
        this.expertStatistics$ = this.loadExpertsStatistics();
      });
    });
  }

  createNewOutcome(event: any): void {
    if (event.target.tagName !== 'BUTTON') {
      this.router.navigate(['/', this.paths.EVIDENCE, this.paths.ORGANIZATIONS, this.organizationId, this.paths.OUTCOMES]);
      return;
    }
    this.organizationId$.subscribe(id => {
      const ngbModalRef = this.ngbModal.open(OutcomeDetailEditModalComponent, {size: 'xl', backdrop: 'static'});
      const componentInstance = ngbModalRef.componentInstance;
      componentInstance.organizationId = id;
      ngbModalRef.hidden.subscribe(() => {
        this.outcomeStatistics$ = this.loadOutcomesStatistics();
      });
    });

  }

  createNewProject(event: any): void {
    if (event.target.tagName !== 'BUTTON') {
      this.router.navigate(['/', this.paths.EVIDENCE, this.paths.ORGANIZATIONS, this.organizationId, this.paths.PROJECTS]);
      return;
    }
    this.organizationId$.subscribe(id => {
      const ngbModalRef = this.ngbModal.open(ProjectDetailEditModalComponent, {size: 'xl', backdrop: 'static'});
      const componentInstance = ngbModalRef.componentInstance;
      componentInstance.organizationId = id;
    });
  }

  createNewCommercialization(event: any): void {
    if (event.target.tagName !== 'BUTTON') {
      this.router.navigate(['/', this.paths.EVIDENCE, this.paths.ORGANIZATIONS, this.organizationId, this.paths.COMMERCIALIZATION]);
      return;
    }
    this.organizationId$.subscribe(id => {
      const ngbModalRef = this.ngbModal.open(CommercializationDetailEditModalComponent, {size: 'xl', backdrop: 'static'});
      const componentInstance = ngbModalRef.componentInstance;
      componentInstance.organizationId$.next(id);
    });
  }

  createNewOpportunity(event: any): void {
    if (event.target.tagName !== 'BUTTON') {
      this.router.navigate(['/', this.paths.EVIDENCE, this.paths.ORGANIZATIONS, this.organizationId, this.paths.OPPORTUNITY]);
      return;
    }
    this.organizationId$.subscribe(id => {
      const ngbModalRef = this.ngbModal.open(OpportunityEditModalComponent, {size: 'xl', backdrop: 'static'});
      const componentInstance = ngbModalRef.componentInstance;
      componentInstance.opportunity$.next(null);
      componentInstance.organizationId$.next(id);
      ngbModalRef.hidden.subscribe(() => {
        this.opportunityStatistics$ = this.loadOpportunitiesStatistics();
      });
    });
  }

  createNewEquipment(event: any): void {
    if (event.target.tagName !== 'BUTTON') {
      this.router.navigate(['/', this.paths.EVIDENCE, this.paths.ORGANIZATIONS, this.organizationId, this.paths.EQUIPMENT]);
      return;
    }
    this.organizationId$.subscribe(id => {
      const ngbModalRef = this.ngbModal.open(EquipmentEditModalComponent, {size: 'xl', backdrop: 'static'});
      const componentInstance = ngbModalRef.componentInstance;
      componentInstance.equipment$.next(null);
      componentInstance.organizationId$.next(id);
      ngbModalRef.hidden.subscribe(() => {
        this.equipmentStatistics$ = this.loadServiceStatistics();
      });
      ngbModalRef.result.then((equipmentCode: string) => this.router.navigate(['/', this.paths.EQUIPMENT, equipmentCode]));
    });
  }

  notImplementedYetTileFunction(event: any): void {
    this.flashService.default('Not implemented yet. We are working on it!');
    if (event.target.tagName !== 'BUTTON') {
      this.router.navigate(['/', this.paths.EVIDENCE, this.paths.ORGANIZATIONS, this.organizationId]);
    }
  }

  addNewMember(event: any): void {
    if (event.target.tagName !== 'BUTTON') {
      this.router.navigate(['/', this.paths.EVIDENCE, this.paths.ORGANIZATIONS, this.organizationId, this.paths.MEMBERS]);
      return;
    }
    this.organizationId$.pipe(debounceTime(0), first()).subscribe(id => {
      const ngbModalRef = this.ngbModal.open(AddMembersModalComponent, {size: 'xl'});
      const componentInstance = ngbModalRef.componentInstance;
      componentInstance.organizationId = id;
      ngbModalRef.hidden.subscribe(() => {
        this.expertStatistics$ = this.loadExpertsStatistics();
      });
      componentInstance.doAddOrganization = (o: OrganizationBaseDto) => {
        return this.organizationStructureService.addLowerOrganization(id, o.organizationId, OrganizationStructureRelation.MEMBER);
      };
    });
  }

  updateGdpr(event: any): void {
    console.log(event.target.checked);
    console.log('bls');
    this.organizationDetailService
      .setSearchableOrganization(this.organizationId, event.target.checked)
      .subscribe(x => console.log('send'));
  }

  loadExpertsStatistics(): Observable<ExpertStatistics> {
    return this.organizationId$.pipe(
      switchMap(organizationId => this.statisticsService.getExpertStatistics(organizationId)),
      tapAllHandleError(() => this.loaderHelper$.next()),
      share()
    );
  }

  loadOutcomesStatistics(): Observable<OutcomeStatistics> {
    return this.organizationId$.pipe(
      switchMap(organizationId => this.statisticsService.getOutcomeStatistics(organizationId)),
      tapAllHandleError(() => this.loaderHelper$.next()),
      share()
    );
  }

  loadOpportunitiesStatistics(): Observable<OpportunityStatistics> {
    return this.organizationId$.pipe(
      switchMap(organizationId => this.statisticsService.getOpportunityStatistics(organizationId)),
      tapAllHandleError(() => this.loaderHelper$.next()),
      share()
    );
  }

  loadServiceStatistics(): Observable<BasicStatistics> {
    return this.organizationId$.pipe(
      switchMap(organizationId => this.statisticsService.getServiceEquipmentStatistics(organizationId)),
      tapAllHandleError(() => this.loaderHelper$.next()),
      share()
    );
  }

  loadMemberStatistics(): Observable<MemberStatistics> {
    return this.organizationId$.pipe(
      switchMap(organizationId => this.statisticsService.getMemberCount(organizationId)),
      tapAllHandleError(() => this.loaderHelper$.next()),
      share()
    );
  }
}
