import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import {
  KeywordStatisticsWrapper,
  OrganizationStatisticsService
} from 'ng-src/app/services/organization-statistics.service';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { filter, share, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-keyword-statistics-tile',
  templateUrl: './keyword-statistics-tile.component.html'
})
export class KeywordStatisticsTileComponent implements OnInit {


  @Input()
  set organizationId(organizationId: number | null) {
    this.organizationId$.next(organizationId);
  }

  organizationId$ = new BehaviorSubject<number | null>(null);
  keywordStatistics$: Observable<KeywordStatisticsWrapper>;
  keywordStatisticsPageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 10});

  constructor(private statisticsService: OrganizationStatisticsService) {
  }

  ngOnInit(): void {
    this.keywordStatistics$ = combineLatest([this.organizationId$, this.keywordStatisticsPageInfo$]).pipe(
      filter(([id]) => !!id),
      switchMap(([organizationId, pageInfo]) => this.statisticsService.getKeywordStatistics((organizationId as number), pageInfo).pipe(share())),
      share()
    );
  }

}
