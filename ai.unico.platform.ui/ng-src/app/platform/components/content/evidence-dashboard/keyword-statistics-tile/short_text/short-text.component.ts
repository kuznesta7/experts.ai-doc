import {Component, Input, OnInit} from "@angular/core";

@Component({
  selector: 'app-short-text',
  templateUrl: 'short-text.component.html'
  })
export class ShortTextComponent implements OnInit {
  @Input() text: string

  hideText = true;
  parsedText = '';
  ngOnInit() {
    let arrayOfKeywords = this.text.toLowerCase().split(' or ');
    if (arrayOfKeywords.length === 1){
      this.parsedText = this.text;
    } else this.parsedText = arrayOfKeywords[0].replace(/"/gi,'').trim();
  }

  toggleHiddenText = () => {
    this.hideText = !this.hideText;
  }
}
