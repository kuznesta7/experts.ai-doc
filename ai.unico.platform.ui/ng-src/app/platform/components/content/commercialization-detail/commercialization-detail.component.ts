import { Component, Input, OnInit } from '@angular/core';
import { EvidenceCommercializationService } from 'ng-src/app/services/evidence-commercialization.service';
import { CommercializationDetailDto } from 'ng-src/app/interfaces/commercialization-detail-dto';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  finalize,
  map,
  shareReplay,
  startWith,
  switchMap,
  tap
} from 'rxjs/operators';
import { UrlParams } from 'ng-src/app/constants/url-params';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, combineLatest, Observable, of, Subject } from 'rxjs';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommercializationDetailEditModalComponent } from 'ng-src/app/platform/components/content/commercialization-detail/commercialization-detail-edit-modal/commercialization-detail-edit-modal.component';
import { CommercializationOrganizationRelation } from 'ng-src/app/services/commercialization-enum.service';
import { FilePreview } from 'ng-src/app/platform/components/common/file-preview/file-preview.component';
import { environment } from 'ng-src/environments/environment';
import { UploadService } from 'ng-src/app/services/upload.service';
import { ProjectDetailMode } from 'ng-src/app/platform/components/content/project-detail/project-detail.component';

@Component({
  selector: 'app-commercialization-detail',
  templateUrl: './commercialization-detail.component.html'
})
export class CommercializationDetailComponent implements OnInit {

  @Input()
  commercializationDetail: CommercializationDetailDto;
  loading$ = new BehaviorSubject(true);
  commercializationDetail$: Observable<CommercializationDetailDto>;
  update$ = new Subject<void>();
  updateDocuments$ = new Subject<void>();
  commercializationOrganizationRelations = CommercializationOrganizationRelation;
  mode$: Observable<CommercializationDetailMode>;
  documentsLoading$ = new BehaviorSubject<boolean>(true);
  modes = CommercializationDetailMode;
  documents$: Observable<ExtendedFilePreview[]>;

  constructor(private commercializationService: EvidenceCommercializationService,
              private route: ActivatedRoute,
              private ngbModal: NgbModal,
              private uploadService: UploadService) {
  }

  ngOnInit(): void {
    this.mode$ = this.route.queryParams.pipe(
      map(params => params.mode),
      filter(m => !!m),
      startWith(ProjectDetailMode.OVERVIEW),
      distinctUntilChanged(),
      shareReplay()
    );

    const commercializationId$ = this.route.params.pipe(
      map(p => p[UrlParams.COMMERCIALIZATION_ID])
    );
    this.commercializationDetail$ = combineLatest([commercializationId$, this.update$.pipe(startWith(null))]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([id]) => this.commercializationService.getCommercializationDetail(id)),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );

    this.documents$ = combineLatest([this.commercializationDetail$, this.updateDocuments$.pipe(startWith(true))]).pipe(
      tap(() => this.documentsLoading$.next(true)),
      debounceTime(0),
      switchMap(([p]) => {
        if (p.commercializationProjectId) {
          return this.commercializationService.getCommercializationDocuments(p.commercializationProjectId).pipe(
            tapAllHandleError(() => this.documentsLoading$.next(false)),
            map(documents => documents.map(d => {
              return {
                fileName: d.documentName,
                fileSize: d.size,
                fileType: d.documentType,
                downloadUrl: `${environment.internalRestUrl}commercialization/projects/${p.commercializationProjectId}/documents/${d.documentId}`,
                documentId: d.documentId,
                deleting$: new BehaviorSubject(false)
              };
            }))
          );
        }
        return of([]);
      }),
      shareReplay()
    );
  }

  back(): void {
    window.history.back();
  }

  openEditCommercializationModal(commercializationDetail: CommercializationDetailDto): void {
    const ngbModalRef = this.ngbModal.open(CommercializationDetailEditModalComponent, {size: 'xl'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.commercializationDetail$.next(commercializationDetail);
    ngbModalRef.closed.subscribe(() => this.update$.next());
  }

  uploadDocument(projectId: number): void {
    this.uploadService.openUploadModal((files) => {
      return combineLatest(files.map(f => this.commercializationService.addCommercializationDocument(projectId, f))).pipe(
        tap(() => this.updateDocuments$.next())
      );
    }, {multiple: true});
  }

  deleteFile(projectId: number, document: ExtendedFilePreview): void {
    document.deleting$.next(true);
    this.commercializationService.deleteCommercializationDocument(projectId, document.documentId).pipe(
      finalize(() => document.deleting$.next(false))
    ).subscribe(() => this.updateDocuments$.next());
  }
}

enum CommercializationDetailMode {
  OVERVIEW = 'OVERVIEW',
  DOCUMENTS = 'DOCUMENTS'
}

interface ExtendedFilePreview extends FilePreview {
  documentId: number;
  deleting$: BehaviorSubject<boolean>;
}
