import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, combineLatest, Observable, of} from 'rxjs';
import {
  CommercializationDetailDto,
  OrganizationWithRelationBaseDto,
  TeamMemberBaseDto
} from 'ng-src/app/interfaces/commercialization-detail-dto';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ExpertAutocompleteService} from 'ng-src/app/services/expert-autocomplete.service';
import {EvidenceCommercializationService} from 'ng-src/app/services/evidence-commercialization.service';
import {
  CommercializationEnumService,
  CommercializationOrganizationRelation,
  CommercializationStatus
} from 'ng-src/app/services/commercialization-enum.service';
import {TagService} from 'ng-src/app/services/tag.service';
import {filter, finalize, first, map, shareReplay, switchMap, tap} from 'rxjs/operators';
import {OrganizationAutocompleteService} from 'ng-src/app/services/organization-autocomplete.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormValidationService} from 'ng-src/app/shared/services/form-validation.service';
import {CommercializationOrganizationService} from 'ng-src/app/services/commercialization-organization.service';
import {CommercializationExpertService} from 'ng-src/app/services/commercialization-expert.service';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {PathService} from 'ng-src/app/shared/services/path.service';
import {Router} from '@angular/router';
import {UserExpertBaseDto} from 'ng-src/app/interfaces/user-expert-base-dto';

@Component({
  selector: 'app-commercialization-detail-edit-modal',
  templateUrl: './commercialization-detail-edit-modal.component.html',
})
export class CommercializationDetailEditModalComponent implements OnInit {

  readonly organizationRelations = [
    {
      title: 'Intellectual property owners',
      relation: CommercializationOrganizationRelation.INTELLECTUAL_PROPERTY_OWNER
    },
    {title: 'Commercialization partners', relation: CommercializationOrganizationRelation.COMMERCIALIZATION_PARTNER},
    {title: 'Investors', relation: CommercializationOrganizationRelation.INVESTOR},
  ];
  organizationId$ = new BehaviorSubject<number | null>(null);

  commercializationProjectId$ = new BehaviorSubject<number | null>(null);
  commercializationDetail$ = new BehaviorSubject<CommercializationDetailDto | null>(null);
  imgFilePreviewUrl$ = new BehaviorSubject<string | null>(null);
  imgFile$ = new BehaviorSubject<File | null>(null);

  formGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    executiveSummary: new FormControl('', Validators.required),
    useCase: new FormControl(),
    painDescription: new FormControl(),
    competitiveAdvantage: new FormControl(),
    technicalPrinciples: new FormControl(),
    technologyReadinessLevel: new FormControl(),
    startDate: new FormControl(),
    endDate: new FormControl(),
    statusDeadline: new FormControl(),
    commercializationDomainId: new FormControl(),
    link: new FormControl('', Validators.pattern(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/)),
    keywords: new FormControl([]),
    commercializationCategoryIds: new FormControl([]),
    commercializationStatusId: new FormControl()
  });

  teamMembers$ = new BehaviorSubject<TeamMemberBaseDto[]>([]);
  organizationSearchFg = new FormGroup(this.organizationRelations.reduce((controls, relation) => {
    controls[relation.relation] = new FormControl('');
    return controls;
  }, {} as any));
  organizations$ = new BehaviorSubject<OrganizationWithRelationBaseDto[]>([]);

  commercializationOrganizationRelations = CommercializationOrganizationRelation;

  commercializationCategories$ = this.commercializationEnumService.loadCategories(false).pipe(map(categories => categories.map(c => ({
    id: c.commercializationCategoryId,
    description: c.commercializationCategoryName
  }))));
  commercializationPriorities$ = this.commercializationEnumService.loadPriorities(false).pipe(map(priorities => priorities.map(p => ({
    id: p.priorityTypeId,
    description: p.priorityTypeName
  }))));
  commercializationDomains$ = this.commercializationEnumService.loadDomains(false).pipe(map(domains => domains.map(d => ({
    id: d.domainId,
    description: d.domainName
  }))));

  private requests: Observable<any>[] = [];
  private validCommercializationProjectId$: Observable<number> = this.commercializationProjectId$.pipe(filter(id => !!id), first(), map(v => v as number));
  commercializationStatuses$: Observable<CommercializationStatus[]>;

  lookupKeywords = this.tagService.lookupFunction;

  saving$ = new BehaviorSubject(false);

  constructor(private commercializationService: EvidenceCommercializationService,
              private commercializationOrganizationService: CommercializationOrganizationService,
              private commercializationExpertService: CommercializationExpertService,
              private commercializationEnumService: CommercializationEnumService,
              private tagService: TagService,
              private expertAutocompleteService: ExpertAutocompleteService,
              private organizationAutocompleteService: OrganizationAutocompleteService,
              private ngbActiveModal: NgbActiveModal,
              private formValidationService: FormValidationService,
              private router: Router,
              private pathService: PathService) {
  }

  ngOnInit(): void {
    this.commercializationDetail$.subscribe(p => {
      if (!p) {
        return;
      }
      this.commercializationProjectId$.next(p.commercializationProjectId);
      this.formGroup.patchValue({
        ...p,
        startDate: new Date(p.startDate),
        endDate: new Date(p.endDate),
        statusDeadline: p.statusDeadline
      });
      this.teamMembers$.next(p.teamMemberDtos);
      this.organizations$.next(p.projectOrganizations);

    });
    this.organizationId$.subscribe(id => {
      if (!id) {
        return;
      }
      this.organizationAutocompleteService.getOrganizationsForAutocomplete([id]).subscribe(o => {
        this.doAddOrganization(o[0], CommercializationOrganizationRelation.INTELLECTUAL_PROPERTY_OWNER);
      });
    });

    this.commercializationStatuses$ = this.commercializationEnumService.loadStatuses(false);
  }

  dismiss(): void {
    this.ngbActiveModal.dismiss();
  }

  saveChanges(): void {
    this.formValidationService.validateForm(this.formGroup);
    const project = {...this.commercializationDetail$.value, ...this.formGroup.getRawValue()};
    this.saving$.next(true);
    let request: Observable<any>;
    if (project.commercializationProjectId) {
      request = this.commercializationService.updateCommercialization(project);
    } else {
      request = this.commercializationService.createCommercialization(project).pipe(tap(id => {
        this.commercializationProjectId$.next(id);
        project.commercializationProjectId = id;
      }));
    }
    const imgFile = this.imgFile$.getValue();
    let imgRequest = of('');
    if (imgFile) {
      imgRequest = this.validCommercializationProjectId$.pipe(switchMap(id => this.commercializationService.updateCommercializationImg(id, imgFile)));
    }
    combineLatest([...this.requests, request, imgRequest]).pipe(
      finalize(() => this.saving$.next(false))
    ).subscribe(() => {
      this.ngbActiveModal.close(project);
      if (!this.commercializationDetail$.value) {
        const pathWithReplacedParams = this.pathService.getPathWithReplacedParams('commercializationDetail', this.commercializationProjectId$.value);
        this.router.navigate([pathWithReplacedParams]);
      }
    });
  }

  filterOrganizations(organizations: OrganizationWithRelationBaseDto[], relation: CommercializationOrganizationRelation): OrganizationBaseDto[] {
    return organizations.filter(o => o.relation === relation);
  }

  organizationChange(event: { organizationId: number, added: boolean }, relation: CommercializationOrganizationRelation): void {
    if (event.added) {
      this.requests.push(this.validCommercializationProjectId$.pipe(switchMap(id => this.commercializationOrganizationService.addOrganization(id, event.organizationId, relation)), shareReplay()));
    } else {
      this.requests.push(this.validCommercializationProjectId$.pipe(switchMap(id => this.commercializationOrganizationService.removeOrganization(id, event.organizationId, relation)), shareReplay()));
    }
  }

  isOwnerRelation(relation: CommercializationOrganizationRelation): boolean {
    return relation === CommercializationOrganizationRelation.INTELLECTUAL_PROPERTY_OWNER;
  }

  private doAddOrganization(organization: OrganizationBaseDto, relation: CommercializationOrganizationRelation): void {
    this.requests.push(this.validCommercializationProjectId$.pipe(switchMap(id => this.commercializationOrganizationService.addOrganization(id, organization.organizationId, relation)), shareReplay()));
    this.organizations$.next(this.organizations$.value.concat({...organization, relation}));
  }

  expertChange(event: { expert: UserExpertBaseDto, added: boolean }, teamLeader?: boolean): void {
    const value = {...event.expert, teamLeader} as TeamMemberBaseDto;
    if (event.added) {
      this.requests.push(this.validCommercializationProjectId$.pipe(switchMap(id => this.commercializationExpertService.addTeamMember(id, value)), shareReplay()));
    } else {
      this.requests.push(this.validCommercializationProjectId$.pipe(switchMap(id => this.commercializationExpertService.removeTeamMember(id, event.expert)), shareReplay()));
    }
  }
}
