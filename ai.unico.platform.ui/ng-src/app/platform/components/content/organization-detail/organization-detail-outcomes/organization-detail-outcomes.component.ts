import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { OrganizationDetailService } from 'ng-src/app/services/organization-detail.service';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { filter, shareReplay, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { EvidenceItemWrapper } from 'ng-src/app/interfaces/evidence-item-wrapper';

@Component({
  selector: 'app-organization-detail-outcomes',
  templateUrl: './organization-detail-outcomes.component.html',
})
export class OrganizationDetailOutcomesComponent implements OnInit {

  @Input()
  set organizationId(id: number) {
    this.organizationId$.next(id);
  }

  organizationId$ = new BehaviorSubject<number | null>(null);
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 5});
  loading$ = new BehaviorSubject<boolean>(true);
  outcomeWrapper$: Observable<EvidenceItemWrapper>;

  constructor(private organizationDetailService: OrganizationDetailService) {
  }

  ngOnInit(): void {
    this.outcomeWrapper$ = combineLatest([this.organizationId$, this.pageInfo$]).pipe(
      filter(([id]) => !!id),
      tap(() => this.loading$.next(true)),
      switchMap(([id, pi]) => this.organizationDetailService.getOrganizationOutcomes(id as number, pi)),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );
  }

}
