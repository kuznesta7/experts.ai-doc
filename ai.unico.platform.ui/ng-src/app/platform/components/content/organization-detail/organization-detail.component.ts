import { Component, OnInit } from '@angular/core';
import {
  OrganizationDetail,
  OrganizationDetailService,
  PublicOrganizationStatistics
} from 'ng-src/app/services/organization-detail.service';
import { ActivatedRoute } from '@angular/router';
import { AuthorizationService } from 'ng-src/app/services/authorization.service';
import { QueryParamService } from 'ng-src/app/shared/services/query-param.service';
import { filter, map, shareReplay, switchMap, tap } from 'rxjs/operators';
import { UrlParams } from 'ng-src/app/constants/url-params';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { UploadService } from 'ng-src/app/services/upload.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OrganizationDetailEditModalComponent } from 'ng-src/app/platform/components/content/organization-detail/organization-detail-edit-modal/organization-detail-edit-modal.component';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {
  OrganizationStructureDto,
  OrganizationStructureService
} from 'ng-src/app/services/organization-structure.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ExternalMessageDto } from 'ng-src/app/interfaces/external-message';

@Component({
  selector: 'app-organization-detail',
  templateUrl: './organization-detail.component.html'
})
export class OrganizationDetailComponent implements OnInit {

  contactForm: FormGroup;
  contactFormResult = new ExternalMessageDto();
  organizationId: number;

  constructor(private organizationDetailService: OrganizationDetailService,
              private route: ActivatedRoute,
              private authorizationService: AuthorizationService,
              private queryParamService: QueryParamService,
              private uploadService: UploadService,
              private fb: FormBuilder,
              private organizationStructureService: OrganizationStructureService,
              private ngbModal: NgbModal) {
    this.contactForm = this.fb.group({
      message: ['', Validators.required]
    });
  }

  organizationDetail$: Observable<OrganizationDetail>;
  organizationStructure$: Observable<OrganizationStructureDto[]>;
  organizationKeywords$: Observable<{ keyword: string, occurrence: number }[]>;
  statistics$: Observable<PublicOrganizationStatistics | null>;
  editable$: Observable<boolean>;
  update$ = new Subject<void>();
  reloadImgUrl$: Observable<string>;
  loading$ = new BehaviorSubject(true);
  statisticsLoading$ = new BehaviorSubject(true);

  listModes = ListMode;
  listModeDetails: ListModeDetail[] = [
    {
      outcomeListMode: ListMode.EXPERT,
      title: 'Experts',
      countProperty: 'numberOfExperts',
      icon: 'users' as any
    }, {
      outcomeListMode: ListMode.RESEARCH_OUTCOME,
      title: 'Research outcomes',
      countProperty: 'numberOfOutcomes',
      icon: 'file-alt' as any
    }, {
      outcomeListMode: ListMode.RESEARCH_PROJECT,
      title: 'Research projects',
      countProperty: 'numberOfProjects',
      icon: 'microscope' as any
    },
  ];
  listMode$ = new BehaviorSubject<ListMode | null>(null);

  ngOnInit(): void {
    const organizationId$ = this.route.params.pipe(
      map(p => p[UrlParams.ORGANIZATION_ID])
    );


    organizationId$.subscribe(id => {
      this.organizationId = id;
    });

    this.editable$ = organizationId$.pipe(
      switchMap(id => this.authorizationService.canEditOrganizationProfile(id))
    );

    this.organizationDetail$ = organizationId$.pipe(
      tap(() => this.loading$.next(true)),
      switchMap(organizationId => this.organizationDetailService.getOrganizationDetail(organizationId)),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );
    this.organizationKeywords$ = organizationId$.pipe(
      switchMap(organizationId => this.organizationDetailService.getOrganizationKeywords(organizationId)),
      filter(keywords => !!keywords),
      map(keywords => {
        const newList = Object.entries(keywords).map(([k, o]) => {
          return {keyword: k, occurrence: o};
        });
        newList.sort((a, b) => b.occurrence - a.occurrence);
        return newList;
      }),
      shareReplay()
    );

    this.organizationStructure$ = organizationId$.pipe(
      switchMap(organizationId => this.organizationStructureService.getStructure(organizationId)),
      shareReplay()
    );

    this.statistics$ = organizationId$.pipe(
      tap(() => this.statisticsLoading$.next(true)),
      switchMap(id => this.organizationDetailService.getOrganizationStatistics(id)),
      tap(stats => {
        if (!stats) {
          return;
        }
        const found = this.listModeDetails.find(lm => (stats as any)[lm.countProperty] > 0);
        if (found) {
          this.listMode$.next(found.outcomeListMode);
        }
      }),
      tapAllHandleError(() => this.statisticsLoading$.next(false)),
      shareReplay()
    );

    let v = 2;
    this.reloadImgUrl$ = this.update$.pipe(
      map(() => '&v=' + v++),
      shareReplay()
    );
  }

  changeCoverImg(organizationDetail: OrganizationDetail): any {
    this.uploadService.openUploadModal(files => {
      return this.organizationDetailService.updateOrganizationImg(organizationDetail.organizationId, 'COVER', files[0]);
    }, {
      title: 'Edit organization cover image',
      fileType: 'image',
      preview: true,
      multiple: false
    }).closed.subscribe(result => {
      this.update$.next();
    });
  }

  openContactModal(content: any): any {
    this.ngbModal.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.contactFormResult = Object.assign({}, this.contactForm.value);
      this.contactFormResult.organizationId = this.organizationId;
      this.contactFormResult.messageType = 'ORGANIZATION';
      if (result === 'Save click') {
        this.organizationDetailService.contactOrganization(this.contactFormResult).subscribe(value => {
        });
      }
    }, () => {
    });
  }

  openEditModal(organizationDetail: OrganizationDetail): any {
    const ngbModalRef = this.ngbModal.open(OrganizationDetailEditModalComponent, {size: 'xl'});
    const componentInstance = ngbModalRef.componentInstance as OrganizationDetailEditModalComponent;
    componentInstance.organization = organizationDetail;
    ngbModalRef.closed.subscribe(() => {
      this.update$.next();
    });
  }
}

enum ListMode {
  EXPERT, RESEARCH_OUTCOME, RESEARCH_PROJECT
}

interface ListModeDetail {
  outcomeListMode: ListMode;
  title: string;
  countProperty: string;
  icon: any;
}
