import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { OrganizationDetail, OrganizationDetailService } from 'ng-src/app/services/organization-detail.service';
import { finalize } from 'rxjs/operators';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-organization-detail-edit-modal',
  templateUrl: './organization-detail-edit-modal.component.html'
})
export class OrganizationDetailEditModalComponent implements OnInit {

  organization: OrganizationDetail;

  formGroup = new FormGroup({
    organizationName: new FormControl('', Validators.required),
    organizationAbbrev: new FormControl(''),
    organizationDescription: new FormControl(''),
    phone: new FormControl(''),
    email: new FormControl(''),
    street: new FormControl(''),
    city: new FormControl(''),
    web: new FormControl(''),
  });

  saving$ = new BehaviorSubject(false);
  imgFilePreviewUrl$ = new BehaviorSubject<string | null>(null);
  imgFile$ = new BehaviorSubject<File | null>(null);

  constructor(private organizationService: OrganizationDetailService,
              private activeModal: NgbActiveModal) {
  }

  ngOnInit(): void {
    this.formGroup.patchValue(this.organization)
  }

  saveChanges() {
    const value = this.formGroup.getRawValue();
    this.formGroup.disable();
    this.saving$.next(true);
    const requests = [this.organizationService.updateOrganizationDetail(this.organization.organizationId, value)];
    const imgFile = this.imgFile$.getValue();
    if (imgFile) {
      requests.push(this.organizationService.updateOrganizationImg(this.organization.organizationId, 'LOGO', imgFile));
    }
    combineLatest(requests).pipe(
      finalize(() => {
        this.saving$.next(false);
        this.formGroup.enable();
      })
    ).subscribe(() => {
      this.activeModal.close({...value, ...this.organization});
    });
  }

  dismiss() {
    this.activeModal.dismiss();
  }

}
