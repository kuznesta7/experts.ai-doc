import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { OrganizationDetailService } from 'ng-src/app/services/organization-detail.service';
import { EvidenceExpertWrapper } from 'ng-src/app/interfaces/evidence-expert-wrapper';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { filter, shareReplay, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';

@Component({
  selector: 'app-organization-detail-experts',
  templateUrl: './organization-detail-experts.component.html',
})
export class OrganizationDetailExpertsComponent implements OnInit {

  @Input()
  set organizationId(id: number) {
    this.organizationId$.next(id);
  }

  organizationId$ = new BehaviorSubject<number | null>(null);
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 5});
  loading$ = new BehaviorSubject<boolean>(true);
  expertsWrapper$: Observable<EvidenceExpertWrapper>;

  constructor(private organizationDetailService: OrganizationDetailService) {
  }

  ngOnInit(): void {
    this.expertsWrapper$ = combineLatest([this.organizationId$, this.pageInfo$]).pipe(
      filter(([id]) => !!id),
      tap(() => this.loading$.next(true)),
      switchMap(([id, pi]) => this.organizationDetailService.getOrganizationExperts(id as number, pi)),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );
  }

}
