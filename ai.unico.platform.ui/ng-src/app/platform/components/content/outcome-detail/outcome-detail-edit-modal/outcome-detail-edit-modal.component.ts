import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ItemDetailDto} from 'ng-src/app/interfaces/item-detail-dto';
import {Confidentiality} from 'ng-src/app/interfaces/confidetiality';
import {switchMap} from 'rxjs/operators';
import {OrganizationAutocompleteService} from 'ng-src/app/services/organization-autocomplete.service';
import {ExpertAutocompleteService} from 'ng-src/app/services/expert-autocomplete.service';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {UserExpertBaseDto} from 'ng-src/app/interfaces/user-expert-base-dto';
import {TagService} from 'ng-src/app/services/tag.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {EvidenceItemService} from 'ng-src/app/services/evidence-item.service';
import {ItemExpertService} from 'ng-src/app/services/item-expert.service';
import {ItemOrganizationService} from 'ng-src/app/services/item-organization.service';
import {FormValidationService} from 'ng-src/app/shared/services/form-validation.service';
import {SecurityService} from 'ng-src/app/services/security.service';
import {OrganizationRole} from 'ng-src/app/services/session.service';
import {ItemTypeService} from 'ng-src/app/services/item-type.service';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {NotRegisteredExpertDTO} from "ng-src/app/interfaces/item-not-registered-expert";

@Component({
  selector: 'app-outcome-detail-edit-modal',
  templateUrl: './outcome-detail-edit-modal.component.html'
})
export class OutcomeDetailEditModalComponent implements OnInit {

  constructor(private organizationAutocompleteService: OrganizationAutocompleteService,
              private expertAutocompleteService: ExpertAutocompleteService,
              private itemService: EvidenceItemService,
              private itemExpertService: ItemExpertService,
              private itemOrganizationService: ItemOrganizationService,
              private activeModal: NgbActiveModal,
              private tagService: TagService,
              private formValidationService: FormValidationService,
              private securityService: SecurityService,
              private itemTypeService: ItemTypeService) {
  }

  formGroup = new FormGroup({
    itemName: new FormControl('', Validators.required),
    itemDescription: new FormControl('', Validators.required),
    year: new FormControl('', Validators.required),
    itemTypeId: new FormControl('', Validators.required),
    confidentiality: new FormControl(Confidentiality.PUBLIC, Validators.required),
    keywords: new FormControl([])
  });

  experts$ = new BehaviorSubject<UserExpertBaseDto[]>([]);
  notRegisteredExperts$ = new BehaviorSubject<NotRegisteredExpertDTO[]>([]);

  organizationId: number;
  organizationId$ = new BehaviorSubject<number | null>(null);
  organizations$ = new BehaviorSubject<OrganizationBaseDto[]>([]);
  userId: number;
  saving$ = new BehaviorSubject<boolean>(false);
  outcome$ = new BehaviorSubject<ItemDetailDto | null>(null);
  outcomeId$ = new BehaviorSubject<number | null>(null);
  confidentiality = Confidentiality;
  confidentialityEditable$: Observable<boolean>;
  itemTypes$ = this.itemTypeService.findItemTypes();
  organizations: OrganizationBaseDto[] = [];
  experts: UserExpertBaseDto[] = [];
  notRegisteredExperts: NotRegisteredExpertDTO[] = [];

  lookupKeywords = this.tagService.lookupFunction;

  ngOnInit(): void {
    this.organizationId$.next(this.organizationId);
    this.outcome$.subscribe(o => {
      if (!o) {
        return;
      }
      this.formGroup.patchValue(o);
      this.organizations$.next(o.itemOrganizations);
      this.experts$.next(o.itemExperts);
      this.notRegisteredExperts$.next(o.itemNotRegisteredExperts);
      this.outcomeId$.next(o.itemId);
    });

    this.confidentialityEditable$ = this.outcome$.pipe(switchMap(o => {
      if (!o && this.organizationId) {
        return this.securityService.hasOrganizationRole(this.organizationId, OrganizationRole.ORGANIZATION_EDITOR);
      }
      if (!o || !o.ownerOrganizationId) {
        return of(false);
      }
      return this.securityService.hasOrganizationRole(o.ownerOrganizationId, OrganizationRole.ORGANIZATION_EDITOR);
    }));
  }

  organizationChange(organizationBaseDtos: OrganizationBaseDto[]): void {
    this.organizations = organizationBaseDtos;
  }

  expertsChange(userExpertBaseDtos: UserExpertBaseDto[]): void {
    this.experts = userExpertBaseDtos;
  }

  notRegisteredExpertsChange(notRegisteredExpertDTOs: NotRegisteredExpertDTO[]): void {
    this.notRegisteredExperts = notRegisteredExpertDTOs;
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }

  saveChanges(): void {
    this.formValidationService.validateForm(this.formGroup);
    this.outcome$.subscribe(o => {

    });
    this.outcome$.pipe(
      switchMap(o => {
        const outcome: ItemDetailDto = {...o,
          ...this.formGroup.getRawValue(),
          itemOrganizations: this.organizations,
          itemExperts: this.experts,
          itemNotRegisteredExperts: this.notRegisteredExperts
        };
        this.saving$.next(true);
        if (o === null) {
          return this.itemService.createItem(outcome);
        } else {
          return this.itemService.updateItem(outcome);
        }
      }
    )).subscribe((id: number) => {
      this.saving$.next(false);
      this.activeModal.close(id);
    });
  }
}
