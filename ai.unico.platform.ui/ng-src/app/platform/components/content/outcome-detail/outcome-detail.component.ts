import {Component, OnInit} from '@angular/core';
import {map, shareReplay, startWith, switchMap, tap} from 'rxjs/operators';
import {UrlParams} from 'ng-src/app/constants/url-params';
import {PlatformDataSource} from 'ng-src/app/enums/platform-data-source';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {BehaviorSubject, combineLatest, Observable, Subject} from 'rxjs';
import {ItemDetailDto} from 'ng-src/app/interfaces/item-detail-dto';
import {EvidenceItemService} from 'ng-src/app/services/evidence-item.service';
import {tapAllHandleError} from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {
  OutcomeDetailEditModalComponent
} from 'ng-src/app/platform/components/content/outcome-detail/outcome-detail-edit-modal/outcome-detail-edit-modal.component';
import {PathService} from 'ng-src/app/shared/services/path.service';
import {OrganizationLicenceService} from 'ng-src/app/services/organization-licence.service';
import {ContactModalComponent} from 'ng-src/app/platform/components/common/contact-modal/contact-modal.component';
import {PathFragment} from 'ng-src/app/constants/path-fragment';

@Component({
  selector: 'app-outcome-detail',
  templateUrl: './outcome-detail.component.html'
})
export class OutcomeDetailComponent implements OnInit {

  loading$ = new BehaviorSubject<boolean>(true);
  update$ = new Subject<void>();
  outcomeDetail$: Observable<ItemDetailDto>;

  constructor(private route: ActivatedRoute,
              private ngbModal: NgbModal,
              private router: Router,
              private pathService: PathService,
              private organizationLicenseService: OrganizationLicenceService,
              private itemService: EvidenceItemService) {
  }

  ngOnInit(): void {
    const outcomeCode$ = this.route.params.pipe(
      map(p => p[UrlParams.OUTCOME_CODE])
    );

    this.outcomeDetail$ = combineLatest([outcomeCode$, this.update$.pipe(startWith(true))]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([code]) => this.itemService.getItemDetail(code)),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );
  }

  back(): void {
    window.history.back();
  }

  openEditOutcomeModal(outcome: ItemDetailDto): void {
    const ngbModalRef = this.ngbModal.open(OutcomeDetailEditModalComponent, {size: 'xl'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.outcome$.next(outcome);
    ngbModalRef.result.then((id: number) => {
      if (!outcome.itemId && id !== null) {
        this.router.navigate(['/', PathFragment.OUTCOMES, 'ST' + id]);
      } else {
        this.update$.next();
      }
    });
  }
}
