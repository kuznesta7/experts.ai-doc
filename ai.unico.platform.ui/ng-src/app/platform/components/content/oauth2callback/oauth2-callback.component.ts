import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {combineLatest} from 'rxjs';
import {OauthService} from 'ng-src/app/services/oauth.service';
import {FlashService} from 'ng-src/app/shared/services/flash.service';
import {localStorageGet} from 'ng-src/app/shared/utils/localStorageUtil';
import {SessionService} from "../../../../services/session.service";

@Component({
  selector: 'app-oauth2-callback',
  templateUrl: './oauth2-callback.component.html'
})
export class Oauth2CallbackComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private router: Router,
              private oauthService: OauthService,
              private flashService: FlashService,
              private sessionService: SessionService) {
  }

  ngOnInit(): void {
    combineLatest([this.route.params, this.route.queryParams]).subscribe(([rp, qp]) => {
      const service = rp.service;
      const code = qp.code;
      this.oauthService.doLogin(service, code).subscribe(() => {
        if (localStorageGet(OauthService.REDIRECT_URL_KEY) === '/login'){
          this.sessionService.updateSession().subscribe((tmp) => {
            if (tmp.organizationRoles.length > 0){
              this.router.navigate(['/evidence', 'organizations', tmp.organizationRoles[0].organizationId]);
            } else {
              this.router.navigate(['/experts', tmp.expertCode]);
            }
          })
        } else {
          this.router.navigateByUrl(localStorageGet(OauthService.REDIRECT_URL_KEY));
        }
        this.flashService.ok('You have been successfully signed in');
      });
    });
  }

}
