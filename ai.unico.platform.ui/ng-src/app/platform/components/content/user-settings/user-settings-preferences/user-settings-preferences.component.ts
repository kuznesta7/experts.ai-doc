import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserSettingsService } from 'ng-src/app/services/user-settings.service';
import { FlashService } from 'ng-src/app/shared/services/flash.service';
import { Role, SessionService } from 'ng-src/app/services/session.service';
import { filter, finalize, first, map, shareReplay, switchMap } from 'rxjs/operators';
import { CountryService } from 'ng-src/app/services/country.service';
import { BehaviorSubject } from 'rxjs';
import { UserRegistrationDto } from 'ng-src/app/interfaces/user-registration-dto';
import { FormValidationService } from 'ng-src/app/shared/services/form-validation.service';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-user-settings-preferences',
  templateUrl: './user-settings-preferences.component.html'
})
export class UserSettingsPreferencesComponent implements OnInit {

  constructor(private userSettingsService: UserSettingsService,
              private flashService: FlashService,
              private countryService: CountryService,
              private sessionService: SessionService,
              private formValidationService: FormValidationService,
              private titleService: Title) {
    this.titleService.setTitle('Settings - Account preferences');
  }

  formGroup = new FormGroup({
    email: new FormControl('', Validators.required),
    countryOfInterestCodes: new FormControl([]),
    userRolePresets: new FormControl([])
  });
  countries$ = this.countryService.findCountries().pipe(
    map(countries => countries.map(c => {
      return {id: c.countryCode, description: c.countryName};
    })),
    shareReplay()
  );

  roles = [
    {id: Role.ACADEMIC_EXPERT, description: 'Academic expert'},
    {id: Role.COMPANY_EXPERT, description: 'Company expert'},
    {id: Role.INVESTOR, description: 'Investor'},
  ];
  loading$ = new BehaviorSubject<boolean>(true);
  saving$ = new BehaviorSubject<boolean>(false);
  private allSettings: UserRegistrationDto | null;

  ngOnInit(): void {
    this.sessionService.getCurrentSession().pipe(
      filter(s => !!s),
      first(),
      switchMap(s => this.userSettingsService.getUserPreferences(s.userId)),
      finalize(() => this.loading$.next(false))
    ).subscribe(u => {
      this.allSettings = u;
      this.formGroup.patchValue(u);
    });
  }

  updateSettings(): void {
    if (!this.allSettings || !this.allSettings.id) {
      return;
    }
    this.formValidationService.validateForm(this.formGroup);
    this.saving$.next(true);
    const data = {...this.allSettings, ...this.formGroup.value};
    this.formGroup.disable();
    this.userSettingsService.updateUserPreferences(this.allSettings.id, data).pipe(
      finalize(() => {
        this.saving$.next(false);
        this.formGroup.enable();
      })
    ).subscribe(() => this.flashService.ok('Your preferences have been updated'));
  }
}
