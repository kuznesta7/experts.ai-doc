import { Component, OnInit } from '@angular/core';
import { UserSettingsService } from 'ng-src/app/services/user-settings.service';
import { BehaviorSubject } from 'rxjs';
import { finalize, shareReplay } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';
import { FlashService } from 'ng-src/app/shared/services/flash.service';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-user-settings-consents',
  templateUrl: './user-settings-consents.component.html',
})
export class UserSettingsConsentsComponent implements OnInit {

  constructor(private userSettingsService: UserSettingsService,
              private flashService: FlashService,
              private titleService: Title) {
    this.titleService.setTitle('Settings - Consents');
  }

  saving$ = new BehaviorSubject<boolean>(false);
  loading$ = new BehaviorSubject<boolean>(true);
  formGroup = new FormGroup({});
  consents$ = this.userSettingsService.getUserConsents().pipe(
    finalize(() => this.loading$.next(false)),
    shareReplay()
  );

  ngOnInit(): void {
    this.consents$.subscribe(consents => {
      consents.forEach(c => {
        this.formGroup.addControl(c.consentId, new FormControl(c.valid))
      });
    });
  }

  updateConsents() {
    this.consents$.subscribe(consents => {
      const data = consents.map(c => {
        const value = this.formGroup.value;
        return {...c, valid: value[c.consentId]};
      });
      this.saving$.next(true);
      this.userSettingsService.updateUserConsents(data).pipe(
        finalize(() => this.saving$.next(false))
      ).subscribe(() => this.flashService.ok('Your preferences have been updated'));
    });
  }

}
