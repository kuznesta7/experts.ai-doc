import { Component, OnInit } from '@angular/core';
import { ClaimProfileService, ClaimSuggestionProfile } from 'ng-src/app/services/claim-profile.service';
import { SessionService } from 'ng-src/app/services/session.service';
import { finalize, map, shareReplay, switchMap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { BehaviorSubject, Observable } from 'rxjs';
import { ConfirmationModalOptions, ConfirmationService } from 'ng-src/app/shared/services/confirmation.service';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-user-settings-claim-profiles',
  templateUrl: './user-settings-claim-profiles.component.html',
})
export class UserSettingsClaimProfilesComponent implements OnInit {

  constructor(private claimProfileService: ClaimProfileService,
              private sessionService: SessionService,
              private confirmationService: ConfirmationService,
              private titleService: Title) {
    this.titleService.setTitle('Settings - Claim profiles');
  }

  loading$ = new BehaviorSubject<boolean>(true);
  suggestions$: Observable<ClaimSuggestionContainer[]> = this.sessionService.getCurrentSession().pipe(
    switchMap(s => this.claimProfileService.findClaimSuggestions(s.fullName).pipe(
      tapAllHandleError(() => this.loading$.next(false))
    )),
    map(suggestions => suggestions.map(s => {
      return {profile: s, saving$: new BehaviorSubject(false), claimed$: new BehaviorSubject(s.claimed)};
    })),
    shareReplay()
  );

  ngOnInit(): void {
  }

  claimProfile(container: ClaimSuggestionContainer) {
    const options: ConfirmationModalOptions = {
      title: 'Claim profile',
      content: 'Are you sure you want to claim selected profile?',
      confirmLabel: 'Claim'
    };
    this.confirmationService.confirm(options).subscribe(() => {
      let request$;
      const userId = container.profile.userId;
      if (userId) {
        request$ = this.claimProfileService.claimUserProfile(userId);
      } else {
        request$ = this.claimProfileService.claimExpertProfile(container.profile.expertId);
      }
      container.saving$.next(true);
      request$.pipe(
        finalize(() => container.saving$.next(false))
      ).subscribe(() => container.claimed$.next(true));
    });
  }

  unclaimProfile(container: ClaimSuggestionContainer) {
    const options: ConfirmationModalOptions = {
      title: 'Revert profile claim',
      content: 'Are you sure you want to revert claim of the selected profile?',
      confirmLabel: 'Revert claim',
      confirmClass: 'danger'
    };
    this.confirmationService.confirm(options).subscribe(() => {
      let request$;
      const userId = container.profile.userId;
      if (userId) {
        request$ = this.claimProfileService.unclaimUserProfile(userId);
      } else {
        request$ = this.claimProfileService.unclaimExpertProfile(container.profile.expertId);
      }
      container.saving$.next(true);
      request$.pipe(
        finalize(() => container.saving$.next(false))
      ).subscribe(() => container.claimed$.next(false));
    });
  }
}

interface ClaimSuggestionContainer {
  profile: ClaimSuggestionProfile;
  saving$: BehaviorSubject<boolean>;
  claimed$: BehaviorSubject<boolean>;
}
