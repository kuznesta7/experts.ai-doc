import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormValidationService } from 'ng-src/app/shared/services/form-validation.service';
import { FlashService } from 'ng-src/app/shared/services/flash.service';
import { UserSettingsService } from 'ng-src/app/services/user-settings.service';
import { finalize, map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { SessionService } from 'ng-src/app/services/session.service';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-user-settings-password',
  templateUrl: './user-settings-password.component.html',
})
export class UserSettingsPasswordComponent implements OnInit {

  formGroup = new FormGroup({
    currentPassword: new FormControl('', Validators.required),
    newPassword: new FormControl('', Validators.required),
    confirmPassword: new FormControl('', Validators.required),
  });
  saving$ = new BehaviorSubject<boolean>(false);
  email$ = this.sessionService.getCurrentSession().pipe(map(s => s.username));

  constructor(private formValidationService: FormValidationService,
              private flashService: FlashService,
              private sessionService: SessionService,
              private userSettingsService: UserSettingsService,
              private titleService: Title) {
    this.titleService.setTitle('Settings - Change password');
  }

  ngOnInit(): void {
  }

  submit(): void {
    this.formValidationService.validateForm(this.formGroup);
    const value = this.formGroup.value;
    const newPassword = value.newPassword;
    if (newPassword !== value.confirmPassword) {
      this.flashService.error('Entered passwords must match');
      return;
    }
    this.saving$.next(true);
    this.formGroup.disable();
    this.userSettingsService.changePassword(value.currentPassword, newPassword).pipe(
      finalize(() => {
        this.saving$.next(false);
        this.formGroup.enable();
      })
    ).subscribe(() => this.flashService.ok('Your password has been successfully changed'));
  }

}
