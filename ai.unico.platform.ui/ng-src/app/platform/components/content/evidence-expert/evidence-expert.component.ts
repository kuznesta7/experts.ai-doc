import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {EvidenceExpertService} from 'ng-src/app/services/evidence-expert.service';
import {BehaviorSubject, combineLatest, Observable, Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {debounceTime, first, map, share, shareReplay, startWith, switchMap, takeUntil, tap} from 'rxjs/operators';
import {FormControl, FormGroup} from '@angular/forms';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {EvidenceFilter} from 'ng-src/app/interfaces/evidence-filter';
import {EvidenceExpertWrapper} from 'ng-src/app/interfaces/evidence-expert-wrapper';
import {QueryParamService} from 'ng-src/app/shared/services/query-param.service';
import {ExpertOrganizationRelation} from 'ng-src/app/enums/expert-organization-relation';
import {UrlParams} from 'ng-src/app/constants/url-params';
import {PathFragment} from 'ng-src/app/constants/path-fragment';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AddExpertModalComponent} from 'ng-src/app/platform/components/common/add-expert-modal/add-expert-modal.component';
import {UserExpertPreviewDto} from 'ng-src/app/services/expert-autocomplete.service';
import {ExpertOrganizationService} from 'ng-src/app/services/expert-organization.service';
import {EventBusService} from 'ng-src/app/services/event-bus.service';
import {MessageBus} from 'ng-src/app/classes/event.class';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-evidence-expert',
  templateUrl: './evidence-expert.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvidenceExpertComponent implements OnInit, OnDestroy {
  selected: Set<string> = new Set<string>();
  filterFormGroup = new FormGroup({
    query: new FormControl(''),
    organizationRelatedStatistics: new FormControl(false),
    verifiedOnly: new FormControl(false),
    notVerifiedOnly: new FormControl(false),
    retired: new FormControl(false),
    sortByFullName: new FormControl(false),
    expertSearchType: new FormControl(''),
    activeOnly: new FormControl(true),
  });
  expertOrganizationRelations = ExpertOrganizationRelation;

  organizationId$: Observable<number>;
  expertWrapper$: Observable<EvidenceExpertWrapper>;
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 15});
  loading$ = new BehaviorSubject(true);
  expertOrganizationRelation$: Observable<ExpertOrganizationRelation> =
    this.route.data.pipe(
      map((d) => d.expertOrganizationRelation),
      shareReplay()
    );

  private unsubscribe$: Subject<boolean> = new Subject();

  constructor(
    private evidenceExpertService: EvidenceExpertService,
    private route: ActivatedRoute,
    private router: Router,
    private queryParamService: QueryParamService,
    private ngbModal: NgbModal,
    private expertOrganizationService: ExpertOrganizationService,
    private eventBusService: EventBusService,
    private cdr: ChangeDetectorRef,
    private titleService: Title) {
    this.titleService.setTitle('Experts');
    this.organizationId$ = this.route.params.pipe(
      map((p) => p[UrlParams.ORGANIZATION_ID])
    );

    this.queryParamService.handleFilterFormGroup(this.filterFormGroup);

    const filter$ = this.filterFormGroup.valueChanges.pipe(
      debounceTime(300),
      startWith(this.filterFormGroup.value as EvidenceFilter)
    );

    this.eventBusService
      .of(MessageBus)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        this.cdr.markForCheck();

        this.expertWrapper$ = combineLatest([
          this.organizationId$,
          filter$,
          this.pageInfo$,
          this.expertOrganizationRelation$,
        ]).pipe(
          debounceTime(200),
          switchMap(([id, filter, pageInfo, relation]) => {
            this.loading$.next(true);
            if (relation === ExpertOrganizationRelation.AFFILIATED) {
              return this.evidenceExpertService.getAffiliatedExperts(
                id,
                filter,
                pageInfo
              );
            } else {
              return this.evidenceExpertService.getEvidenceExperts(
                id,
                filter,
                pageInfo
              );
            }
          }),
          tap(() => this.loading$.next(false)),
          share()
        );
      });
  }

  ngOnInit(): void {
    this.eventBusService.publish(new MessageBus('expertActionExecuted', true));
  }

  openEditModal(): void {
    this.organizationId$.pipe(debounceTime(0), first()).subscribe((id) => {
      const ngbModalRef = this.ngbModal.open(AddExpertModalComponent, {
        size: 'xl',
      });
      const componentInstance = ngbModalRef.componentInstance;
      componentInstance.organizationId = id;
      componentInstance.doAddExpert = (e: UserExpertPreviewDto) => {
        return this.expertOrganizationService.addOrganization(e, id, true);
      };
      ngbModalRef.dismissed.subscribe(() => {
        this.loading$.next(true);
        componentInstance.expertsChanged.subscribe((e: boolean) => {
          if (e) this.ngOnInit();
        });
        this.loading$.next(false);
      });
    });
  }

  select(expertCode: string): void {
    if (!this.selected.delete(expertCode)) {
      this.selected.add(expertCode);
    }
  }

  changeRelation(relation: ExpertOrganizationRelation): void {
    switch (relation) {
      case ExpertOrganizationRelation.AFFILIATED:
        this.router.navigate([PathFragment.EXPERTS_AFFILIATED], {
          relativeTo: this.route.parent,
        });
        return;
      case ExpertOrganizationRelation.INTERNAL:
      default:
        this.router.navigate([PathFragment.EXPERTS], {
          relativeTo: this.route.parent,
        });
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }
}
