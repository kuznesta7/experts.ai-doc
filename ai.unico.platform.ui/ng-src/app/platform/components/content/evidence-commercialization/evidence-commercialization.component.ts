import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { debounceTime, map, shareReplay, startWith, switchMap } from 'rxjs/operators';
import { UrlParams } from 'ng-src/app/constants/url-params';
import { ActivatedRoute, Router } from '@angular/router';
import { QueryParamService } from 'ng-src/app/shared/services/query-param.service';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { FormControl, FormGroup } from '@angular/forms';
import { EvidenceCommercializationService } from 'ng-src/app/services/evidence-commercialization.service';
import { CommercializationProjectWrapper } from 'ng-src/app/interfaces/commercialization-project-wrapper';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommercializationDetailEditModalComponent } from 'ng-src/app/platform/components/content/commercialization-detail/commercialization-detail-edit-modal/commercialization-detail-edit-modal.component';
import { PathService } from 'ng-src/app/shared/services/path.service';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-evidence-commercialization',
  templateUrl: './evidence-commercialization.component.html'
})
export class EvidenceCommercializationComponent implements OnInit {

  organizationId$: Observable<number>;
  commercializationProjectWrapper$: Observable<CommercializationProjectWrapper>;
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 15});
  loading$ = new BehaviorSubject(true);
  filterFormGroup = new FormGroup({
    query: new FormControl('')
  });

  constructor(private route: ActivatedRoute,
              private router: Router,
              private pathService: PathService,
              private queryParamService: QueryParamService,
              private ngbModal: NgbModal,
              private evidenceCommercializationService: EvidenceCommercializationService,
              private titleService: Title) {
    this.titleService.setTitle('Commercialization');
  }

  ngOnInit(): void {
    this.organizationId$ = this.route.params.pipe(
      map(p => parseInt(p[UrlParams.ORGANIZATION_ID], 10))
    );

    this.queryParamService.handleFilterFormGroup(this.filterFormGroup);

    const filter$ = this.filterFormGroup.valueChanges.pipe(
      debounceTime(300),
      startWith(this.filterFormGroup.value)
    );
    this.commercializationProjectWrapper$ = combineLatest([this.organizationId$, filter$, this.pageInfo$]).pipe(
      debounceTime(0),
      switchMap(([organizationId, filter, pageInfo]) => {
        this.loading$.next(true);
        return this.evidenceCommercializationService.getEvidenceCommercialization(organizationId, filter, pageInfo);
      }),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );
  }

  openCreateCommercializationModal(organizationId: number): void {
    const ngbModalRef = this.ngbModal.open(CommercializationDetailEditModalComponent, {size: 'xl', backdrop: 'static'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.organizationId$.next(organizationId);
  }
}
