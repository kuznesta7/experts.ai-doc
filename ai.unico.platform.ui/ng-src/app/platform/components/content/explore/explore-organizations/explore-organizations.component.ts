import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { FormControl, FormGroup } from '@angular/forms';
import { OrganizationWrapper } from 'ng-src/app/interfaces/organization-wrapper';
import { debounceTime, map, share, startWith, switchMap, tap } from 'rxjs/operators';
import { CountryService } from 'ng-src/app/services/country.service';
import { QueryParamService } from 'ng-src/app/shared/services/query-param.service';
import { ExploreService } from 'ng-src/app/services/explore.service';
import { OrganizationDetailService } from 'ng-src/app/services/organization-detail.service';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-explore-organizations',
  templateUrl: './explore-organizations.component.html'
})
export class ExploreOrganizationsComponent implements OnInit {
  query$ = new BehaviorSubject('');
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 15});
  organizationWrapper$: Observable<OrganizationWrapper>;
  // loading$ = new BehaviorSubject(false);
  stats: any[] = [];
  loading$ = true;

  constructor(private countryService: CountryService,
              private queryParamService: QueryParamService,
              private exploreService: ExploreService,
              private organizationDetailService: OrganizationDetailService,
              private titleService: Title,
  ) {
    this.titleService.setTitle('Explore - Organizations');
  }

  filterFormGroup = new FormGroup({
    countryCodes: new FormControl([]),
    expertSearchType: new FormControl('')
  });

  countries$ = this.countryService.findCountries().pipe(map(countries => {
    return countries.map(c => {
      return {id: c.countryCode, description: c.countryName};
    });
  }));

  ngOnInit(): void {
    this.queryParamService.handleFilterFormGroup(this.filterFormGroup);

    const filter$ = this.filterFormGroup.valueChanges.pipe(
      startWith(this.filterFormGroup.value),
      tap(() => this.pageInfo$.next({...this.pageInfo$.value, page: 1})),
      debounceTime(300)
    );
    this.organizationWrapper$ = combineLatest([this.query$, this.pageInfo$, filter$]).pipe(
      switchMap(([q, pi, f]) => {
        return this.exploreService.searchOrganizations({...f, query: q}, pi).pipe(
          // tapAllHandleError(() => this.loading$.next(false)),
          share()
        );
      }),
      share()
    );
  }

  startLoading(e: boolean): void {
    this.loading$ = e;
  }
}
