import { Component, OnInit } from '@angular/core';
import { ExploreService } from 'ng-src/app/services/explore.service';
import { QueryParamService } from 'ng-src/app/shared/services/query-param.service';
import { OrganizationAutocompleteService } from 'ng-src/app/services/organization-autocomplete.service';
import { CountryService } from 'ng-src/app/services/country.service';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { FormControl, FormGroup } from '@angular/forms';
import { debounceTime, map, share, startWith, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { EvidenceThesisWrapper } from 'ng-src/app/interfaces/evidence-thesis-wrapper';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-explore-thesis',
  templateUrl: './explore-thesis.component.html'
})
export class ExploreThesisComponent implements OnInit {

  constructor(private exploreService: ExploreService,
              private queryParamService: QueryParamService,
              private organizationService: OrganizationAutocompleteService,
              private countryService: CountryService,
              private titleService: Title) {
    this.titleService.setTitle('Explore - Thesis');
  }

  query$ = new BehaviorSubject('');
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 15});
  thesisWrapper$: Observable<EvidenceThesisWrapper>;
  loading$ = new BehaviorSubject(true);

  filterFormGroup = new FormGroup({
    organizationIds: new FormControl([]),
    countryCodes: new FormControl([])
  });

  countries$ = this.countryService.findCountries().pipe(map(countries => {
    return countries.map(c => {
      return {id: c.countryCode, description: c.countryName};
    });
  }));

  loadOrganizations = (ids: number[]) => {
    return this.organizationService.getOrganizationsForAutocompleteStandardized(ids);
  }

  autocompleteOrganizations = (query: string) => {
    return this.organizationService.autocompleteOrganizationsStandardized(query);
  }

  ngOnInit(): void {
    this.queryParamService.handleFilterFormGroup(this.filterFormGroup);

    const filter$ = this.filterFormGroup.valueChanges.pipe(
      startWith(this.filterFormGroup.value),
      tap(() => this.pageInfo$.next({...this.pageInfo$.value, page: 1})),
      debounceTime(300)
    );
    this.thesisWrapper$ = combineLatest([this.query$, this.pageInfo$, filter$]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([q, pi, f]) => {
        return this.exploreService.searchThesis({...f, query: q}, pi).pipe(
          tapAllHandleError(() => this.loading$.next(false)),
          share()
        );
      }),
      share()
    );
  }
}
