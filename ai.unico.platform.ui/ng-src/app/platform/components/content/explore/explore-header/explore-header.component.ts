import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { GlobalSearchService, SearchType } from 'ng-src/app/services/global-search.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-explore-header',
  templateUrl: './explore-header.component.html'
})
export class ExploreHeaderComponent implements OnInit {

  @Input()
  set type(type: SearchType) {
    this.globalSearchService.setLastUsedSearchType(type);
    this.searchType$.next(type);
  }

  @Output()
  query = new EventEmitter<string>();

  searchQuery$ = new BehaviorSubject<string>('');
  searchType$ = new BehaviorSubject<SearchType>('experts');

  constructor(private activatedRoute: ActivatedRoute,
              private globalSearchService: GlobalSearchService) {
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.pipe(
      map(qp => qp.searchQuery),
      filter(p => !!p),
      distinctUntilChanged()
    ).subscribe(q => {
      this.searchQuery$.next(q);
    });
    this.searchQuery$.subscribe(q => this.query.emit(q));
  }

  // setSearchType(type: SearchType) {
  //   const path = this.globalSearchService.getPathForSearchType(type);
  // }
}
