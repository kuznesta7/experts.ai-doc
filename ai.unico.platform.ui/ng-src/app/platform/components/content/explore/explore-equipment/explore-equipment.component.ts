import { Component, OnInit } from '@angular/core';
import { ExploreService } from 'ng-src/app/services/explore.service';
import { QueryParamService } from 'ng-src/app/shared/services/query-param.service';
import { OrganizationAutocompleteService } from 'ng-src/app/services/organization-autocomplete.service';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { FormControl, FormGroup } from '@angular/forms';
import { EvidenceEquipmentWrapper } from 'ng-src/app/interfaces/evidence-equipment-wrapper';
import { debounceTime, share, startWith, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-explore-equipment',
  templateUrl: './explore-equipment.component.html'
})
export class ExploreEquipmentComponent implements OnInit {

  constructor(private exploreService: ExploreService,
              private queryParamService: QueryParamService,
              private organizationService: OrganizationAutocompleteService,
              private titleService: Title) {
    this.titleService.setTitle('Explore - Equipment');
  }


  query$ = new BehaviorSubject('');
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 15});
  equipmentWrapper$: Observable<EvidenceEquipmentWrapper>;
  loading$ = new BehaviorSubject(true);

  filterFormGroup = new FormGroup({
    organizationIds: new FormControl([])
  });

  loadOrganizations = (ids: number[]) => {
    return this.organizationService.getOrganizationsForAutocompleteStandardized(ids);
  }

  autocompleteOrganizations = (query: string) => {
    return this.organizationService.autocompleteOrganizationsStandardized(query);
  }

  ngOnInit(): void {
    this.queryParamService.handleFilterFormGroup(this.filterFormGroup);

    const filter$ = this.filterFormGroup.valueChanges.pipe(
      startWith(this.filterFormGroup.value),
      tap(() => this.pageInfo$.next({...this.pageInfo$.value, page: 1})),
      debounceTime(300)
    );
    this.equipmentWrapper$ = combineLatest([this.query$, this.pageInfo$, filter$]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([q, pi, f]) => {
        return this.exploreService.searchEquipment({...f, query: q}, pi).pipe(
          tapAllHandleError(() => this.loading$.next(false)),
          share()
        );
      }),
      share()
    );
  }

}
