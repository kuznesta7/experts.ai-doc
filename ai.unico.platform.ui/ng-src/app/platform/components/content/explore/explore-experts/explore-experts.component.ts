import { Component, OnInit } from '@angular/core';
import { ExploreService } from 'ng-src/app/services/explore.service';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { FormControl, FormGroup } from '@angular/forms';
import { debounceTime, map, share, startWith, switchMap, tap } from 'rxjs/operators';
import { EvidenceExpertWrapper } from 'ng-src/app/interfaces/evidence-expert-wrapper';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { QueryParamService } from 'ng-src/app/shared/services/query-param.service';
import { OrganizationAutocompleteService } from 'ng-src/app/services/organization-autocomplete.service';
import { CountryService } from 'ng-src/app/services/country.service';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-explore-experts',
  templateUrl: './explore-experts.component.html'
})
export class ExploreExpertsComponent implements OnInit {

  constructor(private exploreService: ExploreService,
              private queryParamService: QueryParamService,
              private organizationService: OrganizationAutocompleteService,
              private countryService: CountryService,
              private titleService: Title) {
    this.titleService.setTitle('Explore - Experts');
  }

  query$ = new BehaviorSubject('');
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 15});
  expertWrapper$: Observable<EvidenceExpertWrapper>;
  loading$ = new BehaviorSubject(true);

  filterFormGroup = new FormGroup({
    organizationIds: new FormControl([]),
    countryCodes: new FormControl([]),
    expertSearchType: new FormControl('')
  });

  countries$ = this.countryService.findCountries().pipe(map(countries => {
    return countries.map(c => {
      return {id: c.countryCode, description: c.countryName};
    });
  }));

  loadOrganizations = (ids: number[]) => {
    return this.organizationService.getOrganizationsForAutocompleteStandardized(ids);
  }
  autocompleteOrganizations = (query: string) => {
    return this.organizationService.autocompleteOrganizationsStandardized(query);
  }

  ngOnInit(): void {
    this.queryParamService.handleFilterFormGroup(this.filterFormGroup);

    const filter$ = this.filterFormGroup.valueChanges.pipe(
      startWith(this.filterFormGroup.value),
      tap(() => this.pageInfo$.next({...this.pageInfo$.value, page: 1})),
      debounceTime(300)
    );
    this.expertWrapper$ = combineLatest([this.query$, this.pageInfo$, filter$]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([q, pi, f]) => {
        return this.exploreService.searchExperts({...f, query: q}, pi).pipe(
          tapAllHandleError(() => this.loading$.next(false)),
          share()
        );
      }),
      share()
    );
  }
}
