import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject, combineLatest, Observable, of} from 'rxjs';
import {ProjectDetailDto} from 'ng-src/app/interfaces/project-detail-dto';
import {debounceTime, filter, first, map, startWith, switchMap, tap} from 'rxjs/operators';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {UserExpertBaseDto} from 'ng-src/app/interfaces/user-expert-base-dto';
import {Confidentiality} from 'ng-src/app/interfaces/confidetiality';
import {OrganizationRole} from 'ng-src/app/services/session.service';
import {SecurityService} from 'ng-src/app/services/security.service';
import {ExpertAutocompleteService} from 'ng-src/app/services/expert-autocomplete.service';
import {TagService} from 'ng-src/app/services/tag.service';
import {ProjectProgramDto, ProjectProgramService} from 'ng-src/app/services/project-program.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {EvidenceProjectService} from 'ng-src/app/services/evidence-project.service';
import {ProjectExpertService} from 'ng-src/app/services/project-expert.service';
import {ProjectOrganizationService} from 'ng-src/app/services/project-organization.service';
import {FormValidationService} from 'ng-src/app/shared/services/form-validation.service';
import {Router} from '@angular/router';
import {PathService} from 'ng-src/app/shared/services/path.service';
import {ProjectAutocompleteService} from 'ng-src/app/services/project-autocomplete.service';
import {PlatformDataSource} from 'ng-src/app/enums/platform-data-source';

@Component({
  selector: 'app-project-detail-edit-modal',
  templateUrl: './project-detail-edit-modal.component.html'
})
export class ProjectDetailEditModalComponent implements OnInit {
  organizationId$ = new BehaviorSubject<number | null>(null);

  constructor(private securityService: SecurityService,
              private activeModal: NgbActiveModal,
              private expertAutocompleteService: ExpertAutocompleteService,
              private projectAutocompleteService: ProjectAutocompleteService,
              private tagService: TagService,
              private projectProgramService: ProjectProgramService,
              private formValidationService: FormValidationService,
              private evidenceProjectService: EvidenceProjectService,
              private projectExpertService: ProjectExpertService,
              private projectOrganizationService: ProjectOrganizationService,
              private router: Router,
              private pathService: PathService) {
  }

  project$ = new BehaviorSubject<ProjectDetailDto | null>(null);
  projectId$ = new BehaviorSubject<number | null>(null);
  private validProjectId$ = this.projectId$.pipe(filter(id => !!id), first(), map(v => v as number));
  private requests: Observable<any>[] = [];
  projectProgram = new FormControl('');
  organizationIds: number[] = [];
  experts: UserExpertBaseDto[] = [];

  formGroup = new FormGroup({
    projectCode: new FormControl(),
    projectNumber: new FormControl(),
    name: new FormControl('', Validators.required),
    description: new FormControl(),
    startDate: new FormControl(),
    endDate: new FormControl(),
    budgetTotal: new FormControl(),
    confidentiality: new FormControl(Confidentiality.PUBLIC),
    keywords: new FormControl([]),
    organizationFinanceMap: new FormGroup({}),
    projectProgram: this.projectProgram
  });

  organizations$ = new BehaviorSubject<OrganizationBaseDto[]>([]);
  experts$ = new BehaviorSubject<UserExpertBaseDto[]>([]);
  programs$ = new BehaviorSubject<ProjectProgramDto[]>([]);

  programOptions$: Observable<ProjectProgramDto[]> = combineLatest([this.projectProgram.valueChanges, this.programs$]).pipe(
    debounceTime(300),
    switchMap(([q, programs]: [string, ProjectProgramDto[]]) => this.projectAutocompleteService.autocompleteProjectProgram(q))
  );

  saving$ = new BehaviorSubject<boolean>(false);
  confidentiality = Confidentiality;
  confidentialityEditable$: Observable<boolean>;
  years$ = new BehaviorSubject<number[]>([]);

  projectPrograms$ = this.projectProgramService.getProjectPrograms();

  lookupKeywords = (q: string) => this.tagService.findRelevantTags(q).pipe(map(tags => tags.map(t => ({
    id: t,
    description: t
  }))))

  ngOnInit(): void {
    this.project$.subscribe(p => {
      if (!p) {
        return;
      }
      this.projectId$.next(p.projectId);
      this.experts$.next(p.projectExperts);
      this.organizations$.next(p.organizationBaseDtos);
      p.organizationBaseDtos.forEach(x => this.organizationIds.push(x.organizationId));
      p.projectExperts.forEach(x => this.experts.push(x));
      this.formGroup.patchValue({...p, startDate: new Date(p.startDate), endDate: new Date(p.endDate)});
    });

    this.confidentialityEditable$ = combineLatest([this.organizationId$, this.project$]).pipe(switchMap(([oId, p]) => {
      if (oId && !p) {
        return this.securityService.hasOrganizationRole(oId, OrganizationRole.ORGANIZATION_EDITOR);
      } else if (p && p.ownerOrganizationId) {
        return this.securityService.hasOrganizationRole(p.ownerOrganizationId, OrganizationRole.ORGANIZATION_EDITOR);
      }
      return of(false);
    }));

    const financeMapFg = this.formGroup.controls.organizationFinanceMap as FormGroup;
    const startDateFc = this.formGroup.controls.startDate;
    const endDateFc = this.formGroup.controls.endDate;
    const startDate$ = startDateFc.valueChanges.pipe(startWith(startDateFc.value));
    const endDate$ = endDateFc.valueChanges.pipe(startWith(endDateFc.value));
    combineLatest([this.project$, this.organizations$, startDate$, endDate$]).subscribe(([project, organizations, startDate, endDate]) => {
      const years: number[] = [];
      if (!startDate || !endDate) {
        return;
      }
      const startYear = (startDate as Date).getFullYear();
      const endYear = (endDate as Date).getFullYear();
      for (let y = startYear; y <= endYear; y++) {
        years.push(y);
        organizations.forEach(o => {
          const controlKey = String(o.organizationId);
          if (!financeMapFg.controls[controlKey]) {
            financeMapFg.addControl(controlKey, new FormGroup({}));
          }
          const organizationFinanceMapFg = financeMapFg.controls[controlKey] as FormGroup;
          if (organizationFinanceMapFg.controls[y]) {
            return;
          }
          const organizationFinances = project?.organizationFinanceMap[o.organizationId] || {};
          const value = organizationFinances[y];
          organizationFinanceMapFg.addControl(String(y), new FormControl(value));
        });
      }
      this.years$.next(years);
    });
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }

  saveChanges(): void {
    this.formValidationService.validateForm(this.formGroup);
    const project = {...this.project$.value, ...this.formGroup.value};
    this.saving$.next(true);

    combineLatest([this.organizationId$, this.experts$, this.organizations$]).pipe(
      // finalize(() => this.saving$.next(false)),
      switchMap(([organizationId, experts, organizations]: [number | null, UserExpertBaseDto[], OrganizationBaseDto[]]) => {
        project.expertIds = this.experts.map(x => x.expertId).filter(x => x !== null);
        project.userIds = this.experts.map(x => x.userId).filter(x => x !== null);
        project.organizationIds = this.organizationIds;

        if (project.originalProjectId && !project.projectId) {
          return this.evidenceProjectService.moveProject(project.originalProjectId).pipe(switchMap(id => {
            project.projectId = id;
            this.projectId$.next(id);
            return this.evidenceProjectService.updateProject(project);
          }));
        } else if (project.projectId) {
          return this.evidenceProjectService.updateProject(project);
        } else {
          return this.evidenceProjectService.createProject(project).pipe((tap(id => this.projectId$.next(id))));
        }
      }),
    ).subscribe(() => {
      this.saving$.next(false);
      this.activeModal.close(project);
      if (!this.project$.value) {
        const projectId = this.projectId$.value;
        const path = this.pathService.getPathWithReplacedParams('projectDetail', PlatformDataSource.STORE + projectId);
        this.router.navigate([path]);
      }
    });
  }

  organizationChange(event: { organizationId: number, added: boolean }): void {
    if (event.added) {
      this.organizationIds.push(event.organizationId);
    } else {
      this.organizationIds = this.organizationIds.filter(id => id !== event.organizationId);
    }
  }

  expertChange(event: { expert: UserExpertBaseDto, added: boolean }): void {
    if (event.added) {
      this.experts.push(event.expert);
    } else {
      this.experts = this.experts.filter(x => x.expertCode !== event.expert.expertCode);
    }
  }

  displayFn(value?: ProjectProgramDto): string {
    return value ? value.projectProgramName : '';
  }
}
