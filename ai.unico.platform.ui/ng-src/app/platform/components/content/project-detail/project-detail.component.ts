import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, combineLatest, Observable, of, Subject} from 'rxjs';
import {ProjectDetailDto} from 'ng-src/app/interfaces/project-detail-dto';
import {EvidenceProjectService} from 'ng-src/app/services/evidence-project.service';
import {ActivatedRoute, Router} from '@angular/router';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  finalize,
  map,
  shareReplay,
  startWith,
  switchMap,
  tap
} from 'rxjs/operators';
import {UrlParams} from 'ng-src/app/constants/url-params';
import {tapAllHandleError} from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {
  ProjectDetailEditModalComponent
} from 'ng-src/app/platform/components/content/project-detail/project-detail-edit-modal/project-detail-edit-modal.component';
import {PathService} from 'ng-src/app/shared/services/path.service';
import {PlatformDataSource} from 'ng-src/app/enums/platform-data-source';
import {FilePreview} from 'ng-src/app/platform/components/common/file-preview/file-preview.component';
import {environment} from 'ng-src/environments/environment';
import {UploadService} from 'ng-src/app/services/upload.service';
import {OrganizationLicenceService} from 'ng-src/app/services/organization-licence.service';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
})
export class ProjectDetailComponent implements OnInit {

  loading$ = new BehaviorSubject<boolean>(true);
  documentsLoading$ = new BehaviorSubject<boolean>(true);
  projectDetail$: Observable<ProjectDetailDto>;
  mode$: Observable<ProjectDetailMode>;
  years$: Observable<number[]>;

  modes = ProjectDetailMode;
  documents$: Observable<ExtendedFilePreview[]>;
  private update$ = new Subject<void>();
  private updateDocuments$ = new Subject<void>();

  constructor(private route: ActivatedRoute,
              private ngbModal: NgbModal,
              private router: Router,
              private pathService: PathService,
              private projectService: EvidenceProjectService,
              private organizationLicenseService: OrganizationLicenceService,
              private uploadService: UploadService) {
  }

  ngOnInit(): void {
    const projectCode$ = combineLatest([this.route.params, this.update$.pipe(startWith(true))]).pipe(
      map(([p]) => p[UrlParams.PROJECT_CODE])
    );

    this.projectDetail$ = projectCode$.pipe(
      tap(() => this.loading$.next(true)),
      switchMap(code => this.projectService.getProjectDetail(code)),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );

    this.years$ = this.projectDetail$.pipe(
      map(project => {
        const set = Object.values(project.organizationFinanceMap).reduce((result, yv) => {
          Object.keys(yv).forEach(y => result.add(parseInt(y, 10)));
          return result;
        }, new Set<number>());
        return [...set];
      }),
      shareReplay()
    );

    this.mode$ = this.route.queryParams.pipe(
      map(params => params.mode),
      filter(m => !!m),
      startWith(ProjectDetailMode.OVERVIEW),
      distinctUntilChanged(),
      shareReplay()
    );


    this.documents$ = combineLatest([this.projectDetail$, this.updateDocuments$.pipe(startWith(true))]).pipe(
      tap(() => this.documentsLoading$.next(true)),
      debounceTime(0),
      switchMap(([p]) => {
        if (p.projectId) {
          this.documentsLoading$.next(true);
          return this.projectService.getProjectDocuments(p.projectId).pipe(
            tapAllHandleError(() => this.documentsLoading$.next(false)),
            map(documents => documents.map(d => {
              return {
                fileName: d.documentName,
                fileSize: d.size,
                fileType: d.documentType,
                downloadUrl: `${environment.restUrl}projects/${p.projectId}/documents/${d.documentId}`,
                documentId: d.documentId,
                deleting$: new BehaviorSubject(false)
              };
            }))
          );
        }
        return of([]);
      }),
      shareReplay()
    );
  }

  back(): void {
    window.history.back();
  }

  openEditProjectModal(project: ProjectDetailDto): void {
    const ngbModalRef = this.ngbModal.open(ProjectDetailEditModalComponent, {size: 'xl'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.project$.next(project);
    ngbModalRef.closed.subscribe(e => {
      if (!project.projectId) {
        const path = this.pathService.getPathWithReplacedParams('projectDetail', PlatformDataSource.STORE + e.projectId);
        this.router.navigate([path], {replaceUrl: true});
      } else {
        this.update$.next();
      }
    });
  }

  uploadDocument(projectId: number): void {
    this.uploadService.openUploadModal((files) => {
      return combineLatest(files.map(f => this.projectService.uploadDocument(projectId, f))).pipe(
        tap(() => this.updateDocuments$.next())
      );
    }, {multiple: true});
  }

  deleteFile(projectId: number, document: ExtendedFilePreview): void {
    document.deleting$.next(true);
    this.projectService.deleteDocument(projectId, document.documentId).pipe(
      finalize(() => document.deleting$.next(false))
    ).subscribe(() => this.updateDocuments$.next());
  }
}

export enum ProjectDetailMode {
  OVERVIEW = 'OVERVIEW',
  OUTCOMES = 'OUTCOMES',
  DOCUMENTS = 'DOCUMENTS'
}

interface ExtendedFilePreview extends FilePreview {
  documentId: number;
  deleting$: BehaviorSubject<boolean>;
}
