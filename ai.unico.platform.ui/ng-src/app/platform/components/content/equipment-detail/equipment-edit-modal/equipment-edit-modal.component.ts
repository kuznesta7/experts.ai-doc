import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject, Observable} from 'rxjs';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {UserExpertBaseDto} from 'ng-src/app/interfaces/user-expert-base-dto';
import {TagService} from 'ng-src/app/services/tag.service';
import {EquipmentPreview} from 'ng-src/app/interfaces/equipment-preview';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormValidationService} from 'ng-src/app/shared/services/form-validation.service';
import {EquipmentIdResponse, EvidenceEquipmentService} from 'ng-src/app/services/evidence-equipment.service';
import {EquipmentLangData, EvidenceEquipment} from 'ng-src/app/interfaces/evidence-equipment';
import {EquipmentCategoryService} from 'ng-src/app/services/equipment-category.service';
import {CategoryDto} from 'ng-src/app/interfaces/category-dto';
import {MultilingualDto} from 'ng-src/app/interfaces/multilingual-dto';

@Component({
  selector: 'app-equipment-edit-modal',
  templateUrl: './equipment-edit-modal.component.html',
})
export class EquipmentEditModalComponent implements OnInit {

  emptyEquipment = {name: '', type: 1, description: '', goodForDescription: '', infrastructureDescription: '', targetGroup: '', preparationTime: '', trl: '', durationTime: '', equipmentCost: ''};
  selectedLanguage = '';
  previousLanguage = '';
  equipmentLanguagesData: EquipmentLangData = {};

  langFormGroup = new FormGroup({
    languageCode: new FormControl(''),
  });
  formGroup = new FormGroup({
    name: new FormControl(''),
    type: new FormControl('', Validators.required),
    description: new FormControl(''),
    goodForDescription: new FormControl(''),
    infrastructureDescription: new FormControl(''),
    targetGroup: new FormControl(''),
    preparationTime: new FormControl(''),
    trl: new FormControl(''),
    durationTime: new FormControl(''),
    equipmentCost: new FormControl(''),
    specialization: new FormControl([])
  });
  organizations$ = new BehaviorSubject<OrganizationBaseDto[]>([]);
  organizationIds: number[] = [];
  experts$ = new BehaviorSubject<UserExpertBaseDto[]>([]);
  experts: UserExpertBaseDto[] = [];

  organizationId: number;
  userId: number;
  saving$ = new BehaviorSubject<boolean>(false);
  equipment$ = new BehaviorSubject<MultilingualDto<EquipmentPreview> | null>(null);
  multilingualEquipment$ = new BehaviorSubject<MultilingualDto<EquipmentPreview> | null>(null);
  equipmentId$ = new BehaviorSubject<number | null>(null);
  organizationId$ = new BehaviorSubject<number | null>(null);
  equipmentTypes$: Observable<CategoryDto[]>;
  equipmentLanguages$: Observable<CategoryDto[]>;

  lookupKeywords = this.tagService.lookupFunction;

  constructor(private tagService: TagService,
              private activeModal: NgbActiveModal,
              private formValidationService: FormValidationService,
              private equipmentService: EvidenceEquipmentService,
              private equipmentCategoryService: EquipmentCategoryService
  ) {
  }

  ngOnInit(): void {
    console.log(this.emptyEquipment);
    try {
      this.equipmentTypes$ = this.equipmentCategoryService.getEquipmentType();
      this.equipmentLanguages$ = this.equipmentCategoryService.getEquipmentLanguage();
      this.equipment$.subscribe((e: MultilingualDto<EquipmentPreview> | null) => {
        if (!e) {
          return;
        }
        this.langFormGroup.patchValue({languageCode: e.preview.languageCode});
        this.equipmentLanguagesData = this.fromArrayToDataLanguageObject(e);
        console.log(this.equipmentLanguagesData);
        this.formGroup.patchValue({...e.preview});
        this.equipmentId$.next(e.preview.id);
        this.experts$.next(e.preview.expertPreviews);
        this.organizations$.next(e.preview.organizations);
        e.preview.expertPreviews.forEach(x => this.experts.push(x));
        e.preview.organizations.forEach(x => this.organizationIds.push(x.organizationId));
        this.selectedLanguage = e.preview.languageCode;
        this.previousLanguage = e.preview.languageCode;
      });
    } catch (e) {
      console.log(e);
    }
    console.log();
  }

  saveChanges(): void {
    this.formValidationService.validateForm(this.formGroup);
    this.saving$.next(true);
    this.equipmentLanguagesData[this.selectedLanguage] = this.formGroup.getRawValue();
    console.log(this.selectedLanguage);
    const multiLangEq = this.multilingualEquipment$.getValue();
    const equipment: EvidenceEquipment = {
      ...multiLangEq,
      expertCodes: this.experts.map(x => x.expertCode),
      organizationIds: this.organizationIds,
      ...this.formGroup.getRawValue(),
      languageCode: this.selectedLanguage,
      type: this.formGroup.value.type,
    };

    const translations = this.fromDataLanguageObjectToArray(equipment, this.equipmentLanguagesData);
    let multilingual: MultilingualDto<EvidenceEquipment> = {
      id: equipment.id,
      code: 'ST' + equipment.id,
      preview: equipment,
      translations
    };

    console.log(multilingual);

    if (multiLangEq == null) {

      multilingual = {
        ...multilingual,
        id: null,
        code: null,
      };
      this.equipmentService.createEquipment(multilingual).subscribe((response: EquipmentIdResponse) => {
        this.activeModal.close();
        this.saving$.next(false);
      });
    } else {
      this.equipmentService.updateEquipment(multilingual).subscribe((response: EquipmentIdResponse) => {
        this.activeModal.close(response.id);
        this.saving$.next(false);
      });
    }
  }

  fromDataLanguageObjectToArray(eq: EvidenceEquipment, equipmentData: EquipmentLangData): EvidenceEquipment[] {
    const translate: EvidenceEquipment[] = [];
    for (const key in equipmentData) {
      if (equipmentData.hasOwnProperty(key)) {
        const element = equipmentData[key];
        translate.push({
          ...eq,
          languageCode: key,
          ...element,
          type: eq.type
        });
      }
    }
    return translate;
  }
  fromArrayToDataLanguageObject(multilingual: MultilingualDto<EquipmentPreview>): EquipmentLangData {
    const eq: EquipmentLangData = {};
    multilingual.translations.forEach(x => {
      eq[x.languageCode] = x;
    });
    console.log('eq', eq);
    return eq;
  }


  dismiss(): void {
    try {
      this.activeModal.dismiss();
    } catch (e) {
      console.log(e);
    }
  }

  organizationChange(event: { organizationId: number, added: boolean }): void {

    try {

      if (event.added) {
        this.organizationIds.push(event.organizationId);
      } else {
        this.organizationIds = this.organizationIds.filter(id => id !== event.organizationId);
      }
    } catch (e) {
      console.log(e);
    }
  }

  expertChange(event: { expert: UserExpertBaseDto, added: boolean }): void {
    try {
      if (event.added) {
        this.experts.push(event.expert);
      } else {
        this.experts = this.experts.filter(x => x.expertCode !== event.expert.expertCode);
      }
    } catch (e) {
      console.log(e);
    }
  }

  onLanguageChange(event: any): void {
    if (this.selectedLanguage === this.previousLanguage) {
      return;
    } else if (this.previousLanguage === '') {
      this.previousLanguage = this.selectedLanguage;
      return;
    } else {
      this.equipmentLanguagesData[this.previousLanguage] = this.formGroup.getRawValue();
      this.emptyEquipment = {
        ...this.emptyEquipment,
        type: this.equipmentLanguagesData[this.previousLanguage].type
      };
      console.log(this.equipmentLanguagesData);
      this.formGroup.patchValue(this.emptyEquipment);
      if (this.equipmentLanguagesData && this.equipmentLanguagesData.hasOwnProperty(this.selectedLanguage)) {
        const selectedLanguageData = this.equipmentLanguagesData[this.selectedLanguage];
        this.formGroup.patchValue(selectedLanguageData);
        this.formGroup.patchValue({type: this.equipmentLanguagesData[this.previousLanguage].type});
      }
      this.previousLanguage = this.selectedLanguage;
      console.log('selected2', this.selectedLanguage);
      console.log('prev2', this.previousLanguage);
    }
  }
}
