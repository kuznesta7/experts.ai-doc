import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, combineLatest, Observable, of, Subject} from 'rxjs';
import {EquipmentPreview} from 'ng-src/app/interfaces/equipment-preview';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {PathService} from 'ng-src/app/shared/services/path.service';
import {map, shareReplay, startWith, switchMap, take, tap} from 'rxjs/operators';
import {UrlParams} from 'ng-src/app/constants/url-params';
import {tapAllHandleError} from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {EvidenceEquipmentService} from 'ng-src/app/services/evidence-equipment.service';
import {
  EquipmentEditModalComponent
} from 'ng-src/app/platform/components/content/equipment-detail/equipment-edit-modal/equipment-edit-modal.component';
import {MultilingualDto} from 'ng-src/app/interfaces/multilingual-dto';

@Component({
  selector: 'app-equipment-detail',
  templateUrl: './equipment-detail.component.html'
})
export class EquipmentDetailComponent implements OnInit {

  loading$ = new BehaviorSubject<boolean>(true);
  multilingualDetail$: Observable<MultilingualDto<EquipmentPreview>>;
  languageCodes: string[] = [];
  selectedLanguageCode: string;
  equipmentDetail$: Observable<EquipmentPreview>;
  private update$ = new Subject<void>();

  constructor(private route: ActivatedRoute,
              private ngbModal: NgbModal,
              private router: Router,
              private pathService: PathService,
              private equipmentService: EvidenceEquipmentService) {
  }

  ngOnInit(): void {
    const equipmentId$ = this.route.params.pipe(
      map(p => p[UrlParams.EQUIPMENT_CODE])
    );

    this.multilingualDetail$ = combineLatest([equipmentId$, this.update$.pipe(startWith(true))]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([code]) => this.equipmentService.getEquipmentDetail(code)),
      // map((equipment: MultilingualDto<EquipmentPreview>) => equipment.preview),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );
    this.multilingualDetail$.subscribe((multilingual) => {
      this.selectedLanguageCode = multilingual.preview.languageCode;
      this.equipmentDetail$ = new BehaviorSubject(multilingual.preview);
      this.languageCodes = multilingual.translations.map(t => t.languageCode);
    });
    // this.equipmentDetail$.subscribe((x) => console.log(x));
  }

  onLanguageChange() {
    this.multilingualDetail$.pipe(
      take(1),
      map(multilingual => {
        const selectedTranslation = multilingual.translations.find(translation => translation.languageCode === this.selectedLanguageCode);
        return {
          ...multilingual,
          preview: selectedTranslation || multilingual.preview
        };
      })
    ).subscribe(newMultilingual => {
      this.multilingualDetail$ = of(newMultilingual);
    });
  }

  back(): void {
    window.history.back();
  }

  openEditModal(multilingual: MultilingualDto<EquipmentPreview>): void {
    const ngbModalRef: NgbModalRef = this.ngbModal.open(EquipmentEditModalComponent, {size: 'xl'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.multilingualEquipment$.next(multilingual);
    componentInstance.equipment$.next(multilingual);
    ngbModalRef.closed.subscribe(() => {
      this.update$.next();
    });
  }
}
