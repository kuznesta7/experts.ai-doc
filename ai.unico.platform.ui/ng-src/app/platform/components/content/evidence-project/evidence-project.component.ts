import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, combineLatest, Observable, of} from 'rxjs';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {ProjectWrapper} from 'ng-src/app/interfaces/project-wrapper';
import {EvidenceProjectService} from 'ng-src/app/services/evidence-project.service';
import {ActivatedRoute} from '@angular/router';
import {debounceTime, finalize, map, mergeMap, shareReplay, startWith, switchMap} from 'rxjs/operators';
import {EvidenceFilter} from 'ng-src/app/interfaces/evidence-filter';
import {FormControl, FormGroup} from '@angular/forms';
import {tapAllHandleError} from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AddProjectModalComponent} from 'ng-src/app/platform/components/common/add-project-modal/add-project-modal.component';
import {ProjectOrganizationService} from 'ng-src/app/services/project-organization.service';
import {ProjectPreviewDto} from 'ng-src/app/interfaces/project-preview-dto';
import {ConfirmationService} from 'ng-src/app/shared/services/confirmation.service';
import {OrganizationAutocompleteService} from 'ng-src/app/services/organization-autocomplete.service';
import {ExpertAutocompleteService} from 'ng-src/app/services/expert-autocomplete.service';
import {QueryParamService} from '../../../../shared/services/query-param.service';
import {cmp} from '../../../../shared/utils/compare-util';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-evidence-project',
  templateUrl: './evidence-project.component.html'
})
export class EvidenceProjectComponent implements OnInit {
  filterFormGroup = new FormGroup({
    query: new FormControl(''),
    participatingOrganizationIds: new FormControl([]),
    expertCodes: new FormControl([]),
    yearFrom: new FormControl([]),
    yearTo: new FormControl([]),
  });

  selectedYear: number;
  years: number[] = [];

  organizationId$: Observable<number>;
  organizationId: number;
  projectWrapper$: Observable<ProjectWrapper>;
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 15});
  loading$ = new BehaviorSubject(true);
  projects$: Observable<ExtendedProjectPreview[]>;

  loadOrganizations = (ids: number[]) => {
    return this.organizationService.getOrganizationsForAutocompleteStandardized(ids);
  }
  autocompleteOrganizations = (query: string) => {
    return this.organizationService.autocompleteOrganizationProjectStandardized(query, this.organizationId);
  }
  loadExperts = (expertCodes: string[]) => {
    return this.expertAutocompleteService.findUserExpertOptions({expertCodes});
  }
  autocompleteExperts = (query: string) => {
    return this.expertAutocompleteService.findUserExpertOptions({query});
  }


  constructor(private evidenceProjectService: EvidenceProjectService,
              private route: ActivatedRoute,
              private ngbModal: NgbModal,
              private expertAutocompleteService: ExpertAutocompleteService,
              private organizationService: OrganizationAutocompleteService,
              private projectOrganizationService: ProjectOrganizationService,
              private confirmationService: ConfirmationService,
              private queryParamService: QueryParamService,
              private titleService: Title) {

    this.titleService.setTitle('Projects');

    this.queryParamService.handleFilterFormGroup(this.filterFormGroup);

    this.selectedYear = new Date().getFullYear();
    for (let year = this.selectedYear; year >= 1970; year--) {
      this.years.push(year);
    }

    this.organizationId$ = this.route.params.pipe(
      map(p => parseInt(p.organizationId, 10))
    );

    const filter$ = this.filterFormGroup.valueChanges.pipe(
      debounceTime(300),
      startWith(this.filterFormGroup.value as EvidenceFilter)
    );

    this.projectWrapper$ = combineLatest([this.organizationId$, filter$, this.pageInfo$]).pipe(
      debounceTime(10),
      switchMap(([id, filter, pageInfo]) => {
        this.loading$.next(true);
        this.organizationId = id;
        return this.evidenceProjectService.getEvidenceProjects(id, filter, pageInfo).pipe(
          tapAllHandleError(() => this.loading$.next(false))
        );
      }),
      shareReplay()
    );

    this.projects$ = this.projectWrapper$.pipe(map(w => w.projectPreviewDtos.map(p => {
      return {
        ...p,
        saving$: new BehaviorSubject<boolean>(false),
        verified$: new BehaviorSubject<boolean>(p.verified),
        deleted$: new BehaviorSubject<boolean>(false)
      };
    })), shareReplay());

    // this.filterFormGroup.setControl('yearFrom', new FormControl());
    // this.filterFormGroup.setControl('yearTo', new FormControl());
  }

  ngOnInit(): void {
  }

  verifyProject(project: ExtendedProjectPreview, organizationId: number, verify: boolean): void {
    project.saving$.next(true);
    this.getProjectId(project).pipe(
      mergeMap(id => this.projectOrganizationService.verifyProjectsForOrganization([id], organizationId, verify)),
      finalize(() => project.saving$.next(false))
    ).subscribe(() => {
      project.verified = verify;
      project.verified$.next(verify);
    });
  }

  addProject(organizationId: number): void {
    const ngbModalRef = this.ngbModal.open(AddProjectModalComponent, {size: 'xl', backdrop: 'static'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.organizationId = organizationId;
    componentInstance.doAddProject = (p: ProjectPreviewDto) => {
      return this.getProjectId(p).pipe(switchMap(projectId => this.projectOrganizationService.addOrganization(projectId, organizationId, true)));
    };
    ngbModalRef.closed.subscribe(() => this.pageInfo$.next(this.pageInfo$.value));
  }

  private getProjectId(project: ProjectPreviewDto): Observable<number> {
    if (project.projectId) {
      return of(project.projectId);
    }
    return this.evidenceProjectService.moveProject(project.originalProjectId);
  }

  removeProject(project: ExtendedProjectPreview, organizationId: number): void {
    this.confirmationService.confirm({
      title: 'Remove outcome from your organization',
      content: 'Are you sure you want to remove project <i>"' + project.name + '"</i> from your current organization?',
      confirmClass: 'danger',
      confirmLabel: 'Remove'
    }).subscribe(() => {
      project.saving$.next(true);
      this.getProjectId(project).pipe(mergeMap(id => this.projectOrganizationService.removeOrganization(id, organizationId))).pipe(
        finalize(() => project.saving$.next(false))
      ).subscribe(() => project.deleted$.next(true));
    });

  }

  compare(o1: any, o2: any): boolean {
    return cmp(o1, o2);
  }
}

interface ExtendedProjectPreview extends ProjectPreviewDto {
  saving$: BehaviorSubject<boolean>;
  verified$: BehaviorSubject<boolean>;
  deleted$: BehaviorSubject<boolean>;
}
