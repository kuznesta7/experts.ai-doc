import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subject, Subscription } from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { UserWorkspacePreview, UserWorkspaceService } from 'ng-src/app/services/user-workspace.service';
import {
  ExpertFollowService,
  FollowedProfilePreviewDto,
  FollowedProfilesWrapper
} from 'ng-src/app/services/expert-follow.service';
import { FormControl, Validators } from '@angular/forms';
import { SessionService } from 'ng-src/app/services/session.service';
import { filter, map, shareReplay, startWith, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';

@Component({
  selector: 'app-saved-experts',
  templateUrl: './saved-experts.component.html'
})
export class SavedExpertsComponent implements OnInit {

  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 30});
  loading$ = new BehaviorSubject<boolean>(true);
  followedProfilesWrapper$: Observable<FollowedProfilesWrapper>;
  followedProfiles$: Observable<ExtendedFollowedProfilePreviewDto[]>;
  workspaces$: Observable<UserWorkspacePreview[]>;
  workspacesToFilter$ = new BehaviorSubject<string[]>([]);
  reloadWorkspaces$ = new Subject();

  constructor(private expertFollowService: ExpertFollowService,
              private userWorkspaceService: UserWorkspaceService,
              private sessionService: SessionService) {
  }

  ngOnInit(): void {
    this.followedProfilesWrapper$ = combineLatest([this.sessionService.getCurrentSession(), this.pageInfo$, this.workspacesToFilter$]).pipe(
      tap(() => this.loading$.next(true)),
      filter(([s]) => !!s),
      switchMap(([session, pi, workspaces]) => this.expertFollowService.findFollowedExperts(session.userId, pi, workspaces).pipe(
        tapAllHandleError(() => this.loading$.next(false))
      )),
      shareReplay()
    );

    this.followedProfiles$ = this.followedProfilesWrapper$.pipe(map(wrapper => wrapper.followedProfilePreviewDtos.map(row => {
      return {
        row,
        saving$: new BehaviorSubject<boolean>(false),
        deleted$: new BehaviorSubject<boolean>(false),
        workspaces$: new BehaviorSubject<string[]>(row.workspaces),
        newWorkspaceFc: new FormControl('', Validators.required)
      };
    })));

    this.workspaces$ = combineLatest([this.sessionService.getCurrentSession(), this.reloadWorkspaces$.pipe(startWith(true))]).pipe(
      switchMap(([s]) => this.userWorkspaceService.findUserWorkspaces(s.userId)),
      shareReplay()
    );
  }

  toggleWorkspace(workspace: UserWorkspacePreview): void {
    const currentWorkspaces = this.workspacesToFilter$.value;
    const workspaceName = workspace.workspaceName;
    if (currentWorkspaces.indexOf(workspaceName) === -1) {
      this.workspacesToFilter$.next(currentWorkspaces.concat(workspaceName));
    } else {
      this.workspacesToFilter$.next(currentWorkspaces.filter(w => w !== workspaceName));
    }
  }

  toggleRowWorkspace(rowWrapper: ExtendedFollowedProfilePreviewDto, workspaceName: string): Subscription {
    const currentSession$ = this.sessionService.getCurrentSession();
    let workspaces = rowWrapper.workspaces$.value;
    if (workspaces.indexOf(workspaceName) !== -1) {
      workspaces = workspaces.filter(w => w !== workspaceName);
    } else {
      workspaces = workspaces.concat(workspaceName);
    }
    rowWrapper.saving$.next(true);
    const row = rowWrapper.row;
    const definition = {followedUserId: row.userId, followedExpertId: row.expertId, workspaces};
    return currentSession$.pipe(
      switchMap(s => this.expertFollowService.followProfile(s.userId, definition).pipe(
        tapAllHandleError(() => rowWrapper.saving$.next(false))
      ))
    ).subscribe(() => rowWrapper.workspaces$.next(workspaces));
  }

  addNewWorkspace(rowWrapper: ExtendedFollowedProfilePreviewDto): void {
    rowWrapper.newWorkspaceFc.markAsTouched();
    if (rowWrapper.newWorkspaceFc.invalid) {
      return;
    }
    this.toggleRowWorkspace(rowWrapper, rowWrapper.newWorkspaceFc.value).add(() => {
      this.reloadWorkspaces$.next();
    });
  }

  removeFollowing(rowWrapper: ExtendedFollowedProfilePreviewDto): void {
    const currentSession$ = this.sessionService.getCurrentSession();
    rowWrapper.saving$.next(true);
    const row = rowWrapper.row;
    const definition = {followedUserId: row.userId, followedExpertId: row.expertId};
    currentSession$.pipe(
      switchMap(s => this.expertFollowService.unfollowProfile(s.userId, definition).pipe(
        tapAllHandleError(() => rowWrapper.saving$.next(false)))
      )
    ).subscribe(() => rowWrapper.deleted$.next(true));
  }
}

interface ExtendedFollowedProfilePreviewDto {
  row: FollowedProfilePreviewDto;
  saving$: BehaviorSubject<boolean>;
  deleted$: BehaviorSubject<boolean>;
  workspaces$: BehaviorSubject<string[]>;
  newWorkspaceFc: FormControl;
}
