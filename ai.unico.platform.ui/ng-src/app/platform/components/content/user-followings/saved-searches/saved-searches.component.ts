import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subject, Subscription } from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import {
  SearchHistoryDto,
  SearchHistoryService,
  SearchHistoryWrapper
} from 'ng-src/app/services/search-history.service';
import { SessionService } from 'ng-src/app/services/session.service';
import { filter, finalize, map, shareReplay, startWith, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { FormControl, Validators } from '@angular/forms';
import { UserWorkspacePreview, UserWorkspaceService } from 'ng-src/app/services/user-workspace.service';

@Component({
  selector: 'app-saved-searches',
  templateUrl: './saved-searches.component.html'
})
export class SavedSearchesComponent implements OnInit {

  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 30});
  loading$ = new BehaviorSubject<boolean>(true);
  searchHistoryWrapper$: Observable<SearchHistoryWrapper>;
  searchHistoryDtos$: Observable<ExtendedSearchHistory[]>;
  workspaces$: Observable<UserWorkspacePreview[]>;
  workspacesToFilter$ = new BehaviorSubject<string[]>([]);
  reloadWorkspaces$ = new Subject();

  constructor(private sessionService: SessionService,
              private userWorkspaceService: UserWorkspaceService,
              private searchHistoryService: SearchHistoryService) {
  }

  ngOnInit(): void {
    this.searchHistoryWrapper$ = combineLatest([this.sessionService.getCurrentSession(), this.pageInfo$, this.workspacesToFilter$]).pipe(
      tap(() => this.loading$.next(true)),
      filter(([s]) => !!s),
      switchMap(([session, pi, workspaces]) => this.searchHistoryService.findSearchHistory(session.userId, {
        workspaces,
        favorite: true
      }, pi).pipe(
        tapAllHandleError(() => this.loading$.next(false))
      )),
      shareReplay()
    );

    this.searchHistoryDtos$ = this.searchHistoryWrapper$.pipe(
      map(w => w.historyDtos.map(row => {
        return {
          row,
          saving$: new BehaviorSubject<boolean>(false),
          deleted$: new BehaviorSubject<boolean>(false),
          workspaces$: new BehaviorSubject<string[]>(row.workspaces),
          newWorkspaceFc: new FormControl('', Validators.required)
        };
      }))
    );

    this.workspaces$ = combineLatest([this.sessionService.getCurrentSession(), this.reloadWorkspaces$.pipe(startWith(true))]).pipe(
      switchMap(([s]) => this.userWorkspaceService.findUserWorkspaces(s.userId)),
      shareReplay()
    );
  }

  updateRow(rowWrapper: ExtendedSearchHistory, favorite: boolean, workspaces: string[]): Subscription {
    rowWrapper.saving$.next(true);
    return this.searchHistoryService.updateSearchHistory(rowWrapper.row.searchHistId, favorite, workspaces).pipe(
      finalize(() => rowWrapper.saving$.next(false))
    ).subscribe(() => {
      if (!favorite) {
        rowWrapper.deleted$.next(true);
      }
      rowWrapper.workspaces$.next(workspaces);
    });
  }

  toggleWorkspace(workspace: UserWorkspacePreview): void {
    const currentWorkspaces = this.workspacesToFilter$.value;
    const workspaceName = workspace.workspaceName;
    if (currentWorkspaces.indexOf(workspaceName) === -1) {
      this.workspacesToFilter$.next(currentWorkspaces.concat(workspaceName));
    } else {
      this.workspacesToFilter$.next(currentWorkspaces.filter(w => w !== workspaceName));
    }
  }

  toggleRowWorkspace(rowWrapper: ExtendedSearchHistory, workspaceName: string): Subscription {
    const currentWorkspaces = rowWrapper.workspaces$.value;
    let newWorkspaces;
    if (currentWorkspaces.indexOf(workspaceName) === -1) {
      newWorkspaces = rowWrapper.workspaces$.value.concat(workspaceName);
    } else {
      newWorkspaces = currentWorkspaces.filter(w => w !== workspaceName);
    }
    return this.updateRow(rowWrapper, true, newWorkspaces);
  }

  addNewWorkspace(rowWrapper: ExtendedSearchHistory): void {
    rowWrapper.newWorkspaceFc.markAsTouched();
    if (!rowWrapper.newWorkspaceFc.valid) {
      return;
    }
    this.toggleRowWorkspace(rowWrapper, rowWrapper.newWorkspaceFc.value).add(() => {
      this.reloadWorkspaces$.next();
      rowWrapper.newWorkspaceFc.reset();
    });
  }
}

interface ExtendedSearchHistory {
  row: SearchHistoryDto;
  saving$: BehaviorSubject<boolean>;
  deleted$: BehaviorSubject<boolean>;
  workspaces$: BehaviorSubject<string[]>;
  newWorkspaceFc: FormControl;
}
