import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { filter, map, shareReplay, startWith } from 'rxjs/operators';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-user-followings',
  templateUrl: './user-followings.component.html'
})
export class UserFollowingsComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private titleService: Title) {
    this.titleService.setTitle('Followings');
  }

  mode$: Observable<UserFollowingsMode>;
  modes = UserFollowingsMode;

  ngOnInit(): void {
    this.mode$ = this.route.queryParams.pipe(
      filter(q => !!q.mode),
      map(q => q.mode),
      startWith(UserFollowingsMode.SEARCHES),
      shareReplay()
    );
  }

}

enum UserFollowingsMode {
  SEARCHES = 'SEARCHES',
  EXPERTS = 'EXPERTS'
}
