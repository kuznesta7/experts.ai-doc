import {Component, OnInit} from '@angular/core';
import {
  OrganizationAutocompleteService,
  OrganizationForAutocomplete
} from 'ng-src/app/services/organization-autocomplete.service';
import {ExpertAutocompleteService, UserExpertPreviewDto} from 'ng-src/app/services/expert-autocomplete.service';
import {EvidenceItemService} from 'ng-src/app/services/evidence-item.service';
import {ItemExpertService} from 'ng-src/app/services/item-expert.service';
import {ItemOrganizationService} from 'ng-src/app/services/item-organization.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {TagService} from 'ng-src/app/services/tag.service';
import {FormValidationService} from 'ng-src/app/shared/services/form-validation.service';
import {SecurityService} from 'ng-src/app/services/security.service';
import {ItemTypeService} from 'ng-src/app/services/item-type.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Confidentiality} from 'ng-src/app/interfaces/confidetiality';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {UserExpertBaseDto} from 'ng-src/app/interfaces/user-expert-base-dto';
import {debounceTime, switchMap, take} from 'rxjs/operators';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {OpportunityPreview} from 'ng-src/app/interfaces/opportunity-preview';
import {OpportunityCategoryService} from 'ng-src/app/services/opportunity-category.service';
import {CategoryDto} from 'ng-src/app/interfaces/category-dto';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {EvidenceOpportunityService} from 'ng-src/app/services/evidence-opportunity.service';

@Component({
  selector: 'app-opportunity-edit-modal',
  templateUrl: './opportunity-edit-modal.component.html'
})
export class OpportunityEditModalComponent implements OnInit {

  formGroup = new FormGroup({
    opportunityName: new FormControl('', Validators.required),
    opportunityDescription: new FormControl('', Validators.required),
    opportunityType: new FormControl('', Validators.required),
    opportunitySignupDate: new FormControl(null),
    opportunityLocation: new FormControl(''),
    opportunityKw: new FormControl([]),
    opportunityWage: new FormControl(''),
    opportunityTechReq: new FormControl(''),
    opportunityFormReq: new FormControl(''),
    opportunityOtherReq: new FormControl(''),
    opportunityBenefit: new FormControl(''),
    opportunityJobStartDate: new FormControl(null),
    opportunityExtLink: new FormControl(''),
    opportunityHomeOffice: new FormControl(''),
    jobTypes: new FormControl([])
  });
  searchExpertFc = new FormControl('');
  searchOrganizationFc = new FormControl('');
  organizations$ = new BehaviorSubject<OrganizationBaseDto[]>([]);
  experts$ = new BehaviorSubject<UserExpertBaseDto[]>([]);
  organizationsModalList$ = new BehaviorSubject<OrganizationBaseDto[]>([]);
  organizationId: number;
  userId: number;
  saving$ = new BehaviorSubject<boolean>(false);
  opportunity$ = new BehaviorSubject<OpportunityPreview | null>(null);
  organizationId$ = new BehaviorSubject<number | null>(null);
  outcomeId$ = new BehaviorSubject<string | null>(null);
  confidentiality = Confidentiality;
  opportunitiesUpdated = false;
  opportunityTypes$ = this.categoryService.getOpportunityType();
  jobTypes$ = this.categoryService.getJobType();
  jobs: boolean[];
  isTopMemberOrganization$ = new BehaviorSubject<boolean>(false);
  topMemberOrganizationId$ = new BehaviorSubject<number | null>(null);
  expertOptions$ = combineLatest([this.searchExpertFc.valueChanges, this.experts$, this.organizationId$, this.organizationsModalList$]).pipe(
    debounceTime(300),
    switchMap(([q, experts, id, organizations]) => {
      const options = {query: q, disallowedExpertCodes: experts.map(o => o.expertCode)};
      if (organizations.length > 0) {
          return this.expertAutocompleteService.findUserExperts({...options, organizationIds: organizations.map(x => x.organizationId)});
      }
      if (id == null){
        return this.expertAutocompleteService.findUserExperts(options);
      }
      return this.expertAutocompleteService.findUserExperts({...options, organizationIds: [id]});
    })
  );
  lookupKeywords = this.tagService.lookupFunction;


  organizationOptions$ = combineLatest([this.searchOrganizationFc.valueChanges, this.topMemberOrganizationId$]).pipe(
    debounceTime(300),
    switchMap(([q, id]) => {
      if (id != null)
        return this.organizationAutocompleteService.autocompleteOrganizationOpportunity(q, id);
      return new Observable<OrganizationForAutocomplete[]>();
    })
  );

  constructor(private organizationAutocompleteService: OrganizationAutocompleteService,
              private expertAutocompleteService: ExpertAutocompleteService,
              private itemService: EvidenceItemService,
              private itemExpertService: ItemExpertService,
              private itemOrganizationService: ItemOrganizationService,
              private activeModal: NgbActiveModal,
              private tagService: TagService,
              private formValidationService: FormValidationService,
              private securityService: SecurityService,
              private itemTypeService: ItemTypeService,
              private categoryService: OpportunityCategoryService,
              private opportunityService: EvidenceOpportunityService) {
    this.jobTypes$.pipe(take(1)).subscribe(x => this.jobs = Array(x.length).fill(false));
  }

  ngOnInit(): void {

    this.opportunity$.subscribe(o => {
      if (!o) {
        return;
      }
      this.formGroup.patchValue({...o, opportunityJobStartDate: new Date(o.opportunityJobStartDate), opportunitySignupDate: new Date(o.opportunitySignupDate)});
      this.experts$.next(o.expertPreviews);
      this.organizations$.next(o.organizationBaseDtos);
      this.outcomeId$.next(o.opportunityId);
      this.organizationsModalList$.next(o.organizationBaseDtos)
      o.jobTypes.forEach(x => this.jobs[x] = true);
    });

    this.organizationId$.subscribe(id => {
      if (!id) {
        return;
      }
      this.organizationId = id;
      this.organizations$.next([{organizationId: id, organizationAbbrev: '', organizationName: ''}]);
    });

  }

  checkChange(event: MatCheckboxChange, type: CategoryDto): void {
    const tps = this.formGroup.value.jobTypes;
    if (event.checked) {
      tps.splice(tps.length, 0, type.id);
    } else {
      tps.splice(tps.indexOf(type.id), 1);
    }
    this.formGroup.patchValue({jobTypes: tps});
  }

  removeOrganization(organization: OrganizationBaseDto): void {
    this.organizationsModalList$.next([]);
  }

  removeExpert(expert: UserExpertBaseDto): void {
    this.experts$.next([]);
  }

  addOrganization(event: MatAutocompleteSelectedEvent): void {
    const organization: OrganizationBaseDto = event.option.value;
    this.organizationsModalList$.next(this.organizationsModalList$.value.concat(organization));
    this.searchOrganizationFc.reset();
  }

  addExpert(event: MatAutocompleteSelectedEvent): void {
    const expert: UserExpertPreviewDto = event.option.value;
    const expertBase: UserExpertBaseDto = {
      expertCode: expert.expertCode,
      userId: expert.userId,
      expertId: expert.expertId,
      name: expert.name,
      organizationIds: expert.organizationDtos.map(o => o.organizationId)
    };
    this.experts$.next(this.experts$.value.concat(expertBase));
    console.log('exps', this.experts$.value);
    this.searchExpertFc.reset();
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }

  ngOnDestroy(): void {
    if (this.opportunitiesUpdated) {
      window.location.reload();
    }
  }

  saveChanges(): void {
    this.formValidationService.validateForm(this.formGroup);
    const outcome = {
      ...this.opportunity$.value,
      expertIds: this.experts$.value.map(x => x.expertCode),
      organizationIds: this.organizations$.value.map(x => x.organizationId),
      memberOrganizationIds: this.organizationsModalList$.value.map(x => x.organizationId),
      ...this.formGroup.getRawValue()
    };
    // this.saving$.next(true);
    let req: Observable<any>;
    if (this.opportunity$.value == null) {
      req = this.opportunityService.createOpportunity(outcome);
    } else {
      req = this.opportunityService.updateOpportunity(this.opportunity$.value?.opportunityId, outcome);
    }
    this.opportunitiesUpdated = true;
    req.subscribe(() => this.activeModal.close(outcome));
  }
}

interface CheckedCategory extends CategoryDto {
  checked: boolean;
}
