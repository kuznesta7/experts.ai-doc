import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { OpportunityPreview } from 'ng-src/app/interfaces/opportunity-preview';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { map, shareReplay, startWith, switchMap, tap } from 'rxjs/operators';
import { WidgetService } from 'ng-src/app/services/widget.service';
import { UrlParams } from 'ng-src/app/constants/url-params';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { ContactModalComponent } from 'ng-src/app/platform/components/common/contact-modal/contact-modal.component';
import { EvidenceOpportunityService } from 'ng-src/app/services/evidence-opportunity.service';
import { OpportunityEditModalComponent } from 'ng-src/app/platform/components/content/opportunity-detail/opportunity-edit-modal/opportunity-edit-modal.component';

@Component({
  selector: 'app-opportunity-detail',
  templateUrl: './opportunity-detail.component.html'
})
export class OpportunityDetailComponent implements OnInit {

  loading$ = new BehaviorSubject<boolean>(true);
  update$ = new Subject<void>();
  opportunityDetail$: Observable<OpportunityPreview>;
  pathKey = 'widgetOpportunityDetail';
  organizationId: number | undefined;

  constructor(private route: ActivatedRoute,
              private ngbModal: NgbModal,
              private widgetService: WidgetService,
              private opportunityService: EvidenceOpportunityService) {
  }

  ngOnInit(): void {
    const opportunityId$ = this.route.params.pipe(
      map(p => p[UrlParams.OPPORTUNITY_ID])
    );
    this.route.queryParams.subscribe(params => {
      this.organizationId = params.orgId;
    });
    this.opportunityDetail$ = combineLatest([opportunityId$, this.update$.pipe(startWith(true))]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([code]) => this.opportunityService.getOpportunity(code)),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );
  }

  back(): void {
    window.history.back();
  }

  openContactModal(): void {
    const ngbModalRef = this.ngbModal.open(ContactModalComponent, {size: 'lg'});
    this.opportunityDetail$.subscribe(opDetails => {
      let orgId = this.organizationId;
      if (opDetails.organizationBaseDtos.length === 1) {
        orgId = opDetails.organizationBaseDtos[0].organizationId;
      }
      ngbModalRef.componentInstance.opportunityPreview = opDetails;
      ngbModalRef.componentInstance.organizationId = orgId;
      ngbModalRef.componentInstance.contactType = this.pathKey;
    });
  }

  openEditOpportunityModal(opportunity: OpportunityPreview): void {
    const ngbModalRef = this.ngbModal.open(OpportunityEditModalComponent, {size: 'xl'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.opportunity$.next(opportunity);
    ngbModalRef.closed.subscribe(e => {
      this.update$.next();
    });
  }


}
