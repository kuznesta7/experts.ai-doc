import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, combineLatest, iif, Observable, Subject} from 'rxjs';
import {debounceTime, map, shareReplay, startWith, switchMap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {OpportunityWrapper} from 'ng-src/app/interfaces/opportunity-wrapper';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {QueryParamService} from 'ng-src/app/shared/services/query-param.service';
import {OpportunityCategoryService} from 'ng-src/app/services/opportunity-category.service';
import {OrganizationAutocompleteService} from 'ng-src/app/services/organization-autocomplete.service';
import {FormControl, FormGroup} from '@angular/forms';
import {EvidenceOpportunityService} from 'ng-src/app/services/evidence-opportunity.service';
import {tapAllHandleError} from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {OpportunityPreview} from 'ng-src/app/interfaces/opportunity-preview';
import {OpportunityEditModalComponent} from 'ng-src/app/platform/components/content/opportunity-detail/opportunity-edit-modal/opportunity-edit-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OpportunityFilter} from 'ng-src/app/interfaces/opportunity-filter';
import {cmp} from '../../../../shared/utils/compare-util';
import {OrganizationStructureService} from '../../../../services/organization-structure.service';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-evidence-opportunity',
  templateUrl: './evidence-opportunity.component.html'
})
export class EvidenceOpportunityComponent implements OnInit {

  organizationId$: Observable<number>;

  filterFormGroup = new FormGroup({
    query: new FormControl(''),
    jobType: new FormControl([]),
    opportunityType: new FormControl([]),
    includeHidden: new FormControl(false)
  });

  opportunityWrapper$: Observable<OpportunityWrapper>;
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 10});
  jobTypes$ = this.opportunityCategoryService.getJobType();
  opportunityTypes$ = this.opportunityCategoryService.getOpportunityType();
  loading$ = new BehaviorSubject(false);
  update$ = new Subject<void>();


  constructor(private route: ActivatedRoute,
              private queryParamService: QueryParamService,
              private opportunityCategoryService: OpportunityCategoryService,
              private organizationService: OrganizationAutocompleteService,
              private evidenceOpportunityService: EvidenceOpportunityService,
              private organizationStructureService: OrganizationStructureService,
              private ngbModal: NgbModal,
              private titleService: Title) {
    this.titleService.setTitle('Opportunities');

    this.organizationId$ = this.route.params.pipe(
      map(p => parseInt(p.organizationId, 10))
    );

    this.queryParamService.handleFilterFormGroup(this.filterFormGroup);

    const filter$ = this.filterFormGroup.valueChanges.pipe(
      debounceTime(400),
      startWith(this.filterFormGroup.value as OpportunityFilter)
    );
    this.opportunityWrapper$ = combineLatest([this.organizationId$, filter$, this.pageInfo$, this.update$]).pipe(
      debounceTime(0),
      switchMap(([organizationId, filter, pageInfo]) => {
        this.loading$.next(true);
        return this.evidenceOpportunityService.findOpportunities(organizationId, filter, pageInfo);
      }),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );

    this.opportunityWrapper$.subscribe((opportunityWrapper: OpportunityWrapper) => {
      console.log('loaded');
    });

    this.update$.next();
  }

  acceptOpportunityType(opportunityId: number): void {
    this.filterFormGroup.patchValue({opportunityType: opportunityId});
  }

  acceptJobType(jobId: number): void {
    this.filterFormGroup.patchValue({jobType: jobId});
  }

  acceptQuery(query: string): void {
    this.filterFormGroup.patchValue({query});
  }

  acceptOrganization(id: number): void {
    this.filterFormGroup.patchValue({organizationId: [id]});
  }

  ngOnInit(): void {
  }

  toggleHidden(opportunity: OpportunityPreview): void {
    this.loading$.next(true);
    iif(() => opportunity.hidden,
      this.evidenceOpportunityService.showOpportunity(opportunity.opportunityId),
      this.evidenceOpportunityService.hideOpportunity(opportunity.opportunityId))
      .subscribe(e => {
        setTimeout(() => this.update$.next(), 1000);
      });
  }

  deleteOpportunity(opportunity: OpportunityPreview): void {
    this.loading$.next(true);
    this.evidenceOpportunityService.deleteOpportunity(opportunity.opportunityId).subscribe();
    setTimeout(() => this.update$.next(), 1000);
  }

  openEditOpportunityModal(opportunity: OpportunityPreview | null): void {
    const ngbModalRef = this.ngbModal.open(OpportunityEditModalComponent, {size: 'xl'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.opportunity$.next(opportunity);
    this.organizationId$.subscribe((id: number) => {
      if (opportunity === null) {
        componentInstance.organizationId$.next(id);
      }
      this.organizationStructureService.getMemberIds(id).subscribe((ids: number[]) => {
        componentInstance.isTopMemberOrganization$.next(ids.length > 0);
        componentInstance.topMemberOrganizationId$.next(id);
      });
    });
    ngbModalRef.closed.subscribe(e => {
      setTimeout(() => this.update$.next(), 1000);
    });
  }

  compare(o1: any, o2: any): boolean {
    return cmp(o1, o2);
  }

}
