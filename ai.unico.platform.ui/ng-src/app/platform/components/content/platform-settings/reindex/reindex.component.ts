import { Component, OnInit } from '@angular/core';
import { IndexStatusDetail, IndexTarget, ReindexService } from 'ng-src/app/services/reindex.service';
import { FlashService } from 'ng-src/app/shared/services/flash.service';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { startWith, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-reindex',
  templateUrl: './reindex.component.html'
})
export class ReindexComponent implements OnInit {

  constructor(private reindexService: ReindexService,
              private flashService: FlashService,
              private titleService: Title) {
    this.titleService.setTitle('Index administration');
  }

  reload$ = new Subject<void>();
  loading$ = new BehaviorSubject(true);
  data$: Observable<IndexStatusDetail[]> = this.reload$.pipe(
    startWith(true),
    tap(() => this.loading$.next(true)),
    switchMap(() => this.reindexService.getStatuses().pipe(
      tapAllHandleError(() => this.loading$.next(false))
    )));

  statusTranslations = {
    STARTED: 'Started',
    FAILED: 'Failed',
    FINISHED: 'Finished',
  };

  statusClasses = {
    STARTED: 'text-warn',
    FAILED: 'text-danger',
    FINISHED: 'text-success',
  };

  targetTranslations = {
    COMMERCIALIZATION_PROJECTS: 'Commercialization projects',
    EXPERT_EVALUATION: 'Expert evaluation',
    EXPERTS: 'Experts',
    ITEM_ASSOCIATION: 'Item association',
    ITEMS: 'Items',
    ONTOLOGY: 'Ontology',
    ORGANIZATIONS: 'Organizations',
    PROJECTS: 'Projects',
    THESIS: 'Thesis',
    LECTURES: 'Lectures',
    EQUIPMENT: 'Equipment',
    OPPORTUNITY: 'Opportunity'
  };

  ngOnInit(): void {
  }

  reindexAll(clean: boolean): void {
    this.flashService.ok('Reindex has started. The complete indexing can take up to 2 hours.');
    const targets: IndexTarget[] = ['ORGANIZATIONS', 'ITEMS', 'PROJECTS', 'COMMERCIALIZATION_PROJECTS', 'EXPERTS', 'EXPERT_EVALUATION', 'ITEM_ASSOCIATION', 'ONTOLOGY'];
    this.reindexService.reindex(clean, targets).subscribe(() => this.reload$.next());
  }

  reindex(row: IndexStatusDetail, clean: boolean): void {
    row.status = 'STARTED';
    this.reindexService.reindex(clean, [row.target]).subscribe(() => this.reload$.next());
  }
}
