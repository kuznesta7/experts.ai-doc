import { Component, OnInit } from '@angular/core';
import { AdminService } from 'ng-src/app/services/admin.service';
import { Observable } from 'rxjs';
import { VersionDto } from 'ng-src/app/interfaces/version-dto';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-platform-settings',
  templateUrl: './platform-settings.component.html'
})
export class PlatformSettingsComponent implements OnInit {

  constructor(private adminService: AdminService,
              private titleService: Title) {
    this.titleService.setTitle('Platform settings');
  }

  version$: Observable<VersionDto>;

  ngOnInit(): void {
    this.version$ = this.adminService.getVersions();
  }

}
