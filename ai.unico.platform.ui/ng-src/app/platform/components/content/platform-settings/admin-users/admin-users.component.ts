import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { debounceTime, startWith, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { AdminUserDto, AdminUsersService, AdminUsersWrapper } from 'ng-src/app/services/admin-users.service';
import { SearchHistoryService } from 'ng-src/app/services/search-history.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SearchHistoryModalComponent } from 'ng-src/app/platform/components/content/platform-settings/admin-users/search-history-modal/search-history-modal.component';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html'
})
export class AdminUsersComponent implements OnInit {

  constructor(private adminUsersService: AdminUsersService,
              private searchHistoryService: SearchHistoryService,
              private ngbModal: NgbModal,
              private titleService: Title) {
    this.titleService.setTitle('Users administration');
  }

  filterFormGroup = new FormGroup({
    query: new FormControl()
  });

  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 10});
  loading$ = new BehaviorSubject<boolean>(true);
  data$: Observable<AdminUsersWrapper>;

  ngOnInit(): void {
    const filter$ = this.filterFormGroup.valueChanges.pipe(
      debounceTime(300),
      startWith(this.filterFormGroup.value)
    );
    this.data$ = combineLatest([filter$, this.pageInfo$]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([filter, pageInfo]) =>
        this.adminUsersService.loadUsers(filter, pageInfo).pipe(
          tapAllHandleError(() => this.loading$.next(false))
        ))
    );
  }

  impersonate(user: AdminUserDto) {
    this.adminUsersService.impersonate(user).subscribe(e => location.reload());
  }

  showSearchHistory(user: AdminUserDto) {
    const ngbModalRef = this.ngbModal.open(SearchHistoryModalComponent, {size: 'lg'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.user = user;
  }
}
