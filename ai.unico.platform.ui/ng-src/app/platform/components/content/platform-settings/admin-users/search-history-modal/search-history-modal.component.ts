import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { AdminUserDto } from 'ng-src/app/services/admin-users.service';
import {
  SearchHistoryDto,
  SearchHistoryService,
  SearchHistoryWrapper
} from 'ng-src/app/services/search-history.service';
import { filter, finalize, map, shareReplay, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';

@Component({
  selector: 'app-search-history-modal',
  templateUrl: './search-history-modal.component.html',
  styleUrls: []
})
export class SearchHistoryModalComponent implements OnInit {
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 5});
  loading$ = new BehaviorSubject<boolean>(true);
  searchHistoryWrapper$: Observable<SearchHistoryWrapper>;
  searchHistoryDtos$: Observable<ExtendedSearchHistory[]>;

  user: AdminUserDto;

  constructor(private ngbActiveModal: NgbActiveModal,
              private searchHistoryService: SearchHistoryService) {
  }

  ngOnInit(): void {
    this.searchHistoryWrapper$ = combineLatest([this.pageInfo$]).pipe(
      tap(() => this.loading$.next(true)),
      filter(([s]) => !!s),
      switchMap(([pi]) =>
        this.searchHistoryService.findSearchHistory(this.user.userId, {
          workspaces: [],
          favorite: false
        }, pi).pipe(
          tapAllHandleError(() => this.loading$.next(false))
        )),
      shareReplay()
    );

    this.searchHistoryDtos$ = this.searchHistoryWrapper$.pipe(
      map(w => w.historyDtos.map(row => {
        return {
          row,
          saving$: new BehaviorSubject<boolean>(false),
          deleted$: new BehaviorSubject<boolean>(false),
          favorite$: new BehaviorSubject<boolean>(row.favorite)
        };
      }))
    );
  }

  deleteRow(rowWrapper: ExtendedSearchHistory): void {
    rowWrapper.saving$.next(true);
    this.searchHistoryService.deleteSearchHistory(rowWrapper.row.searchHistId).pipe(
      finalize(() => rowWrapper.saving$.next(false))
    ).subscribe(() => rowWrapper.deleted$.next(true));
  }

  dismiss(): void {
    this.ngbActiveModal.dismiss();
  }
}

interface ExtendedSearchHistory {
  row: SearchHistoryDto;
  saving$: BehaviorSubject<boolean>;
  deleted$: BehaviorSubject<boolean>;
  favorite$: BehaviorSubject<boolean>;
}
