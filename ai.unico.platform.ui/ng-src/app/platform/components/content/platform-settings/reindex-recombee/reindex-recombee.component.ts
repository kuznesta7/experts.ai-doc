import {Component, OnInit} from '@angular/core';
import {IndexTarget, RecombeeReindexOptionDto, ReindexService} from 'ng-src/app/services/reindex.service';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {FlashService} from 'ng-src/app/shared/services/flash.service';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';

@Component({
  selector: 'app-reindex-recombee',
  templateUrl: './reindex-recombee.component.html',
})
export class ReindexRecombeeComponent implements OnInit {
  reindexOptions$: Observable<RecombeeReindexOptionDto[]> = this.reindexService.getRecombeeReindexStatuses();
  loading$ = new BehaviorSubject<boolean>(true);
  targets$ = new BehaviorSubject<IndexTarget[]>([]);
  organizations$ = new BehaviorSubject<OrganizationBaseDto[]>([]);

  constructor(private reindexService: ReindexService,
              private flashService: FlashService) { }

  ngOnInit(): void {

    this.reindexOptions$.subscribe((options: RecombeeReindexOptionDto[]): void => {
      this.loading$.next(false);
      this.organizations$.next(options.map((option: RecombeeReindexOptionDto) => option.organization));
      const set = new Set(options.reduce((targets: IndexTarget[], option: RecombeeReindexOptionDto) => option.targets.concat(targets), []));
      this.targets$.next(Array.from(set));
    });
  }

  reindexAll(targets: IndexTarget[], organizations: OrganizationBaseDto[], clean: boolean, event: any): void {
    event.target.disabled = true;
    this.flashService.ok('Recombee index started');
    const jobs: Observable<void>[] = [];
    for (const organization of organizations) {
      const obs = this.reindexService.reindexRecombee(clean, targets, organization.organizationId);
      jobs.push(obs);
      obs.subscribe(() => {
        this.flashService.ok('Recombee index ' + targets.join(', ') + ' for organization ' + (organization.organizationAbbrev || organization.organizationName) + ' finished');
      });
    }
    combineLatest(jobs).subscribe(() => {
      event.target.disabled = false;
      this.flashService.ok('Recombee index finished');
    });
  }

  reindex(targets: IndexTarget[], organization: OrganizationBaseDto, clean: boolean, event: any): void {
    event.target.disabled = true;
    this.flashService.ok('Recombee index started');
    this.reindexService.reindexRecombee(clean, targets, organization.organizationId).subscribe(() => {
      event.target.disabled = false;
      this.flashService.ok('Recombee index ' + targets.join(', ') + ' for organization ' + (organization.organizationAbbrev || organization.organizationName) + ' finished');
    });
  }

  resetAll(organizations: OrganizationBaseDto[]): void {
    this.flashService.ok('Recombee reset started');
    const jobs: Observable<void>[] = [];
    for (const organization of organizations) {
      const obs = this.reindexService.resetRecombee(organization.organizationId);
      jobs.push(obs);
      obs.subscribe(() => {
        this.flashService.ok('Recombee reset for organization ' + (organization.organizationAbbrev || organization.organizationName) + ' finished');
      });
    }
    combineLatest(jobs).subscribe(() => {
      this.flashService.ok('Recombee reset finished');
    });
  }

  reset(organization: OrganizationBaseDto): void {
    this.reindexService.resetRecombee(organization.organizationId).subscribe(() => {
      this.flashService.ok('Recombee reset finished');
    });
  }

}
