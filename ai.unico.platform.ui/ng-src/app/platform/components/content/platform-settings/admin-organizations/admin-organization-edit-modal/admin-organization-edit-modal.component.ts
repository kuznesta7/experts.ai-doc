import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject, Observable } from 'rxjs';
import { CountryService } from 'ng-src/app/services/country.service';
import { AdminOrganizationDto, AdminOrganizationsService } from 'ng-src/app/services/admin-organizations.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { finalize, tap } from 'rxjs/operators';
import { OrganizationAutocompleteService } from 'ng-src/app/services/organization-autocomplete.service';

@Component({
  selector: 'app-admin-organization-edit-modal',
  templateUrl: './admin-organization-edit-modal.component.html'
})
export class AdminOrganizationEditModalComponent implements OnInit {

  constructor(private countryService: CountryService,
              private adminOrganizationsService: AdminOrganizationsService,
              private ngbActiveModal: NgbActiveModal,
              private organizationAutocompleteService: OrganizationAutocompleteService) {
  }

  formGroup = new FormGroup({
    organizationName: new FormControl('', Validators.required),
    organizationAbbrev: new FormControl(),
    countryCode: new FormControl('', Validators.required)
  });

  countries$ = this.countryService.findCountries();
  organization$ = new BehaviorSubject<AdminOrganizationDto | null>(null);
  saving$ = new BehaviorSubject<boolean>(false);

  ngOnInit(): void {
    this.organization$.subscribe(organization => {
      if (!organization) {
        return;
      }
      this.formGroup.patchValue(organization);
    });
  }

  save(): void {
    const o = this.organization$.value || {} as any;
    const organization = {
      organizationId: o.organizationId,
      ...this.formGroup.value
    };
    this.saving$.next(true);
    let request: Observable<any>;
    if (organization.organizationId) {
      request = this.adminOrganizationsService.updateOrganization(organization);
    } else {
      request = this.adminOrganizationsService.createOrganization(organization).pipe(
        tap(id => organization.organizationId$ = id)
      );
    }
    request.pipe(
      finalize(() => this.saving$.next(false))
    ).subscribe(() => {
      this.ngbActiveModal.close(organization);
    });
  }

  dismiss(): void {
    this.ngbActiveModal.dismiss();
  }
}
