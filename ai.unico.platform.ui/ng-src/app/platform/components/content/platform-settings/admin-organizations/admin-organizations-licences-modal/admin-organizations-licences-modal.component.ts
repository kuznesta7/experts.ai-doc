import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { AdminOrganizationDto } from 'ng-src/app/services/admin-organizations.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { OrganizationLicenceDto, OrganizationLicenceService } from 'ng-src/app/services/organization-licence.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { OrganizationRole } from 'ng-src/app/services/session.service';
import { FormValidationService } from 'ng-src/app/shared/services/form-validation.service';
import { filter, finalize, startWith, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { ConfirmationService } from 'ng-src/app/shared/services/confirmation.service';

@Component({
  selector: 'app-admin-organizations-licences-modal',
  templateUrl: './admin-organizations-licences-modal.component.html'
})
export class AdminOrganizationsLicencesModalComponent implements OnInit {

  constructor(private ngbActiveModal: NgbActiveModal,
              private organizationLicenceService: OrganizationLicenceService,
              private formValidationService: FormValidationService,
              private confirmationService: ConfirmationService) {
  }

  organization$ = new BehaviorSubject<AdminOrganizationDto | null>(null);
  addLicenceMode$ = new BehaviorSubject<boolean>(false);
  saving$ = new BehaviorSubject<boolean>(false);
  loading$ = new BehaviorSubject<boolean>(false);
  licences$: Observable<OrganizationLicenceDto[]>;

  formGroup = new FormGroup({
    role: new FormControl('', Validators.required),
    validUntil: new FormControl(),
    numberOfLicences: new FormControl('', Validators.required)
  });

  rolesToChoose = [OrganizationRole.ORGANIZATION_USER, OrganizationRole.ORGANIZATION_EDITOR, OrganizationRole.ORGANIZATION_VIEWER];
  roleTranslations: any = {
    [OrganizationRole.ORGANIZATION_USER]: 'Basic User',
    [OrganizationRole.ORGANIZATION_EDITOR]: 'Organization Editor',
    [OrganizationRole.ORGANIZATION_VIEWER]: 'Organization Viewer',
  };

  private reload$ = new Subject<void>();
  private licencesUpdated = false;

  ngOnInit(): void {
    this.licences$ = combineLatest([this.organization$ as Observable<AdminOrganizationDto>, this.reload$.pipe(startWith(true))]).pipe(
      filter(([organization]) => !!organization && !!organization.organizationId),
      tap(() => this.loading$.next(true)),
      switchMap(([{organizationId}]) => this.organizationLicenceService.getLicences(organizationId, true).pipe(
        tapAllHandleError(() => this.loading$.next(false))
      ))
    );
  }

  dismiss(): void {
    if (!this.licencesUpdated) {
      this.ngbActiveModal.dismiss();
    } else {
      this.ngbActiveModal.close();
    }
  }

  addLicence(): void {
    if (!this.organization$.value?.organizationId) {
      return;
    }
    this.formValidationService.validateForm(this.formGroup);
    const value = this.formGroup.getRawValue();
    this.saving$.next(true);
    this.organizationLicenceService.addLicence(this.organization$.value.organizationId, value.role, value.numberOfLicences, value.validUntil).pipe(
      finalize(() => this.saving$.next(false))
    ).subscribe(() => {
      this.reload$.next();
      this.addLicenceMode$.next(false);
      this.formGroup.reset();
      this.licencesUpdated = true;
    });
  }

  removeLicence(licence: OrganizationLicenceDto): void {
    const organization = this.organization$.value;
    if (!organization?.organizationId) {
      return;
    }
    this.confirmationService.confirm({
      title: 'Remove organization licence',
      content: `Are you sure you want to remove the "${this.roleTranslations[licence.licenceRole]}" licence?`,
      confirmLabel: 'Remove',
      confirmClass: 'danger'
    }).subscribe(() => {
      this.loading$.next(true);
      this.organizationLicenceService.removeLicence(organization.organizationId, licence.organizationLicenceId).subscribe(() => this.reload$.next());
      this.licencesUpdated = true;
    });
  }
}
