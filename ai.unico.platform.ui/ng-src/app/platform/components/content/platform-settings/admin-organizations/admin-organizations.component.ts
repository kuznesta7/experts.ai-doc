import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import {
  AdminOrganizationDto,
  AdminOrganizationsService,
  AdminOrganizationWrapper
} from 'ng-src/app/services/admin-organizations.service';
import { debounceTime, finalize, map, shareReplay, startWith, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { QueryParamService } from 'ng-src/app/shared/services/query-param.service';
import { CountryService } from 'ng-src/app/services/country.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminOrganizationEditModalComponent } from 'ng-src/app/platform/components/content/platform-settings/admin-organizations/admin-organization-edit-modal/admin-organization-edit-modal.component';
import { AdminOrganizationsLicencesModalComponent } from 'ng-src/app/platform/components/content/platform-settings/admin-organizations/admin-organizations-licences-modal/admin-organizations-licences-modal.component';
import { OrganizationStructureRelation } from 'ng-src/app/enums/organization-structure-relation';
import { OrganizationStructureService } from 'ng-src/app/services/organization-structure.service';
import { OrganizationBaseDto } from 'ng-src/app/interfaces/organization-base-dto';
import { ConfirmationService } from 'ng-src/app/shared/services/confirmation.service';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-admin-organizations',
  templateUrl: './admin-organizations.component.html'
})
export class AdminOrganizationsComponent implements OnInit {

  constructor(private adminOrganizationService: AdminOrganizationsService,
              private queryParamService: QueryParamService,
              private countryService: CountryService,
              private ngbModal: NgbModal,
              private organizationStructureService: OrganizationStructureService,
              private confirmationService: ConfirmationService,
              private titleService: Title) {
    this.titleService.setTitle('Organizations administration');
  }

  filterFormGroup = new FormGroup({
    query: new FormControl(),
    exactMatch: new FormControl(true),
    countryCodes: new FormControl([]),
    onlyWithoutCountry: new FormControl(false),
    invalidLicencesOnly: new FormControl(false),
    activeLicencesOnly: new FormControl(false),
    replacementState: new FormControl('VALID')
  });

  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 30});
  loading$ = new BehaviorSubject<boolean>(true);
  saving$ = new BehaviorSubject<boolean>(false);
  data$: Observable<AdminOrganizationWrapper>;
  organizations$: Observable<ExtendedAdminOrganizationDto[]>;
  countries$ = this.countryService.findCountriesStandardized();
  updateMode$ = new BehaviorSubject<UpdateMode>(null);
  selectedOrganization$ = new BehaviorSubject<ExtendedAdminOrganizationDto | null>(null);
  relations = OrganizationStructureRelation;

  ngOnInit(): void {
    this.queryParamService.handleFilterFormGroup(this.filterFormGroup);

    const filter$ = this.filterFormGroup.valueChanges.pipe(
      debounceTime(300),
      startWith(this.filterFormGroup.value)
    );
    this.data$ = combineLatest([filter$, this.pageInfo$]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([filter, pageInfo]) => this.adminOrganizationService.loadOrganizations(filter, pageInfo).pipe(
        tapAllHandleError(() => this.loading$.next(false))
      )),
      shareReplay()
    );

    this.organizations$ = this.data$.pipe(
      map(w => w.adminOrganizationDtos.map(o => ({
        ...o,
        parentOrganizationIds: o.upperOrganizations.map(ro => ro.organizationId)
      })))
    );
  }

  editOrganization(organization?: AdminOrganizationDto): void {
    const ngbModalRef = this.ngbModal.open(AdminOrganizationEditModalComponent, {size: 'xl'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.organization$.next(organization);
    ngbModalRef.closed.subscribe(() => this.reload());
  }

  editLicences(organization: AdminOrganizationDto): void {
    const ngbModalRef = this.ngbModal.open(AdminOrganizationsLicencesModalComponent, {size: 'lg'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.organization$.next(organization);
    ngbModalRef.closed.subscribe(() => this.reload());
  }

  updateStructure(selected: AdminOrganizationDto, organization: ExtendedAdminOrganizationDto, relation: OrganizationStructureRelation): void {
    this.saving$.next(true);
    this.organizationStructureService.addLowerOrganization(selected.organizationId, organization.organizationId, relation).pipe(
      finalize(() => this.saving$.next(false))
    ).subscribe(() => {
      this.reload();
    });
  }

  removeOrganizationUnit(organization: ExtendedAdminOrganizationDto, lowerOrganization: OrganizationBaseDto): void {
    this.saving$.next(true);
    this.organizationStructureService.removeLowerOrganization(organization.organizationId, lowerOrganization.organizationId).pipe(
      finalize(() => this.saving$.next(false))
    ).subscribe(() => this.reload());
  }

  replaceOrganization(organization: ExtendedAdminOrganizationDto, substituteOrganization: ExtendedAdminOrganizationDto): void {
    let confirm: Observable<any> = of(true);
    if (organization.rank > substituteOrganization.rank) {
      confirm = this.confirmationService.confirm({
        title: 'Replace organization',
        content: 'You are going to replace an organization with a higher rank by an organization with a lower rank. Are you sure you didn\'t want to do it vice versa?'
      });
    }
    confirm.subscribe(() => {
      this.saving$.next(true);
      this.adminOrganizationService.replaceOrganization(organization.organizationId, substituteOrganization.organizationId).pipe(
        finalize(() => this.saving$.next(false))
      ).subscribe(() => this.reload());
    });
  }

  toggleSearch(organization: AdminOrganizationDto): void {
    this.loading$.next(true);
    this.adminOrganizationService.setOrganizationSearch(organization.organizationId, !organization.hasAllowedSearch).pipe(
    ).subscribe(() => this.reload());
  }

  toggleMembers(organization: AdminOrganizationDto): void {
    this.loading$.next(true);
    this.adminOrganizationService.setOrganizationMembers(organization.organizationId, !organization.membersAllowed).pipe(
    ).subscribe(() => this.reload());
  }

  private reload(): void {
    this.filterFormGroup.patchValue({});
  }
}

interface ExtendedAdminOrganizationDto extends AdminOrganizationDto {
  parentOrganizationIds: number[];
}

type UpdateMode = 'structure' | 'replace' | null;
