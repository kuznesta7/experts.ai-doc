import { Component, OnInit } from '@angular/core';
import { UrlParams } from 'ng-src/app/constants/url-params';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { EvidenceSettingsService, PublicProfileVisibilityDto } from 'ng-src/app/services/evidence-settings.service';
import { FormControl, FormGroup } from '@angular/forms';
import { FlashService } from 'ng-src/app/shared/services/flash.service';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-public-profile',
  templateUrl: './public-profile.component.html',
  styleUrls: []
})
export class PublicProfileComponent implements OnInit {
  organizationId: number;
  data$: Observable<PublicProfileVisibilityDto>;

  formGroup = new FormGroup({
    visible: new FormControl(false),
    keywordsVisible: new FormControl(false),
    graphVisible: new FormControl(false),
    expertsVisible: new FormControl(false),
    outcomesVisible: new FormControl(false),
    projectsVisible: new FormControl(false)
  });

  constructor(private route: ActivatedRoute,
              private flashService: FlashService,
              private evidenceSettingsService: EvidenceSettingsService,
              private titleService: Title) {
    this.titleService.setTitle('Organization settings - Public profile');
  }

  ngOnInit(): void {
    this.organizationId = this.route.snapshot.params[UrlParams.ORGANIZATION_ID];
    this.data$ = this.evidenceSettingsService.getVisibilities(this.organizationId);

    this.data$.subscribe(e => {
      // @ts-ignore
      delete e['organizationId'];
      this.formGroup.setValue(e);
    })
  }

  submit(): void {
    const form = this.formGroup.value;
    this.evidenceSettingsService.setVisibilities(this.organizationId, form)
      .subscribe(() => {
        this.flashService.ok('Saved');
      });
  }
}
