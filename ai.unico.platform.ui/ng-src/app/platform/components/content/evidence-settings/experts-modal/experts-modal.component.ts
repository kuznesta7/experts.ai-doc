import { Component, OnInit } from '@angular/core';
import { OrganizationRole } from 'ng-src/app/services/session.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import {
  AdminUserDto,
  AdminUsersWrapper,
  EvidenceSettingsService
} from 'ng-src/app/services/evidence-settings.service';
import { FormControl, FormGroup } from '@angular/forms';
import { debounceTime, startWith, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';

@Component({
  selector: 'app-experts-modal',
  templateUrl: './experts-modal.component.html',
  styleUrls: []
})
export class ExpertsModalComponent implements OnInit {
  role: OrganizationRole;
  organizationId: number;

  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 10});
  loading$ = new BehaviorSubject<boolean>(true);
  data$: Observable<AdminUsersWrapper>;

  filterFormGroup = new FormGroup({
    query: new FormControl()
  });

  constructor(private ngbActiveModal: NgbActiveModal,
              private evidenceSettingsService: EvidenceSettingsService) {
  }

  ngOnInit(): void {
    const filter$ = this.filterFormGroup.valueChanges.pipe(
      debounceTime(300),
      startWith(this.filterFormGroup.value)
    );
    this.data$ = combineLatest([filter$, this.pageInfo$]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([filter, pageInfo]) =>
        this.evidenceSettingsService.getExperts(filter, pageInfo).pipe(
          tapAllHandleError(() => this.loading$.next(false))
        ))
    );
  }

  addRole(user: AdminUserDto) {
    this.evidenceSettingsService
      .addOrganizationRoles(user.userId, this.organizationId, this.role)
      .subscribe(e => location.reload());
  }

  dismiss(): void {
    this.ngbActiveModal.dismiss();
  }
}
