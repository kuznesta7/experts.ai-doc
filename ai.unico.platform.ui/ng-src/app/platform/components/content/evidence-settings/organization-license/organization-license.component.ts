import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {
  EvidenceSettingsService,
  OrganizationLicencePreviewDto,
  OrganizationUserDto
} from 'ng-src/app/services/evidence-settings.service';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UrlParams } from 'ng-src/app/constants/url-params';
import { OrganizationRole } from 'ng-src/app/services/session.service';
import { ExpertsModalComponent } from 'ng-src/app/platform/components/content/evidence-settings/experts-modal/experts-modal.component';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-organization-license',
  templateUrl: './organization-license.component.html',
  styleUrls: []
})
export class OrganizationLicenseComponent implements OnInit {
  organizationId: number;
  data$: Observable<OrganizationLicencePreviewDto[]>;
  organizationTypes: OrganizationType[];

  constructor(private route: ActivatedRoute,
              private evidenceSettingsService: EvidenceSettingsService,
              private ngbModal: NgbModal,
              private titleService: Title) {
    this.titleService.setTitle('Organization settings - Licenses');
  }

  ngOnInit(): void {
    this.organizationId = this.route.snapshot.params[UrlParams.ORGANIZATION_ID];
    this.data$ = this.evidenceSettingsService.findAvailableOrganizationsRoles(this.organizationId);

    this.organizationTypes = [
      {
        title: 'Organization viewer',
        buttonTitle: 'Add organization viewer',
        licenseRole: OrganizationRole.ORGANIZATION_VIEWER
      },
      {
        title: 'Organization editor',
        buttonTitle: 'Add organization editor',
        licenseRole: OrganizationRole.ORGANIZATION_EDITOR
      },
      {
        title: 'User manager',
        buttonTitle: 'Add user manager',
        licenseRole: OrganizationRole.ORGANIZATION_USER
      },
    ];
  }

  removeRole(user: OrganizationUserDto, role: OrganizationRole): void {
    this.evidenceSettingsService
      .removeOrganizationRoles(user.userId, this.organizationId, role)
      .subscribe(e => location.reload());
  }

  showExpertsModal(role: OrganizationRole): void {
    const ngbModalRef = this.ngbModal.open(ExpertsModalComponent, {size: 'lg'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.role = role;
    componentInstance.organizationId = this.organizationId;
  }

}

export interface OrganizationType {
  title: string;
  buttonTitle: string;
  licenseRole: OrganizationRole;
}
