import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { UrlParams } from 'ng-src/app/constants/url-params';
import { ActivatedRoute } from '@angular/router';
import { EvidenceSettingsService } from 'ng-src/app/services/evidence-settings.service';
import { Observable } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { FlashService } from 'ng-src/app/shared/services/flash.service';
import { PathFragment } from 'ng-src/app/constants/path-fragment';
import { debounceTime, map, shareReplay } from 'rxjs/operators';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-widget-type',
  templateUrl: './widget-type.component.html',
  styleUrls: []
})
export class WidgetTypeComponent implements OnInit {

  @ViewChild('widgetCodeElement')
  widgetCodeElement: ElementRef;

  organizationId: number;
  widgetUrl$: Observable<string>;
  widgetParams$: Observable<string>;

  widgetTypes = [
    {id: 'ORGANIZATION_OVERVIEW', description: 'Overview'},
    {id: 'ORGANIZATION_EXPERTS', description: 'Organization experts'},
    {id: 'ORGANIZATION_PROJECTS', description: 'Research projects'},
    {id: 'ORGANIZATION_ITEMS', description: 'Research outcomes'},
    {id: 'ORGANIZATION_COMMERCIALIZATION', description: 'Commercialization projects'},
    {id: 'ORGANIZATION_LECTURES', description: 'Lectures'},
    {id: 'ORGANIZATION_EQUIPMENT', description: 'Equipment/Service'},
    {id: 'ORGANIZATION_CHILDREN', description: 'Organization children'},
    {id: 'ORGANIZATION_OPPORTUNITIES', description: 'Opportunities'}
  ];

  formGroup = new FormGroup({
    ORGANIZATION_OVERVIEW: new FormControl(false),
    ORGANIZATION_EXPERTS: new FormControl(false),
    ORGANIZATION_PROJECTS: new FormControl(false),
    ORGANIZATION_ITEMS: new FormControl(false),
    ORGANIZATION_COMMERCIALIZATION: new FormControl(false),
    EXPERT_DETAIL: new FormControl(false),
    ORGANIZATION_LECTURES: new FormControl(false),
    ORGANIZATION_EQUIPMENT: new FormControl(false),
    ORGANIZATION_CHILDREN: new FormControl(false),
    ORGANIZATION_OPPORTUNITIES: new FormControl(false)
  });

  constructor(private route: ActivatedRoute,
              private flashService: FlashService,
              private evidenceSettingsService: EvidenceSettingsService,
              private titleService: Title) {
    this.titleService.setTitle('Organization settings - Widgets');
  }

  ngOnInit(): void {
    this.organizationId = this.route.snapshot.params[UrlParams.ORGANIZATION_ID];
    const data$ = this.evidenceSettingsService.getAllowedWidgets(this.organizationId).pipe(shareReplay());

    this.widgetParams$ = this.formGroup.valueChanges.pipe(
      map(v => {
        const params = {} as any;
        if (v.ORGANIZATION_OVERVIEW) {
          params[UrlParams.WIDGET_OVERVIEW] = true;
        }
        if (v.ORGANIZATION_EXPERTS) {
          params[UrlParams.WIDGET_EXPERTS] = true;
        }
        if (v.ORGANIZATION_PROJECTS) {
          params[UrlParams.WIDGET_PROJECTS] = true;
        }
        if (v.ORGANIZATION_ITEMS) {
          params[UrlParams.WIDGET_OUTCOMES] = true;
        }
        if (v.ORGANIZATION_COMMERCIALIZATION) {
          params[UrlParams.WIDGET_COMMERCIALIZATION] = true;
        }
        if (v.ORGANIZATION_LECTURES) {
          params[UrlParams.WIDGET_LECTURE] = true;
        }
        if (v.ORGANIZATION_EQUIPMENT) {
          params[UrlParams.WIDGET_EQUIPMENT] = true;
        }
        if (v.ORGANIZATION_CHILDREN) {
          params[UrlParams.WIDGET_ORGANIZATION_CHILDREN] = true;
        }
        if (v.ORGANIZATION_OPPORTUNITIES) {
          params[UrlParams.WIDGET_OPPORTUNITY] = true;
        }
        return params;
      }),
      shareReplay()
    );

    this.widgetUrl$ = this.widgetParams$.pipe(map(params => {
      const httpParams = createHttpParams(params);
      return location.origin + '/' + PathFragment.WIDGETS + '/' + PathFragment.ORGANIZATIONS + '/' + this.organizationId + '?' + httpParams.toString();
    }));

    this.widgetUrl$.pipe(debounceTime(0)).subscribe(() => {
      const nativeElement = this.widgetCodeElement.nativeElement;
      nativeElement.style.height = null;
      nativeElement.style.height = nativeElement.scrollHeight + 'px';
    });

    data$.subscribe(e => {
      this.formGroup.setValue(e);
    });

  }

  submit(): void {
    const form = this.formGroup.value;
    this.evidenceSettingsService.setAllowedWidgets(this.organizationId, form)
      .subscribe(() => {
        this.flashService.ok('Saved');
      });
  }

  copyToClipboard(): void {
    const nativeElement = this.widgetCodeElement.nativeElement;
    nativeElement.select();
    document.execCommand('copy');
    this.flashService.ok('Widget code has been copied to your clipboard');
  }
}
