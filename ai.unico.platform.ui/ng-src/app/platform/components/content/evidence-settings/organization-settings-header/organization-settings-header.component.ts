import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-organization-settings-header',
  templateUrl: './organization-settings-header.component.html',
  styleUrls: []
})
export class OrganizationSettingsHeaderComponent implements OnInit {

  @Input()
  organizationId: number;

  constructor() {
  }

  ngOnInit(): void {
  }

}
