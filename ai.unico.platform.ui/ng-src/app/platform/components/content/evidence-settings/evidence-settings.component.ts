import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-evidence-settings',
  templateUrl: './evidence-settings.component.html',
  styleUrls: []
})
export class EvidenceSettingsComponent implements OnInit {

  constructor(
    private titleService: Title
  ) {
    this.titleService.setTitle('Organization settings');
  }

  ngOnInit(): void {
  }
}

