import { Component, OnInit } from '@angular/core';
import { SessionService } from 'ng-src/app/services/session.service';
import {
  SearchHistoryDto,
  SearchHistoryService,
  SearchHistoryWrapper
} from 'ng-src/app/services/search-history.service';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { filter, finalize, map, shareReplay, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-search-history',
  templateUrl: './search-history.component.html',
})
export class SearchHistoryComponent implements OnInit {

  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 30});
  loading$ = new BehaviorSubject<boolean>(true);
  searchHistoryWrapper$: Observable<SearchHistoryWrapper>;
  searchHistoryDtos$: Observable<ExtendedSearchHistory[]>;

  constructor(private sessionService: SessionService,
              private searchHistoryService: SearchHistoryService,
              private titleService: Title) {
    this.titleService.setTitle('Search history');
  }

  ngOnInit(): void {
    this.searchHistoryWrapper$ = combineLatest([this.sessionService.getCurrentSession(), this.pageInfo$]).pipe(
      tap(() => this.loading$.next(true)),
      filter(([s]) => !!s),
      switchMap(([session, pi]) => this.searchHistoryService.findSearchHistory(session.userId, {
        workspaces: [],
        favorite: false
      }, pi).pipe(
        tapAllHandleError(() => this.loading$.next(false))
      )),
      shareReplay()
    );

    this.searchHistoryDtos$ = this.searchHistoryWrapper$.pipe(
      map(w => w.historyDtos.map(row => {
        return {
          row,
          saving$: new BehaviorSubject<boolean>(false),
          deleted$: new BehaviorSubject<boolean>(false),
          favorite$: new BehaviorSubject<boolean>(row.favorite)
        };
      }))
    );
  }

  updateRow(rowWrapper: ExtendedSearchHistory, favorite: boolean): void {
    rowWrapper.saving$.next(true);
    this.searchHistoryService.updateSearchHistory(rowWrapper.row.searchHistId, favorite, rowWrapper.row.workspaces).pipe(
      finalize(() => rowWrapper.saving$.next(false))
    ).subscribe(() => rowWrapper.favorite$.next(favorite));
  }

  deleteRow(rowWrapper: ExtendedSearchHistory): void {
    rowWrapper.saving$.next(true);
    this.searchHistoryService.deleteSearchHistory(rowWrapper.row.searchHistId).pipe(
      finalize(() => rowWrapper.saving$.next(false))
    ).subscribe(() => rowWrapper.deleted$.next(true));
  }
}

interface ExtendedSearchHistory {
  row: SearchHistoryDto;
  saving$: BehaviorSubject<boolean>;
  deleted$: BehaviorSubject<boolean>;
  favorite$: BehaviorSubject<boolean>;
}
