import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PathService } from 'ng-src/app/shared/services/path.service';
import { map, shareReplay, switchMap, tap } from 'rxjs/operators';
import { UrlParams } from 'ng-src/app/constants/url-params';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { ThesisPreview } from 'ng-src/app/interfaces/thesis-preview';
import { EvidenceThesisService } from 'ng-src/app/services/evidence-thesis.service';

@Component({
  selector: 'app-thesis-detail',
  templateUrl: './thesis-detail.component.html'
})
export class ThesisDetailComponent implements OnInit {

  loading$ = new BehaviorSubject<boolean>(true);
  thesisDetail$: Observable<ThesisPreview>;

  constructor(private route: ActivatedRoute,
              private ngbModal: NgbModal,
              private router: Router,
              private pathService: PathService,
              private thesisService: EvidenceThesisService) {
  }

  ngOnInit(): void {
    const outcomeCode$ = this.route.params.pipe(
      map(p => p[UrlParams.THESIS_CODE])
    );

    this.thesisDetail$ = combineLatest([outcomeCode$,]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([code]) => this.thesisService.getThesisDetail(code)),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );
  }

  back(): void {
    window.history.back();
  }


}
