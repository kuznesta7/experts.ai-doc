import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PathService } from 'ng-src/app/shared/services/path.service';
import { map, shareReplay, switchMap, tap } from 'rxjs/operators';
import { UrlParams } from 'ng-src/app/constants/url-params';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { EvidenceLectureService } from 'ng-src/app/services/evidence-lecture.service';
import { LecturePreview } from 'ng-src/app/interfaces/lecture-preview';

@Component({
  selector: 'app-lecture-detail',
  templateUrl: './lecture-detail.component.html'
})
export class LectureDetailComponent implements OnInit {

  loading$ = new BehaviorSubject<boolean>(true);
  lectureDetail$: Observable<LecturePreview>;

  constructor(private route: ActivatedRoute,
              private ngbModal: NgbModal,
              private router: Router,
              private pathService: PathService,
              private lectureService: EvidenceLectureService) {
  }

  ngOnInit(): void {
    const outcomeCode$ = this.route.params.pipe(
      map(p => p[UrlParams.LECTURE_CODE])
    );

    this.lectureDetail$ = combineLatest([outcomeCode$,]).pipe(
      tap(() => this.loading$.next(true)),
      switchMap(([code]) => this.lectureService.getLectureDetail(code)),
      tapAllHandleError(() => this.loading$.next(false)),
      shareReplay()
    );
  }

  back(): void {
    window.history.back();
  }


}
