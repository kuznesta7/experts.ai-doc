import {Component, OnInit} from '@angular/core';
import {ExpertDetailDto, UserOrganizationDto} from 'ng-src/app/services/expert-detail.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TagService} from 'ng-src/app/services/tag.service';
import {debounceTime, finalize, map, switchMap} from 'rxjs/operators';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {
  OrganizationAutocompleteService,
  OrganizationForAutocomplete
} from 'ng-src/app/services/organization-autocomplete.service';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {UserProfileService} from 'ng-src/app/services/user-profile.service';
import {Router} from '@angular/router';
import {PathFragment} from '../../../../../constants/path-fragment';

@Component({
  selector: 'app-expert-detail-edit-modal',
  templateUrl: './expert-detail-edit-modal.component.html'
})
export class ExpertDetailEditModalComponent implements OnInit {

  constructor(private activeModal: NgbActiveModal,
              private tagService: TagService,
              private userProfileService: UserProfileService,
              private router: Router,
              private organizationAutocompleteService: OrganizationAutocompleteService) {
  }

  searchOrganizationFc = new FormControl();
  organizationOptions$: Observable<OrganizationForAutocomplete[]>;

  saving$ = new BehaviorSubject(false);
  imgFilePreviewUrl$ = new BehaviorSubject<string | null>(null);
  imgFile$ = new BehaviorSubject<File | null>(null);
  paths = PathFragment;

  expert: ExpertDetailDto;
  formGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    organizationDtos: new FormControl([]),
    description: new FormControl(''),
    keywords: new FormControl([])
  });
  lookupKeywords = (query: string) => this.tagService.findRelevantTags(query).pipe(
    map(tags => tags.map(t => {
      return {id: t, description: t};
    }))
  )

  ngOnInit(): void {
    this.formGroup.patchValue(this.expert);

    this.organizationOptions$ = this.searchOrganizationFc.valueChanges.pipe(
      debounceTime(300),
      switchMap(q => {
        const organizationDtos = this.formGroup.value.organizationDtos as OrganizationBaseDto[];
        const options = {disabledOrganizationIds: organizationDtos.map(o => o.organizationId)};
        return this.organizationAutocompleteService.autocompleteOrganizations(q, options);
      })
    );
  }

  saveChanges(): void {
    const userProfileDto = this.formGroup.getRawValue();
    const requests = [];
    this.formGroup.disable();
    this.saving$.next(true);
    console.log(this.expert);
    const imgFile = this.imgFile$.getValue();
    if (this.expert.userId) {
      requests.push(this.userProfileService.updateUserProfile(this.expert.userId, userProfileDto));
      if (imgFile) {
        requests.push(this.userProfileService.updateUserProfileImg(this.expert.userId, 'PORTRAIT', imgFile));
      }
      combineLatest(requests).pipe(
        finalize(() => {
          this.saving$.next(false);
          this.formGroup.enable();
        })
      ).subscribe(() => {
        this.activeModal.close({...this.expert, ...userProfileDto});
      });
    } else {
      const newUserId$ = this.userProfileService.expertToUserProfile(this.expert.expertId, userProfileDto);
      newUserId$.pipe(
        finalize(() => {
          this.saving$.next(false);
          this.formGroup.enable();
        })
      ).subscribe((userId: number) => {
        this.expert.userId = userId;
        if (imgFile) {
          this.userProfileService.updateUserProfileImg(userId, 'PORTRAIT', imgFile).subscribe(() => {});
        }
        this.router.navigate(['/', this.paths.EXPERTS, 'ST' + userId]).then(r => this.activeModal.close({...this.expert, ...userProfileDto}));
      });
    }
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }

  removeOrganization(o: any): void {
    const control = this.formGroup.controls.organizationDtos;
    const newList = (control.value as any[]).filter(v => v !== o);
    control.patchValue(newList);
  }

  addOrganization(event: MatAutocompleteSelectedEvent): void {
    const control = this.formGroup.controls.organizationDtos;
    control.patchValue(control.value.concat([event.option.value]));
    control.value.slice(-1)[0].visible = true;
    this.searchOrganizationFc.patchValue('');
  }

  toggleHidden(o: UserOrganizationDto): void {
    o.visible = !o.visible;
  }

  toggleActive(o: UserOrganizationDto, orgs: UserOrganizationDto[]): void {
    o.active = !o.active;
    if (!o.visible) {
      o.visible = true;
    }
  }
}
