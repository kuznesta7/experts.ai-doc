import { Component, Input, OnInit } from '@angular/core';
import {BehaviorSubject, combineLatest, Observable, of} from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { FormControl, FormGroup } from '@angular/forms';
import {debounceTime, filter, finalize, map, mergeMap, shareReplay, startWith, switchMap, tap} from 'rxjs/operators';
import { ExpertDetailService } from 'ng-src/app/services/expert-detail.service';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { EvidenceItemWrapper } from 'ng-src/app/interfaces/evidence-item-wrapper';
import { ItemTypeService } from 'ng-src/app/services/item-type.service';
import { EvidenceFilter } from 'ng-src/app/interfaces/evidence-filter';
import {
  AddOutcomeModalComponent
} from "ng-src/app/platform/components/common/add-outcome-modal/add-outcome-modal.component";
import {EvidenceItemPreviewWithExperts} from "ng-src/app/interfaces/evidence-item-preview-with-experts";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ItemExpertService} from "ng-src/app/services/item-expert.service";
import {EvidenceItemPreview} from "ng-src/app/interfaces/evidence-item-preview";
import {EvidenceItemService} from "ng-src/app/services/evidence-item.service";
import {
  OutcomeDetailEditModalComponent
} from "ng-src/app/platform/components/content/outcome-detail/outcome-detail-edit-modal/outcome-detail-edit-modal.component";
import {ConfirmationService} from "ng-src/app/shared/services/confirmation.service";
import {SessionService} from "ng-src/app/services/session.service";

@Component({
  selector: 'app-expert-detail-outcomes',
  templateUrl: './expert-detail-outcomes.component.html'
})
export class ExpertDetailOutcomesComponent implements OnInit {

  @Input()
  set expertCode(expertCode: string) {
    this.expertCode$.next(expertCode);
  }

  @Input()
  set filter(filter: EvidenceFilter) {
    this.filterFormGroup.patchValue(filter);
  }

  @Input()
  set precomputeFilter(precomputeFilter: EvidenceFilter) {
    this.precomputeFilterFormGroup.patchValue(precomputeFilter)
    this.searchQuery = precomputeFilter.query
  }

  authorizedUser: number;
  expertCode$ = new BehaviorSubject<string | null>(null);
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 5});
  expertItemsWrapper$: Observable<EvidenceItemWrapper>;
  itemList$: Observable<ExtendedItemPreview[]>;
  precomputedItemsWrapper$: Observable<EvidenceItemWrapper>;
  searchQuery? = ""
  loading$ = new BehaviorSubject(true);
  itemTypeGroups$ = this.itemTypeService.findItemTypeGroups().pipe(
    map(groups => groups.map(g => {
      return {id: g.itemTypeGroupId, description: g.itemTypeGroupTitle};
    }))
  );

  expertId$ = this.expertCode$.pipe(
    map(expertCode => {
      const match = expertCode?.match(/ST(\d+)/);
      return match ? Number(match[1]) : null;
    })
  )

filterFormGroup = new FormGroup({
    query: new FormControl(''),
    resultTypeGroupIds: new FormControl([])
  });

  precomputeFilterFormGroup = new FormGroup({
    query: new FormControl(''),
    resultTypeGroupIds: new FormControl([])
  });

  constructor(private expertDetailService: ExpertDetailService,
              private itemTypeService: ItemTypeService,
              private itemExpertService: ItemExpertService,
              private evidenceItemService: EvidenceItemService,
              private confirmationService: ConfirmationService,
              private ngbModal: NgbModal,
              private sessionService: SessionService) {
  }

  addOutcome(expertId: number): void {
    const ngbModalRef = this.ngbModal.open(AddOutcomeModalComponent, {size: 'xl'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.userId = expertId;
    componentInstance.doAddOutcome = (outcome: EvidenceItemPreviewWithExperts) => {
      return this.getItemId(outcome).pipe(mergeMap(id => this.itemExpertService.addUser(id, expertId)));
    };
  }

  openEditOutcomeModal(item: EvidenceItemPreview) {
    const ngbModalRef = this.ngbModal.open(OutcomeDetailEditModalComponent, {size: 'xl'});
    const componentInstance = ngbModalRef.componentInstance;
    this.evidenceItemService.getItemDetail('ST' + item.itemId).subscribe(
      outcome => {
        componentInstance.outcome$.next(outcome);
      }
    )
  }

  removeOutcome(item: ExtendedItemPreview, expertId: number): void {
    this.confirmationService.confirm({
      title: 'Remove outcome from your profile',
      content: 'Are you sure you want to remove outcome <i>"' + item.itemName + '"</i> from your profile?',
      confirmClass: 'danger',
      confirmLabel: 'Remove'
    }).subscribe(() => {
      item.saving$.next(true);
      this.getItemId(item).pipe(mergeMap(id => this.itemExpertService.removeUser(id, expertId))).pipe(
        finalize(() => { item.saving$.next(false); })
      ).subscribe(() => { item.deleted$.next(true); });
    });
  }

  computeOutcomes(formGroup: FormGroup, precomputing: boolean): void {
    const filter$ = formGroup.valueChanges.pipe(
      debounceTime(500),
      tap(() => this.pageInfo$.next({...this.pageInfo$.value, page: 1})),
      startWith(formGroup.value)
    );
    let tmpItemsWrapper$ : Observable<EvidenceItemWrapper>
    tmpItemsWrapper$ = combineLatest([this.expertCode$, filter$, this.pageInfo$]).pipe(
      filter(([exertCode]) => !!exertCode),
      debounceTime(0),
      tap(() => this.loading$.next(true)),
      switchMap(([expertCode, filter, pageInfo]) => {
        return this.expertDetailService.getExpertOutcomes(expertCode || '', filter, pageInfo).pipe(
          tapAllHandleError(() => this.loading$.next(false))
        );
      }),
      shareReplay()
    );

    if (precomputing) {
      this.precomputedItemsWrapper$ = tmpItemsWrapper$
    } else {
      this.expertItemsWrapper$ = tmpItemsWrapper$
    }

    this.itemList$ = tmpItemsWrapper$.pipe(
      map(w => w.itemPreviewDtos.map(i => {
        return {
          ...i,
          saving$: new BehaviorSubject(false),
          deleted$: new BehaviorSubject(false)
        } as ExtendedItemPreview;
      }))
    );
  }

  private getItemId(item: EvidenceItemPreview): Observable<number> {
    if (item.itemId) {
      return of(item.itemId);
    }
    return this.evidenceItemService.moveItem(item.originalItemId);
  }

  ngOnInit(): void {
    this.sessionService.getCurrentSessionWithUpdates().subscribe((session) => {
      this.authorizedUser = session.userId;
    });

    // Counting number of outcomes on a query
    this.computeOutcomes(this.precomputeFilterFormGroup, true)

    // Removing query from filter if there are no matches and showing all outcomes
    this.precomputedItemsWrapper$.subscribe(result => {
      if (result.numberOfAllItems === 0) {
        this.filter = {query: ""}
      } else {
        this.filter = {query: this.searchQuery}
      }
      this.computeOutcomes(this.filterFormGroup, false)
    })
  }
}

interface ExtendedItemPreview extends EvidenceItemPreviewWithExperts {
  saving$: BehaviorSubject<boolean>;
  deleted$: BehaviorSubject<boolean>;
}
