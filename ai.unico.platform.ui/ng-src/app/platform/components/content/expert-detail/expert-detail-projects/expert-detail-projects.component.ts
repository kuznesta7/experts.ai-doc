import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { debounceTime, filter, shareReplay, startWith, switchMap, tap } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';
import { ExpertDetailService } from 'ng-src/app/services/expert-detail.service';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { ProjectWrapper } from 'ng-src/app/interfaces/project-wrapper';

@Component({
  selector: 'app-expert-detail-projects',
  templateUrl: './expert-detail-projects.component.html'
})
export class ExpertDetailProjectsComponent implements OnInit {

  @Input()
  set expertCode(expertCode: string) {
    this.expertCode$.next(expertCode);
  }

  expertCode$ = new BehaviorSubject<string | null>(null);
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 5});
  expertProjectsWrapper$: Observable<ProjectWrapper>;
  loading$ = new BehaviorSubject(true);

  filterFormGroup = new FormGroup({
    query: new FormControl(''),
    resultTypeGroupIds: new FormControl([])
  });

  constructor(private expertDetailService: ExpertDetailService) {
  }

  ngOnInit(): void {
    const filter$ = this.filterFormGroup.valueChanges.pipe(
      debounceTime(500),
      tap(() => this.pageInfo$.next({...this.pageInfo$.value, page: 1})),
      startWith(this.filterFormGroup.value)
    );
    this.expertProjectsWrapper$ = combineLatest([this.expertCode$, filter$, this.pageInfo$]).pipe(
      filter(([exertCode]) => !!exertCode),
      debounceTime(0),
      tap(() => this.loading$.next(true)),
      switchMap(([expertCode, filter, pageInfo]) => {
        return this.expertDetailService.getExpertProjects(expertCode || '', filter, pageInfo).pipe(
          tapAllHandleError(() => this.loading$.next(false))
        );
      }),
      shareReplay()
    );
  }

}
