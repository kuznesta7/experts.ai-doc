import { Component, Input, OnInit } from '@angular/core';
import { EvidenceFilter } from 'ng-src/app/interfaces/evidence-filter';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { FormControl, FormGroup } from '@angular/forms';
import { ExpertDetailService } from 'ng-src/app/services/expert-detail.service';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { debounceTime, filter, shareReplay, startWith, switchMap, tap } from 'rxjs/operators';
import { CommercializationProjectWrapper } from 'ng-src/app/interfaces/commercialization-project-wrapper';

@Component({
  selector: 'app-expert-detail-commercialization',
  templateUrl: './expert-detail-commercialization.component.html'
})
export class ExpertDetailCommercializationComponent implements OnInit {

  @Input()
  set expertCode(expertCode: string) {
    this.expertCode$.next(expertCode);
  }

  @Input()
  set filter(filter: EvidenceFilter) {
    this.filterFormGroup.patchValue(filter);
  }

  commercializationProjectWrapper$: Observable<CommercializationProjectWrapper>;
  expertCode$ = new BehaviorSubject<string | null>(null);
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 5});
  loading$ = new BehaviorSubject(true);
  filterFormGroup = new FormGroup({
    query: new FormControl('')
  });

  constructor(private expertDetailService: ExpertDetailService) {
  }

  ngOnInit(): void {
    const filter$ = this.filterFormGroup.valueChanges.pipe(
      debounceTime(500),
      tap(() => this.pageInfo$.next({...this.pageInfo$.value, page: 1})),
      startWith(this.filterFormGroup.value)
    );

    this.commercializationProjectWrapper$ = combineLatest([this.expertCode$, filter$, this.pageInfo$]).pipe(
      filter(([id]) => !!id),
      debounceTime(0),
      tap(() => this.loading$.next(true)),
      switchMap(([expertCode, filter, pageInfo]) => {
        return this.expertDetailService.getExpertCommercialization(expertCode || '', filter, pageInfo).pipe(
          tapAllHandleError(() => this.loading$.next(false))
        );
      }),
      shareReplay()
    );
  }

}
