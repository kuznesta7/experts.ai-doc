import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { FormControl, FormGroup } from '@angular/forms';
import { debounceTime, filter, map, shareReplay, startWith, switchMap, tap } from 'rxjs/operators';
import { ExpertDetailService } from 'ng-src/app/services/expert-detail.service';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { ItemTypeService } from 'ng-src/app/services/item-type.service';
import { EvidenceFilter } from 'ng-src/app/interfaces/evidence-filter';
import { EvidenceThesisWrapper } from 'ng-src/app/interfaces/evidence-thesis-wrapper';

@Component({
  selector: 'app-expert-detail-thesis',
  templateUrl: './expert-detail-thesis.component.html'
})
export class ExpertDetailThesisComponent implements OnInit {

  @Input()
  set expertCode(expertCode: string) {
    this.expertCode$.next(expertCode);
  }

  @Input()
  set filter(filter: EvidenceFilter) {
    this.filterFormGroup.patchValue(filter);
  }

  expertCode$ = new BehaviorSubject<string | null>(null);
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 5});
  expertThesisWrapper$: Observable<EvidenceThesisWrapper>;
  loading$ = new BehaviorSubject(true);
  itemTypeGroups$ = this.itemTypeService.findItemTypeGroups().pipe(
    map(groups => groups.map(g => {
      return {id: g.itemTypeGroupId, description: g.itemTypeGroupTitle};
    }))
  );

  filterFormGroup = new FormGroup({
    query: new FormControl(''),
    resultTypeGroupIds: new FormControl([])
  });

  constructor(private expertDetailService: ExpertDetailService,
              private itemTypeService: ItemTypeService) {
  }

  ngOnInit(): void {
    const filter$ = this.filterFormGroup.valueChanges.pipe(
      debounceTime(500),
      tap(() => this.pageInfo$.next({...this.pageInfo$.value, page: 1})),
      startWith(this.filterFormGroup.value)
    );
    this.expertThesisWrapper$ = combineLatest([this.expertCode$, filter$, this.pageInfo$]).pipe(
      filter(([exertCode]) => !!exertCode),
      debounceTime(0),
      tap(() => this.loading$.next(true)),
      switchMap(([expertCode, filterO, pageInfo]) => {
        return this.expertDetailService.getExpertThesis(expertCode || '', filterO, pageInfo).pipe(
          tapAllHandleError(() => this.loading$.next(false))
        );
      }),
      shareReplay()
    );
  }

}
