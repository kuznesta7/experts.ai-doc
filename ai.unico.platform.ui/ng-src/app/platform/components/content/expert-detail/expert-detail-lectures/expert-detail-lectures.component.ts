import { Component, Input, OnInit } from '@angular/core';
import { EvidenceFilter } from 'ng-src/app/interfaces/evidence-filter';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { debounceTime, filter, map, shareReplay, startWith, switchMap, tap } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';
import { ExpertDetailService } from 'ng-src/app/services/expert-detail.service';
import { ItemTypeService } from 'ng-src/app/services/item-type.service';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { EvidenceLectureWrapper } from 'ng-src/app/interfaces/evidence-lecture-wrapper';

@Component({
  selector: 'app-expert-detail-lectures',
  templateUrl: './expert-detail-lectures.component.html'
})
export class ExpertDetailLecturesComponent implements OnInit {

  @Input()
  set expertCode(expertCode: string) {
    this.expertCode$.next(expertCode);
  }

  @Input()
  set filter(filterO: EvidenceFilter) {
    this.filterFormGroup.patchValue(filterO);
  }

  expertCode$ = new BehaviorSubject<string | null>(null);
  pageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 5});
  evidenceLectureWrapper$: Observable<EvidenceLectureWrapper>;
  loading$ = new BehaviorSubject(true);
  itemTypeGroups$ = this.itemTypeService.findItemTypeGroups().pipe(
    map(groups => groups.map(g => {
      return {id: g.itemTypeGroupId, description: g.itemTypeGroupTitle};
    }))
  );

  filterFormGroup = new FormGroup({
    query: new FormControl(''),
    resultTypeGroupIds: new FormControl([])
  });

  constructor(private expertDetailService: ExpertDetailService,
              private itemTypeService: ItemTypeService) {
  }

  ngOnInit(): void {
    const filter$ = this.filterFormGroup.valueChanges.pipe(
      debounceTime(500),
      tap(() => this.pageInfo$.next({...this.pageInfo$.value, page: 1})),
      startWith(this.filterFormGroup.value)
    );
    this.evidenceLectureWrapper$ = combineLatest([this.expertCode$, filter$, this.pageInfo$]).pipe(
      filter(([exertCode]) => !!exertCode),
      debounceTime(0),
      tap(() => this.loading$.next(true)),
      switchMap(([expertCode, filterO, pageInfo]) => {
        return this.expertDetailService.getExpertLectures(expertCode || '', filterO, pageInfo).pipe(
          tapAllHandleError(() => this.loading$.next(false))
        );
      }),
      shareReplay()
    );
  }

}
