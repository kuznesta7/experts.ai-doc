import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {filter, finalize, map, shareReplay, startWith, switchMap, tap} from 'rxjs/operators';
import {UrlParams} from 'ng-src/app/constants/url-params';
import {ExpertDetailDto, ExpertDetailService} from 'ng-src/app/services/expert-detail.service';
import {BehaviorSubject, combineLatest, Observable, Subject} from 'rxjs';
import {tapAllHandleError} from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {AuthorizationService} from 'ng-src/app/services/authorization.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {
  ExpertDetailEditModalComponent
} from 'ng-src/app/platform/components/content/expert-detail/expert-detail-edit-modal/expert-detail-edit-modal.component';
import {EvidenceFilter} from 'ng-src/app/interfaces/evidence-filter';
import {QueryParamService} from 'ng-src/app/shared/services/query-param.service';
import {UploadService} from 'ng-src/app/services/upload.service';
import {UserProfileService} from 'ng-src/app/services/user-profile.service';
import {ExpertFollowService, FollowedProfilePreviewDto} from 'ng-src/app/services/expert-follow.service';
import {SessionService} from 'ng-src/app/services/session.service';
import {RecombeeService} from 'ng-src/app/services/recombee.service';
import {RecommendationWrapper} from 'ng-src/app/interfaces/recommendation-wrapper';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-expert-detail',
  templateUrl: './expert-detail.component.html'
})
export class ExpertDetailComponent implements OnInit {
  expertDetail$: Observable<ExpertDetailDto>;
  detailLoading$ = new BehaviorSubject<boolean>(true);
  filter: EvidenceFilter;
  recombeeService: RecombeeService;

  @Input()
  recommendationWrapper: RecommendationWrapper;

  outcomeListModes = OutcomeListMode;
  outcomeListModeDetails: OutcomeListModeDetail[] = [
    {
      outcomeListMode: OutcomeListMode.COMMERCIALIZATION,
      title: 'Commercialization projects',
      countProperty: 'numberOfInvestmentProjects',
      icon: 'rocket' as any
    }, {
      outcomeListMode: OutcomeListMode.RESEARCH_OUTCOME,
      title: 'Research outcomes',
      countProperty: 'numberOfAllItems',
      icon: 'file-alt' as any
    }, {
      outcomeListMode: OutcomeListMode.RESEARCH_PROJECT,
      title: 'Research projects',
      countProperty: 'numberOfProjects',
      icon: 'microscope' as any
    }, {
      outcomeListMode: OutcomeListMode.THESIS,
      title: 'Thesis',
      countProperty: 'numberOfAllThesis',
      icon: 'university' as any
    }, {
      outcomeListMode: OutcomeListMode.LECTURES,
      title: 'Lectures',
      countProperty: 'numberOfAllLectures',
      icon: 'graduation-cap' as any
    }
  ];
  outcomeListMode$ = new BehaviorSubject<OutcomeListMode | null>(null);
  outcomeListPageInfo$ = new BehaviorSubject<PageInfo>({page: 1, limit: 10});
  editable$ = new BehaviorSubject<boolean>(false);
  update$ = new Subject<void>();
  reloadImgUrl$: Observable<string>;
  reloadFollow$ = new Subject<void>();
  follow$: Observable<FollowedProfilePreviewDto>;
  followLoading$ = new BehaviorSubject<boolean>(true);
  session$ = this.sessionService.getCurrentSession();

  constructor(private route: ActivatedRoute,
              private expertDetailService: ExpertDetailService,
              private authorizationService: AuthorizationService,
              private queryParamService: QueryParamService,
              private userProfileService: UserProfileService,
              private uploadService: UploadService,
              private ngbModal: NgbModal,
              private expertFollowService: ExpertFollowService,
              private sessionService: SessionService,
              private titleService: Title) {
  }

  ngOnInit(): void {
    this.recombeeService = new RecombeeService();
    const expertCode$ = this.route.params.pipe(
      map(p => p[UrlParams.EXPERT_CODE])
    );
    this.outcomeListMode$.subscribe(() => {
      this.outcomeListPageInfo$.next({...this.outcomeListPageInfo$.value, page: 1});
    });
    this.expertDetail$ = combineLatest([expertCode$, this.update$.pipe(startWith(null))]).pipe(
      tap(() => this.detailLoading$.next(true)),
      switchMap(([expertCode]) => this.expertDetailService.getExpertDetail(expertCode).pipe(
        tap(expertDetail => {
          if (expertDetail.numberOfInvestmentProjects > 0) {
            this.outcomeListMode$.next(OutcomeListMode.COMMERCIALIZATION);
          } else if (expertDetail.numberOfAllItems > 0) {
            this.outcomeListMode$.next(OutcomeListMode.RESEARCH_OUTCOME);
          } else if (expertDetail.numberOfProjects > 0) {
            this.outcomeListMode$.next(OutcomeListMode.RESEARCH_PROJECT);
          } else if (expertDetail.numberOfAllThesis > 0) {
            this.outcomeListMode$.next(OutcomeListMode.THESIS);
          } else if (expertDetail.numberOfAllLectures > 0) {
            this.outcomeListMode$.next(OutcomeListMode.LECTURES);
          }
          this.authorizationService.canEditUserProfile(expertDetail.userId).subscribe(e => this.editable$.next(e));
        }),
        tapAllHandleError(() => this.detailLoading$.next(false))
      )),
      shareReplay()
    );

    this.expertDetail$.pipe().subscribe(expert => {
      const name = expert.name;
      this.titleService.setTitle('Expert - ' + name);
    });


    this.follow$ = combineLatest([this.expertDetail$, this.session$, this.reloadFollow$.pipe(startWith(true))]).pipe(
      filter(([_, s]) => !!s),
      tap(() => this.followLoading$.next(true)),
      switchMap(([e, session]) => this.expertFollowService.getFollowForProfile(session.userId, {
        followedUserId: e.userId,
        followedExpertId: e.expertId
      }).pipe(tapAllHandleError(() => this.followLoading$.next(false)))),
      shareReplay()
    );

    let v = 2;
    this.reloadImgUrl$ = this.update$.pipe(
      map(() => '&v=' + v++),
      shareReplay()
    );

    const filterFromUrl = this.queryParamService.getFilter({[UrlParams.SEARCH_QUERY]: ''});
    this.filter = {query: filterFromUrl[UrlParams.SEARCH_QUERY]};
  }

  back(): void {
    window.history.back();
  }

  setMode(mode: OutcomeListModeDetail): void {
    this.outcomeListMode$.next(mode.outcomeListMode);
  }

  openEditModal(expert: ExpertDetailDto): void {
    const ngbModalRef = this.ngbModal.open(ExpertDetailEditModalComponent, {size: 'xl'});
    const componentInstance = ngbModalRef.componentInstance as ExpertDetailEditModalComponent;
    componentInstance.expert = expert;
    ngbModalRef.closed.subscribe(_ => this.update$.next());
  }

  changeCoverImg(expert: ExpertDetailDto): void {
    this.uploadService.openUploadModal(files => {
      return this.userProfileService.updateUserProfileImg(expert.userId, 'COVER', files[0]);
    }, {preview: true, fileType: 'image', title: 'Change cover image'}).closed.subscribe(_ => {
      this.update$.next();
    });
  }

  followProfile(userId: number, expertDetail: ExpertDetailDto, follow: boolean): void {
    this.followLoading$.next(true);
    const definition = {followedExpertId: expertDetail.expertId, followedUserId: expertDetail.userId};
    let request;
    if (follow) {
      request = this.expertFollowService.followProfile(userId, definition);
    } else {
      request = this.expertFollowService.unfollowProfile(userId, definition);
    }
    request.pipe(
      finalize(() => this.followLoading$.next(false))
    ).subscribe(() => this.reloadFollow$.next());
  }
}

enum OutcomeListMode {
  COMMERCIALIZATION, RESEARCH_OUTCOME, RESEARCH_PROJECT, THESIS,
  LECTURES
}

interface OutcomeListModeDetail {
  outcomeListMode: OutcomeListMode;
  title: string;
  countProperty: string;
  icon: any;
}
