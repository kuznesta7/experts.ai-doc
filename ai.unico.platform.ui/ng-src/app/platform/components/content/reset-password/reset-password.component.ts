import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { UrlParams } from 'ng-src/app/constants/url-params';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormValidationService } from 'ng-src/app/shared/services/form-validation.service';
import { FlashService } from 'ng-src/app/shared/services/flash.service';
import { BehaviorSubject } from 'rxjs';
import { UserSettingsService } from 'ng-src/app/services/user-settings.service';
import { LoginService } from 'ng-src/app/services/login.service';
import { SessionService } from 'ng-src/app/services/session.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html'
})
export class ResetPasswordComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private flashService: FlashService,
              private loginService: LoginService,
              private sessionService: SessionService,
              private userSettingsService: UserSettingsService,
              private formValidationService: FormValidationService) {
  }

  private token: string;
  email$ = new BehaviorSubject<string>('');

  saving$ = new BehaviorSubject<boolean>(false);

  formGroup = new FormGroup({
    password: new FormControl('', Validators.required),
    confirmPassword: new FormControl('', Validators.required)
  });

  session$ = this.sessionService.getCurrentSession();

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(p => {
      console.log(p);
      this.token = p[UrlParams.PASSWORD_TOKEN];
      this.email$.next(p[UrlParams.USER_EMAIL]);
    });
  }

  submit(): void {
    this.formValidationService.validateForm(this.formGroup);
    const value = this.formGroup.value;
    const password = value.password;
    if (password !== value.confirmPassword) {
      this.flashService.error('Entered passwords must match');
      return;
    }
    this.saving$.next(true);
    this.formGroup.disable();
    this.userSettingsService.resetPassword(this.email$.value, this.token, password).pipe(
      finalize(() => {
        this.saving$.next(false);
        this.formGroup.enable();
      })
    ).subscribe(() => {
      this.flashService.ok('Your password has been successfully set');
      this.loginService.doLogin(this.email$.value, password, true).subscribe(() =>
        window.location.pathname = '');
    });
  }

}
