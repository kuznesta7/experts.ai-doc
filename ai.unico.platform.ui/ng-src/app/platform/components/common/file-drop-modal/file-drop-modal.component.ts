import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { FlashService } from 'ng-src/app/shared/services/flash.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FileType } from 'ng-src/app/platform/components/common/file-drop/file-drop.component';
import { FilePreview } from 'ng-src/app/platform/components/common/file-preview/file-preview.component';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-file-drop-modal',
  templateUrl: './file-drop-modal.component.html'
})
export class FileDropModalComponent implements OnInit {

  files$ = new BehaviorSubject<File[]>([]);
  title: string | undefined;
  multiple: boolean;
  preview: boolean;
  uploading$ = new BehaviorSubject<boolean>(false);
  previewUrl$ = new BehaviorSubject<string | null>(null);
  uploadCallback: (files: File[]) => Observable<any>;
  fileType: FileType | null;
  filePreviews$: Observable<ExtendedFilePreview[]> = this.files$.pipe(
    map(files => files.map(f => {
      return {
        fileName: f.name,
        fileSize: f.size,
        fileType: f.name.slice(f.name.lastIndexOf('.') + 1),
        file: f
      };
    })),
    shareReplay()
  );

  constructor(private flashService: FlashService,
              private activeModal: NgbActiveModal) {
  }

  ngOnInit(): void {
  }

  submit(): void {
    const files = this.files$.value;
    if (files.length < 1) {
      this.flashService.error('No file selected');
      return;
    }
    this.uploading$.next(true);
    this.uploadCallback(files).subscribe(() => {
      this.activeModal.close(files);
    }, e => {
      this.uploading$.next(false);
      if (e.status === 0) {
        this.flashService.error('Error opening file. Please close the file if opened in another application.');
      }
    });
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }

  removeFile(file: ExtendedFilePreview): void {
    this.files$.next(this.files$.value.filter(f => f !== file.file));
  }
}

interface ExtendedFilePreview extends FilePreview {
  file: File;
}
