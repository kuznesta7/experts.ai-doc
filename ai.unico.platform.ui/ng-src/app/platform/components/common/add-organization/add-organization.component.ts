import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {BehaviorSubject, combineLatest} from 'rxjs';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {debounceTime, switchMap} from 'rxjs/operators';
import {
  OrganizationAutocompleteService,
  OrganizationForAutocomplete
} from 'ng-src/app/services/organization-autocomplete.service';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';

@Component({
  selector: 'app-add-organization',
  templateUrl: './add-organization.component.html',
})
export class AddOrganizationComponent implements OnInit {

  @Input() organizationDtos: OrganizationBaseDto[];
  @Input() label = 'Organizations';
  @Input() organizationId$?: BehaviorSubject<number | null>;
  @Output() organizationChangeEvent = new EventEmitter<{ organizationId: number, added: boolean }>();
  @Output() organizationsChangeEvent = new EventEmitter<OrganizationBaseDto[]>();

  searchOrganizationFc = new FormControl('');
  organizations$ = new BehaviorSubject<OrganizationBaseDto[]>([]);
  organizationOptions$ = combineLatest([this.searchOrganizationFc.valueChanges, this.organizations$]).pipe(
    debounceTime(300),
    switchMap(([q, organizationDtos]) => {
      const options = {disabledOrganizationIds: organizationDtos.map(o => o.organizationId)};
      return this.organizationAutocompleteService.autocompleteOrganizations(q, options);
    })
  );
  constructor(private organizationAutocompleteService: OrganizationAutocompleteService) { }

  ngOnInit(): void {
    this.organizations$.next(this.organizationDtos);
    this.organizationsChangeEvent.emit(this.organizationDtos);
    this.organizationId$?.subscribe(id => {
      if (id) {
        this.organizationAutocompleteService.getOrganizationsForAutocomplete([id]).subscribe(o => {
          const organizations = this.organizations$.value.concat(o);
          this.organizations$.next(organizations);
          this.organizationsChangeEvent.emit(organizations);
          this.organizationChangeEvent.emit({organizationId: o[0].organizationId, added: true});
        });
      }
    });
  }

  removeOrganization(organization: OrganizationBaseDto): void {
    const organizations = this.organizations$.value.filter(o => o !== organization);
    this.organizations$.next(organizations);
    this.organizationsChangeEvent.emit(organizations);
    this.organizationChangeEvent.emit({organizationId: organization.organizationId, added: false});
  }

  addOrganization(event: MatAutocompleteSelectedEvent): void {
    const organization: OrganizationForAutocomplete = event.option.value;
    this.searchOrganizationFc.reset();
    const organizations = this.organizations$.value.concat(organization);
    this.organizations$.next(organizations);
    this.organizationsChangeEvent.emit(organizations);
    this.organizationChangeEvent.emit({organizationId: organization.organizationId, added: true});
  }

}
