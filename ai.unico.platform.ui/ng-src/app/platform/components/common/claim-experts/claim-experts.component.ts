import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject, iif, Observable, of } from 'rxjs';
import { ExpertAutocompleteService } from '../../../../services/expert-autocomplete.service';
import { FormValidationService } from '../../../../shared/services/form-validation.service';
import { LoginService } from '../../../../services/login.service';
import { UserProfileService } from '../../../../services/user-profile.service';
import { FlashService } from '../../../../shared/services/flash.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { debounceTime, filter, finalize, map, shareReplay, startWith, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from '../../../../shared/operators/tap-all-handle-error-operator';
import { ExpertClaimService } from '../../../../services/expert-claim.service';
import { EvidenceExpertPreview } from '../../../../interfaces/evidence-expert-preview';
import { ExpertClaimPreview } from '../../../../interfaces/expert-claim-preview';

@Component({
  selector: 'app-claim-experts',
  templateUrl: './claim-experts.component.html'
})
export class ClaimExpertsComponent implements OnInit {

  searchFormControl = new FormControl('');
  loading$ = new BehaviorSubject<boolean>(false);
  experts$: Observable<ExtendedExpertClaimPreview  []>;
  organizationId: number;
  expert: EvidenceExpertPreview;

  constructor(private expertAutocompleteService: ExpertAutocompleteService,
              private formValidationService: FormValidationService,
              private registrationService: LoginService,
              private userProfileService: UserProfileService,
              private flashService: FlashService,
              private expertClaimService: ExpertClaimService,
              private ngbActiveModal: NgbActiveModal) {
  }

  ngOnInit(): void {
    this.searchFormControl.setValue(this.expert.name);
    this.experts$ = this.searchFormControl.valueChanges.pipe(
      startWith(this.expert.name),
      debounceTime(300),
      filter(f => !!f),
      tap(() => this.loading$.next(true)),
      switchMap(v => this.expertClaimService.getClaimableExperts(v).pipe(
        tapAllHandleError(() => this.loading$.next(false))
      )),
      map(experts => experts.filter(e => e.expertId !== this.expert.expertId)),
      map(experts => experts.map(e => {
        return {
          ...e,
          saving$: new BehaviorSubject(false),
          claimed$: new BehaviorSubject(e.claimed)
        };
      })),
      shareReplay()
    );
    this.experts$.subscribe(x => console.log(x));
  }

  claimExpert(expert: ExtendedExpertClaimPreview): void {
    if (expert.expertId) {
      expert.saving$.next(true);
      iif(() => this.expert.userId !== null,
        of(this.expert.userId),
        this.expertClaimService.verifyExpert(this.expert.expertId)).subscribe(
        userId => {
          this.expert.userId = userId;
          this.expertClaimService.claimExpertProfile(expert.expertId, userId).pipe(
            finalize(() => expert.saving$.next(false))
          ).subscribe(() => expert.claimed$.next(true));
        }
      );
    }
  }

  close(): void {
    this.ngbActiveModal.dismiss();
  }

}

interface ExtendedExpertClaimPreview extends ExpertClaimPreview {
  saving$: BehaviorSubject<boolean>;
  claimed$: BehaviorSubject<boolean>;
}
