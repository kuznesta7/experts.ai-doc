import { Component, Input, OnInit } from '@angular/core';
import { SearchHistoryDto } from 'ng-src/app/services/search-history.service';
import { QueryParamService } from 'ng-src/app/shared/services/query-param.service';
import { UrlParams } from 'ng-src/app/constants/url-params';

@Component({
  selector: 'app-search-history-row',
  templateUrl: './search-history-row.component.html'
})
export class SearchHistoryRowComponent implements OnInit {

  constructor(private queryParamService: QueryParamService) {
  }

  @Input()
  row: SearchHistoryDto;

  params: any;

  ngOnInit(): void {
    if (!this.row) {
      return;
    }
    this.params = this.queryParamService.createQueryParamsObject({
      [UrlParams.SEARCH_QUERY]: this.row.query,
      organizationIds: this.row.organizationIds,
      countryCodes: this.row.countryCodes,
      expertSearchType: this.row.expertSearchType
    });
  }

}
