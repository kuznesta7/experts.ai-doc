import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { FormControl } from '@angular/forms';
import { EvidenceItemService } from 'ng-src/app/services/evidence-item.service';
import {BehaviorSubject, forkJoin, Observable} from 'rxjs';
import { EvidenceItemPreviewWithExperts } from 'ng-src/app/interfaces/evidence-item-preview-with-experts';
import { debounceTime, finalize, map, shareReplay, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OutcomeDetailEditModalComponent } from 'ng-src/app/platform/components/content/outcome-detail/outcome-detail-edit-modal/outcome-detail-edit-modal.component';
import {parse} from '@retorquere/bibtex-parser';
import {ItemDetailDto} from 'ng-src/app/interfaces/item-detail-dto';
import {FlashService} from 'ng-src/app/shared/services/flash.service';
import {Confidentiality} from 'ng-src/app/interfaces/confidetiality';

@Component({
  selector: 'app-add-outcome-modal',
  templateUrl: './add-outcome-modal.component.html'
})
export class AddOutcomeModalComponent implements OnInit {
  searchFormControl = new FormControl();
  outcomes$: Observable<ExtendedItemPreview[]>;
  loading$ = new BehaviorSubject<boolean>(false);
  importing$ = new BehaviorSubject<boolean>(false);
  added = false;
  organizationId: number;
  userId: number;
  @ViewChild('fileInput', { static: true }) fileInput: ElementRef;
  validExtensions = ['.txt', '.bib'];
  outcome$ = new BehaviorSubject<ItemDetailDto | null>(null);

  doAddOutcome: (outcome: EvidenceItemPreviewWithExperts) => Observable<void>;

  constructor(private evidenceItemService: EvidenceItemService,
              private activeModal: NgbActiveModal,
              private ngbModal: NgbModal,
              private flashService: FlashService) {
  }

  ngOnInit(): void {
    this.outcomes$ = this.searchFormControl.valueChanges.pipe(
      debounceTime(300),
      tap(() => this.loading$.next(true)),
      switchMap(v => this.evidenceItemService.searchForItems({query: v}).pipe(
        tapAllHandleError(() => this.loading$.next(false))
      )),
      map(outcomes => outcomes.map(o => {
        return {
          ...o,
          added$: new BehaviorSubject(o.organizationBaseDtos.some(org => org.organizationId === this.organizationId)),
          loading$: new BehaviorSubject(false)
        };
      })),
      shareReplay()
    );
  }

  close(): void {
    if (this.added) {
      this.activeModal.close();
    } else {
      this.activeModal.dismiss();
    }
  }

  createNew(): void {
    this.activeModal.dismiss();
    const ngbModalRef = this.ngbModal.open(OutcomeDetailEditModalComponent, {size: 'xl', backdrop: 'static'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.organizationId = this.organizationId;
    componentInstance.userId = this.userId;
  }

  onFileImport(): void {
    const fileInput = this.fileInput.nativeElement;
    fileInput.value = ''; // clearing previous file input
    fileInput.click();
    fileInput.onchange = (event: Event) => {
      const file = (<HTMLInputElement>event.target).files?.[0];
      if (!file) return;
      if (!this.validExtensions.some(ext => file.name.endsWith(ext))) {
        this.flashService.error('Invalid file extension');
        return;
      }
      const reader = new FileReader();
      reader.readAsText(file);
      reader.onload = () => {
        this.importing$.next(true);
        const bibtex = reader.result as string;
        const items = this.parseBibtex(bibtex);
        if (items.length === 0) {
          this.flashService.error('No outcomes found in file');
          this.importing$.next(false);
          return;
        }
        this.itemsAreUnique(items).then(isUnique => {
          if (isUnique) {
            const itemObservables = items.map(item => this.selectCreationMethod(item));
            forkJoin(itemObservables).subscribe(
              _ => {
                this.importing$.next(false);
                this.close()
                this.flashService.ok('Outcomes successfully added');
              },
              error => {
                this.importing$.next(false);
                this.flashService.error('Error while adding outcomes');
                console.log('Error:');
                console.log(error);
              }
            );
          } else this.importing$.next(false);
        }).catch(error => {
          this.importing$.next(false);
          this.flashService.error('Error while checking outcomes uniqueness');
          console.log('Error:');
          console.log(error);
        });
      };
    };
  }

  removeLatexFromAbstract(bibtex: string): string {
    let abstractStart = bibtex.indexOf('abstract="');
    if (abstractStart !== -1) {
      let abstractEnd = bibtex.indexOf('"', abstractStart + 10); // start searching after 'abstract="'
      if (abstractEnd !== -1) {
        let abstractField = bibtex.substring(abstractStart, abstractEnd);
        let modifiedAbstractField = abstractField.replace(/\{[^{}]*\}/g, '');
        bibtex = bibtex.substring(0, abstractStart) + modifiedAbstractField + bibtex.substring(abstractEnd);
      }
    }
    return bibtex;
  }

  private parseBibtex(bibtex: string): any[] {
    const parsedBibtex = parse(this.removeLatexFromAbstract(bibtex));
    const outcomes = parsedBibtex.entries.map((entry: any) => {
      return {
        itemName: entry.fields.title.toString(),
        itemDescription: entry.fields.abstract ? entry.fields.abstract.toString() : "No description provided",
        year: entry.fields.year ? parseInt(entry.fields.year) : 2023,
        itemNotRegisteredExperts: entry.fields.author ? entry.fields.author.map((author: string) => {
          const authorSplit = author.split(',');
          if (authorSplit[1] == undefined) {
            return { expertName: authorSplit[0].trim() };
          }
          const authorName = authorSplit[1] + ' ' + authorSplit[0];
          return { expertName: authorName.trim() };
          }) : [],
        itemTypeId: articleTypeToId[entry.type] || 55,
        confidentiality: Confidentiality.PUBLIC,
        keywords: []
      }
    });
    return outcomes.map(outcomeParams => {
      return {...this.outcome$.value, ...outcomeParams};
    })
  }

  private async itemsAreUnique(items: any[]): Promise<boolean> {
    if (this.organizationId) {
      for (let i = 0; i < items.length; i++) {
        const exists = await this.evidenceItemService.organizationHasItem(this.organizationId, items[i].itemName).toPromise();
        if (exists) {
          this.flashService.error('Organization already has an outcome with name ' + items[i].itemName);
          return false;
        }
      }
    } else if (this.userId) {
      for (let i = 0; i < items.length; i++) {
        const exists = await this.evidenceItemService.userHasItem(this.userId, items[i].itemName).toPromise();
        if (exists) {
          this.flashService.error('User already has an outcome with name ' + items[i].itemName);
          return false;
        }
      }
    }
    return true;
  }

  private selectCreationMethod(outcome: any): Observable<number> {
    if (this.organizationId) {
      return this.evidenceItemService.createOrganizationItem(outcome, this.organizationId);
    } else if (this.userId) {
      return this.evidenceItemService.createUserItem(outcome, this.userId);
    } else {
      return this.evidenceItemService.createItem(outcome);
    }
  }

  addOutcome(outcome: ExtendedItemPreview): void {
    outcome.loading$.next(true);
    this.doAddOutcome(outcome).pipe(
      finalize(() => outcome.loading$.next(false))
    ).subscribe(() => outcome.added$.next(true));
  }
}

interface ExtendedItemPreview extends EvidenceItemPreviewWithExperts {
  added$: BehaviorSubject<boolean>;
  loading$: BehaviorSubject<boolean>;
}

// Mapping BibTex types to our item types' ids
interface ArticleTypeToIdMap {
  [key: string]: number;
}

const articleTypeToId: ArticleTypeToIdMap = {
  "inproceedings": 59,
  "article": 10,
};
