import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ExpertAutocompleteService, UserExpertPreviewDto} from 'ng-src/app/services/expert-autocomplete.service';
import {UserExpertBaseDto} from 'ng-src/app/interfaces/user-expert-base-dto';
import {BehaviorSubject, combineLatest} from 'rxjs';
import {FormControl} from '@angular/forms';
import {debounceTime, switchMap, tap} from 'rxjs/operators';
import {NotRegisteredExpertDTO} from "ng-src/app/interfaces/item-not-registered-expert";

@Component({
  selector: 'app-add-experts',
  templateUrl: './add-experts.component.html',
})
export class AddExpertsComponent implements OnInit {

  @Input() userExpertBaseDtos: UserExpertBaseDto[];
  @Input() notRegisteredExperts: NotRegisteredExpertDTO[];

  @Output() expertChangeEvent = new EventEmitter<{ expert: UserExpertBaseDto, added: boolean }>();
  @Output() expertsChangeEvent = new EventEmitter<UserExpertBaseDto[]>();
  @Output() notRegisteredExpertChangeEvent = new EventEmitter<{ expert: NotRegisteredExpertDTO, added: boolean }>();
  @Output() notRegisteredExpertsChangeEvent = new EventEmitter<NotRegisteredExpertDTO[]>();


  experts$ = new BehaviorSubject<UserExpertBaseDto[]>([]);
  notRegisteredExperts$ = new BehaviorSubject<NotRegisteredExpertDTO[]>([]);
  searchExpertFc = new FormControl('');
  expertOptions$ = combineLatest([this.searchExpertFc.valueChanges, this.experts$]).pipe(
    debounceTime(300),
    switchMap(([q, experts]) => {
      const options = {query: q, disallowedExpertCodes: experts.map(o => o.expertCode)};
      return this.expertAutocompleteService.findUserExperts(options);
    })
  );

  editExpertLoading = new BehaviorSubject<boolean>(true);
  editExpertOptions: UserExpertPreviewDto[] = [];
  editExpertFc = new FormControl('');
  editExpertOptions$ = combineLatest([this.editExpertFc.valueChanges, this.experts$]).pipe(
    switchMap(([q, experts]) => {
      this.editExpertLoading.next(true);
      const options = {query: q, disallowedExpertCodes: experts.map(o => o.expertCode)};
      return this.expertAutocompleteService.findUserExperts(options).pipe(
        tap(() => this.editExpertLoading.next(false))
      );
    })
  );

  constructor(private expertAutocompleteService: ExpertAutocompleteService) { }

  ngOnInit(): void {
    this.editExpertOptions$.subscribe(experts => {
      this.editExpertOptions = experts;
    });
    this.experts$.next(this.userExpertBaseDtos);
    this.notRegisteredExperts$.next(this.notRegisteredExperts);
    this.expertsChangeEvent.emit(this.userExpertBaseDtos);
    this.notRegisteredExpertsChangeEvent.emit(this.notRegisteredExperts);
  }

  setChosenExpert(expertName: string) {
    this.editExpertFc.setValue(expertName);
  }

  removeExpert(expert: UserExpertBaseDto): void {
    const experts = this.experts$.value.filter(e => e !== expert);
    this.experts$.next(experts);
    this.expertsChangeEvent.emit(experts);
    this.expertChangeEvent.emit({expert, added: false});
  }

  removeNotRegisteredExpert(expert: NotRegisteredExpertDTO): void {
    const experts = this.notRegisteredExperts$.value.filter(e => e !== expert);
    this.notRegisteredExperts$.next(experts);
    this.notRegisteredExpertsChangeEvent.emit(experts);
    this.notRegisteredExpertChangeEvent.emit({expert, added: false});
  }

  makeExpertActive(expert: UserExpertPreviewDto, notRegisteredExpert: NotRegisteredExpertDTO): void {
    this.removeNotRegisteredExpert(notRegisteredExpert);
    this.addExpert(expert);
  }

  addExpert(expert: UserExpertPreviewDto): void {
    const expertBase: UserExpertBaseDto = {
      expertCode: expert.expertCode,
      userId: expert.userId,
      expertId: expert.expertId,
      name: expert.name,
      organizationIds: expert.organizationDtos.map(o => o.organizationId)
    };
    const experts = this.experts$.value.concat(expertBase);
    this.experts$.next(experts);
    this.expertsChangeEvent.emit(experts);
    this.expertChangeEvent.emit({expert: expertBase, added: true});
    this.searchExpertFc.reset();
  }
}
