import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-file-preview',
  templateUrl: './file-preview.component.html'
})
export class FilePreviewComponent implements OnInit {

  @Input()
  file: FilePreview;

  constructor() {
  }

  ngOnInit(): void {
  }

}

export interface FilePreview {
  fileName: string;
  fileSize: number;
  fileType: string;
  downloadUrl?: string;
  fileDescription?: string;
}
