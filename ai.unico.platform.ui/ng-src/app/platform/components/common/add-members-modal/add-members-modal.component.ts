import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {BehaviorSubject, Observable} from 'rxjs';
import {EvidenceItemService} from 'ng-src/app/services/evidence-item.service';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {debounceTime, finalize, map, shareReplay, switchMap, tap} from 'rxjs/operators';
import {tapAllHandleError} from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {ExploreService} from 'ng-src/app/services/explore.service';
import {OrganizationStructureService} from 'ng-src/app/services/organization-structure.service';

@Component({
  selector: 'app-add-members-modal',
  templateUrl: './add-members-modal.component.html'
})
export class AddMembersModalComponent implements OnInit {

  searchFormControl = new FormControl();
  organizations$: Observable<ExtendedOrganizationPreview[]>;
  members: number[];
  loading$ = new BehaviorSubject<boolean>(false);
  added = false;
  organizationId: number;
  userId: number;
  doAddOrganization: (member: OrganizationBaseDto) => Observable<void>;

  constructor(private evidenceItemService: EvidenceItemService,
              private activeModal: NgbActiveModal,
              private exploreService: ExploreService,
              private organizationStructureService: OrganizationStructureService,
              private ngbModal: NgbModal) {
  }

  ngOnInit(): void {
    this.organizationStructureService.getMemberIds(this.organizationId).subscribe(x => this.members = x);
    this.organizations$ = this.searchFormControl.valueChanges.pipe(
      debounceTime(300),
      tap(() => this.loading$.next(true)),
      switchMap(v => this.exploreService.searchOrganizations({query: v, useSubstituteOrganization: false}, {page: 1, limit: 5}).pipe(
        tapAllHandleError(() => this.loading$.next(false))
      )),
      map(outcomes => outcomes.organizationBaseDtos.map(o => {
        return {
          ...o,
          added$: new BehaviorSubject(this.members.includes(o.organizationId)), // todo hide members
          loading$: new BehaviorSubject(false)
        };
      })),
      shareReplay()
    );
  }

  close(): void {
    if (this.added) {
      this.activeModal.close();
    } else {
      this.activeModal.dismiss();
    }
  }

  addOrganization(member: ExtendedOrganizationPreview): void {
    this.added = true;
    this.members.push(member.organizationId);
    member.loading$.next(true);
    this.doAddOrganization(member).pipe(
      finalize(() => member.loading$.next(false))
    ).subscribe(() => member.added$.next(true));
  }
}

interface ExtendedOrganizationPreview extends OrganizationBaseDto {
  added$: BehaviorSubject<boolean>;
  loading$: BehaviorSubject<boolean>;
}
