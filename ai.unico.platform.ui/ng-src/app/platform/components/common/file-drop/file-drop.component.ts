import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FileSystemFileEntry, NgxFileDropEntry } from 'ngx-file-drop';
import { BehaviorSubject } from 'rxjs';
import { FlashService } from 'ng-src/app/shared/services/flash.service';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-file-drop',
  templateUrl: './file-drop.component.html'
})
export class FileDropComponent implements OnInit {

  @Input()
  multiple = false;

  @Input()
  showPreviews: boolean;

  @Input()
  set fileType(fileType: FileType | null) {
    if (!fileType) {
      this.acceptedFiles$.next('');
      return;
    }
    const acceptedExtensions = this.fileTypeToExtensionMap[(fileType as string)].map(e => '.' + e).join(',');
    this.acceptedFiles$.next(acceptedExtensions);
  }

  @Output()
  fileUrlUpdated = new EventEmitter<string>();

  @Output()
  filesUpdated = new EventEmitter<File[]>();

  acceptedFiles$ = new BehaviorSubject<string>('');
  private files$ = new BehaviorSubject<File[]>([]);
  private fileTypeToExtensionMap: { [key: string]: string[] } = {
    image: ['png', 'jpg', 'jpeg', 'gif'],
    document: ['doc', 'docx', 'pdf']
  };

  constructor(private flashService: FlashService) {
  }

  ngOnInit(): void {
    this.files$.pipe(debounceTime(0)).subscribe(files => this.filesUpdated.emit([...files]));
  }

  dropped(files: NgxFileDropEntry[]): void {
    if (!this.multiple) {
      if (files.length > 1) {
        this.flashService.error('Only one file is required');
        return;
      }
    }
    for (const droppedFile of files) {
      if (droppedFile.fileEntry.isFile && droppedFile.relativePath) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          if (this.multiple) {
            this.files$.next(this.files$.getValue().concat([file]));
          } else {
            this.files$.next([file]);

            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
              this.fileUrlUpdated.emit(reader.result as string);
            };
          }
        });
      }
    }

  }
}

export type FileType = 'image' | 'document';
