import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {WidgetService} from 'ng-src/app/services/widget.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {EvidenceExpertPreview} from 'ng-src/app/interfaces/evidence-expert-preview';
import {ExternalMessageDto, MessageType} from 'ng-src/app/interfaces/external-message';
import {EvidenceItemPreview} from 'ng-src/app/interfaces/evidence-item-preview';
import {PathKey} from 'ng-src/app/shared/services/path.service';
import {ProjectPreviewDto} from 'ng-src/app/interfaces/project-preview-dto';
import {OrganizationDetailService} from 'ng-src/app/services/organization-detail.service';
import {OpportunityPreview} from 'ng-src/app/interfaces/opportunity-preview';
import {GoogleAnalyticsService} from 'ng-src/app/services/google-analytics.service';
import {RecombeeService} from 'ng-src/app/services/recombee.service';
import {GoogleAnalyticsCategories} from 'ng-src/app/enums/google-analytics-categories';
import {InitConfigService} from 'ng-src/app/services/init-config.service';
import {FlashService} from 'ng-src/app/shared/services/flash.service';
import {ActivatedRoute} from '@angular/router';
import {EquipmentPreview} from 'ng-src/app/interfaces/equipment-preview';
import {iif} from 'rxjs';

@Component({
  selector: 'app-contact-modal',
  templateUrl: './contact-modal.component.html',
  styleUrls: []
})
export class ContactModalComponent implements OnInit {

  contactForm: FormGroup;

  contactFormResult = new ExternalMessageDto();

  contactKey: ContactKey;

  title: string;

  submitted = false;

  @Input()
  set contactType(type: PathKey) {
    switch (type) {
      case 'outcomeDetail': {
        this.contactKey = 'requestOutcome';
        this.title = 'Send outcome request to: ' + this.itemPreview?.itemName;
        return;
      }
      case 'widgetOutcomeDetail': {
        this.contactKey = 'requestWidgetOutcome';
        this.title = 'Send outcome request to: ' + this.itemPreview?.itemName;
        return;
      }
      case 'widgetProjectDetail': {
        this.contactKey = 'requestWidgetProject';
        this.title = 'Send project request to: ' + this.project?.name;
        return;
      }
      case 'projectDetail': {
        this.contactKey = 'requestProject';
        this.title = 'Send project request to: ' + this.project?.name;
        return;
      }
      case 'widgetExpertDetail': {
        this.contactKey = 'contactExpertWidget';
        this.title = 'Send contact to: ' + this.expert?.name;
        return;
      }
      case 'expertDetail': {
        this.contactKey = 'contactExpert';
        this.title = 'Send contact to: ' + this.expert?.name;
        return;
      }
      case 'organizationDetail': {
        this.contactKey = 'requestInformation';
        this.title = 'Request for information';
        return;
      }
      case 'login': {
        this.contactKey = 'contactUs';
        this.title = 'Contact Us';
        return;
      }
      case 'widgetOpportunityDetail': {
        this.contactKey = 'applyForOpportunity';
        this.title = 'Apply for opportunity';
        return;
      }
      case 'equipmentDetail':
      case 'widgetEquipmentDetail': {
        this.contactKey = 'requestEquipment';
        this.title = 'Equipment request for ' + this.equipmentPreview?.name;
        return;
      }
    }
  }

  @Input() public expert: EvidenceExpertPreview;

  @Input() public organizationId: number;

  @Input() public itemPreview: EvidenceItemPreview;

  @Input() public project: ProjectPreviewDto;

  @Input() public opportunityPreview: OpportunityPreview;

  @Input() public equipmentPreview: EquipmentPreview;

  siteKey$ = this.initConfigService.getRecaptchaPublicKey();

  constructor(private widgetService: WidgetService,
              private organizationDetailService: OrganizationDetailService,
              private ngbActiveModal: NgbActiveModal,
              private fb: FormBuilder,
              private initConfigService: InitConfigService,
              private flashService: FlashService,
              private route: ActivatedRoute,
              private googleAnalyticsService: GoogleAnalyticsService) {
    this.contactForm = this.fb.group({
      senderEmail: ['', [Validators.email]],
      message: ['', Validators.required],
      recaptcha: ['', Validators.required],
      agreeTerms: [false]
    });
  }

  ngOnInit(): void {
  }

  close(): void {
    this.ngbActiveModal.dismiss();
  }

  constructSendMail(): any {
    if (this.contactKey === 'applyForOpportunity' && this.f.agreeTerms.value === false) {
      this.flashService.error('Please check agree terms box!');
      return;
    } else if (this.contactForm.invalid) {
      this.flashService.error('Please fill all fields!');
      return;
    }
    const recombeeService = new RecombeeService();
    let category = '';
    let id = '';
    let index = 0;
    switch (this.contactKey) {
      case 'requestWidgetOutcome':
      case 'requestOutcome':
        this.constructOutcomeRequest();
        break;
      case 'contactExpert':
      case 'contactExpertWidget':
        category = GoogleAnalyticsCategories.EXPERT;
        id = this.expert.expertCode;
        index = this.expert.index;
        this.constructContactRequest();
        break;
      case 'requestProject':
      case 'requestWidgetProject':
        this.constructProjectRequest();
        break;
      case 'requestInformation':
        this.constructRequestInformation();
        break;
      case 'applyForOpportunity':
        this.constructApplyRequest();
        category = GoogleAnalyticsCategories.OPPORTUNITY;
        id = this.opportunityPreview.opportunityId.toString();
        index = this.opportunityPreview.index;
        break;
      case 'contactUs':
        this.constructContactUs();
        break;
      case 'requestEquipment':
        category = GoogleAnalyticsCategories.EQUIPMENT;
        id = this.equipmentPreview.equipmentCode;
        index = -1;
        this.constructEquipmentRequest();
        break;
    }
    if (category) {
      this.googleAnalyticsService.purchase(id, category, index);
      recombeeService.sendPurchase(id);
    }
  }

  get f(): any {
    return this.contactForm.controls;
  }

  constructOutcomeRequest(): any {
    this.contactFormResult = Object.assign({}, this.contactForm.value);
    this.contactFormResult.organizationId = this.organizationId;
    this.contactFormResult.outcomeName = this.itemPreview?.itemName;
    this.contactFormResult.messageType = 'OUTCOME';
    if (this.contactKey === 'requestOutcome') {
      this.widgetService.sendEmailStandard(this.contactFormResult).subscribe(value => {
        this.ngbActiveModal.dismiss();
      });
    } else if (this.contactKey === 'requestWidgetOutcome') {
      this.widgetService.sendEmailWidget(this.contactFormResult, this.contactForm.value.recaptcha).subscribe(value => {
        this.ngbActiveModal.dismiss();
      });
    }
  }
  constructEquipmentRequest(): any {
   this.constructRequest('EQUIPMENT', this.equipmentPreview?.name, false);
  }

  constructRequest(messageType: MessageType, name: string, standardEmail: boolean): any {
    this.contactFormResult = Object.assign({}, this.contactForm.value);
    this.contactFormResult.organizationId = this.organizationId;
    this.contactFormResult.messageType = messageType;
    this.contactFormResult.name = name;
    iif(() => standardEmail,
      this.widgetService.sendEmailStandard(this.contactFormResult),
      this.widgetService.sendEmailWidget(this.contactFormResult, this.contactForm.value.recaptcha)
    ).subscribe(
      value => this.flashService.ok('Your request has been sent successfully!'),
      error => this.flashService.error('Your request has not been sent successfully!'),
      () => this.ngbActiveModal.dismiss()
    );
  }

  constructProjectRequest(): any {
    this.contactFormResult = Object.assign({}, this.contactForm.value);
    this.contactFormResult.organizationId = this.organizationId;
    this.contactFormResult.projectName = this.project?.name;
    this.contactFormResult.messageType = 'PROJECT';
    if (this.contactKey === 'requestProject') {
      this.widgetService.sendEmailStandard(this.contactFormResult).subscribe(value => {
        this.ngbActiveModal.dismiss();
      });
    } else if (this.contactKey === 'requestWidgetProject') {
      this.widgetService.sendEmailWidget(this.contactFormResult, this.contactForm.value.recaptcha).subscribe(value => {
        this.ngbActiveModal.dismiss();
      });
    }
  }

  constructContactRequest(): any {
    this.contactFormResult = Object.assign({}, this.contactForm.value);
    this.contactFormResult.expertToId = this.expert?.expertId;
    this.contactFormResult.userToId = this.expert?.userId;
    this.contactFormResult.organizationId = this.organizationId;
    this.contactFormResult.content = this.contactFormResult.message;
    this.contactFormResult.messageType = 'EXPERT';
    if (this.contactKey === 'contactExpert') {
      this.organizationDetailService.contactExpert(this.contactFormResult).subscribe(value => {
        this.ngbActiveModal.dismiss();
      });
    } else if (this.contactKey === 'contactExpertWidget') {
      this.widgetService.sendEmailWidget(this.contactFormResult, this.contactForm.value.recaptcha).subscribe(value => {
        this.ngbActiveModal.dismiss();
      });
    }
  }

  constructRequestInformation(): any {
    this.contactFormResult = Object.assign({}, this.contactForm.value);
    this.contactFormResult.organizationId = this.organizationId;
    this.contactFormResult.messageType = 'REQUEST_INFORMATION';
    if (this.contactKey === 'requestInformation') {
      this.widgetService.sendEmailWidget(this.contactFormResult, this.contactForm.value.recaptcha).subscribe(value => {
        this.ngbActiveModal.dismiss();
        console.log('Email is sent', this.contactFormResult);
      });
    }
  }

  private constructApplyRequest(): any {
    this.contactFormResult = Object.assign({}, this.contactForm.value);
    this.contactFormResult.organizationId = this.organizationId;
    this.contactFormResult.messageType = 'APPLY';
    this.contactFormResult.opportunityId = this.opportunityPreview.opportunityId;
    this.contactFormResult.opportunityName = this.opportunityPreview.opportunityName;
    if (this.contactKey === 'applyForOpportunity') {
      this.widgetService.sendEmailWidget(this.contactFormResult, this.contactForm.value.recaptcha).subscribe(value => {
        this.ngbActiveModal.dismiss();
        console.log('Email is sent', this.contactFormResult);
      });
    }
  }

  private constructContactUs(): any {
    this.contactFormResult = Object.assign({}, this.contactForm.value);
    this.contactFormResult.messageType = 'CONTACT';
    if (this.contactKey === 'contactUs') {
      this.widgetService.sendEmailContactUs(this.contactFormResult, this.contactForm.value.recaptcha).subscribe(value => {
        this.ngbActiveModal.dismiss();
        console.log('Email is sent', this.contactFormResult);
      });
    }
  }
}

export type ContactKey =
  'contactExpert'
  | 'contactExpertWidget'
  | 'requestProject'
  | 'requestWidgetProject'
  | 'requestOutcome'
  | 'requestWidgetOutcome'
  | 'requestInformation'
  | 'applyForOpportunity'
  | 'contactUs'
  | 'requestEquipment';
