import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ExternalMessageDto } from 'ng-src/app/interfaces/external-message';
import { WidgetService } from 'ng-src/app/services/widget.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LecturePreview } from 'ng-src/app/interfaces/lecture-preview';

@Component({
  selector: 'app-order-modal',
  templateUrl: './order-modal.component.html'
})
export class OrderModalComponent implements OnInit {

  contactForm: FormGroup;

  contactFormResult = new ExternalMessageDto();

  @Input() public lecture: LecturePreview;

  @Input() public organizationId: number;

  constructor(private widgetService: WidgetService,
              private ngbActiveModal: NgbActiveModal,
              private fb: FormBuilder) {
    this.contactForm = this.fb.group({
      senderEmail: ['', [Validators.required, Validators.email]],
      message: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  close(): void {
    this.ngbActiveModal.dismiss();
  }

  sendContactRequest(): any {
    this.contactFormResult = Object.assign({}, this.contactForm.value);
    this.contactFormResult.lectureCode = this.lecture.lectureCode;
    this.contactFormResult.organizationId = this.organizationId;
    this.contactFormResult.messageType = 'LECTURE';
    this.widgetService.sendEmailStandard(this.contactFormResult).subscribe(value => {
      this.ngbActiveModal.dismiss();
    });
  }

}
