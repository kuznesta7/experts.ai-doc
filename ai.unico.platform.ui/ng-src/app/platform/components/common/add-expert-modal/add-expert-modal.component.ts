import {Component, OnInit, ChangeDetectionStrategy, OnDestroy, EventEmitter, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject, Observable} from 'rxjs';
import {ExpertAutocompleteService, UserExpertPreviewDto} from 'ng-src/app/services/expert-autocomplete.service';
import {debounceTime, filter, finalize, map, shareReplay, switchMap, tap} from 'rxjs/operators';
import {tapAllHandleError} from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import {FormValidationService} from 'ng-src/app/shared/services/form-validation.service';
import {LoginService} from 'ng-src/app/services/login.service';
import {UserProfileService} from 'ng-src/app/services/user-profile.service';
import {ExpertDefinition} from 'ng-src/app/services/expert-organization.service';
import {FlashService} from 'ng-src/app/shared/services/flash.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {MessageBus} from 'ng-src/app/classes/event.class';
import {EventBusService} from 'ng-src/app/services/event-bus.service';

@Component({
  selector: 'app-add-expert-modal',
  templateUrl: './add-expert-modal.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddExpertModalComponent implements OnInit, OnDestroy {

  expertsAdded = false;
  @Output() expertsChanged = new EventEmitter<boolean>();
  searchFormControl = new FormControl('');
  loading$ = new BehaviorSubject<boolean>(false);
  experts$: Observable<ExtendedUserExpertPreview[]>;
  organizationId: number;
  doAddExpert: (expert: ExpertDefinition) => Observable<void>;
  mode$ = new BehaviorSubject<AddExpertMode>(AddExpertMode.SEARCH);
  modes = AddExpertMode;
  createUserFormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', Validators.email)
  });
  creatingUser$ = new BehaviorSubject<boolean>(false);

  constructor(private expertAutocompleteService: ExpertAutocompleteService,
              private formValidationService: FormValidationService,
              private registrationService: LoginService,
              private userProfileService: UserProfileService,
              private flashService: FlashService,
              private ngbActiveModal: NgbActiveModal,
              private eventBusService: EventBusService) {
  }

  ngOnInit(): void {
    this.experts$ = this.searchFormControl.valueChanges.pipe(
      debounceTime(300),
      filter(f => !!f),
      tap(() => this.loading$.next(true)),
      switchMap(v => this.expertAutocompleteService.findUserExperts({query: v}).pipe(
        tapAllHandleError(() => this.loading$.next(false))
      )),
      map(experts => experts.map(e => {
        return {
          ...e,
          saving$: new BehaviorSubject(false),
          added$: new BehaviorSubject(e.organizationDtos.some(o => String(o.organizationId) === String(this.organizationId)))
        };
      })),
      shareReplay()
    );
  }

  ngOnDestroy(): void {
    if (this.expertsAdded) {
      this.eventBusService.publish(
        new MessageBus('addExpertModalDestroyed', true)
      );
    }
  }

  addExpert(expert: ExtendedUserExpertPreview): void {
    expert.added$.next(true);  // for loading button change `added$` to `saving$`
    this.doAddExpert(expert).pipe(
      finalize(() => expert.saving$.next(false))
    ).subscribe(() => {
      expert.added$.next(true);
      this.expertsAdded = true;
      this.expertsChanged.emit(this.expertsAdded);
    });
  }

  close(): void {
    this.ngbActiveModal.dismiss();
  }


  createUser(): void {
    this.formValidationService.validateForm(this.createUserFormGroup);
    const value = this.createUserFormGroup.value;
    let request$: Observable<number>;
    if (value.email) {
      value.invitation = true;
      request$ = this.registrationService.doRegister({
        fullName: value.name,
        invitation: true,
        email: value.email
      }, [], []);
    } else {
      request$ = this.userProfileService.createClaimableProfile({name: value.name});
    }
    this.creatingUser$.next(true);
    request$.pipe(
      switchMap(id => this.doAddExpert({userId: id})),
      finalize(() => this.creatingUser$.next(false))
    ).subscribe(() => {
      this.flashService.ok('Expert has been successfully added to your organization');
      this.createUserFormGroup.reset();
      this.mode$.next(AddExpertMode.SEARCH);
      this.expertsAdded = true;
      this.expertsChanged.emit(this.expertsAdded);
    });
  }
}

interface ExtendedUserExpertPreview extends UserExpertPreviewDto {
  saving$: BehaviorSubject<boolean>;
  added$: BehaviorSubject<boolean>;
}

enum AddExpertMode {
  SEARCH = 'SEARCH',
  CREATE = 'CREATE'
}
