import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject, Observable } from 'rxjs';
import { ProjectPreviewDto } from 'ng-src/app/interfaces/project-preview-dto';
import { EvidenceProjectService } from 'ng-src/app/services/evidence-project.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { debounceTime, finalize, map, shareReplay, switchMap, tap } from 'rxjs/operators';
import { tapAllHandleError } from 'ng-src/app/shared/operators/tap-all-handle-error-operator';
import { ProjectDetailEditModalComponent } from 'ng-src/app/platform/components/content/project-detail/project-detail-edit-modal/project-detail-edit-modal.component';

@Component({
  selector: 'app-add-project-modal',
  templateUrl: './add-project-modal.component.html'
})
export class AddProjectModalComponent implements OnInit {

  searchFormControl = new FormControl();
  projects$: Observable<ExtendedProjectPreview[]>;
  loading$ = new BehaviorSubject<boolean>(false);
  added = false;
  organizationId: number;
  doAddProject: (outcome: ProjectPreviewDto) => Observable<void>;

  constructor(private evidenceProjectService: EvidenceProjectService,
              private activeModal: NgbActiveModal,
              private ngbModal: NgbModal) {
  }

  ngOnInit(): void {
    this.projects$ = this.searchFormControl.valueChanges.pipe(
      debounceTime(300),
      tap(() => this.loading$.next(true)),
      switchMap(v => this.evidenceProjectService.searchForProjects({query: v}).pipe(
        tapAllHandleError(() => this.loading$.next(false))
      )),
      map(projects => projects.map(p => {
        return {
          ...p,
          added$: new BehaviorSubject(p.organizationBaseDtos.some(org => org.organizationId === this.organizationId)),
          loading$: new BehaviorSubject(false)
        };
      })),
      shareReplay()
    );
  }

  close(): void {
    if (this.added) {
      this.activeModal.close();
    } else {
      this.activeModal.dismiss();
    }
  }

  createNew(): void {
    this.activeModal.dismiss();
    const ngbModalRef = this.ngbModal.open(ProjectDetailEditModalComponent, {size: 'xl', backdrop: 'static'});
    const componentInstance = ngbModalRef.componentInstance;
    componentInstance.organizationId$.next(this.organizationId);
  }

  addProject(project: ExtendedProjectPreview): void {
    project.loading$.next(true);
    this.added = true;
    this.doAddProject(project).pipe(
      finalize(() => project.loading$.next(false))
    ).subscribe(() => project.added$.next(true));
  }
}

interface ExtendedProjectPreview extends ProjectPreviewDto {
  added$: BehaviorSubject<boolean>;
  loading$: BehaviorSubject<boolean>;
}
