import {Component} from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {
  OrganizationStructureDto,
  OrganizationStructureService
} from 'ng-src/app/services/organization-structure.service';
import {filter, shareReplay, switchMap} from 'rxjs/operators';
import {SessionService} from 'ng-src/app/services/session.service';
import {UiThemeService} from '../shared/services/ui-theme.service';

@Component({
  selector: 'app-platform',
  templateUrl: './platform.component.html'
})
export class PlatformComponent {
  title = 'platform';

  userOrganizations$: Observable<OrganizationStructureDto[]> = this.sessionService.getCurrentSessionWithUpdates().pipe(
    filter(s => !!s),
    switchMap(s => {
      if (s.organizationRoles.length === 0) {
        return of([]);
      }
      const organizationIds = s.organizationRoles.map(o => o.organizationId);
      return this.organizationStructureService.getStructure(organizationIds, false);
    }),
    shareReplay()
  );
  navigationOpen$ = new BehaviorSubject(true);

  constructor(private sessionService: SessionService,
              private organizationStructureService: OrganizationStructureService,
              private uiThemeService: UiThemeService) {
    this.uiThemeService.loadTheme();
  }

}
