import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';

export interface EvidenceExpertPreview {
  expertCode: string;
  userId: number;
  expertId: number;
  name: string;
  numberOfItems: number;
  numberOfProjects: number;
  numberOfCommercializationProjects: number;
  organizationBaseDtos: OrganizationBaseDto[];
  visibleOrganizationBaseDtos: OrganizationBaseDto[];
  keywords: string[];
  verified: boolean;
  unitVerified: boolean;
  retired: boolean;
  retiredInOrganization: boolean;
  claimable: boolean;
  invitationEmail: string;
  itemTypeGroupCounts: { [itemTypeGroupId: number]: number };
  yearActivityHistogram: { [year: number]: number };
  index: number;
}
