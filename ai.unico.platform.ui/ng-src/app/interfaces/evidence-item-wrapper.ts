import {EvidenceItemPreviewWithExperts} from 'ng-src/app/interfaces/evidence-item-preview-with-experts';
import {EvidenceWrapper} from './evidence-wrapper';

export interface EvidenceItemWrapper extends EvidenceWrapper{
  organizationUnitIds: number[];
  itemPreviewDtos: EvidenceItemPreviewWithExperts[];
}
