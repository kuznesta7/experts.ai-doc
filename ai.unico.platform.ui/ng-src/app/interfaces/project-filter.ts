import {Confidentiality} from 'ng-src/app/interfaces/confidetiality';
import {BaseFilter} from './base-filter';

export interface ProjectFilter extends BaseFilter {
  limit?: number;
  page?: number;
  organizationId?: number;
  yearFrom?: number;
  yearTo?: number;
  organizationIds?: number[];
  confidentiality?: Confidentiality[];
  expertCodes?: string[];
  projectProgramIds?: number[];
  participatingOrganizationIds?: number[];
  restrictOrganization?: boolean;
  verifiedOnly?: boolean;
  notVerifiedOnly?: boolean;
}
