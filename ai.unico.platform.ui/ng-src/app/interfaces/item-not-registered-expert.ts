export interface NotRegisteredExpertDTO {
  id?: number;
  itemId?: number;
  expertName: string;
}
