import { Confidentiality } from 'ng-src/app/interfaces/confidetiality';
import { UserExpertBaseDto } from 'ng-src/app/interfaces/user-expert-base-dto';
import { OrganizationBaseDto } from 'ng-src/app/interfaces/organization-base-dto';

export interface LecturePreview {
  lectureCode: string;
  lectureId: number;
  lectureName: string;
  lectureDescription: string;
  itemTypeId: number;
  year: number;
  confidentiality: Confidentiality;
  itemSpecification: string;
  itemDomain: string;
  itemScienceSpecification: string;
  keywords: string[];
  expertPreviews: UserExpertBaseDto[];
  organizationBaseDtos: OrganizationBaseDto[];
}
