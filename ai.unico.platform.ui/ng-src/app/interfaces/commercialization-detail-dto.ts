import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {UserExpertBaseDto} from 'ng-src/app/interfaces/user-expert-base-dto';
import {CommercializationOrganizationRelation} from 'ng-src/app/services/commercialization-enum.service';

export interface CommercializationDetailDto {
  commercializationProjectId: number;
  name: string;
  executiveSummary: string;
  useCase: string;
  painDescription: string;
  competitiveAdvantage: string;
  technicalPrinciples: string;
  technologyReadinessLevel: number;
  startDate: Date;
  endDate: Date;
  statusDeadline: Date;
  deleted: boolean;
  imgCheckSum: string;
  commercializationPriorityName: string;
  commercializationDomainName: string;
  commercializationStatusId: number;
  commercializationStatusName: string;
  projectOrganizations: OrganizationWithRelationBaseDto[];
  ownerOrganization: OrganizationBaseDto;
  teamMemberDtos: TeamMemberBaseDto[];
  commercializationInvestmentFrom: number;
  commercializationInvestmentTo: number;
  commercializationCategoryPreview: CommercializationCategory[];
  commercializationPriorityId: number;
  commercializationDomainId: number;
  ideaScore: number;
  keywords: string[];
  ownerOrganizationId: number;
  link: string;
}

export interface CommercializationCategory {
  commercializationCategoryId: number;
  commercializationCategoryName: string;
}

export interface TeamMemberBaseDto extends UserExpertBaseDto {
  teamLeader: boolean;
}

export interface OrganizationWithRelationBaseDto extends OrganizationBaseDto {
  relation: CommercializationOrganizationRelation;
}
