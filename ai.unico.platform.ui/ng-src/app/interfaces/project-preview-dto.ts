import { OrganizationBaseDto } from 'ng-src/app/interfaces/organization-base-dto';
import { UserExpertBaseDto } from 'ng-src/app/interfaces/user-expert-base-dto';
import { Confidentiality } from 'ng-src/app/interfaces/confidetiality';

export interface ProjectPreviewDto {
  projectCode: string;
  projectNumber: string;
  projectId: number;
  projectLink: string;
  originalProjectId: number;
  name: string;
  description: string;
  startDate: Date;
  endDate: Date;
  verified: boolean;
  unitVerified: boolean;
  budgetTotal: number;
  organizationBudgetTotal: number;
  keywords: string[];
  projectExperts: UserExpertBaseDto[];
  parentProjectIds: Map<number, string>;
  organizationBaseDtos: OrganizationBaseDto[];
  projectProgramName: string;
  projectProgramId: number;
  confidentiality: Confidentiality;
}
