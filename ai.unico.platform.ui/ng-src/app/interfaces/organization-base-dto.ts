export interface OrganizationBaseDto {
  organizationId: number;
  organizationName: string;
  organizationAbbrev: string;
  gdpr?: boolean;
}
