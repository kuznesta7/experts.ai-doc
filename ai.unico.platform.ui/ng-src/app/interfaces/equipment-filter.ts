import {BaseFilter} from './base-filter';

export interface EquipmentFilter extends BaseFilter {
  organizationId: number;
  organizationIds: number[];
  equipmentDomainId: number;
  equipmentTypeId: number;
  yearFrom: number;
  yearTo: number;
  portableDevice: boolean;
}
