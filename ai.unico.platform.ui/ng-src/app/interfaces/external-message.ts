export class ExternalMessageDto {
  senderEmail?: string;
  message?: string;
  content?: string;
  organizationId?: number;
  expertToId?: number;
  userToId?: number;
  userId?: number;
  expertId?: number;
  outcomeName?: string;
  projectName?: string;
  lectureCode?: string;
  messageType?: MessageType;
  opportunityId?: string;
  opportunityName?: string;
  name?: string;
}

export type MessageType =
  'ORGANIZATION'
  | 'EXPERT'
  | 'LECTURE'
  | 'OUTCOME'
  | 'PROJECT'
  | 'REQUEST_INFORMATION'
  | 'APPLY'
  | 'CONTACT'
  | 'EQUIPMENT';

