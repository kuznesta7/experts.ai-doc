import { ThesisPreview } from 'ng-src/app/interfaces/thesis-preview';

export interface EvidenceThesisWrapper {
  numberOfAllItems: number;
  limit: number;
  page: number;
  organizationId: number;
  organizationUnitIds: number[];
  thesisPreviewDtos: ThesisPreview[];
}
