export class InternalMessageDto {
  senderEmail?: string;
  message?: string;
  organizationId?: number;
  userId?: number;
  expertId?: number;
  outcomeName?: string;
  projectName?: string;
  lectureCode?: string;
  messageType?: MessageType;
}

export type MessageType = 'ORGANIZATION' | 'EXPERT' | 'LECTURE' | 'OUTCOME' | 'PROJECT';

