import {BaseFilter} from './base-filter';

export interface OpportunityFilter extends BaseFilter {
  organizationId: number | null;
  opportunityType: number | null;
  jobType: number | null;
  organizationIds: number | null;
  user: string | null;
  studentHash: string | null;
  includeHidden: boolean;
}
