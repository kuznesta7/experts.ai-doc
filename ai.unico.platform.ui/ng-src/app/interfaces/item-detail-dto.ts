import { Confidentiality } from 'ng-src/app/interfaces/confidetiality';
import { OrganizationBaseDto } from 'ng-src/app/interfaces/organization-base-dto';
import { UserExpertBaseDto } from 'ng-src/app/interfaces/user-expert-base-dto';
import {NotRegisteredExpertDTO} from 'ng-src/app/interfaces/item-not-registered-expert';

export interface ItemDetailDto {
  itemId: number;
  originalItemId: number;
  itemName: string;
  itemDescription: string;
  year: number;
  itemTypeId: number;
  confidentiality: Confidentiality;
  itemOrganizations: OrganizationBaseDto[];
  itemExperts: UserExpertBaseDto[];
  itemNotRegisteredExperts: NotRegisteredExpertDTO[];
  keywords: string[];
  hidden: boolean;
  deleted: boolean;
  translationUnavailable: boolean;
  ownerUserId: number;
  ownerOrganizationId: number;
  ownerOrganizationName: string;
  doi: string;
  itemLink: string;
}
