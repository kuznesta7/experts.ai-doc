export interface RecommendationWrapper {
  recommId: string;
  publicKey: string;
  databaseName: string;
  testGroup: string;
}
