export interface ItemType {
  typeId: number;
  typeTitle: string;
  typeNameEn: string;
  typeCode: string;
  addable: boolean;
  impacted: boolean;
  utilityModel: boolean;
}
