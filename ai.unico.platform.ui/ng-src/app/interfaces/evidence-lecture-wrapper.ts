import {LecturePreview} from 'ng-src/app/interfaces/lecture-preview';
import {EvidenceWrapper} from './evidence-wrapper';

export interface EvidenceLectureWrapper extends EvidenceWrapper {
  organizationUnitIds: number[];
  lecturePreviewDtos: LecturePreview[];
}
