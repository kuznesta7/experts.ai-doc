import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';

export interface EvidenceItemPreview {
  itemCode: string;
  itemId: number;
  originalItemId: number;
  itemBk: string;
  itemName: string;
  itemLink: string;
  doi: string;
  itemDescription: string;
  year: number;
  itemTypeId: number;
  keywords: string[];
  verified: boolean;
  active: boolean;
  unitVerified: boolean;
  ownerOrganizationId: number;
  organizationBaseDtos: OrganizationBaseDto[];
}
