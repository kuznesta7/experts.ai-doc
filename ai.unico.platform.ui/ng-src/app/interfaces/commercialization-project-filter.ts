import { CommercializationRelation } from 'ng-src/app/enums/commercialization-relation';

export interface CommercializationProjectFilter {
  organizationId: number;
  organizationIds: number[];
  ipOwnerOrganizationIds: number[];
  investorOrganizationIds: number[]
  commercializationPartnerOrganizationIds: number[];
  organizationRelations: CommercializationRelation[];
  yearFrom: number;
  yearTo: number;
  budgetFrom: number;
  budgetTo: number;
  ideaScoreFrom: number;
  ideaScoreTo: number;
  commercializationStatusIds: number[];
  commercializationDomainIds: number[];
  commercializationCategoryIds: number[];
  commercializationPriorityIds: number[];
  expertCodes: string[];
  participatingOrganizationIds: number[];
}
