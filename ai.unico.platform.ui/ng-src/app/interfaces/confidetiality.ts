export enum Confidentiality {
  PUBLIC = 'PUBLIC',
  CONFIDENTIAL = 'CONFIDENTIAL',
  SECRET = 'SECRET'
}
