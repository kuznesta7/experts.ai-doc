import {EvidenceItemPreview} from 'ng-src/app/interfaces/evidence-item-preview';
import {UserExpertBaseDto} from 'ng-src/app/interfaces/user-expert-base-dto';

export interface EvidenceItemPreviewWithExperts extends EvidenceItemPreview {
  expertPreviews: UserExpertBaseDto[];
}
