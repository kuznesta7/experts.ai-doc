import {OpportunityPreview} from 'ng-src/app/interfaces/opportunity-preview';
import {RecommendationWrapper} from 'ng-src/app/interfaces/recommendation-wrapper';
import {EvidenceWrapper} from './evidence-wrapper';

export interface OpportunityWrapper extends EvidenceWrapper {
  organizationUnitIds: number[];
  opportunityPreviewDtos: OpportunityPreview[];
  recommendationWrapper: RecommendationWrapper;
}
