import { Confidentiality } from 'ng-src/app/interfaces/confidetiality';
import { UserExpertBaseDto } from 'ng-src/app/interfaces/user-expert-base-dto';
import { OrganizationBaseDto } from 'ng-src/app/interfaces/organization-base-dto';

export interface ThesisPreview {
  thesisCode: string;
  thesisId: number;
  thesisName: string;
  thesisDescription: string;
  thesisTypeId: number;
  year: number;
  confidentiality: Confidentiality;
  assignee: UserExpertBaseDto;
  supervisor: UserExpertBaseDto;
  reviewer: UserExpertBaseDto;
  university: OrganizationBaseDto;
  keywords: string[];
}
