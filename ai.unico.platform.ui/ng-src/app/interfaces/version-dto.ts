export interface VersionDto {
  DB_VERSION: string;
  DWH_VERSION: string;
  LAST_BUILD_DATE: string;
}
