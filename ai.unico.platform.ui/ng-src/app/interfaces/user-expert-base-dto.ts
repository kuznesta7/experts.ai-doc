export interface UserExpertBaseDto {
  expertCode: string;
  userId?: number;
  expertId?: number;
  name: string;
  organizationIds: number[];
}
