import {UserExpertBaseDto} from 'ng-src/app/interfaces/user-expert-base-dto';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';

export interface OpportunityPreview {
  opportunityId: string;
  opportunityName: string;
  opportunityDescription: string;
  opportunityKw: string[];
  opportunitySignupDate: Date;
  opportunityLocation: string;
  opportunityWage: string;
  opportunityTechReq: string;
  opportunityFormReq: string;
  opportunityOtherReq: string;
  opportunityBenefit: string;
  opportunityJobStartDate: Date;
  opportunityExtLink: string;
  opportunityHomeOffice: string;
  opportunityType: number;
  jobTypes: number[];
  expertPreviews: UserExpertBaseDto[];
  organizationBaseDtos: OrganizationBaseDto[];
  index: number;
  hidden: boolean;
}
