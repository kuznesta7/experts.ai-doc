import {BaseFilter} from './base-filter';

export interface OrganizationSearchFilter extends BaseFilter {
  countryCodes?: string[];
  expertSearchType?: 'EXPERTISE' | 'NAME' | null;
  searchId?: string;
  firstOrganizations?: number[];
  organizationId?: number;
  useSubstituteOrganization?: boolean;
}
