import {Confidentiality} from 'ng-src/app/interfaces/confidetiality';
import {UserExpertBaseDto} from 'ng-src/app/interfaces/user-expert-base-dto';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {EvidenceItemPreview} from 'ng-src/app/interfaces/evidence-item-preview';
import {ProjectProgramDto} from '../services/project-program.service';

export interface ProjectDetailDto {
  projectCode: string;
  projectNumber: string;
  projectId: number;
  originalProjectId: number;
  name: string;
  description: string;
  projectLink: string;
  startDate: Date;
  endDate: Date;
  budgetTotal: number;
  ownerOrganizationId: number;
  ownerOrganizationName: string;
  confidentiality: Confidentiality;
  keywords: string[];
  projectExperts: UserExpertBaseDto[];
  parentProjectIds: number[];
  organizationBaseDtos: OrganizationBaseDto[];
  providingOrganizationBaseDtos: OrganizationBaseDto[];
  itemPreviewDtos: EvidenceItemPreview[];
  organizationFinanceMap: { [organizationId: number]: { [year: number]: number } };
  projectProgram: ProjectProgramDto;
}
