export class SubscriptionTypeDto {
  id: number;
  subscriptionGroup: string;
  description: string;
}
