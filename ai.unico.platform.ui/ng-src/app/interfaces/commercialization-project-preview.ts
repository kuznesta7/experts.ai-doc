import {UserExpertBaseDto} from 'ng-src/app/interfaces/user-expert-base-dto';

export interface CommercializationProjectPreview {
  id: number;
  imgCheckSum: string;
  name: string;
  executiveSummary: string;
  domainId: number;
  domainName: string;
  investmentFrom: number;
  investmentTo: number;
  statusId: number;
  statusName: string;
  ideaScore: number;
  readinessLevel: number;
  teamMemberDtos: UserExpertBaseDto[];
  keywords: string[];
  commercializationCategoryPreviews: {
    commercializationCategoryId: number;
    commercializationCategoryName: string;
  }[];
  commercializationPriorityId: number;
  commercializationPriorityName: string;
  startDate: Date;
  endDate: Date;
  intellectualPropertyOwnerOrganizationId: number;
  intellectualPropertyOwnerOrganizationName: string;
  intermediaryOrganizationId: number;
  intermediaryOrganizationName: string;
  link: string;
}
