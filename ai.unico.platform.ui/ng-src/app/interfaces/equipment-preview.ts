import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {UserExpertBaseDto} from './user-expert-base-dto';

export interface EquipmentPreview {
  id: number;
  equipmentCode: string;
  languageCode: string;
  name: string;
  marking: string;
  description: string;
  specialization: string[];
  year: number;
  serviceLife: number;
  portableDevice: boolean;
  domainId: number;
  type: number;
  organizations: OrganizationBaseDto[];
  expertPreviews: UserExpertBaseDto[];
  hidden: boolean;
  deleted?: boolean;
  goodForDescription: string;
  targetGroup: string;
  preparationTime: string;
  equipmentCost: string;
  infrastructureDescription: string;
  durationTime: string;
  trl: string;
}

