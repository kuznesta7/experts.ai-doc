import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {EvidenceWrapper} from './evidence-wrapper';

export interface OrganizationWrapper extends EvidenceWrapper {
  organizationBaseDtos: OrganizationBaseDto[];
}
