import {Confidentiality} from 'ng-src/app/interfaces/confidetiality';
import {BaseFilter} from './base-filter';

export interface EvidenceFilter extends BaseFilter {
  countryCodes?: string[];
  organizationId?: number;
  organizationIds?: number[];
  suborganizationIds?: number[];
  affiliatedOrganizationIds?: number[];
  resultTypeGroupIds?: number[];
  resultTypeIds?: number[];
  expertCodes?: string[];
  participatingOrganizationIds?: number[];
  yearFrom?: number;
  yearTo?: number;
  ontologyDisabled?: boolean;
  verifiedOnly?: boolean;
  notVerifiedOnly?: boolean;
  retired?: boolean;
  restrictOrganization?: boolean;
  organizationRelatedStatistics?: boolean;
  confidentiality?: Confidentiality[];
  expertSearchType?: 'EXPERTISE' | 'NAME' | null;
  searchId?: string;
  user?: string;
  retirementIgnored?: boolean;
  activeOnly?: boolean;
}
