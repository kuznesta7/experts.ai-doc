export interface EvidenceEquipment {
  id: number;
  languageCode: string;
  name: string;
  marking: string;
  description: string;
  specialization: string[];
  year: number;
  serviceLife: number;
  portableDevice: boolean;
  organizationIds: number[];
  type: number;
  hidden: boolean;
  equipmentKws: string[];
  expertCodes: string[];
  goodForDescription: string;
  targetGroup: string;
  preparationTime: string;
  equipmentCost: string;
  infrastructureDescription: string;
  durationTime: string;
  trl: string;
}

export interface EquipmentLangData {
  [language: string]: {
    name: string,
    type: number,
    description: string,
    goodForDescription: string,
    infrastructureDescription: string,
    targetGroup: string,
    preparationTime: string,
    trl: string,
    durationTime: string,
    equipmentCost: string,
  }
}
