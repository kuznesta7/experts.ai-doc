export interface EvidenceOpportunity {
  opportunityCode: string;
  originalId: number;
  opportunityName: string;
  opportunityDescription: string;
  opportunityKw: string[];
  opportunitySignupDate: Date;
  opportunityLocation: string;
  opportunityWage: string;
  opportunityTechReq: string;
  opportunityFormReq: string;
  opportunityOtherReq: string;
  opportunityBenefit: string;
  opportunityJobStartDate: Date;
  opportunityExtLink: string;
  opportunityHomeOffice: string;
  opportunityType: number;
  jobTypes: number[];
  organizationIds: number[];
  expertIds: string[];
  memberOrganizationIds: number[];
}
