import { OrganizationBaseDto } from 'ng-src/app/interfaces/organization-base-dto';

export interface UserRegistrationDto {
  id?: number;
  fullName: string;
  email?: string;
  organizationDtos?: OrganizationBaseDto[];
  countryOfInterestCodes?: string[];
  invitation?: boolean;
  password?: string;
}
