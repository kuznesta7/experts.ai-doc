export class MessageDto {
  senderEmail?: string;
  message?: string;
  organizationId?: number;
}
