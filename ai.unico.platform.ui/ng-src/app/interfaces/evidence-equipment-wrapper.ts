import {EquipmentPreview} from 'ng-src/app/interfaces/equipment-preview';
import {EvidenceWrapper} from './evidence-wrapper';
import {MultilingualDto} from 'ng-src/app/interfaces/multilingual-dto';

export interface EvidenceEquipmentWrapper extends EvidenceWrapper {
  organizationUnitIds: number[];
  multilingualPreviews: MultilingualDto<EquipmentPreview>[];
}
