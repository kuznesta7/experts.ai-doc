import {ProjectPreviewDto} from 'ng-src/app/interfaces/project-preview-dto';
import {EvidenceWrapper} from './evidence-wrapper';

export interface ProjectWrapper extends EvidenceWrapper {
  organizationUnitIds: number[];
  projectPreviewDtos: ProjectPreviewDto[];
}
