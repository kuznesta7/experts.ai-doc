import {RecommendationWrapper} from 'ng-src/app/interfaces/recommendation-wrapper';

export interface EvidenceWrapper {
  numberOfAllItems: number;
  limit: number;
  page: number;
  organizationId: number;
  recommendationWrapper?: RecommendationWrapper;
}
