import {EvidenceExpertPreview} from 'ng-src/app/interfaces/evidence-expert-preview';
import {EvidenceWrapper} from './evidence-wrapper';

export interface EvidenceExpertWrapper extends EvidenceWrapper {
  organizationUnitIds: number[];
  analyticsExpertPreviewDtos: EvidenceExpertPreview[];
}
