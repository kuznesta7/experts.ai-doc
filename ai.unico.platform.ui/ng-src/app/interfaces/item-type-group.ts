export interface ItemTypeGroup {
  itemTypeGroupId: number;
  itemTypeGroupTitle: string;
  itemTypeIds: number[];
}
