import { EvidenceItemPreview } from './evidence-item-preview';
import { OrganizationBaseDto } from './organization-base-dto';

export interface ExpertClaimPreview {
  expertId: number;

  userId: number;

  expertCode: string;

  fullName: string;

  organizationBaseDtos: OrganizationBaseDto[];

  numberOfAllItems: number;

  itemPreviewDtos: EvidenceItemPreview[];

  claimed: boolean;

  claimedByAnotherUser: boolean;

}
