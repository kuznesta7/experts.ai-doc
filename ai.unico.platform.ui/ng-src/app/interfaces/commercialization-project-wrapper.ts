import {CommercializationProjectPreview} from 'ng-src/app/interfaces/commercialization-project-preview';
import {EvidenceWrapper} from './evidence-wrapper';

export interface CommercializationProjectWrapper extends EvidenceWrapper {
  organizationUnitIds: number[];
  evidenceCommercializationDtos: CommercializationProjectPreview[];
}
