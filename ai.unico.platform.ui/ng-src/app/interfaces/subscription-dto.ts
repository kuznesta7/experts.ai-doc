export class SubscriptionDto {
  email: string;
  organizationId: number;
  userToken: string | null;
  subscriptionTypeId: number;
  group?: string;
}
