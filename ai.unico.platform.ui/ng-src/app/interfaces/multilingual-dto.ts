export interface MultilingualDto<T> {
  id: number | null;
  code: string | null;
  preview: T;
  translations: T[];
}
