import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SubscribeComponent} from 'ng-src/app/subscribe/subscribe.component';
import {
  SubscribeSuccessfulComponent
} from 'ng-src/app/subscribe/components/subscribe-successful/subscribe-successful.component';
import {
  AutomaticSubscribeComponent
} from 'ng-src/app/subscribe/components/automatic-subscribe/automatic-subscribe.component';
import {UnsubscribeComponent} from 'ng-src/app/subscribe/components/unsubscribe/unsubscribe.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '', component: SubscribeComponent
      },
      {
        path: 'successful', component: SubscribeSuccessfulComponent
      },
      {
        path: 'automatic', component: AutomaticSubscribeComponent
      },
      {
        path: 'unsubscribe', component: UnsubscribeComponent
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubscribeRoutingModule {
}
