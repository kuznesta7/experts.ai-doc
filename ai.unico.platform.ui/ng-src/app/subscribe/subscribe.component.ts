import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SubscriptionDto} from 'ng-src/app/interfaces/subscription-dto';
import {combineLatest, Observable} from 'rxjs';
import {SubscriptionTypeDto} from 'ng-src/app/interfaces/subscription-type-dto';
import {UiThemeService} from 'ng-src/app/shared/services/ui-theme.service';
import {RecombeeService} from 'ng-src/app/services/recombee.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FlashService} from 'ng-src/app/shared/services/flash.service';
import {FormValidationService} from 'ng-src/app/shared/services/form-validation.service';
import {SubscriptionService} from 'ng-src/app/services/subscription.service';


@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['subscribe.component.scss']
})
export class SubscribeComponent implements OnInit {

    constructor(private route: ActivatedRoute,
                private subscriptionService: SubscriptionService,
                private uiThemeService: UiThemeService,
                private recombeeService: RecombeeService,
                private router: Router,
                private flashService: FlashService,
                private formValidationService: FormValidationService) { }

  static AVAILABLE_MAILS = ['@cvut.cz', '@fit.cvut.cz'];

  subscription: SubscriptionDto = new SubscriptionDto();
  subscriptionTypes$: Observable<SubscriptionTypeDto[]> = this.subscriptionService.getSubscriptionTypes();

  formGroup = new FormGroup({
    subscriptionType: new FormControl(1),
    mail: new FormControl('', [Validators.email, Validators.required]),
  });

  ngOnInit(): void {
    this.uiThemeService.loadTheme();
    this.route.queryParams.subscribe(params => {
      if (params.email !== undefined) {
        this.formGroup.patchValue({mail: params.email});
      } else if (params.username !== undefined) {
        this.formGroup.patchValue({mail: params.username + '@cvut.cz'});  // todo - change in the future
      }
    });

  }

  onSubmit(): void {
    const subscriptionTypeId = this.formGroup.get('subscriptionType')?.value;
    combineLatest([this.subscriptionTypes$, this.route.queryParams]).subscribe(([subscriptionTypes, params]) => {
      this.subscription.organizationId = params.organization;
      this.subscription.email = this.formGroup.get('mail')?.value;
      if (this.recombeeService.isLocalStorageAvailable()) {
        this.subscription.userToken = this.recombeeService.getUserRecombeeToken();
      }
      this.subscription.subscriptionTypeId = subscriptionTypeId;
      const subscriptionType = subscriptionTypes.find((type: SubscriptionTypeDto) => type.id === subscriptionTypeId);
      if (subscriptionType && this.validate() && this.checkEmailAvailability(this.subscription.email, subscriptionTypeId, subscriptionType)) {
        this.subscriptionService.widgetEmailSubscription(this.subscription, '').subscribe(
          (result: any) => this.router.navigate(['/subscribe/successful'], {queryParams: {email: this.subscription.email, token: result.data, organization: this.subscription.organizationId}})
        );
      }
    });
  }

  private validate(): boolean {
    this.formValidationService.validateForm(this.formGroup);
    if (!this.subscription.subscriptionTypeId) {
      this.flashService.warn('Cannot subscribe - please choose a subscription option');
      return false;
    }
    if (this.subscription.email === undefined || this.subscription.organizationId === undefined) {
      this.flashService.warn('Cannot subscribe - missing email or organization');
      return false;
    }
    return true;
  }

  private checkEmailAvailability(email: string, subscriptionTypeId: number, subscriptionType: SubscriptionTypeDto): boolean {
    if (subscriptionTypeId !== 1){
      return true;
    }
    let correctEmail = false;
    SubscribeComponent.AVAILABLE_MAILS.forEach((mail: string): void => {
      if (email.endsWith(mail)) {
        correctEmail = true;
      }
    });
    if (!correctEmail) {
      const emailOptions: string = SubscribeComponent.AVAILABLE_MAILS.join(' or ');
      this.flashService.warn('For subscription group' + subscriptionType.subscriptionGroup + ' you need to use your ' + emailOptions + ' email address');
    }
    return correctEmail;
  }
}
