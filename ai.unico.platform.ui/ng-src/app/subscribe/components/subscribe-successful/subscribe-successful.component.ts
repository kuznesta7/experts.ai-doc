import {Component, OnInit} from '@angular/core';
import {UiThemeService} from 'ng-src/app/shared/services/ui-theme.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-subscribe-successful',
  templateUrl: './subscribe-successful.component.html',
})
export class SubscribeSuccessfulComponent implements OnInit {


  constructor(private uiThemeService: UiThemeService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.uiThemeService.loadTheme();
  }

  unsubscribe(): void {
    this.router.navigate(['/subscribe/unsubscribe'], {queryParams: this.route.snapshot.queryParams});
  }

}
