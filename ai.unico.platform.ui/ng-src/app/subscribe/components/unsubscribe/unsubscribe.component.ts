import {Component, OnInit} from '@angular/core';
import {UiThemeService} from 'ng-src/app/shared/services/ui-theme.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SubscriptionService} from 'ng-src/app/services/subscription.service';
import {FlashService} from 'ng-src/app/shared/services/flash.service';
import {SubscriptionDto} from 'ng-src/app/interfaces/subscription-dto';

@Component({
  selector: 'app-unsubscribe',
  templateUrl: './unsubscribe.component.html',
})
export class UnsubscribeComponent implements OnInit {

  constructor(private themeService: UiThemeService,
              private route: ActivatedRoute,
              private subscriptionService: SubscriptionService,
              private flashService: FlashService,
              private router: Router) { }

  ngOnInit(): void {
    this.themeService.loadTheme();
    this.unsubscribe();
  }

  unsubscribe(): void {
    this.route.queryParams.subscribe(params => {
      if (params.email && params.token && params.organization) {
        const subscription: SubscriptionDto = new SubscriptionDto();
        subscription.email = params.email;
        subscription.organizationId = params.organization;
        subscription.userToken = params.token;
        this.subscriptionService.unsubscribe(params.email, params.token, params.organization)
          .subscribe((result: any) => {
              console.log(result);
              this.flashService.default(result.resultCode + ': ' + result.resultMessage);
            },
            () => this.flashService.error('Unsubscribe failed please contact the support'));
      } else {
        console.log(params.email);
        console.log(params.token);
        console.log(params.organization);
        this.flashService.error('Unsubscribe failed please contact the support');
      }
    });
  }

  resubscribe(): void {
    this.router.navigate(['/subscribe/automatic'], {queryParams: this.route.snapshot.queryParams});
  }

}
