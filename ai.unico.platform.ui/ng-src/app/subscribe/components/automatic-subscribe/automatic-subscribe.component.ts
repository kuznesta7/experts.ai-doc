import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UiThemeService} from 'ng-src/app/shared/services/ui-theme.service';
import {SubscriptionDto} from 'ng-src/app/interfaces/subscription-dto';
import {combineLatest, Observable} from 'rxjs';
import {SubscriptionTypeDto} from 'ng-src/app/interfaces/subscription-type-dto';
import {RecombeeService} from 'ng-src/app/services/recombee.service';
import {SubscriptionService} from 'ng-src/app/services/subscription.service';

@Component({
  selector: 'app-automatic-subscribe',
  templateUrl: './automatic-subscribe.component.html',
})
export class AutomaticSubscribeComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private subscriptionService: SubscriptionService,
              private uiThemeService: UiThemeService,
              private router: Router,
              private recombeeService: RecombeeService) { }

  subscription: SubscriptionDto = new SubscriptionDto();
  subscriptionTypes$: Observable<SubscriptionTypeDto[]> = this.subscriptionService.getSubscriptionTypes();

  ngOnInit(): void {

    this.uiThemeService.loadTheme();

    combineLatest([this.subscriptionTypes$, this.route.queryParams])
      .subscribe(([subscriptionTypes, params]) => {
        this.subscription.organizationId = params.organization;
        this.subscription.email = params.email;
        if (this.subscription.email === undefined && params.username !== undefined) {
          this.subscription.email = params.username + '@cvut.cz';  // todo - change in the future
        }
        this.subscription.subscriptionTypeId = params.subscriptionType;
        if (this.subscription.subscriptionTypeId === undefined && params.group !== undefined) {
          const subscriptionType = subscriptionTypes.find((type: SubscriptionTypeDto) => type.subscriptionGroup === params.group);
          if (subscriptionType !== undefined) {
            this.subscription.subscriptionTypeId = subscriptionType.id;
          }
        }
        if (this.recombeeService.isLocalStorageAvailable()) {
          this.subscription.userToken = this.recombeeService.getUserRecombeeToken();
        }
        this.subscription.group = params.group;
        this.subscriptionService.widgetEmailSubscription(this.subscription, '')
          .subscribe(() => this.router.navigate(['/subscribe/successful'], {queryParams: this.route.snapshot.queryParams}));
      });

  }
}
