import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SubscribeComponent} from 'ng-src/app/subscribe/subscribe.component';
import {SubscribeRoutingModule} from 'ng-src/app/subscribe/subscribe-routing.module';
import {SharedModule} from 'ng-src/app/shared/shared.module';
import {PlatformModule} from 'ng-src/app/platform/platform.module';
import {SubscribeSuccessfulComponent} from './components/subscribe-successful/subscribe-successful.component';
import {MatRadioModule} from '@angular/material/radio';
import {FormsModule} from '@angular/forms';
import {AutomaticSubscribeComponent} from './components/automatic-subscribe/automatic-subscribe.component';
import {UnsubscribeComponent} from './components/unsubscribe/unsubscribe.component';


@NgModule({
  declarations: [SubscribeComponent, SubscribeSuccessfulComponent, AutomaticSubscribeComponent, UnsubscribeComponent],
  bootstrap: [SubscribeComponent],
  imports: [
    CommonModule,
    SubscribeRoutingModule,
    SharedModule,
    MatRadioModule,
    FormsModule,
  ],
  providers: [
    PlatformModule
  ]
})
export class SubscribeModule { }
