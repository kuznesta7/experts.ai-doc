import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { SingleRequestHelperService } from 'ng-src/app/shared/services/single-request-helper.service';
import { OrganizationBaseDto } from 'ng-src/app/interfaces/organization-base-dto';
import { EvidenceItemPreview } from 'ng-src/app/interfaces/evidence-item-preview';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClaimProfileService {

  constructor(private singleRequestHelperService: SingleRequestHelperService,
              private httpClient: HttpClient) {
  }

  findClaimSuggestions(query: string): Observable<ClaimSuggestionProfile[]> {
    const params = createHttpParams({query});
    return this.singleRequestHelperService.get<ClaimSuggestionProfile[]>(environment.restUrl + 'claims', {params});
  }

  claimUserProfile(userId: number): Observable<void> {
    return this.httpClient.put<void>(environment.internalRestUrl + 'claims/users/' + userId, null);
  }

  unclaimUserProfile(userId: number): Observable<void> {
    return this.httpClient.delete<void>(environment.internalRestUrl + 'claims/users/' + userId);
  }

  claimExpertProfile(expertId: number): Observable<void> {
    return this.httpClient.put<void>(environment.internalRestUrl + 'claims/experts/' + expertId, null);
  }

  unclaimExpertProfile(expertId: number): Observable<void> {
    return this.httpClient.delete<void>(environment.internalRestUrl + 'claims/experts/' + expertId);
  }

}

export interface ClaimSuggestionProfile {
  claimed: boolean;
  claimedByAnotherUser: boolean;
  expertCode: string;
  fullName: string;
  expertId: number;
  userId: number;
  organizationBaseDtos: OrganizationBaseDto[];
  itemPreviewDtos: EvidenceItemPreview[];
}

