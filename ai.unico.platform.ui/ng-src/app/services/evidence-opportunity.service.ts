import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EvidenceOpportunity} from 'ng-src/app/interfaces/evidence-opportunity';
import {Observable} from 'rxjs';
import {environment} from 'ng-src/environments/environment';
import {OpportunityFilter} from 'ng-src/app/interfaces/opportunity-filter';
import {OpportunityWrapper} from 'ng-src/app/interfaces/opportunity-wrapper';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {OpportunityPreview} from 'ng-src/app/interfaces/opportunity-preview';

@Injectable({
  providedIn: 'root'
})
export class EvidenceOpportunityService {

  constructor(private httpClient: HttpClient) {
  }

  findOpportunities(organizationId: number, opportunityFilter: OpportunityFilter, pageInfo: PageInfo): Observable<OpportunityWrapper> {
    const params = createHttpParams({organizationId, ...opportunityFilter, ...pageInfo});
    return this.httpClient.get<OpportunityWrapper>(environment.restUrl + '/opportunity', {params});
  }

  getOpportunity(opportunityCode: string): Observable<OpportunityPreview> {
    return this.httpClient.get<OpportunityPreview>(environment.restUrl + '/opportunity/' + opportunityCode);
  }

  createOpportunity(evidenceOpportunity: EvidenceOpportunity): Observable<number> {
    return this.httpClient.post<number>(environment.restUrl + '/opportunity', evidenceOpportunity);
  }

  updateOpportunity(opportunityCode: string, evidenceOpportunity: EvidenceOpportunity): Observable<void> {
    return this.httpClient.put<void>(environment.restUrl + '/opportunity/' + opportunityCode, evidenceOpportunity);
  }

  deleteOpportunity(opportunityCode: string): Observable<void> {
    return this.httpClient.delete<void>(environment.restUrl + 'opportunity/' + opportunityCode);
  }

  registerOpportunity(opportunityCode: string): Observable<void> {
    return this.httpClient.post<void>(environment.restUrl + '/opportunity/' + opportunityCode, {});
  }

  hideOpportunity(opportunityCode: string): Observable<void> {
    return this.httpClient.post<void>(environment.restUrl + 'opportunity/' + opportunityCode + '/hide', {});
  }

  showOpportunity(opportunityCode: string): Observable<void> {
    return this.httpClient.post<void>(environment.restUrl + 'opportunity/' + opportunityCode + '/show', {});
  }
}
