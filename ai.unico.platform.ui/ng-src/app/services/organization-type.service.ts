import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from 'ng-src/environments/environment';
import {SingleRequestHelperService} from 'ng-src/app/shared/services/single-request-helper.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrganizationTypeService {

  constructor(private singleRequestHelperService: SingleRequestHelperService,
              private httpClient: HttpClient) {
  }

  getOrganizationTypes(organizationId: number): Observable<OrganizationTypeWrapper> {
    return this.singleRequestHelperService.get<OrganizationTypeWrapper>(`${environment.restUrl}organizations/${organizationId}/types`);
  }

  getOrganizationsTypes(): Observable<OrganizationTypeDto[]> {
    return this.singleRequestHelperService.get<OrganizationTypeDto[]>(`${environment.restUrl}organizations/types`);
  }

  setOrganizationType(organizationId: number, organizationType: number): Observable<void> {
    return this.httpClient.put<void>(`${environment.internalRestUrl}organizations/${organizationId}/types/${organizationType}`, null);
  }

}

export interface OrganizationTypeWrapper {
  organizationTypeDtos: OrganizationTypeDto[];
  selected: OrganizationTypeDto;
}

export interface OrganizationTypeDto {
  id: number;
  name: string;
  description: string;
}
