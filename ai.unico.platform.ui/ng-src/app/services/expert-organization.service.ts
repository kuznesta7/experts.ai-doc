import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'ng-src/environments/environment';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';

@Injectable({
  providedIn: 'root'
})
export class ExpertOrganizationService {

  constructor(private httpClient: HttpClient) {
  }

  addOrganization(expertDefinition: ExpertDefinition, organizationId: number, verified?: boolean): Observable<void> {
    const params = createHttpParams({verified});
    const expertCode = this.createExpertCode(expertDefinition);
    return this.httpClient.put<void>(environment.internalRestUrl + `users/${expertCode}/organizations/${organizationId}`, null, {params});
  }

  removeOrganization(expertDefinition: ExpertDefinition, organizationId: number): Observable<void> {
    const pathFragment = this.createPathFragment(expertDefinition);
    return this.httpClient.delete<void>(environment.internalRestUrl + `${pathFragment}/organizations/${organizationId}`);
  }

  private createPathFragment(expertDefinition: ExpertDefinition): string {
    return expertDefinition.userId ? `users/${expertDefinition.userId}` : `experts/${expertDefinition.expertId}`;
  }

  private createExpertCode(expertDefinition: ExpertDefinition): string {
    if (expertDefinition.userId) {
      return 'ST' + expertDefinition.userId;
    } else if (expertDefinition.expertId) {
      return 'DWH' + expertDefinition.expertId;
    }
    return '';
  }
}

export interface ExpertDefinition {
  userId?: number;
  expertId?: number;
}
