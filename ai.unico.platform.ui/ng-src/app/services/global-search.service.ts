import { Injectable } from '@angular/core';
import { localStorageGet, localStorageStore } from 'ng-src/app/shared/utils/localStorageUtil';
import { PathService } from 'ng-src/app/shared/services/path.service';

@Injectable({
  providedIn: 'root'
})
export class GlobalSearchService {

  constructor(private pathService: PathService) {
  }

  getLastUsedSearchType(): SearchType {
    return localStorageGet('lastUsedSearchType');
  }

  setLastUsedSearchType(type: SearchType): void {
    localStorageStore('lastUsedSearchType', type);
  }

  getPathForSearchType(type: SearchType): string {
    switch (type) {
      case 'experts':
        return this.pathService.getPathWithReplacedParams('exploreExperts');
      case 'organizations':
        return this.pathService.getPathWithReplacedParams('exploreOrganizations');
      /*      case 'commercialization':
              return this.pathService.getPathWithReplacedParams('exploreCommercialization');*/
      case 'equipment':
        return this.pathService.getPathWithReplacedParams('exploreExperts');
      case 'thesis':
        return this.pathService.getPathWithReplacedParams('exploreThesis');
      case 'lecture':
        return this.pathService.getPathWithReplacedParams('exploreLectures');
    }
    return '';
  }
}

export type SearchType =
  'experts'
  | 'organizations'
  | 'outcomes'
  | /*'commercialization' |*/ 'equipment'
  | 'thesis'
  | 'lecture';
