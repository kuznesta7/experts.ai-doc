import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjectExpertService {

  constructor(private httpClient: HttpClient) {
  }

  addUser(projectId: number, userId: number): Observable<void> {
    return this.httpClient.put<void>(environment.internalRestUrl + `projects/${projectId}/users/${userId}`, null);
  }

  removeUser(projectId: number, userId: number): Observable<void> {
    return this.httpClient.delete<void>(environment.internalRestUrl + `projects/${projectId}/users/${userId}`);
  }

  addExpert(projectId: number, expertId: number): Observable<void> {
    return this.httpClient.put<void>(environment.internalRestUrl + `projects/${projectId}/experts/${expertId}`, null);
  }

  removeExpert(projectId: number, expertId: number): Observable<void> {
    return this.httpClient.delete<void>(environment.internalRestUrl + `projects/${projectId}/experts/${expertId}`);
  }
}
