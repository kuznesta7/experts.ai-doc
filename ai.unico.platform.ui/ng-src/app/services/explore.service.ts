import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SingleRequestHelperService } from 'ng-src/app/shared/services/single-request-helper.service';
import { EvidenceFilter } from 'ng-src/app/interfaces/evidence-filter';
import { EvidenceExpertWrapper } from 'ng-src/app/interfaces/evidence-expert-wrapper';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { environment } from 'ng-src/environments/environment.prod';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { OrganizationSearchFilter } from 'ng-src/app/interfaces/organization-search-filter';
import { OrganizationWrapper } from 'ng-src/app/interfaces/organization-wrapper';
import { EvidenceThesisWrapper } from 'ng-src/app/interfaces/evidence-thesis-wrapper';
import { EvidenceLectureWrapper } from 'ng-src/app/interfaces/evidence-lecture-wrapper';
import { EquipmentFilter } from 'ng-src/app/interfaces/equipment-filter';
import { EvidenceEquipmentWrapper } from 'ng-src/app/interfaces/evidence-equipment-wrapper';

@Injectable({
  providedIn: 'root'
})
export class ExploreService {

  constructor(private singleRequestHelperService: SingleRequestHelperService) {
  }

  searchExperts(filter: EvidenceFilter, pageInfo: PageInfo): Observable<EvidenceExpertWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<EvidenceExpertWrapper>(environment.restUrl + 'search/experts', {params});
  }

  searchOrganizations(filter: OrganizationSearchFilter, pageInfo: PageInfo): Observable<OrganizationWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<OrganizationWrapper>(environment.restUrl + 'search/organizations', {params});
  }

  searchThesis(filter: EvidenceFilter, pageInfo: PageInfo): Observable<EvidenceThesisWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<EvidenceThesisWrapper>(environment.restUrl + 'search/thesis', {params});
  }

  searchLectures(filter: EvidenceFilter, pageInfo: PageInfo): Observable<EvidenceLectureWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<EvidenceLectureWrapper>(environment.restUrl + 'search/lectures', {params});
  }

  searchEquipment(filter: EquipmentFilter, pageInfo: PageInfo): Observable<EvidenceEquipmentWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<EvidenceEquipmentWrapper>(environment.restUrl + 'search/equipment', {params});
  }

  findSuggestions(query: string, limit: number, randomize?: boolean): Observable<SearchSuggestion[]> {
    const params = createHttpParams({query, limit, randomize});
    return this.singleRequestHelperService.get<SearchSuggestion[]>(environment.restUrl + 'search/suggestions', {params});
  }
}

export interface SearchSuggestion {
  searchExpression: string;
  appearance: number;
  totalAppearance: number;
}
