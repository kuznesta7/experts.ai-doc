import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { OrganizationRole } from 'ng-src/app/services/session.service';
import { HttpClient } from '@angular/common/http';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';

@Injectable({
  providedIn: 'root'
})
export class EvidenceSettingsService {

  constructor(private httpClient: HttpClient) {
  }

  findAvailableOrganizationsRoles(organizationId: number): Observable<OrganizationLicencePreviewDto[]> {
    return this.httpClient.get<OrganizationLicencePreviewDto[]>(environment.internalRestUrl + 'organizations/' + organizationId + '/roles');
  }

  addOrganizationRoles(userId: number, organizationId: number, organizationRole: OrganizationRole): Observable<void> {
    const params = createHttpParams({role: organizationRole});
    return this.httpClient.post<void>(environment.internalRestUrl + 'users/' + userId + '/organizations/' + organizationId + '/roles', params);
  }

  removeOrganizationRoles(userId: number, organizationId: number, organizationRole: OrganizationRole): Observable<void> {
    const params = createHttpParams({role: organizationRole});
    return this.httpClient.delete<void>(environment.internalRestUrl + 'users/' + userId + '/organizations/' + organizationId + '/roles', {params: params});
  }

  getExperts(filter: UserFilter, pageInfo: PageInfo): Observable<AdminUsersWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.httpClient.get<AdminUsersWrapper>(environment.internalRestUrl + 'users', {params});
  }

  getAllowedWidgets(organizationId: number): Observable<AllowedWidgets> {
    return this.httpClient.get<AllowedWidgets>(environment.restUrl + 'widgets/organizations/' + organizationId + '/allowed');
  }

  setAllowedWidgets(organizationId: number, widgetChanges: AllowedWidgets): Observable<void> {
    return this.httpClient.post<void>(environment.restUrl + 'widgets/organizations/' + organizationId + '/allowed', widgetChanges);
  }

  getVisibilities(organizationId: number): Observable<PublicProfileVisibilityDto> {
    return this.httpClient.get<PublicProfileVisibilityDto>(environment.restUrl + 'organizations/' + organizationId + '/visibility');
  }

  setVisibilities(organizationId: number, visibilities: PublicProfileVisibilityDto): Observable<void> {
    return this.httpClient.post<void>(environment.restUrl + 'organizations/' + organizationId + '/visibility', visibilities);
  }
}

export interface PublicProfileVisibilityDto {
  organizationId: number;
  visible: boolean;
  keywordsVisible: boolean;
  graphVisible: boolean;
  expertsVisible: boolean;
  outcomesVisible: boolean;
  projectsVisible: boolean;
}

export interface AllowedWidgets {
  ORGANIZATION_OVERVIEW: boolean;
  ORGANIZATION_EXPERTS: boolean;
  ORGANIZATION_PROJECTS: boolean;
  ORGANIZATION_ITEMS: boolean;
  ORGANIZATION_COMMERCIALIZATION: boolean;
  EXPERT_DETAIL: boolean;
}

export interface UserFilter {
  query: string;
}

export interface AdminUsersWrapper {
  numberOfUsers: number;
  limit: number;
  page: number;
  adminUserDtos: AdminUserDto[];
}

export interface AdminUserDto {
  userId: number;
  firstName: string;
  lastName: string;
  email: string;
  username: string;
  registrationDate: Date;
  lastLoginDate: Date;
  loginCount: number;
}


export interface OrganizationUserDto {
  userId: number;
  name: string;
  username: string;
}

export interface OrganizationLicencePreviewDto {
  organizationLicenceId: number;
  validUntil: Date;
  licenceRole: OrganizationRole;
  organizationUserDtos: OrganizationUserDto[];
}
