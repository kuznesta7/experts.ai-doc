import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {Observable} from 'rxjs';
import {ProjectFilter} from 'ng-src/app/interfaces/project-filter';
import {ProjectWrapper} from 'ng-src/app/interfaces/project-wrapper';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';
import {environment} from 'ng-src/environments/environment';
import {shareReplay} from 'rxjs/operators';
import {ProjectDetailDto} from 'ng-src/app/interfaces/project-detail-dto';
import {ProjectPreviewDto} from 'ng-src/app/interfaces/project-preview-dto';

@Injectable({
  providedIn: 'root'
})
export class EvidenceProjectService {

  constructor(private httpClient: HttpClient) {
  }

  getEvidenceProjects(organizationId: number, filter: ProjectFilter, pageInfo: PageInfo): Observable<ProjectWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.httpClient
      .get<ProjectWrapper>(environment.internalRestUrl + 'evidence/organizations/' + organizationId + '/projects', {params})
      .pipe(shareReplay());
  }

  updateProject(project: ProjectDetailDto): Observable<void> {
    return this.httpClient.put<void>(environment.restUrl + `projects/${project.projectId}`, project);
  }

  createProject(project: ProjectDetailDto): Observable<number> {
    return this.httpClient.post<number>(environment.restUrl + 'projects', project);
  }

  createOrganizationProject(project: ProjectDetailDto, organizationId: number): Observable<number> {
    return this.httpClient.post<number>(environment.internalRestUrl + `organizations/${organizationId}/projects`, project);
  }

  getProjectDetail(projectCode: string): Observable<ProjectDetailDto> {
    return this.httpClient.get<ProjectDetailDto>(environment.restUrl + 'projects/' + projectCode);
  }

  searchForProjects(filter: ProjectFilter): Observable<ProjectPreviewDto[]> {
    const params = createHttpParams(filter);
    return this.httpClient.get<ProjectPreviewDto[]>(environment.restUrl + '/projects', {params}).pipe(shareReplay());
  }

  moveProject(originalProjectId: number): Observable<number> {
    return this.httpClient.post<number>(`${environment.restUrl}/projects/register/dwh/${originalProjectId}`, null).pipe(shareReplay());
  }

  getProjectDocuments(projectId: number): Observable<ProjectDocument[]> {
    return this.httpClient.get<ProjectDocument[]>(`${environment.restUrl}/projects/${projectId}/documents`);
  }

  uploadDocument(projectId: number, file: File): Observable<number> {
    const formData: FormData = new FormData();
    formData.append('content', file, file.name);
    const params = createHttpParams({fileName: file.name});
    return this.httpClient.post<number>(`${environment.restUrl}/projects/${projectId}/documents`, formData, {params});
  }

  deleteDocument(projectId: number, documentId: number): Observable<void> {
    return this.httpClient.delete<void>(`${environment.restUrl}/projects/${projectId}/documents/${documentId}`);
  }
}

export interface ProjectDocument {
  projectId: number;
  documentId: number;
  documentName: string;
  documentType: string;
  dateCreated: Date;
  authorUserId: number;
  size: number;
}
