import {Injectable} from '@angular/core';
import {environment} from 'ng-src/environments/environment';
import {Observable} from 'rxjs';
import {SingleRequestHelperService} from 'ng-src/app/shared/services/single-request-helper.service';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';

@Injectable({
  providedIn: 'root'
})
export class OrganizationStatisticsService {

  constructor(private singleRequestHelperService: SingleRequestHelperService) {
  }

  getOutcomeStatistics(organizationId: number): Observable<OutcomeStatistics> {
    return this.singleRequestHelperService.get<OutcomeStatistics>(this.createBaseUrl(organizationId) + 'items');
  }

  getExpertStatistics(organizationId: number): Observable<ExpertStatistics> {
    return this.singleRequestHelperService.get<ExpertStatistics>(this.createBaseUrl(organizationId) + 'experts');
  }

  getProjectStatistics(organizationId: number): Observable<ProjectStatistics> {
    return this.singleRequestHelperService.get<ProjectStatistics>(this.createBaseUrl(organizationId) + 'projects');
  }

  getCommercializationStatistics(organizationId: number): Observable<CommercializationStatistics> {
    return this.singleRequestHelperService.get<CommercializationStatistics>(this.createBaseUrl(organizationId) + 'commercialization');
  }

  getOpportunityStatistics(organizationId: number): Observable<OpportunityStatistics> {
    return this.singleRequestHelperService.get<OpportunityStatistics>(this.createBaseUrl(organizationId) + 'opportunity');
  }

  getServiceEquipmentStatistics(organizationId: number): Observable<BasicStatistics> {
    return this.singleRequestHelperService.get<BasicStatistics>(this.createBaseUrl(organizationId) + 'service');
  }

  getProjectCumulativeStatistics(organizationId: number): Observable<ProjectStatistics> {
    return this.singleRequestHelperService.get<ProjectStatistics>(this.createBaseUrl(organizationId) + 'projects/cumulative');
  }

  getMemberCount(organizationId: number): Observable<MemberStatistics> {
    return this.singleRequestHelperService.get<MemberStatistics>(this.createBaseUrl(organizationId) + 'members');
  }

  getKeywordStatistics(organizationId: number, pageInfo: PageInfo): Observable<KeywordStatisticsWrapper> {
    const params = createHttpParams(pageInfo);
    return this.singleRequestHelperService.get<KeywordStatisticsWrapper>(this.createBaseUrl(organizationId) + 'keywords', {params});
  }

  getExpertSearchStatistics(organizationId: number, pageInfo: PageInfo): Observable<ExpertSearchStatisticsWrapper> {
    const params = createHttpParams(pageInfo);
    return this.singleRequestHelperService.get<ExpertSearchStatisticsWrapper>(this.createBaseUrl(organizationId) + 'experts/searched', {params});
  }

  getExpertVisitationStatistics(organizationId: number, pageInfo: PageInfo): Observable<ExpertVisitationStatisticsWrapper> {
    const params = createHttpParams(pageInfo);
    return this.singleRequestHelperService.get<ExpertVisitationStatisticsWrapper>(this.createBaseUrl(organizationId) + 'experts/visited', {params});
  }

  private createBaseUrl(organizationId: number): string {
    return environment.internalRestUrl + 'evidence/organizations/' + organizationId + '/statistics/';
  }
}

export interface ProjectStatistics {
  numberOfAllProjects: number;
  allProjectsFinancesSum: number;
  yearProjectFinancesSumHistogram: { [year: number]: number };
}

export interface ExpertStatistics {
  numberOfExperts: number;
  numberOfRegisteredExperts: number;
}

export interface OutcomeStatistics {
  numberOfAllItems: number;
  allItemsYearHistogram: { [year: number]: number };
  itemTypeStatistics: ItemTypeGroupStatistics[];
}

export interface CommercializationStatistics {
  numberOfAllProjects: number;
}

export interface OpportunityStatistics {
  numberOfAllOpportunities: number;
}

export interface BasicStatistics {
  numOfAllItems: number;
}


export interface MemberStatistics {
  memberCount: number;
  membershipCount: number;
  allowedMembers: boolean;
}

export interface ItemTypeGroupStatistics {
  itemTypeGroupId: number;
  groupTitle: string;
  numberOfItems: number;
  numberOfImpacted: number;
  numberOfUtilityModels: number;
  itemsYearHistogram: { [year: number]: number };
}

export interface KeywordStatisticsWrapper {
  mostSearchedKeywords: KeywordStatistics[];
  numberOfSearchedKeywords: number;
  page: number;
  limit: number;
}

export interface KeywordStatistics {
  query: string;
  searchCount: number;
  numberOfExpertsShown: number;
  avgPosition: number;
  bestPosition: number;
}

export interface ExpertSearchStatisticsWrapper {
  numberOfProfilesShown: number;
  page: number;
  limit: number;
  mostShownProfiles: ShownProfileStatisticsDto[];
}

export interface ShownProfileStatisticsDto {
  userId: number;
  expertId: number;
  shownCount: number;
  avgPosition: number;
  bestPosition: number;
  fullName: string;
}

export interface ExpertVisitationStatisticsWrapper {
  numberOfProfilesVisited: number;
  page: number;
  limit: number;
  mostVisitedProfiles: ExpertVisitationStatisticsDto[];
}

export interface ExpertVisitationStatisticsDto {
  userId: number;
  expertId: number;
  numberOfVisitations: number;
  fullName: string;
}
