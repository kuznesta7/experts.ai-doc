import { Injectable } from '@angular/core';
import { OrganizationRole, Role, SessionService } from 'ng-src/app/services/session.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  constructor(private sessionService: SessionService) {
  }

  isPortalAdmin(): Observable<boolean> {
    return this.hasOneOfRoles(Role.PORTAL_ADMIN);
  }

  isPortalMaintainer(): Observable<boolean> {
    return this.hasOneOfRoles(Role.PORTAL_ADMIN, Role.PORTAL_MAINTAINER);
  }

  hasOneOfRoles(...roles: Role[]): Observable<boolean> {
    return this.sessionService.getCurrentSession().pipe(
      map(session => {
        if (roles === undefined || roles.length === 0) {
          return true;
        }
        return this.hasSomeRole(roles, session?.roles);
      }));
  }

  hasOrganizationRole(organizationId: number, ...requiredRoles: OrganizationRole[]): Observable<boolean> {
    return this.sessionService.getCurrentSession().pipe(
      map(session => {
        if (session.roles.indexOf(Role.PORTAL_ADMIN) !== -1) {
          return true;
        }
        return session?.organizationRoles?.some(organizationRole => {
          return organizationRole.organizationId * 1 === organizationId * 1 && organizationRole.roles.some(role => {
            return role.valid && (requiredRoles.length === 0 || requiredRoles.some(requiredRole => requiredRole === role.role));
          });
        });
      })
    );
  }

  private hasSomeRole(requiredRoles: Role[] | string[], sessionRoles: Role[]): boolean {
    return requiredRoles.some(required => !!sessionRoles?.find(actual => actual === required));
  }
}
