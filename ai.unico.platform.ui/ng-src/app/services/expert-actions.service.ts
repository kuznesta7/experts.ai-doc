import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';
import {environment} from 'ng-src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ExpertActionsService {
  constructor(private httpClient: HttpClient) {
  }

  setExpertRetiredStatus(
    retiredStatus: boolean,
    userId: number
  ): Observable<void> {
    const params = createHttpParams({retired: retiredStatus});
    return this.httpClient.put<void>(
      environment.restUrl + `profiles/users/${userId}/retired`,
      null,
      {params}
    );
  }

  setExpertOrganizationRetiredStatus(
    retiredStatus: boolean,
    expertCode: string,
    organizationId: number
  ): Observable<void> {
    const params = createHttpParams({retired: retiredStatus});
    return this.httpClient.put<void>(environment.internalRestUrl + `evidence/organizations/${organizationId}/expert/${expertCode}/retired`,
      null,
      {params});
  }

  setExpertVerificationStatus(
    verifyStatus: boolean,
    organizationId: number,
    expertId: number
  ): Observable<void> {
    const params = createHttpParams({
      verify: String(verifyStatus),
      expertIds: expertId,
    });
    return this.httpClient.put<void>(
      environment.internalRestUrl +
      `evidence/organizations/${organizationId}/experts/verified`,
      null,
      {params}
    );
  }

  removeExpertFromOrganization(organizationId: number, expertId: number): Observable<void> {
    const params = createHttpParams({
      expertIds: [expertId],
    });
    return this.httpClient.delete<void>(`${environment.internalRestUrl}evidence/organizations/${organizationId}/experts`, {params});
  }

  removeUserFromOrganization(organizationId: number, userId: number): Observable<void> {
    const params = createHttpParams({
      userIds: [userId],
    });
    return this.httpClient.delete<void>(`${environment.internalRestUrl}evidence/organizations/${organizationId}/users`, {params});
  }

  setUserVerificationStatus(
    verifyStatus: boolean,
    organizationId: number,
    userId: number
  ): Observable<void> {
    const params = createHttpParams({
      verify: String(verifyStatus),
      userIds: userId,
    });
    return this.httpClient.put<void>(
      environment.internalRestUrl +
      `evidence/organizations/${organizationId}/users/verified`,
      null,
      {params}
    );
  }
}
