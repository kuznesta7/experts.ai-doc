import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ItemType} from 'ng-src/app/interfaces/item-type';
import {environment} from 'ng-src/environments/environment';
import {Observable} from 'rxjs';
import {map, shareReplay} from 'rxjs/operators';
import {ItemTypeGroup} from 'ng-src/app/interfaces/item-type-group';

@Injectable({
  providedIn: 'root'
})
export class ItemTypeService {

  constructor(private httpClient: HttpClient) {
  }

  private itemTypes$ = this.httpClient.get<ItemType[]>(environment.restUrl + 'item-types/').pipe(map(x => x.map(y => {
    return {...y, typeNameEn: y.typeTitle};
  })), shareReplay());
  private itemTypeGroups$ = this.httpClient.get<ItemTypeGroup[]>(environment.restUrl + 'item-types/groups/').pipe(shareReplay());

  private dwhItemTypes$ = this.httpClient.get<ItemType[]>(environment.restUrl + 'item-types/dwh').pipe(shareReplay());

  findItemTypes(): Observable<ItemType[]> {
    return this.itemTypes$;
  }

  findDWHItemTypes(): Observable<ItemType[]> {
    return this.dwhItemTypes$;
  }

  findItemTypeGroups(): Observable<ItemTypeGroup[]> {
    return this.itemTypeGroups$;
  }
}
