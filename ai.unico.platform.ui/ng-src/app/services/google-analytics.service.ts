import { Injectable } from '@angular/core';
import { RecombeeService } from 'ng-src/app/services/recombee.service';
import { ActivatedRoute } from '@angular/router';

declare let gtag: any;

@Injectable({
  providedIn: 'root'
})
export class GoogleAnalyticsService {

  constructor(private route: ActivatedRoute) {
  }

  recombeeService: RecombeeService = new RecombeeService();

  public eventEmitter(
    eventName: string,
    eventCategory: string,
    eventAction: string,
    eventLabel: string,
    eventValue: number
  ): void {
    gtag('event', eventName, {
      eventCategory,
      eventLabel,
      eventAction,
      eventValue
    });
  }

  getGroupFromUserToken(token: string | null): string {
    if (token && token.slice(0, -1).endsWith('-type')) {
      return token.slice(-1);
    }
    return 'no-group';
  }

  public getUserData(): { user_id: string | null, recombee_user_id: string | null, ab_testing_group: string, student_hash: string | null } {
    const token: string | null = this.recombeeService.getUserRecombeeToken();
    if (token) {
      return {
        user_id: token,
        recombee_user_id: token,
        ab_testing_group: this.getGroupFromUserToken(token),
        student_hash: this.route.snapshot.queryParams.studentHash
      };
    }
    return {
      user_id: null,
      recombee_user_id: null,
      ab_testing_group: this.getGroupFromUserToken(token),
      student_hash: this.route.snapshot.queryParams.studentHash
    };
  }

  public view(id: string, category: string, index: number): void {
    gtag('event', 'detail_view', {
      event_category: category,
      index,
      ...this.getUserData(),
      item_id: id
    });
  }

  public purchase(id: string, category: string, index: number): void {
    gtag('event', 'purchase', {
      event_category: category,
      index,
      ...this.getUserData(),
      item_id: id
    });
  }

  public purchaseDetailClick(id: string, category: string, index: number): void {
    gtag('event', 'purchase_detail_click', {
      event_category: category,
      index,
      ...this.getUserData(),
      item_id: id
    });
  }

  public receiveRecommendation(category: string, page: number, itemIds: string): void {
    gtag('event', 'recombee_recommendation', {
      event_category: category,
      page,
      ...this.getUserData(),
      item_ids: itemIds
    });
  }

  public search(category: string, searchedText: string): void {
    gtag('event', 'search', {
      event_category: category,
      search_text: searchedText,
      ...this.getUserData()
    });
  }

  public pageChange(category: string, index: number, next: boolean): void {
    let nextPage = -1;
    if (next) {
      nextPage = 1;
    }
    gtag('event', 'page_change', {
      event_category: category,
      next_page: nextPage,
      ...this.getUserData()
    });
  }

  public filter(category: string, field: string, value: number): void {
    gtag('event', 'filter', {
      event_category: category,
      label: field,
      value,
      ...this.getUserData()
    });
  }

  public subscribe(email: string, organizationId: number): void {
    gtag('event', 'subscribe', {
      email,
      organization_id: organizationId,
      ...this.getUserData()
    });
  }
}
