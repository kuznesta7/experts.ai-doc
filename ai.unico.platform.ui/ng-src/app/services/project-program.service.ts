import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProjectProgramService {

  private projectPrograms$ = this.httpClient.get<ProjectProgramDto[]>(environment.internalRestUrl + 'projects/programs').pipe(
    shareReplay()
  );

  constructor(private httpClient: HttpClient) {
  }

  getProjectPrograms(): Observable<ProjectProgramDto[]> {
    return this.projectPrograms$;
  }

}

export interface ProjectProgramDto {
  projectProgramId: number;
  projectProgramName: string;
}

