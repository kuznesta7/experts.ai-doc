import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { SingleRequestHelperService } from 'ng-src/app/shared/services/single-request-helper.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserWorkspaceService {

  constructor(private singleRequestHelperService: SingleRequestHelperService,
              private httpClient: HttpClient) {
  }

  findUserWorkspaces(userId: number): Observable<UserWorkspacePreview[]> {
    return this.singleRequestHelperService.get<UserWorkspacePreview[]>(`${environment.internalRestUrl}users/${userId}/workspaces`);
  }

  deleteWorkspace(userId: number, workspaceId: number): Observable<void> {
    return this.httpClient.delete<void>(`${environment.internalRestUrl}users/${userId}/workspaces/${workspaceId}`);
  }
}

export interface UserWorkspacePreview {
  workspaceId: number;
  userId: number;
  workspaceName: string;
  workspaceNote: string;
}

