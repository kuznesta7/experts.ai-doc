import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { map, shareReplay } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class InitConfigService {

  constructor(private httpClient: HttpClient) {
  }

  private readonly initConfig$ = this.httpClient.get<InitConfig>(environment.restUrl + 'config').pipe(shareReplay());

  getInitConfig(): Observable<InitConfig> {
    return this.initConfig$;
  }

  getRecaptchaPublicKey(): Observable<string> {
    return this.getInitConfig().pipe(map(c => c.recpatchaPublicKey));
  }
}

export interface InitConfig {
  googleAnalyticsCode: string;
  platformVersion: string;
  environment: string;
  recpatchaPublicKey: string;
}
