import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { SingleRequestHelperService } from 'ng-src/app/shared/services/single-request-helper.service';
import { HttpClient } from '@angular/common/http';
import { OrganizationRole } from 'ng-src/app/services/session.service';
import { RestUtilService } from 'ng-src/app/shared/services/rest-util.service';

@Injectable({
  providedIn: 'root'
})
export class OrganizationLicenceService {

  constructor(private singleRequestHelperService: SingleRequestHelperService,
              private httpClient: HttpClient) {
  }

  getLicences(organizationId: number, includeInvalid?: boolean): Observable<OrganizationLicenceDto[]> {
    const params = createHttpParams({validOnly: !includeInvalid});
    return this.singleRequestHelperService.get<OrganizationLicenceDto[]>(`${environment.internalRestUrl}organizations/${organizationId}/licences`, {params});
  }


  organizationHasLicense(organizationId: number): Observable<boolean> {
    return this.singleRequestHelperService.get<boolean>(environment.restUrl + `organizations/${organizationId}/hasLicense`);
  }

  organizationsHasLicenses(organizationIds: number[]): Observable<boolean> {
    const params = createHttpParams({orgIds: organizationIds});
    return this.singleRequestHelperService.get<boolean>(environment.restUrl + `organizations/hasLicense`, {params});
  }

  addLicence(organizationId: number, licenceRole: OrganizationRole, numberOfUserLicences: number, validUntil: Date): Observable<number> {
    const params = createHttpParams({
      licenceRole,
      numberOfUserLicences,
      validUntil: RestUtilService.formatDateForRest(validUntil)
    });
    return this.httpClient.post<number>(`${environment.internalRestUrl}organizations/${organizationId}/licences`, null, {params});
  }

  removeLicence(organizationId: number, licenceId: number): Observable<void> {
    return this.httpClient.delete<void>(`${environment.internalRestUrl}organizations/${organizationId}/licences/${licenceId}`);
  }
}

export interface OrganizationLicenceDto {
  organizationLicenceId: number;
  organizationId: number;
  validUntil: Date;
  licenceRole: OrganizationRole;
  numberOfUserLicences: number;
  valid: boolean;
}
