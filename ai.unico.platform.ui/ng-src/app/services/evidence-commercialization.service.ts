import { Injectable } from '@angular/core';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { Observable } from 'rxjs';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { environment } from 'ng-src/environments/environment';
import { shareReplay } from 'rxjs/operators';
import { CommercializationProjectWrapper } from 'ng-src/app/interfaces/commercialization-project-wrapper';
import { SingleRequestHelperService } from 'ng-src/app/shared/services/single-request-helper.service';
import { CommercializationProjectFilter } from 'ng-src/app/interfaces/commercialization-project-filter';
import { CommercializationDetailDto } from 'ng-src/app/interfaces/commercialization-detail-dto';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class EvidenceCommercializationService {

  constructor(private singleRequestHelper: SingleRequestHelperService,
              private httpClient: HttpClient) {
  }

  getEvidenceCommercialization(organizationId: number, filter: CommercializationProjectFilter, pageInfo: PageInfo): Observable<CommercializationProjectWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelper.get<CommercializationProjectWrapper>(`${environment.internalRestUrl}evidence/organizations/${organizationId}/commercialization`, {params})
      .pipe(shareReplay());
  }

  getCommercializationDetail(commercializationId: number): Observable<CommercializationDetailDto> {
    return this.singleRequestHelper.get<CommercializationDetailDto>(`${environment.internalRestUrl}commercialization/projects/${commercializationId}`)
      .pipe(shareReplay());
  }

  updateCommercialization(commercializationProject: CommercializationDetailDto): Observable<void> {
    return this.httpClient.put<void>(`${environment.internalRestUrl}commercialization/projects/${commercializationProject.commercializationProjectId}`, commercializationProject);
  }

  createCommercialization(commercializationProject: CommercializationDetailDto): Observable<number> {
    return this.httpClient.post<number>(`${environment.internalRestUrl}commercialization/projects`, commercializationProject);
  }

  updateCommercializationImg(commercializationProjectId: number, imgFile: File): Observable<string> {
    const formData: FormData = new FormData();
    formData.append('content', imgFile, imgFile.name);
    const params = createHttpParams({fileName: imgFile.name});
    return this.httpClient.put<string>(environment.internalRestUrl + 'commercialization/projects/' + commercializationProjectId + '/image', formData, {
      params,
      responseType: 'text' as any
    });
  }

  getCommercializationDocuments(commercializationProjectId: number): Observable<CommercializationDocument[]> {
    return this.singleRequestHelper.get<CommercializationDocument[]>(`${environment.internalRestUrl}commercialization/projects/${commercializationProjectId}/documents`);
  }

  deleteCommercializationDocument(commercializationProjectId: number, documentId: number): Observable<void> {
    return this.httpClient.delete<void>(`${environment.internalRestUrl}commercialization/projects/${commercializationProjectId}/documents/${documentId}`);
  }

  addCommercializationDocument(commercializationProjectId: number, file: File): Observable<void> {
    const formData = new FormData();
    formData.append('content', file, file.name);
    const params = createHttpParams({fileName: file.name});
    return this.httpClient.post<void>(`${environment.internalRestUrl}commercialization/projects/${commercializationProjectId}/documents`, formData, {params});
  }
}

export interface CommercializationDocument {
  commercializationId: number;
  documentId: number;
  documentName: string;
  documentType: string;
  dateCreated: Date;
  authorUserId: number;
  size: number;
}
