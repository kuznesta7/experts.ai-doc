import {Injectable} from '@angular/core';
import {EvidenceExpertWrapper} from 'ng-src/app/interfaces/evidence-expert-wrapper';
import {EvidenceExpertPreview} from 'ng-src/app/interfaces/evidence-expert-preview';
import {RecommendationWrapper} from 'ng-src/app/interfaces/recommendation-wrapper';

declare var recombee: any;

@Injectable({
  providedIn: 'root'
})
export class RecombeeService {
  constructor() {
    this.client = null;
  }

  client: any;
  databaseApi: any;
  recombeeTokenName = 'recombeeToken';

  sendDetailView(expertCode: string): void {
    this.cacheClickedExpert(expertCode);
    this.updateLastClickInteraction();
    const recommendationWrapper = this.setClient();
    if (!recommendationWrapper) {
      return;
    }
    if (this.client) {
      this.client.send(new recombee.AddDetailView(this.getUserRecombeeToken(), expertCode, {
        recommId: recommendationWrapper.recommId
      }));
    }
  }

  sendPurchase(expertCode: string): void {
    const recommendationWrapper = this.setClient();
    if (!recommendationWrapper) {
      return;
    }
    if (this.client) {
      this.client.send(new recombee.AddPurchase(this.getUserRecombeeToken(), expertCode, {
        recommId: recommendationWrapper.recommId
      }));
    }
  }

  private setClient(): RecommendationWrapper | undefined {
    const recommendationWrapper = this.getRecommendationWrapper();
    if (recommendationWrapper && recommendationWrapper.publicKey && recommendationWrapper.databaseName) {
      this.client = new recombee.ApiClient(recommendationWrapper.databaseName, recommendationWrapper.publicKey);
      return recommendationWrapper;
    }
    return undefined;
  }

  isLocalStorageAvailable(): boolean {
    try {
      return typeof window.localStorage !== 'undefined';
    } catch (e) {
      return false;
    }
  }

  getUserRecombeeToken(): string {
    if (!this.isLocalStorageAvailable()) {
      return 'no-token';
    }
    let token = localStorage.getItem(this.recombeeTokenName);
    if (token == null) {
      token = this.createAndSaveUserRecombeeToken();
    }
    return token;
  }

  setUserRecombeeToken(token: string): void {
    if (!this.isLocalStorageAvailable()) {
      return;
    }
    localStorage.setItem(this.recombeeTokenName, token);
  }

  generateToken(length: number): string {
    const randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    for (let i = 0; i < length; i++) {
      result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    if (Math.random() < 0.5) {
      result += '-typeA';
    } else {
      result += '-typeB';
    }
    return result;
  }

  private createAndSaveUserRecombeeToken(): string {
    if (!this.isLocalStorageAvailable()) {
      return 'no-token';
    }
    const token = this.generateToken(30);
    localStorage.setItem(this.recombeeTokenName, token);
    return token;
  }

  reloadEvidenceExperts(): boolean {
    const clickInteraction = this.getLastClickInteraction();
    const loadInteraction = this.getLastLoadInteraction();
    this.possiblyDeleteClickedExperts();
    if (clickInteraction == null) {
      return false;
    }
    return loadInteraction == null || clickInteraction > loadInteraction;
  }

  getCachedEvidenceExperts(): EvidenceExpertWrapper | null {
    if (!this.isLocalStorageAvailable()) {
      return null;
    }
    return JSON.parse(localStorage.getItem('expertWrapper') as string);
  }

  setExpertWrapper(expertWrapper: EvidenceExpertWrapper): void {
    if (!this.isLocalStorageAvailable()) {
      return;
    }
    localStorage.setItem('expertWrapper', JSON.stringify(expertWrapper));
  }

  setRecommendationWrapper(recommendationWrapper: RecommendationWrapper): void {
    if (!this.isLocalStorageAvailable()) {
      return;
    }
    localStorage.setItem('recommendationWrapper', JSON.stringify(recommendationWrapper));
    if (recommendationWrapper.testGroup && recommendationWrapper.testGroup.length === 1) {
      let token = this.getUserRecombeeToken();
      token = token.slice(0, -1) + recommendationWrapper.testGroup;
      this.setUserRecombeeToken(token);
    }
  }

  getRecommendationWrapper(): RecommendationWrapper | null {
    if (!this.isLocalStorageAvailable()) {
      return null;
    }
    return JSON.parse(localStorage.getItem('recommendationWrapper') as string);
  }

  getLastLoadInteraction(): string | null {
    if (!this.isLocalStorageAvailable()) {
      return null;
    }
    return JSON.parse(localStorage.getItem('lastLoadInteraction') as string);
  }

  getLastClickInteraction(): string | null {
    if (!this.isLocalStorageAvailable()) {
      return null;
    }
    return JSON.parse(localStorage.getItem('lastClickInteraction') as string);
  }

  updateLastClickInteraction(): void {
    if (!this.isLocalStorageAvailable()) {
      return;
    }
    localStorage.setItem('lastClickInteraction', JSON.stringify(Date.now()));
  }

  updateLastLoadInteraction(): void {
    if (!this.isLocalStorageAvailable()) {
      return;
    }
    localStorage.setItem('lastLoadInteraction', JSON.stringify(Date.now()));
  }

  getClickedExperts(): EvidenceExpertPreview[] | null {
    if (!this.isLocalStorageAvailable()) {
      return null;
    }
    return JSON.parse(localStorage.getItem('clickedExperts') as string);
  }

  pushClickedExpert(expert: EvidenceExpertPreview): void {
    if (!this.isLocalStorageAvailable()) {
      return;
    }
    let experts = this.getClickedExperts();
    if (experts == null) {
      experts = [];
    }
    experts.forEach((item: EvidenceExpertPreview) => {
      if (item.expertCode === expert.expertCode) {
        return;
      }
    });
    experts.push(expert);
    localStorage.setItem('clickedExperts', JSON.stringify(experts));
  }

  possiblyDeleteClickedExperts(): void {
    if (!this.isLocalStorageAvailable()) {
      return;
    }
    const clickInteraction = this.getLastClickInteraction();
    if (clickInteraction !== null && Date.now() - parseInt(clickInteraction, 10) < 1000 * 60 * 15) {
      return;
    }
    localStorage.removeItem('clickedExperts');
  }

  cacheClickedExpert(expertCode: string): void {
    const experts = this.getCachedEvidenceExperts();
    let foundExpert = null;
    if (experts == null) {
      return;
    }
    experts.analyticsExpertPreviewDtos.forEach((expert: EvidenceExpertPreview) => {
      if (expertCode === expert.expertCode) {
        foundExpert = expert;
        this.pushClickedExpert(expert);
        return;
      }
    });
  }
}
