import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'ng-src/environments/environment';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { IdDescriptionOption } from 'ng-src/app/shared/interfaces/idDescriptionOption';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private httpClient: HttpClient) {
  }

  private countries$ = this.httpClient.get<CountryDto[]>(environment.restUrl + 'countries').pipe(shareReplay());

  findCountries(): Observable<CountryDto[]> {
    return this.countries$;
  }

  findCountriesStandardized(): Observable<IdDescriptionOption[]> {
    return this.findCountries().pipe(map(countries => countries.map(c => ({
      id: c.countryCode,
      description: c.countryName
    }))));
  }
}

export interface CountryDto {
  countryCode: string;
  countryName: string;
  countryAbbrev: string;
}
