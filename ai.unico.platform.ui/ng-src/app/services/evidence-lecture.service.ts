import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { LecturePreview } from 'ng-src/app/interfaces/lecture-preview';

@Injectable({
  providedIn: 'root'
})
export class EvidenceLectureService {

  constructor(private httpClient: HttpClient) {
  }

  getLectureDetail(itemCode: string): Observable<LecturePreview> {
    return this.httpClient.get<LecturePreview>(environment.restUrl + `lecture/${itemCode}`);
  }
}
