import { Injectable } from '@angular/core';
import { SingleRequestHelperService } from '../shared/services/single-request-helper.service';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { createHttpParams } from '../shared/utils/httpParamsUtil';
import { HttpClient } from '@angular/common/http';
import { ExpertClaimPreview } from '../interfaces/expert-claim-preview';

@Injectable({
  providedIn: 'root'
})
export class ExpertClaimService {

  constructor(private singleRequestHelperService: SingleRequestHelperService,
              private httpClient: HttpClient) {
  }

  public getClaimableExperts(query: string): Observable<ExpertClaimPreview[]> {
    const params = createHttpParams({query});
    return this.singleRequestHelperService.get<ExpertClaimPreview[]>(environment.restUrl + 'claims', {params});
  }

  public claimExpertProfile(claimedExpertId: number, claimingUserId: number): Observable<void> {
    const params = createHttpParams({claimingUserId});
    return this.httpClient.put<void>(environment.internalRestUrl + 'claims/experts/' + claimedExpertId, {}, {params});
  }

  public verifyExpert(expertId: number): Observable<number> {
    return this.httpClient.patch<number>(environment.internalRestUrl + 'experts/' + expertId, {});
  }
}
