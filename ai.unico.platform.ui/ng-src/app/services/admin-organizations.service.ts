import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from 'ng-src/environments/environment';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';
import {SingleRequestHelperService} from 'ng-src/app/shared/services/single-request-helper.service';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {HttpClient} from '@angular/common/http';
import {OrganizationRole} from 'ng-src/app/services/session.service';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';

@Injectable({
  providedIn: 'root'
})
export class AdminOrganizationsService {

  constructor(private singleRequestHelperService: SingleRequestHelperService,
              private httpClient: HttpClient) {
  }

  loadOrganizations(filter: AdminOrganizationFilter, pageInfo: PageInfo): Observable<AdminOrganizationWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<AdminOrganizationWrapper>(environment.internalRestUrl + 'organizations', {params});
  }

  updateOrganization(organization: AdminOrganizationUpdateDto): Observable<void> {
    return this.httpClient.put<void>(`${environment.internalRestUrl}organizations`, organization);
  }

  createOrganization(organization: AdminOrganizationUpdateDto): Observable<number> {
    return this.httpClient.post<number>(`${environment.internalRestUrl}organizations`, organization);
  }

  replaceOrganization(organizationToReplaceId: number, substituteOrganizationId: number): Observable<void> {
    return this.httpClient.post<void>(`${environment.internalRestUrl}organizations/${substituteOrganizationId}/replaced/${organizationToReplaceId}`, null);
  }

  setOrganizationSearch(organizationId: number, allow: boolean): Observable<void> {
    return this.httpClient.post<void>(`${environment.internalRestUrl}organizations/${organizationId}/search/${allow}`, null);
  }

  setOrganizationMembers(organizationId: number, allow: boolean): Observable<void> {
    return this.httpClient.post<void>(`${environment.internalRestUrl}organizations/${organizationId}/member/${allow}`, null);
  }

  setSearchableOrganization(organizationId: number, allow: boolean): Observable<void> {
    return this.httpClient.post<void>(`${environment.internalRestUrl}organizations/${organizationId}/searchable/${allow}`, null);
  }
}

export interface AdminOrganizationFilter {
  query: string;
  exactMatch: boolean;
  invalidLicencesOnly: boolean;
  activeLicencesOnly: boolean;
  onlyWithoutCountry: boolean;
  replacementState: OrganizationReplacementState;
}

export interface AdminOrganizationWrapper {
  numberOfAllItems: number;
  limit: number;
  page: number;
  adminOrganizationDtos: AdminOrganizationDto[];
}

export interface AdminOrganizationDto {
  organizationId: number;
  organizationName: string;
  organizationAbbrev: string;
  rootOrganizations: OrganizationBaseDto[];
  upperOrganizations: OrganizationBaseDto[];
  lowerOrganizations: OrganizationBaseDto[];
  affiliatedOrganizations: OrganizationBaseDto[];
  replacedOrganizations: OrganizationBaseDto[];
  substituteOrganizationId: number;
  substituteOrganizationName: string;
  substituteOrganizationAbbrev: string;
  rank: number;
  countryCode: string;
  organizationLicencePreviewDtos: OrganizationLicencePreviewDto[];
  hasValidLicence: boolean;
  hasAllowedSearch: boolean;
  membersAllowed: boolean;
}

export interface AdminOrganizationUpdateDto {
  organizationId: number;
  organizationName: string;
  countryCode: string;
  organizationAbbrev: string;
  replacedOrganizationIds: number[];
}

export interface OrganizationLicencePreviewDto {
  organizationLicenceId: number;
  validUntil: Date;
  licenceRole: OrganizationRole;
}

export type OrganizationReplacementState = 'ALL' | 'VALID' | 'REPLACED';
