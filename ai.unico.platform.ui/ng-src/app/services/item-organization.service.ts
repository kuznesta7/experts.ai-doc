import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'ng-src/environments/environment';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';

@Injectable({
  providedIn: 'root'
})
export class ItemOrganizationService {

  constructor(private httpClient: HttpClient) {
  }

  addOrganization(itemId: number, organizationId: number, verify?: boolean): Observable<void> {
    const params = createHttpParams({verify});
    return this.httpClient.put<void>(environment.internalRestUrl + `items/${itemId}/organizations/${organizationId}`, null, {params});
  }

  removeOrganization(itemId: number, organizationId: number): Observable<void> {
    return this.httpClient.delete<void>(environment.internalRestUrl + `items/${itemId}/organizations/${organizationId}`);
  }

  verifyItemForOrganization(itemIds: number[], organizationId: number, verify: boolean): Observable<void> {
    const params = createHttpParams({itemIds, verify});
    return this.httpClient.put<void>(environment.internalRestUrl + `organizations/${organizationId}/items`, null, {params});
  }

  activateItemForOrganization(itemIds: number[], organizationId: number, activate: boolean): Observable<void> {
    const params = createHttpParams({itemIds, active: activate});
    return this.httpClient.put<void>(environment.internalRestUrl + `organizations/${organizationId}/items/activate`, null, {params});
  }
}
