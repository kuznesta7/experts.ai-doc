import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from 'ng-src/environments/environment';
import {tap} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {Session, SessionService} from 'ng-src/app/services/session.service';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';
import {UserRegistrationDto} from 'ng-src/app/interfaces/user-registration-dto';
import {PathFragment} from 'ng-src/app/constants/path-fragment';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient,
              private sessionService: SessionService,
              private router: Router,
  ) {
  }

  private sidebarOpen$ = new BehaviorSubject<SidebarMode | null>(null);
  private _preventSidebarClose = false;
  paths = PathFragment;


  set preventSidebarClose(value: boolean) {
    this._preventSidebarClose = value;
  }

  openLoginSidebar(mode: SidebarMode): void {
    this.sidebarOpen$.next(mode);
    this.sessionService.getCurrentSession().subscribe(session => {
      if (session) {
        this.sidebarOpen$.next(null);
      }
    });
  }

  closeLoginSidebar(): void {
    if (! this._preventSidebarClose) {
      this.sidebarOpen$.next(null);
    }
  }

  forceCloseLoginSidebar(): void {
    this.sidebarOpen$.next(null);
  }

  getLoginSidebarState(): Observable<SidebarMode | null> {
    return this.sidebarOpen$.asObservable();
  }

  doLogin(username: string, password: string, doNotReload?: boolean): Observable<void> {
    return this.httpClient.post<void>(environment.restUrl + 'login', {username, password}).pipe(
      tap(() => {
        this.sessionService.updateSession();
        if (!doNotReload) {
          window.location.reload();
        }
      })
    );
  }

  doRegister(userRegistration: UserRegistrationDto, userIdsToClaim: number[], expertIdsToClaim: number[]): Observable<number> {
    const params = createHttpParams({userIdsToClaim, expertIdsToClaim});
    return this.httpClient.post<number>(environment.restUrl + 'users', userRegistration, {params});
  }

  doLogout(): Observable<void> {
    return this.httpClient.delete<void>(environment.restUrl + 'session').pipe(
      tap(() => {
        this.sessionService.updateSession();
        window.location.reload();
      })
    );
  }

  redirectOnLoginSession(): void {
    this.sessionService.getCurrentSession().subscribe(session => {
      if (session) {
        this.redirectBasedOnSession(session);
      }
    });
  }

  redirectBasedOnSession(session: Session): void {
    if (session) {
      if (session.organizationRoles.length > 0) {
        this.router.navigate(['/', this.paths.EVIDENCE, this.paths.ORGANIZATIONS, session.organizationRoles[0].organizationId]);
      } else if (session.expertCode) {
        this.router.navigate(['/', this.paths.EXPERTS, session.expertCode]);
      } else {
        this.router.navigate(['/', this.paths.EVIDENCE]);
      }
      this.closeLoginSidebar();
    }
  }

  applyForResetPassword(email: string): Observable<void> {
    return this.httpClient.post<void>(environment.restUrl + 'password/new', {email});
  }
}

export enum SidebarMode {
  LOG_IN = 'LOG_IN', REGISTER = 'REGISTER', RESET_PASSWORD = 'RESET_PASSWORD'
}

