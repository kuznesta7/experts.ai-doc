import {Injectable} from '@angular/core';
import {combineLatest, Observable} from 'rxjs';
import {environment} from 'ng-src/environments/environment';
import {debounceTime, map, switchMap} from 'rxjs/operators';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';
import {SingleRequestHelperService} from 'ng-src/app/shared/services/single-request-helper.service';
import {IdDescriptionOption} from 'ng-src/app/shared/interfaces/idDescriptionOption';
import {FormControl} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class OrganizationAutocompleteService {

  constructor(private singleRequestHelperService: SingleRequestHelperService) {
  }

  autocompleteOrganizations(query: string, options?: OrganizationAutocompleteOptions): Observable<OrganizationForAutocomplete[]> {
    const params = createHttpParams({...options, query});
    return this.singleRequestHelperService.get<OrganizationForAutocomplete[]>(environment.restUrl + 'organizations/autocomplete', {params});
  }

  autocompleteOrganizationSuborganizationsStandardized(query: string, organizationId: number,  options?: OrganizationAutocompleteOptions): Observable<IdDescriptionOption[]> {
    const params = createHttpParams({...options, query});
    return this.singleRequestHelperService.get<OrganizationForAutocomplete[]>(environment.restUrl + 'organizations/' + organizationId + '/autocomplete/suborganizations', {params}).pipe(
      map(organizations => organizations.map(o => this.convert(o)))
    );
  }

  autocompleteOrganizationOpportunityStandardized(query: string, organizationId: number,  options?: OrganizationAutocompleteOptions): Observable<IdDescriptionOption[]> {
    const params = createHttpParams({...options, query});
    return this.singleRequestHelperService.get<OrganizationForAutocomplete[]>(environment.restUrl + 'organizations/' + organizationId + '/autocomplete/opportunity', {params}).pipe(
      map(organizations => organizations.map(o => this.convert(o)))
    );
  }
  autocompleteOrganizationOpportunity(query: string, organizationId: number, options?: OrganizationAutocompleteOptions): Observable<OrganizationForAutocomplete[]> {
    const params = createHttpParams({...options, query});
    return this.singleRequestHelperService.get<OrganizationForAutocomplete[]>(environment.restUrl + 'organizations/' + organizationId + '/autocomplete/opportunity', {params});
  }

  autocompleteOrganizationProjectStandardized(query: string, organizationId: number,  options?: OrganizationAutocompleteOptions): Observable<IdDescriptionOption[]> {
    const params = createHttpParams({...options, query});
    return this.singleRequestHelperService.get<OrganizationForAutocomplete[]>(environment.restUrl + 'organizations/' + organizationId + '/autocomplete/project', {params}).pipe(
      map(organizations => organizations.map(o => this.convert(o)))
    );
  }

  autocompleteOrganizationOutcomesStandardized(query: string, organizationId: number,  options?: OrganizationAutocompleteOptions): Observable<IdDescriptionOption[]> {
    const params = createHttpParams({...options, query});
    return this.singleRequestHelperService.get<OrganizationForAutocomplete[]>(environment.restUrl + 'organizations/' + organizationId + '/autocomplete/outcomes', {params}).pipe(
      map(organizations => organizations.map(o => this.convert(o)))
    );
  }

  autocompleteOrganizationsStandardized(query: string, options?: OrganizationAutocompleteOptions): Observable<IdDescriptionOption[]> {
    return this.autocompleteOrganizations(query).pipe(
      map(organizations => organizations.map(o => this.convert(o)))
    );
  }

  getOrganizationsForAutocomplete(organizationIds: number[]): Observable<OrganizationForAutocomplete[]> {
    const params = createHttpParams({organizationIds});
    return this.singleRequestHelperService.get<OrganizationForAutocomplete[]>(environment.restUrl + 'organizations', {params});
  }

  getOrganizationsForAutocompleteStandardized(organizationIds: number[]): Observable<IdDescriptionOption[]> {
    return this.getOrganizationsForAutocomplete(organizationIds).pipe(
      map(organizations => organizations.map(o => this.convert(o)))
    );
  }

  getSuborganizationsForAutocomplete(organizationIds: number[]): Observable<OrganizationForAutocomplete[]> {
    const params = createHttpParams({organizationIds});
    return this.singleRequestHelperService.get<OrganizationForAutocomplete[]>(environment.restUrl + 'organizations/suborganizations', {params});
  }

  getSuborganizationsForAutocompleteStandardized(organizationIds: number[]): Observable<IdDescriptionOption[]> {
    return this.getSuborganizationsForAutocomplete(organizationIds).pipe(
      map(organizations => organizations.map(o => this.convert(o)))
    );
  }

  createSuggestionsObservable(searchFc: FormControl, actualOrganizations$: Observable<{ organizationId: number }[]>): Observable<OrganizationForAutocomplete[]> {
    return combineLatest([searchFc.valueChanges.pipe(debounceTime(300)), actualOrganizations$]).pipe(
      switchMap(([q, organizationDtos]) => {
        const options = {disabledOrganizationIds: organizationDtos.map(o => o.organizationId)};
        return this.autocompleteOrganizations(q, options);
      })
    );
  }

  private convert(organization: OrganizationForAutocomplete): IdDescriptionOption {
    return {
      id: organization.organizationId,
      description: organization.organizationAbbrev || organization.organizationName,
      subDescription: organization.organizationAbbrev ? organization.organizationName : '',
      descriptionLong: organization.organizationName
    };
  }
}

export interface OrganizationForAutocomplete {
  organizationId: number;
  organizationName: string;
  organizationAbbrev: string;
}

export interface OrganizationAutocompleteOptions {
  disabledOrganizationIds?: number[];
}
