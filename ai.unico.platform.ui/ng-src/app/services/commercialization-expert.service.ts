import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { environment } from 'ng-src/environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class CommercializationExpertService {

  constructor(private httpClient: HttpClient) {
  }

  addTeamMember(commercializationProjectId: number, definition: { userId?: number, expertId?: number, teamLeader?: boolean }): Observable<void> {
    const params = createHttpParams(definition);
    return this.httpClient.put<void>(`${environment.internalRestUrl}commercialization/projects/${commercializationProjectId}/team-members`, null, {params});
  }

  removeTeamMember(commercializationProjectId: number, definition: { userId?: number, expertId?: number }): Observable<void> {
    const params = createHttpParams(definition);
    return this.httpClient.delete<void>(`${environment.internalRestUrl}commercialization/projects/${commercializationProjectId}/team-members`, {params});
  }
}
