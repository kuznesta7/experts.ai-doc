import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { shareReplay } from 'rxjs/operators';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { EvidenceExpertWrapper } from 'ng-src/app/interfaces/evidence-expert-wrapper';
import { EvidenceFilter } from 'ng-src/app/interfaces/evidence-filter';
import { SingleRequestHelperService } from 'ng-src/app/shared/services/single-request-helper.service';

@Injectable({
  providedIn: 'root'
})
export class EvidenceExpertService {

  constructor(private singleRequestHelperService: SingleRequestHelperService) {
  }

  getEvidenceExperts(organizationId: number, filter: EvidenceFilter, pageInfo: PageInfo): Observable<EvidenceExpertWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService
      .get<EvidenceExpertWrapper>(environment.internalRestUrl + 'evidence/organizations/' + organizationId + '/experts', {params})
      .pipe(shareReplay());
  }

  getAffiliatedExperts(organizationId: number, filter: EvidenceFilter, pageInfo: PageInfo): Observable<EvidenceExpertWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService
      .get<EvidenceExpertWrapper>(environment.internalRestUrl + 'evidence/organizations/' + organizationId + '/affiliated-experts', {params})
      .pipe(shareReplay());
  }
}
