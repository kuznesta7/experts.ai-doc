import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { ThesisPreview } from 'ng-src/app/interfaces/thesis-preview';

@Injectable({
  providedIn: 'root'
})
export class EvidenceThesisService {

  constructor(private httpClient: HttpClient) {
  }

  getThesisDetail(itemCode: string): Observable<ThesisPreview> {
    return this.httpClient.get<ThesisPreview>(environment.restUrl + `thesis/${itemCode}`);
  }
}
