import {Observable, of} from 'rxjs';
import {CategoryDto} from 'ng-src/app/interfaces/category-dto';
import {environment} from 'ng-src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {shareReplay} from 'rxjs/operators';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EquipmentCategoryService {

  constructor(private httpClient: HttpClient) {
  }

  private equipmentDomain$ = this.httpClient.get<CategoryDto[]>(environment.restUrl + `equipment/domain`).pipe(shareReplay());
  private equipmentType$ = this.httpClient.get<CategoryDto[]>(environment.restUrl + `equipment/type`).pipe(shareReplay());

  getEquipmentDomain(): Observable<CategoryDto[]> {
    return this.equipmentDomain$;
  }

  getEquipmentType(): Observable<CategoryDto[]> {
    this.equipmentType$.subscribe();
    return this.equipmentType$;
  }

  getEquipmentLanguage(): Observable<CategoryDto[]> {
    const languages: CategoryDto[] = [{id : 1, name : "en"}, {id :2, name :  "cz"}];
    return of(languages);
  }
}
