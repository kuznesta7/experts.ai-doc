import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from 'ng-src/environments/environment';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';
import {HttpClient} from '@angular/common/http';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';

@Injectable({
  providedIn: 'root'
})
export class ReindexService {

  constructor(private httpClient: HttpClient) {
  }

  reindex(clean: boolean, targets: IndexTarget[]): Observable<void> {
    const params = createHttpParams({clean, targets});
    return this.httpClient.put<void>(environment.internalRestUrl + 'index', null, {params});
  }

  reindexRecombee(clean: boolean, targets: IndexTarget[], organizationId: number): Observable<void> {
    const params = createHttpParams({clean, targets, organizationId});
    return this.httpClient.put<void>(environment.internalRestUrl + 'index/recombee', null, {params});
  }

  resetRecombee(organizationId: number): Observable<void> {
    const params = createHttpParams({organizationId});
    return this.httpClient.delete<void>(environment.internalRestUrl + 'index/recombee', {params});
  }

  getStatuses(): Observable<IndexStatusDetail[]> {
    return this.httpClient.get<IndexStatusDetail[]>(environment.internalRestUrl + 'index');
  }

  getRecombeeReindexStatuses(): Observable<RecombeeReindexOptionDto[]> {
    return this.httpClient.get<RecombeeReindexOptionDto[]>(environment.internalRestUrl + 'index/recombee');
  }
}

export interface IndexStatusDetail {
  historyId: number;
  target: IndexTarget;
  status: IndexStatus;
  clean: boolean;
  userId: number;
  date: Date;
}

export interface RecombeeReindexOptionDto {
  organization: OrganizationBaseDto;
  targets: IndexTarget[];
  availableTargets: IndexTarget[];
  recombeeIdentifier: string;
}

export type IndexTarget =
  'ITEMS'
  | 'EXPERTS'
  | 'ORGANIZATIONS'
  | 'ONTOLOGY'
  | 'PROJECTS'
  | 'COMMERCIALIZATION_PROJECTS'
  | 'EXPERT_EVALUATION'
  | 'ITEM_ASSOCIATION';
export type IndexStatus = 'STARTED' | 'FINISHED' | 'FAILED';
