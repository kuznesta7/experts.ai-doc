import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { SessionService } from 'ng-src/app/services/session.service';

@Injectable({
  providedIn: 'root'
})
export class OauthService {

  constructor(private httpClient: HttpClient, private sessionService: SessionService) {
  }

  static readonly REDIRECT_URL_KEY = 'REDIRECT_URL_AFTER_LOGIN';

  getOauthInfo(): Observable<OauthInfo> {
    return this.httpClient.get<OauthInfo>(environment.restUrl + 'oauth2');
  }

  doLogin(service: string, code: string) {
    const params = createHttpParams({code});
    return this.httpClient.post<OauthInfo>(environment.restUrl + 'oauth2/' + service, null, {params}).pipe(
      tap(() => {
        this.sessionService.updateSession();
      })
    );
  }
}

export interface OauthInfo {
  google: string;
  linkedin: string;
}

export enum SocialService {
  LINKED_IN = 'LINKED_IN', GOOGLE = 'GOOGLE'
}
