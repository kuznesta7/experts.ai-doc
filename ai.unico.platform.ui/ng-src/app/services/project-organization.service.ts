import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';

@Injectable({
  providedIn: 'root'
})
export class ProjectOrganizationService {

  constructor(private httpClient: HttpClient) {
  }

  addOrganization(projectId: number, organizationId: number, verify?: boolean): Observable<void> {
    const params = createHttpParams({verify});
    return this.httpClient.put<void>(environment.internalRestUrl + `projects/${projectId}/organizations/${organizationId}`, null, {params});
  }

  removeOrganization(projectId: number, organizationId: number): Observable<void> {
    return this.httpClient.delete<void>(environment.internalRestUrl + `projects/${projectId}/organizations/${organizationId}`);
  }

  verifyProjectsForOrganization(projectIds: number[], organizationId: number, verify: boolean): Observable<void> {
    const params = createHttpParams({projectIds, verify});
    return this.httpClient.put<void>(`${environment.internalRestUrl}organizations/${organizationId}/projects`, null, {params});
  }
}
