import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { SingleRequestHelperService } from 'ng-src/app/shared/services/single-request-helper.service';
import { HttpClient } from '@angular/common/http';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';

@Injectable({
  providedIn: 'root'
})
export class SearchHistoryService {

  constructor(private singleRequestHelperService: SingleRequestHelperService,
              private httpClient: HttpClient) {
  }

  findSearchHistory(userId: number, filter: SearchHistoryFilter, pageInfo: PageInfo): Observable<SearchHistoryWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<SearchHistoryWrapper>(`${environment.internalRestUrl}users/${userId}/search/history`, {params});
  }

  updateSearchHistory(searchHistoryId: number, favorite: boolean, workspaces: string[]): Observable<void> {
    const params = createHttpParams({favorite, workspaces});
    return this.httpClient.put<void>(`${environment.internalRestUrl}search/history/${searchHistoryId}`, null, {params});
  }

  deleteSearchHistory(searchHistoryId: number): Observable<void> {
    return this.httpClient.delete<void>(`${environment.internalRestUrl}search/history/${searchHistoryId}`);
  }

}

export interface SearchHistoryWrapper {
  limit: number;
  page: number;
  historyDtos: SearchHistoryDto[];
  numberOfResults: number;
}

export interface SearchHistoryDto {
  searchHistId: number;
  date: Date;
  query: string;
  organizationIds: number[];
  resultTypeGroupIds: number[];
  yearFrom: number;
  yearTo: number;
  secondaryQueries: string[];
  secondaryWeights: number[];
  workspaces: string[];
  ontologyDisabled: boolean;
  favorite: boolean;
  resultsFound: number;
  countryCodes: string[];
  expertSearchType: string;
}

export interface SearchHistoryFilter {
  workspaces: string[];
  favorite: boolean;
}
