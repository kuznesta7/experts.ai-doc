import {Injectable} from '@angular/core';
import {SingleRequestHelperService} from '../shared/services/single-request-helper.service';
import {Observable} from 'rxjs';
import {ProjectProgramDto} from './project-program.service';
import {createHttpParams} from '../shared/utils/httpParamsUtil';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ProjectAutocompleteService {

  constructor(private singleRequestHelperService: SingleRequestHelperService) {
  }

  autocompleteProjectProgram(query: string): Observable<ProjectProgramDto[]> {
    const params = createHttpParams({query});
    return this.singleRequestHelperService.get<ProjectProgramDto[]>(environment.internalRestUrl + 'projects/programs/autocomplete', {params});
  }
}
