import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CategoryDto } from 'ng-src/app/interfaces/category-dto';
import { environment } from 'ng-src/environments/environment';
import { shareReplay } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OpportunityCategoryService {

  constructor(private httpClient: HttpClient) {
  }

  private opportunityType$ = this.httpClient.get<CategoryDto[]>(environment.restUrl + 'opportunity/type').pipe(shareReplay());
  private jobType$ = this.httpClient.get<CategoryDto[]>(environment.restUrl + 'opportunity/jobType').pipe(shareReplay());

  getOpportunityType(): Observable<CategoryDto[]> {
    return this.opportunityType$;
  }

  getJobType(): Observable<CategoryDto[]> {
    return this.jobType$;
  }
}
