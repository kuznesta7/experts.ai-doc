import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from 'ng-src/environments/environment';
import {shareReplay} from 'rxjs/operators';
import {SingleRequestHelperService} from 'ng-src/app/shared/services/single-request-helper.service';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {EvidenceFilter} from 'ng-src/app/interfaces/evidence-filter';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';
import {EvidenceItemWrapper} from 'ng-src/app/interfaces/evidence-item-wrapper';
import {ProjectWrapper} from 'ng-src/app/interfaces/project-wrapper';
import {CommercializationProjectFilter} from 'ng-src/app/interfaces/commercialization-project-filter';
import {CommercializationProjectWrapper} from 'ng-src/app/interfaces/commercialization-project-wrapper';
import {EvidenceThesisWrapper} from 'ng-src/app/interfaces/evidence-thesis-wrapper';
import {EvidenceLectureWrapper} from 'ng-src/app/interfaces/evidence-lecture-wrapper';

@Injectable({
  providedIn: 'root'
})
export class ExpertDetailService {

  constructor(private singleRequestHelperService: SingleRequestHelperService) {
  }

  getExpertDetail(expertCode: string): Observable<ExpertDetailDto> {
    const url = environment.restUrl + 'profiles/experts/' + expertCode;
    return this.singleRequestHelperService.get<ExpertDetailDto>(url).pipe(
      shareReplay()
    );
  }

  getExpertOutcomes(expertCode: string, filter: EvidenceFilter, pageInfo: PageInfo): Observable<EvidenceItemWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    const url = environment.restUrl + 'profiles/experts/' + expertCode + '/items';
    return this.singleRequestHelperService.get<EvidenceItemWrapper>(url, {params}).pipe(
      shareReplay()
    );
  }

  getExpertThesis(expertCode: string, filter: EvidenceFilter, pageInfo: PageInfo): Observable<EvidenceThesisWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    const url = environment.restUrl + 'profiles/experts/' + expertCode + '/thesis';
    return this.singleRequestHelperService.get<EvidenceThesisWrapper>(url, {params}).pipe(
      shareReplay()
    );
  }

  getExpertLectures(expertCode: string, filter: EvidenceFilter, pageInfo: PageInfo): Observable<EvidenceLectureWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    const url = environment.restUrl + 'profiles/experts/' + expertCode + '/lectures';
    return this.singleRequestHelperService.get<EvidenceLectureWrapper>(url, {params}).pipe(
      shareReplay()
    );
  }

  getExpertProjects(expertCode: string, filter: EvidenceFilter, pageInfo: PageInfo): Observable<ProjectWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    const url = environment.restUrl + 'profiles/experts/' + expertCode + '/projects';
    return this.singleRequestHelperService.get<ProjectWrapper>(url, {params}).pipe(
      shareReplay()
    );
  }

  getExpertCommercialization(expertCode: string, filter: CommercializationProjectFilter, pageInfo: PageInfo): Observable<CommercializationProjectWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    const url = environment.restUrl + 'profiles/experts/' + expertCode + '/commercialization';
    return this.singleRequestHelperService.get<CommercializationProjectWrapper>(url, {params}).pipe(
      shareReplay()
    );
  }
}

export interface ExpertDetailDto {
  expertCode: string;
  userId: number;
  expertId: number;
  name: string;
  organizationDtos: UserOrganizationDto[];
  rank: number;
  itemAppearance: number;
  investmentProjectAppearance: number;
  numberOfAllItems: number;
  numberOfInvestmentProjects: number;
  numberOfProjects: number;
  numberOfAllThesis: number;
  numberOfAllLectures: number;
  itemTypeGroupsWithCount: {
    itemTypeGroupId: number;
    itemTypeIds: number[];
    itemTypeGroupTitle: string;
    count: number;
  };
  verified: boolean;
  retired: boolean;
  claimable: boolean;
  description: string;
  keywords: string[];
}

export interface UserOrganizationDto extends OrganizationBaseDto {
  visible?: boolean;
  active?: boolean;
}
