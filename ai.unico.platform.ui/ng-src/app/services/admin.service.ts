import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { VersionDto } from 'ng-src/app/interfaces/version-dto';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private httpClient: HttpClient) {
  }

  getVersions(): Observable<VersionDto> {
    return this.httpClient.get<VersionDto>(environment.internalRestUrl + 'administration/version');
  }
}
