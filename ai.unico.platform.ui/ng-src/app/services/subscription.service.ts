import {Injectable} from '@angular/core';
import {SubscriptionDto} from 'ng-src/app/interfaces/subscription-dto';
import {Observable} from 'rxjs';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';
import {environment} from 'ng-src/environments/environment';
import {SubscriptionTypeDto} from 'ng-src/app/interfaces/subscription-type-dto';
import {HttpClient} from '@angular/common/http';
import {SingleRequestHelperService} from 'ng-src/app/shared/services/single-request-helper.service';

@Injectable({
  providedIn: 'root'
})

export class SubscriptionService {

    constructor(private httpClient: HttpClient,
                private singleRequestHelperService: SingleRequestHelperService) { }

  widgetEmailSubscription(subscriptionDto: SubscriptionDto, recaptchaToken: string): Observable<any> {
    const params = createHttpParams({recaptchaToken});
    return this.httpClient.post<any>(environment.restUrl + '/widgets/subscribe', subscriptionDto, {params});
  }

  getSubscriptionTypes(): Observable<SubscriptionTypeDto[]> {
    return this.singleRequestHelperService.get<SubscriptionTypeDto[]>(environment.restUrl + '/subscribe/type');
  }

  unsubscribe(email: string, userToken: string, organizationId: number): Observable<boolean> {
    return this.httpClient.delete<any>(`${environment.restUrl}/subscribe/${organizationId}/${email}/${userToken}`);
  }
}
