import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { OrganizationStructureRelation } from 'ng-src/app/enums/organization-structure-relation';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrganizationStructureService {

  constructor(private httpClient: HttpClient) {
  }

  getMemberIds(organizationId: number): Observable<number[]> {
    return this.httpClient.get<number[]>(`${environment.restUrl}organizations/${organizationId}/memberIds`);
  }

  getStructure(organizationIds: number[] | number, includeMembers: boolean = true): Observable<OrganizationStructureDto[]> {
    const params = createHttpParams({organizationIds, includeMembers});
    return this.httpClient.get<OrganizationStructureDto[]>(environment.restUrl + 'organizations/structure', {params});
  }

  addLowerOrganization(organizationId: number, lowerOrganizationId: number, relation?: OrganizationStructureRelation): Observable<void> {
    const params = createHttpParams({relation: relation || OrganizationStructureRelation.PARENT});
    return this.httpClient.put<void>(`${environment.restUrl}organizations/${organizationId}/children/${lowerOrganizationId}`, null, {params});
  }

  removeLowerOrganization(organizationId: number, lowerOrganizationId: number): Observable<void> {
    return this.httpClient.delete<void>(`${environment.restUrl}organizations/${organizationId}/children/${lowerOrganizationId}`);
  }
}

export interface OrganizationStructureDto {
  organizationId: number;
  organizationAbbrev: string;
  organizationName: string;
  countryCode: string;
  memberOrganization: boolean;
  relationType: OrganizationStructureRelation;
  organizationUnits: OrganizationStructureDto[];
}
