import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from 'ng-src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {EquipmentPreview} from 'ng-src/app/interfaces/equipment-preview';
import {EquipmentFilter} from 'ng-src/app/interfaces/equipment-filter';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {EvidenceEquipmentWrapper} from 'ng-src/app/interfaces/evidence-equipment-wrapper';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';
import {SingleRequestHelperService} from 'ng-src/app/shared/services/single-request-helper.service';
import {EvidenceEquipment} from 'ng-src/app/interfaces/evidence-equipment';
import {MultilingualDto} from 'ng-src/app/interfaces/multilingual-dto';

@Injectable({
  providedIn: 'root'
})
export class EvidenceEquipmentService {

  constructor(private httpClient: HttpClient,
              private singleRequestHelperService: SingleRequestHelperService) {
  }

  getOrganizationEquipment(organizationId: number, filter: EquipmentFilter, pageInfo: PageInfo): Observable<EvidenceEquipmentWrapper> {
    console.log(filter);
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<EvidenceEquipmentWrapper>(environment.restUrl + 'widgets/organizations/' + organizationId + '/equipment', {params});
  }

  getEquipmentDetail(equipmentId: number): Observable<MultilingualDto<EquipmentPreview>> {
    return this.httpClient.get<MultilingualDto<EquipmentPreview>>(environment.restUrl + `equipment/${equipmentId}`);
  }

  createEquipment(evidenceEquipment: MultilingualDto<EvidenceEquipment>): Observable<EquipmentIdResponse> {
    return this.httpClient.post<EquipmentIdResponse>(environment.restUrl + '/equipment', evidenceEquipment);
  }

  updateEquipment(evidenceEquipment: MultilingualDto<EvidenceEquipment>): Observable<EquipmentIdResponse> {
    return this.httpClient.put<EquipmentIdResponse>(environment.restUrl + '/equipment/', evidenceEquipment);
  }

  deleteEquipment(equipmentCode: string): Observable<void> {
    return this.httpClient.delete<void>(environment.restUrl + '/equipment/' + equipmentCode);
  }

  toggleHidden(equipmentCode: string, hidden: boolean): Observable<void> {
    const body = createHttpParams({
      equipmentCode,
      hidden
    });
    return this.httpClient.post<void>(environment.restUrl + `/equipment/hidden/`, body);
  }

}

export interface EquipmentIdResponse {
  id: number;
}
