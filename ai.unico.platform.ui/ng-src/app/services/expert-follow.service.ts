import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { SingleRequestHelperService } from 'ng-src/app/shared/services/single-request-helper.service';
import { HttpClient } from '@angular/common/http';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';

@Injectable({
  providedIn: 'root'
})
export class ExpertFollowService {

  constructor(private singleRequestHelperService: SingleRequestHelperService,
              private httpClient: HttpClient) {
  }

  findFollowedExperts(userId: number, pageInfo: PageInfo, workspaces?: string[]): Observable<FollowedProfilesWrapper> {
    const params = createHttpParams({...pageInfo, workspaces});
    return this.singleRequestHelperService.get<FollowedProfilesWrapper>(`${environment.internalRestUrl}/users/${userId}/followed-profiles`, {params});
  }

  followProfile(userId: number, definition: FollowProfileDto): Observable<void> {
    return this.httpClient.put<void>(`${environment.internalRestUrl}/users/${userId}/followed-profiles`, definition);
  }

  unfollowProfile(userId: number, definition: FollowProfileDto): Observable<void> {
    const pathVariables = definition.followedUserId ? `users/${definition.followedUserId}` : `experts/${definition.followedExpertId}`;
    return this.httpClient.delete<void>(`${environment.internalRestUrl}/users/${userId}/followed-profiles/${pathVariables}`);
  }

  getFollowForProfile(userId: number, definition: FollowProfileDto): Observable<FollowedProfilePreviewDto> {
    const pathVariables = definition.followedUserId ? `users/${definition.followedUserId}` : `experts/${definition.followedExpertId}`;
    return this.singleRequestHelperService.get<FollowedProfilePreviewDto>(`${environment.internalRestUrl}/users/${userId}/followed-profiles/${pathVariables}`);
  }
}

export interface FollowedProfilesWrapper {
  limit: number;
  page: number;
  followedProfilePreviewDtos: FollowedProfilePreviewDto[];
  numberOfResults: number;
}

export interface FollowedProfilePreviewDto {
  userId: number;
  expertId: number;
  name: string;
  workspaces: string[];
}

export interface FollowProfileDto {
  followedUserId?: number;
  followedExpertId?: number;
  workspaces?: string[];
}
