import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { shareReplay } from 'rxjs/operators';
import { EvidenceFilter } from 'ng-src/app/interfaces/evidence-filter';
import { EvidenceItemWrapper } from 'ng-src/app/interfaces/evidence-item-wrapper';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { ItemDetailDto } from 'ng-src/app/interfaces/item-detail-dto';
import { EvidenceItemPreviewWithExperts } from 'ng-src/app/interfaces/evidence-item-preview-with-experts';

@Injectable({
  providedIn: 'root'
})
export class EvidenceItemService {

  constructor(private httpClient: HttpClient) {
  }

  searchForItems(filter: EvidenceFilter): Observable<EvidenceItemPreviewWithExperts[]> {
    const params = createHttpParams(filter);
    return this.httpClient
      .get<EvidenceItemPreviewWithExperts[]>(environment.restUrl + 'items', {params})
      .pipe(shareReplay());
  }

  getEvidenceItems(organizationId: number, filter: EvidenceFilter, pageInfo: PageInfo): Observable<EvidenceItemWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.httpClient
      .get<EvidenceItemWrapper>(environment.internalRestUrl + `evidence/organizations/${organizationId}/items`, {params})
      .pipe(shareReplay());
  }

  organizationHasItem(organizationId: number, name: string): Observable<boolean> {
    return this.httpClient.get<boolean>(environment.internalRestUrl + `evidence/organizations/${organizationId}/items/exists/${name}`);
  }

  updateItem(item: ItemDetailDto): Observable<number> {
    let itemId = item.itemId;
    if (itemId === null || itemId === undefined) {
      itemId = item.originalItemId;
    }
    return this.httpClient.put<number>(environment.restUrl + `items/${itemId}`, item);
  }

  createItem(item: ItemDetailDto): Observable<number> {
    return this.httpClient.post<number>(environment.restUrl + 'items', item);
  }

  createOrganizationItem(item: ItemDetailDto, organizationId: number): Observable<number> {
    return this.httpClient.post<number>(environment.internalRestUrl + `organizations/${organizationId}/items`, item);
  }

  createUserItem(item: ItemDetailDto, userId: number): Observable<number> {
    return this.httpClient.post<number>(environment.internalRestUrl + `users/${userId}/items`, item);
  }

  userHasItem(userId: number, name: string): Observable<boolean> {
    return this.httpClient.get<boolean>(environment.internalRestUrl + `evidence/users/${userId}/items/exists/${name}`);
  }

  getItemDetail(itemCode: string): Observable<ItemDetailDto> {
    return this.httpClient.get<ItemDetailDto>(environment.restUrl + `items/${itemCode}`);
  }

  moveItem(originalItemId: number): Observable<number> {
    return this.httpClient.post<number>(environment.restUrl + `items/register/dwh/${originalItemId}`, null);
  }
}
