import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {environment} from 'ng-src/environments/environment.prod';
import {Observable} from 'rxjs';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {

  constructor(private httpClient: HttpClient) {
  }

  updateUserProfile(userId: number, userProfileDto: UserProfileDto): Observable<void> {
    return this.httpClient.put<void>(environment.restUrl + 'profiles/users/' + userId, userProfileDto);
  }

  expertToUserProfile(expertId: number, userProfileDto: UserProfileDto): Observable<number> {
    return this.httpClient.put<number>(environment.restUrl + `profiles/experts/${expertId}`, userProfileDto);
  }

  updateUserProfileImg(userId: number, type: UserProfileImgType, file: File): Observable<void> {
    const formData: FormData = new FormData();
    formData.append('content', file, file.name);
    const params = createHttpParams({fileName: file.name, type});
    return this.httpClient.put<void>(environment.restUrl + 'profiles/users/' + userId + '/img', formData, {params});
  }

  createClaimableProfile(userProfile: UserProfileDto): Observable<number> {
    return this.httpClient.post<number>(environment.restUrl + 'users/claimable', userProfile);
  }
}

export interface UserProfileDto {
  userId?: number;
  name: string;
  description?: string;
  organizationDtos?: OrganizationBaseDto[];
  keywords?: string[];
}

export type UserProfileImgType = 'PORTRAIT' | 'COVER';
