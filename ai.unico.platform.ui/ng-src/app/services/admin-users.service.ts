import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { SingleRequestHelperService } from 'ng-src/app/shared/services/single-request-helper.service';
import { HttpClient } from '@angular/common/http';
import { PageInfo } from 'ng-src/app/shared/interfaces/page-info';

@Injectable({
  providedIn: 'root'
})
export class AdminUsersService {

  constructor(private singleRequestHelperService: SingleRequestHelperService,
              private httpClient: HttpClient) {
  }

  loadUsers(filter: UserFilter, pageInfo: PageInfo): Observable<AdminUsersWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<AdminUsersWrapper>(environment.internalRestUrl + 'users', {params});
  }

  impersonate(user: AdminUserDto): Observable<Object> {
    return this.httpClient.post<Object>(environment.internalRestUrl + 'impersonation/' + user.userId, {});
  }
}

export interface UserFilter {
  query: string;
}

export interface AdminUsersWrapper {
  numberOfUsers: number;
  limit: number;
  page: number;
  adminUserDtos: AdminUserDto[];
}

export interface AdminUserDto {
  userId: number;
  firstName: string;
  lastName: string;
  email: string;
  username: string;
  registrationDate: Date;
  lastLoginDate: Date;
  loginCount: number;
}
