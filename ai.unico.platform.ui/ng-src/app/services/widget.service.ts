import {Injectable} from '@angular/core';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {Observable} from 'rxjs';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';
import {ProjectWrapper} from 'ng-src/app/interfaces/project-wrapper';
import {HttpClient} from '@angular/common/http';
import {SingleRequestHelperService} from 'ng-src/app/shared/services/single-request-helper.service';
import {CommercializationProjectWrapper} from 'ng-src/app/interfaces/commercialization-project-wrapper';
import {EvidenceExpertWrapper} from 'ng-src/app/interfaces/evidence-expert-wrapper';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';
import {EvidenceFilter} from 'ng-src/app/interfaces/evidence-filter';
import {environment} from 'ng-src/environments/environment';
import {EvidenceItemWrapper} from 'ng-src/app/interfaces/evidence-item-wrapper';
import {OrganizationDetail} from 'ng-src/app/services/organization-detail.service';
import {RecombeeService} from 'ng-src/app/services/recombee.service';
import {ExpertDetailDto} from 'ng-src/app/services/expert-detail.service';
import {ProjectDetailDto} from 'ng-src/app/interfaces/project-detail-dto';
import {ItemDetailDto} from 'ng-src/app/interfaces/item-detail-dto';
import {CommercializationDetailDto} from 'ng-src/app/interfaces/commercialization-detail-dto';
import {EvidenceLectureWrapper} from 'ng-src/app/interfaces/evidence-lecture-wrapper';
import {LecturePreview} from 'ng-src/app/interfaces/lecture-preview';
import {ExternalMessageDto} from 'ng-src/app/interfaces/external-message';
import {EquipmentFilter} from 'ng-src/app/interfaces/equipment-filter';
import {EvidenceEquipmentWrapper} from 'ng-src/app/interfaces/evidence-equipment-wrapper';
import {EquipmentPreview} from 'ng-src/app/interfaces/equipment-preview';
import {OpportunityFilter} from 'ng-src/app/interfaces/opportunity-filter';
import {OpportunityWrapper} from 'ng-src/app/interfaces/opportunity-wrapper';
import {OpportunityPreview} from 'ng-src/app/interfaces/opportunity-preview';
import {SubscriptionDto} from 'ng-src/app/interfaces/subscription-dto';
import {OrganizationWrapper} from 'ng-src/app/interfaces/organization-wrapper';
import {MultilingualDto} from 'ng-src/app/interfaces/multilingual-dto';

@Injectable({
  providedIn: 'root'
})
export class WidgetService {

  constructor(private httpClient: HttpClient,
              private singleRequestHelperService: SingleRequestHelperService,
              private recombeeService: RecombeeService) {
  }

  getGdpr(ogranizationId: number): Observable<boolean> {
    return this.singleRequestHelperService.get<boolean>(environment.restUrl + '/organizations/' + ogranizationId + '/gdpr')
  }

  getOrganizationCommercialization(organizationId: number, filter: EvidenceFilter, pageInfo: PageInfo): Observable<CommercializationProjectWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<CommercializationProjectWrapper>(environment.restUrl + 'widgets/organizations/' + organizationId + '/commercialization', {params});
  }

  getProjects(organizationId: number, filter: EvidenceFilter, pageInfo: PageInfo): Observable<ProjectWrapper> {
    filter.user = this.recombeeService.getUserRecombeeToken();
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<ProjectWrapper>(environment.restUrl + 'widgets/organizations/' + organizationId + '/projects/recombee', {params});
  }

  getLectures(organizationId: number, filter: EvidenceFilter, pageInfo: PageInfo): Observable<EvidenceLectureWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<EvidenceLectureWrapper>(environment.restUrl + 'widgets/organizations/' + organizationId + '/lectures', {params});
  }

  getOpportunities(organizationId: number, filter: OpportunityFilter, pageInfo: PageInfo): Observable<OpportunityWrapper> {
    filter.user = this.recombeeService.getUserRecombeeToken();
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<OpportunityWrapper>(environment.restUrl + 'widgets/organizations/' + organizationId + '/opportunity/recombee', {params});
  }

  getOpportunity(opportunityId: string): Observable<OpportunityPreview> {
    return this.singleRequestHelperService.get<OpportunityPreview>(environment.restUrl + 'widgets/opportunity/' + opportunityId);
  }

  getOrganizationExperts(organizationId: number, filter: EvidenceFilter, pageInfo: PageInfo): Observable<EvidenceExpertWrapper> {
    filter.user = this.recombeeService.getUserRecombeeToken();
    filter.retirementIgnored = false;
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<EvidenceExpertWrapper>(environment.restUrl + 'widgets/organizations/' + organizationId + '/experts/recombee', {params});
  }

  getOrganizationPreview(organizationId: number): Observable<OrganizationBaseDto> {
    return this.singleRequestHelperService.get<OrganizationBaseDto>(environment.restUrl + 'widgets/organizations/' + organizationId);
  }

  getOrganizationChildren(organizationId: number, filter: EvidenceFilter, pageInfo: PageInfo): Observable<OrganizationWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<OrganizationWrapper>(environment.restUrl + '/organizations/' + organizationId + '/children', {params});
  }

  getOrganizationOutcomes(organizationId: number, filter: EvidenceFilter, pageInfo: PageInfo): Observable<EvidenceItemWrapper> {
    filter.user = this.recombeeService.getUserRecombeeToken();
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<EvidenceItemWrapper>(environment.restUrl + 'widgets/organizations/' + organizationId + '/items/recombee', {params});
  }

  getOrganizationEquipment(organizationId: number, filter: EquipmentFilter, pageInfo: PageInfo): Observable<EvidenceEquipmentWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<EvidenceEquipmentWrapper>(environment.restUrl + 'widgets/organizations/' + organizationId + '/equipment', {params});
  }

  getOrganizationDetail(organizationId: number): Observable<OrganizationDetail> {
    return this.singleRequestHelperService.get<OrganizationDetail>(environment.restUrl + 'widgets/organizations/' + organizationId + '/detail');
  }

  getExpertDetail(expertCode: string): Observable<ExpertDetailDto> {
    return this.singleRequestHelperService.get<ExpertDetailDto>(environment.restUrl + 'widgets/experts/' + expertCode);
  }

  getProjectDetail(projectCode: string): Observable<ProjectDetailDto> {
    return this.singleRequestHelperService.get<ProjectDetailDto>(environment.restUrl + 'widgets/projects/' + projectCode);
  }

  getOutcomeDetail(itemCode: string): Observable<ItemDetailDto> {
    return this.singleRequestHelperService.get<ItemDetailDto>(environment.restUrl + 'widgets/items/' + itemCode);
  }

  getLectureDetail(lectureCode: string): Observable<LecturePreview> {
    return this.singleRequestHelperService.get<LecturePreview>(environment.restUrl + 'widgets/lectures/' + lectureCode);
  }

  getCommercializationDetail(commercializationId: string): Observable<CommercializationDetailDto> {
    return this.singleRequestHelperService.get<CommercializationDetailDto>(environment.restUrl + 'widgets/commercialization/' + commercializationId);
  }

  getEquipmentDetail(equipmentId: number): Observable<MultilingualDto<EquipmentPreview>> {
    return this.singleRequestHelperService.get<MultilingualDto<EquipmentPreview>>(environment.restUrl + 'widgets/equipment/' + equipmentId);
  }

  sendEmailWidget(externalMessageDto: ExternalMessageDto, recaptchaToken: string): Observable<any> {
    const params = createHttpParams({recaptchaToken});
    return this.httpClient.post<any>(environment.restUrl + 'messages', externalMessageDto, {params});
  }

  sendEmailContactUs(externalMessageDto: ExternalMessageDto, recaptchaToken: string): Observable<any> {
    const params = createHttpParams({recaptchaToken});
    return this.httpClient.post<any>(environment.restUrl + 'messages/contactus', externalMessageDto, {params});
  }

  sendEmailStandard(emailMessageDto: ExternalMessageDto): Observable<any> {
    return this.httpClient.post<any>(environment.internalRestUrl + 'messages/request', emailMessageDto);
  }

  widgetEmailSubscription(subscriptionDto: SubscriptionDto, recaptchaToken: string): Observable<any> {
    const params = createHttpParams({recaptchaToken});
    return this.httpClient.post<any>(environment.restUrl + '/widgets/subscribe', subscriptionDto, {params});
  }
}
