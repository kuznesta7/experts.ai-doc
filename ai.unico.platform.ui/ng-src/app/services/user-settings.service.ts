import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'ng-src/environments/environment.prod';
import { Observable } from 'rxjs';
import { UserRegistrationDto } from 'ng-src/app/interfaces/user-registration-dto';

@Injectable({
  providedIn: 'root'
})
export class UserSettingsService {

  constructor(private httpClient: HttpClient) {
  }

  getUserPreferences(userId: number): Observable<UserRegistrationDto> {
    return this.httpClient.get<UserRegistrationDto>(environment.restUrl + 'users/' + userId);
  }

  updateUserPreferences(userId: number, data: UserRegistrationDto): Observable<void> {
    return this.httpClient.put<void>(environment.restUrl + 'users/' + userId, data);
  }

  changePassword(currentPassword: string, password: string): Observable<void> {
    return this.httpClient.put<void>(environment.restUrl + 'password', {currentPassword, password});
  }

  resetPassword(email: string, token: string, password: string): Observable<void> {
    return this.httpClient.post<void>(environment.restUrl + 'password', {email, token, password});
  }

  getUserConsents(): Observable<UserConsentDto[]> {
    return this.httpClient.get<UserConsentDto[]>(environment.internalRestUrl + 'consents');
  }

  updateUserConsents(data: UserConsentDto[]): Observable<void> {
    return this.httpClient.put<void>(environment.internalRestUrl + 'consents', data);
  }
}

export interface UserConsentDto {
  consentId: string;
  consentTitle: string;
  valid: boolean;
}
