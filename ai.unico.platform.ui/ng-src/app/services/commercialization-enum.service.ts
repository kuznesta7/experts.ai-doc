import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { environment } from 'ng-src/environments/environment';
import { shareReplay } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CommercializationEnumService {

  constructor(private httpClient: HttpClient) {
  }

  private categories$ = this.loadCategories(false);
  private domains$ = this.loadDomains(false);
  private priorities$ = this.loadPriorities(false);
  private statuses$ = this.loadStatuses(false);

  getCurrentCategories(): Observable<CommercializationCategory[]> {
    return this.categories$;
  }

  loadCategories(includeInactive: boolean): Observable<CommercializationCategory[]> {
    const params = createHttpParams({includeInactive});
    return this.httpClient.get<CommercializationCategory[]>(environment.internalRestUrl + 'commercialization/categories', {params}).pipe(
      shareReplay()
    );
  }

  getCurrentDomains(): Observable<CommercializationDomain[]> {
    return this.domains$;
  }

  loadDomains(includeInactive: boolean): Observable<CommercializationDomain[]> {
    const params = createHttpParams({includeInactive});
    return this.httpClient.get<CommercializationDomain[]>(environment.internalRestUrl + 'commercialization/domains', {params}).pipe(
      shareReplay()
    );
  }

  getCurrentPriorities(): Observable<CommercializationPriority[]> {
    return this.priorities$;
  }

  loadPriorities(includeInactive: boolean): Observable<CommercializationPriority[]> {
    const params = createHttpParams({includeInactive});
    return this.httpClient.get<CommercializationPriority[]>(environment.internalRestUrl + 'commercialization/priorities', {params}).pipe(
      shareReplay()
    );
  }

  getCurrentStatuses(): Observable<CommercializationStatus[]> {
    return this.statuses$;
  }

  loadStatuses(includeInactive: boolean): Observable<CommercializationStatus[]> {
    const params = createHttpParams({includeInactive});
    return this.httpClient.get<CommercializationStatus[]>(environment.internalRestUrl + 'commercialization/statuses', {params}).pipe(
      shareReplay()
    );
  }
}

export interface CommercializationCategory {
  commercializationCategoryId: number;
  commercializationCategoryName: string;
  commercializationCategoryDescription: string;
  commercializationCategoryCode: string;
  deleted: boolean;
}

export interface CommercializationDomain {
  domainId: number;
  domainName: string;
  domainCode: string;
  domainDescription: string;
  order: number;
  deleted: boolean;
}

export interface CommercializationPriority {
  priorityTypeId: number;
  priorityTypeName: string;
  priorityTypeCode: string;
  priorityTypeDescription: string;
  deleted: boolean;
}

export interface CommercializationStatus {
  statusId: number;
  index: number;
  statusName: string;
  statusCode: string;
  useInSearch: boolean;
  useInInvest: boolean;
  canSetOrganization: boolean;
  deleted: boolean;
  mustBeFilled: boolean;
  statusOrder: number;
  categoryIds: number[];
}

export enum CommercializationOrganizationRelation {
  INTELLECTUAL_PROPERTY_OWNER = 'INTELLECTUAL_PROPERTY_OWNER',
  INVESTOR = 'INVESTOR',
  COMMERCIALIZATION_PARTNER = 'COMMERCIALIZATION_PARTNER'
}

