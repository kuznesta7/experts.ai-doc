import { Injectable } from '@angular/core';
import { OrganizationRole, Session, SessionService } from 'ng-src/app/services/session.service';
import { map, share } from 'rxjs/operators';
import { SecurityService } from 'ng-src/app/services/security.service';
import { Observable, zip } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor(private sessionService: SessionService,
              private securityService: SecurityService) {
  }

  canEditUserProfile(userId: number): Observable<boolean> {
    return this.maintainerOr((session: Session) => session.userId == userId);
  }

  canEditOrganizationProfile(organizationId: number): Observable<boolean> {
    const editor$ = this.securityService.hasOrganizationRole(organizationId, OrganizationRole.ORGANIZATION_EDITOR);
    return zip(editor$, this.securityService.isPortalMaintainer()).pipe(
      map(requirements => requirements.some(r => !!r))
    );
  }

  private maintainerOr(fn: (session: Session) => boolean): Observable<boolean> {
    return zip(this.sessionService.getCurrentSession(), this.securityService.isPortalMaintainer()).pipe(
      map(([session, maintainer]) => {
        if (maintainer) return true;
        if (!session) return false;
        return fn(session);
      }),
      share()
    );
  }
}
