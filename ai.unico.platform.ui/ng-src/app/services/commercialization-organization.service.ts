import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { environment } from 'ng-src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { CommercializationOrganizationRelation } from 'ng-src/app/services/commercialization-enum.service';


@Injectable({
  providedIn: 'root'
})
export class CommercializationOrganizationService {

  constructor(private httpClient: HttpClient) {
  }

  addOrganization(commercializationProjectId: number, organizationId: number, relation: CommercializationOrganizationRelation): Observable<void> {
    const params = createHttpParams({relation});
    return this.httpClient.put<void>(`${environment.internalRestUrl}commercialization/projects/${commercializationProjectId}/organizations/${organizationId}`, null, {params});
  }

  removeOrganization(commercializationProjectId: number, organizationId: number, relation: CommercializationOrganizationRelation): Observable<void> {
    const params = createHttpParams({relation});
    return this.httpClient.delete<void>(`${environment.internalRestUrl}commercialization/projects/${commercializationProjectId}/organizations/${organizationId}`, {params});
  }
}
