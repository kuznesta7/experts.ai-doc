import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { SingleRequestHelperService } from 'ng-src/app/shared/services/single-request-helper.service';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private singleRequestService: SingleRequestHelperService) {
  }

  readonly lookupFunction = (q: string) => this.findRelevantTags(q).pipe(map(tags => tags.map(t => ({
    id: t,
    description: t
  }))))

  findRelevantTags(query: string): Observable<string[]> {
    const params = createHttpParams({query});
    return this.singleRequestService.get(environment.restUrl + 'tags', {params});
  }
}
