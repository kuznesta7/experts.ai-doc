import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { shareReplay, switchMap } from 'rxjs/operators';
import { environment } from 'ng-src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private httpClient: HttpClient) {
  }

  private lastSessionObservable$: Observable<Session> = this.getSession();

  private update$ = new BehaviorSubject<void>(undefined);
  private currentSession$ = this.update$.pipe(
    switchMap(() => this.getSession()),
    shareReplay()
  );

  private getSession(): Observable<Session> {
    this.lastSessionObservable$ = this.httpClient.get<Session>(environment.restUrl + 'session').pipe(
      shareReplay()
    );
    return this.lastSessionObservable$;
  }

  getCurrentSessionWithUpdates(): Observable<Session> {
    return this.currentSession$;
  }

  getCurrentSession(): Observable<Session> {
    return this.lastSessionObservable$;
  }

  updateSession(): Observable<Session> {
    this.update$.next();
    return this.getCurrentSession();
  }
}


export interface Session {
  expertCode: string;
  userId: number;
  username: string;
  fullName: string;
  roles: Role[];
  lastLoginDate: Date;
  organizationRoles: OrganizationWithRoles[];
  impersonatorUserId: number;
  profileNeedsToBeCompleted: boolean;
  countryOfInterestCodes: string[];
}

export interface OrganizationWithRoles {
  organizationId: number;
  organizationName: string;
  organizationAbbrev: string;
  roles: OrganizationRoleWithDetails[];
}

export interface OrganizationRoleWithDetails {
  valid: boolean;
  validUntil: boolean;
  role: OrganizationRole;
}

export enum OrganizationRole {
  ORGANIZATION_EDITOR = 'ORGANIZATION_EDITOR',
  ORGANIZATION_VIEWER = 'ORGANIZATION_VIEWER',
  ORGANIZATION_USER = 'ORGANIZATION_USER',
  ORGANIZATION_ADMIN = 'ORGANIZATION_ADMIN'
}

export enum Role {
  PORTAL_ADMIN = 'PORTAL_ADMIN',
  COMMERCIALIZATION_ADMIN = 'COMMERCIALIZATION_ADMIN',
  PORTAL_MAINTAINER = 'PORTAL_MAINTAINER',
  USER = 'USER',
  BASIC_USER = 'BASIC_USER',
  PREMIUM_USER = 'PREMIUM_USER',
  INFINITY_USER = 'INFINITY_USER',
  DATA_CLEANER = 'DATA_CLEANER',
  DATA_IMPORTER = 'DATA_IMPORTER',
  COMMERCIALIZATION_INVESTOR = 'COMMERCIALIZATION_INVESTOR',
  COMPANY_EXPERT = 'COMPANY_EXPERT',
  ACADEMIC_EXPERT = 'ACADEMIC_EXPERT',
  INVESTOR = 'INVESTOR'
}
