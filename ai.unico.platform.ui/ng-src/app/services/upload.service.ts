import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FileDropModalComponent } from 'ng-src/app/platform/components/common/file-drop-modal/file-drop-modal.component';
import { FileType } from 'ng-src/app/platform/components/common/file-drop/file-drop.component';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private ngbModal: NgbModal) {
  }

  openUploadModal(uploadCallback: (files: File[]) => Observable<any>, options?: UploadModalOptions): NgbModalRef {
    const ngbModalRef = this.ngbModal.open(FileDropModalComponent);
    const componentInstance: FileDropModalComponent = ngbModalRef.componentInstance;
    componentInstance.uploadCallback = uploadCallback;
    componentInstance.title = options?.title;
    componentInstance.multiple = !!options?.multiple;
    componentInstance.preview = !!options?.preview;
    componentInstance.fileType = options?.fileType || null;
    return ngbModalRef;
  }
}

export interface UploadModalOptions {
  title?: string;
  multiple?: boolean;
  preview?: boolean;
  fileType?: FileType;
}
