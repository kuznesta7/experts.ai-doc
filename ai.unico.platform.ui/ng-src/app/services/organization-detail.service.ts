import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from 'ng-src/environments/environment';
import {createHttpParams} from 'ng-src/app/shared/utils/httpParamsUtil';
import {SingleRequestHelperService} from 'ng-src/app/shared/services/single-request-helper.service';
import {HttpClient} from '@angular/common/http';
import {EvidenceExpertWrapper} from 'ng-src/app/interfaces/evidence-expert-wrapper';
import {EvidenceItemWrapper} from 'ng-src/app/interfaces/evidence-item-wrapper';
import {ProjectWrapper} from 'ng-src/app/interfaces/project-wrapper';
import {PageInfo} from 'ng-src/app/shared/interfaces/page-info';
import {ExternalMessageDto} from 'ng-src/app/interfaces/external-message';
import {OrganizationWrapper} from 'ng-src/app/interfaces/organization-wrapper';
import {OrganizationSearchFilter} from 'ng-src/app/interfaces/organization-search-filter';
import {OrganizationBaseDto} from 'ng-src/app/interfaces/organization-base-dto';

@Injectable({
  providedIn: 'root'
})
export class OrganizationDetailService {

  constructor(private httpClient: HttpClient,
              private singleRequestHelperService: SingleRequestHelperService) {
  }

  findOrganizationMemberList(organizationId: number, filter: OrganizationSearchFilter, pageInfo: PageInfo): Observable<OrganizationWrapper> {
    const params = createHttpParams({...filter, ...pageInfo});
    return this.singleRequestHelperService.get<OrganizationWrapper>(environment.restUrl + 'organizations/' + organizationId + '/memberSearch', {params});
  }

  findMyMemberships(organizationId: number): Observable<OrganizationBaseDto[]> {
    return this.singleRequestHelperService.get<OrganizationBaseDto[]>(environment.restUrl + 'organizations/' + organizationId + '/myMemberships');
  }

  findParents(organizationId: number): Observable<OrganizationBaseDto[]> {
    return this.singleRequestHelperService.get<OrganizationBaseDto[]>(environment.restUrl + 'organizations/' + organizationId + '/findParents');
  }

  getOrganizationDetail(organizationId: number): Observable<OrganizationDetail> {
    return this.singleRequestHelperService.get<OrganizationDetail>(environment.restUrl + 'organizations/' + organizationId);
  }

  getOrganizationKeywords(organizationId: number): Observable<OrganizationKeywords> {
    return this.singleRequestHelperService.get<OrganizationKeywords>(environment.restUrl + 'organizations/' + organizationId + '/keywords');
  }

  getOrganizationSuggestedKeywords(organizationId: number, widgetTab: ListMode): Observable<OrganizationKeywords> {
    const params = createHttpParams({widgetTab});
    return this.singleRequestHelperService.get<OrganizationKeywords>(environment.restUrl + 'organizations/' + organizationId + '/keywords/suggested', {params});
  }

  contactOrganization(messageDto: ExternalMessageDto): Observable<any> {
    return this.httpClient.post<any>(environment.internalRestUrl + 'messages/toOrganization', messageDto);
  }

  contactExpert(messageDto: ExternalMessageDto): Observable<any> {
    return this.httpClient.post<any>(environment.internalRestUrl + 'messages', messageDto);
  }

  updateOrganizationImg(organizationId: number, type: OrganizationImgType, file: File): Observable<void> {
    const formData: FormData = new FormData();
    formData.append('content', file, file.name);
    const params = createHttpParams({fileName: file.name, type});
    return this.httpClient.put<void>(environment.restUrl + 'organizations/' + organizationId + '/img', formData, {params});
  }

  getProfileVisibility(organizationId: number): Observable<PublicProfileVisibilityDto> {
    return this.singleRequestHelperService.get<PublicProfileVisibilityDto>(environment.restUrl + 'organizations/' + organizationId + '/visibility');
  }

  updateProfileVisibility(organizationId: number, visibility: PublicProfileVisibilityDto): Observable<void> {
    return this.httpClient.post<void>(environment.restUrl + 'organizations/' + organizationId + '/visibility', visibility);
  }

  updateOrganizationDetail(organizationId: number, organizationDetail: OrganizationDetail): Observable<void> {
    return this.httpClient.put<void>(environment.restUrl + 'organizations/' + organizationId, organizationDetail);
  }

  getOrganizationStatistics(organizationId: number): Observable<PublicOrganizationStatistics> {
    return this.singleRequestHelperService.get<PublicOrganizationStatistics>(environment.restUrl + 'organizations/' + organizationId + '/statistics');
  }

  getOrganizationExperts(organizationId: number, pageInfo: PageInfo): Observable<EvidenceExpertWrapper> {
    const params = createHttpParams(pageInfo);
    return this.singleRequestHelperService.get<EvidenceExpertWrapper>(environment.restUrl + 'organizations/' + organizationId + '/experts', {params});
  }

  getOrganizationOutcomes(organizationId: number, pageInfo: PageInfo): Observable<EvidenceItemWrapper> {
    const params = createHttpParams(pageInfo);
    return this.singleRequestHelperService.get<EvidenceItemWrapper>(environment.restUrl + 'organizations/' + organizationId + '/items', {params});
  }

  getOrganizationProjects(organizationId: number, pageInfo: PageInfo): Observable<ProjectWrapper> {
    const params = createHttpParams(pageInfo);
    return this.singleRequestHelperService.get<ProjectWrapper>(environment.restUrl + 'organizations/' + organizationId + '/projects', {params});
  }

  setSearchableOrganization(organizationId: number, searchable: boolean): Observable<void> {
    return this.httpClient.post<void>(environment.internalRestUrl + 'organizations/' + organizationId + '/searchable/' + searchable, {});
  }

}

export type OrganizationKeywords = { [keyword: string]: number };

export interface OrganizationDetail {
  organizationId: number;
  organizationName: string;
  organizationAbbrev: string;
  organizationUnitIds: number[];
  organizationRank: number;
  organizationDescription: string;
  phone: string;
  email: string;
  street: string;
  city: string;
  web: string;
  countryName: string;
  countryCode: string;
  gdpr: boolean;
}

export interface PublicProfileVisibilityDto {
  visible: boolean;
  keywordsVisible: boolean;
  graphVisible: boolean;
  expertsVisible: boolean;
  outcomesVisible: boolean;
  projectsVisible: boolean;
}

export interface PublicOrganizationStatistics {
  numberOfOutcomes: number;
  numberOfExperts: number;
  numberOfProjects: number;
}

export type OrganizationImgType = 'LOGO' | 'COVER';

export type ListMode =
  | 'EXPERT'
  | 'RESEARCH_OUTCOME'
  | 'RESEARCH_PROJECT'
  | 'COMMERCIALIZATION_PROJECT'
  | 'LECTURE'
  | 'EQUIPMENT'
  | 'OPPORTUNITY'
  | 'ORGANIZATION_CHILDREN';
