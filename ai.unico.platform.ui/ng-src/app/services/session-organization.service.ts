import {Injectable} from '@angular/core';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {OrganizationWithRoles, SessionService} from 'ng-src/app/services/session.service';
import {localStorageGet, localStorageStore} from 'ng-src/app/shared/utils/localStorageUtil';
import {distinctUntilChanged, map, share} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SessionOrganizationService {

  private sessionOrganizationId$ = new BehaviorSubject<number | null>(null);
  private readonly LOCAL_STORAGE_KEY = 'activeOrganizationId';

  constructor(private sessionService: SessionService) {
    const organizationId = localStorageGet(this.LOCAL_STORAGE_KEY);
    this.updateActiveOrganization(organizationId);
  }

  getSessionOrganizationWithUpdates(): Observable<OrganizationWithRoles | null> {
    const sessionOrganizationId$ = this.sessionOrganizationId$.pipe(distinctUntilChanged());
    return combineLatest([this.sessionService.getCurrentSessionWithUpdates(), sessionOrganizationId$]).pipe(
      map(([session, organizationId]) => {
        if (session.organizationRoles.length == 0) return null;
        return session.organizationRoles.find(o => o.organizationId == organizationId) || session.organizationRoles[0];
      }),
      share()
    );
  }

  getActiveOrganizationIdWithUpdates(): Observable<number | null> {
    return this.sessionOrganizationId$.asObservable();
  }

  updateActiveOrganization(organizationId: number): void {
    localStorageStore(this.LOCAL_STORAGE_KEY, organizationId);
    this.sessionOrganizationId$.next(Number(organizationId));
  }

}
