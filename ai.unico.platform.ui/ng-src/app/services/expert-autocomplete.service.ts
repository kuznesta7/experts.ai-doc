import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import { map, shareReplay } from 'rxjs/operators';
import { createHttpParams } from 'ng-src/app/shared/utils/httpParamsUtil';
import { SingleRequestHelperService } from 'ng-src/app/shared/services/single-request-helper.service';
import { IdDescriptionOption } from 'ng-src/app/shared/interfaces/idDescriptionOption';
import { OrganizationForAutocomplete } from 'ng-src/app/services/organization-autocomplete.service';

@Injectable({
  providedIn: 'root'
})
export class ExpertAutocompleteService {

  constructor(private singleRequestHelperService: SingleRequestHelperService) {
  }

  findUserExperts(filter: ExpertLookupFilter): Observable<UserExpertPreviewDto[]> {
    const params = createHttpParams(filter);
    return this.singleRequestHelperService.get<UserExpertPreviewDto[]>(environment.internalRestUrl + 'experts', {params});
  }

  findWidgetUserExperts(filter: ExpertLookupFilter): Observable<UserExpertPreviewDto[]> {
    const params = createHttpParams(filter);
    return this.singleRequestHelperService.get<UserExpertPreviewDto[]>(environment.restUrl + 'widgets/experts/autocomplete', {params});
  }

  findUserExpertOptions(filter: ExpertLookupFilter): Observable<IdDescriptionOption[]> {
    return this.findUserExperts(filter).pipe(
      map(experts => experts.map(e => {
        const numberOfOrg = e.organizationDtos.length;
        const o = e.organizationDtos[0];
        const apendix = numberOfOrg > 1 ? ' +' + (numberOfOrg - 1) + 'more' : '';
        return {
          id: e.expertCode,
          description: e.name,
          subDescription: numberOfOrg === 0 ? '' : o.organizationAbbrev || o.organizationName + apendix
        };
      })),
      shareReplay()
    );
  }

  findWidgetUserExpertOptions(filter: ExpertLookupFilter): Observable<IdDescriptionOption[]> {
    return this.findWidgetUserExperts(filter).pipe(
      map(experts => experts.map(e => {
        const numberOfOrg = e.organizationDtos.length;
        const o = e.organizationDtos[0];
        const apendix = numberOfOrg > 1 ? ' +' + (numberOfOrg - 1) + 'more' : '';
        return {
          id: e.expertCode,
          description: e.name,
          subDescription: numberOfOrg === 0 ? '' : o.organizationAbbrev || o.organizationName + apendix
        };
      })),
      shareReplay()
    );
  }

}

export interface ExpertLookupFilter {
  query?: string;
  organizationIds?: number[];
  affiliatedOrganizationIds?: number[];
  disallowedOrganizationIds?: number[];
  expertCodes?: string[];
  userIds?: number[];
  expertIds?: number[];
  disallowedUserIds?: number[];
  disallowedExpertIds?: number[];
  disallowedExpertCodes?: string[];
  usersOnly?: boolean;
}

export interface UserExpertPreviewDto {
  expertCode: string;
  userId?: number;
  expertId?: number;
  name: string;
  username?: string;
  email?: string;
  claimable?: boolean;
  organizationDtos: OrganizationForAutocomplete[];
}
