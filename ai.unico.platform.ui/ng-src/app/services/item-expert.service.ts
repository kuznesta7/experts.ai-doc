import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'ng-src/environments/environment';
import {NotRegisteredExpertDTO} from "ng-src/app/interfaces/item-not-registered-expert";

@Injectable({
  providedIn: 'root'
})
export class ItemExpertService {

  constructor(private httpClient: HttpClient) {
  }

  addUser(itemId: number, userId: number): Observable<void> {
    return this.httpClient.put<void>(environment.internalRestUrl + `items/${itemId}/users/${userId}`, null);
  }

  removeUser(itemId: number, userId: number): Observable<void> {
    return this.httpClient.delete<void>(environment.internalRestUrl + `items/${itemId}/users/${userId}`);
  }

  addExpert(itemId: number, expertId: number): Observable<void> {
    return this.httpClient.put<void>(environment.internalRestUrl + `items/${itemId}/experts/${expertId}`, null);
  }

  removeExpert(itemId: number, expertId: number): Observable<void> {
    return this.httpClient.delete<void>(environment.internalRestUrl + `items/${itemId}/experts/${expertId}`);
  }
}
