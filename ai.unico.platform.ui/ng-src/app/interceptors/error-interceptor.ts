import {Injectable} from '@angular/core';
import {
  HTTP_INTERCEPTORS,
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import {FlashService} from 'ng-src/app/shared/services/flash.service';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {LoginService, SidebarMode} from 'ng-src/app/services/login.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private flashService: FlashService,
              private loginService: LoginService) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 400 && error.error?.msg) {
          this.flashService.error(error.error.msg);
        } else if (error.status === 401 || error.status === 403) {
          this.loginService.openLoginSidebar(SidebarMode.LOG_IN);
        } else if (error.status === 409 && error.error?.msg) {
          this.flashService.error(error.error?.msg.replace(/_/g, " "));
        } else if (error.status === 503) {
          this.flashService.error(error.error);
        } else {
          this.flashService.error('An unexpected error has occurred');
        }
        throw error;
      }));
  }

}

export const errorHttpInterceptorProvider = [
  {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}
];
