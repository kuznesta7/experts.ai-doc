export const environment = {
  production: true,
  restUrl: '/ai.unico.platform.rest/api/common/',
  internalRestUrl: '/ai.unico.platform.rest/api/internal/',
  assetsUrl: 'static/assets/',
  static: 'static/',
};
