package ai.unico.platform.ui;

import ai.unico.platform.util.GitProperties;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

public class ResourceServlet extends HttpServlet {

    public static final String INDEX_HTML = "index.html";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handleIndex(request, response);
    }

    private void handleIndex(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final ServletContext servletContext = getServletContext();
        final String code = GitProperties.getLastCommitId();

        try (final InputStream input = servletContext.getResourceAsStream(INDEX_HTML)) {
            final String string = IOUtils.toString(input, "UTF-8");
            final String newString = string.replaceAll("(\"resources/.*)\"", "$1?v=" + code + "\"");
            IOUtils.write(newString, response.getOutputStream(), "UTF-8");
        }
    }
}
