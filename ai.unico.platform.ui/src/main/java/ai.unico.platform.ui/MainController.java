package ai.unico.platform.ui;

import ai.unico.platform.util.GitProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("*")
public class MainController {

    @Value("${env.host}")
    private String host;

    @RequestMapping(method = RequestMethod.GET)
    public String getIndex(ModelMap modelMap) {
        final String code = GitProperties.getLastCommitId();
        modelMap.put("host", host);
        modelMap.put("version", code);
        modelMap.put("lastCommit", GitProperties.getValue("git.commit.time"));
        return "index";
    }
}
