package ai.unico.platform.ui;

import org.springframework.beans.factory.annotation.Value;

public class ResourceHelper {

    @Value("${env.host}")
    private String host;

    public String getHost() {
        return host;
    }
}
