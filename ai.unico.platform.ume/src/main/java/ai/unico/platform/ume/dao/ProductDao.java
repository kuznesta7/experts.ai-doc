package ai.unico.platform.ume.dao;

import ai.unico.platform.ume.entity.FeatureEntity;
import ai.unico.platform.ume.entity.ProductEntity;
import ai.unico.platform.ume.entity.ProductVariantEntity;

import java.util.List;

public interface ProductDao {

    ProductEntity findProduct(Long productId);

    ProductVariantEntity findProductVariant(Long productVariantId);

    List<ProductEntity> findProducts();

    List<FeatureEntity> findFeatures();

}
