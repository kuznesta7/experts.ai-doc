package ai.unico.platform.ume.dao;

import ai.unico.platform.ume.entity.UserRoleEntity;

import java.util.Collection;
import java.util.List;

public interface UserRoleDao {

    void saveRole(UserRoleEntity userRoleEntity);

    List<UserRoleEntity> findRolesForUser(Long userId);

    List<UserRoleEntity> findUserRolesForRole(String role);

    List<UserRoleEntity> findRolesForUsers(Collection<Long> userIds);
}
