package ai.unico.platform.ume.service;

import ai.unico.platform.ume.api.service.UserActivityService;
import ai.unico.platform.ume.dao.UserActivityDao;
import ai.unico.platform.ume.entity.UserActivityEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

public class UserActivityServiceImpl implements UserActivityService {

    @Autowired
    private UserActivityDao userActivityDao;

    @Override
    @Transactional
    public void saveActivity(Long userId, String type, String note) {
        final UserActivityEntity userActivityEntity = new UserActivityEntity();
        userActivityEntity.setActivityDate(new Date());
        userActivityEntity.setActivityType(type);
        userActivityEntity.setUserId(userId);
        userActivityDao.saveUserActivity(userActivityEntity);
    }

    @Override
    @Transactional
    public Date getLastLogin(Long userId) {
        final UserActivityEntity lastActivity = userActivityDao.findLastActivity(userId, AUTHENTICATION_SUCCEEDED, new Date(System.currentTimeMillis() - 10000));
        if (lastActivity == null) return null;
        return lastActivity.getActivityDate();
    }

}
