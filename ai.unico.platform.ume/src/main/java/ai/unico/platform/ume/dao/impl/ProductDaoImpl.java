package ai.unico.platform.ume.dao.impl;

import ai.unico.platform.ume.dao.ProductDao;
import ai.unico.platform.ume.entity.FeatureEntity;
import ai.unico.platform.ume.entity.ProductEntity;
import ai.unico.platform.ume.entity.ProductVariantEntity;
import ai.unico.platform.ume.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class ProductDaoImpl implements ProductDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public ProductEntity findProduct(Long productId) {
        return entityManager.find(ProductEntity.class, productId);
    }

    @Override
    public ProductVariantEntity findProductVariant(Long productVariantId) {
        return entityManager.find(ProductVariantEntity.class, productVariantId);
    }

    @Override
    public List<ProductEntity> findProducts() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProductEntity.class, "product");
        criteria.add(Restrictions.eq("deleted", false));
        return (List<ProductEntity>) criteria.list();
    }

    @Override
    public List<FeatureEntity> findFeatures() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(FeatureEntity.class, "feature");
        criteria.add(Restrictions.eq("deleted", false));
        return (List<FeatureEntity>) criteria.list();
    }
}
