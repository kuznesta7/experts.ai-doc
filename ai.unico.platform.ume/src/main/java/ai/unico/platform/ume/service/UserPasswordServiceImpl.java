package ai.unico.platform.ume.service;

import ai.unico.platform.ume.api.dto.UserCredentialsDto;
import ai.unico.platform.ume.api.dto.UserDto;
import ai.unico.platform.ume.api.service.UserPasswordService;
import ai.unico.platform.ume.dao.TokenDao;
import ai.unico.platform.ume.dao.UserCredentialsDao;
import ai.unico.platform.ume.dao.UserDao;
import ai.unico.platform.ume.entity.TokenEntity;
import ai.unico.platform.ume.entity.UserCredentialsEntity;
import ai.unico.platform.ume.entity.UserEntity;
import ai.unico.platform.ume.util.TrackableUtil;
import ai.unico.platform.ume.util.UserUtil;
import ai.unico.platform.util.exception.InvalidParameterException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.OutdatedEntityException;
import ai.unico.platform.util.model.UserContext;
import org.joda.time.DateTime;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

public class UserPasswordServiceImpl implements UserPasswordService {

    public static final String SET_PASSWORD_TOKEN = "SET_PASSWORD_TOKEN";

    public static final int TOKEN_VALIDITY_DAYS = 7;

    private static final Logger LOG = LoggerFactory.getLogger(UserPasswordService.class);

    @Autowired
    private TokenDao tokenDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserCredentialsDao userCredentialsDao;

    @Override
    @Transactional
    public UserDto doSetPassword(String email, String token, String newPassword, UserContext userContext) throws OutdatedEntityException, NonexistentEntityException {
        final TokenEntity tokenEntity = tokenDao.findByTokenValue(token);
        validatePasswordToken(email, token);
        tokenEntity.setUsed(new Date());
        TrackableUtil.fillUpdate(tokenEntity, null);

        return doSetPassword(email, newPassword, userContext);
    }

    @Override
    @Transactional
    public UserDto doSetPassword(String email, String newPassword, UserContext userContext) throws OutdatedEntityException, NonexistentEntityException {
        validateNewPassword(newPassword);
        final UserCredentialsEntity userCredentialsEntity;
        final UserEntity userEntity = userDao.findByEmail(email);
        final UserCredentialsEntity userCredentialsEntityTmp = userCredentialsDao.find(userEntity.getUserId(), UserCredentialsDto.TYPE_PASSWORD);
        if (userCredentialsEntityTmp == null) {
            userCredentialsEntity = new UserCredentialsEntity();
            userCredentialsEntity.setType(UserCredentialsDto.TYPE_PASSWORD);
            userCredentialsEntity.setUserEntity(userEntity);
            userCredentialsEntity.setIdentification(userEntity.getUserId().toString());
        } else {
            userCredentialsEntity = userCredentialsEntityTmp;
        }
        userCredentialsEntity.setSecret(BCrypt.hashpw(newPassword, BCrypt.gensalt()));

        if (userCredentialsEntity.getUserCredentialsId() == null) userCredentialsDao.save(userCredentialsEntity);

        TrackableUtil.fillCreate(userCredentialsEntity, userContext);
        return UserUtil.convert(userEntity);
    }

    @Override
    @Transactional
    public void doChangePassword(Long userId, String currentPassword, String newPassword, UserContext userContext) throws NonexistentEntityException {
        UserCredentialsEntity userCredentialsEntity = userCredentialsDao.find(userId, UserCredentialsDto.TYPE_PASSWORD);
        if (userCredentialsEntity == null) throw new NonexistentEntityException(UserCredentialsEntity.class);
        if (!BCrypt.checkpw(currentPassword, userCredentialsEntity.getSecret()))
            throw new InvalidParameterException("Current password is not correct", "User " + userId + " trying to change password, but enter invalid current password");

        validateNewPassword(newPassword);

        TrackableUtil.fillUpdate(userCredentialsEntity, userContext);
        userCredentialsEntity.setSecret(BCrypt.hashpw(newPassword, BCrypt.gensalt()));
    }

    @Override
    @Transactional
    public String createPasswordSetToken(Long userId) {
        final TokenEntity tokenEntity = new TokenEntity();
        do {
            final String tokenValue = UUID.randomUUID().toString();
            tokenEntity.setTokenValue(tokenValue);
        } while (tokenDao.doesTokenValueExist(tokenEntity.getTokenValue()));

        tokenEntity.setIdentification(userId.toString());
        tokenEntity.setType(SET_PASSWORD_TOKEN);
        tokenEntity.setValidTo(DateTime.now().plusDays(TOKEN_VALIDITY_DAYS).toDate());

        TrackableUtil.fillCreate(tokenEntity, null);

        tokenDao.save(tokenEntity);

        return tokenEntity.getTokenValue();
    }

    @Override
    @Transactional
    public UserDto validatePasswordToken(String email, String token) {
        final UserEntity userEntity = userDao.findByEmail(email);
        final TokenEntity tokenEntity = tokenDao.findByTokenValue(token);
        if (tokenEntity == null
                || userEntity == null
                || !userEntity.getUserId().toString().equals(tokenEntity.getIdentification())) {
            throw new NonexistentEntityException(TokenEntity.class, "INVALID_TOKEN_EXCEPTION");
        }
        if (tokenEntity.getValidTo().before(new Date())) {
            throw new OutdatedEntityException("Authorization token is outdated. Please use \"forgotten password\" function in login section.", "Somebody tries to use token belonging to user with id " + tokenEntity.getIdentification() + " which is valid to " + tokenEntity.getValidTo());
        }
        if (tokenEntity.getUsed() != null) {
            throw new OutdatedEntityException("Authorization token has been already used. Please use \"forgotten password\" function in login section.", "Somebody tries to use token belonging to user with id " + tokenEntity.getIdentification() + " which has been already used on " + tokenEntity.getUsed());
        }
        return UserUtil.convert(userEntity);
    }

    public void validateNewPassword(String newPassword) {
        if (newPassword.length() < 8)
            throw new InvalidParameterException("New password is too short. Password must be at least 8 characters long.", "Trying to set short paswword");
        if (!newPassword.matches("\\S*[!@#$%^&*(),.?\":{}|<>+-_=]\\S*"))
            throw new InvalidParameterException("New password must contain at least one special character.", "Trying to set paswword without special character");
        if (!newPassword.matches("\\S*[0-9]\\S*"))
            throw new InvalidParameterException("New password must contain at least one number.", "Trying to set paswword without number");
        if (!newPassword.matches("\\S*[A-Za-z]\\S*"))
            throw new InvalidParameterException("New password must contain at least one text character.", "Trying to set paswword without text char");
        if (!newPassword.matches("^[!-~]*$")) {
            throw new InvalidParameterException("New password contains invalid characters (!,- or ~).", "Trying to set paswword with invalid chars");
        }
    }
}
