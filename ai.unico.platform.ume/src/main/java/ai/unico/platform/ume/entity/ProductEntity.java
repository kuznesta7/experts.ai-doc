package ai.unico.platform.ume.entity;

import ai.unico.platform.util.enums.Role;

import javax.persistence.*;
import java.util.Set;

@Entity
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long productId;

    private String productName;

    private String productDescription;

    private Integer roleTrialDuration;

    private Integer index;

    private boolean deleted;

    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToMany(mappedBy = "productEntity")
    private Set<ProductFeatureEntity> productFeatureEntities;

    @OneToMany(mappedBy = "productEntity")
    private Set<ProductVariantEntity> productVariantEntities;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Integer getRoleTrialDuration() {
        return roleTrialDuration;
    }

    public void setRoleTrialDuration(Integer roleTrialDuration) {
        this.roleTrialDuration = roleTrialDuration;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Set<ProductFeatureEntity> getProductFeatureEntities() {
        return productFeatureEntities;
    }

    public void setProductFeatureEntities(Set<ProductFeatureEntity> productFeatureEntities) {
        this.productFeatureEntities = productFeatureEntities;
    }

    public Set<ProductVariantEntity> getProductVariantEntities() {
        return productVariantEntities;
    }

    public void setProductVariantEntities(Set<ProductVariantEntity> productVariantEntities) {
        this.productVariantEntities = productVariantEntities;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
