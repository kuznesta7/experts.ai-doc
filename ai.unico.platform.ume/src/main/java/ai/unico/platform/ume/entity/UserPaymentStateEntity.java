package ai.unico.platform.ume.entity;

import ai.unico.platform.ume.api.enums.PaymentState;
import ai.unico.platform.ume.entity.superclass.Trackable;

import javax.persistence.*;

@Entity
public class UserPaymentStateEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userPaymentStateId;

    @Enumerated(EnumType.STRING)
    private PaymentState paymentState;

    @ManyToOne
    private UserOrderEntity userOrderEntity;

    public Long getUserPaymentStateId() {
        return userPaymentStateId;
    }

    public void setUserPaymentStateId(Long userPaymentStateId) {
        this.userPaymentStateId = userPaymentStateId;
    }

    public PaymentState getPaymentState() {
        return paymentState;
    }

    public void setPaymentState(PaymentState paymentState) {
        this.paymentState = paymentState;
    }

    public UserOrderEntity getUserOrderEntity() {
        return userOrderEntity;
    }

    public void setUserOrderEntity(UserOrderEntity userOrderEntity) {
        this.userOrderEntity = userOrderEntity;
    }
}
