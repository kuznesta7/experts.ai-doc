package ai.unico.platform.ume.dao.impl;

import ai.unico.platform.ume.dao.UserOrderDao;
import ai.unico.platform.ume.entity.UserOrderEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class UserOrderDaoImpl implements UserOrderDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(UserOrderEntity userOrderEntity) {
        entityManager.persist(userOrderEntity);
    }

    @Override
    public UserOrderEntity findOrder(Long userOrderId) {
        return entityManager.find(UserOrderEntity.class, userOrderId);
    }
}
