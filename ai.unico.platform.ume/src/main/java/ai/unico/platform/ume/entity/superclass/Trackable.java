package ai.unico.platform.ume.entity.superclass;

import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
public class Trackable {

    private Date dateInsert;

    private Date dateUpdate;

    private Long userInsert;

    private Long userUpdate;

    public Date getDateInsert() {
        return dateInsert;
    }

    public void setDateInsert(Date dateInsert) {
        this.dateInsert = dateInsert;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserInsert() {
        return userInsert;
    }

    public void setUserInsert(Long userInsert) {
        this.userInsert = userInsert;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

}
