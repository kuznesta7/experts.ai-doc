package ai.unico.platform.ume.service;

import ai.unico.platform.ume.api.dto.BillingInfoDto;
import ai.unico.platform.ume.api.service.BillingInfoService;
import ai.unico.platform.ume.dao.BillingInfoDao;
import ai.unico.platform.ume.dao.UserDao;
import ai.unico.platform.ume.entity.BillingInfoEntity;
import ai.unico.platform.ume.entity.UserEntity;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public class BillingInfoServiceImpl implements BillingInfoService {

    @Autowired
    private BillingInfoDao billingInfoDao;

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional
    public Long createBillingInfo(BillingInfoDto billingInfoDto, UserContext userContext) {
        final UserEntity userEntity = userDao.find(billingInfoDto.getUserId());
        if (userEntity == null) throw new NonexistentEntityException(UserEntity.class);
        final BillingInfoEntity billingInfoEntity = new BillingInfoEntity();
        billingInfoEntity.setUserEntity(userEntity);
        fillEntity(billingInfoDto, billingInfoEntity);
        billingInfoDao.save(billingInfoEntity);
        setDefault(billingInfoDto.getUserId(), billingInfoEntity.getBillingInfoId(), billingInfoDto.isDefaultBillingInfo(), userContext);
        return billingInfoEntity.getBillingInfoId();
    }

    @Override
    @Transactional
    public Long updateBillingInfo(BillingInfoDto billingInfoDto, UserContext userContext) {
        final BillingInfoEntity billingInfoEntity = billingInfoDao.find(billingInfoDto.getBillingInfoId());
        if (billingInfoEntity == null || !billingInfoDto.getUserId().equals(billingInfoEntity.getUserEntity().getUserId()))
            throw new NonexistentEntityException(BillingInfoEntity.class);
        billingInfoEntity.setDeleted(true);
        return createBillingInfo(billingInfoDto, userContext);
    }

    @Override
    @Transactional
    public void deleteBillingInfo(Long billingInfoId, UserContext userContext) {
        final BillingInfoEntity billingInfoEntity = billingInfoDao.find(billingInfoId);
        if (billingInfoEntity == null) throw new NonexistentEntityException(BillingInfoEntity.class);
        setDefault(billingInfoEntity.getUserEntity().getUserId(), billingInfoId, billingInfoEntity.isDefaultBillingInfo(), userContext);
        billingInfoEntity.setDeleted(true);
    }

    @Override
    @Transactional
    public void setDefault(Long userId, Long billingInfoId, boolean favorite, UserContext userContext) {
        final List<BillingInfoEntity> forUser = billingInfoDao.findForUser(userId);
        if (forUser.size() == 0) return;
        if (forUser.stream().noneMatch(billingInfoEntity -> billingInfoEntity.getBillingInfoId().equals(billingInfoId))) {
            throw new NonexistentEntityException(BillingInfoEntity.class);
        }
        if (favorite) {
            for (BillingInfoEntity infoEntity : forUser) {
                if (infoEntity.getBillingInfoId().equals(billingInfoId)) {
                    infoEntity.setDefaultBillingInfo(true);
                } else infoEntity.setDefaultBillingInfo(false);
            }
        } else if (forUser.stream().noneMatch(BillingInfoEntity::isDefaultBillingInfo)) {
            forUser.get(0).setDefaultBillingInfo(true);
        }
    }

    @Override
    @Transactional
    public List<BillingInfoDto> findBillingInfoForUser(Long userId) {
        final List<BillingInfoEntity> billingInfoEntities = billingInfoDao.findForUser(userId);
        final List<BillingInfoDto> billingInfoDtos = new ArrayList<>();
        for (BillingInfoEntity billingInfoEntity : billingInfoEntities) {
            final BillingInfoDto dto = new BillingInfoDto();
            dto.setAddress(billingInfoEntity.getAddress());
            dto.setCompanyName(billingInfoEntity.getCompanyName());
            dto.setCompanyRegistrationNumber(billingInfoEntity.getCompanyRegistrationNumber());
            dto.setCompanyVatin(billingInfoEntity.getCompanyVatin());
            dto.setEmail(billingInfoEntity.getEmail());
            dto.setPhone(billingInfoEntity.getPhone());
            dto.setResponsiblePersonName(billingInfoEntity.getResponsiblePersonName());
            dto.setDefaultBillingInfo(billingInfoEntity.isDefaultBillingInfo());
            dto.setBillingInfoId(billingInfoEntity.getBillingInfoId());
            dto.setUserId(billingInfoEntity.getUserEntity().getUserId());
            billingInfoDtos.add(dto);
        }
        billingInfoDtos.sort((o1, o2) -> {
            if (o1.isDefaultBillingInfo()) return -1;
            else if (o2.isDefaultBillingInfo()) return 1;
            return 0;
        });
        return billingInfoDtos;
    }

    private void fillEntity(BillingInfoDto billingInfoDto, BillingInfoEntity billingInfoEntity) {
        if (billingInfoDto.isDefaultBillingInfo()) {
            final List<BillingInfoEntity> existingBillingInfos = billingInfoDao.findForUser(billingInfoDto.getUserId());
            for (BillingInfoEntity existingBillingInfo : existingBillingInfos) {
                existingBillingInfo.setDefaultBillingInfo(false);
            }
        }
        billingInfoEntity.setAddress(billingInfoDto.getAddress());
        billingInfoEntity.setCompanyName(billingInfoDto.getCompanyName());
        billingInfoEntity.setCompanyRegistrationNumber(billingInfoDto.getCompanyRegistrationNumber());
        billingInfoEntity.setCompanyVatin(billingInfoDto.getCompanyVatin());
        billingInfoEntity.setEmail(billingInfoDto.getEmail());
        billingInfoEntity.setPhone(billingInfoDto.getPhone());
        billingInfoEntity.setResponsiblePersonName(billingInfoDto.getResponsiblePersonName());
        billingInfoEntity.setDefaultBillingInfo(billingInfoDto.isDefaultBillingInfo());
    }
}
