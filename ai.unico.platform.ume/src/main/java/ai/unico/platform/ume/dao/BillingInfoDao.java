package ai.unico.platform.ume.dao;

import ai.unico.platform.ume.entity.BillingInfoEntity;

import java.util.List;

public interface BillingInfoDao {

    void save(BillingInfoEntity billingInfoEntity);

    BillingInfoEntity find(Long billingInfoId);

    List<BillingInfoEntity> findForUser(Long userId);

}
