package ai.unico.platform.ume.dao.impl;

import ai.unico.platform.ume.dao.UserRoleDao;
import ai.unico.platform.ume.entity.UserRoleEntity;
import ai.unico.platform.ume.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class UserRoleDaoImpl implements UserRoleDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void saveRole(UserRoleEntity userRoleEntity) {
        entityManager.persist(userRoleEntity);
    }

    @Override
    public List<UserRoleEntity> findRolesForUser(Long userId) {
        return findRolesForUsers(Collections.singleton(userId));
    }

    @Override
    public List<UserRoleEntity> findUserRolesForRole(String role) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserRoleEntity.class, "userRole");
        criteria.add(Restrictions.eq("role", role));
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.ge("validTo", new Date()));
        return (List<UserRoleEntity>) criteria.list();
    }

    @Override
    public List<UserRoleEntity> findRolesForUsers(Collection<Long> userIds) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserRoleEntity.class, "userRole");
        criteria.add(Restrictions.in("userRole.userEntity.id", userIds));
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.or(Restrictions.isNull("validTo"), Restrictions.ge("validTo", new Date())));
        return (List<UserRoleEntity>) criteria.list();

    }
}
