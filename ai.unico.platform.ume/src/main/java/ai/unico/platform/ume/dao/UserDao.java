package ai.unico.platform.ume.dao;

import ai.unico.platform.ume.entity.UserEntity;
import ai.unico.platform.util.enums.Role;
import ai.unico.platform.util.exception.NonexistentEntityException;

import java.util.Collection;
import java.util.List;

public interface UserDao {

    UserEntity findByEmail(String email) throws NonexistentEntityException;

    UserEntity findByUsername(String username) throws NonexistentEntityException;

    UserEntity find(Long id) throws NonexistentEntityException;

    List<UserEntity> getAll();

    Long createUser(UserEntity userEntity);

    List<UserEntity> findUsersWithRoles(Collection<Role> roles);

}
