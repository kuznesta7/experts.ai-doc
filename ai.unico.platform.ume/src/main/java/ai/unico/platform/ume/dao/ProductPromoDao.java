package ai.unico.platform.ume.dao;

import ai.unico.platform.ume.entity.ProductPromoEntity;

import java.util.List;

public interface ProductPromoDao {

    ProductPromoEntity find(Long promoCodeId);

    ProductPromoEntity find(String promoCode);

    ProductPromoEntity findValid(String promoCode);

    List<ProductPromoEntity> findAll();

    List<ProductPromoEntity> findValid();

    void save(ProductPromoEntity productPromoEntity);

}
