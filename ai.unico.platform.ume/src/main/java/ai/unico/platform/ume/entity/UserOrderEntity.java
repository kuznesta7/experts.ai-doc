package ai.unico.platform.ume.entity;

import ai.unico.platform.ume.entity.superclass.Trackable;

import javax.persistence.*;
import java.util.Set;

@Entity
public class UserOrderEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userOrderId;

    private Long paymentId;

    private String gwUrl;

    private Double orderTotal;

    private boolean delivered;

    @ManyToOne
    @JoinColumn(name = "product_variant_id")
    private ProductVariantEntity productVariantEntity;

    @ManyToOne
    @JoinColumn(name = "user_role_id")
    private UserRoleEntity userRoleEntity;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    @ManyToOne
    @JoinColumn(name = "billing_info_id")
    private BillingInfoEntity billingInfoEntity;

    @OneToMany(mappedBy = "userOrderEntity")
    private Set<UserPaymentStateEntity> paymentStateEntities;

    public Long getUserOrderId() {
        return userOrderId;
    }

    public void setUserOrderId(Long userOrderId) {
        this.userOrderId = userOrderId;
    }

    public ProductVariantEntity getProductVariantEntity() {
        return productVariantEntity;
    }

    public void setProductVariantEntity(ProductVariantEntity productVariantEntity) {
        this.productVariantEntity = productVariantEntity;
    }

    public UserRoleEntity getUserRoleEntity() {
        return userRoleEntity;
    }

    public void setUserRoleEntity(UserRoleEntity userRoleEntity) {
        this.userRoleEntity = userRoleEntity;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public String getGwUrl() {
        return gwUrl;
    }

    public void setGwUrl(String gwUrl) {
        this.gwUrl = gwUrl;
    }

    public Set<UserPaymentStateEntity> getPaymentStateEntities() {
        return paymentStateEntities;
    }

    public void setPaymentStateEntities(Set<UserPaymentStateEntity> paymentStateEntities) {
        this.paymentStateEntities = paymentStateEntities;
    }

    public Double getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(Double orderTotal) {
        this.orderTotal = orderTotal;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public BillingInfoEntity getBillingInfoEntity() {
        return billingInfoEntity;
    }

    public void setBillingInfoEntity(BillingInfoEntity billingInfoEntity) {
        this.billingInfoEntity = billingInfoEntity;
    }
}
