package ai.unico.platform.ume.service;

import ai.unico.platform.ume.api.dto.PromoCodeDto;
import ai.unico.platform.ume.api.exception.NonexistentPromoCodeException;
import ai.unico.platform.ume.api.exception.PromoAlreadyUsedException;
import ai.unico.platform.ume.api.exception.PromoExpiredException;
import ai.unico.platform.ume.api.service.PromoService;
import ai.unico.platform.ume.api.service.UserRoleService;
import ai.unico.platform.ume.dao.ProductDao;
import ai.unico.platform.ume.dao.ProductPromoDao;
import ai.unico.platform.ume.dao.UserDao;
import ai.unico.platform.ume.entity.ProductEntity;
import ai.unico.platform.ume.entity.ProductPromoEntity;
import ai.unico.platform.ume.entity.UserEntity;
import ai.unico.platform.ume.entity.UserRoleEntity;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.InvalidParameterException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

public class PromoServiceImpl implements PromoService {

    @Autowired
    private ProductPromoDao productPromoDao;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserRoleService userRoleService;

    @Override
    @Transactional
    public Long createPromoCode(PromoCodeDto promoCodeDto, UserContext userContext) {
        final String promoCode = promoCodeDto.getPromoCode();
        if (promoCode == null || promoCode.isEmpty()) throw new InvalidParameterException("invalid promo code");

        final ProductPromoEntity valid = productPromoDao.findValid(promoCode);
        if (valid != null) throw new EntityAlreadyExistsException("promo code " + promoCode + " already exists");

        final ProductEntity product = productDao.findProduct(promoCodeDto.getProductId());
        if (product == null) throw new NonexistentEntityException(ProductEntity.class);

        final ProductPromoEntity productPromoEntity = new ProductPromoEntity();
        productPromoEntity.setDuration(promoCodeDto.getDuration());
        productPromoEntity.setProductEntity(product);
        productPromoEntity.setOneTime(promoCodeDto.isOneTime());
        productPromoEntity.setValidUntil(promoCodeDto.getValidUntil());
        productPromoEntity.setPromoCode(promoCode);
        productPromoDao.save(productPromoEntity);
        return productPromoEntity.getProductPromoId();
    }

    @Override
    @Transactional
    public List<PromoCodeDto> findPromoCodes() {
        final List<ProductPromoEntity> all = productPromoDao.findAll();
        final List<PromoCodeDto> promoCodeDtos = new ArrayList<>();
        for (ProductPromoEntity productPromoEntity : all) {
            promoCodeDtos.add(convert(productPromoEntity));
        }
        return promoCodeDtos;
    }

    @Override
    @Transactional
    public PromoCodeDto findPromoCode(String promoCode) {
        final ProductPromoEntity validPromoCode = findValidPromoCode(promoCode);
        return convert(validPromoCode);
    }

    @Override
    @Transactional
    public void deletePromoCode(Long promoCodeId, UserContext userContext) {
        final ProductPromoEntity productPromoEntity = productPromoDao.find(promoCodeId);
        if (productPromoEntity == null) throw new NonexistentPromoCodeException();
        productPromoEntity.setDeleted(true);
    }

    @Override
    @Transactional
    public void usePromoCode(String promoCode, Long userId, UserContext userContext) {
        final UserEntity userEntity = userDao.find(userId);
        final Set<UserRoleEntity> userRoleEntities = userEntity.getUserRoleEntities();
        final ProductPromoEntity validPromoEntity = findValidPromoCode(promoCode);
        for (UserRoleEntity userRoleEntity : userRoleEntities) {
            final ProductPromoEntity productPromoEntity = userRoleEntity.getProductPromoEntity();
            if (productPromoEntity == null) continue;
            if (productPromoEntity.equals(validPromoEntity)) throw new PromoAlreadyUsedException();
        }
        final ProductEntity productEntity = validPromoEntity.getProductEntity();
        final Calendar cal = Calendar.getInstance();
        final boolean infinityDuration = validPromoEntity.getDuration() != null;
        if (infinityDuration) {
            cal.setTime(new Date());
            cal.add(Calendar.DATE, validPromoEntity.getDuration());
        }
        userRoleService.addRole(userId, productEntity.getRole(), infinityDuration ? null : cal.getTime(), validPromoEntity.getProductPromoId(), userContext);
        if (validPromoEntity.isOneTime()) {
            validPromoEntity.setUsed(true);
        }
    }

    private ProductPromoEntity findValidPromoCode(String promoCode) {
        final ProductPromoEntity validPromoEntity = productPromoDao.findValid(promoCode);
        if (validPromoEntity == null) {
            final ProductPromoEntity invalidPromoEntity = productPromoDao.find(promoCode);
            if (invalidPromoEntity == null) throw new NonexistentPromoCodeException();
            if (invalidPromoEntity.isUsed()) throw new PromoAlreadyUsedException();
            throw new PromoExpiredException();
        }
        return validPromoEntity;
    }

    private PromoCodeDto convert(ProductPromoEntity productPromoEntity) {
        final PromoCodeDto promoCodeDto = new PromoCodeDto();
        promoCodeDto.setPromoCode(productPromoEntity.getPromoCode());
        promoCodeDto.setDuration(productPromoEntity.getDuration());
        promoCodeDto.setOneTime(productPromoEntity.isOneTime());
        promoCodeDto.setUsed(productPromoEntity.isUsed());
        promoCodeDto.setValidUntil(productPromoEntity.getValidUntil());
        promoCodeDto.setProductId(productPromoEntity.getProductEntity().getProductId());
        promoCodeDto.setProductPromoId(productPromoEntity.getProductPromoId());
        return promoCodeDto;
    }
}
