package ai.unico.platform.ume.util;

import ai.unico.platform.ume.api.dto.UserDto;
import ai.unico.platform.ume.entity.UserEntity;

public class UserUtil {

    public static UserDto convert(UserEntity userEntity) {
        final UserDto userDto = new UserDto();
        userDto.setId(userEntity.getUserId());
        userDto.setUsername(userEntity.getUsername());
        userDto.setEmail(userEntity.getEmail());
        userDto.setFirstName(userEntity.getFirstName());
        userDto.setLastName(userEntity.getLastName());
        userDto.setDeleted(userEntity.isDeleted());
        userDto.setTermsOfUseAccepted(userEntity.isTermsOfUseAccepted());
        return userDto;
    }

    public static UserEntity convert(UserDto userDto) {
        final UserEntity userEntity = new UserEntity();
        fillUserEntity(userEntity, userDto);
        return userEntity;
    }

    public static void fillUserEntity(UserEntity userEntity, UserDto userDto) {
        userEntity.setUsername(userDto.getUsername());
        userEntity.setEmail(userDto.getEmail());
        userEntity.setFirstName(userDto.getFirstName());
        userEntity.setLastName(userDto.getLastName());
        userEntity.setDeleted(userDto.isDeleted());
        userEntity.setTermsOfUseAccepted(userDto.isTermsOfUseAccepted());
    }

}
