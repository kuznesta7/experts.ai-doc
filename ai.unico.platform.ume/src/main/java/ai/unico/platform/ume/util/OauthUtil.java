package ai.unico.platform.ume.util;

import ai.unico.platform.util.exception.UserUnauthorizedException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class OauthUtil {

    public static Map<String, String> getToken(String tokenUrl, Map<String, String> parameters) throws IOException {
        final URL url = new URL(tokenUrl);
        final HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        final String paramsString = getParamsString(parameters);

        con.setDoOutput(true);
        final DataOutputStream out = new DataOutputStream(con.getOutputStream());
        out.writeBytes(paramsString);
        out.flush();
        out.close();
        con.disconnect();

        final BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        final StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }

        in.close();

        final TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {
        };
        final ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(content.toString(), typeRef);
    }

    public static String getUserInfo(String userInfoUrl, String accessToken) throws IOException, UserUnauthorizedException {
        if (accessToken == null) throw new UserUnauthorizedException();
        final URL getUserInfoUrl = new URL(userInfoUrl);
        HttpURLConnection con2 = (HttpURLConnection) getUserInfoUrl.openConnection();
        con2.setRequestMethod("GET");
        BufferedReader in2 = new BufferedReader(
                new InputStreamReader(con2.getInputStream(), StandardCharsets.UTF_8));
        String inputLine2;
        StringBuffer content = new StringBuffer();
        while ((inputLine2 = in2.readLine()) != null) {
            content.append(inputLine2);
        }

        return content.toString();
    }

    private static String getParamsString(Map<String, String> params) throws UnsupportedEncodingException {
        final StringBuilder result = new StringBuilder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            result.append("&");
        }

        String resultString = result.toString();
        return resultString.length() > 0
                ? resultString.substring(0, resultString.length() - 1)
                : resultString;
    }

    public static byte[] getImage(URL url) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream is = null;
        try {
            is = url.openStream();
            byte[] byteChunk = new byte[4096]; // Or whatever size you want to read in at a time.
            int n;

            while ((n = is.read(byteChunk)) > 0) {
                baos.write(byteChunk, 0, n);
            }
        } catch (IOException e) {
            System.err.printf("Failed while reading bytes from %s: %s", url.toExternalForm(), e.getMessage());
            e.printStackTrace();
            // Perform any other exception handling that's appropriate.
        } finally {
            if (is != null) {
                is.close();
            }
        }
        return baos.toByteArray();
    }

}
