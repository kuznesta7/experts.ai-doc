package ai.unico.platform.ume.dao.impl;

import ai.unico.platform.ume.dao.TokenDao;
import ai.unico.platform.ume.entity.TokenEntity;
import ai.unico.platform.ume.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class TokenDaoImpl implements TokenDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public TokenEntity findByTokenValue(String tokenValue) {
        final TokenEntity tokenEntity = findTokenEntityByValue(tokenValue);

        return tokenEntity;
    }

    @Override
    public Boolean doesTokenValueExist(String value) {
        return (findTokenEntityByValue(value) != null);
    }

    @Override
    public void save(TokenEntity tokenEntity) {
        entityManager.persist(tokenEntity);
    }


    private TokenEntity findTokenEntityByValue(String value) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(TokenEntity.class, "tokenEntity");
        criteria.add(Restrictions.eq("tokenEntity.tokenValue", value));
        return (TokenEntity) criteria.uniqueResult();
    }

    ;
}
