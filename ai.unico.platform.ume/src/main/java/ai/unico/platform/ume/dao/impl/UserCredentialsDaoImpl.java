package ai.unico.platform.ume.dao.impl;

import ai.unico.platform.ume.dao.UserCredentialsDao;
import ai.unico.platform.ume.entity.UserCredentialsEntity;
import ai.unico.platform.ume.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class UserCredentialsDaoImpl implements UserCredentialsDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public UserCredentialsEntity find(Long userId, String type) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserCredentialsEntity.class, "userCredentialsEntity");
        criteria.add(Restrictions.eq("userCredentialsEntity.userEntity.userId", userId));
        criteria.add(Restrictions.eq("userCredentialsEntity.type", type));
        return (UserCredentialsEntity) criteria.uniqueResult();
    }

    @Override
    public void save(UserCredentialsEntity userCredentialsEntity) {
        entityManager.persist(userCredentialsEntity);
    }
}
