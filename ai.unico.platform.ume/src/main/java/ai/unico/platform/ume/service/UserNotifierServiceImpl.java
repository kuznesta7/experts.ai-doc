package ai.unico.platform.ume.service;

import ai.unico.platform.store.api.service.email.EmailService;
import ai.unico.platform.ume.api.service.UserNotifierService;
import ai.unico.platform.util.ServiceRegistryUtil;
import org.springframework.beans.factory.annotation.Value;

import java.util.HashMap;
import java.util.Map;

public class UserNotifierServiceImpl implements UserNotifierService {


    @Value("${env.host}")
    private String host;

    @Override
    public void sendResetPasswordLink(String email, String token) {
        final Map<String, String> variables = new HashMap<>();
        final EmailService emailService = ServiceRegistryUtil.getService(EmailService.class);
        variables.put("url", createLinkToSetPassword(email, token));
        emailService.sendEmailWithTemplate(email, "apply-for-reset-password", variables);
    }

    @Override
    public void sendRegistrationSetPasswordLink(String email, String token) {
        final Map<String, String> variables = new HashMap<>();
        final EmailService emailService = ServiceRegistryUtil.getService(EmailService.class);
        variables.put("url", createLinkToSetPassword(email, token));
        variables.put("email", email);
        emailService.sendEmailWithTemplate(email, "new-user", variables);
    }

    @Override
    public void sendInvitationSetPasswordLink(String email, String passwordSetToken, String invitingUserName) {
        final Map<String, String> variables = new HashMap<>();
        final EmailService emailService = ServiceRegistryUtil.getService(EmailService.class);
        variables.put("url", createLinkToSetPassword(email, passwordSetToken));
        variables.put("email", email);
        variables.put("invitingUserName", invitingUserName);
        emailService.sendEmailWithTemplate(email, "user-invitation", variables);
    }

    private String createLinkToSetPassword(String email, String token) {
        return host + "reset-password?token=" + token + "&email=" + email;
    }
}
