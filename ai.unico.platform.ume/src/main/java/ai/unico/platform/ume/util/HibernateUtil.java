package ai.unico.platform.ume.util;

import ai.unico.platform.util.enums.OrderDirection;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;

public class HibernateUtil {
    public static void setOrder(Criteria criteria, String columnToSort, OrderDirection direction) {
        if (direction != null) {
            switch (direction) {
                case ASC:
                    criteria.addOrder(Order.asc(columnToSort));
                    break;
                case DESC:
                    criteria.addOrder(Order.desc(columnToSort));
                    break;
            }
        } else {
            criteria.addOrder(Order.asc(columnToSort));
        }
    }

    public static void setPagination(Criteria criteria, Integer page, Integer limit) {
        if (limit == null) {
            return;
        }
        if (page == null) {
            page = 1;
        }
        criteria.setMaxResults(limit);
        criteria.setFirstResult(limit * (page - 1));
    }
}
