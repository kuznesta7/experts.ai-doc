package ai.unico.platform.ume.entity;

import javax.persistence.*;

@Entity
public class BillingInfoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long billingInfoId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    private String responsiblePersonName;

    private String companyName;

    private String address;

    private String companyRegistrationNumber;

    private String companyVatin;

    private String phone;

    private String email;

    private boolean deleted;

    private boolean defaultBillingInfo;

    public Long getBillingInfoId() {
        return billingInfoId;
    }

    public void setBillingInfoId(Long billingInfoId) {
        this.billingInfoId = billingInfoId;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public String getResponsiblePersonName() {
        return responsiblePersonName;
    }

    public void setResponsiblePersonName(String responsiblePersonName) {
        this.responsiblePersonName = responsiblePersonName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompanyRegistrationNumber() {
        return companyRegistrationNumber;
    }

    public void setCompanyRegistrationNumber(String companyRegistrationNumber) {
        this.companyRegistrationNumber = companyRegistrationNumber;
    }

    public String getCompanyVatin() {
        return companyVatin;
    }

    public void setCompanyVatin(String companyVatin) {
        this.companyVatin = companyVatin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isDefaultBillingInfo() {
        return defaultBillingInfo;
    }

    public void setDefaultBillingInfo(boolean defaultBillingInfo) {
        this.defaultBillingInfo = defaultBillingInfo;
    }
}
