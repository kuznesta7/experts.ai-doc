package ai.unico.platform.ume.dao;

import ai.unico.platform.ume.entity.TokenEntity;

public interface TokenDao {

    TokenEntity findByTokenValue(String tokenValue);

    Boolean doesTokenValueExist(String tokenValue);

    void save(TokenEntity tokenEntity);

}
