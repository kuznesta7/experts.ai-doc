package ai.unico.platform.ume.dao;

import ai.unico.platform.ume.entity.UserOrderEntity;

public interface UserOrderDao {

    void save(UserOrderEntity userOrderEntity);

    UserOrderEntity findOrder(Long userOrderId);

}
