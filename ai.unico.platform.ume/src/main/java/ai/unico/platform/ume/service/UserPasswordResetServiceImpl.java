package ai.unico.platform.ume.service;

import ai.unico.platform.ume.api.dto.UserDto;
import ai.unico.platform.ume.api.service.UserNotifierService;
import ai.unico.platform.ume.api.service.UserPasswordResetService;
import ai.unico.platform.ume.api.service.UserPasswordService;
import ai.unico.platform.ume.api.service.UserService;
import ai.unico.platform.util.exception.NonexistentEntityException;
import org.springframework.beans.factory.annotation.Autowired;

public class UserPasswordResetServiceImpl implements UserPasswordResetService {

    @Autowired
    private UserPasswordService userPasswordService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserNotifierService userNotifierService;

    @Override
    public void applyForResetPassword(String email) throws NonexistentEntityException {
        final UserDto userDto = userService.findByEmail(email);
        if (userDto == null) throw new NonexistentEntityException(UserDto.class);
        final Long id = userDto.getId();
        final String passwordSetToken = userPasswordService.createPasswordSetToken(id);
        userNotifierService.sendResetPasswordLink(email, passwordSetToken);
    }
}
