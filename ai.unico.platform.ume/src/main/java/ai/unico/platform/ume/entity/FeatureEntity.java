package ai.unico.platform.ume.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class FeatureEntity {

    @Id
    private Long featureId;

    private String feature;

    private String note;

    private boolean deleted;

    public Long getFeatureId() {
        return featureId;
    }

    public void setFeatureId(Long featureId) {
        this.featureId = featureId;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
