package ai.unico.platform.ume.dao.impl;

import ai.unico.platform.ume.dao.UserActivityDao;
import ai.unico.platform.ume.entity.UserActivityEntity;
import ai.unico.platform.ume.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class UserActivityDaoImpl implements UserActivityDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void saveUserActivity(UserActivityEntity userActivityEntity) {
        entityManager.persist(userActivityEntity);
    }

    @Override
    public UserActivityEntity findLastActivity(Long userId, String activityType) {
        return findLastActivity(userId, activityType, null);
    }

    @Override
    public UserActivityEntity findLastActivity(Long userId, String activityType, Date dateMax) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserActivityEntity.class, "userActivity");
        criteria.add(Restrictions.eq("userId", userId));
        criteria.add(Restrictions.eq("activityType", activityType));
        if (dateMax != null) criteria.add(Restrictions.le("activityDate", dateMax));
        criteria.addOrder(Order.desc("activityDate"));
        criteria.setMaxResults(1);
        return (UserActivityEntity) criteria.uniqueResult();
    }

    @Override
    public List<UserActivityEntity> findLastActivity(Set<Long> userIds, String activityType, Date dateMax) {
        return null;
    }
}
