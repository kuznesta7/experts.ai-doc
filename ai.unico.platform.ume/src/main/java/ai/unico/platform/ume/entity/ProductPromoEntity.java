package ai.unico.platform.ume.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class ProductPromoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long productPromoId;

    @ManyToOne
    private ProductEntity productEntity;

    @OneToMany(mappedBy = "productPromoEntity")
    private List<UserRoleEntity> usedForRoles;

    private String promoCode;

    private boolean oneTime;

    private boolean used;

    private Date validUntil;

    private Integer duration;

    private boolean deleted;

    public Long getProductPromoId() {
        return productPromoId;
    }

    public void setProductPromoId(Long productPromoId) {
        this.productPromoId = productPromoId;
    }

    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public boolean isOneTime() {
        return oneTime;
    }

    public void setOneTime(boolean oneTime) {
        this.oneTime = oneTime;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
