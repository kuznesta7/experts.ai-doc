package ai.unico.platform.ume.entity;

import javax.persistence.*;

@Entity
public class ProductFeatureEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long productFeatureId;

    @ManyToOne
    private FeatureEntity featureEntity;

    @ManyToOne
    private ProductEntity productEntity;

    private String note;

    private boolean deleted;

    public Long getProductFeatureId() {
        return productFeatureId;
    }

    public void setProductFeatureId(Long productFeatureId) {
        this.productFeatureId = productFeatureId;
    }

    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public FeatureEntity getFeatureEntity() {
        return featureEntity;
    }

    public void setFeatureEntity(FeatureEntity featureEntity) {
        this.featureEntity = featureEntity;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
