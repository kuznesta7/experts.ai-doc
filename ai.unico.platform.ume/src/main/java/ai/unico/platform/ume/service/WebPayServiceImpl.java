package ai.unico.platform.ume.service;

import ai.unico.platform.ume.api.dto.PaymentRequestDto;
import ai.unico.platform.ume.api.dto.PaymentResponseDto;
import ai.unico.platform.ume.api.enums.PaymentState;
import ai.unico.platform.ume.api.exception.OrderAlreadyDeliveredException;
import ai.unico.platform.ume.api.exception.WebPayUnavailableException;
import ai.unico.platform.ume.api.service.ProductService;
import ai.unico.platform.ume.api.service.WebPayService;
import ai.unico.platform.ume.dao.UserOrderDao;
import ai.unico.platform.ume.entity.UserOrderEntity;
import ai.unico.platform.ume.entity.UserPaymentStateEntity;
import cz.gopay.api.v3.GPClientException;
import cz.gopay.api.v3.IGPConnector;
import cz.gopay.api.v3.impl.apacheclient.HttpClientGPConnector;
import cz.gopay.api.v3.model.common.Currency;
import cz.gopay.api.v3.model.payment.BasePayment;
import cz.gopay.api.v3.model.payment.Payment;
import cz.gopay.api.v3.model.payment.PaymentFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class WebPayServiceImpl implements WebPayService {


    @Value("${goPay.clientId}")
    private String goPayClientId;

    @Value("${goPay.clientSecret}")
    private String goPayClientSecret;

    @Value("${goPay.authenticationUrl}")
    private String goPayAuthenticationUrl;

    @Value("${goPay.goId}")
    private Long goPayId;

    @Value("${env.host}")
    private String host;

    @Value("${env.host.api}")
    private String apiAddress;

    @Autowired
    private UserOrderDao userOrderDao;
    @Autowired
    private ProductService productService;

    private IGPConnector connector;

    public void init() {
        connector = HttpClientGPConnector.build(goPayAuthenticationUrl);
    }

    @Override
    public PaymentResponseDto createPayment(PaymentRequestDto paymentRequestDto) throws WebPayUnavailableException, OrderAlreadyDeliveredException {
        if (connector == null) throw new WebPayUnavailableException();
        try {
            connector.getAppToken(goPayClientId, goPayClientSecret);
            final BasePayment basePayment = PaymentFactory.createBasePaymentBuilder()
                    .order(paymentRequestDto.getUserOrderId().toString(), paymentRequestDto.getAmount().longValue(), Currency.EUR, null)
                    .addItem(paymentRequestDto.getProductName(), paymentRequestDto.getAmount().longValue(), 1L)
                    .withCallback("todo", host + apiAddress + "payments")
                    .toEshop(goPayId)
                    .build();
            final Payment payment = connector.createPayment(basePayment);
            final PaymentResponseDto paymentResponseDto = convert(payment);
            updatePayment(paymentResponseDto);
            return paymentResponseDto;
        } catch (GPClientException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public PaymentResponseDto checkStateForPayment(Long paymentId) throws WebPayUnavailableException, OrderAlreadyDeliveredException {
        if (connector == null) throw new WebPayUnavailableException();
        try {
            connector.getAppToken(goPayClientId, goPayClientSecret);
            final Payment payment = connector.paymentStatus(paymentId);
            final PaymentResponseDto paymentDto = convert(payment);
            updatePayment(paymentDto);
            return paymentDto;
        } catch (GPClientException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void updatePayment(PaymentResponseDto paymentDto) throws OrderAlreadyDeliveredException {
        final Long orderId = paymentDto.getUserOrderId();
        final UserOrderEntity order = userOrderDao.findOrder(orderId);
        final UserPaymentStateEntity userPaymentStateEntity = new UserPaymentStateEntity();
        userPaymentStateEntity.setUserOrderEntity(order);
        userPaymentStateEntity.setPaymentState(paymentDto.getPaymentState());

        if (paymentDto.getPaymentState().equals(PaymentState.PAID) && !order.isDelivered()) {
            productService.deliverProduct(orderId);
        }
    }

    private PaymentResponseDto convert(Payment payment) {
        final PaymentResponseDto paymentResponseDto = new PaymentResponseDto();
        final Payment.SessionState state = payment.getState();
        paymentResponseDto.setGwUrl(payment.getGwUrl());
        paymentResponseDto.setPaymentId(payment.getId());
        paymentResponseDto.setPaymentState(PaymentState.valueOf(state.name()));
        paymentResponseDto.setUserOrderId(Long.parseLong(payment.getOrderNumber()));
        return paymentResponseDto;
    }
}
