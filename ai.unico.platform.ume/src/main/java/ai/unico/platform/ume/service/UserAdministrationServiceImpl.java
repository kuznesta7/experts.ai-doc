package ai.unico.platform.ume.service;

import ai.unico.platform.ume.api.dto.AdminUserDto;
import ai.unico.platform.ume.api.filter.AdminUserFilter;
import ai.unico.platform.ume.api.service.UserAdministrationService;
import ai.unico.platform.ume.api.wrapper.AdminUserWrapper;
import ai.unico.platform.ume.entity.UserAdminViewEntity;
import ai.unico.platform.ume.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.ume.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

public class UserAdministrationServiceImpl implements UserAdministrationService {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    @Transactional
    public AdminUserWrapper findUsersForAdmin(AdminUserFilter adminUserFilter) {
        final Criteria criteria = prepareCriteriaForFilter(adminUserFilter);
        HibernateUtil.setPagination(criteria, adminUserFilter.getPage(), adminUserFilter.getLimit());

        final Criteria countCriteria = prepareCriteriaForFilter(adminUserFilter);
        countCriteria.setProjection(Projections.rowCount());

        final Long usersCount = (Long) countCriteria.uniqueResult();
        final AdminUserWrapper adminUserWrapper = new AdminUserWrapper();
        final List<UserAdminViewEntity> list = criteria.list();
        adminUserWrapper.setPage(adminUserFilter.getPage());
        adminUserWrapper.setLimit(adminUserFilter.getLimit());
        adminUserWrapper.setAdminUserDtos(list.stream()
                .map(this::convert)
                .collect(Collectors.toList()));
        adminUserWrapper.setNumberOfUsers(usersCount);
        return adminUserWrapper;
    }

    private Criteria prepareCriteriaForFilter(AdminUserFilter adminUserFilter) {
        final String query = adminUserFilter.getQuery();
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserAdminViewEntity.class, "user");
        criteria.add(Restrictions.eq("deleted", false));
        if (query != null && !query.isEmpty()) {
            final String[] strings = query.split(" ");
            for (String string : strings) {
                criteria.add(Restrictions.or(
                        Restrictions.ilike("firstName", string, MatchMode.ANYWHERE),
                        Restrictions.ilike("lastName", string, MatchMode.ANYWHERE),
                        Restrictions.ilike("email", string, MatchMode.ANYWHERE),
                        Restrictions.ilike("username", string, MatchMode.ANYWHERE)
                ));
            }
        }
        return criteria;
    }

    private AdminUserDto convert(UserAdminViewEntity userAdminViewEntity) {
        AdminUserDto adminUserDto = new AdminUserDto();
        adminUserDto.setUserId(userAdminViewEntity.getUserId());
        adminUserDto.setFirstName(userAdminViewEntity.getFirstName());
        adminUserDto.setLastName(userAdminViewEntity.getLastName());
        adminUserDto.setEmail(userAdminViewEntity.getEmail());
        adminUserDto.setUsername(userAdminViewEntity.getUsername());
        adminUserDto.setLastLoginDate(userAdminViewEntity.getLastLoginDate());
        adminUserDto.setRegistrationDate(userAdminViewEntity.getRegistrationDate());
        adminUserDto.setLoginCount(userAdminViewEntity.getLoginCount());
        return adminUserDto;
    }
}
