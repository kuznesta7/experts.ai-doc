package ai.unico.platform.ume.util;

import ai.unico.platform.ume.entity.superclass.Trackable;
import ai.unico.platform.util.model.UserContext;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Optional;


public class TrackableUtil {

    @Deprecated
    public static void fillCreateTrackable(Trackable trackable, Long userId) {
        trackable.setDateInsert(new Timestamp(new Date().getTime()));
        trackable.setUserInsert(userId);
        fillUpdateTrackable(trackable, userId);
    }

    @Deprecated
    public static void fillUpdateTrackable(Trackable trackable, Long userId) {
        trackable.setDateUpdate(new Timestamp(new Date().getTime()));
        trackable.setUserUpdate(userId);
    }

    public static void fillCreate(Trackable trackable, UserContext userContext) {
        trackable.setDateInsert(new Timestamp(new Date().getTime()));
        trackable.setUserInsert(getAuthorUserId(userContext));
        fillUpdate(trackable, userContext);
    }

    public static void fillUpdate(Trackable trackable, UserContext userContext) {
        trackable.setDateUpdate(new Timestamp(new Date().getTime()));
        trackable.setUserUpdate(getAuthorUserId(userContext));
    }

    public static <T extends Trackable> T getLatestCreated(Collection<T> trackables) {
        if (trackables.isEmpty()) return null;
        final Optional<T> optional = trackables.stream()
                .max(Comparator.comparing(Trackable::getDateInsert));
        return optional.get();
    }

    private static Long getAuthorUserId(UserContext userContext) {
        if (userContext == null) return null;
        if (userContext.getImpersonatorUserId() != null) return userContext.getImpersonatorUserId();
        return userContext.getUserId();
    }
}

