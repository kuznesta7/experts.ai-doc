package ai.unico.platform.ume.entity;

import ai.unico.platform.ume.entity.superclass.Trackable;

import javax.persistence.*;


@Entity
public class UserCredentialsEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userCredentialsId;

    @ManyToOne
    private UserEntity userEntity;

    private String type;

    private String identification;

    private String secret;

    public Long getUserCredentialsId() {
        return userCredentialsId;
    }

    public void setUserCredentialsId(Long userCredentialsId) {
        this.userCredentialsId = userCredentialsId;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
