package ai.unico.platform.ume.dao.impl;

import ai.unico.platform.ume.dao.ProductPromoDao;
import ai.unico.platform.ume.entity.ProductPromoEntity;
import ai.unico.platform.ume.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

public class ProductPromoDaoImpl implements ProductPromoDao {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public ProductPromoEntity find(Long promoCodeId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProductPromoEntity.class, "promo");
        criteria.add(Restrictions.eq("productPromoId", promoCodeId));
        criteria.add(Restrictions.eq("deleted", false));
        return (ProductPromoEntity) criteria.uniqueResult();
    }

    @Override
    public ProductPromoEntity find(String promoCode) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProductPromoEntity.class, "promo");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.eq("promoCode", promoCode));
        return (ProductPromoEntity) criteria.uniqueResult();
    }

    @Override
    public ProductPromoEntity findValid(String promoCode) {
        final Criteria criteria = prepareValidationCriteria();
        criteria.add(Restrictions.eq("promoCode", promoCode));
        return (ProductPromoEntity) criteria.uniqueResult();
    }

    @Override
    public List<ProductPromoEntity> findAll() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProductPromoEntity.class, "promo");
        criteria.add(Restrictions.eq("deleted", false));
        return criteria.list();
    }

    @Override
    public List<ProductPromoEntity> findValid() {
        final Criteria criteria = prepareValidationCriteria();
        return criteria.list();
    }

    @Override
    public void save(ProductPromoEntity productPromoEntity) {
        entityManager.persist(productPromoEntity);
    }

    private Criteria prepareValidationCriteria() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProductPromoEntity.class, "promo");
        criteria.add(Restrictions.eq("deleted", false));
        criteria.add(Restrictions.or(Restrictions.eq("oneTime", false), Restrictions.eq("used", false)));
        criteria.add(Restrictions.or(Restrictions.isNull("validUntil"), Restrictions.ge("validUntil", new Date())));
        return criteria;
    }
}
