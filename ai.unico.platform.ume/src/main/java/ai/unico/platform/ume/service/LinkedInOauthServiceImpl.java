package ai.unico.platform.ume.service;

import ai.unico.platform.store.api.dto.UserRegistrationDto;
import ai.unico.platform.store.api.service.UserProfileImgService;
import ai.unico.platform.store.api.service.UserRegistrationService;
import ai.unico.platform.ume.api.service.LinkedInOauthService;
import ai.unico.platform.ume.dao.UserDao;
import ai.unico.platform.ume.entity.UserEntity;
import ai.unico.platform.ume.util.OauthUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.exception.RuntimeExceptionWithMessage;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class LinkedInOauthServiceImpl implements LinkedInOauthService {

    @Value("${env.host}")
    private String host;

    @Value("${oauth2.linkedin.clientId}")
    private String linkedinClientId;

    @Value("${oauth2.linkedin.clientSecret}")
    private String linkedinClientSecret;

    @Value("${oauth2.linkedin.authenticationUrl}")
    private String linkedinAuthenticationUrl;

    @Value("${oauth2.linkedin.scope}")
    private String linkedinScope;

    @Value("${oauth2.linkedin.tokenUrl}")
    private String linkedinTokenUrl;

    @Value("${oauth2.linkedin.userEmailUrl}")
    private String linkedinUserEmailUrl;

    @Value("${oauth2.linkedin.userInfoUrl}")
    private String linkedinUserInfoUrl;

    @Autowired
    private UserDao userDao;

    @Override
    public String getApplicationUrl() {
        try {
            final String encodedClientId = URLEncoder.encode(linkedinClientId, "UTF-8");
            final String encodedScope = URLEncoder.encode(linkedinScope, "UTF-8");
            final String encodedRedirectUrl = URLEncoder.encode(host + "oauth2callback/linkedin", "UTF-8");
            return linkedinAuthenticationUrl + "?client_id=" + encodedClientId + "&scope=" + encodedScope + "&redirect_uri=" + encodedRedirectUrl + "&response_type=code&state=asdfghjkl";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    @Transactional
    public String getLogin(String code) throws Exception {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("code", code);
        parameters.put("client_id", linkedinClientId);
        parameters.put("client_secret", linkedinClientSecret);
        parameters.put("grant_type", "authorization_code");
        parameters.put("redirect_uri", host + "oauth2callback/linkedin");

        final Map<String, String> tokenMap = OauthUtil.getToken(linkedinTokenUrl, parameters);
        final String accessToken = tokenMap.get("access_token");

        if (accessToken == null) throw new UserUnauthorizedException();

        final String emailInfoUrl = linkedinUserEmailUrl + "&oauth2_access_token=" + accessToken;
        final String response = OauthUtil.getUserInfo(emailInfoUrl, accessToken);

        final ObjectMapper mapper = new ObjectMapper();

        final UserEmailInfo userEmailInfo = mapper.readValue(response, UserEmailInfo.class);
        final List<UserEmailInfo.Element> elements = userEmailInfo.elements;

        String email = null;
        if (elements != null) {
            final Optional<String> optionalEmail = elements.stream().findFirst().map(e -> e.handle).map(h -> h.emailAddress);
            if (optionalEmail.isPresent()) {
                email = optionalEmail.get();
            }
        }
        if (email == null) {
            throw new RuntimeExceptionWithMessage("Linkedin API email retrieve failed");
        }
        final UserEntity userEntity = userDao.findByEmail(email);

        if (userEntity == null) {
            final UserRegistrationService userRegistrationService = ServiceRegistryUtil.getService(UserRegistrationService.class);
            final UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
            final String userInfoUrl = linkedinUserInfoUrl + "&oauth2_access_token=" + accessToken;
            final String userInfoString = OauthUtil.getUserInfo(userInfoUrl, accessToken);
            final UserInfo userInfo = mapper.readValue(userInfoString, UserInfo.class);
            userRegistrationDto.setFullName(userInfo.localizedFirstName + " " + userInfo.localizedLastName);
            userRegistrationDto.setEmail(email);
            userRegistrationDto.setSocial(true);
            final Long userId = userRegistrationService.registerUser(userRegistrationDto, null);

            try {
                final UserProfileImgService userProfileImgService = ServiceRegistryUtil.getService(UserProfileImgService.class);
                final UserInfo.ProfilePicture profilePicture = userInfo.profilePicture;
                if (profilePicture != null && profilePicture.displayImage != null && profilePicture.displayImage.elements != null && profilePicture.displayImage.elements.size() > 0) {
                    final UserInfo.ProfilePicture.DisplayImage.Element element = profilePicture.displayImage.elements.get(profilePicture.displayImage.elements.size() - 1);
                    if (element.identifiers != null && element.identifiers.size() > 0) {
                        final UserInfo.ProfilePicture.DisplayImage.Element.Identifier identifier = element.identifiers.get(0);
                        final String src = identifier.identifier;
                        final byte[] image = OauthUtil.getImage(new URL(src));
                        final String imageName = userRegistrationDto.getEmail() + "_LINKEDIN_IMG";
                        final FileDto fileDto = new FileDto(image, imageName);
                        userProfileImgService.saveImg(null, userId, UserProfileImgService.ImgType.PORTRAIT, fileDto);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return email;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class UserEmailInfo {

        private List<Element> elements;

        public UserEmailInfo(@JsonProperty("elements") List<Element> elements) {
            this.elements = elements;
        }

        @JsonIgnoreProperties(ignoreUnknown = true)
        private static class Element {
            private Handle handle;

            public Element(@JsonProperty("handle~") Handle handle) {
                this.handle = handle;
            }

            @JsonIgnoreProperties(ignoreUnknown = true)
            private static class Handle {
                private String emailAddress;

                public Handle(@JsonProperty("emailAddress") String emailAddress) {
                    this.emailAddress = emailAddress;
                }
            }
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class UserInfo {
        private String localizedLastName;

        private String localizedFirstName;

        private ProfilePicture profilePicture;

        public UserInfo(@JsonProperty("localizedLastName") String localizedLastName, @JsonProperty("localizedFirstName") String localizedFirstName, @JsonProperty("profilePicture") ProfilePicture profilePicture) {
            this.localizedLastName = localizedLastName;
            this.localizedFirstName = localizedFirstName;
            this.profilePicture = profilePicture;
        }

        @JsonIgnoreProperties(ignoreUnknown = true)
        private static class ProfilePicture {
            private DisplayImage displayImage;

            public ProfilePicture(@JsonProperty("displayImage~") DisplayImage displayImage) {
                this.displayImage = displayImage;
            }

            @JsonIgnoreProperties(ignoreUnknown = true)
            private static class DisplayImage {
                private List<Element> elements;

                public DisplayImage(@JsonProperty("elements") List<Element> elements) {
                    this.elements = elements;
                }

                @JsonIgnoreProperties(ignoreUnknown = true)
                private static class Element {
                    private List<Identifier> identifiers;

                    public Element(@JsonProperty("identifiers") List<Identifier> identifiers) {
                        this.identifiers = identifiers;
                    }

                    @JsonIgnoreProperties(ignoreUnknown = true)
                    private static class Identifier {

                        private String identifierType;

                        private String identifier;

                        public Identifier(@JsonProperty("identifierType") String identifierType,
                                          @JsonProperty("identifier") String identifier) {
                            this.identifierType = identifierType;
                            this.identifier = identifier;
                        }
                    }
                }
            }
        }
    }
}
