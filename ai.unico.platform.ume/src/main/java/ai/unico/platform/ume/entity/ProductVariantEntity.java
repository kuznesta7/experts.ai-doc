package ai.unico.platform.ume.entity;

import javax.persistence.*;

@Entity
public class ProductVariantEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long productVariantId;

    @ManyToOne
    private ProductEntity productEntity;

    private Double price;

    private Integer rolePaidDuration;

    private String variantNote;

    private boolean deleted;

    public Long getProductVariantId() {
        return productVariantId;
    }

    public void setProductVariantId(Long productVariantId) {
        this.productVariantId = productVariantId;
    }

    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getRolePaidDuration() {
        return rolePaidDuration;
    }

    public void setRolePaidDuration(Integer rolePaidDuration) {
        this.rolePaidDuration = rolePaidDuration;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getVariantNote() {
        return variantNote;
    }

    public void setVariantNote(String variantNote) {
        this.variantNote = variantNote;
    }
}
