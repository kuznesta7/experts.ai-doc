package ai.unico.platform.ume.service;

import ai.unico.platform.ume.api.service.UserRoleService;
import ai.unico.platform.ume.dao.ProductPromoDao;
import ai.unico.platform.ume.dao.UserDao;
import ai.unico.platform.ume.dao.UserRoleDao;
import ai.unico.platform.ume.entity.ProductPromoEntity;
import ai.unico.platform.ume.entity.UserEntity;
import ai.unico.platform.ume.entity.UserRoleEntity;
import ai.unico.platform.ume.util.TrackableUtil;
import ai.unico.platform.util.HibernateUtil;
import ai.unico.platform.util.enums.Role;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleDao userRoleDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ProductPromoDao productPromoDao;

    @Override
    @Transactional
    public Map<Long, List<Role>> findRolesForUsers(Collection<Long> userIds) {
        final List<UserRoleEntity> rolesForUsers = userRoleDao.findRolesForUsers(userIds);
        return rolesForUsers.stream()
                .collect(Collectors.groupingBy(x -> HibernateUtil.getId(x, UserRoleEntity::getUserEntity, UserEntity::getUserId), Collectors.mapping(UserRoleEntity::getRole, Collectors.toList())));
    }

    @Override
    @Transactional
    public List<Role> findRolesForUser(Long userId) {
        final List<Role> result = new ArrayList<>();
        final List<UserRoleEntity> userRoleEntities = userRoleDao.findRolesForUser(userId);

        for (UserRoleEntity userRoleEntity : userRoleEntities) {
            result.add(userRoleEntity.getRole());
        }

        return result;
    }

    @Override
    @Transactional
    public List<Long> findUsersForRole(String id) {
        final List<UserRoleEntity> userRolesForRole = userRoleDao.findUserRolesForRole(id);
        final List<Long> userIds = new ArrayList<>();

        for (UserRoleEntity userRoleEntity : userRolesForRole) {
            final Long userId = userRoleEntity.getUserEntity().getUserId();
            userIds.add(userId);
        }

        return userIds;
    }

    @Override
    @Transactional
    public void addRole(Long userId, Role role, UserContext userContext) throws NonexistentEntityException {
        addRole(userId, role, null, false, userContext);
    }

    @Override
    @Transactional
    public void addRole(Long userId, Role role, Date validTo, boolean trial, UserContext userContext) throws NonexistentEntityException {
        final UserRoleEntity userRoleEntity = prepare(userId, role, validTo, trial, userContext);
        userRoleDao.saveRole(userRoleEntity);
    }

    @Override
    @Transactional
    public void addRole(Long userId, Role role, Date validTo, Long promoCodeId, UserContext userContext) throws NonexistentEntityException {
        final ProductPromoEntity productPromoEntity = productPromoDao.find(promoCodeId);
        if (productPromoEntity == null) throw new NonexistentEntityException(ProductPromoEntity.class);
        final UserRoleEntity userRoleEntity = prepare(userId, role, validTo, true, userContext);
        userRoleEntity.setProductPromoEntity(productPromoEntity);
        userRoleDao.saveRole(userRoleEntity);
    }

    @Override
    @Transactional
    public void removeRole(Long userId, Role role, UserContext userContext) throws NonexistentEntityException {
        final UserEntity userEntity = userDao.find(userId);
        if (userEntity == null) throw new NonexistentEntityException(UserEntity.class);
        final Set<UserRoleEntity> userRoleEntities = userEntity.getUserRoleEntities();
        for (UserRoleEntity userRoleEntity : userRoleEntities) {
            if (role.equals(userRoleEntity.getRole())) {
                userRoleEntity.setDeleted(true);
                TrackableUtil.fillUpdateTrackable(userRoleEntity, userContext.getUserId());
                return;
            }
        }
    }

    private UserRoleEntity prepare(Long userId, Role role, Date validTo, boolean trial, UserContext userContext) {
        final UserEntity userEntity = userDao.find(userId);
        if (userEntity == null) throw new NonexistentEntityException(UserEntity.class);
        final Set<UserRoleEntity> userRoleEntities = userEntity.getUserRoleEntities();
        if (userRoleEntities != null) {
            for (UserRoleEntity roleEntity : userRoleEntities) {
                final Date userRoleValidTo = roleEntity.getValidTo();
                if (!roleEntity.isDeleted() && roleEntity.getRole().equals(role) && (userRoleValidTo == null || new Date().before(userRoleValidTo))) {
                    throw new EntityAlreadyExistsException("USER_ALREADY_HAS_THIS_ROLE", "user " + (userContext != null ? userContext.getUserId().toString() : "") + " tries to add role " + role + " to user " + userId);
                }
            }
        }
        final UserRoleEntity userRoleEntity = new UserRoleEntity();
        userRoleEntity.setUserEntity(userEntity);
        userRoleEntity.setTrial(trial);
        userRoleEntity.setRole(role);
        userRoleEntity.setValidTo(validTo);

        TrackableUtil.fillCreate(userRoleEntity, userContext);

        if (userContext != null && userId.equals(userContext.getUserId())) {
            userContext.getRoles().add(role);
        }
        return userRoleEntity;
    }
}
