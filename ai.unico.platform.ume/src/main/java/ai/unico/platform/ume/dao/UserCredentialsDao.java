package ai.unico.platform.ume.dao;

import ai.unico.platform.ume.entity.UserCredentialsEntity;

public interface UserCredentialsDao {

    UserCredentialsEntity find(Long userId, String userCredentialsType);

    void save(UserCredentialsEntity userCredentialsEntity);

}
