package ai.unico.platform.ume.dao.impl;

import ai.unico.platform.ume.dao.UserDao;
import ai.unico.platform.ume.entity.UserEntity;
import ai.unico.platform.ume.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.util.enums.Role;
import org.hibernate.Criteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;

public class UserDaoImpl implements UserDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public UserEntity findByEmail(String email) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserEntity.class, "user");
        criteria.add(Restrictions.eq("user.email", email).ignoreCase());
        criteria.add(Restrictions.eq("user.deleted", false));
        return (UserEntity) criteria.uniqueResult();
    }

    @Override
    public UserEntity findByUsername(String username) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserEntity.class, "user");
        criteria.add(Restrictions.eq("user.username", username).ignoreCase());
        criteria.add(Restrictions.eq("user.deleted", false));
        return (UserEntity) criteria.uniqueResult();
    }

    @Override
    public UserEntity find(Long userId) {
        final UserEntity userEntity = entityManager.find(UserEntity.class, userId);
        if (userEntity == null || userEntity.isDeleted()) return null;
        return userEntity;
    }

    @Override
    public List<UserEntity> getAll() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserEntity.class, "userEntity");
        criteria.add(Restrictions.eq("userEntity.deleted", false));
        return criteria.list();
    }

    @Override
    @Transactional
    public Long createUser(UserEntity userEntity) {
        entityManager.persist(userEntity);
        return userEntity.getUserId();
    }

    @Override
    public List<UserEntity> findUsersWithRoles(Collection<Role> roles) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserEntity.class, "user");
        final LogicalExpression joinRestrictions = Restrictions.and(Restrictions.in("userRole.role", roles), Restrictions.eq("userRole.deleted", false));
        criteria.createAlias("userRoleEntities", "userRole", JoinType.RIGHT_OUTER_JOIN, joinRestrictions);
        criteria.add(Restrictions.eq("user.deleted", false));
        return criteria.list();
    }
}
