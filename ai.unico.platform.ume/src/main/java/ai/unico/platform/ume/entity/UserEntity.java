package ai.unico.platform.ume.entity;

import ai.unico.platform.ume.entity.superclass.Trackable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity(name = "\"user\"")
public class UserEntity extends Trackable {

    @Id
    private Long userId;

    private String firstName;

    private String lastName;

    private String email;

    private String username;

    private boolean deleted;

    private boolean termsOfUseAccepted;

    @OneToMany(mappedBy = "userEntity")
    private Set<UserRoleEntity> userRoleEntities;

    @OneToMany
    @JoinColumn(name = "user_id")
    private Set<UserActivityEntity> userActivityEntities;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Set<UserRoleEntity> getUserRoleEntities() {
        return userRoleEntities;
    }

    public void setUserRoleEntities(Set<UserRoleEntity> userRoleEntities) {
        this.userRoleEntities = userRoleEntities;
    }

    public Set<UserActivityEntity> getUserActivityEntities() {
        return userActivityEntities;
    }

    public void setUserActivityEntities(Set<UserActivityEntity> userActivityEntities) {
        this.userActivityEntities = userActivityEntities;
    }

    public boolean isTermsOfUseAccepted() {
        return termsOfUseAccepted;
    }

    public void setTermsOfUseAccepted(boolean termsOfUseAccepted) {
        this.termsOfUseAccepted = termsOfUseAccepted;
    }
}
