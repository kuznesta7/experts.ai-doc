package ai.unico.platform.ume.service;

import ai.unico.platform.store.api.dto.UserRegistrationDto;
import ai.unico.platform.store.api.service.UserProfileImgService;
import ai.unico.platform.store.api.service.UserRegistrationService;
import ai.unico.platform.ume.api.service.GoogleOauthService;
import ai.unico.platform.ume.dao.UserDao;
import ai.unico.platform.ume.entity.UserEntity;
import ai.unico.platform.ume.util.OauthUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.dto.FileDto;
import ai.unico.platform.util.exception.UserUnauthorizedException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class GoogleOauthServiceImpl implements GoogleOauthService {

    @Value("${env.host}")
    private String host;

    @Value("${oauth2.google.clientId}")
    private String googleClientId;

    @Value("${oauth2.google.clientSecret}")
    private String googleClientSecret;

    @Value("${oauth2.google.authenticationUrl}")
    private String googleAuthenticationUrl;

    @Value("${oauth2.google.scope}")
    private String googleScope;

    @Value("${oauth2.google.tokenUrl}")
    private String googleTokenUrl;

    @Value("${oauth2.google.userInfoUrl}")
    private String googleUserInfoUrl;

    @Value("${oauth2.google.userInfoUrl.image}")
    private String googleUserImageUrl;

    @Autowired
    private UserDao userDao;

    @Override
    public String getApplicationUrl() {
        try {
            final String encodedClientId = URLEncoder.encode(googleClientId, "UTF-8");
            final String encodedScope = URLEncoder.encode(googleScope, "UTF-8");
            final String encodedRedirectUrl = URLEncoder.encode(host + "oauth2callback/google", "UTF-8");
            return googleAuthenticationUrl + "?client_id=" + encodedClientId + "&scope=" + encodedScope + "&redirect_uri=" + encodedRedirectUrl + "&response_type=code";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    @Transactional
    public String getLogin(String code) throws IOException, UserUnauthorizedException {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("code", code);
        parameters.put("client_id", googleClientId);
        parameters.put("client_secret", googleClientSecret);
        parameters.put("grant_type", "authorization_code");
        parameters.put("redirect_uri", host + "oauth2callback/google");

        final Map<String, String> tokenMap = OauthUtil.getToken(googleTokenUrl, parameters);
        final String accessToken = tokenMap.get("access_token");

        if (accessToken == null) throw new UserUnauthorizedException();

        final String userInfoUrl = googleUserInfoUrl + "?access_token=" + accessToken;
        final String response = OauthUtil.getUserInfo(userInfoUrl, accessToken);
        TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {
        };
        final ObjectMapper mapper = new ObjectMapper();
        final Map<String, String> userInfo = mapper.readValue(response, typeRef);

        final String email = userInfo.get("email");
        final UserEntity userEntity = userDao.findByEmail(email);
        if (userEntity == null) {
            final UserRegistrationService userRegistrationService = ServiceRegistryUtil.getService(UserRegistrationService.class);
            final UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
            userRegistrationDto.setFullName(userInfo.get("given_name") + " " + userInfo.get("family_name"));
            userRegistrationDto.setEmail(email);
            userRegistrationDto.setSocial(true);
            final Long userId = userRegistrationService.registerUser(userRegistrationDto, null);

            try {
                final String imageJson = OauthUtil.getUserInfo(googleUserImageUrl + "&access_token=" + accessToken, accessToken);
                final JsonNode jsonNode = mapper.readTree(imageJson);
                final JsonNode at = jsonNode.at("/image/isDefault");
                final boolean isDefault = at.asBoolean();
                if (!isDefault) {
                    final UserProfileImgService userProfileImgService = ServiceRegistryUtil.getService(UserProfileImgService.class);
                    final byte[] image = OauthUtil.getImage(new URL(userInfo.get("picture")));
                    final String imageName = userRegistrationDto.getEmail() + "_GOOGLE_IMG";
                    final FileDto fileDto = new FileDto(image, imageName);
                    userProfileImgService.saveImg(null, userId, UserProfileImgService.ImgType.PORTRAIT, fileDto);
                }
            } catch (Exception er) {
                er.printStackTrace();
            }
        }
        return email;
    }
}
