package ai.unico.platform.ume.service;

import ai.unico.platform.ume.api.dto.UserCredentialsDto;
import ai.unico.platform.ume.api.dto.UserDto;
import ai.unico.platform.ume.api.model.UserFilter;
import ai.unico.platform.ume.api.service.UserNotifierService;
import ai.unico.platform.ume.api.service.UserPasswordService;
import ai.unico.platform.ume.api.service.UserRoleService;
import ai.unico.platform.ume.api.service.UserService;
import ai.unico.platform.ume.dao.UserDao;
import ai.unico.platform.ume.entity.UserCredentialsEntity;
import ai.unico.platform.ume.entity.UserEntity;
import ai.unico.platform.ume.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.ume.util.TrackableUtil;
import ai.unico.platform.ume.util.UserUtil;
import ai.unico.platform.util.enums.Role;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.exception.OutdatedEntityException;
import ai.unico.platform.util.model.UserContext;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class UserServiceImpl implements UserService {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserPasswordService userPasswordService;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private UserNotifierService userNotifierService;

    @Override
    @Transactional
    public Long createUmeUser(UserDto userDto, Boolean createPasswordLink, UserContext userContext) throws EntityAlreadyExistsException, NonexistentEntityException {
        final String email = userDto.getEmail();
        final UserEntity userEntity = userDao.findByEmail(email);
        if (userEntity != null)
            throw new EntityAlreadyExistsException("USER_WITH_THIS_EMAIL_ALREADY_EXIST", "Somebody trying to create duplicate user with email " + email);

        final Long userId = userDto.getId();
        final UserEntity newUserEntity = UserUtil.convert(userDto);

        newUserEntity.setUserId(userId);
        TrackableUtil.fillCreate(newUserEntity, userContext);
        userDao.createUser(newUserEntity);

        userRoleService.addRole(userId, Role.USER, userContext);
        for (Role role : userDto.getUserRolesPreset()) {
            if (role.isOpen())
                userRoleService.addRole(userId, role, userContext);
        }

        if (createPasswordLink) {
            final String passwordSetToken = userPasswordService.createPasswordSetToken(userId);
            if (userContext == null) {
                userNotifierService.sendRegistrationSetPasswordLink(email, passwordSetToken);
            } else {
                userNotifierService.sendInvitationSetPasswordLink(email, passwordSetToken, userContext.getFullName());
            }
        }

        return userId;
    }

    @Override
    @Transactional
    public void editUmeUser(UserDto userDto, UserContext userContext) throws OutdatedEntityException, NonexistentEntityException {
        final Long userId = userDto.getId();
        final UserEntity userEntity = userDao.find(userId);
        if (userEntity == null) throw new NonexistentEntityException(UserEntity.class);
        UserUtil.fillUserEntity(userEntity, userDto);
        final List<Role> rolesForUser = userRoleService.findRolesForUser(userId);
        for (Role role : userDto.getUserRolesPreset()) {
            if (!role.isOpen()) continue;
            if (rolesForUser.contains(role)) continue;
            userRoleService.addRole(userId, role, userContext);
        }
        for (Role role : rolesForUser) {
            if (!role.isOpen()) continue;
            if (!userDto.getUserRolesPreset().contains(role)) {
                userRoleService.removeRole(userId, role, userContext);
            }
        }
    }

    @Override
    @Transactional
    public UserDto find(Long id) throws NonexistentEntityException {
        final UserEntity userEntity = userDao.find(id);
        if (userEntity == null) throw new NonexistentEntityException(UserEntity.class);
        final UserDto userDto = UserUtil.convert(userEntity);
        userDto.setUserRolesPreset(userRoleService.findRolesForUser(id));
        return userDto;
    }

    @Override
    @Transactional
    public UserDto findByUsername(String username) throws NonexistentEntityException {
        final UserEntity userEntity = userDao.findByUsername(username);
        if (userEntity == null) throw new NonexistentEntityException(UserEntity.class);
        return UserUtil.convert(userEntity);
    }

    @Override
    @Transactional
    public UserDto findByEmail(String email) {
        final UserEntity userEntity = userDao.findByEmail(email);
        if (userEntity == null) return null;
        return UserUtil.convert(userEntity);
    }

    @Override
    @Transactional
    public List<UserDto> find(UserFilter userFilter) {
        final List<UserEntity> userEntities = userDao.getAll();
        final List<UserDto> userDtos = new ArrayList<>();
        for (UserEntity userEntity : userEntities) {
            final UserDto userDto = UserUtil.convert(userEntity);
            userDtos.add(userDto);
        }
        return userDtos;
    }

    @Override
    @Transactional
    public List<UserDto> findUsersWithRole(Role role) {
        final List<UserEntity> usersWithRoles = userDao.findUsersWithRoles(Collections.singleton(role));
        return usersWithRoles.stream().map(UserUtil::convert).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public UserDto authenticate(UserCredentialsDto userCredentialsDto) throws NonexistentEntityException {
        final Long userId;
        if (userCredentialsDto.getType().equals(UserCredentialsDto.TYPE_PASSWORD)) {
            final String passwordFromDb = findPassword(userCredentialsDto.getIdentification());
            if (passwordFromDb != null && BCrypt.checkpw(userCredentialsDto.getSecret(), passwordFromDb)) {
                userId = userCredentialsDto.getUserId();
            } else {
                userId = null;
            }
        } else {
            userId = findForCredential(userCredentialsDto);
        }
        if (userId == null) return null;
        return find(userId);
    }

    private Long findForCredential(UserCredentialsDto userCredentialsDto) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserCredentialsEntity.class, "userCredentialsDto");
        criteria.add(Restrictions.eq("type", userCredentialsDto.getType()));
        criteria.add(Restrictions.eq("identification", userCredentialsDto.getIdentification()));
        criteria.add(Restrictions.eq("secret", userCredentialsDto.getSecret()));
        final UserCredentialsEntity userCredentialsEntity = (UserCredentialsEntity) criteria.uniqueResult();
        if (userCredentialsEntity == null) {
            return null;
        }
        return userCredentialsEntity.getUserEntity().getUserId();
    }

    private String findPassword(String identification) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(UserCredentialsEntity.class, "userCredential");
        criteria.add(Restrictions.eq("type", UserCredentialsDto.TYPE_PASSWORD));
        criteria.add(Restrictions.eq("identification", identification));
        final UserCredentialsEntity userCredentialsEntity = (UserCredentialsEntity) criteria.uniqueResult();
        if (userCredentialsEntity == null) {
            return null;
        }
        return userCredentialsEntity.getSecret();
    }
}
