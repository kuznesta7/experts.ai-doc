package ai.unico.platform.ume.dao;

import ai.unico.platform.ume.entity.UserActivityEntity;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface UserActivityDao {

    void saveUserActivity(UserActivityEntity userActivityEntity);

    UserActivityEntity findLastActivity(Long userId, String activityType);

    UserActivityEntity findLastActivity(Long userId, String activityType, Date dateMax);

    List<UserActivityEntity> findLastActivity(Set<Long> userIds, String activityType, Date dateMax);

}
