package ai.unico.platform.ume.entity;

import ai.unico.platform.ume.entity.superclass.Trackable;
import ai.unico.platform.util.enums.Role;

import javax.persistence.*;
import java.util.Date;

@Entity
public class UserRoleEntity extends Trackable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userRoleId;

    @ManyToOne
    private UserEntity userEntity;

    @ManyToOne
    private ProductPromoEntity productPromoEntity;

    @Enumerated(EnumType.STRING)
    private Role role;

    private boolean deleted = false;

    private boolean trial = false;

    private Date validTo;

    public Long getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(Long userRoleId) {
        this.userRoleId = userRoleId;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public boolean isTrial() {
        return trial;
    }

    public void setTrial(boolean trial) {
        this.trial = trial;
    }

    public ProductPromoEntity getProductPromoEntity() {
        return productPromoEntity;
    }

    public void setProductPromoEntity(ProductPromoEntity productPromoEntity) {
        this.productPromoEntity = productPromoEntity;
    }
}
