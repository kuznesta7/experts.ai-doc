package ai.unico.platform.ume.service;

import ai.unico.platform.store.api.service.email.EmailService;
import ai.unico.platform.ume.api.dto.FeatureDto;
import ai.unico.platform.ume.api.dto.PaymentResponseDto;
import ai.unico.platform.ume.api.dto.ProductDto;
import ai.unico.platform.ume.api.exception.OrderAlreadyDeliveredException;
import ai.unico.platform.ume.api.exception.TrialExpiredException;
import ai.unico.platform.ume.api.exception.TrialUnavailableException;
import ai.unico.platform.ume.api.exception.WebPayUnavailableException;
import ai.unico.platform.ume.api.service.ProductService;
import ai.unico.platform.ume.api.service.UserRoleService;
import ai.unico.platform.ume.dao.ProductDao;
import ai.unico.platform.ume.dao.UserDao;
import ai.unico.platform.ume.dao.UserOrderDao;
import ai.unico.platform.ume.dao.UserRoleDao;
import ai.unico.platform.ume.entity.*;
import ai.unico.platform.ume.util.TrackableUtil;
import ai.unico.platform.util.ServiceRegistryUtil;
import ai.unico.platform.util.enums.Role;
import ai.unico.platform.util.exception.EntityAlreadyExistsException;
import ai.unico.platform.util.exception.NonexistentEntityException;
import ai.unico.platform.util.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @Autowired
    private UserOrderDao userOrderDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserRoleService userRoleService;

    @Value("${contact.requests}")
    private String requestEmailAddress;

    @Override
    @Transactional
    public List<ProductDto> findProducts(UserContext userContext) {
        final List<ProductEntity> products = productDao.findProducts();
        products.sort(Comparator.comparing(ProductEntity::getIndex));
        final ArrayList<ProductDto> productDtos = new ArrayList<>();
        final List<UserRoleEntity> rolesForUser;
        if (userContext != null) {
            rolesForUser = userRoleDao.findRolesForUser(userContext.getUserId());
        } else {
            rolesForUser = Collections.emptyList();
        }
        for (ProductEntity product : products) {
            final ProductDto productDto = new ProductDto();

            productDto.setProductId(product.getProductId());
            productDto.setProductDescription(product.getProductDescription());
            productDto.setProductName(product.getProductName());
            productDto.setRoleTrialDuration(product.getRoleTrialDuration());

            for (UserRoleEntity userRoleEntity : rolesForUser) {
                if (userRoleEntity.isDeleted()) continue;
                final Date validTo = userRoleEntity.getValidTo();
                if (validTo != null && new Date().after(validTo)) continue;
                if (product.getRole().equals(userRoleEntity.getRole())) {
                    if (userRoleEntity.isTrial()) {
                        productDto.setTrialUntil(validTo);
                    } else {
                        productDto.setPaidUntil(validTo);
                    }
                }
            }
            productDto.setTrialAvailable(product.getRoleTrialDuration() != null
                    && product.getRoleTrialDuration() > 0
                    && !isTrialUsed(product.getRole(), rolesForUser));

            for (ProductVariantEntity productVariantEntity : product.getProductVariantEntities()) {
                if (productVariantEntity.isDeleted()) continue;
                final ProductDto.ProductVariantDto productVariantDto = new ProductDto.ProductVariantDto();
                productVariantDto.setProductVariantId(productVariantEntity.getProductVariantId());
                productVariantDto.setPrice(productVariantEntity.getPrice());
                productVariantDto.setRolePaidDuration(productVariantEntity.getRolePaidDuration());
                productVariantDto.setNote(productVariantEntity.getVariantNote());
                productDto.getProductVariantDtos().add(productVariantDto);
            }

            productDto.setFeatureMap(new HashMap<>());
            for (ProductFeatureEntity productFeatureEntity : product.getProductFeatureEntities()) {
                if (productFeatureEntity.isDeleted() || productFeatureEntity.getFeatureEntity().isDeleted()) continue;
                final Long featureId = productFeatureEntity.getFeatureEntity().getFeatureId();
                final String note = productFeatureEntity.getNote();
                final ProductDto.ProductFeatureDto featureDto = new ProductDto.ProductFeatureDto(note);
                productDto.getFeatureMap().put(featureId, featureDto);
            }
            productDtos.add(productDto);
        }
        return productDtos;
    }

    @Override
    @Transactional
    public List<FeatureDto> findAllFeatures() {
        final List<FeatureEntity> features = productDao.findFeatures();
        final List<FeatureDto> featureDtos = new ArrayList<>();
        for (FeatureEntity feature : features) {
            final FeatureDto featureDto = new FeatureDto();
            featureDto.setFeatureId(feature.getFeatureId());
            featureDto.setFeature(feature.getFeature());
            featureDto.setNote(feature.getNote());
            featureDtos.add(featureDto);
        }
        return featureDtos;
    }

    @Override
    @Transactional
    public PaymentResponseDto purchaseProduct(Long productVariantId, Long userId, UserContext userContext) throws WebPayUnavailableException, OrderAlreadyDeliveredException {
        final ProductVariantEntity productVariantEntity = productDao.findProductVariant(productVariantId);
        final UserEntity userEntity = userDao.find(userId);
        if (productVariantEntity == null) throw new NonexistentEntityException(ProductVariantEntity.class);
        if (userEntity == null) throw new NonexistentEntityException(UserEntity.class);

        final UserOrderEntity userOrderEntity = new UserOrderEntity();
        userOrderEntity.setProductVariantEntity(productVariantEntity);
        userOrderEntity.setUserEntity(userEntity);
        userOrderEntity.setOrderTotal(productVariantEntity.getPrice());
        TrackableUtil.fillCreateTrackable(userOrderEntity, userContext.getUserId());
        userOrderDao.save(userOrderEntity);

        final EmailService service = ServiceRegistryUtil.getService(EmailService.class);
        service.sendEmail(requestEmailAddress, "User interested in product", "User " + userEntity.getEmail() + " is interested in product " + productVariantEntity.getProductEntity().getProductName() + " in variant for " + productVariantEntity.getPrice() + "EUR", null); //todo
        return null;

        //uncomment when webpay finished
//        final PaymentRequestDto paymentRequestDto = new PaymentRequestDto();
//        paymentRequestDto.setAmount(productVariantEntity.getPrice());
//        paymentRequestDto.setProductName(productVariantEntity.getProductEntity().getProductName());
//        paymentRequestDto.setUserOrderId(userOrderEntity.getUserOrderId());
//        return webPayService.createPayment(paymentRequestDto);
    }

    @Override
    @Transactional
    public void deliverProduct(Long orderId) throws OrderAlreadyDeliveredException {
        final UserOrderEntity order = userOrderDao.findOrder(orderId);
        if (order.isDelivered()) throw new OrderAlreadyDeliveredException();
        final UserEntity userEntity = order.getUserEntity();
        final ProductVariantEntity productVariantEntity = order.getProductVariantEntity();
        final ProductEntity product = productVariantEntity.getProductEntity();
        if (userEntity == null) throw new NonexistentEntityException(UserEntity.class);
        if (product == null) throw new NonexistentEntityException(ProductEntity.class);
        final Set<UserRoleEntity> userRoleEntities = userEntity.getUserRoleEntities();
        final Date dateNow = new Date();

        order.setDelivered(true);
        TrackableUtil.fillUpdateTrackable(order, order.getUserInsert());

        for (UserRoleEntity userRoleEntity : userRoleEntities) {
            if (userRoleEntity.isDeleted()
                    || !product.getRole().equals(userRoleEntity.getRole())) {
                continue;
            }
            final Date validTo = userRoleEntity.getValidTo();
            if (validTo == null) throw new EntityAlreadyExistsException("");
            if (dateNow.after(validTo)) {
                final Calendar cal = Calendar.getInstance();
                cal.setTime(validTo);
                cal.add(Calendar.DATE, productVariantEntity.getRolePaidDuration());
                userRoleEntity.setValidTo(cal.getTime());
                TrackableUtil.fillUpdateTrackable(userRoleEntity, order.getUserInsert());

                order.setUserRoleEntity(userRoleEntity);
                return;
            }
        }

        final Calendar cal = Calendar.getInstance();
        cal.setTime(dateNow);
        cal.add(Calendar.DATE, productVariantEntity.getRolePaidDuration());
        userRoleService.addRole(userEntity.getUserId(), product.getRole(), cal.getTime(), false, null);
        //todo notify about delivering the services
    }

    @Override
    @Transactional
    public void tryProduct(Long productId, Long userId, UserContext userContext) throws TrialExpiredException, TrialUnavailableException {
        final List<UserRoleEntity> rolesForUser = userRoleDao.findRolesForUser(userId);
        final ProductEntity product = productDao.findProduct(productId);

        if (product.getRoleTrialDuration() == null || product.getRoleTrialDuration().equals(0))
            throw new TrialUnavailableException();
        if (isTrialUsed(product.getRole(), rolesForUser)) throw new TrialExpiredException();

        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, product.getRoleTrialDuration());

        userRoleService.addRole(userId, product.getRole(), cal.getTime(), true, userContext);
    }

    private boolean isTrialUsed(Role role, List<UserRoleEntity> userRoleEntities) {
        for (UserRoleEntity userRoleEntity : userRoleEntities) {
            if (userRoleEntity.isDeleted()) continue;
            if (role.equals(userRoleEntity.getRole()) && userRoleEntity.isTrial()) {
                return true;
            }
        }
        return false;
    }
}
