package ai.unico.platform.ume.dao.impl;

import ai.unico.platform.ume.dao.BillingInfoDao;
import ai.unico.platform.ume.entity.BillingInfoEntity;
import ai.unico.platform.ume.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class BillingInfoDaoImpl implements BillingInfoDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public void save(BillingInfoEntity billingInfoEntity) {
        entityManager.persist(billingInfoEntity);
    }

    @Override
    public BillingInfoEntity find(Long billingInfoId) {
        final BillingInfoEntity billingInfoEntity = entityManager.find(BillingInfoEntity.class, billingInfoId);
        if (billingInfoEntity.isDeleted()) return null;
        return billingInfoEntity;
    }

    @Override
    public List<BillingInfoEntity> findForUser(Long userId) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(BillingInfoEntity.class, "billingInfo");
        criteria.add(Restrictions.eq("userEntity.userId", userId));
        criteria.add(Restrictions.eq("deleted", false));
        return criteria.list();
    }
}
