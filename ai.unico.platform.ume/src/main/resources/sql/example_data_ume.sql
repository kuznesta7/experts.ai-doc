INSERT INTO public."user" (user_id, first_name, last_name, email, username, deleted)
VALUES (1, 'Test', 'Testovič', 'test@test.test', 'test', FALSE);

INSERT INTO public.user_credentials (user_id, type, identification, secret) VALUES (1, 'PASSWORD', '1', '$2a$10$Wd3oglUmYV0KgONWmPU1xe3rEmKqxsy3rZl7j1f/6jp078PCe0Brq');

INSERT INTO public.user_role (user_id, role) VALUES (1, 'USER');
INSERT INTO public.user_role (user_id, role) VALUES (1, 'PORTAL_ADMIN');