drop table if exists product cascade;
drop table if exists feature cascade;
drop table if exists product_variant cascade;
drop table if exists product_feature cascade;
drop table if exists user_order cascade;
drop table if exists user_payment_state cascade;
drop table if exists product_promo cascade;

create table product (
  product_id          bigint primary key,
  product_name        text not null,
  product_description text,
  role                text,
  role_trial_duration integer,
  index               integer,
  deleted             boolean default false
);

create table product_variant (
  product_variant_id bigserial primary key,
  product_id         bigint references product,
  price              double precision,
  role_paid_duration integer,
  variant_note       text,
  deleted            boolean default false
);

create table feature (
  feature_id bigint primary key,
  feature    text,
  note       text,
  deleted    boolean default false
);

create table product_feature (
  product_feature_id bigserial primary key,
  product_id         bigint references product,
  feature_id         bigserial references feature,
  note               text,
  deleted            boolean default false
);

create table user_order (
  user_order_id      bigserial primary key,
  product_variant_id bigint references product not null,
  user_id            bigint references "user"  not null,
  user_role_id       bigint references user_role,
  order_total        double precision,
  payment_id         bigint,
  gw_url             text,
  delivered          boolean
)
  inherits (trackable);

create table user_payment_state (
  user_payment_state_id bigserial primary key,
  user_order_id         bigint references user_order not null,
  payment_state         text                         not null
)
  inherits (trackable);

insert into product (product_id, product_name, product_description, role, role_trial_duration, index)
values (1, 'FREE_PRODUCT', 'FREE_PRODUCT_DESCRIPTION', 'USER', null, 10),
       (2, 'BASIC_PRODUCT', 'BASIC_PRODUCT_DESCRIPTION', 'BASIC_USER', null, 20),
       (3, 'PREMIUM_PRODUCT', 'PREMIUM_PRODUCT_DESCRIPTION', 'PREMIUM_USER', null, 30);

insert into product_variant (product_id, price, role_paid_duration)
values (2, 20.0, 30),
       (2, 220.0, 365),
       (3, 220.0, 30),
       (3, 2400.0, 365);
insert into feature (feature_id, feature, note)
VALUES (1, 'EDIT_MY_PROFILE', null),
       (2, 'SHARE_MY_PROFILE', null),
       (3, 'SEARCH_BY_EXPERTISE', null),
       (4, 'SEARCH_BY_NAME', null),
       (5, 'SEARCH_MODULE_DASHBOARD', null),
       (6, 'SHOW_EXPERT_PROFILE', null),
       (7, 'SEMANTICS', null),
       (8, 'FILTER_EXPERT_OUTCOMES', null),
       (9, 'FILTER_RESULTS', null),
       (10, 'SAVE_SEARCH', null),
       (11, 'FOLLOW_EXPERT', null),
       (12, 'ADD_PERSONAL_NOTE_TO_EXPERT', null),
       (13, 'CONTACT_DIRECTLY', null),
       (14, 'CONTACT_INDIRECTLY', null),
       (15, 'EXPORT_SEARCH_RESULTS', null),
       (16, 'SHOW_MY_ORGANIZATION_EXPERTS_AND_OUTCOMES', null),
       (17, 'FILTER_MY_ORGANIZATION_EXPERTS_AND_OUTCOMES', null),
       (18, 'VERIFY_MY_ORGANIZATION_EXPERTS_AND_OUTCOMES', null),
       (19, 'EXPORT_MY_ORGANIZATION_EXPERTS_AND_OUTCOMES', null),
       (20, 'EVIDENCE_MODULE_DASHBOARD', null),
       (21, 'EDIT_MY_OUTCOMES', null);

insert into product_feature (feature_id, product_id, note)
values (1, 1, null),
       (2, 1, null),
       (3, 1, 'FIRST_5'),
       (4, 1, 'FIRST_5'),
       (5, 1, 'LIMITED'),
       (6, 1, 'LIMITED_NUMBER_OF_OUTCOMES'),
       (7, 1, null),
       (1, 2, null),
       (2, 2, null),
       (3, 2, null),
       (4, 2, null),
       (5, 2, null),
       (6, 2, null),
       (7, 2, null),
       (8, 2, null),
       (9, 2, null),
       (10, 2, null),
       (11, 2, null),
       (12, 2, null),
       (13, 2, null),
       (14, 2, 'ONE_A_YEAR_FOR_FREE'),
       (15, 2, 'FIRST_100'),
       (1, 3, null),
       (2, 3, null),
       (3, 3, null),
       (4, 3, null),
       (5, 3, null),
       (6, 3, null),
       (7, 3, null),
       (8, 3, null),
       (9, 3, null),
       (10, 3, null),
       (11, 3, null),
       (12, 3, null),
       (13, 3, null),
       (14, 3, 'FIVE_A_YEAR_FOR_FREE'),
       (15, 3, 'FIRST_100'),
       (16, 3, null),
       (17, 3, null),
       (18, 3, null),
       (19, 3, null),
       (20, 3, null),
       (21, 3, null);

drop table if exists billing_info;
create table billing_info (
  billing_info_id             bigserial primary key,
  user_id                     bigint references "user",
  responsible_person_name     text,
  company_name                text,
  address                     text,
  company_registration_number text,
  company_vatin               text,
  phone                       text,
  email                       text,
  deleted                     boolean default false,
  default_billing_info        boolean default false
);

alter table user_order
  add column billing_info_id bigint references billing_info;

create table product_promo (
  product_promo_id bigserial primary key,
  product_id       bigint references product,
  promo_code       text,
  one_time         boolean default true,
  used             boolean default false,
  duration         integer default 7,
  valid_until      timestamp,
  deleted          boolean default false
)
  inherits (trackable);


alter table user_role
  add column if not exists trial boolean default false,
  add column if not exists product_promo_id bigint references product_promo;
