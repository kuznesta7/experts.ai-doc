alter table user_role
  add column valid_to timestamp;
alter table user_role
  rename column outdated to deleted;