drop table user_credential;

alter table user_credentials
  add column user_update BIGINT;
alter table user_credentials
  add column date_update TIMESTAMP;
alter table user_credentials
  add column user_insert BIGINT;
alter table user_credentials
  add column date_insert TIMESTAMP;

alter table user_credentials
  inherit trackable;