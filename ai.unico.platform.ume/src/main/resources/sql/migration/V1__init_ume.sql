CREATE TABLE trackable (
  user_update BIGINT,
  date_update TIMESTAMP,
  user_insert BIGINT,
  date_insert TIMESTAMP
);

CREATE TABLE "user" (
  user_id    BIGINT PRIMARY KEY NOT NULL,
  first_name TEXT,
  last_name  TEXT,
  email      TEXT               NOT NULL,
  username   TEXT,
  deleted    BOOLEAN
);

CREATE TABLE user_credential (
  user_credential_id BIGSERIAL PRIMARY KEY NOT NULL,
  user_id            BIGINT                NOT NULL REFERENCES "user",
  type               TEXT,
  identification     TEXT,
  secret             TEXT
);

CREATE TABLE user_role (
  user_role_id BIGSERIAL PRIMARY KEY NOT NULL,
  user_id      BIGINT                NOT NULL REFERENCES "user",
  role         TEXT,
  outdated     BOOLEAN DEFAULT FALSE
)
  INHERITS (trackable);


CREATE TABLE token (
  token_id       BIGSERIAL PRIMARY KEY           NOT NULL,
  identification TEXT                            NOT NULL,
  token_value    TEXT                            UNIQUE NOT NULL,
  type           TEXT                            NOT NULL,
  valid_to       TIMESTAMP                       NOT NULL,
  used           TIMESTAMP
) INHERITS (trackable);


CREATE TABLE user_credentials (
  user_credentials_id BIGSERIAL PRIMARY KEY NOT NULL,
  user_id             BIGINT                NOT NULL REFERENCES "user",
  type                TEXT,
  identification      TEXT,
  secret              TEXT
);

CREATE TABLE user_activity (
  user_activity_id BIGSERIAL PRIMARY KEY,
  user_id          BIGINT REFERENCES "user" NOT NULL,
  activity_type    TEXT                     NOT NULL,
  activity_date    TIMESTAMP                NOT NULL
);