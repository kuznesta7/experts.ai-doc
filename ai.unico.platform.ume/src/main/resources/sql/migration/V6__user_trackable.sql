alter table "user"
    add column user_insert bigint,
    add column user_update bigint,
    add column date_insert timestamp,
    add column date_update timestamp,
    inherit trackable;

update "user" u
set date_insert=(select min(activity_date) from user_activity where user_id = u.user_id);
update "user" u
set date_update=date_insert;