alter table "user"
    add column terms_of_use_accepted boolean default false;
update "user"
set terms_of_use_accepted = true;