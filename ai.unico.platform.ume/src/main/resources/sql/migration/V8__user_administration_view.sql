create or replace view user_administration as (
    select u.user_id,
           first_name,
           last_name,
           email,
           username,
           date_insert             as registration_date,
           count(user_activity_id) as login_count,
           max(activity_date)      as last_login_date,
           deleted
    from "user" u
             left join user_activity ua on u.user_id = ua.user_id
    group by u.user_id, first_name, last_name, email, username, date_insert, deleted
    order by greatest(date_insert, max(activity_date)) desc nulls last
);