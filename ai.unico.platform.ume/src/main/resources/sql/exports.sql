select "user".user_id, first_name, last_name, email, max(activity_date) as last_login
from "user"
         left join user_activity
                   on "user".user_id = user_activity.user_id and activity_type = 'AUTHENTICATION_SUCCEEDED'
where deleted = false
group by "user".user_id
order by last_login desc nulls last;