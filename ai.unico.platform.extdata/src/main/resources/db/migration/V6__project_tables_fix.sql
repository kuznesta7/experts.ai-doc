alter table t_project_t_finance
    add column if not exists id bigserial primary key;
alter table t_project_t_items_rel
    add column if not exists id bigserial primary key;
alter table t_project_t_organization_rel
    add column if not exists id bigserial primary key;
alter table t_project_t_organization_rel
    rename column role to organization_role;
update t_project_t_organization_rel
set organization_role='PARTICIPANT'
where organization_role = 'ucastnik';
update t_project_t_organization_rel
set organization_role='PROVIDER'
where organization_role = 'poskytovatel';

alter table t_project
    alter column project_start_date type timestamp;
alter table t_project
    alter column project_end_date type timestamp;