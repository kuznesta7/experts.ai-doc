alter table t_expert_item_rel
  add column id bigserial primary key not null;
alter table t_item_t_organization_rel
  add column id bigserial primary key not null;
alter table t_affiliation_t_expert_t_organization
  add column id bigserial primary key not null;
alter table t_organization_struct
    add column id bigserial primary key not null;

update t_organization
set root_organization = null
where root_organization = organization_id_tk;
