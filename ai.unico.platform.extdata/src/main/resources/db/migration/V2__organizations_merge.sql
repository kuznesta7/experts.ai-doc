drop extension if exists unaccent;
create extension unaccent;

alter table t_organization
  add column unaccent text;

alter table t_organization
  add column transformed text;

alter table t_organization
  add column tokenized tsvector;

alter table t_organization
  add column item_count bigint;

update t_organization
set unaccent = trim(unaccent(lower(organization_name)));

update t_organization
set tokenized = to_tsvector(transformed);

update t_organization
set transformed = trim(regexp_replace(regexp_replace(
                                        regexp_replace(unaccent, '[^A-Za-z0-9]', ' ', 'g'),
                                        '([spol]*[ ]*s[ ]*r[ ]*o|a[ ]*s|o[ ]*s*)[ ]*$', '', 'g'), '[ ]+', ' ', 'g'));

create index transformed_name
  on t_organization (transformed);