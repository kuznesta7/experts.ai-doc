package ai.unico.platform.extdata.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "edge", schema = "ontology")
public class OntologyEdgeEntity {

    @Id
    private Long edgeId;

    private String startSkillName;

    private String endSkillName;

    private Double semantic;

    private Long startSkillArtCount;

    private Long endSkillArtCount;

    private Long commonArt;

    public Long getEdgeId() {
        return edgeId;
    }

    public void setEdgeId(Long edgeId) {
        this.edgeId = edgeId;
    }

    public String getStartSkillName() {
        return startSkillName;
    }

    public void setStartSkillName(String startSkillName) {
        this.startSkillName = startSkillName;
    }

    public String getEndSkillName() {
        return endSkillName;
    }

    public void setEndSkillName(String endSkillName) {
        this.endSkillName = endSkillName;
    }

    public Double getSemantic() {
        return semantic;
    }

    public void setSemantic(Double semantic) {
        this.semantic = semantic;
    }

    public Long getStartSkillArtCount() {
        return startSkillArtCount;
    }

    public void setStartSkillArtCount(Long startSkillArtCount) {
        this.startSkillArtCount = startSkillArtCount;
    }

    public Long getEndSkillArtCount() {
        return endSkillArtCount;
    }

    public void setEndSkillArtCount(Long endSkillArtCount) {
        this.endSkillArtCount = endSkillArtCount;
    }

    public Long getCommonArt() {
        return commonArt;
    }

    public void setCommonArt(Long commonArt) {
        this.commonArt = commonArt;
    }
}
