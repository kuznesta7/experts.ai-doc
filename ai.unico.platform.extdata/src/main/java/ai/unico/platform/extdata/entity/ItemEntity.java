package ai.unico.platform.extdata.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "t_item")
public class ItemEntity {

    @Id
    @Column(name = "item_id_tk")
    private Long itemId;

    private String itemBk;

    private String itemNameOrig;

    private String itemNameEn;

    private String descriptionOrig;

    private String descriptionEn;

    private String keywordsTextEn;

    private String keywordsTextOrig;

    private Integer itemYear;

    private boolean confidential;

    private String doi;

    private String itemLink;

    private String itemSpec;

    private String itemSpecGroup;

    private String itemSpecScience;

    @OneToMany
    @JoinColumn(name = "item_id_tk_t_item")
    private Set<ItemOrganizationEntity> itemOrganizationEntities;

    @OneToMany
    @JoinColumn(name = "item_id_tk_t_item")
    private Set<ExpertItemEntity> expertItemEntities;

    @Column(name = "type_id_tk_t_item_type")
    private Long itemTypeId;

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getItemBk() {
        return itemBk;
    }

    public void setItemBk(String itemBk) {
        this.itemBk = itemBk;
    }

    public String getItemNameOrig() {
        return itemNameOrig;
    }

    public void setItemNameOrig(String itemNameOrig) {
        this.itemNameOrig = itemNameOrig;
    }

    public String getItemNameEn() {
        return itemNameEn;
    }

    public void setItemNameEn(String itemNameEn) {
        this.itemNameEn = itemNameEn;
    }

    public String getDescriptionOrig() {
        return descriptionOrig;
    }

    public void setDescriptionOrig(String descriptionOrig) {
        this.descriptionOrig = descriptionOrig;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public Set<ItemOrganizationEntity> getItemOrganizationEntities() {
        return itemOrganizationEntities;
    }

    public void setItemOrganizationEntities(Set<ItemOrganizationEntity> itemOrganizationEntities) {
        this.itemOrganizationEntities = itemOrganizationEntities;
    }

    public Long getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(Long itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public String getKeywordsTextEn() {
        return keywordsTextEn;
    }

    public void setKeywordsTextEn(String keywordsText) {
        this.keywordsTextEn = keywordsText;
    }

    public Integer getItemYear() {
        return itemYear;
    }

    public void setItemYear(Integer itemYear) {
        this.itemYear = itemYear;
    }

    public Set<ExpertItemEntity> getExpertItemEntities() {
        return expertItemEntities;
    }

    public void setExpertItemEntities(Set<ExpertItemEntity> expertItemEntities) {
        this.expertItemEntities = expertItemEntities;
    }

    public String getKeywordsTextOrig() {
        return keywordsTextOrig;
    }

    public void setKeywordsTextOrig(String keywordsTextOrig) {
        this.keywordsTextOrig = keywordsTextOrig;
    }

    public boolean isConfidential() {
        return confidential;
    }

    public void setConfidential(boolean confidential) {
        this.confidential = confidential;
    }

    public String getItemSpec() {
        return itemSpec;
    }

    public void setItemSpec(String itemSpec) {
        this.itemSpec = itemSpec;
    }

    public String getItemSpecGroup() {
        return itemSpecGroup;
    }

    public void setItemSpecGroup(String itemSpecGroup) {
        this.itemSpecGroup = itemSpecGroup;
    }

    public String getItemSpecScience() {
        return itemSpecScience;
    }

    public void setItemSpecScience(String itemSpecScience) {
        this.itemSpecScience = itemSpecScience;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getItemLink() {
        return itemLink;
    }

    public void setItemLink(String itemLink) {
        this.itemLink = itemLink;
    }
}
