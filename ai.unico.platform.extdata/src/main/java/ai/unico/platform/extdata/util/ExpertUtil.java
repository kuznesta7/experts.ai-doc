package ai.unico.platform.extdata.util;

import ai.unico.platform.extdata.dto.ExpertProfileDetailDto;
import ai.unico.platform.extdata.entity.ExpertEntity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ExpertUtil {

    public static ExpertProfileDetailDto convert(ExpertEntity expertEntity) {
        final ExpertProfileDetailDto expertProfileDetailDto = new ExpertProfileDetailDto();
        expertProfileDetailDto.setExpertId(expertEntity.getExpertId());
        expertProfileDetailDto.setExpertFullName(expertEntity.getFullName());
        return expertProfileDetailDto;
    }

    public static List<ExpertProfileDetailDto> convertList(List<ExpertEntity> expertEntities) {
        final List<ExpertProfileDetailDto> result = new ArrayList<>();
        Set<Long> expertIds = new HashSet<>();
        for (ExpertEntity expertEntity : expertEntities) {
            if (expertEntity == null || expertIds.contains(expertEntity.getExpertId())) continue;
            expertIds.add(expertEntity.getExpertId());
            result.add(convert(expertEntity));
        }
        return result;
    }

}
