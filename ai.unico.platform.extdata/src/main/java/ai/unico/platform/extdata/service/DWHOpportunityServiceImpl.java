package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.DWHOpportunityDto;
import ai.unico.platform.extdata.entity.*;
import ai.unico.platform.extdata.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.util.KeywordsUtil;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

public class DWHOpportunityServiceImpl implements DWHOpportunityService{

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    @Transactional
    public List<DWHOpportunityDto> findOpportunities(Integer limit, Integer offset) {
        Criteria criteria = hibernateCriteriaCreator.createCriteria(OpportunityEntity.class, "opportunity");
        criteria.setMaxResults(limit);
        criteria.setFirstResult(offset);
        criteria.addOrder(Order.asc("opportunityId"));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        final List<OpportunityEntity> list = criteria.list();

        return list.stream().map(this::convert).collect(Collectors.toList());
    }

    private DWHOpportunityDto convert(OpportunityEntity entity){
        DWHOpportunityDto dto = new DWHOpportunityDto();
        dto.setOpportunityId(entity.getOpportunityId());
        dto.setOpportunityName(entity.getOpportunityName());
        dto.setOpportunityDescription(entity.getOpportunityDescription());
        dto.setOpportunityKw(KeywordsUtil.convert(entity.getOpportunityKw()));
        dto.setOpportunitySignupDate(entity.getOpportunitySignupDate());
        dto.setOpportunityLocation(entity.getOpportunityLocation());
        dto.setOpportunityWage(entity.getOpportunityWage());
        dto.setOpportunityTechReq(entity.getOpportunityTechReq());
        dto.setOpportunityFormReq(entity.getOpportunityFormReq());
        dto.setOpportunityOtherReq(entity.getOpportunityOtherReq());
        dto.setOpportunityBenefit(entity.getOpportunityBenefit());
        dto.setOpportunityJobStartDate(entity.getOpportunityJobStartDate());
        dto.setOpportunityExtLink(entity.getOpportunityExtLink());
        dto.setOpportunityHomeOffice(entity.getOpportunityHomeOffice());
        dto.setOpportunityType(entity.getOpportunityTypeEntity().getOpportunityTypeId());
        dto.setJobTypes(entity.getJobTypes().stream().map(JobTypeEntity::getJobId).collect(Collectors.toSet()));
        dto.setOrganizationIds(entity.getOrganizationOpportunityEntity().stream().map(OrganizationOpportunityEntity::getOrganizationId).collect(Collectors.toSet()));
        dto.setExpertIds(entity.getExpertOpportunityEntity().stream().map(ExpertOpportunityEntity::getExpertId).collect(Collectors.toSet()));
        return dto;
    }

}
