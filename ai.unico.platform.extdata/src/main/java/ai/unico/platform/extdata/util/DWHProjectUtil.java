package ai.unico.platform.extdata.util;

import ai.unico.platform.extdata.dto.DWHProjectDto;
import ai.unico.platform.extdata.dto.ProjectFinanceDto;
import ai.unico.platform.extdata.entity.*;
import ai.unico.platform.util.KeywordsUtil;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public class DWHProjectUtil {

    public static DWHProjectDto convert(ProjectEntity projectEntity, Collection<ProjectOrganizationEntity> projectOrganizationEntities, Collection<ProjectItemEntity> projectItemEntities, Collection<ProjectExpertEntity> projectExpertEntities, Collection<ProjectFinanceEntity> projectFinanceEntities) {
        final DWHProjectDto dwhProjectDto = new DWHProjectDto();
        dwhProjectDto.setOriginalProjectId(projectEntity.getProjectId());
        dwhProjectDto.setProjectNumber(projectEntity.getProjectNumber());
        dwhProjectDto.setStartDate(projectEntity.getStartDate());
        dwhProjectDto.setEndDate(projectEntity.getEndDate());
        dwhProjectDto.setProjectLink(projectEntity.getProjectLink());
        final String projectName = projectEntity.getProjectName();
        if (projectName != null && !projectName.isEmpty()) {
            dwhProjectDto.setProjectName(projectName);
        } else {
            dwhProjectDto.setTranslationUnavailable(true);
            dwhProjectDto.setProjectName(projectEntity.getProjectNameOrig());
        }
        final String projectDescription = projectEntity.getProjectDescription();
        dwhProjectDto.setProjectDescription(projectDescription != null && !projectDescription.isEmpty() ? projectDescription : projectEntity.getProjectDescriptionOrig());
        final String keywordsString = projectEntity.getKeywords();
        if (keywordsString != null && !keywordsString.isEmpty()) {
            dwhProjectDto.setKeywords(KeywordsUtil.convert(keywordsString));
        } else {
            dwhProjectDto.setKeywords(KeywordsUtil.convert(projectEntity.getKeywordsOrig()));
        }
        dwhProjectDto.setCountryCode(projectEntity.getCountryCode());
        dwhProjectDto.setConfidential(projectEntity.isConfidential());
        dwhProjectDto.setProjectProgramId(HibernateUtil.getId(projectEntity, ProjectEntity::getProjectProgramEntity, ProjectProgramEntity::getId));

        if (projectOrganizationEntities != null) {
            final Set<Long> participatingOrganizations = projectOrganizationEntities.stream()
                    .filter(x -> "PARTICIPANT".equals(x.getOrganizationRole()))
                    .map(ProjectOrganizationEntity::getOrganizationId)
                    .collect(Collectors.toSet());
            final Set<Long> providingOrganizations = projectOrganizationEntities
                    .stream().filter(x -> "PROVIDER".equals(x.getOrganizationRole()))
                    .map(ProjectOrganizationEntity::getOrganizationId)
                    .collect(Collectors.toSet());

            dwhProjectDto.setParticipatingOriginalOrganizationIds(participatingOrganizations);
            dwhProjectDto.setProvidingOriginalOrganizationIds(providingOrganizations);
        }

        if (projectItemEntities != null) {
            final Set<Long> projectItemIds = projectItemEntities.stream()
                    .map(ProjectItemEntity::getItemId)
                    .collect(Collectors.toSet());
            dwhProjectDto.setProjectOriginalItemIds(projectItemIds);
        }

        if (projectExpertEntities != null) {
            final Set<Long> expertIds = projectExpertEntities.stream()
                    .map(ProjectExpertEntity::getExpertId)
                    .collect(Collectors.toSet());
            dwhProjectDto.setProjectExpertIds(expertIds);
        }

        if (projectFinanceEntities != null) {
            for (ProjectFinanceEntity projectFinanceEntity : projectFinanceEntities) {
                final String year = projectFinanceEntity.getYear();
                if (year == null || !year.matches("\\d+")) continue;

                final ProjectFinanceDto projectFinanceDto = new ProjectFinanceDto();
                projectFinanceDto.setYear(Integer.parseInt(year));
                final Long nationalSupport = projectFinanceEntity.getNationalSupport();
                final Long privateFinances = projectFinanceEntity.getPrivateFinances();
                final Long otherFinances = projectFinanceEntity.getOtherFinances();
                final Long total = projectFinanceEntity.getTotal();
                final Long organizationId = projectFinanceEntity.getOrganizationId();

                projectFinanceDto.setNationalSupport(nationalSupport != null ? Double.valueOf(nationalSupport) : null);
                projectFinanceDto.setPrivateFinances(privateFinances != null ? Double.valueOf(privateFinances) : null);
                projectFinanceDto.setOtherFinances(otherFinances != null ? Double.valueOf(otherFinances) : null);
                projectFinanceDto.setTotal(total != null ? Double.valueOf(total) : null);
                projectFinanceDto.setOriginalOrganizationId(organizationId);
                dwhProjectDto.getProjectFinances().add(projectFinanceDto);
            }
        }
        return dwhProjectDto;
    }

}
