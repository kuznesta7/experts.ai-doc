package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.DWHProjectDto;
import ai.unico.platform.extdata.entity.*;
import ai.unico.platform.extdata.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.extdata.util.DWHProjectUtil;
import ai.unico.platform.util.exception.NonexistentEntityException;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;
import java.util.stream.Collectors;

public class DWHProjectServiceImpl implements DWHProjectService {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public List<DWHProjectDto> findProjects(Integer limit, Integer offset) {
        final Criteria projectCriteria = hibernateCriteriaCreator.createCriteria(ProjectEntity.class, "project");
        projectCriteria.addOrder(Order.asc("projectId"));
        projectCriteria.setMaxResults(limit);
        projectCriteria.setFirstResult(offset);
        final List<ProjectEntity> projectList = projectCriteria.list();
        if (projectList.isEmpty()) return Collections.emptyList();

        final Set<Long> projectIds = projectList.stream().map(ProjectEntity::getProjectId).collect(Collectors.toSet());

        final Criteria projectOrganizationCriteria = hibernateCriteriaCreator.createCriteria(ProjectOrganizationEntity.class, "projectOrganization");
        projectOrganizationCriteria.add(Restrictions.in("projectId", projectIds));
        final List<ProjectOrganizationEntity> projectOrganizations = projectOrganizationCriteria.list();
        final Map<Long, List<ProjectOrganizationEntity>> projectIdProjectOrganizationsMap = projectOrganizations.stream().collect(Collectors.groupingBy(ProjectOrganizationEntity::getProjectId));

        final Criteria projectItemCriteria = hibernateCriteriaCreator.createCriteria(ProjectItemEntity.class, "projectItem");
        projectItemCriteria.add(Restrictions.in("projectId", projectIds));
        final List<ProjectItemEntity> projectItemEntities = projectItemCriteria.list();
        final Map<Long, List<ProjectItemEntity>> projectIdProjectItemsMap = projectItemEntities.stream().collect(Collectors.groupingBy(ProjectItemEntity::getProjectId));

        final Criteria projectFinanceCriteria = hibernateCriteriaCreator.createCriteria(ProjectFinanceEntity.class, "projectFinance");
        projectFinanceCriteria.add(Restrictions.in("projectId", projectIds));
        final List<ProjectFinanceEntity> projectFinanceEntities = projectFinanceCriteria.list();
        final Map<Long, List<ProjectFinanceEntity>> projectIdProjectFinancesMap = projectFinanceEntities.stream().collect(Collectors.groupingBy(ProjectFinanceEntity::getProjectId));

        final Criteria projectExpertCriteria = hibernateCriteriaCreator.createCriteria(ProjectExpertEntity.class, "projectExpert");
        projectExpertCriteria.add(Restrictions.in("projectId", projectIds));
        final List<ProjectExpertEntity> projectExpertEntities = projectExpertCriteria.list();
        final Map<Long, List<ProjectExpertEntity>> projectIdExpertsMap = projectExpertEntities.stream().collect(Collectors.groupingBy(ProjectExpertEntity::getProjectId));

        final List<DWHProjectDto> dwhProjectDtos = new ArrayList<>();
        for (ProjectEntity projectEntity : projectList) {
            final Long projectId = projectEntity.getProjectId();
            final List<ProjectFinanceEntity> finances = projectIdProjectFinancesMap.get(projectId);
            final List<ProjectItemEntity> items = projectIdProjectItemsMap.get(projectId);
            final List<ProjectOrganizationEntity> organizations = projectIdProjectOrganizationsMap.get(projectId);
            final List<ProjectExpertEntity> experts = projectIdExpertsMap.get(projectId);
            dwhProjectDtos.add(DWHProjectUtil.convert(projectEntity, organizations, items, experts, finances));
        }
        return dwhProjectDtos;
    }

    @Override
    @Transactional
    public DWHProjectDto findProject(Long originalProjectId) {
        final ProjectEntity projectEntity = entityManager.find(ProjectEntity.class, originalProjectId);
        if (projectEntity == null) throw new NonexistentEntityException(ProjectEntity.class);
        return DWHProjectUtil.convert(projectEntity, projectEntity.getProjectOrganizationEntities(), projectEntity.getProjectItemEntities(), projectEntity.getProjectExpertEntities(), projectEntity.getProjectFinanceEntities());
    }
}
