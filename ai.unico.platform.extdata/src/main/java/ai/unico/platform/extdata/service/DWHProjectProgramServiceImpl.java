package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.DWHProjectProgramDto;
import ai.unico.platform.extdata.entity.ProjectProgramEntity;
import ai.unico.platform.extdata.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

public class DWHProjectProgramServiceImpl implements DWHProjectProgramService {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    @Transactional
    public List<DWHProjectProgramDto> findProjectPrograms() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ProjectProgramEntity.class, "projectProgram");
        final List<ProjectProgramEntity> list = criteria.list();
        return list.stream().map(this::convert).collect(Collectors.toList());
    }

    private DWHProjectProgramDto convert(ProjectProgramEntity projectProgramEntity) {
        final DWHProjectProgramDto dwhProjectProgramDto = new DWHProjectProgramDto();
        dwhProjectProgramDto.setProjectProgramId(projectProgramEntity.getId());
        dwhProjectProgramDto.setProjectProgramName(projectProgramEntity.getName());
        return dwhProjectProgramDto;
    }
}
