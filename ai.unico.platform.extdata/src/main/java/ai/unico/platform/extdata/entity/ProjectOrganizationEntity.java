package ai.unico.platform.extdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "t_project_t_organization_rel")
public class ProjectOrganizationEntity implements Serializable {

    @Id
    @Column(name = "project_id_tk_t_project")
    private Long projectId;

    @Id
    @Column(name = "organization_id_tk_t_organization")
    private Long organizationId;

    private String organizationRole;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationRole() {
        return organizationRole;
    }

    public void setOrganizationRole(String role) {
        this.organizationRole = role;
    }
}
