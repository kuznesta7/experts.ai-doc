package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.DWHEquipmentDto;
import ai.unico.platform.extdata.entity.EquipmentEntity;
import ai.unico.platform.extdata.entity.EquipmentOrganizationEntity;
import ai.unico.platform.extdata.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

public class DWHEquipmentServiceImpl implements DWHEquipmentService {

    @Autowired
    private HibernateCriteriaCreator criteriaCreator;

    @Override
    @Transactional
    public List<DWHEquipmentDto> findEquipment(Integer offset, Integer limit) {
        Criteria criteria = criteriaCreator.createCriteria(EquipmentEntity.class, "equipment");
        criteria.setMaxResults(limit);
        criteria.setFirstResult(offset);
        criteria.addOrder(Order.asc("equipmentId"));
        List<EquipmentEntity> equipmentEntities = criteria.list();
        if (equipmentEntities.isEmpty())
            return Collections.emptyList();

        Set<Long> equipmentIds = equipmentEntities.stream().map(EquipmentEntity::getEquipmentId).collect(Collectors.toSet());

        Criteria organizationCriteria = criteriaCreator.createCriteria(EquipmentOrganizationEntity.class, "equipmentOrganization");
        organizationCriteria.add(Restrictions.in("equipmentId", equipmentIds));
        List<EquipmentOrganizationEntity> organizations = organizationCriteria.list();
        Map<Long, List<Long>> equipmentOrganizations = new HashMap<>();
        organizations.forEach(x -> {
            equipmentOrganizations.putIfAbsent(x.getEquipmentId(), new ArrayList<>());
            equipmentOrganizations.get(x.getEquipmentId()).add(x.getOrganizationId());
        });
        return equipmentEntities.stream().map(x -> convert(x, equipmentOrganizations)).collect(Collectors.toList());
    }

    private static DWHEquipmentDto convert(EquipmentEntity equipmentEntity, Map<Long, List<Long>> equipmentOrganizations) {
        DWHEquipmentDto dwhEquipmentDto = new DWHEquipmentDto();
        String specialization = equipmentEntity.getEquipmentSpecializationEn() != null ? equipmentEntity.getEquipmentSpecializationEn() : equipmentEntity.getEquipmentSpecializationOrig();
        String description = equipmentEntity.getEquipmentDescEn() != null ? equipmentEntity.getEquipmentDescEn() : equipmentEntity.getEquipmentDescOrig();
        if (description != null)
            description = description.replace(';', '\n');
        List<String> spec = new ArrayList<>();
        if (specialization != null && !specialization.equals("")){
            spec.addAll(Arrays.asList(specialization.split(";[ ]*")));
        }
        dwhEquipmentDto.setEquipmentId(equipmentEntity.getEquipmentId());
        dwhEquipmentDto.setEquipmentName(equipmentEntity.getEquipmentNameEn() != null ? equipmentEntity.getEquipmentNameEn() : equipmentEntity.getEquipmentNameOrig());
        dwhEquipmentDto.setEquipmentDesc(description);
        dwhEquipmentDto.setEquipmentMarking(equipmentEntity.getEquipmentMarking());
        dwhEquipmentDto.setEquipmentSpecialization(spec);
        dwhEquipmentDto.setYear(equipmentEntity.getYear());
        dwhEquipmentDto.setEquipmentServiceLife(equipmentEntity.getEquipmentServiceLife());
        dwhEquipmentDto.setPortableDevice(equipmentEntity.getPortableDevice());
        if (equipmentEntity.getEquipmentDomainEntity() != null)
            dwhEquipmentDto.setEquipmentDomainId(equipmentEntity.getEquipmentDomainEntity().getDomainId());
        if (equipmentEntity.getEquipmentTypeEntity() != null)
            dwhEquipmentDto.setEquipmentTypeId(equipmentEntity.getEquipmentTypeEntity().getTypeId());
        dwhEquipmentDto.setTranslationUnavailable(equipmentEntity.getEquipmentNameEn() == null);
        dwhEquipmentDto.setOrganizationIds(equipmentOrganizations.getOrDefault(equipmentEntity.getEquipmentId(), new ArrayList<>()));
        return dwhEquipmentDto;
    }
}
