package ai.unico.platform.extdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_organization")
public class OrganizationEntity {

    @Id
    @Column(name = "organization_id_tk")
    private Long organizationId;

    private String organizationName;

    private String organizationDescription;

    @Column(name = "organization_country_code")
    private String countryCode;

    @Column(name = "substitute_org_tk")
    private Long substituteOrganizationId;

    private Long itemCount;

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationDescription() {
        return organizationDescription;
    }

    public void setOrganizationDescription(String organizationDescription) {
        this.organizationDescription = organizationDescription;
    }

    public Long getSubstituteOrganizationId() {
        return substituteOrganizationId;
    }

    public void setSubstituteOrganizationId(Long substituteOrganizationId) {
        this.substituteOrganizationId = substituteOrganizationId;
    }

    public Long getItemCount() {
        return itemCount;
    }

    public void setItemCount(Long itemCount) {
        this.itemCount = itemCount;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
