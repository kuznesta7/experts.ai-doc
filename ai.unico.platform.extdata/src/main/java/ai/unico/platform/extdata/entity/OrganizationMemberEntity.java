package ai.unico.platform.extdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "t_organization_member")
public class OrganizationMemberEntity implements Serializable {

    @Id
    @Column(name = "parent_t_organization_id_tk")
    private Long parent;

    @Id
    @Column(name = "child_t_organization_id_tk")
    private Long child;

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public Long getChild() {
        return child;
    }

    public void setChild(Long child) {
        this.child = child;
    }
}
