package ai.unico.platform.extdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "t_expert_t_project_rel")
public class ProjectExpertEntity implements Serializable {

    @Id
    @Column(name = "expert_id_tk_t_expert")
    private Long expertId;

    @Id
    @Column(name = "project_id_tk_t_project")
    private Long projectId;

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }
}
