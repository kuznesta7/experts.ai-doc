package ai.unico.platform.extdata.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_expert_item_rel")
public class ExpertItemEntity {

    @Id
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "expert_id_tk_t_expert")
    private ExpertEntity expertEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_id_tk_t_item")
    private ItemEntity itemEntity;

    private String role;

    public ExpertEntity getExpertEntity() {
        return expertEntity;
    }

    public void setExpertEntity(ExpertEntity expertEntity) {
        this.expertEntity = expertEntity;
    }

    public ItemEntity getItemEntity() {
        return itemEntity;
    }

    public void setItemEntity(ItemEntity itemEntity) {
        this.itemEntity = itemEntity;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
