package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.OntologyItemDto;
import ai.unico.platform.extdata.entity.OntologyEdgeEntity;
import ai.unico.platform.extdata.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

public class OntologyServiceImpl implements OntologyService {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    @Transactional
    public Set<OntologyItemDto> getOntologyItems(int firstResult, int maxResult) {

        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OntologyEdgeEntity.class, "edge");
        criteria.add(
                Restrictions.or(
                        Restrictions.isNotNull("edge.semantic"),
                        Restrictions.and(
                                Restrictions.isNotNull("edge.startSkillArtCount"),
                                Restrictions.ge("edge.startSkillArtCount", 10L))));
        criteria.addOrder(Order.desc("edgeId"));
        criteria.setFirstResult(firstResult);
        criteria.setMaxResults(maxResult);

        final List<OntologyEdgeEntity> list = criteria.list();

        final Map<String, OntologyItemDto> ontologyMap = new HashMap<>();
        for (OntologyEdgeEntity edgeEntity : list) {
            final String startSkillName = edgeEntity.getStartSkillName();
            final String endSkillName = edgeEntity.getEndSkillName();
            if (startSkillName == null || endSkillName == null) continue;

            final Double relevancy;
            if (edgeEntity.getSemantic() != null && !edgeEntity.getSemantic().equals(0D)) {
                relevancy = edgeEntity.getSemantic();
            } else {
                final Long commonArt = edgeEntity.getCommonArt();
                final Long startSkillArtCount = edgeEntity.getStartSkillArtCount();

                if (commonArt == null || startSkillArtCount == null) continue;

                final double freq = (double) commonArt / (double) startSkillArtCount * 2 * Math.atan(startSkillArtCount / 50F) / Math.PI; // total count factor

                if (freq < 0.01) continue; //constant to play with

                relevancy = Math.pow(freq, 0.3); //another constant to play with
            }
            if (!ontologyMap.containsKey(startSkillName)) {
                final OntologyItemDto ontologyItemDto = new OntologyItemDto();
                ontologyItemDto.setValue(convertName(startSkillName));
                ontologyMap.put(startSkillName, ontologyItemDto);
            }
            final OntologyItemDto ontologyItemDto = ontologyMap.get(startSkillName);
            final OntologyItemDto.RelatedItem relatedItem = new OntologyItemDto.RelatedItem();
            relatedItem.setRelevancy(relevancy);
            relatedItem.setValue(convertName(endSkillName));
            ontologyItemDto.getRelatedItems().add(relatedItem);
        }

        return new HashSet<>(ontologyMap.values());
    }

    private String convertName(String originalName) {
        return originalName.replaceAll("_", " ");
    }
}
