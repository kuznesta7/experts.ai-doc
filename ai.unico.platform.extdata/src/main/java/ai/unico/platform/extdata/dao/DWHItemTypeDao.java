package ai.unico.platform.extdata.dao;

import ai.unico.platform.extdata.entity.ItemTypeEntity;


import java.util.List;

public interface DWHItemTypeDao {


    List<ItemTypeEntity> findAll();


}
