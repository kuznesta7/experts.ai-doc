package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dao.DWHItemTypeDao;
import ai.unico.platform.extdata.dto.DWHItemTypeDto;
import ai.unico.platform.extdata.entity.ItemTypeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public class DWHItemTypeServiceImpl implements DWHItemTypeService {

    @Autowired
    private DWHItemTypeDao itemTypeDao;

    @Override
    @Transactional
    public List<DWHItemTypeDto> findItemTypes() {
        final List<DWHItemTypeDto> itemTypeDtos = new ArrayList<>();
        final List<ItemTypeEntity> itemTypeEntities = itemTypeDao.findAll();
        for (ItemTypeEntity itemTypeEntity : itemTypeEntities) {
            final DWHItemTypeDto itemTypeDto = new DWHItemTypeDto();
            itemTypeDto.setTypeId(itemTypeEntity.getTypeId());
            itemTypeDto.setTypeCode(itemTypeEntity.getTypeCode());
            itemTypeDto.setTypeName(itemTypeEntity.getTypeName());
            itemTypeDto.setTypeNameEn(itemTypeEntity.getTypeNameEn());

            itemTypeDtos.add(itemTypeDto);
        }
        return itemTypeDtos;
    }
}
