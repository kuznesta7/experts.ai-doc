package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.ExpertThesisPreviewDto;
import ai.unico.platform.extdata.entity.ExpertItemEntity;
import ai.unico.platform.extdata.entity.ItemEntity;
import ai.unico.platform.extdata.entity.ItemOrganizationEntity;
import ai.unico.platform.extdata.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.extdata.util.ThesisUtil;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.*;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

public class DWHThesisServiceImpl implements DWHThesisService {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    @Transactional
    public List<ExpertThesisPreviewDto> findThesisPreviews(Integer limit, Integer offset) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ItemEntity.class, "item");
        criteria.setMaxResults(limit);
        criteria.setFirstResult(offset);
        criteria.addOrder(Order.asc("itemId"));
        //filter out thesis
        criteria.add(Restrictions.in("itemTypeId", Arrays.asList(68L, 69L)));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        final List<ItemEntity> list = criteria.list();
        final List<ExpertThesisPreviewDto> itemDtos = list.stream().map(ThesisUtil::convert).collect(Collectors.toList());
        final Set<Long> itemIds = list.stream().map(ItemEntity::getItemId).collect(Collectors.toSet());
        if (itemIds.size() == 0) return itemDtos;

        final Criteria organizationCriteria = hibernateCriteriaCreator.createCriteria(ItemOrganizationEntity.class, "itemOrganization");
        organizationCriteria.add(Restrictions.in("itemEntity.itemId", itemIds));
        final ProjectionList organizationProjections = Projections.projectionList()
                .add(Projections.groupProperty("itemEntity.itemId"), "itemId")
                .add(Projections.groupProperty("organizationEntity.organizationId"), "organizationId");
        organizationCriteria.setProjection(organizationProjections);
        organizationCriteria.setResultTransformer(new AliasToBeanResultTransformer(DWHItemService.ItemOrganization.class));
        final List<DWHItemService.ItemOrganization> itemsOrganizationIds = organizationCriteria.list();
        final Map<Long, List<DWHItemService.ItemOrganization>> itemIdOrganizationIdsMap = itemsOrganizationIds.stream().collect(Collectors.groupingBy(DWHItemService.ItemOrganization::getItemId));

        final Criteria expertCriteria = hibernateCriteriaCreator.createCriteria(ExpertItemEntity.class, "expertItem");
        expertCriteria.add(Restrictions.in("itemEntity.itemId", itemIds));
        final ProjectionList expertProjections = Projections.projectionList()
                .add(Projections.groupProperty("itemEntity.itemId"), "itemId")
                .add(Projections.groupProperty("expertEntity.expertId"), "expertId")
                .add(Projections.groupProperty("role"), "role");
        expertCriteria.setProjection(expertProjections);
        expertCriteria.setResultTransformer(new AliasToBeanResultTransformer(DWHItemService.ItemExpert.class));
        final List<DWHItemService.ItemExpert> itemExpertIds = expertCriteria.list();
        final Map<Long, List<DWHItemService.ItemExpert>> itemIdExpertIdsMap = itemExpertIds.stream().collect(Collectors.groupingBy(DWHItemService.ItemExpert::getItemId));

        for (ExpertThesisPreviewDto itemDto : itemDtos) {
            final Long itemId = itemDto.getItemId();
            if (itemIdOrganizationIdsMap.containsKey(itemId)) {
                final Set<Long> organizationIds = itemIdOrganizationIdsMap.get(itemId).stream().map(DWHItemService.ItemOrganization::getOrganizationId).collect(Collectors.toSet());
                itemDto.setOriginalOrganizationIds(organizationIds);
            }
            if (itemIdExpertIdsMap.containsKey(itemId)) {
                final Map<String, Long> expertIds = itemIdExpertIdsMap.get(itemId).stream().collect(Collectors.toMap(DWHItemService.ItemExpert::getRole, DWHItemService.ItemExpert::getExpertId));
                itemDto.setExpertIds(expertIds);
            }
        }

        return itemDtos;
    }

    @Override
    public Map<Long, HashMap<String, Set<Long>>> findExpertIdsForThesis(Set<Long> itemIds) {
        if (itemIds.isEmpty()) return Collections.emptyMap();
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ExpertItemEntity.class, "expertItem");
        criteria.createAlias("expertItem.expertEntity", "expert");
        criteria.createAlias("expertItem.itemEntity", "item");
        criteria.setFetchMode("expert", FetchMode.JOIN);
        criteria.setFetchMode("item", FetchMode.JOIN);
        criteria.add(Restrictions.in("item.itemId", itemIds));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        final List<ExpertItemEntity> list = criteria.list();
        final HashMap<Long, HashMap<String, Set<Long>>> expertIdsForItemMap = new HashMap<>();
        for (ExpertItemEntity expertItemEntity : list) {
            final Long itemId = expertItemEntity.getItemEntity().getItemId();
            final Long expertId = expertItemEntity.getExpertEntity().getExpertId();
            final String role = expertItemEntity.getRole();
            final HashMap<String, Set<Long>> expertRolesIds = expertIdsForItemMap.computeIfAbsent(itemId, x -> new HashMap<>());
            final Set<Long> expertIds = expertRolesIds.computeIfAbsent(role, x -> new HashSet<>());
            expertIds.add(expertId);
        }
        return expertIdsForItemMap;
    }
}
