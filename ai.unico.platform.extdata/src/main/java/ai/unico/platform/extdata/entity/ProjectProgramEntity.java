package ai.unico.platform.extdata.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_project_program")
public class ProjectProgramEntity {

    @Id
    private Long id;

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long projectProgramId) {
        this.id = projectProgramId;
    }

    public String getName() {
        return name;
    }

    public void setName(String projectProgramName) {
        this.name = projectProgramName;
    }
}
