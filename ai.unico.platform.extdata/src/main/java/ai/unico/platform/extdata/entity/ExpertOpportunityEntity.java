package ai.unico.platform.extdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "t_opportunity_t_expert_rel")
public class ExpertOpportunityEntity implements Serializable {
    @Id
    @Column(name = "opportunity_id_tk_t_opportunity")
    private Long opportunityId;
    @Id
    @Column(name = "expert_id_tk_t_expert")
    private Long expertId;
    private String role;

    public Long getOpportunityId() {
        return opportunityId;
    }

    public void setOpportunityId(Long opportunityId) {
        this.opportunityId = opportunityId;
    }

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
