package ai.unico.platform.extdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_opportunity_type")
public class OpportunityTypeEntity {
    @Id
    @Column(name = "opportunity_type_id_tk")
    private Long opportunityTypeId;
    @Column(name = "opportunity_type_name")
    private String name;

    public Long getOpportunityTypeId() {
        return opportunityTypeId;
    }

    public void setOpportunityTypeId(Long opportunityId) {
        this.opportunityTypeId = opportunityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
