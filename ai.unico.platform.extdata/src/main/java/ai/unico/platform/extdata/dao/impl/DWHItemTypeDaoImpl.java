package ai.unico.platform.extdata.dao.impl;


import ai.unico.platform.extdata.dao.DWHItemTypeDao;
import ai.unico.platform.extdata.entity.ItemTypeEntity;
import ai.unico.platform.extdata.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class DWHItemTypeDaoImpl implements DWHItemTypeDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    public List<ItemTypeEntity> findAll() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ItemTypeEntity.class, "itemType");
        return (List<ItemTypeEntity>) criteria.list();
    }

}
