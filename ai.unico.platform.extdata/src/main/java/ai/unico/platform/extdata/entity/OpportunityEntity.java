package ai.unico.platform.extdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "t_opportunity")
public class OpportunityEntity {
    @Id
    @Column(name = "opportunity_id_tk")
    private Long opportunityId;
    private String opportunityName;
    private String opportunityDescription;
    private String opportunityKw; // split by , to list
    private Timestamp opportunitySignupDate;
    private String opportunityLocation;
    private String opportunityWage;
    private String opportunityTechReq;
    private String opportunityFormReq;
    private String opportunityOtherReq;
    private String opportunityBenefit;
    private Timestamp opportunityJobStartDate;
    private String opportunityExtLink;
    private String opportunityHomeOffice;
    private String opportunityIdBk;
    @ManyToOne
    @JoinColumn(name = "opportunity_type_id_tk_t_opportunity_type")
    private OpportunityTypeEntity opportunityTypeEntity;
    @ManyToMany(targetEntity = JobTypeEntity.class)
    @JoinTable(name = "t_opportunity_t_job_type_rel",
            inverseJoinColumns = @JoinColumn(name = "job_type_id_tk_t_job_type"),
            joinColumns = @JoinColumn(name = "opportunity_id_tk_t_opportunity"))
    private Set<JobTypeEntity> jobTypes;
    @OneToMany
    @JoinColumn(name = "opportunity_id_tk_t_opportunity")
    private Set<OrganizationOpportunityEntity> organizationOpportunityEntity;

    @OneToMany
    @JoinColumn(name = "opportunity_id_tk_t_opportunity")
    private Set<ExpertOpportunityEntity> expertOpportunityEntity;

    public Long getOpportunityId() {
        return opportunityId;
    }

    public void setOpportunityId(Long opportunityId) {
        this.opportunityId = opportunityId;
    }

    public String getOpportunityName() {
        return opportunityName;
    }

    public void setOpportunityName(String opportunityName) {
        this.opportunityName = opportunityName;
    }

    public String getOpportunityDescription() {
        return opportunityDescription;
    }

    public void setOpportunityDescription(String opportunityDescription) {
        this.opportunityDescription = opportunityDescription;
    }

    public String getOpportunityKw() {
        return opportunityKw;
    }

    public void setOpportunityKw(String opportunityKw) {
        this.opportunityKw = opportunityKw;
    }

    public Date getOpportunitySignupDate() {
        return opportunitySignupDate;
    }

    public void setOpportunitySignupDate(Timestamp opportunitySignupDate) {
        this.opportunitySignupDate = opportunitySignupDate;
    }

    public String getOpportunityLocation() {
        return opportunityLocation;
    }

    public void setOpportunityLocation(String opportunityLocation) {
        this.opportunityLocation = opportunityLocation;
    }

    public String getOpportunityWage() {
        return opportunityWage;
    }

    public void setOpportunityWage(String opportunityWage) {
        this.opportunityWage = opportunityWage;
    }

    public String getOpportunityTechReq() {
        return opportunityTechReq;
    }

    public void setOpportunityTechReq(String opportunityTechReq) {
        this.opportunityTechReq = opportunityTechReq;
    }

    public String getOpportunityFormReq() {
        return opportunityFormReq;
    }

    public void setOpportunityFormReq(String opportunityFormReq) {
        this.opportunityFormReq = opportunityFormReq;
    }

    public String getOpportunityOtherReq() {
        return opportunityOtherReq;
    }

    public void setOpportunityOtherReq(String opportunityOtherReq) {
        this.opportunityOtherReq = opportunityOtherReq;
    }

    public String getOpportunityBenefit() {
        return opportunityBenefit;
    }

    public void setOpportunityBenefit(String opportunityBenefit) {
        this.opportunityBenefit = opportunityBenefit;
    }

    public Date getOpportunityJobStartDate() {
        return opportunityJobStartDate;
    }

    public void setOpportunityJobStartDate(Timestamp opportunityJobStartDate) {
        this.opportunityJobStartDate = opportunityJobStartDate;
    }

    public String getOpportunityExtLink() {
        return opportunityExtLink;
    }

    public void setOpportunityExtLink(String opportunityExtLink) {
        this.opportunityExtLink = opportunityExtLink;
    }

    public String getOpportunityHomeOffice() {
        return opportunityHomeOffice;
    }

    public void setOpportunityHomeOffice(String opportunityHomeOffice) {
        this.opportunityHomeOffice = opportunityHomeOffice;
    }

    public String getOpportunityIdBk() {
        return opportunityIdBk;
    }

    public void setOpportunityIdBk(String opportunityIdBk) {
        this.opportunityIdBk = opportunityIdBk;
    }

    public OpportunityTypeEntity getOpportunityTypeEntity() {
        return opportunityTypeEntity;
    }

    public void setOpportunityTypeEntity(OpportunityTypeEntity opportunityTypeEntity) {
        this.opportunityTypeEntity = opportunityTypeEntity;
    }

    public Set<JobTypeEntity> getJobTypes() {
        return jobTypes;
    }

    public void setJobTypes(Set<JobTypeEntity> jobTypes) {
        this.jobTypes = jobTypes;
    }

    public Set<OrganizationOpportunityEntity> getOrganizationOpportunityEntity() {
        return organizationOpportunityEntity;
    }

    public void setOrganizationOpportunityEntity(Set<OrganizationOpportunityEntity> organizationOpportunityEntity) {
        this.organizationOpportunityEntity = organizationOpportunityEntity;
    }

    public Set<ExpertOpportunityEntity> getExpertOpportunityEntity() {
        return expertOpportunityEntity;
    }

    public void setExpertOpportunityEntity(Set<ExpertOpportunityEntity> expertOpportunityEntity) {
        this.expertOpportunityEntity = expertOpportunityEntity;
    }
}
