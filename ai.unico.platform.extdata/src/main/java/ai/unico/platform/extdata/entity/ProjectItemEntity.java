package ai.unico.platform.extdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "t_project_t_items_rel")
public class ProjectItemEntity implements Serializable {

    @Id
    @Column(name = "project_id_tk_t_project")
    private Long projectId;

    @Id
    @Column(name = "item_id_tk_t_item")
    private Long itemId;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }
}
