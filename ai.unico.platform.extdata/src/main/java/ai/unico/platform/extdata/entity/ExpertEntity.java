package ai.unico.platform.extdata.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "t_expert")
public class ExpertEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "expert_id_tk")
    private Long expertId;

    private String fullName;

    @OneToMany
    @JoinColumn(name = "expert_id_tk_t_expert")
    private Set<ExpertItemEntity> expertItemEntities;

    @OneToMany
    @JoinColumn(name = "expert_id_tk_t_expert")
    private Set<ExpertAffiliationEntity> expertAffiliationEntities;

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Set<ExpertItemEntity> getExpertItemEntities() {
        return expertItemEntities;
    }

    public void setExpertItemEntities(Set<ExpertItemEntity> expertItemEntities) {
        this.expertItemEntities = expertItemEntities;
    }

    public Set<ExpertAffiliationEntity> getExpertAffiliationEntities() {
        return expertAffiliationEntities;
    }

    public void setExpertAffiliationEntities(Set<ExpertAffiliationEntity> expertAffiliationEntities) {
        this.expertAffiliationEntities = expertAffiliationEntities;
    }
}
