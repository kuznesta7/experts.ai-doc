package ai.unico.platform.extdata.util;

import ai.unico.platform.extdata.entity.OrganizationEntity;
import ai.unico.platform.util.dto.OrganizationDto;

public class OrganizationUtil {

    public static OrganizationDto convert(OrganizationEntity organizationEntity) {
        final OrganizationDto organizationDto = new OrganizationDto();
        organizationDto.setOrganizationId(organizationEntity.getOrganizationId());
        organizationDto.setOrganizationName(organizationEntity.getOrganizationName());
        organizationDto.setGdpr(false);
        return organizationDto;
    }

}
