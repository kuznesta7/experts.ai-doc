package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.ExpertLectureDto;
import ai.unico.platform.extdata.entity.ExpertItemEntity;
import ai.unico.platform.extdata.entity.ItemEntity;
import ai.unico.platform.extdata.entity.ItemOrganizationEntity;
import ai.unico.platform.extdata.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.extdata.util.LectureUtil;
import org.hibernate.Criteria;
import org.hibernate.criterion.*;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class DWHLectureServiceImpl implements DWHLectureService {
    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    @Transactional
    public List<ExpertLectureDto> findItemPreviews(Integer limit, Integer offset) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ItemEntity.class, "item");
        criteria.setMaxResults(limit);
        criteria.setFirstResult(offset);
        criteria.addOrder(Order.asc("itemId"));
        //filter out lectures
        criteria.add(Restrictions.in("itemTypeId", Collections.singletonList(70L)));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        final List<ItemEntity> list = criteria.list();
        final List<ExpertLectureDto> lectureDtos = list.stream().map(LectureUtil::convert).collect(Collectors.toList());
        final Set<Long> itemIds = list.stream().map(ItemEntity::getItemId).collect(Collectors.toSet());
        if (itemIds.size() == 0) return lectureDtos;

        final Criteria organizationCriteria = hibernateCriteriaCreator.createCriteria(ItemOrganizationEntity.class, "itemOrganization");
        organizationCriteria.add(Restrictions.in("itemEntity.itemId", itemIds));
        final ProjectionList organizationProjections = Projections.projectionList()
                .add(Projections.groupProperty("itemEntity.itemId"), "itemId")
                .add(Projections.groupProperty("organizationEntity.organizationId"), "organizationId");
        organizationCriteria.setProjection(organizationProjections);
        organizationCriteria.setResultTransformer(new AliasToBeanResultTransformer(DWHItemService.ItemOrganization.class));
        final List<DWHItemService.ItemOrganization> itemsOrganizationIds = organizationCriteria.list();
        final Map<Long, List<DWHItemService.ItemOrganization>> itemIdOrganizationIdsMap = itemsOrganizationIds.stream().collect(Collectors.groupingBy(DWHItemService.ItemOrganization::getItemId));

        final Criteria expertCriteria = hibernateCriteriaCreator.createCriteria(ExpertItemEntity.class, "expertItem");
        expertCriteria.add(Restrictions.in("itemEntity.itemId", itemIds));
        final ProjectionList expertProjections = Projections.projectionList()
                .add(Projections.groupProperty("itemEntity.itemId"), "itemId")
                .add(Projections.groupProperty("expertEntity.expertId"), "expertId");
        expertCriteria.setProjection(expertProjections);
        expertCriteria.setResultTransformer(new AliasToBeanResultTransformer(DWHItemService.ItemExpert.class));
        final List<DWHItemService.ItemExpert> itemExpertIds = expertCriteria.list();
        final Map<Long, List<DWHItemService.ItemExpert>> itemIdExpertIdsMap = itemExpertIds.stream().collect(Collectors.groupingBy(DWHItemService.ItemExpert::getItemId));

        for (ExpertLectureDto expertLectureDto : lectureDtos) {
            final Long itemId = expertLectureDto.getItemId();
            if (itemIdOrganizationIdsMap.containsKey(itemId)) {
                final Set<Long> organizationIds = itemIdOrganizationIdsMap.get(itemId).stream().map(DWHItemService.ItemOrganization::getOrganizationId).collect(Collectors.toSet());
                expertLectureDto.setOriginalOrganizationIds(organizationIds);
            }
            if (itemIdExpertIdsMap.containsKey(itemId)) {
                final Set<Long> expertIds = itemIdExpertIdsMap.get(itemId).stream().map(DWHItemService.ItemExpert::getExpertId).collect(Collectors.toSet());
                expertLectureDto.setExpertIds(expertIds);
            }
        }

        return lectureDtos;
    }

}
