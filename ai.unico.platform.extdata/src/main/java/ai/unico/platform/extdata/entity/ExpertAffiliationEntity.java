package ai.unico.platform.extdata.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_affiliation_t_expert_t_organization")
public class ExpertAffiliationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @ManyToOne
    @JoinColumn(name = "expert_id_tk_t_expert")
    private ExpertEntity expertEntity;

    @ManyToOne
    @JoinColumn(name = "organization_id_tk_t_organization")
    private OrganizationEntity organizationEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ExpertEntity getExpertEntity() {
        return expertEntity;
    }

    public void setExpertEntity(ExpertEntity expertEntity) {
        this.expertEntity = expertEntity;
    }

    public OrganizationEntity getOrganizationEntity() {
        return organizationEntity;
    }

    public void setOrganizationEntity(OrganizationEntity organizationEntity) {
        this.organizationEntity = organizationEntity;
    }
}
