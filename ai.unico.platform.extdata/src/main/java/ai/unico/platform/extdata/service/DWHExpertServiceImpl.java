package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.ClaimSuggestionDto;
import ai.unico.platform.extdata.dto.ExpertItemPreviewDto;
import ai.unico.platform.extdata.dto.ExpertProfilePreviewDto;
import ai.unico.platform.extdata.entity.*;
import ai.unico.platform.extdata.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.extdata.util.ItemUtil;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class DWHExpertServiceImpl implements DWHExpertService {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;


    @Override
    @Transactional
    public List<ExpertProfilePreviewDto> findExperts(Integer limit, Integer offset) {
        final Criteria idCriteria = hibernateCriteriaCreator.createCriteria(ExpertEntity.class, "expert");
        idCriteria.setMaxResults(limit);
        idCriteria.setFirstResult(offset);
        idCriteria.addOrder(Order.asc("expertId"));
        idCriteria.setProjection(Projections.property("expertId"));
        final List<Long> expertIds = idCriteria.list();
        if (expertIds.isEmpty()) return Collections.emptyList();

        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ExpertEntity.class, "expert");
        criteria.createAlias("expert.expertAffiliationEntities", "expertAffiliation", JoinType.LEFT_OUTER_JOIN);
        criteria.setFetchMode("expertAffiliation", FetchMode.JOIN);
        criteria.add(Restrictions.in("expertId", expertIds));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

        final List<ExpertEntity> list = criteria.list();
        return list.stream().map(this::convertPreview).collect(Collectors.toList());
    }

    private ExpertProfilePreviewDto convertPreview(ExpertEntity expertEntity) {
        final ExpertProfilePreviewDto expertProfilePreviewDto = new ExpertProfilePreviewDto();
        expertProfilePreviewDto.setExpertId(expertEntity.getExpertId());
        expertProfilePreviewDto.setExpertFullName(expertEntity.getFullName());
        final Set<Long> organizationIds = expertEntity.getExpertAffiliationEntities().stream()
                .map(ExpertAffiliationEntity::getOrganizationEntity)
                .filter(Objects::nonNull)
                .map(OrganizationEntity::getOrganizationId)
                .collect(Collectors.toSet());
        expertProfilePreviewDto.setOriginalOrganizationIds(organizationIds);
        return expertProfilePreviewDto;
    }

    private ClaimSuggestionDto convertClaimSuggestion(ExpertEntity expertEntity) {
        final ClaimSuggestionDto claimSuggestionDto = new ClaimSuggestionDto();
        claimSuggestionDto.setExpertId(expertEntity.getExpertId());
        claimSuggestionDto.setExpertFullName(expertEntity.getFullName());
        final Set<String> itemNames = claimSuggestionDto.getItemNames();
        final Set<String> organizationNames = claimSuggestionDto.getOrganizationNames();
        for (ExpertItemEntity expertItemEntity : expertEntity.getExpertItemEntities()) {
            final ItemEntity itemEntity = expertItemEntity.getItemEntity();
            final String itemName = (itemEntity.getItemNameEn() != null && !itemEntity.getItemNameEn().isEmpty() ? itemEntity.getItemNameEn() : itemEntity.getItemNameOrig());
            itemNames.add(itemName);
        }
        for (ExpertAffiliationEntity expertAffiliationEntity : expertEntity.getExpertAffiliationEntities()) {
            final OrganizationEntity organizationEntity = expertAffiliationEntity.getOrganizationEntity();
            organizationNames.add(organizationEntity.getOrganizationName());
        }

        return claimSuggestionDto;
    }

    private List<ExpertItemPreviewDto> convertItems(Set<ExpertItemEntity> expertItemEntities) {
        return expertItemEntities.stream()
                .map(ExpertItemEntity::getItemEntity).map(ItemUtil::convert).collect(Collectors.toList());
    }

    private Criteria prepareExpertsCriteria() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(ExpertEntity.class, "expert");
        criteria.createAlias("expert.expertAffiliationEntities", "expertAffiliation", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("expertAffiliation.organizationEntity", "organizationEntity", JoinType.LEFT_OUTER_JOIN);
        criteria.setFetchMode("expertAffiliation", FetchMode.JOIN);
        criteria.setFetchMode("organizationEntity", FetchMode.JOIN);
        return criteria;
    }
}
