package ai.unico.platform.extdata.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_organization_struct")
public class OrganizationStructureEntity {

    @Id
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_organization_id_tk")
    private OrganizationEntity parentOrganization;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "child_organization_id_tk")
    private OrganizationEntity childOrganization;

    private String description;

    public OrganizationEntity getParentOrganization() {
        return parentOrganization;
    }

    public void setParentOrganization(OrganizationEntity parentOrganization) {
        this.parentOrganization = parentOrganization;
    }

    public OrganizationEntity getChildOrganization() {
        return childOrganization;
    }

    public void setChildOrganization(OrganizationEntity childOrganization) {
        this.childOrganization = childOrganization;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
