package ai.unico.platform.extdata.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "t_project")
public class ProjectEntity {

    @Id
    @Column(name = "project_id_tk")
    private Long projectId;

    @Column(name = "project_number")
    private String projectNumber;

    @Column(name = "project_name_en")
    private String projectName;

    @Column(name = "project_name_orig")
    private String projectNameOrig;

    @Column(name = "project_desc_en")
    private String projectDescription;

    @Column(name = "project_desc_orig")
    private String projectDescriptionOrig;

    @Column(name = "project_start_date")
    private Date startDate;

    @Column(name = "project_end_date")
    private Date endDate;

    @Column(name = "project_state")
    private String countryCode;

    private String projectType;

    @Column(name = "project_link")
    private String projectLink;

    private boolean confidential;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_t_project_program")
    private ProjectProgramEntity projectProgramEntity;

    @Column(name = "project_keywords_en")
    private String keywords;

    @Column(name = "project_keywords_orig")
    private String keywordsOrig;

    @OneToMany
    @JoinColumn(name = "project_id_tk_t_project")
    private Set<ProjectFinanceEntity> projectFinanceEntities;

    @OneToMany
    @JoinColumn(name = "project_id_tk_t_project")
    private Set<ProjectItemEntity> projectItemEntities;

    @OneToMany
    @JoinColumn(name = "project_id_tk_t_project")
    private Set<ProjectOrganizationEntity> projectOrganizationEntities;

    @OneToMany
    @JoinColumn(name = "project_id_tk_t_project")
    private Set<ProjectExpertEntity> projectExpertEntities;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectNumber(String projectCode) {
        this.projectNumber = projectCode;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getProjectType() {
        return projectType;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Set<ProjectFinanceEntity> getProjectFinanceEntities() {
        return projectFinanceEntities;
    }

    public void setProjectFinanceEntities(Set<ProjectFinanceEntity> projectFinanceEntities) {
        this.projectFinanceEntities = projectFinanceEntities;
    }

    public Set<ProjectItemEntity> getProjectItemEntities() {
        return projectItemEntities;
    }

    public void setProjectItemEntities(Set<ProjectItemEntity> projectItemEntities) {
        this.projectItemEntities = projectItemEntities;
    }

    public Set<ProjectOrganizationEntity> getProjectOrganizationEntities() {
        return projectOrganizationEntities;
    }

    public void setProjectOrganizationEntities(Set<ProjectOrganizationEntity> projectOrganizationEntities) {
        this.projectOrganizationEntities = projectOrganizationEntities;
    }

    public ProjectProgramEntity getProjectProgramEntity() {
        return projectProgramEntity;
    }

    public void setProjectProgramEntity(ProjectProgramEntity projectProgramEntity) {
        this.projectProgramEntity = projectProgramEntity;
    }

    public boolean isConfidential() {
        return confidential;
    }

    public void setConfidential(boolean confidential) {
        this.confidential = confidential;
    }

    public String getProjectDescriptionOrig() {
        return projectDescriptionOrig;
    }

    public void setProjectDescriptionOrig(String projectDescriptionOrig) {
        this.projectDescriptionOrig = projectDescriptionOrig;
    }

    public String getProjectNameOrig() {
        return projectNameOrig;
    }

    public void setProjectNameOrig(String projectNameOrig) {
        this.projectNameOrig = projectNameOrig;
    }

    public String getKeywordsOrig() {
        return keywordsOrig;
    }

    public void setKeywordsOrig(String keywordsOrig) {
        this.keywordsOrig = keywordsOrig;
    }

    public Set<ProjectExpertEntity> getProjectExpertEntities() {
        return projectExpertEntities;
    }

    public void setProjectExpertEntities(Set<ProjectExpertEntity> projectExpertEntities) {
        this.projectExpertEntities = projectExpertEntities;
    }

    public String getProjectLink() {
        return projectLink;
    }

    public void setProjectLink(String projectLink) {
        this.projectLink = projectLink;
    }
}
