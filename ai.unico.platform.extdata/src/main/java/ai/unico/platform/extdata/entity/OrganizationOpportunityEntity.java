package ai.unico.platform.extdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "t_opportunity_t_organization_rel")
public class OrganizationOpportunityEntity implements Serializable {
    @Id
    @Column(name = "opportunity_id_tk_t_opportunity")
    private Long opportunityId;
    @Id
    @Column(name = "organization_id_tk_t_organization")
    private Long organizationId;
    private String role;

    public Long getOpportunityId() {
        return opportunityId;
    }

    public void setOpportunityId(Long opportunityId) {
        this.opportunityId = opportunityId;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
