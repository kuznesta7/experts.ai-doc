package ai.unico.platform.extdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "t_project_t_finance")
public class ProjectFinanceEntity implements Serializable {

    @Id
    @Column(name = "organization_id_tk_t_organization")
    private Long organizationId;

    @Id
    @Column(name = "project_id_tk_t_project")
    private Long projectId;

    @Id
    @Column(name = "obdobi")
    private String year;

    @Column(name = "celkove_uzn_nakl")
    private Long total;

    @Column(name = "vyse_podpory_ze_stat_rozp")
    private Long nationalSupport;

    @Column(name = "never_zdr_fin")
    private Long privateFinances;

    @Column(name = "ostat_fin_zdr")
    private Long otherFinances;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getNationalSupport() {
        return nationalSupport;
    }

    public void setNationalSupport(Long nationalSupport) {
        this.nationalSupport = nationalSupport;
    }

    public Long getPrivateFinances() {
        return privateFinances;
    }

    public void setPrivateFinances(Long privateFinances) {
        this.privateFinances = privateFinances;
    }

    public Long getOtherFinances() {
        return otherFinances;
    }

    public void setOtherFinances(Long otherFinances) {
        this.otherFinances = otherFinances;
    }
}
