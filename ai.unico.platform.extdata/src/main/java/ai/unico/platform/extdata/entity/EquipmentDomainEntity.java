package ai.unico.platform.extdata.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_equipment_domain")
public class EquipmentDomainEntity {
    @Id
    private Long domainId;

    private String domainNameEn;

    public Long getDomainId() {
        return domainId;
    }

    public void setDomainId(Long domainId) {
        this.domainId = domainId;
    }

    public String getDomainNameEn() {
        return domainNameEn;
    }

    public void setDomainNameEn(String domainNameEn) {
        this.domainNameEn = domainNameEn;
    }
}
