package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.CategoryDwhDto;
import ai.unico.platform.extdata.entity.JobTypeEntity;
import ai.unico.platform.extdata.entity.OpportunityTypeEntity;
import ai.unico.platform.extdata.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

public class OpportunityCategoryDwhServiceImpl implements OpportunityCategoryDwhService {

    private List<CategoryDwhDto> jobType;

    private List<CategoryDwhDto> opportunityType;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    @Transactional
    public List<CategoryDwhDto> getJobTypes() {
        if (jobType != null)
            return jobType;
        Criteria criteria = hibernateCriteriaCreator.createCriteria(JobTypeEntity.class, "type");
        List<JobTypeEntity> result = criteria.list();
        jobType = result.stream().map(this::convert).collect(Collectors.toList());
        return jobType;
    }

    @Override
    @Transactional
    public List<CategoryDwhDto> getOpportunityTypes() {
        if (opportunityType != null)
            return opportunityType;
        Criteria criteria = hibernateCriteriaCreator.createCriteria(OpportunityTypeEntity.class, "type");
        List<OpportunityTypeEntity> result = criteria.list();
        opportunityType = result.stream().map(this::convert).collect(Collectors.toList());
        return opportunityType;
    }

    private CategoryDwhDto convert(OpportunityTypeEntity entity) {
        CategoryDwhDto dto = new CategoryDwhDto();
        dto.setId(entity.getOpportunityTypeId());
        dto.setName(entity.getName());
        return dto;
    }

    private CategoryDwhDto convert(JobTypeEntity entity) {
        CategoryDwhDto dto = new CategoryDwhDto();
        dto.setId(entity.getJobId());
        dto.setName(entity.getTypeName());
        return dto;
    }
}
