package ai.unico.platform.extdata.util;

import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;

import java.util.function.Function;

public class HibernateUtil {
    public static <T, Id> Id getId(T o, Function<T, Id> function) {
        if (o instanceof HibernateProxy) {
            final LazyInitializer lazyInitializer = ((HibernateProxy) o).getHibernateLazyInitializer();
            if (lazyInitializer.isUninitialized()) {
                return (Id) lazyInitializer.getIdentifier();
            }
        }
        return function.apply(o);
    }

    public static <T, E, Id> Id getId(T o, Function<T, E> function, Function<E, Id> functionId) {
        final E e = function.apply(o);
        if (e == null) return null;
        if (e instanceof HibernateProxy) {
            final LazyInitializer lazyInitializer = ((HibernateProxy) e).getHibernateLazyInitializer();
            if (lazyInitializer.isUninitialized()) {
                return (Id) lazyInitializer.getIdentifier();
            }
        }
        return functionId.apply(e);
    }

    public static <T> Function<T, Long> getId(Function<T, Long> function) {
        return new Function<T, Long>() {
            @Override
            public Long apply(T t) {
                return getId(t, function);
            }
        };
    }

    public static <T, E> Function<T, Long> getId(Function<T, E> function, Function<E, Long> functionId) {
        return new Function<T, Long>() {
            @Override
            public Long apply(T t) {
                return getId(t, function, functionId);
            }
        };
    }

}
