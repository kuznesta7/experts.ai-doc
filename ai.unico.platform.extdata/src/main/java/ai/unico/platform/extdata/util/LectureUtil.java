package ai.unico.platform.extdata.util;

import ai.unico.platform.extdata.dto.ExpertLectureDto;
import ai.unico.platform.extdata.entity.ItemEntity;
import ai.unico.platform.util.KeywordsUtil;

public class LectureUtil {

    public static ExpertLectureDto convert(ItemEntity itemEntity) {
        //todo ask if add translated fields or not
        final ExpertLectureDto expertLectureDto = new ExpertLectureDto();
        // final String keywordsText = itemEntity.getKeywordsTextEn() != null ? itemEntity.getKeywordsTextEn() : itemEntity.getKeywordsTextOrig();
        // final String description = itemEntity.getDescriptionEn() != null ? itemEntity.getDescriptionEn() : itemEntity.getDescriptionOrig();
        final String keywordsText = itemEntity.getKeywordsTextOrig();
        final String description = itemEntity.getDescriptionOrig();
       /* if (itemEntity.getItemNameEn() == null) {
            expertLectureDto.setTranslationUnavailable(true);
            expertLectureDto.setItemName(itemEntity.getItemNameOrig());
        } else {
            expertLectureDto.setItemName(itemEntity.getItemNameEn());
        }*/
        expertLectureDto.setItemName(itemEntity.getItemNameOrig());

        if (itemEntity.getItemBk() != null && itemEntity.getItemBk().startsWith("RIV/"))
            expertLectureDto.setItemBk(itemEntity.getItemBk());
        expertLectureDto.setItemDescription(description);
        expertLectureDto.setItemId(itemEntity.getItemId());
        expertLectureDto.setItemTypeId(itemEntity.getItemTypeId());
        expertLectureDto.setYear(itemEntity.getItemYear());
        expertLectureDto.setConfidential(itemEntity.isConfidential());
        expertLectureDto.setKeywords(KeywordsUtil.convert(keywordsText));
        expertLectureDto.setItemDomain(itemEntity.getItemSpecGroup());
        expertLectureDto.setItemSpecification(itemEntity.getItemSpec());
        expertLectureDto.setItemScienceSpecification(itemEntity.getItemSpecScience());

        return expertLectureDto;
    }
}
