package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.CategoryDwhDto;
import ai.unico.platform.extdata.entity.EquipmentDomainEntity;
import ai.unico.platform.extdata.entity.EquipmentTypeEntity;
import ai.unico.platform.extdata.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

public class EquipmentCategoryServiceImpl implements EquipmentCategoryService {

    private List<CategoryDwhDto> equipmentTypes;

    private List<CategoryDwhDto> equipmentDomains;

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    @Transactional
    public List<CategoryDwhDto> getEquipmentType() {
        if (equipmentTypes != null)
            return equipmentTypes;
        Criteria criteria = hibernateCriteriaCreator.createCriteria(EquipmentTypeEntity.class, "eqType");
        List<EquipmentTypeEntity> entities = criteria.list();
        equipmentTypes = entities.stream().map(x -> {
            CategoryDwhDto a = new CategoryDwhDto();
            a.setId(x.getTypeId());
            a.setName(x.getTypeNameEn());
            return a;
        }).collect(Collectors.toList());
        return equipmentTypes;
    }

    @Override
    @Transactional
    public List<CategoryDwhDto> getEquipmentDomain() {
        if (equipmentDomains != null)
            return equipmentDomains;
        Criteria criteria = hibernateCriteriaCreator.createCriteria(EquipmentDomainEntity.class, "eqDomain");
        List<EquipmentDomainEntity> entities = criteria.list();
        equipmentDomains = entities.stream().map(x -> {
            CategoryDwhDto a = new CategoryDwhDto();
            a.setId(x.getDomainId());
            a.setName(x.getDomainNameEn());
            return a;
        }).collect(Collectors.toList());
        return equipmentDomains;
    }
}
