package ai.unico.platform.extdata.util;

import ai.unico.platform.extdata.dto.ExpertItemPreviewDto;
import ai.unico.platform.extdata.entity.ItemEntity;
import ai.unico.platform.util.KeywordsUtil;

import java.util.Arrays;
import java.util.Comparator;

public class ItemUtil {

    public static ExpertItemPreviewDto convert(ItemEntity itemEntity) {
        final ExpertItemPreviewDto expertItemPreviewDto = new ExpertItemPreviewDto();
        final String keywordsText = itemEntity.getKeywordsTextEn() != null ? itemEntity.getKeywordsTextEn() : itemEntity.getKeywordsTextOrig();
        final String description = itemEntity.getDescriptionEn() != null ? itemEntity.getDescriptionEn() : itemEntity.getDescriptionOrig();
        if (itemEntity.getItemNameEn() == null) {
            expertItemPreviewDto.setTranslationUnavailable(true);
            expertItemPreviewDto.setItemName(itemEntity.getItemNameOrig());
        } else {
            expertItemPreviewDto.setItemName(itemEntity.getItemNameEn());
        }
        if (itemEntity.getItemBk() != null && itemEntity.getItemBk().startsWith("RIV/"))
            expertItemPreviewDto.setItemBk(itemEntity.getItemBk());
        expertItemPreviewDto.setItemDescription(description);
        expertItemPreviewDto.setItemId(itemEntity.getItemId());
        expertItemPreviewDto.setItemTypeId(itemEntity.getItemTypeId());
        expertItemPreviewDto.setYear(itemEntity.getItemYear());
        expertItemPreviewDto.setConfidential(itemEntity.isConfidential());
        expertItemPreviewDto.setKeywords(KeywordsUtil.convert(keywordsText));
        expertItemPreviewDto.setDoi(itemEntity.getDoi());
        expertItemPreviewDto.setItemLink(itemEntity.getItemLink());

        return expertItemPreviewDto;
    }
}
