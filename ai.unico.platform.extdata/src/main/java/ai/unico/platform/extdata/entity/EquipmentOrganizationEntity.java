package ai.unico.platform.extdata.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "t_organizaiton_t_equipment_rel")
public class EquipmentOrganizationEntity implements Serializable {

    @Id
    @Column(name = "equipment_id_tk_t_equipment")
    private Long equipmentId;

    @Id
    @Column(name = "organization_id_tk_t_organization")
    private Long organizationId;

    public Long getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(Long equipmentId) {
        this.equipmentId = equipmentId;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }
}
