package ai.unico.platform.extdata.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_equipment")
public class EquipmentEntity {
    @Id
    @Column(name = "equipment_id_tk")
    private Long equipmentId;
    private String equipmentNameOrig;
    private String equipmentNameEn;
    private String equipmentMarking;
    private String equipmentDescOrig;
    private String equipmentDescEn;
    private String equipmentSpecializationOrig;
    private String equipmentSpecializationEn;
    private Integer year;
    private Integer equipmentServiceLife;
    private Boolean portableDevice;
    @ManyToOne
    @JoinColumn(name = "domain_id_t_equipment_domain")
    private EquipmentDomainEntity equipmentDomainEntity;
    @ManyToOne
    @JoinColumn(name = "type_id_t_equipment_type")
    private EquipmentTypeEntity equipmentTypeEntity;

 public Long getEquipmentId() {
  return equipmentId;
 }

 public void setEquipmentId(Long equipmentId) {
  this.equipmentId = equipmentId;
 }

 public String getEquipmentNameOrig() {
  return equipmentNameOrig;
 }

 public void setEquipmentNameOrig(String equipmentNameOrig) {
  this.equipmentNameOrig = equipmentNameOrig;
 }

 public String getEquipmentNameEn() {
  return equipmentNameEn;
 }

 public void setEquipmentNameEn(String equipmentNameEn) {
  this.equipmentNameEn = equipmentNameEn;
 }

 public String getEquipmentMarking() {
  return equipmentMarking;
 }

 public void setEquipmentMarking(String equipmentMarking) {
  this.equipmentMarking = equipmentMarking;
 }

 public String getEquipmentDescOrig() {
  return equipmentDescOrig;
 }

 public void setEquipmentDescOrig(String equipmentDescOrig) {
  this.equipmentDescOrig = equipmentDescOrig;
 }

 public String getEquipmentDescEn() {
  return equipmentDescEn;
 }

 public void setEquipmentDescEn(String equipmentDescEn) {
  this.equipmentDescEn = equipmentDescEn;
 }

 public String getEquipmentSpecializationOrig() {
  return equipmentSpecializationOrig;
 }

 public void setEquipmentSpecializationOrig(String equipmentSpecializationOrig) {
  this.equipmentSpecializationOrig = equipmentSpecializationOrig;
 }

 public String getEquipmentSpecializationEn() {
  return equipmentSpecializationEn;
 }

 public void setEquipmentSpecializationEn(String equipmentSpecializationEn) {
  this.equipmentSpecializationEn = equipmentSpecializationEn;
 }

 public Integer getYear() {
  return year;
 }

 public void setYear(Integer year) {
  this.year = year;
 }

 public Integer getEquipmentServiceLife() {
  return equipmentServiceLife;
 }

 public void setEquipmentServiceLife(Integer equipmentServiceLife) {
  this.equipmentServiceLife = equipmentServiceLife;
 }

 public Boolean getPortableDevice() {
  return portableDevice;
 }

 public void setPortableDevice(Boolean portableDevice) {
  this.portableDevice = portableDevice;
 }

 public EquipmentDomainEntity getEquipmentDomainEntity() {
  return equipmentDomainEntity;
 }

 public void setEquipmentDomainEntity(EquipmentDomainEntity equipmentDomainEntity) {
  this.equipmentDomainEntity = equipmentDomainEntity;
 }

 public EquipmentTypeEntity getEquipmentTypeEntity() {
  return equipmentTypeEntity;
 }

 public void setEquipmentTypeEntity(EquipmentTypeEntity equipmentTypeEntity) {
  this.equipmentTypeEntity = equipmentTypeEntity;
 }
}
