package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.dto.DWHOrganizationDto;
import ai.unico.platform.extdata.dto.DWHOrganizationStructureDto;
import ai.unico.platform.extdata.entity.OrganizationEntity;
import ai.unico.platform.extdata.entity.OrganizationMemberEntity;
import ai.unico.platform.extdata.entity.OrganizationStructureEntity;
import ai.unico.platform.extdata.hibernate.HibernateCriteriaCreator;
import ai.unico.platform.extdata.util.HibernateUtil;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DWHOrganizationServiceImpl implements DWHOrganizationService {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    @Transactional
    public List<DWHOrganizationDto> findOrganizations() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationEntity.class, "organization");
        final List<OrganizationEntity> list = criteria.list();
        return list.stream().map(this::convert).collect(Collectors.toList());
    }

    @Override
    public List<DWHOrganizationStructureDto> findOrganizationStructure() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationStructureEntity.class, "organization");
        final List<OrganizationStructureEntity> list = criteria.list();
        final List<DWHOrganizationStructureDto> result = new ArrayList<>();
        for (OrganizationStructureEntity organizationStructureEntity : list) {
            final DWHOrganizationStructureDto dto = new DWHOrganizationStructureDto();
            dto.setParentOrganizationId(HibernateUtil.getId(organizationStructureEntity, OrganizationStructureEntity::getParentOrganization, OrganizationEntity::getOrganizationId));
            dto.setChildOrganizationId(HibernateUtil.getId(organizationStructureEntity, OrganizationStructureEntity::getChildOrganization, OrganizationEntity::getOrganizationId));
            dto.setRelationType(organizationStructureEntity.getDescription());
            result.add(dto);
        }
        return result;
    }

    @Override
    public List<DWHOrganizationStructureDto> findOrganizationMembers() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(OrganizationMemberEntity.class, "member");
        final List<OrganizationMemberEntity> list = criteria.list();
        final List<DWHOrganizationStructureDto> result = new ArrayList<>();
        for (OrganizationMemberEntity e :
                list) {
            final DWHOrganizationStructureDto dto = new DWHOrganizationStructureDto();
            dto.setParentOrganizationId(e.getParent());
            dto.setChildOrganizationId(e.getChild());
            dto.setRelationType("MEMBER");
            result.add(dto);
        }
        return result;
    }


    private DWHOrganizationDto convert(OrganizationEntity organizationEntity) {
        final DWHOrganizationDto organizationDto = new DWHOrganizationDto();
        final String countryCode = organizationEntity.getCountryCode();
        organizationDto.setOriginalOrganizationId(organizationEntity.getOrganizationId());
        organizationDto.setOrganizationName(organizationEntity.getOrganizationName());
        organizationDto.setRank(organizationEntity.getItemCount());
        organizationDto.setSubstituteOrganizationId(organizationEntity.getSubstituteOrganizationId());
        organizationDto.setCountryCode(countryCode != null ? countryCode.toLowerCase() : null);
        return organizationDto;
    }

}
