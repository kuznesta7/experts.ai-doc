package ai.unico.platform.extdata.service;

import ai.unico.platform.extdata.entity.DwhVariableEntity;
import ai.unico.platform.extdata.hibernate.HibernateCriteriaCreator;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DWHVariableServiceImpl implements DWHVariableService {

    @Autowired
    private HibernateCriteriaCreator hibernateCriteriaCreator;

    @Override
    @Transactional
    public String getDWHVersion() {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(DwhVariableEntity.class, "variable");
        final List<DwhVariableEntity> list = criteria.list();
        final Map<String, String> variableMap = list.stream().collect(Collectors.toMap(DwhVariableEntity::getName, DwhVariableEntity::getValue));
        return variableMap.get("schema_version") + "." + variableMap.get("data_version");
    }
}
