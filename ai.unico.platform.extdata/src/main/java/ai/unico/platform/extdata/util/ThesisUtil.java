package ai.unico.platform.extdata.util;

import ai.unico.platform.extdata.dto.ExpertThesisPreviewDto;
import ai.unico.platform.extdata.entity.ItemEntity;
import ai.unico.platform.util.KeywordsUtil;

import java.util.Arrays;
import java.util.Comparator;

public class ThesisUtil {

    public static ExpertThesisPreviewDto convert(ItemEntity itemEntity) {
        final ExpertThesisPreviewDto expertThesisPreviewDto = new ExpertThesisPreviewDto();
        final String keywordsText = itemEntity.getKeywordsTextEn() != null ? itemEntity.getKeywordsTextEn() : itemEntity.getKeywordsTextOrig();
        final String description = itemEntity.getDescriptionEn() != null ? itemEntity.getDescriptionEn() : itemEntity.getDescriptionOrig();
        if (itemEntity.getItemNameEn() == null) {
            expertThesisPreviewDto.setTranslationUnavailable(true);
            expertThesisPreviewDto.setItemName(itemEntity.getItemNameOrig());
        } else {
            expertThesisPreviewDto.setItemName(itemEntity.getItemNameEn());
        }
        if (itemEntity.getItemBk() != null && itemEntity.getItemBk().startsWith("RIV/"))
            expertThesisPreviewDto.setItemBk(itemEntity.getItemBk());
        expertThesisPreviewDto.setItemDescription(description);
        expertThesisPreviewDto.setItemId(itemEntity.getItemId());
        expertThesisPreviewDto.setItemTypeId(itemEntity.getItemTypeId());
        expertThesisPreviewDto.setYear(itemEntity.getItemYear());
        expertThesisPreviewDto.setConfidential(itemEntity.isConfidential());
        expertThesisPreviewDto.setKeywords(KeywordsUtil.convert(keywordsText));

        return expertThesisPreviewDto;
    }
}
