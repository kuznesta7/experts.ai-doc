package db.migration;

import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class V3__OrganizationsItemCount extends BaseJavaMigration {

    @Override
    public void migrate(Context context) {
        final DataSource dataSource = context.getConfiguration().getDataSource();
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        final List<Map<String, Object>> organizationsItemCount = jdbcTemplate.queryForList("select organization_id_tk_t_organization, count(item_id_tk_t_item) as item_count from t_item_t_organization_rel group by organization_id_tk_t_organization order by item_count desc");
        final List<Object[]> batchArgs = new ArrayList<>();
        for (Map<String, Object> row : organizationsItemCount) {
            final List<Object> objects = new ArrayList<>();
            objects.add(row.get("item_count"));
            objects.add(row.get("organization_id_tk_t_organization"));
            batchArgs.add(objects.toArray());
        }
        System.out.println("organization item count done... writing");
        System.out.println(organizationsItemCount.size() + " records");
        jdbcTemplate.batchUpdate("update t_organization set item_count=? where organization_id_tk=?", batchArgs);
    }
}
