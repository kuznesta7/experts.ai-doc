package db.migration;

import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class V4__OrganizationsMerge extends BaseJavaMigration {

    @Override
    public void migrate(Context context) {
        final DataSource dataSource = context.getConfiguration().getDataSource();
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        final List<Map<String, Object>> organizations = jdbcTemplate.queryForList("select organization_id_tk, transformed from t_organization where substitute_org_tk isnull order by item_count desc nulls last");
        long i = 0;
        final List<Long> organizationIds = new ArrayList<>();
        final List<String> processedOrganizations = new ArrayList<>();
        for (Map<String, Object> organization : organizations) {
            i++;
            if (i % 1000 == 0) System.out.println(i);
            final Long organizationId = (Long) organization.get("organization_id_tk");
            final String transformedOrganizationName = (String) organization.get("transformed");
            if (transformedOrganizationName == null || transformedOrganizationName.isEmpty() || processedOrganizations.contains(transformedOrganizationName))
                continue;
            processedOrganizations.add(transformedOrganizationName);
            organizationIds.add(organizationId);
        }
        jdbcTemplate.batchUpdate("update t_organization set substitute_org_tk = ? where transformed = ? and organization_id_tk != ? and substitute_org_tk isnull", new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setLong(1, organizationIds.get(i));
                ps.setString(2, processedOrganizations.get(i));
                ps.setLong(3, organizationIds.get(i));
            }

            @Override
            public int getBatchSize() {
                return processedOrganizations.size();
            }
        });
    }
}
